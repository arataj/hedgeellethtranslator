/*
 * About.java
 *
 * Created on May 4, 2011, 1:06:16 PM
 *
 * Copyright (c) 2011 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth;

/**
 * Returns name and version information about Hedgeelleth.
 * 
 * @author Artur Rataj
 */
public class About {
    /**
     * Proper name.
     */
    public static final String NAME = "Verics compiler";
    /**
     * Version.
     */
    public static final String VERSION = "20180511";
    /**
     * Full desciption, including the library name and version.
     */
    public static final String DESCRIPTION = NAME + " version " + VERSION;
}
