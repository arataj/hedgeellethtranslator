/*
 * Hedgeelleth.java
 *
 * Created on Sep 19, 2008
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.CommentTag;
import pl.gliwice.iitis.hedgeelleth.compiler.CompilerAnnotation;
import pl.gliwice.iitis.hedgeelleth.compiler.primitiverange.PrimitiveRangeDescription;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.clazz.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.flags.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.scope.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.CompilerException.Report;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;
import pl.gliwice.iitis.hedgeelleth.interpreter.ModelControl;
import pl.gliwice.iitis.hedgeelleth.modules.AbstractFrontend;
import pl.gliwice.iitis.hedgeelleth.parser.TokenMgrError;

/**
 * A library with methods of parser actions.<br>
 *
 * A single object is intented for a single parsed file.<br>
 * 
 * Every method intended to be called from inside parser actions
 * has its calling policy specified after the word "Calling:".
 * 
 * @author Artur Rataj
 */
public class Hedgeelleth {
    /**
     * An indicator that the called method is some "super"
     * constructor to be found during semantic check.<br>
     *
     * Used when generating constructor calls within
     * constructors.
     */
    public static final Method CONSTRUCTOR_SUPER = new Method("super");
    /**
     * An indicator that the called method is some "this"
     * constructor to be found during semantic check.<br>
     *
     * Used when generating constructor calls within
     * constructors.
     */
    public static final Method CONSTRUCTOR_THIS = new Method("this");
    /**
     * Name of the default package.
     */
    public static final String DEFAULT_PACKAGE = "#default";
    /**
     * Name of the java class tag, that, for some languages, makes
     * the parser stop analysing the current compilation unit,
     * including the class or interface, that contains the token.
     */
    public static final String SKIP_REST_TAG_STRING = "hcSkipRest";
    /**
     * Name of the java class tag, that informs the backend, that constant
     * field in the class are hc internal, and thus should not be used by
     * the user or visible etc.
     */
    public static final String INTERNAL_CONSTANTS_TAG_STRING =
            "hcInternalConstants";

    public static final String LIBRARY_PATH_VARIABLE_NAME_STRING = "hcLibrary";

    /**
      * Parser.
      */
    protected StreamAttributesParser parser;
    /**
     * Compilation.
     */
    protected Compilation compilation;
    /**
     * Compilation unit.
     */
    protected Unit unit;
    /**
     * Unique identifier of the parsed class. See
     * <code>setClassUniqueIdentifier</code> for details.
     */
    protected String classUniqueIdentifier;
    /**
     * Reports of possible parse errors.
     */
    protected ParseException parseErrors;
    /**
     * A collection of named comment tags, which actually have been used, or at
     * least have already caused a parse error. Used to find stray comment tags
     * in the source.<br>
     *
     * Only named comment tags are allowed, as unnamed comment tags are typically
     * only range modifiers, whose usage is collected using <code>prUsage</code>.
     */
    protected Collection<CommentTag> nctUsage;
    /**
     * A collection of primitive range descriptions, which actually have been
     * used, or at least caused a parse error. Used to find
     * stray primitive ranges in the source.
     */
    protected Collection<PrimitiveRangeDescription> prUsage;

    /**
     * Constructs a new library for generated parsers.
     * 
     * @param parser                    parser
     * @param compilation               compilation
     * @param libraryPath               library path, to mark it as a special
     *                                  element of the stream name and thus provide
     *                                  relocability of the name; null for none
     * @param streamName                name of the parsed stream; null for unknown
     */
    public Hedgeelleth(StreamAttributesParser parser, Compilation compilation,
            String libraryPath, String streamName) {
        this.parser = parser;
        this.compilation = compilation;
        parseErrors = new ParseException();
        if(libraryPath != null && streamName != null && streamName.startsWith(libraryPath))
            streamName = "${" + LIBRARY_PATH_VARIABLE_NAME_STRING + "}" +
                    streamName.substring(libraryPath.length());
        parser.setStreamName(streamName);
        nctUsage = new LinkedList<>();
        prUsage = new LinkedList<>();
    }
    /**
     * Returns the parser.
     *
     * @return unit                     compilation unit
     */
    public StreamAttributesParser getParser() {
        return parser;
    }
    /**
     * Returns the current compilation.
     *
     * @return                          compilation
     */
    public Compilation getCompilation() {
        return compilation;
    }
    /**
     * Returns the current unit.
     *
     * @return                          compilation unit
     */
    public Unit getUnit() {
        return unit;
    }
    /**
     * Used by library methods to get current stream position, if none
     * is given.
     * 
     * @param streamPos                 if not null, stream position
     *                                  to return, otherwise the current
     *                                  position in the parsed stream
     *                                  is returned
     * @return                          position in the parsed stream
     *
     * Calling: optional.
     */
    private StreamPos getStreamPos(StreamPos pos) {
        if(pos != null)
            return pos;
        else
            return parser.getStreamPos();
    }
    /**
     * Adds all error reports from a given exception into the list
     * of parse errors in this object.
     * 
     * Calling: optional.
     * 
     * @param exception
     */
    public void addParseException(ParseException exception) {
        parseErrors.addAllReports(exception);
    }
    /**
     * Returns all reported parse errors.
     *
     * @return                          a parse exception with the error
     *                                  reports, the list of reports is
     *                                  empty if no parse errors occured
     */
    public ParseException getParseException() {
        return parseErrors;
    }
    /**
     * Skips the token stream.
     * Stops <i>after</i> the first token <code>after</code> in the parsed stream,
     * or <i>before</i> EOF or tokens <code>before</code>, whichever of the
     * mentioned tokens is found first, and if both, then the <code>after</code>
     * tokens have priority. Takes into account also the current token, so,
     * depending on its value, can skip even no single token.<br>
     * 
     * Does not skip at all, if some of the conditions is immediately fulfilled.<br>
     *
     * The <code>before</code> tokens are used as an expected ones
     * <i>following</i> an expression, and still being expected by the
     * parser, for example a <code>while</code> condition
     * might be followed by ")". Recognition of the type of tokens
     * works only if <code>token.next</code> is not null, where
     * <code>token</code> is the last skipped token, or, if
     * <code>token.next</code> is null and <code>backupLength != -1</code>
     * is true, then as a "last resort" it uses <code>token</code>
     * instead of <code>token.next</code> and then backs up
     * the stream by <code>backupLength</code>. The back up should be only
     * used if problems that the back up can cause can be avoided.
     *
     * Typically used by error recovery routines.<br>
     *
     * @param images                    array <code>tokenImage<code> from
     *                                  a parser constants file generated
     *                                  by JavaCC
     * @param after                     entries in the array of images,
     *                                  determines the token to stop after
     * @param before                    entries in the array of images,
     *                                  determine the tokens before whose
     *                                  the skipping should be stopped
     * @param backupLength              stream backup length, -1 to disable,
     *                                  enable only if potential back up
     *                                  problems can be avoided
     * @return                          skipped tokens, does not contain the
     *                                  current token
     */
    public List<Token> skipTokens(String[] images, String[] after, String[] before,
            int backupLength) {
        List<Token> skippedTokens = new LinkedList<>();
        final int TOKEN_KIND_EOF = 0;
        Token t = parser.getCurrentToken();
        NEXT_TOKEN:
        while(true) {
            if(t.kind == TOKEN_KIND_EOF)
                break;
            for(int i =  0; i < after.length; ++i)
                if(images[t.kind].equals(after[i])) {
                    break NEXT_TOKEN;
                }
            if(t.next != null) {
                for(String b : before)
                    if(images[t.next.kind].equals(b))
                        break NEXT_TOKEN;
            } else if(backupLength != -1) {
                for(String b : before)
                    if(images[t.kind].equals(b)) {
                        parser.backupStream(b.length() - 2);
                        if(!skippedTokens.isEmpty())
                            skippedTokens.remove(skippedTokens.size() - 1);
                        break NEXT_TOKEN;
                    }
            }
            try {
                skippedTokens.add(parser.getCurrentToken());
                t = parser.getNextToken();
            } catch(TokenMgrError error) {
                // likely EOF was found
                break NEXT_TOKEN;
            }
        }
        return skippedTokens;
    }
    /**
     * Tests if an unspecified associativity occurs between operators in some
     * sequence of operators and operands, that does not contain parentheses or
     * other priority-specifying parts. The operators in the sequence should
     * all be of the same priority.
     *
     * @param sets                      sets of operators, operators within each
     *                                  set have the same priority, but only
     *                                  operators within a single set have specified
     *                                  in--set associativity, that is, mutual
     *                                  associativity of operators from two different
     *                                  sets is unspecified
     * @param ops                       operators within some sequence
     * @param opNames                   maps operators to their
     *                                  names, as to be used in an error
     *                                  message
     * @return                          if ok then null, otherwise
     *                                  an error message
     *
     */
    public static String checkUnspecifiedAssociativity(BinaryExpression.Op[][] sets,
            List<BinaryExpression.Op> ops,
            Map<BinaryExpression.Op, String> opNames) {
        // index of the grup to which belong the scanned ops, -1
        // for no scanned ops yet
        int group = -1;
        for(BinaryExpression.Op op : ops) {
            boolean foundInSets = false;
            OP_SEARCH:
            for(int index = 0; index < sets.length; ++index) {
                for(BinaryExpression.Op o : sets[index])
                    if(op.equals(o)) {
                        if(group != -1 && group != index)
                            return "unspecified associativity between " +
                                    opNames.get(op) + " and " + opNames.get(o);
                        group = index;
                        foundInSets = true;
                        break OP_SEARCH;
                    }
            }
            if(!foundInSets)
                throw new RuntimeException("operator not found in sets");
        }
        return null;
    }
    /**
     * Returns a new expression, that represents a variable.<br>
     *
     * The  method just wraps <code>PrimaryExpression.newVariableExpression</code>.
     *
     * Calling: optional.
     *
     * @param pos                       stream position
     * @param scope                     scope of the expression
     * @param variable                  variable
     */
    public static PrimaryExpression newVariableExpression(StreamPos pos, BlockScope scope,
            Variable variable) {
        return PrimaryExpression.newVariableExpression(pos,
                scope, variable);
    }
//    <code>ConstExpression</code> does that.
//    /**
//     * Returns a new expression, that represents a constant.<br>
//     *
//     * The  method just wraps <code>PrimaryExpression.newConstExpression</code>.
//     *
//     * Calling: optional.
//     *
//     * @param pos position in the parsed stream, can be null
//     * @param scope scope of the new expression
//     * @param constant literal representing the constant
//     */
//    public static PrimaryExpression newConstExpression(StreamPos pos, BlockScope scope,
//            Literal constant) {
//        return PrimaryExpression.newConstExpression(pos,
//                scope, constant);
//    }
    /**
     * Returns a new expression, that represents an element within an
     * array, possibly nested within other arrays.<br>
     *
     * Calling: optional.
     *
     * @param pos                       stream position, null for current
     * @param scope                     scope of the expression
     * @param variable                  array variable
     * @param indices                   subsequent indices of nested arrays,
     *                                  the last one is the most nested,
     *                                  if empty this method reduces to
     *                                  <code>newVariableExpression</code>
     * @param frontend                  frontend
     */
    public static PrimaryExpression newArrayElementExpression(StreamPos pos,
            BlockScope scope, Variable variable, List<Long> indices,
            AbstractFrontend frontend) {
        if(!variable.type.isArray(scope.getBoundingJavaClass().frontend.
                arrayClassName))
            throw new RuntimeException("variable must be an array");
        PrimaryExpression e = newVariableExpression(pos, scope, variable);
        for(long index : indices) {
            Literal literal = getIndexingConstant(index, frontend);
            PrimarySuffix suffix = new PrimarySuffix(PrimarySuffix.Type.INDEX,
                    new ConstantExpression(pos, scope, literal));
            e.suffixList.add(suffix);
        }
        return e;
    }
    /**
     * Returns an array indexing constant of the type having the numeric
     * precision of <code>frontend.maxArrayIndexPrecision</code>.
     * 
     * @param index                     value of the constant
     * @param frontend                  frontend
     * @return
     */
    public static Literal getIndexingConstant(long index, AbstractFrontend frontend) {
        Literal literal;
        switch(frontend.maxArrayIndexPrecision) {
            case Type.NumericPrecision.BYTE:
                literal = new Literal((byte)index);
                break;

            case Type.NumericPrecision.SHORT:
                literal = new Literal((short)index);
                break;

            case Type.NumericPrecision.INT:
                literal = new Literal((int)index);
                break;

            case Type.NumericPrecision.LONG:
                literal = new Literal((long)index);
                break;

            default:
                throw new RuntimeException("illegal numeric precision of index");
        }
        return literal;
    }
    /**
     * Returns a new assignment expression, that assigns a given expression
     * to a given variable. It just wraps the variable in an expression,
     * as required by <code>AssignmentsExpression</code>.<br>
     *
     * Calling: optional.
     * 
     * @param pos                       stream position
     * @param scope                     scope of the expression
     * @param variable                  variable to assign the expression
     *                                  to
     * @param expression                expression to be assigned to the
     *                                  variable
     * @return                          an assignment expression
     */
    public static AssignmentExpression newAssignment(StreamPos pos, BlockScope scope,
            Variable variable, AbstractExpression expression) {
/*if(variable.name.equals("protocolBit"))
    variable = variable;*/
        return new AssignmentExpression(pos, scope,
            newVariableExpression(pos, scope, variable),
            expression);
    }
    /**
     * Returns the default initializer literal for a given primitive type,
     * as specified in the Java language.<br>
     * 
     * Instead of directly using this method, in most cases it is simplier to
     * just call <code>newDefaultInitializerAssignment<code>, that returns a
     * respective assignment expression.<br>
     * 
     * Calling: optional.
     * 
     * @param type                      type of initialized variable
     * 
     * see #getDefaultInitializerAssignment
     */
    public static Literal newDefaultInitializerLiteral(Type type) {
        Literal literal = null;
        if(type.isJava())
            literal = new Literal();
        else
            switch(type.getPrimitive()) {
                case BOOLEAN:
                    literal = new Literal(false);
                    break;

                case CHAR:
                    literal = new Literal('\0');
                    break;

                case BYTE:
                    literal = new Literal((byte)0);
                    break;

                case SHORT:
                    literal = new Literal((short)0);
                    break;

                case INT:
                    literal = new Literal(0);
                    break;

                case LONG:
                    literal = new Literal(0L);
                    break;

                case FLOAT:
                    literal = new Literal(0.0f);
                    break;

                case DOUBLE:
                    literal = new Literal(0.0);
                    break;

                default:
                    throw new RuntimeException("unknown primitive type: " + type.getPrimitive());
            }
        return literal;
    }
    /**
     * Returns an assignment expression, that assigns a default initializer
     * to a given variable.<br>
     * 
     * If blank finals are not allowed, this method will throw a runtime
     * exception if <code>variable</code> is final.
     *
     * Calling: optional.
     * 
     * @param pos                       position in the parsed stream
     * @param variable                  variable to assign the default
     *                                  initializer
     * @param initializerScope          scope of the initializer method
     * @return                          an initializer assignment expression
     */
    public AssignmentExpression newDefaultInitializerAssignment(
            StreamPos pos, Variable variable, BlockScope initializerScope) {
        if(!(initializerScope.getBoundingVariableOwner() instanceof  Method))
            throw new RuntimeException("initializer scope not within a method");
        if(!compilation.options.blankFinalsAllowed && variable.flags.isFinal())
            throw new RuntimeException("blank finals not allowed");
        AssignmentExpression a = newAssignment(pos, initializerScope, variable,
            new ConstantExpression(pos, initializerScope,
                newDefaultInitializerLiteral(variable.getResultType())));
        a.acceptFinal = true;
        /* a.defaultInitializer = true; */
        return a;
    }
    /**
     * Returns a comment's text. The text must either not be prefixed
     * with `/*' or '//', in what case null is returned, or be
     * a comment with a valid grammar. If the grammar is invalid, a runtime
     * exception is throw, as comments with invalid grammar should
     * be detected while parsing them.<br>
     * 
     * @param text text
     * @param textPos if not null then position of the text, updated
     * with the position of the returned string
     * @return comment's text, without the prefix and a possible suffix
     */
    public static String getCommentText(String text, StreamPos textPos) {
        boolean starComment = false;
        boolean slashComment = false;
        if(text.length() >= 2) {
            String prefix = text.substring(0, 2);
            starComment = prefix.equals("/*");
            slashComment = prefix.equals("//");
            boolean cut = starComment || slashComment;
            if(cut) {
                if(text.length() >= 3) {
                    prefix = text.substring(0, 3);
                    if(prefix.equals("/**")) {
                        text = text.substring(3);
                        if(textPos != null)
                            textPos.column += 3;
                        cut = false;
                    }
                }
                if(cut) {
                    if(textPos != null)
                        textPos.column += 2;
                    text = text.substring(2);
                }
            }
        }
        if(!starComment && !slashComment)
            text = null;
        else {
            if(text.length() >= 2 && text.substring(text.length() - 2).equals("*/")) {
                if(slashComment)
                    throw new RuntimeException("invalid comment grammar");
                text = text.substring(0, text.length() - 2);
            } else if(starComment)
                throw new RuntimeException("invalid comment grammar");
        }
        return text;
    }
    /**
     * Finds tags in a text of some comment. The comment's boundary
     * tokens like "/*" must be present in the text.<br>
     *
     * If empty tag names are not allowed, tags with empty names
     * cause a parse exception to be thrown. Yet, range tags, which have
     * empty names too, can possibly be classified as
     * stray tags. This in turn might cause two errors for one tag: one for
     * missing name, and the second for stray range. To avoid that, every
     * range that causes a missing name parse exception to be thrown
     * by this method, is also registered as a fake range tag, with
     * position modified as required by range tags.<br>
     * 
     * Any tag whose name is not followed by `(' or ')' is qualified
     * as a non--Hedgeelleth tag and ignored.
     *
     *
     * @param text                      text to search for tags
     * @param textPos                   position of the text
     * @param emptyTagNameAllowed       if it is allowed for the tag to have
     *                                  an empty name
     * @param primaryRanges             true to accept primary ranges, false
     *                                  to accept variable ranges
     * @return                          tags in the text
     *
     */
    public List<CommentTag> findCommentTagsInString(String text,
            StreamPos textPos, boolean emptyTagNameAllowed,
            boolean primaryRanges) throws ParseException {
        List<CommentTag> tags = new LinkedList<>();
        text = getCommentText(text, textPos);
        if (text != null) {
            String tagText;
            int pos = 0;
            NEXT_TAG:
            while (pos < text.length() && (pos = text.indexOf("@", pos)) != -1) {
                // count raw text's position
                StreamPos tagTextPos = new StreamPos(textPos);
                tagTextPos.advancePos(pos, text);
                int parenthesesIndex = text.indexOf("(", pos);
                StreamPos parenthesesPos;
                if(parenthesesIndex != -1) {
                    parenthesesPos = new StreamPos(textPos);
                    parenthesesPos.advancePos(parenthesesIndex, text);
                } else
                    parenthesesPos = null;
                int rawTextBegin = pos;
                int rawTextEnd = -1;
                String tagName = "";
                char c = 0;
                // skip the "@"
                ++pos;
                // skip whitespace
                while (pos < text.length() && Character.isWhitespace(text.charAt(pos))) {
                    ++pos;
                }
                int insideParentheses = 0;
                boolean tagEndFound = false;
                boolean afterName = false;
                while (!tagEndFound && pos < text.length()) {
                    c = text.charAt(pos);
                    if(Character.isWhitespace(c) && insideParentheses == 0)
                        // name of the tag ended
                        afterName = true;
                    if (c == '(') {
                        afterName = false;
                        ++insideParentheses;
                    } else if (c == ')') {
                        if (insideParentheses == 0) {
                            throw new ParseException(tagTextPos,
                                    ParseException.Code.PARSE,
                                    "comment tag " + tagName + ": unexpected `)'");
                        }
                        --insideParentheses;
                        if(insideParentheses == 0) {
                            tagEndFound = true;
                            rawTextEnd = pos;
                        }
                    } else if(!Character.isWhitespace(c) && afterName) {
                        // non--Hedgeelleth tag, ignore
                        continue NEXT_TAG;
                    }
                    tagName = tagName + c;
                    ++pos;
                }
                if(insideParentheses != 0) {
                    throw new ParseException(tagTextPos,
                            ParseException.Code.PARSE,
                            "comment tag " + tagName + ": `)' expected after modifiers");
                }
                if(!tagEndFound) {
                    // non--Hedgeelleth tag, ignore
                    continue NEXT_TAG;
                    /*
                    throw new ParseException(tagTextPos,
                            ParseException.Code.PARSE,
                            "comment tag " + tagName + ": missing `('");
                     */
                }
                tagText = text.substring(rawTextBegin, rawTextEnd + 1);
                List<String> modifierList = new LinkedList<>();
                int cb = tagName.indexOf('(');
                String modifiers = tagName.substring(cb + 1, tagName.length() - 1);
                tagName = tagName.substring(0, cb).trim();
                if(tagName.isEmpty() && !emptyTagNameAllowed) {
                    PrimitiveRangeDescription d = new PrimitiveRangeDescription();
                    d.textPos = parenthesesPos;
                    if(primaryRanges)
                        d.text = PrimitiveRangeDescription.PRIMARY_LEFT_STRING + modifiers +
                                PrimitiveRangeDescription.PRIMARY_RIGHT_STRING;
                    else
                        d.text = PrimitiveRangeDescription.VARIABLE_LEFT_STRING + modifiers +
                                PrimitiveRangeDescription.VARIABLE_RIGHT_STRING;
                    reportPrimitiveRangeTag(d);
                    throw new ParseException(tagTextPos,
                            ParseException.Code.PARSE,
                            "tag name missing");
                }
                int i = 0;
                while (i < modifiers.length()) {
                    int j = modifiers.indexOf(',', i);
                    if (j == -1) {
                        j = modifiers.length();
                    }
                    modifierList.add(modifiers.substring(i, j).trim());
                    i = j + 1;
                }
                CommentTag cm = new CommentTag(tagName,
                        tagText, tagTextPos);
//if(cm.rawText.contains("getNum"))
//    cm = cm;
                for (String m : modifierList) {
                    try {
                        cm.addModifier(m);
                    } catch (ParseException e) {
                        throw new ParseException(tagTextPos,
                                ParseException.Code.PARSE,
                                "can not add a modifier: " + e.toString());
                    }
                }
                tags.add(cm);
            }
        }
        return tags;
    }
    /**
     * Finds a group of special tokens directly before some token.<br>
     * 
     * @param t                         a token before which the special tokens
     *                                  are searched for
     * @param positions                 if not null, respective positions
     *                                  of the tokens found are appended
     *                                  to this list
     * @return                          strings of the subsequent tokens, empty
     *                                  list if no special tokens found
     */
    List<String> findSpecialTokens(Token t, List<StreamPos> positions) {
        List<String> out = new LinkedList<>();
        if (t.specialToken != null) {
            Token tmp_t = t.specialToken;
            while (tmp_t.specialToken != null) {
                tmp_t = tmp_t.specialToken;
            }
            while (tmp_t != null) {
                if (positions != null) {
                    StreamPos tokenPos = new StreamPos(
                            parser.getStreamName(), tmp_t);
                    positions.add(tokenPos);
                }
                String s = tmp_t.image;
                out.add(s);
                tmp_t = tmp_t.next;
            }
        }
        return out;
    }
    /**
     * Finds a group of comments directly before some token.
     * 
     * @param t token
     * @param retainDecoration if to retain the prefix
     * and a possible suffix
     * @return a comment with prefixes and an optional suffix, null
     * if no comment found
     */
    public List<String> findComments(Token t, boolean retainDecoration) {
        List<String> out = new LinkedList<>();
        List<String> specialTokens = findSpecialTokens(t, null);
        for(String text : specialTokens) {
            String trimmed = getCommentText(text, null);
            if(trimmed != null) {
                if(!retainDecoration)
                    text = trimmed;
                out.add(text);
            }
        }
        return out;
    }
    /**
     * Retrieves a special ** comment directly before some token. If such
     * a comment is not found, null is returned.
     * 
     * @param t token
     * @return comment contents including decoration, or null
     */
    public String retrieveSpecialComment(Token t) {
        String comment = null;
        List<String> c = findComments(t, true);
        if(!c.isEmpty()) {
            String last = c.get(c.size() - 1);
            if(last.length() >= 3 && last.substring(0, 3).equals("/**"))
                comment = last;
        }
        return comment;
    }
    /**
     * Finds tags in a direct group of comments before some token.<br>
     *
     * In particular, can be used to find primitive range tags.<br>
     *
     * This implementation supports only /*, /** and // comments.<br>
     *
     * Can also be used to register usage of some named tags. Unnamed tags
     * are typically range tags, and their usage is registered separately,
     * by <code>findPrimitiveRangeTag()</code>.
     *
     * Calling: optional.
     *
     * @param t                         token before which the tag is searched
     * @param emptyTagNameAllowed       if it is allowed for the tag to have
     *                                  an empty name; normally range tags have
     *                                  empty name
     * @param namesToRegister           tags with names within the array will be
     *                                  registered to <code>nctUsage</code>;
     *                                  null or empty array for no registration
     *                                  of any tag
     * @param primaryRanges             true to accept primary ranges, false
     *                                  to acceps variable ranges
     * @return list of tags, empty for no tags, initial @'s are skipped
     */
    public List<CommentTag> findCommentTags(Token t,
            boolean emptyTagNameAllowed, String[] namesToRegister,
            boolean primaryRanges) throws ParseException {
        List<StreamPos> positions = new LinkedList<>();
        List<String> specialTokens = findSpecialTokens(t, positions);
        List<CommentTag> tags = new LinkedList<>();
        Iterator<StreamPos> positionsIterator = positions.iterator();
        for (String tokenString : specialTokens) {
            StreamPos tokenPos = positionsIterator.next();
            tags = findCommentTagsInString(tokenString, tokenPos,
                    emptyTagNameAllowed, primaryRanges);
            if(namesToRegister != null) {
                for(CommentTag ct : tags) {
if(ct.name.indexOf("Igno") != -1)
    ct = ct;
                    for(int i = 0; i < namesToRegister.length; ++i) {
                        if(ct.name.equals(namesToRegister[i])) {
                            reportNamedCommentTag(ct);
                            break;
                        }
                    }
                }
            }
        }
        return tags;
    }
    /**
     * In a sequence of comments tags, searches for a range tag.
     * Only at most a single range tag is allowed in the sequence,
     * Otherwise, a parse exception is thrown.<br>
     *
     * The sequence of tags is normally acquired using the method
     * <code>findCommentTags</code>.<br>
     *
     * The returned range string, like in the other method
     * <code>findPrimitiveRangeTags</code>, is formatted to be
     * enclosed by tokens that depend on whether it is a variable or
     * a primary range.<br>
     *
     * Any range tag, if <code>reportUsage</code> is true,
     * is registered using <code>reportPrimitiveRangeTag</code>,
     * even if the tag is invalid. This way, invalid tags cause only
     * parse errors, and are not later classified as stray ones by
     * <code>findStrayPrimitiveRangeTags</code>, what would cause two
     * error messages about a single tag.<br>
     *
     * An invalid range tag is a tag without a name, that is not
     * a valid range tag.
     *
     * Calling: optional.
     *
     * @param tags                      list of comment tags
     * @param allowNonRangeTags         true if to allow non--range
     *                                  tags, these tags are ignored
     *                                  by this method; if false and
     *                                  a non--range tag is found, a
     *                                  parse exception is thrown;
     *                                  typically false if
     *                                  there is a separated mechanism
     *                                  employed for detecting stray named
     *                                  tags, like the usage of the parameter
     *                                  <code>namesToRegister</code> in the method
     *                                  <code>findCommentTags<code>
     * @param rangePos                  stream position,
     *                                  to be filled by the range
     *                                  tag's position
     * @param reportUsage               if to report usage, see this
     *                                  method docs for details
     * @param primary                   true for a primary and false for
     *                                  a variable range
     * @return                          random tag, or null if not
     *                                  found
     * @throws ParseException
     */
    public String findPrimitiveRangeTag(List<CommentTag> tags,
            boolean allowNonRangeTags, StreamPos rangePos,
            boolean reportUsage, boolean primary) throws ParseException {
        ParseException errors = new ParseException();
        String range = null;
        int count = 0;
        if(!tags.isEmpty())
            for(int index = 0; index < tags.size(); ++index) {
                CommentTag tag = tags.get(index);
                if(!tag.name.isEmpty())
                    if(allowNonRangeTags)
                        continue;
                    else
                        throw new ParseException(tag.getStreamPos(),
                                ParseException.Code.PARSE,
                                "only a range tag allowed here");
                String currRange = tag.rawText;
                StreamPos currRangePos = new StreamPos();
                currRangePos.set(tag.getStreamPos());
                int offset = currRange.indexOf("(");
                currRangePos.advancePos(offset, currRange);
                currRange = currRange.substring(offset);
                String s;
                if(primary)
                    s = PrimitiveRangeDescription.PRIMARY_LEFT_STRING;
                else
                    s = PrimitiveRangeDescription.VARIABLE_LEFT_STRING;
                s += currRange.substring(1, currRange.length() - 1);
                if(primary)
                    s += PrimitiveRangeDescription.PRIMARY_RIGHT_STRING;
                else
                    s += PrimitiveRangeDescription.VARIABLE_RIGHT_STRING;
                PrimitiveRangeDescription d = new PrimitiveRangeDescription();
                d.textPos = currRangePos;
                d.text = s;
                // report even range tags that cause parse errors,
                // to avoid double invalid tag/stray tag eror reports
                if(reportUsage)
                    reportPrimitiveRangeTag(d);
                boolean valid = true;
                ++count;
                if (count > 1) {
                    errors.addReport(new Report(tag.getStreamPos(),
                            ParseException.Code.PARSE,
                            "only a single range tag allowed here"));
                    valid = false;
                }
                if(tag.modifiers.isEmpty()) {
                    errors.addReport(new Report(tag.getStreamPos(),
                            ParseException.Code.PARSE,
                            "missing range in a range tag"));
                    valid = false;
                }
                if(tag.modifiers.size() != 2) {
                    errors.addReport(new Report(tag.getStreamPos(),
                            ParseException.Code.PARSE,
                            "range in a range tag requires two comma-separated expressions"));
                    valid = false;
                }
                if(valid) {
                    // this range tag is valid, save
                    rangePos.set(currRangePos);
                    range = s;
                }
            }
        return range;
    }
    /**
     * Returns tags in a comment before some token. Checks
     * for Hedgeelleth's skip rest tag.
     * 
     * @param token a token after the comment
     * @param namesToRegister register tags with these names,
     * null for no such tags; <code>SKIP_REST_TAG_STRING</code>
     * can not be included, as it is registered anyway
     * @return list of tags, or null if the tag
     * <code>SKIP_REST_TAG_STRING</code> has been encuntered
     */
    public List<CommentTag> getTagsSkipRest(Token token,
            String[] namesToRegister) throws ParseException {
        List<CommentTag> tags = null;
        String[] allowed;
        if(namesToRegister == null)
            allowed = new String[] { SKIP_REST_TAG_STRING, };
        else {
            allowed = new String[namesToRegister.length + 1];
            System.arraycopy(namesToRegister, 0, allowed, 0,
                    namesToRegister.length);
            allowed[namesToRegister.length] = SKIP_REST_TAG_STRING;
        }
        tags = findCommentTags(token, false,
            allowed, false);
        for(CommentTag ct : tags)
            if(ct.name.equals(SKIP_REST_TAG_STRING)) {
                return null;
            }
        return tags;
    }
    /**
     * In the current stream, searches for a range modifier.<br>
     * 
     * It is assumed that the range is enclosed in tokens
     * <code>(&lt;</code> and <code>&gt;)</code>, and the
     * just consumed token is <code>(&lt;</code>.<br>
     *
     * Calling: optional.
     *
     * @param tokenImage                field <code>tokenImage</code>
     *                                  in the parser's constants class
     * @param rangePos                  stream position,
     *                                  to be filled by the range
     *                                  tag's position
     * @param primary                   if aprimary range; determines th
     *                                  image if the token, that ends the
     *                                  range, either ">>" for a variable range
     *                                  and ">>" for a primary range // formerly ">)"
     * @return                          random tag, or nul if not
     *                                  found
     * @throws ParseException
     */
    public String findPrimitiveRangeModifier(String[] tokenImage,
            StreamPos rangePos, boolean primary) throws ParseException {
        String range;
        String last;
        if(primary)
            last = ">>"; // formerly ">)"
        else
            last = ">>";
        String[] after = {
            "\"" + last + "\"",};
        String[] before = {
            "\"}\"",
            "\"{\"",};
        List<Token> tokens = skipTokens(
                tokenImage,
                after, before, 1);
        if (tokens.size() < 2) {
            throw new ParseException(parser.getStreamPos(),
                    ParseException.Code.PARSE,
                    "invalid range definition");
        }
// if(tokens.size() >= 4 && tokens.get(3).image.contains("false"))
//     tokens = tokens;
        rangePos.set(new StreamPos(parser.getStreamName(),
                tokens.get(0).beginLine,
                tokens.get(0).beginColumn));
        tokens.add(parser.getCurrentToken());
        range = StreamPos.reconstructStream(rangePos,
                tokens);
        return range;
    }
    /**
     * Reports named comment tags. See the field <code>nctUsage</code>
     * for details.<br>
     *
     * A comment tag without a name will cause a runtime exception to
     * be throw by this method.
     *
     * @param ct                        a named comment tag
     */
    public void reportNamedCommentTag(CommentTag ct) {
        if(ct.name.isEmpty())
            throw new RuntimeException("only named comment tags can be reported " +
                    "using this method");
        nctUsage.add(ct);
    }
    /**
     * Adds a description of primitive range, so that it is not classified
     * as a stray one by <code>findStrayPrimitiveRanges</code>. Used only
     * for ranges declared as comment tags.<br>
     *
     * This method is called automatically by <code>findPrimitiveRangeTag</code>,
     * see that method docs for details.<br>
     *
     * @param description               description to add
     */
    public void reportPrimitiveRangeTag(PrimitiveRangeDescription description) {
/*if(description.textPos.toString().indexOf("4:11") != -1)
    prUsage = prUsage;*/
        prUsage.add(description);
    }
    /**
     * Searched for model tags, processes them and registers.<br>
     * 
     * Call of this method makes <code>Compilation.modelControl</code>
     * applicable, if it is not yet one.
     * 
     * @param ct list of model tags
     */
    public void analyzeModelTags(List<CommentTag> ct) throws ParseException {
        if(compilation.modelControl == null)
            compilation.modelControl = new ModelControl();
        for(CommentTag t : ct) {
            String contents = t.getArguments();
            if(t.name.equals("modelAppend")) {
                compilation.modelControl.appendVerbatim(contents);
            } else
                throw new RuntimeException("unknown model tag");
        }
    }
    /**
     * Finds all comment tags, or optionally, only
     * primitive ranges, declared in <code>commentTokens</code>,
     * that have not been reported using
     * <code>reportNamedCommentTag</code> or
     * <code>reportPrimitiveRange</code>.
     * Such tags are usually either stray or have invalid syntax, thus,
     * are not effectively used.<br>
     *
     * In the case of range modifiers, used only for ranges declared as
     * comment tags. This is because
     * range modifiers are normal part of parser grammar, thus no need
     * for a separate detection of stray range modifiers.<br>
     *
     * Any not reported tags are returned as error reports in a common parse
     * exception. If no such tags have been found, no exception is
     * thrown.<br>
     *
     * The list of all comment tokens is typically obtained using Hedgeelleth's
     * <code>collect_comment_tokens</code> keyword, which makes the
     * lexer to collect the tags.<br>
     *
     * After <code>findPrimitiveRangeTag()</code>, only at most a
     * single range tag is allowed in a comment token.
     *
     * @param commentTokens             all comment tokens in the exact source
     *                                  stream, for which
     *                                  <code>reportPrimitiveRangeTag</code>
     *                                  was used
     * @param onlyRangeTags             if only range tags should be screened;
     *                                  if false, all comment tags are screened
     */
    public void findStrayCommentTags(List<Token> commentTokens,
            boolean onlyRangeTags)
            throws ParseException {
        ParseException stray = new ParseException();
        // value does not matter: presence is checked, enclosing tokens
        // are discarded
        boolean PRIMARY = false;
        for(Token t : commentTokens) {
            List<CommentTag> tags = findCommentTagsInString(
                    t.image, new StreamPos(parser.getStreamName(), t),
                    true,
                    PRIMARY);
            if(!onlyRangeTags) {
                // search for all tags first
                for(CommentTag ct : tags)
                    if(!ct.name.isEmpty()) {
                        boolean used = false;
                        for(CommentTag registered : nctUsage) {
                            if(ct.getStreamPos().equals(registered.getStreamPos())) {
                                used = true;
                                break;
                            }
                        }
                        if(!used)
                            stray.addReport(new Report(ct.getStreamPos(), ParseException.Code.ILLEGAL,
                                    "stray comment tag"));
                    }
            }
            StreamPos textPos = new StreamPos();
            String text;
            try {
                text = findPrimitiveRangeTag(
                    tags, true, textPos, false, PRIMARY);
            } catch(ParseException e) {
                stray.addAllReports(e);
                text = null;
            }
            if(text != null) {
                boolean used = false;
                for(PrimitiveRangeDescription d : prUsage) {
                    if(textPos.equals(d.textPos)) {
                        used = true;
                        // discard possibly conflicting enclosing tokens
                        final int LENGTH = 2;
                        if(!text.substring(LENGTH, text.length() - LENGTH*2).equals(
                                d.text.substring(LENGTH, d.text.length() - LENGTH*2)))
                            throw new RuntimeException("different text for the same range tag");
                        break;
                    }
                }
                if(!used)
                    stray.addReport(new Report(textPos, ParseException.Code.ILLEGAL,
                            "stray range tag"));
            }
        }
        if(stray.reportsExist())
            throw stray;
    }
    /**
     * Creates a new compilation unit. Called in the generated constructor
     * of the parser.<br>
     *
     * A compilation unit can have several classes, but must belong to a
     * single package and have a single frontend. More compilation units
     * can have a common package.<br>
     * 
     * Calling: obligatory, once per compilation unit. The call is
     * already provided in the generated code.
     *
     * @param frontend                  frontend of the new compilation
     *                                  unit
     * @param defaultImports            default packages, that should
     *                                  exist within each unit,
     *                                  null for none
     */
    public void setCompilationUnit(AbstractFrontend frontend,
            String[] defaultImports) {
        unit = new Unit(frontend, compilation.packages, defaultImports);
    }
    /**
     * Called after package name is known. Should be called after
     * <code>compilationUnitHead</code>.<br>
     * 
     * Calling: obligatory, once per compilation unit.
     * 
     * @param packageName               package name or null for default
     *                                  package
     */
    public void compilationUnitPackage(NameList packageName) {
        if(packageName != null)
            unit.packageName = packageName;
        else
            // defaults to the default package
            unit.packageName = new NameList(DEFAULT_PACKAGE);
        // find a scope of this package, or create a new one if not found
        TopScope packages = compilation.packages;
        String packageString = unit.packageName.toString();
        PackageScope s = packages.packageScopes.get(packageString);
        if(s == null)
            s = new PackageScope(packages, packageString);
        unit.packageScope = s;
    }
    /**
     * Called after each import declaration. Should be called in a series,
     * after <code>compilationUnitPackage</code>.<br>
     * 
     * Calling: obligatory, once per import declaration.
     * 
     * @param imporT import to add
     */
    public void compilationUnitImport(Import imporT) {
        if(imporT.statiC)
            unit.staticImportDeclarations.add(imporT);
        else
            unit.importDeclarations.add(imporT);
    }
    /**
     * Performs operations required after each unit is parsed.<br>
     * 
     * Stores a unit's imports into <code>Compilation.imports</code>.<br>
     * 
     * Calling: obligatory, after a unit is successfully parsed.
     */
    public void compilationUnitEnd() {
        compilation.imports.addAll(unit.importDeclarations);
        compilation.imports.addAll(unit.staticImportDeclarations);
    }
    /**
     * Sets an unique identifier of the class. As opposed to
     * <code>compilationUnitJavaClass</code>, this method should be called
     * before any code within the class is parsed, as the code
     * might need it.<br>
     * 
     * Normally, calling this method is not needed, unless experiments
     * are allowed. Then, the identifier is used by
     * <code>IntegerSetExpression</code> amd should be fully qualified
     * class' name.<br>
     * 
     * Calling: optional, once per java class, before any code of the class
     * is parsed.
     * 
     * @param identifier                an unique identifier of the class,
     *                                  usually class' qualified name
     */
    public void setClassUniqueIdentifier(String identifier) {
        classUniqueIdentifier = identifier;
    }
    /**
     * Returns unique identifier of a class, as described in
     * <code>setClassUniqueIdentifier</code>. Throws a runtime
     * exception if the identifier has not been set.
     *
     * @return                          an unique identifier of the class,
     */
    public String getClassUniqueIdentifier() {
        if(classUniqueIdentifier == null)
            throw new RuntimeException("class identifier not set");
        return classUniqueIdentifier;
    }
    /**
     * Registers a java class after the type is completely parsed.
     * The import declarations registered with <code>compilationUnitImport</code>
     * should already be known before parsing any java class.<br>
     * 
     * Calling: obligatory, once per java class, after the type is parsed.
     *
     * @param javaClass                  java class to add to the current unit
     */
    public void compilationUnitJavaClass(AbstractJavaClass javaClass) throws ParseException {
        unit.packageScope.addJavaClass(javaClass);
    }
    /**
     * If the constructor code does not begin with constructor
     * invocation, and the constructor's class has a superclass,
     * a default super() invocation is added just after the
     * argument range code.<br>
     *
     * Then, regardless of whether the invocation has been added,
     * if therer is any ivocation to super, a call to
     * <code>*object</code> of the constructor's
     * class is added just after the constructor invocation,
     * to initialize fields.<br>
     *
     * It is only a symbolic invocation, that must be later
     * resolved during semantic check.
     *
     * @param s                         constructor, before semantic
     *                                  check
     * @param block block to prepend with the completing code;
     * this is to give precedence to the argument range initialization 
     * code
     */
    public static void completeConstructorCode(Constructor c) throws ParseException {
        Block block = c.code;
        // owner of a constructor must be a class
        ImplementingClass clazz = (ImplementingClass)c.owner;
        // if true, call to object* is not added afterwards
        boolean callToThis = false;
        if(clazz.extendS != null) {
            CallExpression fc;
            if(block.code.size() == 0 ||
                    !(block.code.get(0) instanceof CallExpression))
                fc = null;
            else
                fc = (CallExpression)block.code.get(0);
            if(fc == null ||
                        (fc.method != Hedgeelleth.CONSTRUCTOR_SUPER &&
                        fc.method != Hedgeelleth.CONSTRUCTOR_THIS)) {
                // no explicit constructor invocation, but a superclass exists,
                // so add an implicit invocation super()
                CallExpression constructorCall = new CallExpression(
                        block.getStreamPos(), block.scope,
                        Hedgeelleth.CONSTRUCTOR_SUPER,
                        new LinkedList<AbstractExpression>());
                // calls to constructors are always direct
                constructorCall.directNonStatic = true;
                block.code.add(c.argRangeCodeSize, constructorCall);
            } else if(fc.method == Hedgeelleth.CONSTRUCTOR_THIS) {
                callToThis = true;
            }
        }
        if(!callToThis) {
            CallExpression objectCall = new CallExpression(
                    block.getStreamPos(), block.scope,
                    getInitMethod(clazz.scope, Context.NON_STATIC),
                    new LinkedList<AbstractExpression>());
            // calls to init methods are always direct
            objectCall.directNonStatic = true;
            int index;
            if(clazz.extendS != null)
                // after constructor invocation
                index = 1;
            else
                // no constructor invocation, so
                // at the beginning of the code
                index = 0;
            block.code.add(c.argRangeCodeSize + index, objectCall);
        }
    }
    /**
     * Adds the default constructor to a class.<br>
     *
     * Usually used if the class does not have any constructors.
     *
     * @param clazz                     class
     * @param am                        access modifier of the default
     *                                  constructor
     */
    public static void addDefaultConstructor(ImplementingClass clazz,
            AccessModifier am) throws ParseException {
        MethodFlags nonStaticFlags = new MethodFlags();
        nonStaticFlags.am = am;
        StreamPos pos = new StreamPos(clazz.getStreamPos());
        Constructor s = new Constructor(pos,
                clazz, clazz.name, null, nonStaticFlags);
        s.code = new Block(pos, s.scope);
        completeConstructorCode(s);
        clazz.scope.addMethod(s);
    }
    /**
     * If a class does not have any constructor, a default constructor
     * with no arguments is added to the class.<br>
     * 
     * Calling: optional, once per java class, after the type is parsed.
     * 
     * @param clazz                     class to which the default constructor
     *                                  is possibly added
     * @param am                        access modifier of the default
     *                                  constructor
     */
    public static void completeConstructors(ImplementingClass clazz, AccessModifier am)
            throws ParseException {
        // check if there is at least one constructor
        boolean found = false;
        Collection<Method> methods = clazz.scope.methods.values();
        for(Method m : methods) {
            if(m.name.equals(clazz.name)) {
                found = true;
                break;
            }
        }
        if(!found)
            // no constructor found, add a default one
            addDefaultConstructor(clazz, am);
    }
    /**
     * Returns the method with initial assignments of fields.<br>
     * 
     * There are two such methods per java class: one for static and one for
     * non--static fields. The former is typically run once per class loading,
     * the latter once per object instantation, but it is the particular
     * backend that decides about is.<br>
     * 
     * The method is usually needed to add either field initializations
     * or static blocks to it.<br>
     * 
     * Calling: optional.
     * 
     * @param scope                     a java class scope
     * @param statiC                    which method to return --
     *                                  the one that initializes static
     *                                  fields or the one that initializes
     *                                  non--static fields
     */
    public static Method getInitMethod(JavaClassScope scope, Context context) throws ParseException {
        String initMethodName;
        if(context != Context.NON_STATIC)
            initMethodName = AbstractJavaClass.INIT_STATIC;
        else
            initMethodName = AbstractJavaClass.INIT_OBJECT;
        return scope.lookupMethodAT(
            new MethodSignature(((AbstractJavaClass)scope.owner).frontend,
            initMethodName, null),
            PrimaryExpression.ScopeMode.OMIT_STATIC);
    }
    /**
     * Tests, if a method has one of the annotations, for whose
     * CompilerAnnotation.uniqueMethod() returns true. If yes,
     * that method is added to <code>specialMethods</code>.<br>
     * 
     * Throws a parse exception if an unknown compiler annotation has
     * been found.
     * 
     * Calling: obligatory, per each method parsed.
     * 
     * @param m method to check
     */
    public void checkSpecialMethod(Method m) throws ParseException {
        for(String s : m.annotations) {
            CompilerAnnotation a = CompilerAnnotation.parse(s);
            if(a == CompilerAnnotation.UNKNOWN_COMPILER_ANNOTATION)
                throw new ParseException(m.getStreamPos(),
                        CompilerException.Code.UNKNOWN,
                        "unknown compiler annotation @" + s);
            if(a != CompilerAnnotation.UNKNOWN_ANNOTATION &&
                    a.uniqueMethod()) {
                Map<CompilerAnnotation, Method> map =
                        compilation.specialMethods.get(getUnit().frontend);
                if(map == null) {
                    map = new HashMap<>();
                    compilation.specialMethods.put(getUnit().frontend, map);
                }
                if(map.containsKey(a))
                    throw new ParseException(m.getStreamPos(),
                            CompilerException.Code.DUPLICATE,
                            "duplicate unique compiler annotation @" + s +
                            ", previous occurence at " + map.get(a).getStreamPos());
                map.put(a, m);
            }
        }
    }
    /**
     * Creates a synchronization block. Because it is customary that code
     * within some block needs the enveloping block scope, the code is
     * not created yet when calling this method. Thus, the code must be inserted
     * later, for example by using <code>putToSynchronizedBlock</code>.<br>
     * 
     * The created synchronization block has only one statement of the class
     * <code>SynchronizedStatement</code>.
     *
     * Calling: optional.
     * 
     * @param pos                       stream position, null for current
     * @param scope                     scope of the block
     * @param lock                      lock variable
     * @return                          a synchronized block
     */
    public Block newSynchronizedBlock(StreamPos pos, AbstractLocalOwnerScope scope,
            Variable lock) {
        Block synchronizationBlock = new Block(getStreamPos(pos), scope);
        AbstractExpression lockExpression = PrimaryExpression.
            newVariableExpression(getStreamPos(pos),
            synchronizationBlock.scope, lock);
        SynchronizedStatement synchronization = new SynchronizedStatement(
                getStreamPos(pos), synchronizationBlock.scope,
                lockExpression);
        synchronizationBlock.code.add(synchronization);
        return synchronizationBlock;
    }
    /**
     * Puts a block into the synchronized block. Only one such block can
     * be put. The block must be crated with the synchronized block scope
     * as the parent scope.<br>
     * 
     * The synchronization block must have only one statement of the class
     * <code>SynchronizedStatement</code>. The method
     * <code>newSynchronizedBlock</code> can be used to create such a
     * synchronization block.
     *
     * Calling: optional.
     * 
     * @param synchronizedBlock         synchronized block to insert another
     * @param enveloped                 the inserted block
     */
    public static void putToSynchronizedBlock(Block synchronizedBlock, Block enveloped) {
        SynchronizedStatement synchronization = (SynchronizedStatement)
                synchronizedBlock.code.get(0);
        synchronization.block = enveloped;
    }
    /**
     * Adss a derefernce with a method to an expression. The expression should represent
     * an object in order to get a semantically correct output, but this method will blindly work
     * on anything.
     * 
     * @param object an expression possibly representing an object
     * @param method name of a method within the presumed object
     * @return the input expression parenthesized and then dereferenced
     */
    private static AbstractExpression dereferenceWithMethod(AbstractExpression object,
            String method) {
        LinkedList<PrimarySuffix> suffix = new LinkedList<>();
        suffix.add(new PrimarySuffix(object.getStreamPos(), new NameList(method)));
        suffix.add(new PrimarySuffix(object.getStreamPos(), new LinkedList<>()));
        object = new PrimaryExpression(object.getStreamPos(), (BlockScope)object.outerScope,
                PrimaryExpression.ScopeMode.OMIT_STATIC, new PrimaryPrefix(object.getStreamPos(),
                        PrimaryPrefix.PrefixType.PARENTHESES, object), suffix);
        return object;
    }
    /**
     * Returns an assignment of a literal to a primary expression.
     * 
     * @param lvalue lvalue
     * @param literal constant
     * @return an assignment
     */
    public static AbstractStatement getConstAssignment(PrimaryExpression lvalue, Literal literal) {
        return new AssignmentExpression(lvalue.getStreamPos(),
            (BlockScope)lvalue.outerScope, lvalue, new ConstantExpression(
                lvalue.getStreamPos(), (BlockScope)lvalue.outerScope,
                literal));
    }
    /**
     * Returns an expression of a non--deterministic boolean value.
     * 
     * @param scope scope
     * @param probabilisticMethod if a probabilistic variant, then qualified name of a static method
     * retruning a non--deterministic boolean value; otherwise null
     * @return a primary expression
     */
    public static PrimaryExpression getUnknown(StreamPos pos, BlockScope scope,
            String probabilisticMethod) {
        PrimaryPrefix prefix = new PrimaryPrefix(pos,
              new NameList(probabilisticMethod));
        prefix.textBeg = pos;
        PrimarySuffix postfix = new PrimarySuffix(pos, new LinkedList());
        List<PrimarySuffix> postfixList = new LinkedList<>();
        postfixList.add(postfix);
        return new PrimaryExpression(pos, (BlockScope)scope,
                PrimaryExpression.ScopeMode.OMIT_STATIC, prefix, postfixList);
    }
    /**
     * Returns an assignment of a non--deterministic boolean value to a primary expression.
     * 
     * @param lvalue lvalue
     * @param probabilisticMethod if a probabilistic variant, then qualified name of a static method
     * retruning a non--deterministic boolean value; otherwise null
     * @return an assignment
     */
    public static AbstractStatement getUnknownAssignment(PrimaryExpression lvalue,
            String probabilisticMethod) {
        PrimaryExpression p = getUnknown(lvalue.getStreamPos(), (BlockScope)lvalue.outerScope,
                probabilisticMethod);
        return new AssignmentExpression(lvalue.getStreamPos(),
            (BlockScope)lvalue.outerScope, lvalue, p);
    }
    /**
     * Adds an if--then--else statement or conditional expression code to a
     * block of code.<br>
     *
     * Calling: optional.
     *
     * @param probabilisticMethod if a probabilistic variant, then qualified name of a static method
     * retruning a non--deterministic boolean value; otherwise null
     * @param block                     block to which append the code
     * @param lvalue lvaue for ternary ifs; null for none
     * @param condition                 condition
     * @param truE                      statement to execute in case
     *                                  condition is true
     * @param falsE                     statement to execute in case
     *                                  condition is false, null for
     *                                  none
     * @param unknowN the unknown block of a ternary if; otherwise null
     * @param endStreamPos              position of the stream
     *                                  after the if--then--else statement
     *                                  is parsed, can be null, used only
     *                                  to create the end label with
     *                                  this stream position, if null,
     *                                  the condition stream position
     *                                  is used instead
     *
     */
    public static void appendIfThenElseCode(String probabilisticMethod,
            Block block, PrimaryExpression lvalue, AbstractExpression condition,
            AbstractStatement truE, AbstractStatement falsE, AbstractStatement unknowN,
            StreamPos endStreamPos) throws ParseException {
        boolean probabilistic = probabilisticMethod != null;
        AbstractExpression condition2;
        if(probabilistic) {
            String clazz = "verics.lang.AbstractAgent";
            Variable v = condition.outerScope.newInternalLocal(condition.getStreamPos(),
                    new Type(new NameList(clazz)), null,
                    Variable.Category.INTERNAL_NOT_RANGE);
            condition2 = dereferenceWithMethod(newVariableExpression(condition.getStreamPos(),
                    (BlockScope)condition.outerScope, v), "branch2F");
            condition = dereferenceWithMethod(
                    newAssignment(condition.getStreamPos(), (BlockScope)condition.outerScope,
                        v, condition), "branch1T");
        } else {
            condition2 = null;
            if(lvalue != null)
                throw new RuntimeException("lvalue in a non--ternary if");
        }
        String endLabel = block.scope.newLabel();
        String falseLabel;
        String unknownLabel;
        boolean needsFalseBranch = falsE != null || lvalue != null ||
                    (probabilistic && unknowN != null);
        if(needsFalseBranch)
            falseLabel = block.scope.newLabel();
        else
            falseLabel = endLabel;
        boolean needsUnknownBranch = unknowN != null || lvalue != null;
        if(needsUnknownBranch) {
            if(!probabilistic)
                throw new RuntimeException("unknown branch in a non--ternary if");
            unknownLabel = block.scope.newLabel();
        } else
            unknownLabel = endLabel;
        block.code.add(new BranchStatement(condition.getStreamPos(), block.scope, condition,
            null, falseLabel));
        if(lvalue != null)
            block.code.add(getConstAssignment(lvalue, new Literal(true)));
        block.code.add(truE);
        if(needsFalseBranch) {
            if(falsE == null)
                falsE = new EmptyExpression(condition.getStreamPos(), block.scope);
            AbstractStatement a;
            a = new JumpStatement(falsE.getStreamPos(), block.scope, endLabel);
            a.canBeDead = true;
            block.code.add(a);
            if(probabilistic) {
                a = new BranchStatement(condition2.getStreamPos(), block.scope, condition2,
                        null, unknownLabel);
                block.code.add(new LabeledStatement(falsE.getStreamPos(), block.scope, falseLabel,
                    a));
                if(lvalue != null)
                    block.code.add(getConstAssignment(lvalue, new Literal(false)));
                block.code.add(falsE);
            } else {
                block.code.add(new LabeledStatement(falsE.getStreamPos(), block.scope, falseLabel,
                    falsE));
            }
        }
        if(needsUnknownBranch) {
            if(unknowN == null)
                unknowN = new EmptyExpression(condition.getStreamPos(), block.scope);
            AbstractStatement a;
            a = new JumpStatement(unknowN.getStreamPos(), block.scope, endLabel);
            a.canBeDead = true;
            block.code.add(a);
            AbstractStatement prefix;
            if(lvalue != null)
                prefix = getUnknownAssignment(lvalue, probabilisticMethod);
            else
                prefix = unknowN;
            block.code.add(new LabeledStatement(unknowN.getStreamPos(), block.scope, unknownLabel,
                prefix));
            if(lvalue != null)
                block.code.add(unknowN);
        }
        StreamPos pos;
        if(endStreamPos != null)
            pos = endStreamPos;
        else
            pos = condition.getStreamPos();
        block.code.add(new LabeledStatement(pos, block.scope, endLabel, null));
    }
    /**
     * Like <code>Integer.parseInt</code>, put checks for prefixes of
     * octal or hexadecimal literals. 
     * 
     * @param string an integer literal, decimal, octal or hexadecimal
     * @return integer value
     */
    public static int parseInt(String string) {
        if(string.startsWith("0x"))
            return Integer.parseInt(string.substring(2), 16);
        else if(string.length() > 1 && string.startsWith("0"))
            return Integer.parseInt(string.substring(1), 8);
        else
            return Integer.parseInt(string);
    }
    /**
     * Like <code>Long.parseLong</code>, put checks for prefixes of
     * octal or hexadecimal literals. 
     * 
     * @param string an integer literal, decimal, octal or hexadecimal
     * @return integer value
     */
    public static long parseLong(String string) {
        if(string.startsWith("0x"))
            return Long.parseLong(string.substring(2), 16);
        else if(string.length() > 1 && string.startsWith("0"))
            return Long.parseLong(string.substring(1), 8);
        else
            return Long.parseLong(string);
    }
}
