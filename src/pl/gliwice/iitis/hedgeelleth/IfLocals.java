/*
 * IfLocals.java
 *
 * Created on Oct 17, 2016
 *
 * Copyright (c) 2016  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.Variable;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.scope.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.PrimaryExpression;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * Common variables within the poduction <code>If.grammar</code>.
 * 
 * @author Artur Rataj
 */
public class IfLocals {
    public boolean probabilistic;
    public Block block;
    public PrimaryExpression lvalue = null;
    public Variable variable = null;
    public AbstractExpression condition;
    public boolean trueBranch = false;
    public AbstractStatement truE = null;
    public AbstractStatement falsE = null;
    public AbstractStatement unknowN = null;
    public boolean declaration = false;
    public boolean finaL = false;
    public boolean elseifShortcut = false;
    public StreamPos assignmentPos = null;
    public StreamPos elseifPos = null;
    public StreamPos unknownPos = null;
    
    public IfLocals(StreamPos pos, BlockScope scope) {
        block = new Block(pos, scope);
    }
}
