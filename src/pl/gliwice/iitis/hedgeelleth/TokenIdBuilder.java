/*
 * TokenIdBuilder.java
 *
 * Created on Nov 9, 2011
 *
 * Copyright (c) 2011 Artur Rataj
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */
package pl.gliwice.iitis.hedgeelleth;

import java.util.*;
import java.io.PrintWriter;

import pl.gliwice.iitis.hedgeelleth.compiler.util.CompilerUtils;
import pl.gliwice.iitis.hedgeelleth.languagedescription.CommentTypeDescription;
import pl.gliwice.iitis.hedgeelleth.languagedescription.LanguageDescription;
import pl.gliwice.iitis.hedgeelleth.languagedescription.LiteralTypeDescription;

/**
 * A class for building <code>.jj.tokenId</code> file. The file contains a
 * subclass of <code>org.netbeans.spi.lexer.LanguageHierarchy</code>.<br>
 * 
 * The generated file is a java class, but it has another extension in case, that
 * it should be compiled in another place.
 * 
 * @author Artur Rataj
 */
public class TokenIdBuilder {
    /**
     * A class representing the tuple token name, token primary category, token identifier.
     */
    protected static class TokenId {
        public String name;
        public String category;
        public String id;
        
        public TokenId(String name, String category, String id) {
            this.name = name;
            this.category = category;
            this.id = id;
        }
    };
    /**
     * A partial prefix before the token array code, to be transformed using <code>process()</code>,
     * Netbeans flavour.
     */
    final static String PREFIX_NETBEANS =
        "private static List<#TokenId> tokens;\n" +
        "private static Map<Integer,#TokenId> idToToken;\n" +
        "\n" +
        "private static int lookup(String image) {\n" +
        "    image= \"\\\"\" + image + \"\\\"\";\n" +
        "    int count = 0;\n" +
        "    for(String s : TokenId#ParserConstants.tokenImage) {\n" +
        "        if(image.equals(s))\n" +
        "            return count;\n" +
        "        ++count;\n" +
        "    }\n" +
        "    throw new RuntimeException(\"token image `\" + image + \"' not found\");\n" +
        "}\n" +
        "private static void init() {\n" +
        "    tokens = Arrays.<#TokenId>asList(new #TokenId[] {\n";
    /**
     * A partial prefix before the token array code, to be transformed using <code>process()</code>,
     * Hedgeelleth flavour.
     */
    final static String PREFIX_HEDGEELLETH =
        "private int lookup(String image) {\n" +
        "    image= \"\\\"\" + image.replace(\"'\", \"\\\\'\") + \"\\\"\";\n" +
        "    int count = 0;\n" +
        "    for(String s : TokenId#ParserConstants.tokenImage) {\n" +
        "        if(image.equals(s))\n" +
        "            return count;\n" +
        "        ++count;\n" +
        "    }\n" +
        "    throw new RuntimeException(\"token image `\" + image + \"' not found\");\n" +
        "}\n" +
        "public #Tokenizer() {\n" +
        "    super();\n" +
        "    tokens = Arrays.<TokenDescription>asList(new TokenDescription[] {\n";
         
    /**
     * A partial suffix after the token array code, to be transformed using <code>process()</code>,
     * Netbeans flavour.
     */
    final static String SUFFIX_NETBEANS =
    "    idToToken = new HashMap<Integer, #TokenId>();\n" +
    "    for (#TokenId token : tokens) {\n" +
    "        idToToken.put(token.ordinal(), token);\n" +
    "    }\n" +
    "}\n" +
    "static synchronized #TokenId getToken(int id) {\n" +
    "    if (idToToken == null) {\n" +
    "        init();\n" +
    "    }\n" +
    "    return idToToken.get(id);\n" +
    "}\n" +
    "protected synchronized Collection<#TokenId> createTokenIds() {\n" +
    "    if (tokens == null) {\n" +
    "        init();\n" +
    "    }\n" +
    "    return tokens;\n" +
    "}\n" +
    "protected synchronized Lexer<#TokenId> createLexer(LexerRestartInfo<#TokenId> info) {\n" +
    "    return new #Lexer(info);\n" +
    "}\n" +
    "protected String mimeType() {\n";
    /**
     * A partial suffix after the token array code, to be transformed using <code>process()</code>,
     * Hedgeelleth flavour.
     */
    final static String SUFFIX_HEDGEELLETH =
    "    for(TokenDescription token : tokens) {\n" +
    "        idToToken.put(token.ID, token);\n" +
    "    }\n" +
    "}\n" +
    "public List<HedgeellethToken> parse(String snippet) {\n" +
    "    List<HedgeellethToken> out = new ArrayList<>();\n" +
    "    TokenIdVericsParserTokenManager tm = new TokenId#ParserTokenManager(\n" +
    "            new JavaCharStream(new StringReader(snippet)));\n" +
    "    Token t;\n" +
    "    while((t = tm.getNextToken()).kind != TokenId#ParserConstants.EOF)\n" +
    "        out.add(new HedgeellethToken(getToken(t.kind),\n" +
    "            new StreamPos(null, t.beginLine, t.beginColumn),\n" +
    "            new StreamPos(null, t.beginLine, t.beginColumn)));\n" +
    "    return out;\n" +
    "}\n";

    /**
     * Replaces all instances of the character '#' in <code>in</code> with <code>name</code>.
     * 
     * @param in input string
     * @param name replacement string
     * @return output string
     */
    protected static String process(String in, String name) {
        StringBuilder out = new StringBuilder();
        for(int i = 0; i < in.length(); ++i) {
            char c = in.charAt(i);
            if(c == '#')
                out.append(name);
            else
                out.append(c);
        }
        return out.toString();
    }
    /**
     * Changes `\\' into "\\\\".
     * 
     * @param in input string
     * @return input string with escape characters processed
     */
    protected static String escapeSlashes(String in) {
        String out = "";
        for(int i = 0; i < in.length(); ++i) {
            char c = in.charAt(i);
            switch(c) {
                case '\\':
                    out += "\\\\";
                    break;

                default:
                    out += c;
            }
        }
        return out;
    }
    /**
     * Adds new tokens, whose id is found using <code>lookup(tokenImage)</code>.
     * 
     * @param list list of tokens to be appended
     * @param set a set of token images
     * @param category primary category of the added tokens
     */
    protected static void add(List<TokenId> list, Set<String> set, String category) {
        for(String s : set) {
            s = escapeSlashes(s);
            list.add(new TokenId(s, category, "lookup(\"" + s + "\")"));
        }
    }
    /**
     * Creates a new instance of <code>TokenIdBuilder</code>.
     * 
     * @param tokenId specifies the type of the tokenizer class to build
     * @param languageDescription language description
     * @param parserName fully qualified name of the TokenId parser class
     * @param targetPackageName package of the generated class
     * @param out writer to use; its errors are not checked and it is not closed by this method
     */
    public static void generate(Builder.LexerType tokenId, LanguageDescription ld, String parserName,
            String targetPackageName, PrintWriter out) {
        String className = ld.languageName;
        switch(tokenId) {
            case NETBEANS:
                className += "LanguageHierarchy";
                break;
                
            case HEDGEELLETH:
                className += "Tokenizer";
                break;

            default:
                throw new RuntimeException("unknown type");
        }
        out.println("/* " + className + ".java, generated by " + About.DESCRIPTION + " */\n");
        out.println("package " + targetPackageName + ";\n");
        switch(tokenId) {
            case NETBEANS:
                out.print("import java.util.*;\n" +
                        "import org.netbeans.spi.lexer.*;\n\n" +
                        "import " + parserName + "Constants;\n\n" +
                        "public class " + ld.languageName + "LanguageHierarchy extends " +
                            "LanguageHierarchy<" + ld.languageName + "TokenId> {\n" +
                        CompilerUtils.indent(2, process(PREFIX_NETBEANS, ld.languageName)));
                break;
                
            case HEDGEELLETH:
                out.print(
                        "import java.util.*;\n" +
                        "import java.io.*;\n\n" +
                        "import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;\n" +
                        "import pl.gliwice.iitis.hedgeelleth.tokenizer.*;\n\n" +
                        "import " + parserName + "Constants;\n\n" +
                        "public class " + ld.languageName + "Tokenizer extends " +
                            "AbstractTokenizer {\n" +
                        CompilerUtils.indent(2, process(PREFIX_HEDGEELLETH, ld.languageName)));
                break;

            default:
                throw new RuntimeException("unknown type");
        }
        // a list of tokens
        List<TokenId> tokens = new LinkedList<>();
        // primary category whitespace
        final String WHITESPACE = "whitespace";
        // primary category comment
        final String COMMENT = "comment";
        // primary category number
        final String NUMBER = "number";
        // primary category keyword
        final String KEYWORD = "keyword";
        add(tokens, ld.whitespace, WHITESPACE);
        final String CONSTANTS =
                // unqualified class name suffices because of a respective import
                parserName.substring(parserName.lastIndexOf('.') + 1) +
                "Constants.";
        for(CommentTypeDescription c : ld.commentTypes) {
            switch(c.type) {
                case SINGLE_LINE:
                    tokens.add(new TokenId("SingleLineComment", COMMENT,
                            CONSTANTS + "SINGLE_LINE_COMMENT"));
                    break;
                    
                case SPECIAL_MULTI_LINE:
                    tokens.add(new TokenId("SpecialMultiLineComment", COMMENT,
                            CONSTANTS + "SPECIAL_MULTI_LINE_COMMENT"));
                    break;
                    
                case MULTI_LINE:
                    tokens.add(new TokenId("MultiLineComment", COMMENT,
                            CONSTANTS + "MULTI_LINE_COMMENT"));
                    break;
                    
                default:
                    throw new RuntimeException("unknown comment type");
            }
        }
        boolean integerFound = false;
        for(LiteralTypeDescription l : ld.literals) {
            switch(l.type) {
                case DECIMAL:
                case OCTAL:
                case HEXADECIMAL:
                    if(integerFound)
                        continue;
                    tokens.add(new TokenId("Integer", NUMBER,
                            CONSTANTS + "INTEGER_LITERAL"));
                    integerFound = true;
                    break;
                    
                case FLOATING_POINT:
                    tokens.add(new TokenId("FloatingPoint", NUMBER,
                            CONSTANTS + "FLOATING_POINT_LITERAL"));
                    break;
                    
                case QUOTED_CHARACTER:
                    tokens.add(new TokenId("QuotedCharacter", "character",
                            CONSTANTS + "CHARACTER_LITERAL"));
                    break;
                    
                case QUOTED_STRING:
                    tokens.add(new TokenId("QuotedString", "string",
                            CONSTANTS + "STRING_LITERAL"));
                    break;
                    
                case NULL:
                    tokens.add(new TokenId("Null", KEYWORD,
                            "lookup(\"" + escapeSlashes(l.nullString) + "\")"));
                    break;

                case INFINITY:
                    tokens.add(new TokenId("Infinity", KEYWORD,
                            "lookup(\"" + escapeSlashes(l.infinityString) + "\")"));
                    break;

                case TRUTH:
                    // truth values are present in the keywords section
                    /*
                    tokens.add(new TokenId(escape(l.trueString), KEYWORD,
                            "lookup(\"" + escape(l.trueString) + "\")"));
                    tokens.add(new TokenId(escape(l.falseString), KEYWORD,
                            "lookup(\"" + escape(l.falseString) + "\")"));
                     */
                    break;
                    
                default:
                    throw new RuntimeException("unknown literal type type");
            }
        }
        if(ld.identifierType != null)
            tokens.add(new TokenId("Identifier", "identifier",
                    CONSTANTS + "IDENTIFIER"));
        add(tokens, ld.keywords, KEYWORD);
        add(tokens, ld.operators, "operator");
        add(tokens, ld.separators, "separator");
        add(tokens, ld.illegalTokens, "error");
        tokens.add(new TokenId("Error", "error",
                CONSTANTS + "ERROR"));
        String p;
        switch(tokenId) {
            case NETBEANS:
                p = ld.languageName + "TokenId";
                break;
                
            case HEDGEELLETH:
                p = "TokenDescription";
                break;

            default:
                throw new RuntimeException("unknown type");
        }
        for(TokenId t : tokens) {
            out.println("            new " +
                    p + "(\"" + t.name + "\", " +
                    "\"" + t.category + "\", " +
                    t.id +
                    "),");
        }
        String suffixStr;
        String mimeStr;
        switch(tokenId) {
            case NETBEANS:
                suffixStr = SUFFIX_NETBEANS;
                mimeStr =
                        "    return \"" + ld.mimeType + "\";\n" +
                        "}\n";
                break;
                
            case HEDGEELLETH:
                suffixStr = SUFFIX_HEDGEELLETH;
                mimeStr = "";
                break;

            default:
                throw new RuntimeException("unknown type");
        }
        out.println(CompilerUtils.indent(2,
                    "    });\n" +
                    process(suffixStr, ld.languageName) +
                    mimeStr) +
                "}");
    }
}
