/*
 * VerbatimSectionBuilder.java
 *
 * Created on Nov 18, 2008
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth;

import java.util.*;
import java.io.*;

import pl.gliwice.iitis.hedgeelleth.compiler.primitiverange.PrimitiveRangeDescription;
import pl.gliwice.iitis.hedgeelleth.compiler.util.text.LineIndex;
import pl.gliwice.iitis.hedgeelleth.compiler.util.CompilerUtils;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.languagedescription.*;

/**
 * A class for preprocessing the verbatim section of either section or
 * code. Used by the <code>Builder</code> class.
 * 
 * @author Artur Rataj
 */
public class VerbatimSectionBuilder {
    /**
     * A keyword that marks sections to generate by this processor.
     */
    static final String KEYWORD = "HEDGEELLETH";
    /**
     * First part of the prefix of the production <code>HedgeellethType</code>,
     * to be optionally postfixed with mutator modifier.
     */
    static final String TYPE_PREFIX_1 =
        "Type HedgeellethType() :\n" +
        "{\n" +
        "    Type type;\n" +
        "    NameList name;\n" +
        "}\n" +
        "{\n" +
        "    (\n" +
        "       (" +
        "                type = HedgeellethPrimitiveType()\n" +
        "            |\n" +
        "                name = Name() { type = new Type(name); }\n";
    /**
     * Second part of the prefix of the production <code>HedgeellethType</code>,
     * to be optionally prefixed with mutator modifier.
     */
    static final String TYPE_PREFIX_2 =
        "       )\n";
    /**
     * If arrays are allowed, then this is appended to
     * <code>TYPE_PREFIX</code>.
     */
    static final String TYPE_PREFIX_ARRAYS =
        "         ( // lookahead of 2 needed to exclude a specifier of array size\n" +
        "           LOOKAHEAD(2) \"[\" \"]\" {\n" +
        "             if(\n";
    /**
     * If arrays are allowed, then this is prepended before
     * <code>TYPE_POSTFIX</code>.
     */
    static final String TYPE_POSTFIX_ARRAYS =
        "             ) {\n" +
        "                 type.increaseDimension(hedgeelleth.getUnit().frontend.\n" +
        "                     arrayClassName);\n" +
        "             } else {\n" +
        "                 throw new ParseException(getStreamPos(),\n" +
        "                     ParseException.Code.ILLEGAL,\n" +
        "                     \"array of \" + type.toNameString() +\n" +
        "                     \" not allowed\");\n" +
        "             }\n" +
        "         } )*\n";
    /**
     * Postfix of the production <code>HedgeellethType</code>.
     */
    static final String TYPE_POSTFIX =
        "    )\n" +
        "    {\n" +
        "        if(type.mutatorReference) {\n" +
        "            if(!type.isJava())\n" +
        "                 throw new ParseException(getStreamPos(),\n" +
        "                     ParseException.Code.ILLEGAL,\n" +
        "                     \"mutator flag on primitive type\");\n" +
        "            if(!hedgeelleth.getUnit().frontend.mutatorFlags)\n" +
        "                throw new RuntimeException(\"mutator references \" +\n" +
        "                    \"not allowed by frontend\");\n" +
        "        }\n" +
        "        return type;\n" +
        "    }\n" +
        "}\n";
    /**
     * Prefix of the production <code>HedgeellethPrimitiveType</code>.
     */
    static final String PRIMITIVE_TYPE_PREFIX =
        "Type HedgeellethPrimitiveType() :\n" +
        "{\n" +
        "    Type type;\n" +
        "}\n" +
        "{\n" +
        "    (\n";
    /**
     * Postfix of the production <code>HedgeellethPrimitiveType</code>.
     */
    static final String PRIMITIVE_TYPE_POSTFIX =
        "    )\n" +
        "    {\n" +
        "        return type;\n" +
        "    }\n" +
        "}\n";
    /**
     * Prefix of the production <code>HedgeellethLiteral</code>.
     */
    static final String LITERAL_PREFIX =
        "Literal HedgeellethLiteral() :\n" +
        "{\n" +
        "    Literal literal;\n" +
        "    Token t;\n" +
        "    char suffix;\n" +
        "}\n" +
        "{\n" +
        "    (\n";
    /**
     * Postfix of the production <code>HedgeellethLiteral</code>.
     */
    static final String LITERAL_POSTFIX =
        "    )\n" +
        "    {\n" +
        "        return literal;\n" +
        "    }\n" +
        "}\n";
    /**
     * Production <code>HedgeellethTruthLiteral</code>. The production
     * is used by <code>HedgeellethLiteral</code>.<br>
     *
     * The characters `0' and `1' mark the point of insertion of,
     * respectively, the strings of false and true constants.
     */
    static final String TRUTH_LITERAL =
        "Literal HedgeellethTruthLiteral() :\n" +
        "{\n" +
        "    Literal literal;\n" +
        "}\n" +
        "{\n" +
        "  (\n" +
        "      \"0\" { literal = new Literal(false); }\n" +
        "    |\n" +
        "      \"1\" { literal = new Literal(true); }\n" +
        "  )\n" +
        "  {\n" +
        "    return literal;\n" +
        "  }\n" +
        "}\n";
    /**
     * Production <code>HedgeellethNullLiteral</code>. The production
     * is used by <code>HedgeellethLiteral</code>.<br>
     *
     * The character `0' marks the point of insertion of the null
     * constant string.
     */
    static final String NULL_LITERAL =
        "Literal HedgeellethNullLiteral() :\n" +
        "{}\n" +
        "{\n" +
        "  \"0\"\n" +
        "  {\n" +
        "    return new Literal();\n" +
        "  }\n" +
        "}\n";
    /**
     * Production <code>HedgeellethInfinityLiteral</code>. The production
     * is used by <code>HedgeellethLiteral</code>.<br>
     *
     * The character `0' marks the point of insertion of the infinity
     * constant string.
     */
    static final String INFINITY_LITERAL =
        "Literal HedgeellethInfinityLiteral() :\n" +
        "{}\n" +
        "{\n" +
        "  \"0\"\n" +
        "  {\n" +
        "    return new Literal(Double.POSITIVE_INFINITY);\n" +
        "  }\n" +
        "}\n";
    /**
     * Code for parsing character literal. It is used by
     * <code>HedgeellethLiteral</code>.
     */
    static final String CHARACTER_LITERAL_CODE =
        "String s = t.image.substring(1, t.image.length() - 1);\n" +
        "int[] pos = new int[1];\n" +
        "Character c = StringEscape.next(s, pos);\n" +
        "if(c == null)\n" +
        "    throw new ParseException(getStreamPos(),\n" +
        "       ParseException.Code.INVALID,\n" +
        "        \"invalid escape sequence\");\n" +
        "if(pos[0] != s.length())\n" +
        "    throw new ParseException(getStreamPos(),\n" +
        "       ParseException.Code.INVALID,\n" +
        "        \"invalid length of character definition\");\n" +
        "literal = new Literal(c);\n";
    /**
     * Indent string.
     */
    static final String INDENT = "    ";

    public enum SectionType {
        CODE,
        GRAMMAR;
        
        public String getName() {
            switch(this) {
                case CODE:
                    return "code";
                    
                case GRAMMAR:
                    return "grammar";
                    
                default:
                    throw new RuntimeException("unknown section type");
            }
        }
    }

    /**
     * Language description.
     */
    LanguageDescription ld;
    
    /**
     * Creates a new instance of VerbatimSectionBuilder.
     */
    public VerbatimSectionBuilder(LanguageDescription ld) {
        this.ld = ld;
    }
    /**
     * Generates a production of <code>HedgeellethType</code>.
     *
     * @return                          string with the generated production
     */
    String generateTypeProduction() {
        String out = TYPE_PREFIX_1;
        if(ld.mutatorFlags)
            out +=
                            "                [ \"~\" { type.mutatorReference = true; } ]\n";
        out += TYPE_PREFIX_2;
        if(!ld.arrays.isEmpty()) {
            out += TYPE_PREFIX_ARRAYS;
            boolean first = true;
            for(ArrayTypeDescription pt : ld.arrays) {
                if(!first)
                    out += " ||\n";
                out += INDENT + INDENT + INDENT + INDENT;
                if(pt.elementType != null)
                    out += "type.getPrimitive() == Type.PrimitiveOrVoid." + pt.elementType;
                else
                    out += "type.isJava()";
                first = false;
            }
            out += "\n" + TYPE_POSTFIX_ARRAYS;
        }
        out += TYPE_POSTFIX;
        return CompilerUtils.indent(2, out).trim();
    }
    /**
     * Generates a production of <code>HedgeellethPrimitiveType</code>.
     *
     * @return                          string with the generated production
     */
    String generatePrimitiveTypeProduction() {
        String out = PRIMITIVE_TYPE_PREFIX;
        boolean first = true;
        for(PrimitiveTypeDescription pt : ld.primitiveTypes) {
            if(!first)
                out += INDENT + INDENT + "|\n";
            out += INDENT + INDENT + INDENT +
                    "\"" + pt.keyword + "\" { type = new Type(" +
                    "Type.PrimitiveOrVoid." + pt.type.toString() + "); }\n";
            first = false;
        }
        out += PRIMITIVE_TYPE_POSTFIX;
        return CompilerUtils.indent(2, out).trim();
    }
    /**
     * Generates code that handler literal specifier recognition.<br>
     * 
     * The code sets two variables: <code>bits</code> that contains
     * the number of bits of the literal, and <code>identifier</code>
     * that is the literal identifier with possible postfix trimmed.
     * 
     * @param ltd                       literal description
     * @return                          generated code
     */
    private String generateLiteralSpecifierCode(LiteralTypeDescription ltd) {
        String out =
            "String identifier = t.image;\n" +
            "suffix = identifier.charAt(identifier.length()  - 1);\n";
        String suffixCode = "";
        int defaultBits = -1;
        for(int b = 0; b <= 1; ++b) {
            String condition = "";
            int bits = (b + 1)*32;
            String s = ltd.specifier.select(bits);
            for(int i = 0; i < s.length(); ++i) {
                if(s.charAt(i) == '.')
                    defaultBits = bits;
                else {
                    if(condition.length() == 0) {
                        condition += "if(";
                    } else
                        condition += " ||\n" +
                                INDENT + INDENT;
                    condition +=
                            "suffix == '" + s.charAt(i) + "'";
                }
            }
            if(condition.length() != 0) {
                condition += ") {\n" +
                        INDENT + "bits = " + bits + ";\n" +
                        INDENT + "identifier = identifier.substring(0,\n" +
                        INDENT + INDENT +"identifier.length() - 1);\n" +
                        "}\n";
               if(suffixCode.length() != 0)
                   suffixCode += "else ";
               suffixCode += condition;
            }
        }
        out +=
            "int bits = " + defaultBits + ";\n" +
            suffixCode;
        return out;
    }
    /**
     * Generates a statement that chooses one of constructors
     * of <code>Literal</code>, depending on the number of bits.
     * 
     * @param arguments32               arguments for the 32--bit
     *                                  constructor
     * @param arguments64               arguments for the 64--bit
     *                                  constructor
     * @return                          generated code
     */
    String generateLiteralConstructors(String arguments32, String arguments64) {
        return
            "switch(bits) {\n" +
            INDENT + "case 32:\n" +
            INDENT + INDENT + "literal = new Literal(" + arguments32 + ");\n" +
            INDENT + INDENT + "break;\n" +
            INDENT + INDENT + "\n" +
            INDENT + "case 64:\n" +
            INDENT + INDENT + "literal = new Literal(" + arguments64 + ");\n" +
            INDENT + INDENT + "break;\n" +
            INDENT + INDENT + "\n" +
            INDENT + "default:\n" +
            INDENT + INDENT + "throw new RuntimeException(\n" +
            INDENT + INDENT + INDENT + "\"unknown number of bits\");\n" +
            INDENT + INDENT + "\n" +
            "}\n";
    }
    /**
     * Generates a production of <code>HedgeellethPrimitiveType</code>.
     *
     * @return                          string with the generated production
     */
    String generateLiteralProduction() {
        String out = LITERAL_PREFIX;
        boolean integerAdded = false;
        boolean first = true;
        String miscProductions = "";
        for(LiteralTypeDescription ltd: ld.literals) {
            if(ltd.type.isInteger() && integerAdded)
                continue;
            if(!first)
                out += INDENT + "|\n";
            String tokenString = null;
            String productionString = null;
            String codeString = null;
            if(ltd.type.isInteger() && !integerAdded) {
                tokenString = "INTEGER_LITERAL";
                codeString = generateLiteralSpecifierCode(ltd) +
                        generateLiteralConstructors(
                            "Hedgeelleth.parseInt(identifier)",
                            "Hedgeelleth.parseLong(identifier)");
                integerAdded = true;
            } else
                switch(ltd.type) {
                    case FLOATING_POINT:
                        tokenString = "FLOATING_POINT_LITERAL";
                        codeString = generateLiteralSpecifierCode(ltd) +
                                generateLiteralConstructors(
                                    "Float.parseFloat(identifier)",
                                    "Double.parseDouble(identifier)");
                        break;
                        
                    case QUOTED_CHARACTER:
                        tokenString = "CHARACTER_LITERAL";
                        codeString = CHARACTER_LITERAL_CODE;
                        break;
                        
                    case QUOTED_STRING:
                        tokenString = "STRING_LITERAL";
                        codeString =
                                "int[] unicodeError = new int[1];\n" +
                                "String unicodeString = StringEscape.convert(\n" +
                                INDENT + INDENT + "t.image.substring(1, t.image.length() - 1), unicodeError);\n" +
                                "if(unicodeString == null) {\n" +
                                "    StreamPos unicodePos = new StreamPos(getStreamName(), t);\n" +
                                "    unicodePos.column += 1 + unicodeError[0];\n" +
                                "    throw new ParseException(unicodePos,\n" +
                                "       ParseException.Code.INVALID,\n" +
                                "        \"invalid escape sequence\");\n" +
                                "}\n" +
                                "literal = new Literal(unicodeString,\n" +
                                INDENT + INDENT + "hedgeelleth.getUnit() == null ? null :\n" +
                                INDENT + INDENT + INDENT + "hedgeelleth.getUnit().frontend.stringClassName);\n";
                        break;
                        
                    case TRUTH:
                    {
                        productionString = "HedgeellethTruthLiteral";
                        String t = TRUTH_LITERAL.substring(0, TRUTH_LITERAL.indexOf('0')) +
                                ltd.falseString +
                                TRUTH_LITERAL.substring(TRUTH_LITERAL.indexOf('0') + 1,
                                    TRUTH_LITERAL.indexOf('1')) +
                                ltd.trueString +
                                TRUTH_LITERAL.substring(TRUTH_LITERAL.indexOf('1') + 1);
                        miscProductions += "\n" + t;
                        break;
                    }
                    case NULL: {
                        productionString = "HedgeellethNullLiteral";
                        String t = NULL_LITERAL.substring(0, NULL_LITERAL.indexOf('0')) +
                                ltd.nullString +
                                NULL_LITERAL.substring(NULL_LITERAL.indexOf('0') + 1);
                        miscProductions += "\n" + t;
                        break;
                    }
                    case INFINITY: {
                        productionString = "HedgeellethInfinityLiteral";
                        String t = INFINITY_LITERAL.substring(0, INFINITY_LITERAL.indexOf('0')) +
                                ltd.infinityString +
                                INFINITY_LITERAL.substring(INFINITY_LITERAL.indexOf('0') + 1);
                        miscProductions += "\n" + t;
                        break;
                    }
                    default:
                        throw new RuntimeException("unknown literal type");
                        
                }
            String s = INDENT + INDENT;
            if(tokenString != null)
                s += "t = <" + tokenString + ">";
            else if(productionString != null)
                s += "literal = " + productionString + "()";
            if(codeString != null) {
                codeString = "{\n" +
                        CompilerUtils.indent(1, codeString, INDENT) +
                        "}\n";
                s += " {\n" +
                        CompilerUtils.indent(3, codeString, INDENT) +
                        INDENT + INDENT + "}\n";
            } else
                s += "\n";
            out += s;
            first = false;
        }
        out += LITERAL_POSTFIX +
                miscProductions;
        return CompilerUtils.indent(2, out).trim();
    }
    /**
     * Finds a block delimited with "{" and "}", that begins at or directly
     * after a given index in some string. The block can contain nested
     * "{" and "}".
     *
     * @param in                        scanned string
     * @param index                     index to begin the scan, between the
     *                                  character at the index and the block
     *                                  can occur at most whitespace characters
     * @param endPos                    an array with a single element,
     *                                  contains the end position of the block,
     *                                  points to "}" within <code>in</code>,
     *                                  if the block was not found contains -1
     * @return                          contents of the block, without the
     *                                  block's delimiters "{" and "}", or null
     *                                  if the block was not found
     */
    String getBlock(String in, int index, int[] endPos) {
        while(index < in.length() - 1 && Character.isWhitespace(in.charAt(index)))
            ++index;
        if(in.charAt(index) != '{')
            return null;
        else
            ++index;
        int nested = 0;
        int beginPos = index;
        endPos[0] = -1;
        SCAN:
        while(index < in.length()) {
            char c = in.charAt(index);
            switch(c) {
                case '{':
                    ++nested;
                    break;

                case '}':
                    if(nested == 0) {
                        endPos[0] = index;
                        break SCAN;
                    } else
                        --nested;
                    break;
            }
            ++index;
        }
        if(endPos[0] != -1)
            return in.substring(beginPos, endPos[0]);
        else
            return null;
    }
    /**
     * A helper enumeration for <code>generateBinaryLeftProduction</code>.
     */
    enum BinaryLeftMode {
        OPTIONS_TOKEN,
        OPTIONS_OPEN,
        OPTIONS_CONTENTS,
        EXPRESSION,
        COLON,
        OP,
        OP_SYMBOL,
        OP_CONSTANT,
        SUBEXPRESSION,
    }
    /**
     * Generates a production of a generic binary expression with left
     * associativity.
     *
     * @param in                        string within the brackets following
     *                                  <code>HEDGEELLETH(LEFT_BINARY)</code>
     * @param error                     empty if no error occured, otherwise contains
     *                                  the error's description
     * @return                          string with the generated production,
     *                                  or null if an error occured
     */
    String generateBinaryLeftProduction(String in, StringBuilder error) {
        String out = "";
        error.setLength(0);
        Scanner sc = CompilerUtils.newScanner(in);
        BinaryLeftMode mode = BinaryLeftMode.OPTIONS_TOKEN;
        boolean enablePrimitiveRangeTag = false;
        boolean enablePrimitiveRangeModifier = false;
        String expression = null;
        String opSymbol = null;
        List<String> opSymbols = null;
        List<String> opConstants = null;
        List<List<String>> xorGroups = null;
        List<String> currXorGroup = null;
        while(sc.hasNext()) {
            String token = sc.next();
            // System.out.println("token = " + token + " mode = " + mode + " out = \n" + out);
            switch(mode) {
                case OPTIONS_TOKEN:
                {
                    if(token.equals("options")) {
                        mode = BinaryLeftMode.OPTIONS_OPEN;
                        break;
                    }
                    // continue to the EXPRESSION case
                }
                case EXPRESSION:
                {
                    opSymbols = new LinkedList<>();
                    opConstants = new LinkedList<>();
                    xorGroups = new LinkedList<>();
                    currXorGroup = new LinkedList<>();
//System.out.println("RESET");
                    expression = token;
                    mode = BinaryLeftMode.COLON;
                    break;
                }
                case OPTIONS_OPEN:
                {
                    if(!token.equals("{")) {
                        error.append("no `{' after `options'");
                        return null;
                    } else
                        mode = BinaryLeftMode.OPTIONS_CONTENTS;
                    break;
                }
                case OPTIONS_CONTENTS:
                {
                    if(token.equals("}")) {
                        mode = BinaryLeftMode.EXPRESSION;
                    } else {
                        /* if(token.equals(""))
                            ;
                        else */ {
                            error.append("in the options block: " +
                                    "unknown option `" + token + "'");
                            return null;
                        }
                    }
                    break;
                }
                case COLON:
                {
                    if(!token.equals(":")) {
                        error.append("no `:' after expression");
                        return null;
                    } else
                        mode = BinaryLeftMode.OP;
                    break;
                }
                case OP:
                {
                    if(token.equals("op") || token.equals("xor")) {
                        if(token.equals("op"))
                            mode = BinaryLeftMode.OP_SYMBOL;
                        else {
                            if(currXorGroup.isEmpty()) {
                                error.append("in definition of " + expression + ": " +
                                        "\"xor\" does not follow an operator group");
                                return null;
                            }
//System.out.print("ADDING<xor>");
//for(String t : currXorGroup)
//    System.out.print(" " + t);
//System.out.println();
                            xorGroups.add(currXorGroup);
                            currXorGroup = new LinkedList<>();
                            mode = BinaryLeftMode.OP;
                        }
                    } else {
                        if(opSymbols.size() == 0) {
                            error.append("in definition of " + expression + ": " +
                                    "no operators specified");
                            return null;
                        } else if(!token.equals("sub")) {
                            error.append("in definition of " + expression + ": " +
                                    "no \"sub\" found after operators");
                            return null;
                        } else {
                            if(!xorGroups.isEmpty()) {
                                if(currXorGroup.isEmpty()) {
                                    error.append("in definition of " + expression + ": " +
                                            "\"xor\" does not precede an operator group");
                                    return null;
                                }
//System.out.print("ADDING<sub>");
//for(String t : currXorGroup)
//    System.out.print(" " + t);
//System.out.println();
                                xorGroups.add(currXorGroup);
                                currXorGroup = new LinkedList<>();
                            }
                            mode = BinaryLeftMode.SUBEXPRESSION;
                        }
                    }
                    break;
                }
                case OP_SYMBOL:
                {
                    opSymbol = token;
                    mode = BinaryLeftMode.OP_CONSTANT;
                    break;
                }
                case OP_CONSTANT:
                {
                    String opConstant = token;
                    if(opSymbols.contains(opSymbol)) {
                        error.append("in definition of " + expression + ": " +
                                "duplicate operator symbol: " + opSymbol);
                        return null;
                    }
                    opSymbols.add(opSymbol);
                    opConstants.add(opConstant);
                    currXorGroup.add(opConstant);
                    mode = BinaryLeftMode.OP;
                    break;
                }
                case SUBEXPRESSION:
                {
                    String subexpression = token;
                    out +=
                            "AbstractExpression " + expression + "P(BlockScope parent) :\n" +
                            "{\n"  +
                            "    AbstractExpression left;\n"  +
                            "    AbstractExpression right;\n"  +
                            "    BinaryExpression.Op type;\n";
                    boolean xor = !xorGroups.isEmpty();
                    if(xor)
                        out +=
                            "    List<BinaryExpression.Op> ops =\n" +
                            "        new LinkedList<BinaryExpression.Op>();\n";
                    out +=
                            "    StreamPos pos;\n";
                    if(xor) {
                        String sets =
                                "    final BinaryExpression.Op[][] SETS = {\n";
                        String names =
                                "    final Map<BinaryExpression.Op, String> OP_NAMES =\n" +
                                "        new HashMap<BinaryExpression.Op, String>();\n";
                        int count = 0;
                        for(List<String> group : xorGroups) {
                            out +=
                                    "    final BinaryExpression.Op[] SET_" + (count  + 1) + " = {\n";
//System.out.println("UUUU");
//for(String t : opSymbols)
//    System.out.println("op symbol=" + t);
                            for(String s : group) {
//System.out.println("op group=" + s);
                                out += "        " + s + ",\n";
                                int opCount = 0;
                                for(String t : opConstants) {
                                    if(s.equals(t))
                                        break;
                                    ++opCount;
                                }
                                String n = opSymbols.get(opCount);
                                names +=
                                        "    OP_NAMES.put(" + s + ", " + n + ");\n";
                            }
                            out +=
                                    "    };\n";
                            sets += "        SET_" + (count  + 1) + ",\n";
                            ++count;
                        }
                        sets +=
                                "    };\n";
                        out += sets;
                        out += names;
                    }
                    out +=
                            "}\n"  +
                            "{\n"  +
                            "    left = " + subexpression + "P(parent)\n" +
                            "    (\n";
                    for(String s : opSymbols)
                        if(s.equals("\"" + PrimitiveRangeDescription.VARIABLE_RIGHT_STRING + "\""))
                            // avoid a warning about a variable range's ">>"
                            // -- only partially helps the parser anyway, so a
                            // language with a ">>" operator can have
                            // the ranges only within comment tags, as
                            // these do not contain ">>", which is only
                            // later programmatically added for being
                            // parsed by a nested parser, and is thus the last
                            // token in the string
                            out += "        LOOKAHEAD(2)\n";
                    out +=
                            "        (\n";
                    for(int i = 0; i < opSymbols.size(); ++i) {
                        String symbol = opSymbols.get(i);
                        String constant = opConstants.get(i);
                        out +=
                                "            " + symbol + " { type = " + constant + "; }";
                        if(i < opSymbols.size() - 1)
                            out += " |";
                        out += "\n";
                    }
                    opSymbols.clear();
                    opConstants.clear();
                    xorGroups.clear();
                    out +=
                            "        )\n" +
                            "        { pos = getStreamPos(); }\n";
                    if(xor)
                        out +=
                                "        {\n" +
                                "            ops.add(type);\n" +
                                "            Hedgeelleth.checkUnspecifiedAssociativity(SETS, ops,\n" +
                                "                OP_NAMES);\n" +
                                "        }\n";
                    out +=
                            "        right = " + subexpression + "P(parent) {\n" +
                            "            left = new BinaryExpression(pos, parent,\n" +
                            "                type, left, right);\n" +
                            "        }\n" +
                            "    )*\n" +
                            "    {\n" +
                            "        return left;\n" +
                            "    }\n" +
                            "}\n";
                    mode = BinaryLeftMode.EXPRESSION;
                    break;
                }
            }
        }
        if(mode != BinaryLeftMode.EXPRESSION) {
            error.append("unexpected end of the definition of "  + expression);
            return null;
        }
        out = CompilerUtils.indent(1, out, "    ");
        return out;
    }
    /**
     * A helper enumeration for <code>generateMacroExpansion</code>.
     */
    enum MacroExpansionMode {
        NAME_KEYWORD,
        NAME,
        COLON,
        PARAM_NAME,
        PARAM_VALUE,
    }
    /**
     * Condition type within the #IF[NOT][NULL] blocks.
     */
    enum IfConditionType {
        /**
         * True condition of #IF ... #ENDIF block, block removed
         * if the condition is false.
         */
        TRUE,
        /**
         * False condition of #IFNOT ... #ENDIF block, block removed
         * if the condition is true.
         */
        FALSE,
        /**
         * Null condition of #IFNULL ... #ENDIF block, block removed
         * if the condition is not null.
         */
        NULL,
        /**
         * Null condition of #IFNOTNULL ... #ENDIF block, block removed
         * if the condition is null.
         */
        NOT_NULL,
    }
    /**
     * If the string is either "false" or "true".
     * 
     * @param s                         input string
     * @return                          if the string represents a boolean
     *                                  constant
     */
    static boolean isFalseOrTrue(String s) {
        return s.equals("false") || s.equals("true");
    }
    /**
     * Reduces a condition in a list, in a way depending on a boolean
     * operator.
     *
     * @param list                      list of tokens "false", "true" or
     *                                  <code>operator</code>
     * @param operator                  "&&" or "||"
     * @return                          if the condition could be parsed
     */
    static boolean reduceCondition(List<String> list, String operator) {
        for(int i = 1; i < list.size() - 1; ++i) {
            if(list.get(i).equals(operator)) {
                if(isFalseOrTrue(list.get(i - 1)) && isFalseOrTrue(list.get(i + 1))) {
                    boolean left = Boolean.parseBoolean(list.get(i - 1));
                    boolean right = Boolean.parseBoolean(list.get(i + 1));
                    boolean result;
                    if(operator.equals("&&"))
                        result = left && right;
                    else if(operator.equals("||"))
                        result = left || right;
                    else
                        throw new RuntimeException("unknown operator");
                    list.set(i, result ? "true" : "false");
                    list.remove(i - 1);
                    list.remove(i);
                    --i;
                } else
                    return false;
            } else if(list.get(i).equals("&&") || list.get(i).equals("||"))
                ++i;
            else
                return false;
        }
        return true;
    }
    /**
     * Simplified conditional conditions of the form
     * const ( ( "&&" | "||" ) const )*.
     *
     * @param in                        input condition
     * @return                          simplified condition
     */
    static String simplifyCondition(String in) {
        Scanner sc = CompilerUtils.newScanner(in);
        List<String> l = new ArrayList<String>();
        while(sc.hasNext()) {
            String t = sc.next();
            l.add(t);
        }
        if(!reduceCondition(l, "&&") ||
                !reduceCondition(l, "||"))
            return null;
        String out = "";
        if(l.size() > 1)
            return null;
        else
            return l.get(0);
    }
    /**
     * Processes macro contents, by replacing parameter names with
     * values and then deleting #IF ... #ENDIF blocks with false
     * conditions and #IFNOTNULL ... #ENDIF blocks with null conditions.
     * Tests if the parameters really occur in the file, and reports
     * an error otherwise.<br>
     *
     * Ends of lines in parameter values are treated as spaces.
     *
     * @param in                        contents of the macro
     * @param parameters                a map of parameter names to
     *                                  parameter values
     * @param error                     error message, empty for no
     *                                  error
     * @return                          processed contents of the macro
     *                                  or null if an error occured
     */
    static String processMacro(String in, Map<String, String> parameters,
            StringBuilder error) {
        error.setLength(0);
        // test and process parameters
        //
        // it is tested if there are keys that are absent in the
        // macro's text, and yhe processing replaces ends of lines
        // with spaces
        Map<String, String> p = new HashMap<String, String>();
        for(String key : parameters.keySet()) {
             int i = in.indexOf(key);
             if(i == -1) {
                 error.append("no such parameter name: " + key);
                 return null;
             } else {
                 String v = parameters.get(key);
                 String w = "";
                 for(int index = 0; index < v.length(); ++index) {
                     char c = v.charAt(index);
                     if(c == '\n')
                         c = ' ';
                     w = w + c;
                 }
                 p.put(key, w);
             }
        }
        parameters = p;
        // process parameters
        StringBuilder out = new StringBuilder();
        int index = 0;
        while(index < in.length()) {
            int endIndex = in.length();
            String name = null;
            for(String key : parameters.keySet()) {
                 int i = in.indexOf(key, index);
                 if(i != -1 && endIndex >= i) {
                     if(endIndex == i)
                         // select the longer matching parameter
                         if(name.length() > key.length())
                             key = name;
                     endIndex = i;
                     name = key;
                 }
            }
            out.append(in.substring(index, endIndex));
            if(endIndex == in.length())
                break;
            out.append(parameters.get(name));
            index = endIndex + name.length();
        }
        // process #IF ... #ENDIF blocks
        in = out.toString();
        out.setLength(0);
        index = 0;
        int nested = 0;
        final String IF = "#IF";
        final String ENDIF = "#ENDIF";
        // must have common prefix with <code>IF</code>
        // but be longer
        final String IFNOT = "#IFNOT";
        // must have common prefix with <code>IF</code>
        // but be longer
        final String IFNULL = "#IFNULL";
        // must have common prefix with <code>IFNOT</code>
        // but be longer
        final String IFNOTNULL = "#IFNOTNULL";
        // simple command for nesting macros, the nested macro
        // is called with the same parameter set as the current
        // macro, no possibility for definition of additional
        // parameters
        //
        // UNIMPLEMENTED
        final String INSERT = "#INSERT";
        int deleteNested = -1;
        while(index < in.length()) {
            String name = null;
            int ifI = in.indexOf(IF, index);
            if(ifI == -1)
                ifI = in.length();
            int endIfI = in.indexOf(ENDIF, index);
            if(endIfI == -1)
                endIfI = in.length();
            int insertI = in.indexOf(INSERT, index);
            if(insertI == -1)
                insertI = in.length();
            int endIndex = Math.min(ifI, Math.min(endIfI, insertI));
            if(deleteNested == -1)
                out.append(in.substring(index, endIndex));
            if(endIndex == in.length())
                break;
            int cut;
            IfConditionType conditionType;
            if(endIndex == insertI) {
                throw new RuntimeException("#INSERT not implemented");
            } else if(endIndex == ifI) {
                if(in.indexOf(IFNOTNULL, index) == ifI) {
                    cut = IFNOTNULL.length();
                    conditionType = IfConditionType.NOT_NULL;
                } else if(in.indexOf(IFNOT, index) == ifI) {
                    cut = IFNOT.length();
                    conditionType = IfConditionType.FALSE;
                } else if(in.indexOf(IFNULL, index) == ifI) {
                    cut = IFNULL.length();
                    conditionType = IfConditionType.NULL;
                } else {
                    cut = IF.length();
                    conditionType = IfConditionType.TRUE;
                }
                while(endIndex + cut < in.length() &&
                        Character.isWhitespace(in.charAt(endIndex + cut)))
                    ++cut;
                String condition = "";
                // condition is terminated by end of line
                char c;
                while(endIndex + cut < in.length() &&
                        (c = in.charAt(endIndex + cut)) != '\n') {
                    condition += c;
                    ++cut;
                }
                boolean deleteBlock = false;
                boolean invalidCondition = false;
                // System.out.println("***\n\n****CONDITION = \"" + condition + "\"");
                if(conditionType ==IfConditionType.TRUE) {
                    String reduced = simplifyCondition(condition);
                    if(reduced == null) {
                        error.append("in macro: invalid boolean condition, " +
                            "should be a valid boolean expression " +
                            "consisting of \"false\", " +
                            "\"true\", \"&&\" or \"||\", is \"" + condition + "\"");
                        return null;
                    }
                    condition = reduced;
                }
                if(condition.isEmpty()) {
                    error.append("in macro: missing condition after #IF");
                    return null;
                } else if(condition.equals("true")) {
                    switch(conditionType) {
                        case TRUE:
                            // empty
                            break;

                        case FALSE:
                            deleteBlock = true;
                            break;

                        default:
                            invalidCondition = true;
                            break;

                    }
                } else if(condition.equals("false")) {
                    switch(conditionType) {
                        case TRUE:
                            deleteBlock = true;
                            break;

                        case FALSE:
                            // empty
                            break;

                        default:
                            invalidCondition = true;
                            break;

                    }
                } else if(condition.equals("null")) {
                    switch(conditionType) {
                        case NULL:
                            // empty
                            break;

                        case NOT_NULL:
                            deleteBlock = true;
                            break;

                        default:
                            invalidCondition = true;
                            break;

                    }
                } else {
                    switch(conditionType) {
                        case NULL:
                            deleteBlock = true;
                            break;

                        case NOT_NULL:
                            // empty
                            break;

                        default:
                            invalidCondition = true;
                            break;

                    }
                }
                if(invalidCondition) {
                    error.append("in macro: invalid condition, for " +
                            IF + " and " + IFNOT + " must be a valid boolean expression " +
                            "consisting of \"false\", " +
                            "\"true\", \"&&\" or \"||\", for " +
                            IFNULL + " and " + IFNOTNULL + " must be neither \"false\" nor " +
                            "\"true\", is \"" + condition + "\" for block type " +
                            "#" + conditionType.toString());
                    return null;
                }
                if(deleteBlock) {
                    if(deleteNested == -1)
                        deleteNested = nested;
                }
                ++nested;
            } else {
                cut = ENDIF.length();
                if(--nested < 0) {
                    error.append("in macro: #ENDIF without #IF");
                    return null;
                }
                if(deleteNested == nested)
                    deleteNested = -1;
            }
            index = endIndex + cut;
        }
        return out.toString();
    }
    /**
     * Generates a macro expansion within a verbatim section.<br>
     *
     * Parameter values are read until a token beginning with <code>$</code>
     * is found or until the end of section. All internal whitespace sections
     * within a parameter value are changed to a single space. External
     * whitespace sections are trimmed.
     *
     * @param sectionType               section type
     * @param in                        string within the brackets following
     *                                  <code>HEDGEELLETH(MACRO)</code>
     * @param error                     empty if no error occured, otherwise contains
     *                                  the error's description
     * @return                          string with the generated macro expamsion,
     *                                  or null if an error occured
     */
    String generateMacroExpansion(SectionType sectionType, String in,
            StringBuilder error) {
        String out = "";
        error.setLength(0);
        Scanner sc = CompilerUtils.newScanner(in);
        MacroExpansionMode mode = MacroExpansionMode.NAME_KEYWORD;
        String name = null;
        String parameterName = null;
        Map<String, String> parameters = new HashMap<>();
        String token = null;
        boolean gotNextToken = false;
        while(sc.hasNext() || gotNextToken) {
            if(!gotNextToken) {
                token = sc.next();
            } else
                gotNextToken = false;
            // System.out.println("token = " + token + " mode = " + mode + " out = \n" + out);
            switch(mode) {
                case NAME_KEYWORD:
                {
                    if(!token.equals("name")) {
                        error.append("found \"" + token + "\" where \"name\" was expected");
                        return null;
                    } else
                        mode = MacroExpansionMode.NAME;
                    break;
                }
                case NAME:
                {
                    name = token;
                    mode = MacroExpansionMode.COLON;
                    break;
                }
                case COLON:
                {
                    if(!token.equals(":")) {
                        error.append("found \"" + token + "\" where \":\" was expected");
                        return null;
                    } else
                        mode = MacroExpansionMode.PARAM_NAME;
                    break;
                }
                case PARAM_NAME:
                {
                    parameterName = token;
                    if(parameterName.length() < 2 || parameterName.charAt(0) != '$') {
                        error.append("parameter name must be an identifier " +
                                "that begins with \"$\", is \"" + parameterName + "\"");
                        return null;
                    }
                    if(parameters.containsKey(parameterName)) {
                        error.append("duplicate parameter name: " + parameterName);
                        return null;
                    }
                    mode = MacroExpansionMode.PARAM_VALUE;
                    break;
                }
                case PARAM_VALUE:
                {
                    String parameterValue = "";
                    do {
                        if(!parameterValue.isEmpty())
                            parameterValue += " ";
                        parameterValue += token;
                        gotNextToken = false;
                        if(!sc.hasNext())
                            break;
                        token = sc.next();
                        gotNextToken = true;
                    } while(token.charAt(0) != '$');
                    parameters.put(parameterName, parameterValue);
                    mode = MacroExpansionMode.PARAM_NAME;
                    break;
                }
            }
        }
        if(mode != MacroExpansionMode.COLON &&
                mode != MacroExpansionMode.PARAM_NAME) {
            error.append("unexpected end of macro definition");
            return null;
        }
        String resourceName;
        if(name.charAt(0) == '/')
            resourceName = name;
        else
            resourceName = "/pl/gliwice/iitis/hedgeelleth/languagedescription/macro/" +
                    name;
        switch(sectionType) {
            case CODE:
                resourceName += ".code";
                break;

            case GRAMMAR:
                resourceName += ".grammar";
                break;

            default:
                throw new RuntimeException("unknown section type");
        }
        String macro;
        try {
            macro = CompilerUtils.readResourceFile(getClass(),
                resourceName);
        } catch(IOException e) {
            error.append("could not read macro resource: " + resourceName +
                    ": " + e.getMessage());
            return null;
        }
        out = processMacro(macro, parameters, error);
        if(out == null)
            return null;
        out = CompilerUtils.indent(1, out, "    ");
        return out;
    }
    /**
     * Processes a given verbatim section, and outputs the section
     * processed according to the language description. Throws an
     * exception if there is a parse error.
     *
     * @param sectionType               type of the section, used for
     *                                  detecting errors
     * @param section                   unprocessed verbatim section
     * @param streamName                name of the stream, usually file name,
     *                                  used only to generate parse exceptions
     * @param firstLineNum              line number of the beginning of the
     *                                  section section in the original
     *                                  Hedgeelleth file, counting from 1,
     *                                  used to generate parse exceptions
     * @return                          processed verbatim section section
     */
    @SuppressWarnings("empty-statement")
    public String process(SectionType sectionType, VerbatimSection section)
            throws ParseException {
        String in = section.text;
        String streamName = section.streamName;
        int firstLineNum = section.firstLineNum;
        LineIndex li = new LineIndex(in);
        StringBuilder out = new StringBuilder();
        int index = 0;
        while(index < in.length()) {
            int newIndex = in.indexOf(KEYWORD, index);
            int endIndex;
            if(newIndex != -1)
                endIndex = newIndex;
            else
                endIndex = in.length();
            out.append(in.substring(index, endIndex));
            if(newIndex == -1)
                break;
            index = newIndex + KEYWORD.length();
            while(Character.isWhitespace(in.charAt(index)))
                ++index;
            if(in.charAt(index) != '(')
                throw new ParseException(li.getDescription(
                        streamName, index, firstLineNum, 1),
                        ParseException.Code.PARSE, "`(' expected");
            int openIndex = index + 1;
            index = in.indexOf(')', openIndex);
            if(index == -1)
                throw new ParseException(li.getDescription(
                        streamName, openIndex, firstLineNum, 1),
                        ParseException.Code.PARSE, "matching `)' not found");
            int closeIndex = index;
            String arg = in.substring(openIndex, closeIndex).trim();
            boolean sectionMismatch = false;
            if(arg.equals("TYPE")) {
                if(sectionType != SectionType.GRAMMAR)
                    sectionMismatch = true;
                else
                    out.append(generateTypeProduction());
            } else if(arg.equals("PRIMITIVE_TYPE")) {
                if(sectionType != SectionType.GRAMMAR)
                    sectionMismatch = true;
                else
                    out.append(generatePrimitiveTypeProduction());
            } else if(arg.equals("LITERAL")) {
                if(sectionType != SectionType.GRAMMAR)
                    sectionMismatch = true;
                else
                    out.append(generateLiteralProduction());
            } else if(arg.equals("BINARY_LEFT")) {
                if(sectionType != SectionType.GRAMMAR)
                    sectionMismatch = true;
                else {
                    int[] endPos = new int[1];
                    String block = getBlock(in, closeIndex + 1, endPos);
                    if(block == null)
                        throw new ParseException(li.getDescription(
                                streamName, closeIndex + 1, firstLineNum, 1),
                                ParseException.Code.MISSING,
                                "block to define the expressions expected");
                    index = endPos[0];
                    StringBuilder error = new StringBuilder();
                    String production = generateBinaryLeftProduction(block, error);
                    if(production == null)
                        throw new ParseException(li.getDescription(
                                streamName, closeIndex + 1, firstLineNum, 1),
                                ParseException.Code.INVALID,
                                "invalid block: " + error.toString());
                    else
                        out.append(production);
                }
            } else if(arg.equals("MACRO")) {
                int[] endPos = new int[1];
                String block = getBlock(in, closeIndex + 1, endPos);
                if(block == null)
                    throw new ParseException(li.getDescription(
                            streamName, closeIndex + 1, firstLineNum, 1),
                            ParseException.Code.MISSING,
                            "block to define the macro expected");
                index = endPos[0];
                StringBuilder error = new StringBuilder();
                String production = generateMacroExpansion(sectionType,
                        block, error);
                if(production == null)
                    throw new ParseException(li.getDescription(
                            streamName, closeIndex + 1, firstLineNum, 1),
                            ParseException.Code.INVALID,
                            "invalid block: " + error.toString());
                else
                    out.append(production);
            } else
                throw new ParseException(li.getDescription(
                        streamName, openIndex, firstLineNum, 1),
                        ParseException.Code.INVALID,
                        "unknown argument: " + arg);
            if(sectionMismatch)
                throw new ParseException(li.getDescription(
                        streamName, openIndex, firstLineNum, 1),
                        ParseException.Code.ILLEGAL,
                        "argument " + arg + " can not be used in " +
                        "the verbatim " + sectionType.getName() +
                        " section");
            ++index;
        }
        return out.toString();
    }
    public static void main(String[] args) throws IOException {
        /*
        String resource =
                "/pl/gliwice/iitis/hedgeelleth/languagedescription/macro/" +
                "StatementExpressionList.grammar";
        String in = CompilerUtilities.readResourceFile(
                new VerbatimSectionBuilder(null).getClass(),
                resource);
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("$allowMultipleStatements", "true");
        StringBuilder error = new StringBuilder();
        System.out.println(processMacro(in, parameters, error));
        System.out.println(error.toString());
         */
        simplifyCondition("true && false || true && true");
    }
}
