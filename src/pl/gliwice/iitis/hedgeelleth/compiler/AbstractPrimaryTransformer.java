/*
 * AbstractPrimaryTransformer.java
 *
 * Created on Aug 1, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */
package pl.gliwice.iitis.hedgeelleth.compiler;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.PrimaryExpression;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.ParseException;

/**
 * An abstract transformer of primary expressions.
 * 
 * @author Artur Rataj
 */
public abstract class AbstractPrimaryTransformer {
    /**
     * Transforms a primary expression.
     * 
     * @param e expression to parse
     */
    abstract public void transform(PrimaryExpression e) throws ParseException;
}
