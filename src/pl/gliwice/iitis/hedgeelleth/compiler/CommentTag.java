/*
 * CompilerTag.java
 *
 * Created on Jul 10, 2008
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * A comment tag. Represented by name and a set of modifiers.<br>
 *
 * A modifier consists of a prefix and arguments, which are separated
 * by the first whitespace. The prefix can not be null. The arguments
 * can contain whitespace, and be an empty string.<br>
 *
 * If a prefix begins
 * with a dot, it denotes one of built-in compiler modifiers, see
 * <code>CommentTag.CompilerModifier</code> for details. Otherwise,
 * the modifier is parsed by backend only.
 * 
 * @author Artur Rataj
 */
public class CommentTag implements SourcePosition {
    /**
     * Compiler modifiers. Compiler modifiers are always preceeded with
     * ".".
     */
    public enum CompilerModifier {
        /**
         * Apply recursively when inlining, 0 to tag only the root method call
         */
        INLINE,
        /**
         * Requires a given tag.
         */
        // REQUIRES_TAG,
        /**
         * If requires a given annotation on AbstractCodeOp.tags,
         * and is to be filtered out otherwise.
         */
        REQUIRES_ANNOTATION,
        /** 
         * Requires absence of a given tag.
         */
        // REQUIRES_NO_TAG,
        /**
         * Requires absence of a given annotation.
         */
        // REQUIRES_NO_ANNOTATION
        /**
         * Adds CodeOpNone at the beginning of a method, and assigns
         * a given tag to the operation.
         */
        // HEAD,
        /**
         * Adds CodeOpNone at each end of a method, and assigns
         * a given tag to the operation.
         */
        // TAIL,
        ;
        
        /**
         * Arguments of this compiler tag, empy string for no arguments.
         */
        public String arguments = "";
    };
    /**
     * Name of INLINE compiler modifier.
     */
    public static final String MODIFIER_INLINE_STRING = "inline";
    /**
     * Name of REQUIRES_TAG compiler modifier.
     */
    //public static final String _REQUIRES_TAG_STRING = "+tag";
    /**
     * Name of REQUIRES_ANNOTATION compiler modifier.
     */
    public static final String MODIFIER_REQUIRES_ANNOTATION_STRING = "annotation";
    /**
     * Name of REQUIRES_NO_TAG compiler modifier.
     */
    //public static final String _REQUIRES_NO_TAG_STRING = "-tag";
    /**
     * Name of REQUIRES_NO_ANNOTATION compiler modifier.
     */
    //public static final String REQUIRES_NO_ANNOTATION_STRING = "-annotation";
    /**
     * Name of HEAD generate tag.
     */
    public static final String TAG_GENERATE_HEAD_STRING = "generateHead";
    /**
     * Name of TAIL generate tag.
     */
    public static final String TAG_GENERATE_TAIL_STRING = "generateTail";
    
    /**
     * Constant for <code>inlineRecursionPolicy</code>, see the field's comment
     * for details.
     */
    public static final int INLINE_RECURSION_FIRST = -2;
    /**
     * Constant for <code>inlineRecursionPolicy</code>, see the field's comment
     * for details.
     */
    public static final int INLINE_RECURSION_INFINITE = -3;
    /**
     * Name of argument INLINE_RECURSION_FIRST of the modifier INLINE.
     */
    public static final String INLINE_RECURSION_FIRST_STRING = "first";
    /**
     * Name of argument INLINE_RECURSION_INFINITE of the modifier INLINE.
     */
    public static final String INLINE_RECURSION_INFINITE_STRING = "infinite";
    
    /**
     * Name of this tag.
     */
    public String name;
    /**
     * Modifiers.
     */
    public List<String> modifiers;
    /**
     * Number of inlined calls left to recursively apply this tag on
     * inlined operations. <code>INLINE_RECURSION_FIRST</code> for only
     * the first operation and the topmost inlined method,
     * <code>INLINE_RECURSION_INFINITE</code> for infinite recursion.
     * Value of 0 to delete the tag together with the inlined call,
     * value of 1 means apply only to the topmost inlined method, etc.
     */
    public int inlineRecursionPolicy;
    /**
     * Annotations of an operation required by this tag.
     */
    public Set<String> annotationsRequired;
    /**
     * Annotations of an operation that prevent this tag.
     */
    /*
    HashSet<String> annotationsForbidden;
     */
    /**
     * 0 for an original tag, subsequent values for subsequent recursive
     * applications of the tag into inlined methods.
     */
    public int recursiveInlineApplyCounter;
    /**
     * Raw text of the tag in the input stream.
     */
    public String rawText;
    /**
     * Position of the <code>rawText</code> in the parsed stream.
     */
    protected StreamPos rawTextPos;
    
    /**
     * Creates a new instance of CompilerTag, with an empty list of
     * modifiers.
     * 
     * Default properties: inline recursion is
     * <code>INLINE_RECURSION_FIRST</code>.
     * 
     * @param name                      name of this tag.
     * @param rawText                   the tag's raw text
     * @param pos                       position of the raw tag's text
     *                                  in the parsed stream
     */
    public CommentTag(String name, String rawText, StreamPos rawTextPos) {
        this.name = name;
        modifiers = new LinkedList<>();
        inlineRecursionPolicy = INLINE_RECURSION_FIRST;
        recursiveInlineApplyCounter = 0;
        annotationsRequired = new HashSet<>();
        /*
        annotationsForbidden = new HashSet<String>();
         */
        this.rawText = rawText;
        setStreamPos(rawTextPos);
    }
    /**
     * A copying constructor.
     * 
     * @param tag                       tag to copy
     */
    public CommentTag(CommentTag tag) {
        name = tag.name;
        modifiers = new LinkedList<>(tag.modifiers);
        inlineRecursionPolicy = tag.inlineRecursionPolicy;
        recursiveInlineApplyCounter = tag.recursiveInlineApplyCounter;
        annotationsRequired = new HashSet<>(tag.annotationsRequired);
        rawText = tag.rawText;
        setStreamPos(tag.getStreamPos());
    }
    @Override
    public final void setStreamPos(StreamPos pos) {
        this.rawTextPos = pos;
    }
    @Override
    public final StreamPos getStreamPos() {
        return rawTextPos;
    }
    /**
     * Returns a modifier's prefix, that is, the first word in the modifier,
     * where separator is whitespace.
     *
     * @param modifier                  modifier, whose prefix to find
     * @return                          the modifier's prefix
     */
    public static String getModifierPrefix(String modifier) {
        String prefix = "";
        int pos = 0;
        char c;
        while(pos < modifier.length() &&
                !Character.isWhitespace(c = modifier.charAt(pos++)))
            prefix = prefix + c;
        return prefix;
    }
    /**
     * Returns a modifier's arguments, that is, everything after
     * the first word in the modifier, where separator is whitespace,
     * trimmed.
     *
     * @param modifier                  modifier, whose arguments to find
     * @return                          the modifier's arguments
     */
    @SuppressWarnings("empty-statement")
    public static String getModifierArguments(String modifier) {
        int pos = 0;
        while(pos < modifier.length() &&
                !Character.isWhitespace(modifier.charAt(pos++)))
            ;
        return modifier.substring(pos).trim();
    }
    /**
     * Tests if a modifier is a compiler modifier and returns the modifier
     * if true.
     * 
     * Throws an exception if the modifier begins with `.' but is not
     * recognized as a compiler modifier.
     * 
     * @param modifier                  modifier to test
     * @return                          compiler modifier, null for none
     */
    public static CompilerModifier getCompilerModifier(String modifier)
            throws ParseException {
        CompilerModifier m = null;
        if(modifier.indexOf('.') == 0) {
            String prefix = getModifierPrefix(modifier).substring(1);
            if(prefix.equals(MODIFIER_INLINE_STRING))
                m = CompilerModifier.INLINE;
            /*
            else if(prefix.equals(REQUIRES_TAG_STRING))
                m = CompilerModifier.REQUIRES_TAG;
             */
            else if(prefix.equals(MODIFIER_REQUIRES_ANNOTATION_STRING))
                m = CompilerModifier.REQUIRES_ANNOTATION;
            /*
            else if(prefix.equals(REQUIRES_NO_TAG_STRING))
                m = CompilerModifier.REQUIRES_NO_TAG;
            else if(prefix.equals(REQUIRES_NO_ANNOTATION_STRING))
                m = CompilerModifier.REQUIRES_NO_ANNOTATION;
             */
            /*
            else if(prefix.equals(HEAD_STRING))
                m = CompilerModifier.HEAD;
            else if(prefix.equals(TAIL_STRING))
                m = CompilerModifier.TAIL;
             */
            else
                throw new ParseException(null, ParseException.Code.INVALID,
                        "." + modifier + "begins with `.' " +
                        "but not recognized as a compiler modifier");
            if(m != null)
                m.arguments = getModifierArguments(modifier);
        }
        return m;
    }
    /**
     * Adds a modifier. If it is a compiler modifier, also respective properties
     * of this tag are updated.
     * 
     * @param modifier                  modifier to add
     */
    public void addModifier(String modifier) throws ParseException {
        modifiers.add(modifier);
        CompilerModifier cm = getCompilerModifier(modifier);
        if(cm != null) {
            switch(cm) {
                case INLINE:
                {
                    try {
                        String s = cm.arguments;
                        if(s.equals(INLINE_RECURSION_FIRST_STRING))
                            inlineRecursionPolicy = INLINE_RECURSION_FIRST;
                        else if(s.equals(INLINE_RECURSION_INFINITE_STRING))
                            inlineRecursionPolicy = INLINE_RECURSION_INFINITE;
                        else
                            inlineRecursionPolicy = Integer.parseInt(s);
                    } catch(NumberFormatException e) {
                        throw new ParseException(null, ParseException.Code.PARSE,
                                "modifier ." + MODIFIER_INLINE_STRING +
                                " requires one of: <recursion level>, " +
                                INLINE_RECURSION_FIRST_STRING + ", " +
                                INLINE_RECURSION_INFINITE_STRING);
                    }
                    break;
                }
                case REQUIRES_ANNOTATION:
                /*
                case REQUIRES_NO_ANNOTATION:
                     */
                {
                    if(cm.arguments.indexOf('@') != 0)
                        throw new ParseException(null, ParseException.Code.MISSING,
                                "modifier " + modifier +
                                " does not define annotation that begins with " +
                                "`@' or `@@'");
                    String s = cm.arguments.substring(1);
                    switch(cm) {
                        case REQUIRES_ANNOTATION:
                            annotationsRequired.add(s);
                            break;
                        /*
                        case REQUIRES_NO_ANNOTATION:
                            annotationsForbidden.add(s);
                            break;
                         */
                            
                    }
                    break;
                }

                default:
                    throw new RuntimeException("unknown compiler modifier: " + cm);
                /*
                case HEAD:
                {
                }
                case TAIL:
                {
                }
                 */
            }
        }
    }
    /**
     * Returns the whole contents from inside the parentheses.
     * Typically used only for comment tags that do not have modifiers.
     * 
     * @return a substring of <code>rawText</code>
     */
    public String getArguments() {
        return rawText.substring(rawText.indexOf('(') + 1,
                rawText.length() - 1);
    }
    @Override
    public String toString() {
        String s = "@" + name + "(";
        for(int i = 0; i < modifiers.size(); ++i) {
            if(i != 0)
                s += ", ";
            s += modifiers.get(i);
        }
        s += ")";
        return s;
    }
}
