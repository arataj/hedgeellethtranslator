/*
 * CompilerAnnotation.java
 *
 * Created on Apr 22, 2008
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler;

import java.util.*;

/**
 * Contains values of standard annotations of the compiler.
 * 
 * @author Artur Rataj
 */
public enum CompilerAnnotation {
    UNKNOWN_ANNOTATION,
    UNKNOWN_COMPILER_ANNOTATION,
    IGNORE,
    MARKER,
    // NEW_THREAD,
    NOTIFY,
    NOTIFY_ALL,
    RANDOM,
    SLEEP,
    INTERRUPT,
    START_THREAD,
    WAIT,
    JOIN,
    BARRIER_ACTIVATE,
    // symmetric barrier without a condition
    BARRIER_VOID,
    // symmetric barrier with a direct boolean condition
    BARRIER_CONDITIONAL,
    // symmetric barrier with a reference to a boolean closure
    BARRIER_CLOSURE,
    // asymmetric barrier, producer side, without a condition
    BARRIER_PRODUCE_VOID,
    // asymmetric barrier, producer side, with a direct boolean condition
    BARRIER_PRODUCE_CONDITIONAL,
    // asymmetric barrier, producer side, with a reference to a boolean closure
    BARRIER_PRODUCE_CLOSURE,
    // asymmetric barrier, consumer side, without a condition
    BARRIER_CONSUME_VOID,
    // asymmetric barrier, consumer side, with a direct boolean condition
    BARRIER_CONSUME_CONDITIONAL,
    // asymmetric barrier, consumer side, with a reference to a boolean closure
    BARRIER_CONSUME_CLOSURE,
    // symmetric pair barrier without a condition
    PAIR_BARRIER_VOID,
    // symmetric pair barrier with a direct boolean condition
    PAIR_BARRIER_CONDITIONAL,
    // symmetric pair barrier with a reference to a boolean closure
    PAIR_BARRIER_CLOSURE,
    // asymmetric pair barrier, producer side, without a condition
    PAIR_BARRIER_PRODUCE_VOID,
    // asymmetric pair barrier, producer side, with a direct boolean condition
    PAIR_BARRIER_PRODUCE_CONDITIONAL,
    // asymmetric pair barrier, producer side, with a reference to a boolean closure
    PAIR_BARRIER_PRODUCE_CLOSURE,
    // asymmetric pair barrier, consumer side, without a condition
    PAIR_BARRIER_CONSUME_VOID,
    // asymmetric pair barrier, consumer side, without a condition, chosen producer
    PAIR_BARRIER_CONSUME_VOID_PRODUCER,
    // asymmetric pair barrier, consumer side, with a direct boolean condition
    PAIR_BARRIER_CONSUME_CONDITIONAL,
    // asymmetric pair barrier, consumer side, with a reference to a boolean closure
    PAIR_BARRIER_CONSUME_CLOSURE,
    HEAD,
    TAIL,
    STATE,
    ARRAY_COPY,
    MATH_ABS,
    MATH_MIN,
    MATH_MAX,
    MATH_SQRT,
    MATH_SIN,
    MATH_COS,
    MATH_ASIN,
    MATH_ACOS,
    MATH_FLOOR,
    MATH_CEIL,
    MATH_ROUND,
    MATH_POW,
    DIST_UNI,
    DIST_NXP,
    DIST_ARRAY,
    CLOSURE_BOOLEAN,
    CLOSURE_DOUBLE,
    MODEL_NAME,
    MODEL_CHECK,
    MODEL_STATE_OR,
    MODEL_STATE_AND,
    MODEL_PLAYER,
    MODEL_IS_STATISTICAL,
    MODEL_GET_INT_CONST,
    MODEL_GET_LONG_CONST,
    MODEL_GET_DOUBLE_CONST,
    INTERPRETER_STOP,
    NEW_FILE_FILENAME,
    FILE_EXISTS,
    FILE_DELETE,
    NEW_RANDOM_ACCESS_FILE_FILE,
    NEW_RANDOM_ACCESS_FILE_FILENAME,
    RANDOM_ACCESS_FILE_CLOSE,
    RANDOM_ACCESS_FILE_READ_BYTE,
    RANDOM_ACCESS_FILE_READ_ARRAY,
    RANDOM_ACCESS_FILE_WRITE_BYTE,
    RANDOM_ACCESS_FILE_WRITE_ARRAY,
    RANDOM_ACCESS_FILE_LENGTH,
    RANDOM_ACCESS_FILE_SEEK,
    PRINT,
    PRINTLN,
    IO_NEW_TUPLES,
    IO_NEW_MATRIX,
    IO_GET_ROW_INDEX,
    IO_GET_NEXT,
    IO_ADD_NEXT,
    IO_CLOSE,
    SUBSTRING,
    _ERROR;

    /**
     * Like a marker method in the interpreter threads, but removed
     * in the PTA threads.
     */
    public static final String IGNORE_STRING = "@IGNORE";
    /**
     * A marker method, do not execute.
     */
    public static final String MARKER_STRING = "@MARKER";
    // /**
    //    * A new thread object.
    //    */
    // public static final String NEW_THREAD_STRING = "@NEW_THREAD";
    /**
     * Notify.
     */
    public static final String NOTIFY_STRING = "@NOTIFY";
    /**
     * Notify all.
     */
    public static final String NOTIFY_ALL_STRING = "@NOTIFY_ALL";
    /**
     * Random value.
     */
    public static final String RANDOM_STRING = "@RANDOM";
    /**
     * Sleep.
     */
    public static final String SLEEP_STRING = "@SLEEP";
    /**
     * Interrupt. Allowed only in the main thread, but not allowed
     * to be actually interpreted.
     */
    public static final String INTERRUPT_STRING = "@INTERRUPT";
    /**
     * Start of a thread.
     */
    public static final String START_THREAD_STRING = "@START_THREAD";
    /**
     * Wait.
     */
    public static final String WAIT_STRING = "@WAIT";
    /**
     * Acctivation of a prism--style synchronization barrier.
     */
    public static final String BARRIER_ACTIVATE_STRING = "@BARRIER_ACTIVATE";
    /**
     * Prism--style symmetric synchronization barrier, without a pass condition.
     */
    public static final String BARRIER_VOID_STRING = "@BARRIER_VOID";
    /**
     * Prism--style symmetric synchronization barrier, with a direct, boolean pass condition.
     */
    public static final String BARRIER_CONDITIONAL_STRING = "@BARRIER_CONDITIONAL";
    /**
     * Prism--style symmetric synchronization barrier, with a pass condition within a
     * boolean closure.
     */
    public static final String BARRIER_CLOSURE_STRING = "@BARRIER_CLOSURE";
    /**
     * Prism--style asymmetric, producer side, synchronization barrier, without a pass condition.
     */
    public static final String BARRIER_PRODUCE_VOID_STRING = "@BARRIER_PRODUCE_VOID";
    /**
     * Prism--style asymmetric, producer side, synchronization barrier, with a direct, boolean pass condition.
     */
    public static final String BARRIER_PRODUCE_CONDITIONAL_STRING = "@BARRIER_PRODUCE_CONDITIONAL";
    /**
     * Prism--style asymmetric, producer side, synchronization barrier, with a pass condition within a
     * boolean closure.
     */
    public static final String BARRIER_PRODUCE_CLOSURE_STRING = "@BARRIER_PRODUCE_CLOSURE";
    /**
     * Prism--style asymmetric, consumer side, synchronization barrier, without a pass condition.
     */
    public static final String BARRIER_CONSUME_VOID_STRING = "@BARRIER_CONSUME_VOID";
    /**
     * Prism--style asymmetric, consumer side, synchronization barrier, with a direct, boolean pass condition.
     */
    public static final String BARRIER_CONSUME_CONDITIONAL_STRING = "@BARRIER_CONSUME_CONDITIONAL";
    /**
     * Prism--style asymmetric, consumer side, synchronization barrier, with a pass condition within a
     * boolean closure.
     */
    public static final String BARRIER_CONSUME_CLOSURE_STRING = "@BARRIER_CONSUME_CLOSURE";
    /**
     * Pair synchronization symmetric barrier, without a pass condition.
     */
    public static final String PAIR_BARRIER_VOID_STRING = "@PAIR_BARRIER_VOID";
    /**
     * Pair synchronization symmetric barrier, with a direct, boolean pass condition.
     */
    public static final String PAIR_BARRIER_CONDITIONAL_STRING = "@PAIR_BARRIER_CONDITIONAL";
    /**
     * Pair synchronization symmetric barrier, with a pass condition within a
     * boolean closure.
     */
    public static final String PAIR_BARRIER_CLOSURE_STRING = "@PAIR_BARRIER_CLOSURE";
    /**
     * Pair synchronization asymetric, producer side, synchronization barrier, without a pass condition.
     */
    public static final String PAIR_BARRIER_PRODUCE_VOID_STRING = "@PAIR_BARRIER_PRODUCE_VOID";
    /**
     * Pair synchronization asymmetric, producer side, synchronization barrier, with a direct, boolean pass condition.
     */
    public static final String PAIR_BARRIER_PRODUCE_CONDITIONAL_STRING = "@PAIR_BARRIER_PRODUCE_CONDITIONAL";
    /**
     * Pair synchronization asymmetric, producer side, synchronization barrier, with a pass condition within a
     * boolean closure.
     */
    public static final String PAIR_BARRIER_PRODUCE_CLOSURE_STRING = "@PAIR_BARRIER_PRODUCE_CLOSURE";
    /**
     * Pair synchronization asymmetric, consumer side, synchronization barrier, without a pass condition.
     */
    public static final String PAIR_BARRIER_CONSUME_VOID_STRING = "@PAIR_BARRIER_CONSUME_VOID";
    /**
     * Pair synchronization asymmetric, consumer side, synchronization barrier, without a pass condition.
     * One selected producer to synchronise with.
     */
    public static final String PAIR_BARRIER_CONSUME_VOID_PRODUCER_STRING = "@PAIR_BARRIER_CONSUME_VOID_PRODUCER";
    /**
     * Pair synchronization asymmetric, consumer side, synchronization barrier, with a direct, boolean pass condition.
     */
    public static final String PAIR_BARRIER_CONSUME_CONDITIONAL_STRING = "@PAIR_BARRIER_CONSUME_CONDITIONAL";
    /**
     * Pair synchronization asymmetric, consumer side, synchronization barrier, with a pass condition within a
     * boolean closure.
     */
    public static final String PAIR_BARRIER_CONSUME_CLOSURE_STRING = "@PAIR_BARRIER_CONSUME_CLOSURE";
    /**
     * Join.
     */
    public static final String JOIN_STRING = "@JOIN";
    /**
     * Head, added to operations generated by HEAD tag.
     */
    public static final String HEAD_STRING = "@HEAD";
    /**
     * Tail, added to operations generated by TAIL tag.
     */
    public static final String TAIL_STRING = "@TAIL";
    /**
     * A generic annotation used just to ensure,
     * that a given <code>CodeOpNone</code> won't
     * be optimized out.
     */
    public static final String STATE_STRING = "@STATE";
    /**
     * Array copy, arguments like in <code>System.arraycopy()</code>.
     */
    public static final String ARRAY_COPY_STRING = "@ARRAY_COPY";
    /**
     * Math.abs() for int, long and double.
     */
    public static final String MATH_ABS_STRING = "@MATH_ABS";
    /**
     * Math.min() for int, long and double.
     */
    public static final String MATH_MIN_STRING = "@MATH_MIN";
    /**
     * Math.max() for int, long and double.
     */
    public static final String MATH_MAX_STRING = "@MATH_MAX";
    /**
     * Math.sqrt().
     */
    public static final String MATH_SQRT_STRING = "@MATH_SQRT";
    /**
     * Math.sin().
     */
    public static final String MATH_SIN_STRING = "@MATH_SIN";
    /**
     * Math.cos().
     */
    public static final String MATH_COS_STRING = "@MATH_COS";
    /**
     * Math.asin().
     */
    public static final String MATH_ASIN_STRING = "@MATH_ASIN";
    /**
     * Math.acos().
     */
    public static final String MATH_ACOS_STRING = "@MATH_ACOS";
    /**
     * Math.floor().
     */
    public static final String MATH_FLOOR_STRING = "@MATH_FLOOR";
    /**
     * Math.ceil().
     */
    public static final String MATH_CEIL_STRING = "@MATH_CEIL";
    /**
     * Math.round().
     */
    public static final String MATH_ROUND_STRING = "@MATH_ROUND";
    /**
     * Math.pow().
     */
    public static final String MATH_POW_STRING = "@MATH_POW";
    /**
     * Dist.uni().
     */
    public static final String DIST_UNI_STRING = "@DIST_UNI";
    /**
     * Dist.nxp().
     */
    public static final String DIST_NXP_STRING = "@DIST_NXP";
    /**
     * Dist.array().
     */
    public static final String DIST_ARRAY_STRING = "@DIST_ARRAY";
    /**
     * A boolean closure's getter method.
     */
    public static final String CLOSURE_BOOLEAN_STRING = "@BOOLEAN_CLOSURE";
    /**
     * A double closure's getter method.
     */
    public static final String CLOSURE_DOUBLE_STRING = "@DOUBLE_CLOSURE";
    /**
     * A model's name method.
     */
    public static final String MODEL_NAME_STRING = "@MODEL_NAME";
    /**
     * A model's named property method.
     */
    public static final String MODEL_CHECK_STRING = "@MODEL_CHECK";
    /**
     * A model's state method, taht uses the OR operator.
     */
    public static final String MODEL_STATE_OR_STRING = "@MODEL_STATE_OR";
    /**
     * A model's state method, taht uses the AND operator.
     */
    public static final String MODEL_STATE_AND_STRING = "@MODEL_STATE_AND";
    /**
     * A model's player method.
     */
    public static final String MODEL_PLAYER_STRING = "@MODEL_PLAYER";
    /**
     * A model's isStatistical method.
     */
    public static final String MODEL_IS_STATISTICAL_STRING = "@MODEL_IS_STATISTICAL";
    /**
     * A model's intConst method.
     */
    public static final String MODEL_GET_INT_CONST_STRING = "@MODEL_GET_INT_CONST";
    /**
     * A model's doubleConst method.
     */
    public static final String MODEL_GET_DOUBLE_CONST_STRING = "@MODEL_GET_DOUBLE_CONST";
    /**
     * A model's waitFinish method.
     */
    public static final String INTERPRETER_STOP_STRING = "@INTERPRETER_STOP";
    /**
     * File(String).
     */
    public static final String NEW_FILE_FILENAME_STRING =
            "@NEW_FILE_FILENAME";
    /**
     * File.exists().
     */
    public static final String FILE_EXISTS_STRING =
            "@FILE_EXISTS";
    /**
     * File.delete().
     */
    public static final String FILE_DELETE_STRING =
            "@FILE_DELETE";
    /**
     * RandomAccessFile(File, String).
     */
    public static final String NEW_RANDOM_ACCESS_FILE_FILE_STRING =
            "@NEW_RANDOM_ACCESS_FILE_FILE";
    /**
     * RandomAccessFile(String, String).
     */
    public static final String NEW_RANDOM_ACCESS_FILE_FILENAME_STRING =
            "@NEW_RANDOM_ACCESS_FILE_FILENAME";
    /**
     * RandomAccessFile.close().
     */
    public static final String RANDOM_ACCESS_FILE_CLOSE_STRING =
            "@RANDOM_ACCESS_FILE_CLOSE";
    /**
     * int RandomAccessFile.read().
     */
    public static final String RANDOM_ACCESS_FILE_READ_BYTE_STRING =
            "@RANDOM_ACCESS_FILE_READ_BYTE";
    /**
     * int RandomAccessFile.read(array, offset, length).
     */
    public static final String RANDOM_ACCESS_FILE_READ_ARRAY_STRING =
            "@RANDOM_ACCESS_FILE_READ_ARRAY";
    /**
     * RandomAccessFile.write(int).
     */
    public static final String RANDOM_ACCESS_FILE_WRITE_BYTE_STRING =
            "@RANDOM_ACCESS_FILE_WRITE_BYTE";
    /**
     * RandomAccessFile.write(array, offset, length).
     */
    public static final String RANDOM_ACCESS_FILE_WRITE_ARRAY_STRING =
            "@RANDOM_ACCESS_FILE_WRITE_ARRAY";
    /**
     * long RandomAccessFile.length().
     */
    public static final String RANDOM_ACCESS_FILE_LENGTH_STRING =
            "@RANDOM_ACCESS_FILE_LENGTH";
    /**
     * RandomAccessFile.seek(long).
     */
    public static final String RANDOM_ACCESS_FILE_SEEK_STRING =
            "@RANDOM_ACCESS_FILE_SEEK";
    /**
     * OutputStream.print(String)
     */
    public static final String PRINT_STRING =
            "@PRINT";
    /**
     * OutputStream.println(String)
     */
    public static final String PRINTLN_STRING =
            "@PRINTLN";
    /**
     * new TuplesIO(String)
     */
    public static final String IO_NEW_TUPLES_STRING =
            "@IO_NEW_TUPLES";
    /**
     * new MatrixIO(String)
     */
    public static final String IO_NEW_MATRIX_STRING =
            "@IO_NEW_MATRIX";
    /**
     * double[] MatrixIO.getRowIndex()
     */
    public static final String IO_GET_ROW_INDEX_STRING =
            "@IO_GET_ROW_INDEX";
    /**
     * double[] MatrixIO.getNext()
     */
    public static final String IO_GET_NEXT_STRING =
            "@IO_GET_NEXT";
    /**
     * void MatrixIO.addNext(double[])
     */
    public static final String IO_ADD_NEXT_STRING =
            "@IO_ADD_NEXT";
    /**
     * MatrixIO.close()
     */
    public static final String IO_CLOSE_STRING =
            "@IO_CLOSE";
    /**
     * verics.lang.String.sub(start, stop)
     */
    public static final String SUBSTRING_STRING =
            "@SUBSTRING";
    /**
     * A generic error annotation. No thread can go past an operation
     * with this annotation.
     */
    public static final String _ERROR_STRING =
            "@ERROR";

    /**
     * Avoid instantiation.
     */
    private CompilerAnnotation() {
        /* empty */
    }
    /**
     * Returns the annotation or UNKNOWN_ANNOTATION if the input
     * string is not equal to any of the annotations strings in
     * this class and does not begin with `@'. If the string begins
     * with `@' as the compiler annotations, but is not recognized,
     * UNKNOWN_COMPILER_ANNOTATION is returned.
     * 
     * @param annotation                string whose contents
     *                                  is tested
     * @return                          annotation
     */
    public static CompilerAnnotation parse(String annotation) {
        CompilerAnnotation a;
        if(annotation.equals(IGNORE_STRING))
            a = IGNORE;
        else if(annotation.equals(MARKER_STRING))
            a = MARKER;
        // else if(annotation.equals(NEW_THREAD_STRING))
        //     a = NEW_THREAD;
        else if(annotation.equals(NOTIFY_STRING))
            a = NOTIFY;
        else if(annotation.equals(NOTIFY_ALL_STRING))
            a = NOTIFY_ALL;
        else if(annotation.equals(RANDOM_STRING))
            a = RANDOM;
        else if(annotation.equals(SLEEP_STRING))
            a = SLEEP;
        else if(annotation.equals(INTERRUPT_STRING))
            a = INTERRUPT;
        else if(annotation.equals(START_THREAD_STRING))
            a = START_THREAD;
        else if(annotation.equals(WAIT_STRING))
            a = WAIT;
        else if(annotation.equals(JOIN_STRING))
            a = JOIN;
        else if(annotation.equals(BARRIER_ACTIVATE_STRING))
            a = BARRIER_ACTIVATE;
        else if(annotation.equals(BARRIER_VOID_STRING))
            a = BARRIER_VOID;
        else if(annotation.equals(BARRIER_CONDITIONAL_STRING))
            a = BARRIER_CONDITIONAL;
        else if(annotation.equals(BARRIER_CLOSURE_STRING))
            a = BARRIER_CLOSURE;
        else if(annotation.equals(BARRIER_PRODUCE_VOID_STRING))
            a = BARRIER_PRODUCE_VOID;
        else if(annotation.equals(BARRIER_PRODUCE_CONDITIONAL_STRING))
            a = BARRIER_PRODUCE_CONDITIONAL;
        else if(annotation.equals(BARRIER_PRODUCE_CLOSURE_STRING))
            a = BARRIER_PRODUCE_CLOSURE;
        else if(annotation.equals(BARRIER_CONSUME_VOID_STRING))
            a = BARRIER_CONSUME_VOID;
        else if(annotation.equals(BARRIER_CONSUME_CONDITIONAL_STRING))
            a = BARRIER_CONSUME_CONDITIONAL;
        else if(annotation.equals(BARRIER_CONSUME_CLOSURE_STRING))
            a = BARRIER_CONSUME_CLOSURE;
        else if(annotation.equals(PAIR_BARRIER_VOID_STRING))
            a = PAIR_BARRIER_VOID;
        else if(annotation.equals(PAIR_BARRIER_CONDITIONAL_STRING))
            a = PAIR_BARRIER_CONDITIONAL;
        else if(annotation.equals(PAIR_BARRIER_CLOSURE_STRING))
            a = PAIR_BARRIER_CLOSURE;
        else if(annotation.equals(PAIR_BARRIER_PRODUCE_VOID_STRING))
            a = PAIR_BARRIER_PRODUCE_VOID;
        else if(annotation.equals(PAIR_BARRIER_PRODUCE_CONDITIONAL_STRING))
            a = PAIR_BARRIER_PRODUCE_CONDITIONAL;
        else if(annotation.equals(PAIR_BARRIER_PRODUCE_CLOSURE_STRING))
            a = PAIR_BARRIER_PRODUCE_CLOSURE;
        else if(annotation.equals(PAIR_BARRIER_CONSUME_VOID_STRING))
            a = PAIR_BARRIER_CONSUME_VOID;
        else if(annotation.equals(PAIR_BARRIER_CONSUME_VOID_PRODUCER_STRING))
            a = PAIR_BARRIER_CONSUME_VOID_PRODUCER;
        else if(annotation.equals(PAIR_BARRIER_CONSUME_CONDITIONAL_STRING))
            a = PAIR_BARRIER_CONSUME_CONDITIONAL;
        else if(annotation.equals(PAIR_BARRIER_CONSUME_CLOSURE_STRING))
            a = PAIR_BARRIER_CONSUME_CLOSURE;
        else if(annotation.equals(HEAD_STRING))
            a = HEAD;
        else if(annotation.equals(TAIL_STRING))
            a = TAIL;
        else if(annotation.equals(STATE_STRING))
            a = STATE;
        else if(annotation.equals(ARRAY_COPY_STRING))
            a = ARRAY_COPY;
        else if(annotation.equals(MATH_ABS_STRING))
            a = MATH_ABS;
        else if(annotation.equals(MATH_MIN_STRING))
            a = MATH_MIN;
        else if(annotation.equals(MATH_MAX_STRING))
            a = MATH_MAX;
        else if(annotation.equals(MATH_SQRT_STRING))
            a = MATH_SQRT;
        else if(annotation.equals(MATH_SIN_STRING))
            a = MATH_SIN;
        else if(annotation.equals(MATH_COS_STRING))
            a = MATH_COS;
        else if(annotation.equals(MATH_ASIN_STRING))
            a = MATH_ASIN;
        else if(annotation.equals(MATH_ACOS_STRING))
            a = MATH_ACOS;
        else if(annotation.equals(MATH_FLOOR_STRING))
            a = MATH_FLOOR;
        else if(annotation.equals(MATH_CEIL_STRING))
            a = MATH_CEIL;
        else if(annotation.equals(MATH_ROUND_STRING))
            a = MATH_ROUND;
        else if(annotation.equals(MATH_POW_STRING))
            a = MATH_POW;
        else if(annotation.equals(DIST_UNI_STRING))
            a = DIST_UNI;
        else if(annotation.equals(DIST_NXP_STRING))
            a = DIST_NXP;
        else if(annotation.equals(DIST_ARRAY_STRING))
            a = DIST_ARRAY;
        else if(annotation.equals(CLOSURE_BOOLEAN_STRING))
            a = CLOSURE_BOOLEAN;
        else if(annotation.equals(CLOSURE_DOUBLE_STRING))
            a = CLOSURE_DOUBLE;
        else if(annotation.equals(MODEL_NAME_STRING))
            a = MODEL_NAME;
        else if(annotation.equals(MODEL_CHECK_STRING))
            a = MODEL_CHECK;
        else if(annotation.equals(MODEL_STATE_OR_STRING))
            a = MODEL_STATE_OR;
        else if(annotation.equals(MODEL_STATE_AND_STRING))
            a = MODEL_STATE_AND;
        else if(annotation.equals(MODEL_PLAYER_STRING))
            a = MODEL_PLAYER;
        else if(annotation.equals(MODEL_IS_STATISTICAL_STRING))
            a = MODEL_IS_STATISTICAL;
        else if(annotation.equals(MODEL_GET_INT_CONST_STRING))
            a = MODEL_GET_INT_CONST;
        else if(annotation.equals(MODEL_GET_DOUBLE_CONST_STRING))
            a = MODEL_GET_DOUBLE_CONST;
        else if(annotation.equals(INTERPRETER_STOP_STRING))
            a = INTERPRETER_STOP;
        else if(annotation.equals(NEW_FILE_FILENAME_STRING))
            a = NEW_FILE_FILENAME;
        else if(annotation.equals(FILE_EXISTS_STRING))
            a = FILE_EXISTS;
        else if(annotation.equals(FILE_DELETE_STRING))
            a = FILE_DELETE;
        else if(annotation.equals(NEW_RANDOM_ACCESS_FILE_FILE_STRING))
            a = NEW_RANDOM_ACCESS_FILE_FILE;
        else if(annotation.equals(NEW_RANDOM_ACCESS_FILE_FILENAME_STRING))
            a = NEW_RANDOM_ACCESS_FILE_FILENAME;
        else if(annotation.equals(RANDOM_ACCESS_FILE_CLOSE_STRING))
            a = RANDOM_ACCESS_FILE_CLOSE;
        else if(annotation.equals(RANDOM_ACCESS_FILE_READ_BYTE_STRING))
            a = RANDOM_ACCESS_FILE_READ_BYTE;
        else if(annotation.equals(RANDOM_ACCESS_FILE_READ_ARRAY_STRING))
            a = RANDOM_ACCESS_FILE_READ_ARRAY;
        else if(annotation.equals(RANDOM_ACCESS_FILE_WRITE_BYTE_STRING))
            a = RANDOM_ACCESS_FILE_WRITE_BYTE;
        else if(annotation.equals(RANDOM_ACCESS_FILE_WRITE_ARRAY_STRING))
            a = RANDOM_ACCESS_FILE_WRITE_ARRAY;
        else if(annotation.equals(RANDOM_ACCESS_FILE_LENGTH_STRING))
            a = RANDOM_ACCESS_FILE_LENGTH;
        else if(annotation.equals(RANDOM_ACCESS_FILE_SEEK_STRING))
            a = RANDOM_ACCESS_FILE_SEEK;
        else if(annotation.equals(PRINT_STRING))
            a = PRINT;
        else if(annotation.equals(PRINTLN_STRING))
            a = PRINTLN;
        else if(annotation.equals(IO_NEW_TUPLES_STRING))
            a = IO_NEW_TUPLES;
        else if(annotation.equals(IO_NEW_MATRIX_STRING))
            a = IO_NEW_MATRIX;
        else if(annotation.equals(IO_GET_ROW_INDEX_STRING))
            a = IO_GET_ROW_INDEX;
        else if(annotation.equals(IO_GET_NEXT_STRING))
            a = IO_GET_NEXT;
        else if(annotation.equals(IO_ADD_NEXT_STRING))
            a = IO_ADD_NEXT;
        else if(annotation.equals(IO_CLOSE_STRING))
            a = IO_CLOSE;
        else if(annotation.equals(SUBSTRING_STRING))
            a = SUBSTRING;
        /*else if(annotation.equals(ERROR_STRING))
            a = ERROR;*/
        else if(annotation.startsWith("@"))
            a = UNKNOWN_COMPILER_ANNOTATION;
        else
            a = UNKNOWN_ANNOTATION;
        return a;
    }
    /**
     * Returns if only a single method per <code>AbstractFrontend</code> is allowed
     * to possess this annotation.<br>
     * 
     * For all annotations, that do not have a clear reason for being used with multiple
     * methods, this method should return true.
     * 
     * @return if this annotation should decorate at most a single method
     */
    public boolean uniqueMethod() {
        switch(this) {
            case IGNORE:
            case MARKER:
            // is needed for methods with different number of parameters/aguments
            case RANDOM:
            // there are barrier methods for different arguments
            case HEAD:
            case TAIL:
            case STATE:
            // these math functions may occur for different arithmetic types
            case MATH_ABS:
            case MATH_MIN:
            case MATH_MAX:
            case MATH_POW:
            // may have either integer or real values
            case DIST_ARRAY:
            // occurs in differents kinds of a barrier
            case BARRIER_ACTIVATE:
            // used in both <code>produce()</code> and
            // <code>produce(int)</code>
            case PAIR_BARRIER_PRODUCE_VOID:
                return false;
                
            case NOTIFY:
            case NOTIFY_ALL:
            case SLEEP:
            case INTERRUPT:
            case START_THREAD:
            case WAIT:
            case JOIN:
            case BARRIER_VOID:
            case BARRIER_CONDITIONAL:
            case BARRIER_CLOSURE:
            case BARRIER_PRODUCE_VOID:
            case BARRIER_PRODUCE_CONDITIONAL:
            case BARRIER_PRODUCE_CLOSURE:
            case BARRIER_CONSUME_VOID:
            case BARRIER_CONSUME_CONDITIONAL:
            case BARRIER_CONSUME_CLOSURE:
            case PAIR_BARRIER_VOID:
            case PAIR_BARRIER_CONDITIONAL:
            case PAIR_BARRIER_CLOSURE:
            case PAIR_BARRIER_PRODUCE_CONDITIONAL:
            case PAIR_BARRIER_PRODUCE_CLOSURE:
            case PAIR_BARRIER_CONSUME_VOID:
            case PAIR_BARRIER_CONSUME_VOID_PRODUCER:
            case PAIR_BARRIER_CONSUME_CONDITIONAL:
            case PAIR_BARRIER_CONSUME_CLOSURE:
            case ARRAY_COPY:
            case MATH_SQRT:
            case MATH_SIN:
            case MATH_COS:
            case MATH_ASIN:
            case MATH_ACOS:
            case MATH_FLOOR:
            case MATH_CEIL:
            case MATH_ROUND:
            case DIST_UNI:
            case DIST_NXP:
            case CLOSURE_BOOLEAN:
            case CLOSURE_DOUBLE:
            case MODEL_NAME:
            case MODEL_CHECK:
            case MODEL_STATE_OR:
            case MODEL_STATE_AND:
            case MODEL_PLAYER:
            case MODEL_IS_STATISTICAL:
            case MODEL_GET_INT_CONST:
            case MODEL_GET_DOUBLE_CONST:
            case INTERPRETER_STOP:
            case NEW_FILE_FILENAME:
            case FILE_EXISTS:
            case FILE_DELETE:
            case NEW_RANDOM_ACCESS_FILE_FILE:
            case NEW_RANDOM_ACCESS_FILE_FILENAME:
            case RANDOM_ACCESS_FILE_CLOSE:
            case RANDOM_ACCESS_FILE_READ_BYTE:
            case RANDOM_ACCESS_FILE_READ_ARRAY:
            case RANDOM_ACCESS_FILE_WRITE_BYTE:
            case RANDOM_ACCESS_FILE_WRITE_ARRAY:
            case RANDOM_ACCESS_FILE_LENGTH:
            case RANDOM_ACCESS_FILE_SEEK:
            case PRINT:
            case PRINTLN:
            case IO_NEW_TUPLES:
            case IO_NEW_MATRIX:
            case IO_GET_ROW_INDEX:
            case IO_GET_NEXT:
            case IO_ADD_NEXT:
            case IO_CLOSE:
            case SUBSTRING:
                return true;

            default:
                throw new RuntimeException("unknown string representation");
        }
    }
    /**
     * Returns a string constant, that represents this annotation. Includes
     * the preceeding `@'.
     * 
     * @return a constant
     */
    public String toAnnotationString() {
        switch(this) {
            case IGNORE:
                return IGNORE_STRING;
                
            case MARKER:
                return MARKER_STRING;
                
            case NOTIFY:
                return NOTIFY_STRING;
                
            case NOTIFY_ALL:
                return NOTIFY_ALL_STRING;
                
            case RANDOM:
                return RANDOM_STRING;
                
            case SLEEP:
                return SLEEP_STRING;
                
            case INTERRUPT:
                return INTERRUPT_STRING;
                
            case START_THREAD:
                return START_THREAD_STRING;
                
            case WAIT:
                return WAIT_STRING;
                
            case JOIN:
                return JOIN_STRING;
                
            case BARRIER_ACTIVATE:
                return BARRIER_ACTIVATE_STRING;
                
            case BARRIER_VOID:
                return BARRIER_VOID_STRING;
                
            case BARRIER_CONDITIONAL:
                return BARRIER_CONDITIONAL_STRING;
                
            case BARRIER_CLOSURE:
                return BARRIER_CLOSURE_STRING;
                
            case BARRIER_PRODUCE_VOID:
                return BARRIER_PRODUCE_VOID_STRING;
                
            case BARRIER_PRODUCE_CONDITIONAL:
                return BARRIER_PRODUCE_CONDITIONAL_STRING;
                
            case BARRIER_PRODUCE_CLOSURE:
                return BARRIER_PRODUCE_CLOSURE_STRING;
                
            case BARRIER_CONSUME_VOID:
                return BARRIER_CONSUME_VOID_STRING;
                
            case BARRIER_CONSUME_CONDITIONAL:
                return BARRIER_CONSUME_CONDITIONAL_STRING;
                
            case BARRIER_CONSUME_CLOSURE:
                return BARRIER_CONSUME_CLOSURE_STRING;
                
            case PAIR_BARRIER_VOID:
                return PAIR_BARRIER_VOID_STRING;
                
            case PAIR_BARRIER_CONDITIONAL:
                return PAIR_BARRIER_CONDITIONAL_STRING;
                
            case PAIR_BARRIER_CLOSURE:
                return PAIR_BARRIER_CLOSURE_STRING;
                
            case PAIR_BARRIER_PRODUCE_VOID:
                return PAIR_BARRIER_PRODUCE_VOID_STRING;
                
            case PAIR_BARRIER_PRODUCE_CONDITIONAL:
                return PAIR_BARRIER_PRODUCE_CONDITIONAL_STRING;
                
            case PAIR_BARRIER_PRODUCE_CLOSURE:
                return PAIR_BARRIER_PRODUCE_CLOSURE_STRING;
                
            case PAIR_BARRIER_CONSUME_VOID:
                return PAIR_BARRIER_CONSUME_VOID_STRING;
                
            case PAIR_BARRIER_CONSUME_VOID_PRODUCER:
                return PAIR_BARRIER_CONSUME_VOID_PRODUCER_STRING;
                
            case PAIR_BARRIER_CONSUME_CONDITIONAL:
                return PAIR_BARRIER_CONSUME_CONDITIONAL_STRING;
                
            case PAIR_BARRIER_CONSUME_CLOSURE:
                return PAIR_BARRIER_CONSUME_CLOSURE_STRING;
                
            case HEAD:
                return HEAD_STRING;
                
            case TAIL:
                return TAIL_STRING;
                
            case STATE:
                return STATE_STRING;
                
            case ARRAY_COPY:
                return ARRAY_COPY_STRING;
                
            case MATH_ABS:
                return MATH_ABS_STRING;
                        
            case MATH_MIN:
                return MATH_MIN_STRING;
                        
            case MATH_MAX:
                return MATH_MAX_STRING;
                        
            case MATH_SQRT:
                return MATH_SQRT_STRING;
                        
            case MATH_SIN:
                return MATH_SIN_STRING;
                        
            case MATH_COS:
                return MATH_COS_STRING;
                        
            case MATH_ASIN:
                return MATH_ASIN_STRING;
                        
            case MATH_ACOS:
                return MATH_ACOS_STRING;
                        
            case MATH_FLOOR:
                return MATH_FLOOR_STRING;
                        
            case MATH_CEIL:
                return MATH_CEIL_STRING;
                        
            case MATH_ROUND:
                return MATH_ROUND_STRING;
                        
            case MATH_POW:
                return MATH_POW_STRING;
                        
            case DIST_UNI:
                return DIST_UNI_STRING;
                        
            case DIST_NXP:
                return DIST_NXP_STRING;
                        
            case DIST_ARRAY:
                return DIST_ARRAY_STRING;
                        
            case CLOSURE_BOOLEAN:
                return CLOSURE_BOOLEAN_STRING;
                        
            case CLOSURE_DOUBLE:
                return CLOSURE_DOUBLE_STRING;
                        
            case MODEL_NAME:
                return MODEL_NAME_STRING;
                        
            case MODEL_CHECK:
                return MODEL_CHECK_STRING;
                        
            case MODEL_STATE_OR:
                return MODEL_STATE_OR_STRING;
                        
            case MODEL_STATE_AND:
                return MODEL_STATE_AND_STRING;
                        
            case MODEL_PLAYER:
                return MODEL_PLAYER_STRING;
                        
            case MODEL_IS_STATISTICAL:
                return MODEL_IS_STATISTICAL_STRING;
                        
            case MODEL_GET_INT_CONST:
                return MODEL_GET_INT_CONST_STRING;
                        
            case MODEL_GET_DOUBLE_CONST:
                return MODEL_GET_DOUBLE_CONST_STRING;
                        
            case INTERPRETER_STOP:
                return INTERPRETER_STOP_STRING;
                        
            case NEW_FILE_FILENAME:
                return NEW_FILE_FILENAME_STRING;
                
            case FILE_EXISTS:
                return FILE_EXISTS_STRING;
                
            case FILE_DELETE:
                return FILE_DELETE_STRING;
                
            case NEW_RANDOM_ACCESS_FILE_FILE:
                return NEW_RANDOM_ACCESS_FILE_FILE_STRING;
                
            case NEW_RANDOM_ACCESS_FILE_FILENAME:
                return NEW_RANDOM_ACCESS_FILE_FILENAME_STRING;
                
            case RANDOM_ACCESS_FILE_CLOSE:
                return RANDOM_ACCESS_FILE_CLOSE_STRING;
                
            case RANDOM_ACCESS_FILE_READ_BYTE:
                return RANDOM_ACCESS_FILE_READ_BYTE_STRING;
                
            case RANDOM_ACCESS_FILE_READ_ARRAY:
                return RANDOM_ACCESS_FILE_READ_ARRAY_STRING;
                
            case RANDOM_ACCESS_FILE_WRITE_BYTE:
                return RANDOM_ACCESS_FILE_WRITE_BYTE_STRING;

            case RANDOM_ACCESS_FILE_WRITE_ARRAY:
                return RANDOM_ACCESS_FILE_WRITE_ARRAY_STRING;
                
            case RANDOM_ACCESS_FILE_LENGTH:
                return RANDOM_ACCESS_FILE_LENGTH_STRING;
                
            case RANDOM_ACCESS_FILE_SEEK:
                return RANDOM_ACCESS_FILE_SEEK_STRING;
                
            case PRINT:
                return PRINT_STRING;
                
            case PRINTLN:
                return PRINTLN_STRING;

            case IO_NEW_TUPLES:
                return IO_NEW_TUPLES_STRING;

            case IO_NEW_MATRIX:
                return IO_NEW_MATRIX_STRING;

            case IO_GET_ROW_INDEX:
                return IO_GET_ROW_INDEX_STRING;

            case IO_GET_NEXT:
                return IO_GET_NEXT_STRING;

            case IO_ADD_NEXT:
                return IO_ADD_NEXT_STRING;

            case IO_CLOSE:
                return IO_CLOSE_STRING;

            case SUBSTRING:
                return SUBSTRING_STRING;

            /*case ERROR:
                return ERROR_STRING;*/
                
            default:
                throw new RuntimeException("unknown string representation");
        }
    }
    /**
     * Returns if a set contains a this compiler annotation.
     * 
     * @param set annotations to browse
     * @return if this annotation is present in the set
     */
    public boolean in(Set<String> set) {
        for(String a : set)
            if(parse(a) == this)
                return true;
        return false;
    }
    /**
     * If some annotation represent a barrier, then a respective barrier type is
     * returned.
     * 
     * @param annotations a set of annotations
     * @return a compiler annotation representing a given barrier type, or null
     */
    public static CompilerAnnotation getBarrierType(Collection<String> annotations) {
        for(String s : annotations) {
            CompilerAnnotation a;
            switch(a = parse(s)) {
                case BARRIER_VOID:
                case BARRIER_CONDITIONAL:
                case BARRIER_CLOSURE:
                case BARRIER_PRODUCE_VOID:
                case BARRIER_PRODUCE_CONDITIONAL:
                case BARRIER_PRODUCE_CLOSURE:
                case BARRIER_CONSUME_VOID:
                case BARRIER_CONSUME_CONDITIONAL:
                case BARRIER_CONSUME_CLOSURE:
                case PAIR_BARRIER_VOID:
                case PAIR_BARRIER_CONDITIONAL:
                case PAIR_BARRIER_CLOSURE:
                case PAIR_BARRIER_PRODUCE_VOID:
                case PAIR_BARRIER_PRODUCE_CONDITIONAL:
                case PAIR_BARRIER_PRODUCE_CLOSURE:
                case PAIR_BARRIER_CONSUME_VOID:
                case PAIR_BARRIER_CONSUME_VOID_PRODUCER:
                case PAIR_BARRIER_CONSUME_CONDITIONAL:
                case PAIR_BARRIER_CONSUME_CLOSURE:
                    return a;
                    
                default:
                    ;
            }
        }
        return null;
    }
    /**
     * If the annotation represents a symmetric or directional
     * barrier having a condition.
     * 
     * @return if the barrier or pair barrier is conditional
     */
    public boolean isConditionalBarrier() {
        switch(this) {
            case BARRIER_CONDITIONAL:
            case BARRIER_CLOSURE:
            case BARRIER_PRODUCE_CONDITIONAL:
            case BARRIER_PRODUCE_CLOSURE:
            case BARRIER_CONSUME_CONDITIONAL:
            case BARRIER_CONSUME_CLOSURE:
            case PAIR_BARRIER_CONDITIONAL:
            case PAIR_BARRIER_CLOSURE:
            case PAIR_BARRIER_PRODUCE_CONDITIONAL:
            case PAIR_BARRIER_PRODUCE_CLOSURE:
            case PAIR_BARRIER_CONSUME_CONDITIONAL:
            case PAIR_BARRIER_CONSUME_CLOSURE:
                return true;

            case BARRIER_VOID:
            case BARRIER_PRODUCE_VOID:
            case BARRIER_CONSUME_VOID:
            case PAIR_BARRIER_VOID:
            case PAIR_BARRIER_PRODUCE_VOID:
            case PAIR_BARRIER_CONSUME_VOID:
            case PAIR_BARRIER_CONSUME_VOID_PRODUCER:
                return false;
                
            default:
                throw new RuntimeException("no barrier or no known barrier type");
        }
    }
}
