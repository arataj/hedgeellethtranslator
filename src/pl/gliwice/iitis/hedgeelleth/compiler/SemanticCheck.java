/*
 * SemanticCheck.java
 *
 * Created on Jan 22, 2008, 3:14:05 PM
 *
 * This file is copyright (c) 2008 Artur Rataj.
 *
 * This software is provided under the terms of the GNU Library General
 * Public License, either version 2 of the license or, at your option,
 * any later version. See http://www.gnu.org for details.
 */

package pl.gliwice.iitis.hedgeelleth.compiler;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.Hedgeelleth;
import pl.gliwice.iitis.hedgeelleth.compiler.primitiverange.PrimitiveRangeDescription;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.clazz.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.flags.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.languages.IntegerSetExpression;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.scope.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.CompilerException.Report;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.SourcePosition;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.StreamPos;
import pl.gliwice.iitis.hedgeelleth.modules.AbstractFrontend;
import pl.gliwice.iitis.hedgeelleth.modules.CliConstants;

/**
 * A semantic check for the tree. It mainly sets result types, or looks up
 * variables or methods. Parsing and testing of primary expressions is
 * performed here.<br>
 *
 * This class does not check the uniqueness of expressions within
 * <code>Compilation.uniqueExpressions</code> as that can be done only
 * after static analysis. See <code>StaticAnalysis.updateUniqueExpressions</code>
 * for details.<br>
 *
 * Throughout the translator, overriding also means the static shadowing.
 * Difference between these two is only at the code generation level, by applying
 * a compile time type to the static calls and runtime type to the non--static
 * calls.<br>
 * 
 * The methods <code>visit(CallExpression)</code> when <code>CallExpression</code>
 * is not a constructor invocation with method to be set, and
 * <code>visit(IndexExpression)</code> both throw a runtime exception, see the
 * documentation of <code>ParsedPrimary</code> for details.<br>
 *
 * This class recognizes the field <code>AbstractJavaClass.frontend</code> and calls
 * <code>AbstractFrontend.semanticCheck</code> for block expressions as
 * necessary.<br>
 * 
 * This class and its subclasses must have exactly one public constructor.<br>
 *
 * See the docs of <code>Method<code> for conventions about implementing and
 * overriding a method.<br>
 *
 * All occurences of <code>IntegerSetExpression</code> are registered, to
 * be used for experiments.
 *
 * @author Artur Rataj
 *
 * @see StaticAnalysis.updateUniqueExpressions
 */
public class SemanticCheck implements Visitor<Object> {
    /**
     * If to be verbose.
     */
    boolean verbose = false;
    /**
     * Compilation.
     */
    public Compilation compilation;
    /**
     * Current method whose code is visited or null if none.
     */
    Method currMethod;
    /**
     * If of integer set expressions found in the checked code.
     * Empty list if no such expressions have been found.
     */
    public List<IntegerSetExpression> integerSets;
    /**
     * If primitive range expressions are temporarily forbidden. True if
     * already within a primitive range expression.
     */
    private boolean forbidRangeExpressions;
    /**
     * Transformer of primary expressions, null for not transforming the expressions.
     */
    AbstractPrimaryTransformer primaryTransformer;
    
    /**
     * Creates a new instance of SemanticCheck.
     * 
     * @param compilation compilation to check semantically, null
     * for none, e. g. when there is only some object to check, ouside
     * any compilation
     * @param primaryTransformer transformer of primary expressions,
     * null for not transforming the expressions
     */
    public SemanticCheck(Compilation compilation,
            AbstractPrimaryTransformer primaryTransformer)  throws CompilerException {
        this.primaryTransformer = primaryTransformer;
        integerSets = new LinkedList<>();
        forbidRangeExpressions = false;
        if(compilation != null)
            compilation.accept(this);
    }

    @Override
    public Object visit(Node n) {
        throw new RuntimeException("Error: visit from " + n.toString());
    }
    @Override
    public Object visit(AbstractExpression e) throws CompilerException {
        throw new RuntimeException("Error: visit from " + e.toString());
    }
    @Override
    public Object visit(EmptyExpression e) throws CompilerException {
        return null;
    }
    @Override
    public Object visit(ConstantExpression e) throws CompilerException {
        if(e instanceof IntegerSetExpression)
            integerSets.add((IntegerSetExpression)e);
        return null;
    }
    @Override
    public Object visit(AllocationExpression e) throws CompilerException {
/*if(e.toString().indexOf("LossyChannel") != -1)
    e = e;*/
        for(AbstractExpression expression : e.expressions)
            expression.accept(this);
        // is not it done in PrimaryExpression? no, because
        // the type is assigned by the parser
        try {
            parseJavaClass(e.outerScope, e.getResultType());
        } catch(CompilerException f) {
            f.completePos(e.getStreamPos());
            throw f;
        }
        switch(e.allocation) {
            case CONSTRUCTOR:
                Type classType = e.getResultType();
                AbstractJavaClass javaClass = e.outerScope.
                        lookupJavaClassNA(classType.getJava());
                if(javaClass instanceof Interface)
                    throw new CompilerException(e.getStreamPos(),
                            ParseException.Code.ILLEGAL,
                            javaClass.getQualifiedName() + " is an interface, " +
                            "can not be instantiated");
                else if(javaClass.flags.isAbstract())
                    throw new CompilerException(e.getStreamPos(),
                            ParseException.Code.ILLEGAL,
                            javaClass.getQualifiedName() + " is abstract, " +
                            "can not be instantiated");
                Constructor constructor =
                        e.outerScope.lookupConstructorByClassAT(
                        e.outerScope.getBoundingMethodScope().getBoundingJavaClass().frontend,
                        classType, e.expressions);
                if(constructor == null)
                    throw new CompilerException(e.getStreamPos(),
                            ParseException.Code.LOOK_UP,
                            "constructor not found: " +
                            Method.getDeclarationDescription(
                                classType.getJava().toString(), e.expressions,
                                null, true));
                e.constructor = constructor;
                break;
                
            case ARRAY:
                boolean emptyFound = false;
                for(AbstractExpression expression : e.expressions) {
                    if(expression instanceof EmptyExpression)
                        emptyFound = true;
                    else {
                        if(emptyFound)
                            throw new ParseException(expression.getStreamPos(),
                                    ParseException.Code.PARSE,
                                    "\"[]\" expected");
                        checkIndexType(expression,
                                expression.outerScope.getBoundingMethodScope().
                                getBoundingJavaClass().frontend);
                    }
                }
                e.constructor = null;
                break;

            default:
                throw new RuntimeException("unknown allocation type: " + e.allocation);
        }
        return null;
    }
    @Override
    public Object visit(PrimaryExpression e) throws CompilerException {
        if(primaryTransformer != null)
            primaryTransformer.transform(e);
        CompilerException errors = new CompilerException();
        try {
/*if(e.toString().indexOf("this.f1") != -1)
    e = e;*/
              e.value = new ParsedPrimary(compilation == null ?
                            new CliConstants() : compilation.options.constants,
                        this, e);
              if(e.prefix.rangeDescription != null &&
                      !PrimitiveRangeDescription.validPrimaryRangeType(e.value.getResultType()))
                      errors.addReport(new Report(e.prefix.rangeDescription.textPos,
                              ParseException.Code.ILLEGAL,
                              "invalid type for primitive range: " +
                              e.value.getResultType().toNameString()));
        } catch(CompilerException f) {
            // complete the stream position if missing
            errors.addAllReports(
                    new CompilerException(e.getStreamPos(),
                    ParseException.Code.PARSE,
                    f));
        }
        for(AbstractExpression r : e.prefix.rangeExpressions)
            try {
                if(forbidRangeExpressions)
                    errors.addAllReports(
                            new CompilerException(e.getStreamPos(),
                            ParseException.Code.PARSE,
                            "primitive range expressions can not be nested"));
                else {
                    AssignmentExpression a = (AssignmentExpression)r;
                    forbidRangeExpressions = true;
                    a.accept(this);
                    forbidRangeExpressions = false;
                }
            } catch(CompilerException f) {
                errors.addAllReports(f);
            }
        if(errors.reportsExist())
            throw errors;
        e.setResultType(e.value.getResultType());
        return null;
    }
    @Override
    public Object visit(UnaryExpression e) throws CompilerException {
        e.sub.accept(this);
        Type sub = e.sub.getResultType();
        Type type = Type.promoteUnary(e);
        if(type == null) {
            if(e.operatorType == UnaryExpression.Op.CAST)
                throw new CompilerException(e.getStreamPos(),
                        ParseException.Code.ILLEGAL,
                        "can not cast " + sub.toNameString() +
                        " to " + e.objectType.toNameString());
            else
                throw new CompilerException(e.getStreamPos(),
                        ParseException.Code.ILLEGAL,
                        "illegal operand " + sub.toNameString() +
                        " for unary " + e.operatorType.toString());
        }
        switch(e.operatorType) {
            case POST_DECREMENT:
            case PRE_DECREMENT:
            case POST_INCREMENT:
            case PRE_INCREMENT:
                if(!(e.sub instanceof PrimaryExpression) ||
                        !((PrimaryExpression)e.sub).value.isVariableExpression())
                    throw new CompilerException(e.getStreamPos(),
                            ParseException.Code.ILLEGAL,
                            "operator " + e.operatorType.toString() +
                            " can be applied only to a variable");
                break;

            case BITWISE_COMPLEMENT:
            case CONDITIONAL_NEGATION:
            case NEGATION:
            case A:
            case E:
            case X:
            case F:
            case G:
            case Kc:
            case Eg:
            case Dg:
            case Cg:
            case NEG_Kc:
            case NEG_Eg:
            case NEG_Dg:
            case NEG_Cg:
            case Oc:
            case Khc:
            case NEG_Oc:
            case NEG_Khc:
                /* empty */
                break;

            case CAST:
                if(e.objectType.isJava())
                    parseJavaClass(e.outerScope, e.objectType);
                else {
                    // a fake variable just to check fitting
                    PrimaryExpression lvalue = Hedgeelleth.newVariableExpression(
                            e.getStreamPos(), (BlockScope)e.outerScope,
                            new Variable(e.getStreamPos(), (VariableOwner)e.outerScope.
                                getBoundingMethodScope().owner, e.objectType, "fake"));
                    // parsing is impossible because the variable is fake;
                    // therefore, just set the type instead of parsing
                    lvalue.setResultType(e.objectType);
                    checkCast(e.getStreamPos(), e.outerScope,
                            lvalue, e.sub, true);
                }
                break;

            case INSTANCE_OF:
                if(e.objectType.isJava())
                    parseJavaClass(e.outerScope, e.objectType);
                if(!sub.getResultType().isNull() &&
                        !Type.convertible(e.outerScope,
                        sub.getResultType(), e.objectType))
                    throw new CompilerException(e.getStreamPos(),
                            ParseException.Code.ILLEGAL,
                            "inconvertible types");
                break;

            default:
                throw new RuntimeException("unknown or invalid operator type: " + e.operatorType);
        }
        e.setResultType(type);
        return null;
    }
    @Override
    public Object visit(BinaryExpression e) throws CompilerException {
        CompilerException error = new CompilerException();
        try {
            e.left.accept(this);
        } catch(CompilerException f) {
            error.addAllReports(f);
        }
        try {
            e.right.accept(this);
        } catch(CompilerException f) {
            error.addAllReports(f);
        }
        if(!error.reportsExist()) {
            Type left = e.left.getResultType();
            Type right = e.right.getResultType();
            Type type = Type.promoteBinary(left, right, e.operatorType,
                    e.outerScope.getBoundingMethodScope().getBoundingJavaClass().
                    frontend.stringClassName);
            if(type == null)
                error.addReport(new Report(e.getStreamPos(),
                        ParseException.Code.ILLEGAL,
                        "illegal operands: " + left.toNameString() + ", " +
                        right.toNameString() + " for binary " +
                        e.operatorType.toString()));
            else {
                if(left.isJava() && right.isJava() && e.operatorType.isRelational()) {
                    // comparison of references
                    //
                    // <code>promoteBinary</code> does not check for convertibility,
                    // check it here
                    if(!Type.convertible(e.outerScope, left, right))
                        error.addReport(new Report(e.getStreamPos(),
                                ParseException.Code.ILLEGAL,
                                "types of operands of binary " + e.operatorType.toString() +
                                " not convertible"));
                }
                e.setResultType(type);
            }
        }
        if(error.reportsExist())
            throw error;
        return null;
    }
    /**
     * Checks for validity of a possible assignment.
     * 
     * @param pos position of the assigning expression, for error reporting
     * @param outerScope the expression's scope
     * @param lvalue assigned value
     * @param rvalue right value of an assignment or a cast
     * @param ignoreNarrowingType ignore that the target type is
     * narrower; true for explicit casts; overflowing integer constants are
     * still not allowed
     */
    private void checkCast(StreamPos pos,
            AbstractScope outerScope, PrimaryExpression lvalue,
            AbstractExpression rvalue, boolean ignoreNarrowingType)
            throws ParseException {
        StringBuilder errorStr = new StringBuilder();
        // literals to test for fitting into target type
        List<Literal> constantLiterals = new LinkedList<>();
        if(rvalue.getSimpleConstant() != null)
            constantLiterals.add(rvalue.getSimpleConstant());
        else if(rvalue instanceof PrimaryExpression) {
            List<Typed> contents = ((PrimaryExpression)rvalue).value.contents;
            if(contents.size() == 1) {
                Typed t = contents.iterator().next();
                if(t instanceof IntegerSetExpression) {
                    // complete the field name, which was unknown
                    // within  <code>ParsePrimary</code>
                    IntegerSetExpression s = (IntegerSetExpression)t;
                    for(long l : s.values)
                        constantLiterals.add(new Literal(l));
                     Variable v = lvalue.value.getVariable();
                     if(!v.isLocal()) {
                        if(s.fullyQualifiedFieldName != null)
                            throw new RuntimeException("unexpected: " +
                                    "name already set");
                        s.fullyQualifiedFieldName = ((AbstractJavaClass)v.owner).getQualifiedName() +
                                "." + v.name;
                     }
                } else if(t instanceof ConstantExpression) {
                    // a constant expression, which is not a set of integers
                    ConstantExpression c = (ConstantExpression)t;
                    constantLiterals.add(c.literal);
                }
            }
        }
        Type lType = lvalue.getResultType();
        Type rType = rvalue.getResultType();
        if(constantLiterals.isEmpty()) {
            rType.constant = null;
            // no known constant -- no narrowing implicit
            // cast possible
            if(!Type.castable(outerScope, rType, lType,
                    true, ignoreNarrowingType, errorStr))
                throw new ParseException(pos,
                        ParseException.Code.ILLEGAL,
                        errorStr.toString());
        } else {
            for(Literal literal : constantLiterals) {
                rType.constant = literal;
                if(!Type.castable(outerScope, rType, lType,
                        true, ignoreNarrowingType, errorStr))
                    throw new ParseException(pos,
                            ParseException.Code.ILLEGAL,
                            errorStr.toString());
            }
        }
    }
    @Override
    public Object visit(AssignmentExpression e) throws CompilerException {
/*if(e.toString().indexOf("jj =") != -1)
    e = e;*/
        CompilerException errors = new CompilerException();
        try {
            e.lvalue.accept(this);
        } catch(CompilerException f) {
            errors.addAllReports(f);
        }
        try {
            e.rvalue.accept(this);
        } catch(CompilerException f) {
            errors.addAllReports(f);
        }
        if(e.lvalue.prefix.rangeExpressions.size() != 0)
            throw new ParseException(e.lvalue.getStreamPos(),
                    ParseException.Code.ILLEGAL,
                    "lvalue can not be ranged");
        if(errors.reportsExist())
            throw errors;
        if(!e.lvalue.value.isVariableExpression())
            throw new ParseException(e.lvalue.getStreamPos(),
                    ParseException.Code.ILLEGAL,
                    "not a variable");
        Type lType = e.lvalue.getResultType();
        Type rType = e.rvalue.getResultType();
        if(lType == null) {
            // a primitive range expression, that requires the primitive range
            // bound variable type to be completed
            if(!PrimitiveRangeDescription.validPrimaryRangeType(rType))
                throw new ParseException(e.lvalue.getStreamPos(),
                        ParseException.Code.ILLEGAL,
                        "invalid type for a primitive range");
            lType = rType;
            e.lvalue.setResultType(lType);
            if(e.lvalue.value.contents.size() != 1)
                throw new RuntimeException("can complete type only for scalar " +
                        "variables");
            Variable v = (Variable)e.lvalue.value.contents.get(0);
            v.setResultType(lType);
        }
        checkCast(e.getStreamPos(), e.outerScope,
                e.lvalue, e.rvalue, false);
        e.setResultType(e.lvalue.getResultType());
        log(4, e.toString());
        return null;
    }
    @Override
    public Object visit(CallExpression c) throws CompilerException {
        for(AbstractExpression a : c.arg)
            a.accept(this);
        if(c.method == Hedgeelleth.CONSTRUCTOR_SUPER ||
                c.method == Hedgeelleth.CONSTRUCTOR_THIS) {
            Method constructor;
            try {
                if(c.method == Hedgeelleth.CONSTRUCTOR_SUPER) {
                    Type extendS = currMethod.owner.extendS;
                    if(extendS == null)
                        throw new ParseException(c.getStreamPos(),
                                ParseException.Code.LOOK_UP,
                                currMethod.owner.getQualifiedName() +
                                "has no supertype");
                    constructor = c.outerScope.lookupMethodByNameAT(
                        c.outerScope.getBoundingMethodScope().getBoundingJavaClass().frontend,
                        extendS.getJava().last(1), c.arg,
                        PrimaryExpression.ScopeMode.OMIT_STATIC);
                } else {
                    constructor = c.outerScope.lookupMethodByNameAT(
                        c.outerScope.getBoundingMethodScope().getBoundingJavaClass().frontend,
                        new NameList(currMethod.owner.name), c.arg,
                        PrimaryExpression.ScopeMode.OMIT_STATIC);
                }
            } catch(CompilerException e) {
                throw new CompilerException(c.getStreamPos(),
                    ParseException.Code.PARSE,
                    e);
            }
            c.method = constructor;
            if(c.method == currMethod)
                throw new ParseException(c.getStreamPos(),
                        ParseException.Code.ILLEGAL, "recursive constructor invocation");
            c.setResultType(c.method.getResultType());
        } else if(!c.method.name.equals(AbstractJavaClass.INIT_STATIC) &&
                !c.method.name.equals(AbstractJavaClass.INIT_OBJECT))
            // should be handled within <code>ParsedPrimary</code>
            throw new RuntimeException("visited CallExpression with an expression " +
                    "that is an invocation of neither a constructor nor an init method");
        if(!c.directNonStatic)
            throw new RuntimeException("call to constructor or to init method " +
                    "is not direct");
        // the expression is not a part of a primary expression,
        // so "this" must be specified within the expression itself,
        // and it points to local "this" be this a call to
        // this, to super or to object*.
        c.independentCallThis = Hedgeelleth.newVariableExpression(
                c.getStreamPos(), (BlockScope)c.outerScope,
                currMethod.scope.getLocalThis());
        c.independentCallThis.accept(this);
        return null;
    }
    @Override
    public Object visit(IndexExpression c) throws CompilerException {
        // handled within <code>ParsedPrimary</code>
        throw new RuntimeException("visited IndexExpression");
    }
    @Override
    /**
     * Override for custom parsing of <code>BlockExpression</code>.
     * This implementation should be called after that parsing.<br>
     *
     * Alternatively, the frontend may parse language--specific
     * <code>BlockExpression</code> itself, see the docs of
     * <code>AbstractFrontend</code> for details.
     * Calling of a respective parsing method of the frontend is
     * provided by this implementation. The call occurs before
     * parsing <code>BlockExpression.block</code>.
     *
     */
    public Object visit(BlockExpression b) throws CompilerException {
        CompilerException compileErrors = new CompilerException();
        currMethod.owner.frontend.semanticCheck(compilation, this,
                b, compileErrors);
        if(compileErrors.reportsExist())
            throw compileErrors;
        if(b.block == null)
            throw new RuntimeException("unrecognized block expression");
        b.block.accept(this);
        AssignmentExpression a = (AssignmentExpression)
                b.block.code.get(b.block.code.size() - 1);
        b.setResultType(a.lvalue.getResultType());
        return null;
    }
    @Override
    public Object visit(ReturnStatement s) throws CompilerException {
        if(s.sub != null) {
            s.sub.accept(this);
            Type returnType = s.sub.getResultType();
            if(s.sub instanceof ConstantExpression)
                returnType.constant = ((ConstantExpression)s.sub).literal;
            StringBuilder errorStr = new StringBuilder();
            if(!Type.castable(s.outerScope,
                    returnType, currMethod.getResultType(),
                    errorStr)) {
                if(currMethod.getResultType().isVoid())
                    throw new CompilerException(s.getStreamPos(),
                            ParseException.Code.ILLEGAL,
                            "void method can not return a value");
                else
                    throw new CompilerException(s.getStreamPos(),
                            ParseException.Code.ILLEGAL,
                            "return type mismatch: " + errorStr);
            }
        }
        return null;
    }
    @Override
    public Object visit(ThrowStatement s) throws CompilerException {
        s.sub.accept(this);
        Type thrownType = s.sub.getResultType();
        StringBuilder errorStr = new StringBuilder();
        if(!Type.castable(s.outerScope,
                thrownType, new Type(new NameList(
                    s.outerScope.getBoundingMethodScope().getBoundingJavaClass().
                    frontend.rootExceptionClassName)),
                errorStr)) {
            throw new CompilerException(s.getStreamPos(),
                    ParseException.Code.ILLEGAL,
                    "object is not an exception: " + errorStr);
        }
        // as the "throws" is not implemented, only unchecked
        // exceptions are possible
        if(!Type.castable(s.outerScope,
                thrownType, new Type(new NameList(
                    s.outerScope.getBoundingMethodScope().getBoundingJavaClass().
                    frontend.uncheckedExceptionClassName)),
                errorStr)) {
            throw new CompilerException(s.getStreamPos(),
                    ParseException.Code.ILLEGAL,
                    "only unchecked exceptions are supported");
        }
        return null;
    }
    @Override
    public Object visit(LabeledStatement s) throws CompilerException {
        s.sub.accept(this);
        return null;
    }
    @Override
    public Object visit(JumpStatement s) throws CompilerException {
        return null;
    }
    @Override
    public Object visit(BranchStatement s) throws CompilerException {
        s.condition.accept(this);
        if(/* !s.condition.getResultType().isPrimitive() || */
                !s.condition.getResultType().isBoolean())
            throw new ParseException(s.condition.getStreamPos(),
                    ParseException.Code.ILLEGAL,
                    "found " + s.condition.getResultType().toNameString() +
                    ", required boolean");
        return null;
    }
    @Override
    public Object visit(SynchronizedStatement e) throws CompilerException {
        e.lock.accept(this);
        e.block.accept(this);
        Type lockType = e.lock.getResultType();
        if(!lockType.isJava())
            throw new CompilerException(e.getStreamPos(),
                    ParseException.Code.ILLEGAL,
                    "lock must be an object"); // !!! interfaces
        return null;
    }
    @Override
    public Object visit(Block b) throws CompilerException {
        CompilerException semanticErrors = new CompilerException();
        for(AbstractStatement s: b.code) {
            try {
                s.accept(this);
            } catch(CompilerException e) {
                semanticErrors.addAllReports(e);
            }
        }
        if(semanticErrors.reportsExist())
            throw semanticErrors;
        return null;
    }
    /**
     * Checks if the indexing expression type is valid, that is,
     * if it is an integer and its numeric precision is not
     * larger than that declared in frontend.
     * 
     * @param indexing                  indexing expression to test
     * @param frontend                  frontend
     */
    public static void checkIndexType(AbstractExpression indexing,
            AbstractFrontend frontend) throws CompilerException {
        int precision = indexing.getResultType().numericPrecision();
        if(precision == 0)
            throw new CompilerException(indexing.getStreamPos(),
                    ParseException.Code.ILLEGAL,
                    "index is not a numeric type");
        else if(precision > frontend.maxArrayIndexPrecision)
            throw new CompilerException(indexing.getStreamPos(),
                    ParseException.Code.ILLEGAL,
                    "index is not integer");
    }
    /**
     * Checks if a class extends/implements proper classes, that is, if there are
     * no circular references and only interfaces are implemented. No access
     * rights are tested.
     *
     * All classed in the compilation must already have their types parsed.
     *
     * @param clazz                     class to check
     */
    void checkClassDependency(AbstractJavaClass clazz) throws ParseException {
        ParseException errors = new ParseException();
        String s;
        if(clazz instanceof ImplementingClass)
            s = "class";
        else
            s = "interface";
        // check "extends"
        Set<AbstractJavaClass> trace = new HashSet<>();
        trace.add(clazz);
        AbstractJavaClass currentClass = clazz;
        while(currentClass.extendS != null) {
            AbstractJavaClass superClass = clazz.scope.
                    lookupJavaClassNA(currentClass.extendS.getJava());
            if(currentClass instanceof Interface) {
                if(!superClass.getQualifiedName().toString().equals(
                        currentClass.frontend.objectClassName))
                    throw new RuntimeException("interface can extend only the object class");
            }
            if(superClass instanceof Interface) {
                // possible other interface extension in this dependency chain will be
                // reported for classes that directly extend the interfaces
                if(currentClass == clazz)
                    errors.addReport(new Report(clazz.getStreamPos(),
                        ParseException.Code.ILLEGAL,
                        "class extends interface " + superClass.getQualifiedName()));
                break;
            }
            if(superClass.flags.allowsMutatorFlag &&
                    !clazz.flags.allowsMutatorFlag) {
                // report only once per each class
                if(currentClass == clazz)
                    errors.addReport(new Report(clazz.getStreamPos(),
                        ParseException.Code.ILLEGAL,
                        s + " forbids mutator flag, but superclass " +
                        "does not"));
                break;
            }
            if(clazz.flags.allowsMutatorFlag && !clazz.frontend.mutatorFlags)
                throw new RuntimeException("the frontend does not allow mutator flags");
            if(trace.contains(superClass)) {
                errors.addReport(new Report(clazz.getStreamPos(),
                    ParseException.Code.ILLEGAL,
                    "circular class dependency"));
                break;
            }
            currentClass = superClass;
            trace.add(currentClass);
        }
        // check "implements"
        trace.clear();
        trace.add(clazz);
        Queue<Interface> interfaces = new LinkedList<Interface>();
        for(Type t : clazz.implementS) {
            AbstractJavaClass i = clazz.scope.lookupJavaClassNA(t.getJava());
            if(i instanceof Interface)
                interfaces.add((Interface)i);
            else {
                errors.addReport(new Report(clazz.getStreamPos(),
                    ParseException.Code.ILLEGAL,
                    s + " implements class " + i.getQualifiedName()));
            }
            if(i.flags.allowsMutatorFlag &&
                    !clazz.flags.allowsMutatorFlag) {
                // report only once per each class
                if(currentClass == clazz) {
                    String q;
                    if(clazz instanceof ImplementingClass)
                        q = "implemented";
                    else
                        q = "extended";
                    errors.addReport(new Report(clazz.getStreamPos(),
                        ParseException.Code.ILLEGAL,
                        s + " forbids mutator flag, but " + q + " interface " +
                        i.getQualifiedName() + " does not"));
                }
                break;
            }
        }
        Interface interfacE;
        while((interfacE = interfaces.poll()) != null) {
            for(Type t : interfacE.implementS) {
                AbstractJavaClass i = interfacE.scope.lookupJavaClassNA(t.getJava());
                if(i instanceof Interface)
                    interfaces.add((Interface)i);
                else
                    // interface extension in this dependency tree is
                    // reported only for classes that directly extend
                    // the interfaces to avoid repetitive errors
                    ;
            }
            if(trace.contains(interfacE)) {
                errors.addReport(new Report(clazz.getStreamPos(),
                    ParseException.Code.ILLEGAL,
                    "non-tree interface dependency"));
                break;
            }
            trace.add(interfacE);
        }
        if(errors.reportsExist())
            throw errors;
    }
    /**
     * Calls <code>checkClassDependency(AbstractJavaClass)</code>
     * for all classes in the compilation.
     *
     * @param compilation               compilation to check
     */
    void checkClassDependency(Compilation compilation) throws ParseException {
        ParseException errors = new ParseException();
        for(AbstractJavaClass clazz : compilation.getClasses())
            try {
                checkClassDependency(clazz);
                clazz.subclasses = new HashSet<>();
            } catch(ParseException e) {
                e.completePos(clazz.getStreamPos());
                errors.addAllReports(e);
            }
        if(errors.reportsExist())
            throw errors;
    }
    /**
     * Completes the fields <code>AbstractJavaClass.subclasses</code>.
     *
     * @param compilation               compilation
     */
    void completeSubclasses(Compilation compilation) throws ParseException {
        for(AbstractJavaClass clazz : compilation.getClasses()) {
            Set<AbstractJavaClass> subclasses = new HashSet<>(clazz.subclasses);
            subclasses.add(clazz);
            AbstractJavaClass currentClass = clazz;
            while(currentClass.extendS != null) {
                AbstractJavaClass superClass = clazz.scope.
                        lookupJavaClassNA(currentClass.extendS.getJava());
                superClass.subclasses.addAll(subclasses);
                subclasses.add(currentClass);
                currentClass = superClass;
            }
        }
    }
    /**
     * Used for both implementing/overriding methods and for methods
     * that do not implement/override any other method. In the latter case,
     * <code>superMethod<code> must be null. This method must be called for
     * all translated methods.<br>
     *
     * Checks if method/super method override/implement flags,
     * return types and access privileges match.
     * 
     * @param method                    method
     * @param superMethod               super method, null for none
     * @param errors                    exception to add possible error
     *                                  reports
     */
    void checkMethodPair(Method method, Method superMethod, CompilerException errors) {
        AbstractJavaClass clazz = method.owner;
        AbstractJavaClass superClass;
        if(superMethod == null)
            superClass = null;
        else
            superClass = superMethod.owner;
        StringBuilder errorStr = new StringBuilder();
        MethodFlags f = method.flags;
/*if(method.signature.toString().indexOf("diffContext") != -1)
    method = method;*/
        if(superMethod != null) {
            MethodFlags sf = superMethod.flags;
            if(f.context != sf.context)
                errors.addReport(new Report(method.getStreamPos(),
                    ParseException.Code.ILLEGAL,
                    "shadows a method that has a different context"));
            else {
                if(f.implementS == MethodFlags.Implements.FALSE &&
                        sf.isAbstract())
                    errors.addReport(new Report(method.getStreamPos(),
                        ParseException.Code.ILLEGAL,
                        "implements a method but declares otherwise"));
                if(f.overrides == MethodFlags.Overrides.FALSE &&
                        !sf.isAbstract())
                    if(sf.context == Context.NON_STATIC)
                        errors.addReport(new Report(method.getStreamPos(),
                            ParseException.Code.ILLEGAL,
                            "overrides a method but declares otherwise"));
                    else
                        errors.addReport(new Report(method.getStreamPos(),
                            ParseException.Code.ILLEGAL,
                            "shadows a method but declares otherwise"));
                if(f.implementS == MethodFlags.Implements.TRUE &&
                        !sf.isAbstract())
                    errors.addReport(new Report(method.getStreamPos(),
                        ParseException.Code.ILLEGAL,
                        "non-abstract method in " + superMethod.getStreamPos() +
                        " can not be implemented"));
                if(f.overrides == MethodFlags.Overrides.TRUE &&
                        sf.isAbstract())
                    errors.addReport(new Report(method.getStreamPos(),
                        ParseException.Code.ILLEGAL,
                        "abstract method in " + superMethod.getStreamPos() +
                        " can not be overridden"));
                if(f.isAbstract() && !sf.isAbstract())
                    errors.addReport(new Report(method.getStreamPos(),
                        ParseException.Code.ILLEGAL,
                        "non-abstract method in " + superMethod.getStreamPos() +
                        " overridden by abstract one"));
                String s;
                if(superMethod.flags.isAbstract())
                    s = "implement";
                else
                    s = "override";
                try {
                    if(!(method.getResultType().equals(
                            superMethod.getResultType())) &&
                            !Type.castable(method.scope, method.getResultType(),
                            superMethod.getResultType(), errorStr))
                        errors.addReport(new Report(method.getStreamPos(),
                                ParseException.Code.ILLEGAL,
                                "method " +
                                method.getDeclarationDescription(true) +
                                " in ${" + clazz.getQualifiedName() + "}" +
                                " can not " + s + " " +
                                superMethod.getDeclarationDescription(true) +
                                " in " + superClass.getQualifiedName() +
                                ": wrong return type: " + errorStr));
                } catch(ParseException e) {
                    throw new RuntimeException("unexpected parse exception: " +
                            e.toString());
                }
                if(method.flags.mutator != superMethod.flags.mutator) {
                    String t;
                    if(method.flags.mutator)
                        t = "has ";
                    else
                        t = "has no ";
                    t += "mutator flag and super method ";
                    if(superMethod.flags.mutator)
                        t += "has ";
                    else
                        t += "does not have ";
                    t += "one";
                    errors.addReport(new Report(method.getStreamPos(),
                            ParseException.Code.ILLEGAL,
                            "method " +
                            method.getDeclarationDescription(true) +
                            " in ${" + clazz.getQualifiedName() + "}" +
                            " can not " + s + " " +
                            superMethod.getDeclarationDescription(true) +
                            " in ${" + superClass.getQualifiedName() + "}" +
                            ": " + t));
                }
                if(sf.am.privilegesGreaterThan(f.am))
                    errors.addReport(new Report(method.getStreamPos(),
                            ParseException.Code.ILLEGAL,
                            "method " +
                            method.getDeclarationDescription(true) +
                            " in ${" + clazz.getQualifiedName() + "}" +
                            " can not " + s + " " +
                            superMethod.getDeclarationDescription(true) +
                            " in ${" + superClass.getQualifiedName() + "}" +
                            ": has weaker access privileges, were " +
                            "${" + sf.am.toString() + "}"));
                if(!sf.allowsOverride)
                    if(sf.context != Context.NON_STATIC)
                        errors.addReport(new Report(method.getStreamPos(),
                            ParseException.Code.ILLEGAL,
                            "shadows a method in " +
                            superMethod.getStreamPos() + " that does not allow shadowing"));
                    else
                        errors.addReport(new Report(method.getStreamPos(),
                            ParseException.Code.ILLEGAL,
                            "overrides a method in " +
                            superMethod.getStreamPos() + " that does not allow overriding"));
            }
        } else {
            if(f.implementS == MethodFlags.Implements.TRUE)
                errors.addReport(new Report(method.getStreamPos(),
                    ParseException.Code.ILLEGAL,
                    "does not implement a method but declares otherwise"));
            if(f.overrides == MethodFlags.Overrides.TRUE)
                errors.addReport(new Report(method.getStreamPos(),
                    ParseException.Code.ILLEGAL,
                    "does not override a method but declares otherwise"));
        }
        if(method.flags.mutator && !clazz.flags.allowsMutatorFlag)
            errors.addReport(new Report(method.getStreamPos(),
                ParseException.Code.ILLEGAL,
                "mutator flag not allowed in this class"));
        if(method.flags.mutator && !clazz.frontend.mutatorFlags)
            throw new RuntimeException("the frontend does not allow mutator flags");
    }
    /**
     * Recursively searches for interface super methods in interfaces declared in
     * in <code>implementS</code> of a given class <code>clazz</code>. Does not
     * check access rights of the interface methods as that is done in
     * <code>checkAbstractMethods</code>.
     * 
     * @param method                    method whose interface super methods
     *                                  are searched
     * @param clazz                     class that directly declared implementation
     *                                  of some interfaces
     * @param errors                    exception to add possible error
     *                                  reports
     * @return if at least a single supermethod has been found
     */
    boolean searchImplements(Method method, AbstractJavaClass clazz,
            CompilerException errors) throws ParseException {
        boolean found = false;
        // "implementS" nesting level
        Map<Interface, Integer> levels = new HashMap<>();
        // shadowed valid on current nesting level
        boolean shadowed = false;
        // prepares <code>shadowed</code> for possible next nesting
        // level
        boolean shadowedNew = false;
        Queue<Interface> interfaces = new LinkedList<>();
        for(Type t : clazz.implementS) {
            Interface interfacE = (Interface)clazz.scope.lookupJavaClassNA(t.getJava());
            levels.put(interfacE, 0);
            interfaces.add(interfacE);
        }
        int currLevel = 0;
        Interface interfacE;
        while((interfacE = interfaces.poll()) != null) {
            int level = levels.get(interfacE);
            if(currLevel < level) {
                shadowed = shadowedNew;
                currLevel = level;
            }
            for(Type t : interfacE.implementS) {
                Interface i = (Interface)interfacE.scope.lookupJavaClassNA(t.getJava());
                levels.put(i, levels.get(interfacE) + 1);
                interfaces.add(i);
            }
            Method superMethod = interfacE.scope.methods.get(method.signature);
            if(superMethod != null) {
                found = true;
                // reason for this condition: let <code>method</code> be z;
                // if x is implemented by y and y is implemented/overridden
                // by z, then check only y and z here, because x and y
                // are checked when <code>method</code> is y
                if(!shadowed) {
                    // this condition exists because there can be many ways to
                    // a single interface
                    if(!method.directSuperMethods.containsKey(interfacE)) {
                        checkMethodPair(method, superMethod, errors);
                        // not yet in <code>shadowed</code>, because the super method
                        // does not shadow other methods in the same nesting level
                        shadowedNew = true;
                        method.directSuperMethods.put(interfacE, superMethod);
                        superMethod.directSubMethods.put(method.owner, method);
                    }
                }
                if(!method.flags.isAbstract())
                    superMethod.overridings.put(method.owner, method);
            }/* else if(!shadowed)
                checkMethodPair(method, null, errors); */
        }
        return found;
    }
    /**
     * Checks methods implemented/overridden by a given one and registers
     * that method in possible super methods.<br>
     *
     * A method with access modifiers PRIVATE or INTERNAL can never be
     * overridden, even by a method with the same signature.<br>
     * 
     * @param method                    method to test
     */
    void checkOverriding(Method method) throws CompilerException {
        CompilerException errors = new CompilerException();
        AbstractJavaClass clazz = method.owner;
        AbstractJavaClass currentClass = clazz;
        boolean found = searchImplements(method, currentClass, errors);
        boolean shadowed = false;
        while(currentClass.extendS != null) {
            AbstractJavaClass superClass = method.scope.
                    lookupJavaClassNA(currentClass.extendS.getJava());
            Method superMethod;
            // search only for methods directly in <code>superClass</code>
            superMethod = superClass.scope.methods.get(
                    method.signature);
            if(superMethod != null) {
                try {
                    method.owner.scope.testAccessibility(superClass,
                        superMethod.flags.am, null);
                } catch(ParseException e) {
                    superMethod = null;
                }
            }
/*if(method.signature.toString().indexOf("notOverri") != -1)
    method = method;*/
            if(superMethod != null) {
                // found in super
                found = true;
                // reason for the following condition: let <code>method</code> be z;
                // if x is implemented by y and y is implemented/overridden
                // by z, then check only y and z here, because x and y
                // are checked when <code>method</code> is y
                if(!shadowed) {
                    checkMethodPair(method, superMethod,
                            errors);
                    shadowed = true;
                    method.directSuperMethods.put(superClass, superMethod);
                    superMethod.directSubMethods.put(method.owner, method);
                }
                if(!method.flags.isAbstract())
                    superMethod.overridings.put(clazz, method);
            } /* else if(!shadowed && superClass.extendS == null)
                // so there is no super method
                checkMethodPair(method, null, errors); */
/*
superMethod = superClass.scope.methods.get(
        method.signature);
 */         
            currentClass = superClass;
            if(searchImplements(method, currentClass, errors))
                found = true;
        }
        if(!found)
            // so there is no super method
            checkMethodPair(method, null, errors);
        if(errors.reportsExist())
            throw errors;
    }
    /**
     * Checks if a non--abstract class contains abstract methods. Also
     * checks if all interface methods can be accessed.<br>
     *
     * Can be called only after <code>checkOverriding</code> has been
     * called for all methods.
     *
     * @param clazz                     class to check
     */
    void checkAbstractMethods(AbstractJavaClass clazz) throws ParseException {
        ParseException semanticErrors = new ParseException();
        if(!clazz.flags.isAbstract()) {
            JavaClassScope scope = clazz.scope;
            //
            // all abstract methods must be implemented
            //
            List<Method> abstractMethods = new LinkedList<Method>();
            List<MethodSignature> nonabstractMethods =
                    new LinkedList<MethodSignature>();
            // check methods defined directly in this class
            for(MethodSignature key : scope.methods.keySet()) {
                Method method = scope.methods.get(key);
                if(method.flags.isAbstract())
                    abstractMethods.add(method);
                else
                    nonabstractMethods.add(method.signature);
            }
            // check methods defined in superclasses
            if(abstractMethods.isEmpty() && clazz.extendS != null) {
                List<AbstractJavaClass> inheriting = new LinkedList<AbstractJavaClass>();
                inheriting.add(clazz);
                AbstractJavaClass parent = scope.lookupJavaClassNA(clazz.extendS.getJava());
                inheriting.add(parent);
                while(abstractMethods.isEmpty()) {
                    for(MethodSignature key : parent.scope.methods.keySet()) {
                        Method method = parent.scope.methods.get(key);
                        if(method.flags.isAbstract()) {
                            // check overridings in the local inheritance line
                            boolean overridden = false;
                            for(AbstractJavaClass sub : inheriting)
                                if(method.overridings.containsKey(sub)) {
                                    overridden = true;
                                    break;
                                }
                            if(!overridden)
                                // abstract and not overridden by a non--abstract one in the
                                // current line of inheritance
                                //
                                // it is known that the overridings found are not abstract
                                // as, if they were, <code>abstractMethods.isEmpty()</code>
                                // would have already be true and this loop would already break when
                                // checking the subclass with the overriding
                                abstractMethods.add(method);
                        } else
                            nonabstractMethods.add(method.signature);
                    }
                    if(parent.extendS == null)
                        break;
                    else {
                        parent = scope.lookupJavaClassNA(parent.extendS.getJava());
                        inheriting.add(parent);
                    }
                }
            }
            Queue<Interface> interfaces = new LinkedList<Interface>();
            for(Type t : clazz.implementS) {
                Interface interfacE = (Interface)clazz.scope.
                        lookupJavaClassNA(t.getJava());
                interfaces.add(interfacE);
            }
            Interface interfacE;
            Set<Interface> viewed = new HashSet<Interface>();
            while((interfacE = interfaces.poll()) != null) {
                for(Type t : interfacE.implementS) {
                    Interface i = (Interface)interfacE.scope.lookupJavaClassNA(t.getJava());
                    interfaces.add(i);
                }
                for(Method im : interfacE.scope.methods.values()) {
                    boolean shadowed = false;
                    for(Interface v : viewed)
                        if(im.directSubMethods.containsKey(v)) {
                            shadowed = true;
                            break;
                        }
                    if(!shadowed) {
                        try {
                            clazz.scope.testAccessibility(interfacE,
                                im.flags.am, "interface method " +
                                im.getDeclarationDescription(true) + " in " +
                                im.getStreamPos());
                        } catch(ParseException e) {
                            for(Report r : e.getReports())
                                r.pos = clazz.getStreamPos();
                            semanticErrors.addAllReports(e);
                        }
                        if(!nonabstractMethods.contains(im.signature))
                            semanticErrors.addReport(new Report(clazz.getStreamPos(),
                                ParseException.Code.ILLEGAL,
                                "class is not abstract but does not implement abstract interface method " +
                                im.getDeclarationDescription(true) + " in " +
                                im.getStreamPos()));
                    }
                }
                viewed.add(interfacE);
            }
            for(Method m : abstractMethods)
                semanticErrors.addReport(new Report(clazz.getStreamPos(),
                    ParseException.Code.ILLEGAL,
                    "class is not abstract but contains abstract method " +
                    m.getDeclarationDescription(true) + " in " +
                    m.getStreamPos()));
        }
        if(semanticErrors.reportsExist())
            throw semanticErrors;
    }
    /**
     * Calls <code>checkAbstractMethods(AbstractJavaClass)</code>
     * for all classes in the compilation.
     *
     * @param compilation               compilation to check
     */
    void checkAbstractMethods(Compilation compilation) throws ParseException {
        ParseException errors = new ParseException();
        for(AbstractJavaClass clazz : compilation.getClasses())
            try {
                checkAbstractMethods(clazz);
            } catch(ParseException e) {
                errors.addAllReports(e);
            }
        if(errors.reportsExist())
            throw errors;
    }
    /**
     * Checks if a method's modifiers are valid. As the parser should
     * guarantee that these modifiers are valid, this method throws a
     * runtime exception if an invalid combination is found.<br>
     *
     * This method does not check actual existence or absence of
     * overriding/implementing, as that is done in
     * <code>checkOverriding</code> and <code>checkAbstractMethods</code>.<br>
     *
     * Invalid combinations: implements and override, abstract and
     * implements, abstract and override, abstract and static,
     * static and implements, static and override, abstract and
     * disallowing being implemented, in interface and has
     * private access, in interface and at the same time has protected
     * access and not public access, mutator flag on a static method
     * or if the class does not allow for mutator flags.<br>
     * 
     * @param method                    method to test
     */
    protected void checkMethodModifiers(Method method) {
        MethodFlags f = method.flags;
        if(f.implementS == MethodFlags.Implements.TRUE &&
                f.overrides == MethodFlags.Overrides.TRUE)
            throw new RuntimeException("both implements and override");
        if(f.implementS == MethodFlags.Implements.TRUE &&
                f.isAbstract())
            throw new RuntimeException("abstract can not implement");
        if(f.overrides == MethodFlags.Overrides.TRUE &&
                f.isAbstract())
            throw new RuntimeException("abstract can not override");
        if(f.isAbstract() && f.context != Context.NON_STATIC)
            throw new RuntimeException("can not be both abstract and static");
        if(f.isAbstract() && !f.allowsOverride)
            throw new RuntimeException("method can not be still abstract and " +
                    "already disallowing override");
        if(f.mutator) {
            if(f.context != Context.NON_STATIC)
                throw new RuntimeException("static method can not have mutator flag");
            if(!method.owner.flags.allowsMutatorFlag)
                throw new RuntimeException(method.owner.getGrammaticalName() +
                        " does not allow for mutator flag");
        }
        if(method.owner instanceof Interface) {
            if(!f.am.hasPackageAccess())
                throw new RuntimeException("method in interface must have " +
                    "at least ${internal} access");
            if(!f.am.hasPublicAccess() && f.am.hasProtectedAccess())
                throw new RuntimeException("method in interface can not have " +
                        "${protected} access if not ${public}");
        }
    }
    /**
     * Checks if the type points to a valid java class, and if the
     * type's possible mutator flags is allowed for the type.<br>
     *
     * Also, parses the referenced java class if not yet parsed,
     * and updates the type to the class' fully qualified name. <br>
     * 
     * Also processes the types in parameters.
     * 
     * @param scope                     scope with the object
     * @param type                      type to parse
     */
    public static void parseJavaClass(AbstractScope scope, Type type) throws ParseException {
        AbstractJavaClass javaClass = scope.lookupJavaClassNA(type.getJava());
        String s;
        if(javaClass instanceof ImplementingClass)
            s = "class";
        else
            s = "interface";
        if(javaClass == null)
            throw new ParseException(null, ParseException.Code.LOOK_UP,
                    s + " " + type.toNameString() + " not found");
        if(type.mutatorReference && !javaClass.flags.allowsMutatorFlag)
            throw new ParseException(null, ParseException.Code.LOOK_UP,
                    s + " " + javaClass.getQualifiedName() + " does not allow " +
                    "mutator references");
        type.type = javaClass.getQualifiedName();
        for(Type t : type.parameters)
            if(t.isJava())
                parseJavaClass(scope, t);
    }
    /**
     * Finds the java class of a typed object and updates the return type
     * of that object accordingly. Also finds java classes of arguments
     * and locals if the typed object is Method, and refreshes the object
     * signature if needed.
     * 
     * @param scope                     scope with the object
     * @param typed                     object implementing Typed and
     *                                  SourcePosition
     * @return                          if the return type changed
     */
    public static boolean findJavaClass(AbstractScope scope, Typed typed)
            throws ParseException {
        boolean changed = false;
        Type result = typed.getResultType();
        // check if had not already been searched
        if(!result.javaClassFound) {
            if(result.isJava() /*&& !result.isArrayOfPrimitives()*/) {
                try {
                    parseJavaClass(scope, result);
                } catch(ParseException e) {
                    throw new ParseException(((SourcePosition)typed).
                            getStreamPos(), ParseException.Code.PARSE,
                            e);
                }
/*
                AbstractJavaClass variableJavaClass = scope.lookupJavaClassNA(
                        result.getJava());
                if(variableJavaClass == null)
                // replace class name with a fully qualified one
                typed.setResultType(new Type(variableJavaClass.
                        getQualifiedName(),
                        typed.getResultType().getDimension()));
 */
                changed = true;
            }
            if(typed instanceof Method) {
                boolean signatureChanged = false;
                Method method = (Method)typed;
                for(Variable v : method.arg)
                    if(findJavaClass(scope, v))
                        signatureChanged = true;
                if(signatureChanged) {
                    method.refreshSignature();
                    changed = true;
                }
            }
            result.javaClassFound = true;
        }
        return changed;
    }
    /**
     * Tests, if imports point to existing packages, classes or members.
     * 
     * @param c compilation
     * @param errors semantic errors to complete, if invalid imports
     * are found
     */
    protected void testImports(Compilation c,
            CompilerException errors) throws CompilerException {
        List<AbstractJavaClass> classes = c.getClasses();
        for(Import i : c.imports) {
            if(i.name.list.isEmpty())
                throw new RuntimeException("import name missing");
            if(i.statiC) {
                NameList n = new NameList(i.name);
                // remove the field part, so that a qualified class name is left
                n.list.remove(n.list.size() - 1);
                if(n.list.size() < 2)
                    errors.addReport(new Report(i.pos, CompilerException.Code.MISSING,
                            "qualified member name expected"));
                else {
                    AbstractJavaClass found = null;
                    for(AbstractJavaClass clazz : classes)
                        if(clazz.getQualifiedName().equals(n)) {
                            found = clazz;
                            break;
                        }
                    if(found == null)
                        errors.addReport(new Report(i.pos, CompilerException.Code.MISSING,
                                "class " + n.toString() + " does not exist"));
                    else if(!i.name.last().equals("*")) {
                        // a class exists, now look for the static member
                        String name = i.name.last();
                        boolean memberFound = false;
                        for(Variable v : found.scope.fields.values())
                            if(v.flags.isPublic() &&
                                    ((FieldFlags)v.flags).context != Context.NON_STATIC &&
                                    v.name.equals(name)) {
                                memberFound = true;
                                break;
                            }
                        if(!memberFound)
                            for(Method m : found.scope.methods.values())
                                if(m.flags.am.hasPublicAccess() &&
                                        m.flags.context != Context.NON_STATIC &&
                                        m.name.equals(name)) {
                                    memberFound = true;
                                    break;
                                }
                        if(!memberFound)
                            errors.addReport(new Report(i.pos, CompilerException.Code.MISSING,
                                    "public static member " + name + " does not exist"));
                    }
                }
            } else {
                NameList n = new NameList(i.name);
                // leave only a package's name
                n.list.remove(n.list.size() - 1);
                if(n.list.isEmpty())
                    errors.addReport(new Report(i.pos, CompilerException.Code.MISSING,
                            "qualified class name expected"));
                else {
                    String name = n.toString();
                    boolean found = false;
                    for(PackageScope p : c.packages.packageScopes.values())
                        if(name.equals((String)p.owner)) {
                            found = true;
                            break;
                        }
                    if(!found)
                        errors.addReport(new Report(i.pos, CompilerException.Code.MISSING,
                                "package " + name + " does not exist"));
                    else if(!i.name.last().equals("*")){
                        // a package exists, now check for the class
                        found = false;
                        for(AbstractJavaClass clazz : classes)
                            if(clazz.getQualifiedName().equals(i.name)) {
                                found = true;
                                break;
                            }
                        if(!found)
                            errors.addReport(new Report(i.pos, CompilerException.Code.MISSING,
                                    "class " + i.name.toString() + " does not exist"));
                    }
                }
            }
        }
    }
    /**
     * Tests classes, excluding code.
     * 
     * @param c compilation
     * @param errors semantic errors to complete, if invalid imports
     * are found
     */
    protected void testClasses(Compilation c,
            CompilerException errors) throws CompilerException {
        for(String packagE : c.packages.packageScopes.keySet()) {
            log(0, "checking package `" + packagE + "'");
            PackageScope p = c.packages.packageScopes.get(packagE);
            for(String javaClass : p.javaClassScopes.keySet()) {
                log(1, "checking java class `" + javaClass + "'");
                JavaClassScope j = p.javaClassScopes.get(javaClass);
                if(j.getExtends() != null) {
                    try {
                        parseJavaClass(j, j.getExtends());
                    } catch(CompilerException e) {
                        throw new CompilerException(((AbstractJavaClass)j.owner).
                            getStreamPos(), ParseException.Code.PARSE,
                            e.toString());
                    }
                    if(((AbstractJavaClass)(j.owner)).frontend !=
                            // already parsed
                            j.lookupJavaClassNA(j.getExtends().getJava()).frontend)
                        errors.addReport(new Report(((AbstractJavaClass)j.owner).
                            getStreamPos(), ParseException.Code.ILLEGAL,
                            "class has a superclass in another language"));
                    log(2, "extends " + j.getExtends().toString());
                }
                for(Variable variable : j.fields.values()) {
                    try {
                        findJavaClass(j, variable);
                    } catch(ParseException e) {
                        errors.addAllReports(e);
                    }
                    String m = "";
                    if(((FieldFlags)variable.flags).context != Context.NON_STATIC)
                        m += "static ";
                    log(2, "checked variable `" + variable.name + "' " + m);
                    log(3, "type " + variable.getResultType().toString());
                }
                for(MethodSignature key : new LinkedList<>(
                        j.methods.keySet())) {
                    Method method = j.methods.get(key);
                    checkMethodModifiers(method);
// if(method.signature.name.indexOf("getSize") != -1)
//     method = method;
                    try {
                        // find java class of arguments...
                        if(findJavaClass(j, method)) {
                                // the signature may have changed
                                j.methods.remove(key);
                                j.methods.put(method.signature, method);
                                int i = j.listOfMethods.indexOf(key);
                                j.listOfMethods.remove(i);
                                j.listOfMethods.add(i, method.signature);
                        }
                        // ...and locals
                        for(Variable v : method.getLocals())
                            // bound variables created in
                            // <code>parsePrimaryPrimitiveRangeText</code> have their types
                            // completed lated in semantic check, so test if the variable
                            // already has a type
                            if(v.type != null)
                                findJavaClass(j, v);
                    } catch(ParseException e) {
                        errors.addAllReports(e);
                    }
                    String m = "";
                    if(method.flags.isAbstract())
                        m += "abstract ";
                    if(method.flags.context != Context.NON_STATIC)
                        m += "static ";
                    if(method.flags.synchronizeD)
                        m += "synchronized ";
                    log(2, "checked method `" + method.signature.toString() + "' " + m);
                    log(3, "return type " + method.getResultType().toString());
                }
            }
        }
    }
    /**
     * Tests code.
     * 
     * @param c compilation
     * @param errors semantic errors to complete, if invalid imports
     * are found
     */
    protected void testCode(Compilation c,
            CompilerException errors) throws CompilerException {
        for(String packagE : c.packages.packageScopes.keySet()) {
            log(0, "checking code in package `" + packagE + "'");
            PackageScope p = c.packages.packageScopes.get(packagE);
            for(String javaClass : p.javaClassScopes.keySet()) {
                log(1, "checking code in java class `" + javaClass + "'");
                JavaClassScope j = p.javaClassScopes.get(javaClass);
                AbstractJavaClass currentJavaClass = (AbstractJavaClass)j.owner;
                // test accessibility of superclass
                try {
                    if(currentJavaClass.extendS != null)
                        j.lookupJavaClassAT(currentJavaClass.extendS.getJava());
                } catch(ParseException e) {
                    errors.addAllReports(new CompilerException(
                            currentJavaClass.getStreamPos(), null, e));
                }
                for(MethodSignature key : new LinkedList<>(j.methods.keySet())) {
                    Method method = j.methods.get(key);
                    String m = "";
                    if(method.flags.context != Context.NON_STATIC)
                        m += "static ";
                    if(method.flags.synchronizeD)
                        m += "synchronized ";
                    if(method.flags.isAbstract() && method.flags.context != Context.NON_STATIC)
                        errors.addReport(new Report(method.getStreamPos(),
                            ParseException.Code.ILLEGAL,
                            "method is both abstract and static"));
                    try {
                        checkOverriding(method);
                    } catch(CompilerException e) {
                        errors.addAllReports(e);
                    }
                    log(2, "checking code in method `" +
                            method.signature.toString() + "' " + m);
//if(method.signature.name.indexOf("main") != -1)
//    method = method;
                    log(3, "return type " + method.getResultType().toString());
                    if(!method.flags.isAbstract()) {
                        currMethod = method;
                        try {
                            visit(method.code);
                        } catch(CompilerException e) {
                            errors.addAllReports(e);
                        }
                        currMethod = null;
                    }
                }
            }
        }
    }
    /**
     * Checks the compilation.
     * 
     * When determining extended or implemented java classs,
     * PACKAGE scope does not fit because it does
     * not have the list of imports of a java class,
     * JAVA_TYPE scope does not fit too because it
     * contains fields that should not be visible here
     * so the PACKAGE scope is used with a temporarily added
     * list of imports of the JAVA_TYPE scope.
     * 
     * @param c                         compilation
     */
    @Override
    public Object visit(Compilation c) throws CompilerException {
        CompilerException semanticErrors = new CompilerException();
        testImports(c, semanticErrors);
        testClasses(c, semanticErrors);
        if(semanticErrors.reportsExist())
            throw semanticErrors;
        compilation = c;
        checkClassDependency(compilation);
        completeSubclasses(compilation);
        testCode(c, semanticErrors);
        if(!semanticErrors.reportsExist()) {
            try {
                checkAbstractMethods(c);
            } catch(ParseException e) {
                semanticErrors.addAllReports(e);
            }
        }
        if(semanticErrors.reportsExist())
            throw semanticErrors;
        return null;
    }
    protected void log(int indentLevel, String s) {
        if(verbose) {
            System.out.print("S ");
            for(int i = 0; i < indentLevel; ++i)
                System.out.print("  ");
            System.out.println(s);
        }
    }
}
