/*
 * VariableInitializer.java
 *
 * Created on Jul 31, 2009, 2:10:43 PM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.primitiverange.PrimitiveRangeDescription;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Variable;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.AbstractExpression;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.TextRange;

/**
 * A variable initializer -- contains the variable's initial value
 * and optionally primitive range of the variable.<br>
 *
 * The fields <code>rangeMin</code> and <code>rangeMax</code> should
 * both be either null or non--null.<br>
 *
 * The minimum or maximum value variables should be initialized just before
 * initialization of this initializer's variable, by having the same
 * owner and being initialized within the code directly before.<br>
 *
 * @author Artur Rataj
 */
public class VariableInitializer {
    /**
     * Variable initializer by this initializer, null for primary
     * expression's range.
     */
    public Variable variable;
    /**
     * Null if none, one element for non--array initializer,
     * one or more elements for array initializer, no elements
     * if a parse error occured when parsing the initializer.<br>
     *
     * If null, a default initializer should be assigned to this
     * field.
     */
    public List<AbstractExpression> value = null;
    /**
     * Primitive range description, can not be null. For no primitive range,
     * appropriate fields in this object are null.
     */
    public PrimitiveRangeDescription range;

    /**
     * Creates a new variable initializer.
     *
     * @param variable                  variable initializer by this
     *                                  initializer
     */
    public VariableInitializer(Variable variable) {
        this.variable = variable;
        range = new PrimitiveRangeDescription();
    }
    @Override
    public String toString() {
        String s = variable.toString();
        if(value != null)
            s = s + " " + value.toString();
        if(range != null)
            s = range.toString() + " " + s;
        return s;
    }
}
