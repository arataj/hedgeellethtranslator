/*
 * CodeVariable.java
 *
 * Created on Mar 19, 2008
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.code;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.Literal;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Typed;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * A value, either a constant or a variable, to be used by the code.
 * 
 * @author Artur Rataj
 */
public abstract class AbstractCodeValue implements Typed, SourcePosition, Cloneable {
    /**
     * A bogus code value, to be used when a real code
     * value is not needed. For example, used by
     * <code>Generator.checkUninitializedAndDead</code>
     * to declare that an argument is initialized with
     * some value.<br>
     *
     * Do not rely on anything regarding this constant, but
     * that it is some <code>AbstractCodeValue</code>.
     */
    public static final AbstractCodeValue BOGUS_CODE_VALUE =
            new CodeConstant(null, new Literal("##BOGUS_CODE_VALUE##", null));

    /**
     * Position in the parsed stream, null if none.
     */
    protected StreamPos pos;
    
    /**
     * Creates a new instance of CodeValue.
     * 
     * @param pos                       position in the parsed stream,
     *                                  null if none
     */
    public AbstractCodeValue(StreamPos pos) {
        setStreamPos(pos);
    }
    /**
     * If this value contains a restrictive source level primitive range, either
     * variable one, or primary one. The latter may occur only on
     * index.<br>
     * 
     * For details on restrictive source ranges, see
     * <code>CodeVariable.hasRestrictiveSourceLevelRange()</code>.
     *
     * @return                          if contains a source level
     *                                  primitive range
     */
    public abstract boolean hasRestrictiveSourceLevelRange();
    @Override
    public final void setStreamPos(StreamPos pos) {
        this.pos = pos;
    }
    @Override
    public final StreamPos getStreamPos() {
        return pos;
    }
    /**
     * Like toString(), but returns only names, not types.
     *
     * @return                          description of this value,
     *                                  excluding types
     */
    abstract public String toNameString();
    @Override
    public AbstractCodeValue clone() {
        Object o;
        try {
            o = super.clone();
        } catch(CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
        AbstractCodeValue out = (AbstractCodeValue)o;
        if(pos != null)
            out.pos = new StreamPos(pos);
        return out;
    }
}
