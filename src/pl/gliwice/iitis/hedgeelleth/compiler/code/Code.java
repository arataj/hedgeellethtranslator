/*
 * Code.java
 *
 * Created on Feb 22, 2008, 1:49:51 PM
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.code;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.CommentTag;
import pl.gliwice.iitis.hedgeelleth.compiler.code.op.*;

/**
 * A list of operations.
 * 
 * @author Artur Rataj
 */
public class Code {
    /**
     * List of operations.
     */
    public List<AbstractCodeOp> ops;
    /**
     * A value with the result and optional primitive range,
     * or null for none.<br>
     *
     * Used only for code generation, then this value is
     * meaningless.
     */
    public RangedCodeValue result;
    /**
     * If the result is a temporary variable, made only to hold the result, and
     * thus the variable can be discarded by an up--tree expression in the
     * generator.<br>
     * 
     * The default is false.<br>
     * 
     * Used only for code generation, then this value is
     * meaningless.
     */
    public boolean resultDiscardable  = false;
    
    /**
     * Creates a new instance of Code.
     */
    public Code() {
        ops = new ArrayList<>();
        result = null;
    }
    /**
     * A copying constructor.
     * 
     * The list is copied, and the operations are cloned as
     * described in <code>CodeOp.clone()</code>.
     * 
     * @param code                      code to copy
     */
    public Code(Code code) {
        this();
        for(AbstractCodeOp op : code.ops)
            ops.add(op.clone());
        result = code.result;
    }
    /**
     * Adds a given operation to the end of the operations in this code.
     * 
     * This method does not do anything else, direct addition to the list
     * can also be done.
     * 
     * @param code                      an operation to add
     */
    public void add(AbstractCodeOp op) {
        ops.add(op);
    }
    /**
     * Adds given operations to the end of the operations in this code.
     * 
     * This method does not do anything else, direct addition to the list
     * can also be done.
     * 
     * @param code                      code with operations to add
     */
    public void add(Code code) {
        ops.addAll(code.ops);
    }
    /**
     * Marks all operations in this code as the ones that can be dead.
     */
    public void setCanBeDead() {
        for(AbstractCodeOp op : ops)
            op.canBeDead = true;
    }
    /**
     * Replaces a given operation, cloning its possible label
     * if the replacement operation does not have one.
     * Applies tags, with 
     * 
     * @param index                     index with the operation to
     *                                  replace
     * @param op                        replacement operation
     * @param appendTags                if to append tags, with
     * <code>inlineRecursion</code> and
     * <code>inlineRecursionFirstOp<code> being false
     */
    public void replace(int index, AbstractCodeOp op, boolean appendTags) {
        AbstractCodeOp prev = ops.get(index);
        if(op.label == null && prev.label != null)
                op.label = new CodeLabel(prev.label);
        if(appendTags)
            op.appendTags(prev, false, false);
        ops.set(index, op);
    }
    /**
     * Returns indices of all operations, that jump into
     * one at the index.
     * 
     * @param index index of the analyzed operation
     * @return indices of operations, from which
     * originate the jumps; empty list for none
     */
    public SortedSet<Integer> getJumpSources(int index) {
        SortedSet<Integer> sources = new TreeSet<>();
        CodeLabel label = ops.get(index).label;
        if(label != null) {
            int count = 0;
            for(AbstractCodeOp op : ops) {
                Collection<CodeLabel> refs = op.getLabelRefs();
                for(CodeLabel r : refs)
                    if(r.equals(label))
                        sources.add(count);
                ++count;
            }
        }
        return sources;
    }
    /**
     * Returns indices of all operations, to which
     * the one at <code>index</code> might jump,
     * i. e. contains label references to them.
     * 
     * @param index index of the analyzes operation
     * @return indices of operations, that are targeted
     * by the jumps; empty list for none
     */
    public SortedSet<Integer> getJumpTargets(int index) {
        SortedSet<Integer> targets = new TreeSet<>();
        Set<CodeLabel> labelRefs = ops.get(index).getLabelRefs();
        for(CodeLabel r : labelRefs) {
            int count = 0;
            for(AbstractCodeOp op : ops) {
                CodeLabel label = op.label;
                if(label != null && r.equals(label))
                    targets.add(count);
                ++count;
            }
        }
        return targets;
    }
    /**
     * If the operation at the index is target of some jump
     * or branch within this code.<br>
     * 
     * It is a faster equivalent of
     * <code>!getJumpSources(index).isEmpty()</code>.
     * 
     * @param index                     operation index
     * @return                          if is target of jump or
     *                                  branch
     */
    public boolean isJumpTarget(int index) {
        boolean found = false;
        CodeLabel label = ops.get(index).label;
        if(label != null) {
            SEARCH:
            for(AbstractCodeOp op : ops) {
                Collection<CodeLabel> refs = op.getLabelRefs();
                for(CodeLabel r : refs)
                    if(r.equals(label)) {
                        found = true;
                        break SEARCH;
                    }
            }
        }
        return found;
    }
    /**
     * If a given operation possibly passes control to the next one.
     * Takes into account also normal flow of control outside of jumps.
     * 
     * @param index index of the operation to analyze
     * @return if possibly followed by a next operation; never true
     * for a last operation in a method
     */
    public boolean possiblyFollowedByNext(int index) {
        if(index == ops.size() - 1)
            return false;
        if(getJumpTargets(index).contains(index + 1))
            return true;
        AbstractCodeOp op = ops.get(index);
        if(op instanceof CodeOpJump ||
                op instanceof CodeOpReturn ||
                op instanceof CodeOpThrow)
            return false;
        if(op instanceof CodeOpBranch) {
            CodeOpBranch b = (CodeOpBranch)op;
            return b.labelRefFalse == null || b.labelRefTrue == null;
        }
        return true;
    }
//    /**
//     * In how many operations a local variable is a source variable,
//     * including parts of references.<br>
//     *
//     * A typical application is in merging of two subsequent
//     * instructions by the backend into a single one: if it is
//     * known, that the latter of the two instruction reads the
//     * variable, then calling this method ensures, that the next
//     * instruction is the only one within the code that reads
//     * that variable, thus, the variable can be, for example,
//     * liquidated and the instructions can be merged into some
//     * backend--specific instruction.<br>
//     * 
//     * Replaced by
//     * <code>AbstractTADDGenerator.resultUsedAtMostInNext()</code>.
//     * 
//     * @param v                         variable to test
//     * @return                          number of operations
//     */
//    public int localSourceOpNum(CodeVariable v) {
//        int count = 0;
//        for(AbstractCodeOp op : ops)
//            for(CodeVariable s : op.getLocalSources())
//                if(s == v)
//                    ++count;
//        return count;
//    }
    /**
     * Offsets all label reference indices by a given amount.
     * The indices not determined are ignored.
     * 
     * Usually used when some other code is inserted before or
     * into this code, to correct the label references.
     * 
     * @param minRefIndex               minimum original value of an index
     *                                  in a label reference that
     *                                  makes that index to be modified
     *                                  bye the offset
     * @param offset                    amount by which the indices
     *                                  have to be modified
     */
    public void offsetLabelRefIndices(int minRefIndex, int offset) {
        for(AbstractCodeOp op : ops) {
            Collection<CodeLabel> labels = op.getLabelRefs();
            for(CodeLabel l : labels)
                if(l.codeIndex != -1 &&
                    l.codeIndex >= minRefIndex)
                    l.codeIndex += offset;
        }
    }
    /**
     * Appends given tags to tag lists in all operations in this code.
     * 
     * @param tags                      tags to propagate
     */
    public void appendTags(List<CommentTag> tags) {
        for(AbstractCodeOp op : ops)
            op.addTags(tags);
    }
    /**
     * Like toString(), but variables have names relative to the context.
     * 
     * @param method                    code method
     * @return                          description
     */
    public String toString(CodeMethod method) {
        String s = "";
        int count = 0;
        for(AbstractCodeOp op : ops) {
            String line = " " + count;
            while(line.length() < 3)
                line = " " + line;
            s += line + " " + op.toString(method) + "\n";
            ++count;
        }
        return s;
    }
    @Override
    public String toString() {
        return toString(null);
    }
}
