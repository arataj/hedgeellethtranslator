/*
 * CodeClass.java
 *
 * Created on Feb 28, 2008, 11:11:26 AM
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.code;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.CommentTag;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.CompilerException;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.SourcePosition;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.StreamPos;
import pl.gliwice.iitis.hedgeelleth.modules.AbstractFrontend;

/**
 * An output class, with code compiled to the internal
 * assembler.
 * 
 * @author Artur Rataj
 */
public class CodeClass implements CodeVariableOwner, SourcePosition {
    /**
     * Frontend of this class, needed for conversion of special
     * classes, null for none.
     */
    public AbstractFrontend frontend;
    /**
     * Position in the input stream.
     */
    protected StreamPos streamPos;
    /**
     * Fully qualified name.
     */
    public String name;
    /**
     * A class extended by this class, null for none.
     */
    public CodeClass extendS;
    /**
     * <p>Interfaces implemented by this class, an empty list for none.</p>
     * 
     * <p>It does not have any meaning for the internal code generation, and
     * thus possibly used only by the backends.</p>
     */
    public List<CodeClass> implementS;
    /**
     * Fields, key is the name. Empty list for no fields.
     * By default an empty list.
     * 
     * @see listOfFields
     */
    public Map<String, CodeVariable> fields;
    /**
     * Contains fields in the source code order. Read only
     * by certain backends.
     */
    public List<CodeVariable> listOfFields;
    /**
     * Methods and constructors, key is the signature.
     * Empty list for no fields. By default an empty list.
     * 
     * @see listOfMethods
     */
    public Map<MethodSignature, CodeMethod> methods;
    /**
     * Contains methods in the source code order. Read only
     * by certain backends.
     */
    public List<CodeMethod> listOfMethods;
    /**
     * Main method.
     * 
     * Valid after complete() is called.
     */
    public CodeMethod mainMethod;
    /**
     * Type of this class.
     */
    public Type type;
    /**
     * A special comment directly before the class, in the case of
     * Java a `**' comment. Null if none. To be used by certain
     * backends.
     */
    public String specialComment;
    /**
     * Tags, empty list for no tags.
     */
    public List<CommentTag> tags;
    /**
     * If this class belongs to the standard library;
     */
    public boolean isFromLibrary;
    
    /**
     * Creates a new instance of CodeClass.
     * 
     * @param name                      fully qualified name
     * @param pos                       position in the input stream,
     *                                  null for non--source level
     *                                  classes
     */
    public CodeClass(String name, StreamPos pos) {
        frontend = null;
        this.name = name;
        isFromLibrary = false;
        extendS = null;
        implementS = new LinkedList<>();
        fields = new HashMap<>();
        listOfFields = new LinkedList<>();
        methods = new HashMap<>();
        listOfMethods = new LinkedList<>();
        specialComment = null;
        streamPos = pos;
    }
    @Override
    public void setStreamPos(StreamPos pos) {
        throw new RuntimeException("can be set only at construction");
    }
    @Override
    public StreamPos getStreamPos() {
        return streamPos;
    }
    /**
     * Called after the code of whole compilation is compiled,
     * and method references are completed.
     * 
     * This implementation searches for the main method, also
     * in superclasses if needed.
     *
     * @param compilation               compilation
     * @param codeCompilation           code compilation
     */
    public void complete(Compilation compilation, CodeCompilation
            codeCompilation) throws CompilerException {
        MethodSignature signature = frontend.getMainMethodSignature(null, null);
        mainMethod = lookupMethod(signature);
        if(mainMethod != null) {
            if(mainMethod.context != Context.STATIC)
                throw new CompilerException(mainMethod.getStreamPos(),
                    CompilerException.Code.ILLEGAL,
                    "main method must be static");
            if(mainMethod.returnValue != null)
                throw new CompilerException(mainMethod.getStreamPos(),
                    CompilerException.Code.ILLEGAL,
                    "main method must return void");
        }
    }
    /**
     * If this class is a given class or extends the given class.
     * 
     * @param superClass                class
     * @return                          if is the class or extends
     *                                  the class
     */
    public boolean isEqualToOrExtending(CodeClass superClass) {
        CodeClass current = this;
        while(current != superClass)
            if(current.extendS != null)
                current = current.extendS;
            else
                return false;
        return true;
    }
    /**
     * If this class imlpements another.
     * 
     * @param superClass                class, does not need to be an interface
     * @return                          if implements <code>superClass</code>
     */
    public boolean isImplementing(CodeClass superClass) {
        return implementS.contains(superClass);
    }
    /**
     * If this class extends or implements another.
     * 
     * @param superClass                class
     * @return                          if narrows <code>superClass</code>
     */
    public boolean isNarrower(CodeClass superClass) {
        return isEqualToOrExtending(superClass) ||
                isImplementing(superClass);
    }
    /**
     * Lookups a field, starting at this class and then
     * searching subsequent superclasses, until the field
     * is found. If the field is not found, null is
     * returned.
     * 
     * @param name                      name of the field
     * @return                          code variable or null
     */
    public CodeVariable lookupField(String name) {
        CodeVariable v;
        CodeClass c = this;
        while((v = c.fields.get(name)) == null)
            if(c.extendS != null)
                c = c.extendS;
            else {
                v = null;
                break;
            }
        return v;
    }
    /**
     * Lookups a method, starting at this class and then
     * searching subsequent superclasses, until the method
     * is found. If the method is not found, null is
     * returned.
     * 
     * @param signature                 signature of the method
     * @return                          code method or null
     */
    public CodeMethod lookupMethod(MethodSignature signature) {
        CodeMethod cm;
        CodeClass c = this;
        while((cm = c.methods.get(signature)) == null)
            if(c.extendS != null)
                c = c.extendS;
            else {
                cm = null;
                break;
            }
        return cm;
    }
    /**
     * Converts a fully qualified or an unqualified class name to
     * an unqualified class name.
     * 
     * @param fullName fully qualified or an unqualified name
     * @param cppStyle false if to use the `.' separator,
     * true if to use the `::' separator
     * @return non qualified class name
     */
    public static String toUnqualifiedName(String fullName,
            boolean cppStyle) {
        char separator;
        if(cppStyle)
            separator = ':';
        else
            separator = '.';
        int index = fullName.lastIndexOf(separator);
        return fullName.substring(index + 1);
    }
    /**
     * Returns an unqualified name of this class.
     * 
     * @return unqualified class name
     */
    public String getUnqualifiedName() {
        return toUnqualifiedName(name, false);
    }
    @Override
    public String toString() {
        return name;
    }
}
