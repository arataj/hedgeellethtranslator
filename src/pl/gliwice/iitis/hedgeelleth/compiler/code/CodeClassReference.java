/*
 * CodeReference.java
 *
 * Created on Mar 28, 2008
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.code;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.clazz.AbstractJavaClass;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * A reference to a class.
 * 
 * Used by an allocation operation.
 * 
 * @author Artur Rataj
 */
public class CodeClassReference {
    /**
     * Class.
     * 
     * This field is used to extract a compiled class after
     * the first pass.
     */
    AbstractJavaClass firstPassClass;
    /**
     * A compiled class or null.
     */
    public CodeClass clazz;
    
    /**
     * Creates a new instance of CodeReference. Used in the first pass
     * of referencing, when not all classes are compiled.
     * 
     * @param clazz                     referenced class
     * @param pos                       position in the parsed stream,
     *                                  null if none
     */
    public CodeClassReference(StreamPos pos, AbstractJavaClass clazz) {
        this.firstPassClass = clazz;
    }
    /**
     * Called after the first pass, to set the compiled class
     * or method.
     */
    public void complete() {
        if(firstPassClass != null) {
            clazz = firstPassClass.codeClass;
            if(clazz == null)
                throw new RuntimeException("unknown compiled class");
        } else
            throw new RuntimeException("unknown reference");
    }
    @Override
    public String toString() {
        String s = null;
        if(firstPassClass != null)
            s =  firstPassClass.getQualifiedName().toString();
        return "->" + s;
    }
}
