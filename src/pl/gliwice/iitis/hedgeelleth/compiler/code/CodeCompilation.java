/*
 * CodeCompilation.java
 *
 * Created on Feb 28, 2008, 11:26:03 AM
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.code;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.CommentTag;
import pl.gliwice.iitis.hedgeelleth.compiler.code.op.AbstractCodeOp;
import pl.gliwice.iitis.hedgeelleth.compiler.util.CompilerUtils;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.interpreter.ModelControl;

/**
 * Entire compilation, code level.
 * 
 * @author Artur Rataj
 */
public class CodeCompilation {
    /**
     * Classes, the key is a class' fully qualified name.
     */
    public SortedMap<String, CodeClass> classes;
    /*
     * Classes, the key is a class' type.
     */
     // Map<Type, CodeClass> classesByType;
     /**
      * If special class array is enabled in this compilation by
      * at least a single frontend.
      */
    public boolean arraysEnabled;
     /**
      * If special class string is enabled in this compilation by
      * at least a single frontend.
      */
    public boolean stringsEnabled;
     /**
      * If special class root exception is enabled in this compilation by
      * at least a single frontend.
      */
    public boolean rootExceptionEnabled;
     /**
      * If special class unchecked exception is enabled in this compilation by
      * at least a single frontend.
      */
    public boolean uncheckedExceptionEnabled;
    /**
     * Copied from <code>Compilation.modelControl</code>.
     */
    public ModelControl modelControl;
    
    /**
     * Creates a new instance of CodeCompilation.
    */
    public CodeCompilation() {
        classes = new TreeMap<>();
        arraysEnabled = false;
        stringsEnabled = false;
        rootExceptionEnabled = false;
        uncheckedExceptionEnabled = false;
    }
    /**
     * Returns references to all methods in this compilation,
     * including the constructors.
     * 
     * @return                          methods
     */
    public Collection<CodeMethod> getMethods() {
        List<CodeMethod> list = new LinkedList<>();
        for(CodeClass clazz : classes.values())
            for(CodeMethod method : clazz.methods.values())
                list.add(method);
        return list;
    }
    /**
     * Returns references to all lists of code in this compilation.
     * 
     * @return                          lists of code
     */
    public Collection<Code> getCode() {
        List<Code> list = new LinkedList<>();
        for(CodeMethod method : getMethods())
            if(method.code != null)
                list.add(method.code);
        return list;
    }
    /**
     * Verifies if names of non--compiler tags are within a given set.
     * Throws an exception if a non--recognized compiler modifier
     * is found.
     * 
     * @param allowed                   set to verify the names against
     * @return                          null if all tags are allowed,
     *                                  otherwise name of the first
     *                                  non--allowed tag found
     */
    public String verifyNonCompilerTagModifiers(Set<String> allowed)
            throws ParseException {
        if(allowed != null)
            for(Code c : getCode())
                for(AbstractCodeOp op : c.ops)
                    for(CommentTag ct : op.getTags())
                        for(String m : ct.modifiers)
                            if(CommentTag.getCompilerModifier(m) == null &&
                                !allowed.contains(ct.name))
                                return ct.name;
        return null;
    }
    /**
     * Filters tags, as defined in CodeOp.filterTags(), in all operations
     * within this compilation.
     */
    public void filterTags() {
        for(Code c : getCode())
            for(AbstractCodeOp op : c.ops)
                op.filterTags();
    }
    @Override
    public String toString() {
        String s = "";
        for(CodeClass clazz : classes.values()) {
            s += clazz.name + "\n";
            for(CodeMethod method : clazz.methods.values()) {
                String m = method.toString();
                s += CompilerUtils.indent(1, m) + "\n";
            }
        }
        return s;
    }
}
