/*
 * CodeConstant.java
 *
 * Created on Mar 7, 2008, 12:52:02 PM
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.code;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.Literal;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Type;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;
import pl.gliwice.iitis.hedgeelleth.interpreter.RuntimeValue;

/**
 * A code constant.
 * 
 * @author Artur Rataj
 */
public class CodeConstant extends AbstractCodeValue {
    /**
     * Value of this constant.
     */
    public Literal value;
    
    /**
     * Creates a new instance of CodeConstant.
     * 
     * @param pos                       position in the parsed stream,
     *                                  null if none
     * @param literal                   value of this constant
     */
    public CodeConstant(StreamPos pos, Literal value) {
        super(pos);
        this.value = value;
    }
    @Override
    public void setResultType(Type resultType) {
        throw new RuntimeException("can not set type of a constant");
    }
    @Override
    public Type getResultType() {
        return value.type;
    }
    @Override
    public boolean hasRestrictiveSourceLevelRange() {
        return false;
    }
    @Override
    public boolean equals(Object o) {
        if(!(o instanceof AbstractCodeValue))
            throw new RuntimeException("invalid comparison");
        if(!(o instanceof CodeConstant))
            return false;
        else
            return value.equals(((CodeConstant)o).value);
    }
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + (this.value != null ? this.value.hashCode() : 0);
        return hash;
    }
    @Override
    public CodeConstant clone() {
        Object o = super.clone();
        CodeConstant out = (CodeConstant)o;
        out.value = value.clone();
        return out;
    }
    @Override
    public String toNameString() {
        return value.toString();
    }
    @Override
    public String toString() {
        return value.toString();
    }
}
