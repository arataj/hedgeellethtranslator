/*
 * CodeLabel.java
 *
 * Created on Mar 26, 2008, 1:42:06 PM
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.code;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.code.op.AbstractCodeOp;

/**
 * A label of an operation, its scope is a method and the label
 * is unique within the method. Use equal() to determine equality
 * of two labels.
 * 
 * Two labels are equal if their names are equal. The indices
 * do not count in testing equality, because the indices may
 * be temporarily invalid, after some optimization for example.
 * 
 * @author Artur Rataj
 */
public class CodeLabel {
    /**
     * Shows label names in textual forms.
     */
    public static final boolean LABEL_DEBUG = false;
    
    /**
     * Name of this label.
     */
    public String name;
    /**
     * Index of this label in the list of operations of a method,
     * or -1 if not yet determined.
     */
    public int codeIndex;
    
    /**
     * Creates a new instance of CodeLabel, with an indetermined
     * position in the list of operations.
     * 
     * @paran name                      name of this label, can
     *                                  not be null
     */
    public CodeLabel(String name) {
        this.name = name;
        codeIndex = -1;
    }
    /**
     * A copying constructor.
     * 
     * @param label                     the label to copy
     */
    public CodeLabel(CodeLabel label) {
        this(label.name);
        codeIndex = label.codeIndex;
    }
    /**
     * Sets the contents of this label to the copy of contents
     * of another one.
     * 
     * @param target                    label whose contents to copy
     */
    public void set(CodeLabel target) {
        name = target.name;
        codeIndex = target.codeIndex;
    }
    public static void clearIndices_(Code code) {
        for(AbstractCodeOp op2 : code.ops) {
            if(op2.label != null)
                op2.label.codeIndex = -1;
            for(CodeLabel l : op2.getLabelRefs())
                l.codeIndex = -1;
        }
    }
    /**
     * Sets the label indices. Performed after optimizations,
     * when the operations have already fixed positions, so
     * the indices can be set.
     * 
     * @param code                      code
     */
    public static void setIndices(Code code) {
        Map<CodeLabel, CodeLabel> labels = new HashMap<>();
        int count = 0;
        // set indices
        for(AbstractCodeOp op : code.ops) {
            CodeLabel label = op.label;
            if(label != null) {
                label.codeIndex = count;
                if(labels.containsKey(label)) {
                    clearIndices_(code);
                    throw new RuntimeException("shared label instance");
                }
                labels.put(label, label);
            }
            ++count;
        }
        // update label refs
        for(AbstractCodeOp op : code.ops)
            for(CodeLabel l : op.getLabelRefs())
{if(labels.get(l) == null)
    l = l;
                l.codeIndex = labels.get(l).codeIndex;
}
    }
    @Override
    public boolean equals(Object label) {
        if(!(label instanceof CodeLabel))
            throw new RuntimeException("invalid comparison");
        return name.equals(((CodeLabel)label).name);
    }
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + (this.name != null ? this.name.hashCode() : 0);
        return hash;
    }
    @Override
    public String toString() {
        String s;
        if(name == null)
            s = "-:-";
        else {
            if(codeIndex != -1 && !LABEL_DEBUG)
                s = "" + codeIndex;
            else
                s = "#" + (System.identityHashCode(this)%10000) + ":" + name;
        }
        return s;
    }
}
