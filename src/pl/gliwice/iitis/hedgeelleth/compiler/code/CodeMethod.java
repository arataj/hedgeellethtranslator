/*
 * CodeMethod.java
 *
 * Created on Feb 28, 2008, 11:20:26 AM
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.code;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.CommentTag;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.CodeFieldDereference;
import pl.gliwice.iitis.hedgeelleth.compiler.primitiverange.CodeVariablePrimitiveRange;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.clazz.AbstractJavaClass;
import pl.gliwice.iitis.hedgeelleth.compiler.util.CompilerUtils;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.scope.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * A compiled method, constructor or method declaration.<br>
 * 
 * If a method is non--static, it differs from a static one by
 * having the object argument "this" is the first one. For more
 * details see the documentation of Method.<br>
 * 
 * A single method has at most one object instance, thus comparison by
 * equality operator is possible. The copying operators are, in fact,
 * used to get actual method copies.
 *
 * @author Artur Rataj
 */
public class CodeMethod implements SourcePosition, CodeVariableOwner {
    /**
     * A prefix of prototype arguments, not incuding the "#" which prefixes
     * any internal variable.
     */
    public static String PROTO_PREFIX = "proto#";
    
    /**
     * Class that contains this method.
     */
    public CodeClass owner;
    /**
     * Fully qualified signature.
     */
    public MethodSignature signature;
    /**
     * This method's context.
     */
    public Context context;
    /**
     * If this variable is a part of a class' public API. Read only by
     * certain backends.<br>
     * 
     * Typically, the <code>public</code> or similar modifier of the
     * frontend's language is directly translated into <code>apI</code>.
     */
    public boolean apI;
    /**
     * Keys of subsequent arguments.
     */
    public List<String> argNames;
    /**
     * Code of this method. Null for abstract methods.
     */
    public Code code;
    /**
     * All local variables, keyed and sorted with their names.
     * Arguments and the return value are also locals.
     */
    public SortedMap<String, CodeVariable> variables;
    /**
     * List of subsequent arguments. If this is a non--static
     * method, the first element of the list is the method's
     * object reference "this".
     * 
     * @see argProtos
     */
    public List<CodeVariable> args;
    /**
     * <p>Which of these arguments have range--less prototypes, which
     * should be assigned instead of them. Keyed with an argument,
     * value is the proto to assign.</p>
     * 
     * <p>The actual arguments are assigned by the protos later, when their
     * bounds are already evaluated.</p>
     */
    public Map<CodeVariable, CodeVariable> argProtos;
    /**
     * Local variable holding the return value, or null if this
     * method returns void.<br>
     * 
     * This variable can not be modified by the optimizer, just
     * like <code>CodeOpReturn.returnValue</code> can not.<br>
     * 
     * The local's range must be replaced with locals. So for
     * the original field range variables, use the field
     * <code>range</code>.
     */
    public CodeVariable returnValue;
    /**
     * Range of this method.
     */
    public CodeVariablePrimitiveRange range;
    /*
     * Primitive range of this method return value, null for none.
     */
    // public CodePrimitiveRange returnRange;
    /**
     * Annotations, empty if none, sorted by name.
     */
    public SortedSet<String> annotations;
    /**
     * Methods overriding this one, a key is the class of the
     * runtime object referenced by "this" of a method. Keys
     * of classes that do not define a given method are be
     * missing.
     * 
//     * , but the method <code>getVirtual(CodeClass)</code> still
//     * finds a correct method and completes the missing keys
//     * to make subsequent lookups faster.<br>
     * 
     * This map is used when choosing a correct method to call.
     */
    public Map<CodeClass, CodeMethod> overridings;
    /**
     * A special comment directly before the method, in the case of
     * Java a `**' comment. Null if none. To be used by certain
     * backends.
     */
    public String specialComment;
    /**
     * Tags, empty list for no tags.
     */
    public List<CommentTag> tags;
    /**
     * Next unique label identifier. Inherited from a <code>Method</code>
     * object.
     */
    int uniqueLabelNum;
    /**
     * Position of this method in the parsed stream.
     */
    protected StreamPos pos;
    /**
     * Index of the first operation of the argument range code, if no such
     * operations always 0. Only after this code the arguments which have protos
     * are assigned.
     */
    public int argRangeBeg;
    /**
     * Number of operations within the range code, 0 for no such operations.
     * 
     * @see argRangeBeg
     */
    public int argRangeSize;
    /**
     * Creates a new instance of CodeMethod. Variables and annotations
     * are created on the basis of the given method, but overridings
     * and super method are not created as not all code methods may
     * be generated yet.<br>
     * 
     * This constructor sets <code>apI<code> to
     * <code>method.flags.am == AccessModifier.PUBLIC</code>.
     * 
     * @param clazz                     class that contains this method
     * @param method                    method
     * @param apI                       if this variable is a part of a class'
     *                                  public API
     */
    public CodeMethod(CodeClass clazz, Method method) {
        owner = clazz;
        signature = method.signature;
        context = method.flags.context;
        apI = method.flags.am.hasPublicAccess();
        setArgNames(method);
        returnValue = null;
        setStreamPos(method.getStreamPos());
        /*
        if(method.name.indexOf("nxp") != -1)
            method = method;
        */
        if(method.returnLocal != null &&
                method.returnLocal.primitiveRange != null) {
            range = new CodeVariablePrimitiveRange(
                    new CodeFieldDereference(
                        method.returnLocal.primitiveRange.min.codeVariable),
                    new CodeFieldDereference(
                    method.returnLocal.primitiveRange.max.codeVariable));
            // method ranges are always source--level
            range.setSourceLevel(true);
        }
        setVariables(method.scope, argNames);
        setArgs();
        annotations = new TreeSet<>();
        annotations.addAll(method.annotations);
        overridings = new HashMap<>();
        specialComment = method.specialComment;
        tags = method.tags;
        uniqueLabelNum = method.scope.uniqueLabelNum;
        /*
        if(method.range != null) {
            returnRange = new CodePrimitiveRange(
                    new CodeFieldDereference(method.range.min.variable.codeVariable),
                    new CodeFieldDereference(method.range.max.variable.codeVariable));
        }
         */
    }
    /**
     * A copying constructor.
     * 
     * This is a copy made using the same rules as specified in
     * the copying constructor of class <code>Code</code>.
     * 
     * @param original                  code method to copy
     */
    public CodeMethod(CodeMethod method) {
        owner = method.owner;
        signature = method.signature;
        context = method.context;
        argNames = new ArrayList<>(method.argNames);
        if(method.code != null)
            code = new Code(method.code);
        variables = new TreeMap<>(
                method.variables);
        returnValue = method.returnValue;
        if(method.range != null) {
            range = new CodeVariablePrimitiveRange(
                method.range.min,
                method.range.max);
            range.setSourceLevel(method.range.isSourceLevel());
        }
        args = new ArrayList<>(method.args);
        argProtos = new HashMap<>(method.argProtos);
        annotations = new TreeSet<>(method.annotations);
        overridings = new HashMap<>(method.overridings);
        specialComment = method.specialComment;
        tags = method.tags;
        uniqueLabelNum = method.uniqueLabelNum;
        pos = method.pos;
    }
    /**
     * Sets the list of argument names. These names are keys
     * in the map of local variables.
     * 
     * A possible argument "this" is added as the first one
     * to the list.
     * 
     * @param method
     */
    private void setArgNames(Method method) {
        argNames = new ArrayList<>();
        for(Variable v : method.scope.locals.values())
            if(v.name.equals(Method.LOCAL_THIS))
                argNames.add(v.name);
        for(Variable v : method.arg)
            argNames.add(v.name);
    }
    /**
     * Sets the list of arguments.
     */
    private void setArgs() {
        args = new ArrayList<>();
        for(String key : argNames) {
            CodeVariable type = variables.get(key);
            args.add(type);
        }
        argProtos = new HashMap<>();
    }
    /**
     * If a given local variable of this method is
     * also an argument without a prototype
     * or a prototype of the method, then this method
     * returns the index of the respective argument,
     * otherwise this method returns -1.
     * 
     * @param local                     variable to test
     * @return                          argument index or -1
     */
    public int getWrittenNum(CodeVariable local) {
        int count = 0;
        for(CodeVariable arg : args) {
            if(arg == local && !argProtos.keySet().contains(arg))
                return count;
            if(argProtos.get(arg) == local)
                return count;
            ++count;
        }
        return -1;
    }
    /**
     * Returns a new unique label within this method.
     *
     * @return                          an unique label
     */
    public CodeLabel newLabel() {
        return new CodeLabel("*c" + uniqueLabelNum++);
    }
    /**
     * Creates a new internal local variable, without a specified
     * primitive range.
     * 
     * @param type                      type
     * @param suffix                    name suffix, null for default
     * @param category                  category, must be internal
     * @return                          dereference to the new local
     *                                  variable
     */
    public CodeFieldDereference newLocal(Type type, String suffix,
            Variable.Category category) {
        if(!category.isInternal())
            throw new RuntimeException("category must be internal");
        int count;
        if(suffix == null) {
            suffix = "c";
            count = 0;
        } else
            count = -1;
        String name;
        do {
            name = "#" + suffix;
            if(count >= 0)
                name += variables.size() + count;
            ++count;
        } while(variables.keySet().contains(name));
        CodeVariable v = new CodeVariable(null, this,
                name, type, Context.NON_STATIC, false, false, false,
                category);
/*if(currMethod.variables.size() == 9)
    currMethod = currMethod;*/
        addLocal(v, false);
        return new CodeFieldDereference(v);
    }
    /**
     * If a local is written externally.
     * 
     * @param local local to check
     * @return if an argument without a proto, or a proto itself
     */
    public boolean writtenExternally(CodeVariable local) {
        return (args.contains(local) && !argProtos.keySet().contains(local)) ||
            argProtos.values().contains(local);
    }
    /**
     * If a local is written/read externally.
     * 
     * @param local local to check
     * @return if an argument without a proto, or a proto, or a return value
     */
    public boolean interactsExternally(CodeVariable local) {
        return local == returnValue || writtenExternally(local);
    }
    /**
     * Searched for a bound within its locals, replaces it with a constant.
     * 
     * @param r bound to replace
     * @param c replacement constant
     * @return if the bound has been found and replaced
     */
    public boolean replaceBound(CodeVariable r, CodeConstant c) {
        boolean modified = false;
        for(CodeVariable v : variables.values())
            if(v.hasPrimitiveRange(false)) {
                if(replace(v.primitiveRange.min, r)) {
                    v.primitiveRange.min = c;
                    modified = true;
                }
                if(replace(v.primitiveRange.max, r)) {
                    v.primitiveRange.max = c;
                    modified = true;
                }
            }
        return modified;
    }
    /**
     * If <code>bound</code> represents <code>r</code>,
     * and thus should be replaced. Used by
     * <code>replaceBound()</code>.
     * 
     * @param bound bound in a variable's primitive range
     * @param r variable to replace
     * @return if the bound should be replaced
     */
    private boolean replace(AbstractCodeValue bound, CodeVariable r) {
        if(!r.category.isVariablePRBound())
            throw new RuntimeException("invalid category");
        CodeVariable b;
        if(bound instanceof CodeFieldDereference &&
                (b = ((CodeFieldDereference)bound).variable) == r) {
            if(!b.category.isVariablePRBound())
                throw new RuntimeException("invalid category");
            return true;
        }
        return false;
    }
    /**
     * A class used by <code>setVariables</code> and
     * <code>addVariables</code> to hold a comparable local variable
     * description, with the major sorting key being the variable
     * name and the minor sorting key being the variable's source
     * stream position.<br>
     *
     * The method <code>compareTo</code> should never return 0 for
     * two objects of this class that represent different variables,
     * otherwise a runtime exception is thrown.
     */
    private static class VariableDescription implements Comparable<VariableDescription> {
        public CodeVariable variable;
        public boolean isArgument;

        public VariableDescription(CodeVariable variable, boolean isArgument) {
            this.variable = variable;
            this.isArgument = isArgument;
        }
        @Override
        public int compareTo(VariableDescription d) {
            int order = variable.name.compareTo(
                    d.variable.name);
            if(order == 0) {
                order = variable.getStreamPos().compareTo(
                    d.variable.getStreamPos());
                if(order == 0)
                    throw new RuntimeException("sort order undetermined");
            }
            return order;
        }
        @Override
        public String toString() {
            String s = variable.name;
            if(isArgument)
                s += " [A]";
            return s;
        }
    }
    /**
     * Adds variables from a scope and below as locals of this
     * method. If a local is also an argument, it is also added
     * to the list of arguments. If a local is also the return
     * value, it is also assigned to the field
     * <code>returnValue</code>.<br>
     *
     * Used only during construction of this code method from
     * <code>Method</code>, to find all locals.
     */
    private void setVariables(AbstractLocalOwnerScope scope, List<String> argNames) {
        variables = new TreeMap<>();
        AbstractQueue<VariableDescription> queue =
                addVariables(scope, argNames);
        String prevName = null;
        int serial = 0;
        VariableDescription d;
        while((d = queue.poll()) != null) {
            String currName = d.variable.name;
            if(currName.equals(prevName)) {
                d.variable.name = currName + "#" + serial;
                ++serial;
            } else
                serial = 0;
            addLocal(d.variable, d.isArgument);
            prevName = currName;
        }
    }
    /**
     * Adds recursively variables from a scope and below and returns
     * them. If a local is also an argument, it is also added
     * to the list of arguments. If a local is also the return
     * value, it is also assigned to the field returnValue. Does
     * not resolve name conflicts of the variables.
     * 
     * @param scope                     scope of this method or
     *                                  one of that scope's subscopes
     * @param argNames                  list of argument names
     */
    private AbstractQueue<VariableDescription> addVariables(
            AbstractLocalOwnerScope scope, List<String> argNames) {
        AbstractQueue<VariableDescription> outVariables =
                new PriorityQueue<>();
        for(Variable v : scope.locals.values()) {
// if(v.toString().contains("OUT"))
//     v = v;
            v.codeVariable = new CodeVariable(v, this);
            boolean isArgument = false;
            if(argNames != null && argNames.contains(v.name))
                isArgument = true;
            outVariables.add(new VariableDescription(v.codeVariable, isArgument));
            if(v.name.equals(Method.LOCAL_RETURN))
                returnValue = v.codeVariable;
        }
        // sub--scopes do not contain method arguments anyway
        // so null is passed instead of them
        if(scope instanceof MethodScope) {
            BlockScope blockScope = ((MethodScope)scope).blockScope;
            if(blockScope != null)
                // this method is not abstract, search for variables
                // in the code
                outVariables.addAll(
                        addVariables(blockScope, null));
        } else
            for(BlockScope s : ((BlockScope)scope).innerBlockScopes)
                outVariables.addAll(
                    addVariables(s, null));
        // complete code variables of primitive ranges of locals
        for(Variable v : scope.locals.values()) {
            if(v.primitiveRange != null) {
                // retval's range variables are now fields, so they are replaced with locals
                // in <code>Generator</code>
                v.codeVariable.primitiveRange = new CodeVariablePrimitiveRange(
                        // no internal variables here, but possibly retval
                        v.name.equals(Method.LOCAL_RETURN) ?
                            this.getStreamPos() : v.getStreamPos(),
                        v.primitiveRange.min.codeVariable,
                        v.primitiveRange.max.codeVariable,
                        // source variables have only source ranges
                        //
                        // retval's range is a method's range, thus a source--level
                        true);
            }
        }
        return outVariables;
    }
    /**
     * Adds a local variable. It must have an unique name. If the
     * variable is an argument, it is also added to the list of
     * arguments.
     * 
     * @param v                         variable to add
     * @param isArgument                if the local is an argument of
     *                                  this method
     */
    public void addLocal(CodeVariable v, boolean isArgument) {
        if(variables.containsKey(v.name))
            throw new RuntimeException("duplicate local: " + v.name);
        variables.put(v.name, v);
    }
    /**
     * Finds methods implemented or overridden by this one.
     * 
     * @return a list of supermethods, since the  one most
     * direct one
     * empty list for none
     */
    public List<CodeMethod> getSuper() {
        List<CodeMethod> out = new LinkedList<>();
        CodeClass curr = owner.extendS;
        while(curr != null) {
            for(CodeMethod c : curr.listOfMethods)
                if(c.overridings.get(owner) == this) {
                    out.add(c);
                    break;
                }
            curr = curr.extendS;
        }
        return out;
    }
    /**
     * Returns what method should be called in a call referencing
     * this code method, if the object referenced by "this" is of
     * a given type.
     * 
//     * Usually to be used to lookup non--static method.
//     * 
//     * This method may modify the map of overridings to optimize
//     * subsequent lookups, see the documentation of that map for
//     * details.
//     * 
     * @param clazz                     class of the object referenced
     *                                  by "this" of a non--static
     *                                  method
     *                              
     * @return
     */
    public CodeMethod getVirtual(CodeClass clazz) {
        CodeMethod called = this;
        if(!clazz.isEqualToOrExtending(owner)) {
            boolean implementS = false;
            for(CodeClass i : clazz.implementS)
                if(i.isEqualToOrExtending(owner)) {
                    implementS = true;
                    break;
                }
            if(!implementS)
                throw new RuntimeException("this must be equal to or extending a method's onwer");
        }
        for(CodeClass c : overridings.keySet())
            if(clazz.isEqualToOrExtending(c)) {
                CodeMethod o = overridings.get(c);
                if(!clazz.isEqualToOrExtending(owner))
                    // the class is still pointing to some interface,
                    // replace it with any proposal
                    called = o;
                else if(c.isEqualToOrExtending(called.owner))
                    // replace only if more nested
                    called = o;
            }
        if(called.code == null && overridings.size() == 1)
            called = overridings.values().iterator().next();
//        CodeClass current = clazz;
//        while(current != owner &&
//                (called = overridings.get(current)) == null) {
//            current = current.extendS;
//        }
//        if(called == null)
//            called = this;
//        if(called.code == null)
//            throw new RuntimeException("abstract method in overridings");
//        // complete the missing classes to make subsequent lookups
//        // faster
//        current = clazz;
//        while(current != owner &&
//                overridings.get(current) == null) {
//            overridings.put(current, called);
//            current = current.extendS;
//        }
        return called;
    }
    /**
     * Returns argument "this" if this is a non--static method,
     * null otherwise.
     * 
     * @return                          "this" or null
     */
    public CodeVariable getThisArg() {
        if(context != Context.NON_STATIC)
            return null;
        else
            return args.get(0);
    }
    /**
     * Returns this method declaration description. The
     * description contains the method's name, arguments
     * and annotations. The name is not qualified.<br>
     * 
     * Wraps non--Java types to be entries from <code>MessageStyle</code>.
     * 
     * @param bracket passed to <code>Type.getNameString()</code>
     * @return                      description
     */
    public String getDeclarationDescription(boolean bracket) {
        return Method.getDeclarationDescription(
                signature.name.substring(0, signature.name.indexOf(
                    MethodSignature.SIGNATURE_ARGUMENTS_SEPARATOR)),
                args, new LinkedList<>(annotations), bracket);
    }
    /**
     * Returns contatenation of the method's class name, a dot
     * and the method's signature. The contatenation can be
     * used as a key showing the method's origin in the source
     * code.
     * 
     * @return                          key showing the method's
     *                                  origin in the source code
     */
    public String getKey() {
        return owner.name + "." + signature.name;
    }
    /*
     * If this method contains one or more tags of a given name.
     * 
     * @param name                      names of searched tag
     * @return                          if found
     */
    public boolean containsTag(String name) {
        for(CommentTag ct : tags)
            if(ct.name.equals(name))
                return true;
        return false;
    }
    /**
     * Returns if a method is a constructor.
     * 
     * @param cm method
     * @return if a constructor
     */
    public boolean isConstructor() {
        return signature.extractUnqualifiedNameString().equals(
                owner.getUnqualifiedName());
    }
    /**
     * Returns if a method is an initializer of static fields.
     * 
     * @param cm method
     * @return if a static initialization method
     */
    public boolean isStaticInitializationMethod() {
        return signature.extractUnqualifiedNameString().equals(
                AbstractJavaClass.INIT_STATIC);
    }
    /**
     * Returns if a method is an initializer of non--static fields.<br>
     * 
     * Note that such a method is never a constructor, but is called by
     * constructors.
     * 
     * @param cm method
     * @return if an object initialization method
     */
    public boolean isObjectInitializationMethod() {
        return signature.extractUnqualifiedNameString().equals(
                AbstractJavaClass.INIT_OBJECT);
    }
    @Override
    public final void setStreamPos(StreamPos pos) {
        this.pos = pos;
    }
    @Override
    public final StreamPos getStreamPos() {
        return pos;
    }
    @Override
    public String toString() {
        String s = getKey() + " ";
        for(String key : argNames)
            s += key + " ";
        for(String key : annotations)
            s += "@" + key + " ";
        s = s.substring(0, s.length() - 1) + "\n";
        for(CodeVariable v : variables.values())
            s += "  " + v.toString() + "\n";
        if(code != null)
            s += CompilerUtils.indent(1,
                    code.toString(this));
        s = s.substring(0, s.length() - 1);
        return s;
    }
}
