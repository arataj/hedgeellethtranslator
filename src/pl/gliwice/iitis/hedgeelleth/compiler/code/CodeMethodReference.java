/*
 * CodeReference.java
 *
 * Created on Mar 28, 2008
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.code;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.Method;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Type;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Typed;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * A reference to a method.
 * 
 * Used by a call operation.
 * 
 * @author Artur Rataj
 */
public class CodeMethodReference implements Typed, SourcePosition {
     /**
     * Position in the parsed stream, null if none.
     */
    protected StreamPos pos;
    /**
     * Method.
     * 
     * This field is used to extract a compiled method after
     * the first pass.
     */
    public Method firstPassMethod;
    /**
     * A compiled method or null.
     */
    public CodeMethod method = null;

    /**
     * Creates a new instance of CodeReference. Used in the first pass
     * of referencing, when not all methods are compiled. The direct
     * flag is set to false.
     * 
     * @param method                    referenced method
     * @param pos                       position in the parsed stream,
     *                                  null if none
     */
    public CodeMethodReference(StreamPos pos, Method method) {
        setStreamPos(pos);
        this.firstPassMethod = method;
    }
    /**
     * A copying constructor, creates a shallow copy.
     * 
     * @param reference                 method reference to copy
     */
    public CodeMethodReference(CodeMethodReference reference) {
        this(reference.pos, reference.firstPassMethod);
        method = reference.method;
    }
    /**
     * Called after the first pass, to set the compiled class
     * or method.
     */
    public void complete() {
        if(firstPassMethod != null) {
            method = firstPassMethod.codeMethod;
            if(method == null)
                throw new RuntimeException("unknown compiled method");
        } else
            throw new RuntimeException("unknown reference");
    }
    /**
     * If the code method does not exist, description of
     * declaration of the first pass method is used, otherwise
     * description of declaration of the code method is
     * used. In the latter case, non--static methods have
     * the "this" argument present already.
     * 
     * @return                          description of this
     *                                  reference
     */
    @Override
    public String toString() {
        String s;
        if(method != null)
            s = method.getDeclarationDescription(false);
        else
            s =  firstPassMethod.getDeclarationDescription(false);
        return "->" + s;
    }
    @Override
    public final void setStreamPos(StreamPos pos) {
        this.pos = pos;
    }
    @Override
    public final StreamPos getStreamPos() {
        return pos;
    }
    @Override
    public void setResultType(Type resultType) {
        throw new RuntimeException("can not set type of method reference");
    }
    @Override
    public Type getResultType() {
        return firstPassMethod.getResultType();
    }
}
