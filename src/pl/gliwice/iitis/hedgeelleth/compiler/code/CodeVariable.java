/*
 * CodeVariable.java
 *
 * Created on Feb 28, 2008, 11:53:21 AM
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.code;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.CodeFieldDereference;
import pl.gliwice.iitis.hedgeelleth.compiler.primitiverange.CodeVariablePrimitiveRange;
import pl.gliwice.iitis.hedgeelleth.compiler.sa.RangeCodeStaticAnalysis;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.flags.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.CompilerException;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * A variable, to be used by the code.<br>
 *
 * The rules for naming code variables are the same as these of naming
 * <code>Variable</code> objects, see the docs in <code>Variable</code>
 * for details.<br>
 *
 * A single variable has at most one object instance, thus comparison by
 * equality operator is possible.<br>
 *
 * Optimizing limitations on non--internal variables:<br>
 *
 * 1. Primitive range checking of non--internal variable can not
 * be replaced by range checking of an internal variable, as
 * range checking of internal variable is deemed
 * optional by the optimizer -- see the methods
 * <code>AbstractResultCodeOp.resultIsOpaque()</code> and
 * <code>AssignmentCodeOp.isOpaque()</code> and the classes
 * <code>Optimizer</code> and <code>TraceValues</code> for details.<br>
 *
 * 2. Arbitrary replacement of non--internal variable by other variable
 * is forbidden, if the replacement is not a direct result of one
 * variable having its value copied from another. So, for example,
 * <code>TraceValues.propagateValue()</code> and
 * <code>Optimizer.inline()</code> can replace a non--internal
 * variable, while <code>TraceValues.reduceLocals()</code> can not.
 * This way, for output code readability, non--internal variable can not be
 * replaced by variable that has no direct known relation to it.<br>
 *
 * Note: Please do not confuse range checking of a variable
 * with range checking of a primary expression -- these are different
 * things. The former uses <code>CodeVariable.primitiveRangeMin</code> and
 * <code>CodeVariable.primitiveRangeMax</code>, the latter uses
 * <code>RangedCodeValue</code>. Even if the latter logically reduces
 * to range check of a variable, this is still a different thing.<br>
 *
 * Range checking of a variable's primitive range should be provided at every
 * assignment of the variable, unless proven to be redundant.
 * Range checking of an expression relates only to that expression. Additionally,
 * range checking of a variable's primitive range, if the variable is an array
 * dereference, should also be provided at every read of the variable,
 * unless proven to be redundant, as the
 * array's elements might be outside the range of some dereference variable of
 * the array, if the array is referenced by more than a single dereference.
 *
 * @author Artur Rataj
 */
public class CodeVariable implements Typed, SourcePosition {
    /**
     * Position in the parsed stream, null if none.
     */
    protected StreamPos pos;
    /**
     * Owner -- CodeMethod for local variables, CodeClass for fields.
     */
    public CodeVariableOwner owner;
    /**
     * Name of this variable.
     */
    public String name;
    /**
     * Type of this variable.
     */
    public Type type;
    /**
     * Context of this field.
     */
    public Context context;
    /**
     * If this variable is final.
     */
    public boolean finaL;
    /**
     * If the variable is a field, is <code>finaL</code>, but has a default intializer,
     * and not a source level one, and thus can once be initialized by a
     * source-level assignment.
     */
    public boolean blankFinalField;
    /**
     * If this variable is a part of a class' public API. Read only by
     * certain backends.
     */
    public boolean apI;
    /**
     * Category of this variable.
     */
    public Variable.Category category;
    /**
     * A special comment directly before the field, in the case of
     * Java a `**' comment. Null if none. To be used by certain
     * backends. Locals have this field always equal to null.
     */
    public String specialComment;
    /**
     * Code variable of the original variable, null if
     * <code>Variable.primitiveRange</code> was null.
     */
    public CodeVariablePrimitiveRange primitiveRange;
    /**
     * A proposal of range, from code analysis. Null for none.<br>
     *
     * See the class <code>RangeCodeStaticAnalysis</code> for details.
     */
    public RangeCodeStaticAnalysis.Proposal proposal;
    
    /**
     * Creates a new instance of CodeVariable. The name should follow
     * the rules of the name of a <code>Variable</code> object.<br>
     *
     * Note that this constructor does not set the optional primitive
     * range.<br>
     * 
     * Typically, the <code>public</code> or similar modifier of the
     * frontend's language is directly translated into <code>apI</code>.
     *
     * @param pos                       position in the parsed stream,
     *                                  null if none
     * @param owner                     owner of this variable; for special
     *                                  marker variables can be null
     * @param name                      name
     * @param type                      type
     * @param context                    if this is a static field
     * @param finaL                     if this is a final variable
     * @param blankFinalField           if is a final blank field
     * @param apI                       if this variable is a part of a class'
     *                                  public API
     * @param category                  category of this variabe
     */
    public CodeVariable(StreamPos pos, CodeVariableOwner owner, String name,
            Type type, Context context, boolean finaL, boolean blankFinalField, boolean apI,
            Variable.Category category) {
        setStreamPos(pos);
/*if(name != null && name.equals("i#0example.MicrogridFast_getNumJobs__#c3"))
    name = name;*/
        this.owner = owner;
        this.name = name;
        this.type = type;
        this.context = context;
        this.finaL = finaL;
        this.blankFinalField = blankFinalField;
        if(!(this.owner instanceof CodeClass) &&
                this.blankFinalField)
            throw new RuntimeException("not a field but declared as a blank final field");
        this.apI = apI;
        this.category = category;
        proposal = null;
        specialComment = null;
    }
    /**
     * Creates a new instance of CodeVariable. This is a convenience
     * constructor, that also copies <code>specialComment</code> to
     * this object.
     * 
     * @param variable variable, on basis of which to create this variable.
     * @param owner owner of this variable; for special
     * marker variables can be null
     * @param category category of this variable
     */ 
    public CodeVariable(Variable variable, CodeVariableOwner owner) {
        this(variable.getStreamPos(), owner, variable.name,
            variable.type, variable.flags instanceof AbstractMemberFlags ?
            ((AbstractMemberFlags)variable.flags).context : Context.NON_STATIC,
            variable.flags.isFinal(),
            variable.flags instanceof FieldFlags ?
                ((FieldFlags)variable.flags).isBlankFinal() : false,
            variable.isLocal() ? false : variable.flags.isPublic(),
            variable.category);
/*if(variable.name.indexOf('#') != -1)
    variable = variable;*/
        specialComment = variable.specialComment;
    }
    @Override
    public final void setResultType(Type resultType) {
        type = resultType;
    }
    @Override
    public final Type getResultType() {
        return type;
    }
    @Override
    public final void setStreamPos(StreamPos pos) {
        this.pos = pos;
    }
    @Override
    public final StreamPos getStreamPos() {
        return pos;
    }
    /**
     * If this is a local variable. This is done by checking if
     * <code>owner instanceof CodeMethod</code>.
     *
     * @return                          true for local variable, false for
     *                                  field variable
     */
    public boolean isLocal() {
        return owner instanceof CodeMethod;
    }
    /**
     * If this is a variable added by the compiler.
     *
     * @return                          if the variable is internal
     */
    public boolean isInternal() {
        return category.isInternal();
    }
    /**
     * If this variable is a bound of some variable's primitive range.
     *
     * @return                          if the variable is for a primitive range; if true,
     *                                  implies isInternal()
     */
    public boolean isVariablePRBound() {
        return category.isVariablePRBound();
    }
    /**
     * If this variable has a primitive range specified.
     *
     * @param useProposal               if this variable has a proposal,
     *                                  that has a known range,
     *                                  treat it as one having a primitive
     *                                  range
     * @return                          if primitive range is defined
     */
    public boolean hasPrimitiveRange(boolean useProposal) {
        boolean hasRange = primitiveRange != null;
        if(useProposal && proposal != null && proposal.rangeKnown())
            hasRange = true;
        return hasRange;
    }
    /**
     * If this variable has a range declared in the source,
     * and there is not a proposal, to which that range is a known
     * superset.<br>
     * 
     * The range is source level for any non--internal variable that
     * has range, and for these internal variables, whose range is
     * derived from source--level one.<br>
     *
     * Source level variable primitive range that is restrictive,
     * i. e. there is not a knowingly narrower proposal that makes that range redundant,
     * can not be dismissed by the optimizer, as opposed
     * to variable primitive range of an internal variable, which
     * is merely computed by the optimizer and not defined by the
     * translated sources. See this variable's docs and the
     * method <code>sourceRangeImportant</code> on optimizing
     * limitations caused by that.
     *
     * @return                          if this variable has source--level
     *                                  range
     */
    public boolean hasRestrictiveSourceLevelRange() {
        // <code>useProposal</code> set to false, as source level ranges never
        // are created from a proposal
        if(hasPrimitiveRange(false) && primitiveRange.isSourceLevel())
            return !primitiveRange.knownSuperset(proposal);
        else
            return false;
    }
    /**
     * Tests if a primitive range of this variable used as a source is
     * important. It is the case only if the range is source--level --
     * see docs of <code>CodeVariable.hasRestrictiveSourceLevelRange</code> for
     * details -- and if the variable is an array. If the variable
     * would be a primitive, then it is guaranteed that the variable's
     * value fits inside the range, thus, checking is not needed when
     * the variable is read. This does not apply to arrays, when range
     * is dependent on current reference of the array.
     *
     * @param arrayClassName            fully qualified name of the
     *                                  array class, null for
     *                                  <code>ARRAY_QUALIFIED_CLASS_NAME</code>
     * @return                          if its primitive range is important
     *                                  here
     */
    public boolean rangeWhenReadImportant(String arrayClassName) {
        return hasRestrictiveSourceLevelRange() && type.isArray(null);
    }
    /**
     * Returns true if either (1) primitive range exists and
     * <code>primitiveRange.isEvaluatedAsConstant()</code> is true or
     * (2) <code>useProposals</code> is true and the primitive range does not
     * exist but can be set as constant by using the proposal. Such a proposal
     * would need to have its range known for this.
     *
     * @param v                         variable to evaluate
     * @param useProposals              true assumes, that the range could be
     *                                  modified using the proposal
     * @return                          if range (1) is constant or (2) would become
     *                                  constant is usage of the proposal is possible
     */
    public boolean rangeCanBeConstant(boolean useProposals) {
        if(primitiveRange != null && primitiveRange.isEvaluatedAsConstant())
            return true;
        if(primitiveRange == null && useProposals && proposal != null && proposal.rangeKnown())
            return true;
        return false;
    }
    /**
     * If <code>rangeCanBeConstant(useProposals)</code> is true, this method
     * return a new proposal, being the combination of this variable's range
     * and, if <code>useProposals</code> is true, this variable's proposal.<br>
     *
     * If <code>rangeCanBeConstant(useProposals)</code> is false, this method
     * throws a runtime exception.<br>
     *
     * A proposal to be used, needs to have its range known, just like defined in
     * <code>rangeCanBeConstant(useProposals)</code>. An empty input proposal
     * makes this method return some minimal range, instead of an empty range,
     * so that the range can be applied to a variable range.
     *
     * @param useProposals              true assumes, that the range could be
     *                                  modified using the proposal
     */
    public RangeCodeStaticAnalysis.Proposal getFinalConstantRange(boolean useProposals)
            throws CompilerException {
        try {
            if(rangeCanBeConstant(useProposals)) {
                if(useProposals && proposal != null && proposal.rangeKnown()) {
                    if(primitiveRange == null)
                        // range missing, just return the proposal
                        return new RangeCodeStaticAnalysis.Proposal(proposal);
                    else {
                        // combine the range and the proposal
                        Literal cMin = ((CodeConstant)primitiveRange.min).value;
                        Literal cMax = ((CodeConstant)primitiveRange.max).value;
                        if(proposal.isEmpty())
                            // set some minimal range for this usually unlikely case
                            cMax = cMin;
                        else {
                            if(cMin.arithmeticLessThan(proposal.min))
                                cMin = proposal.min;
                            if(!cMax.arithmeticLessThan(proposal.max))
                                cMax = proposal.max;
                        }
                        return new RangeCodeStaticAnalysis.Proposal(
                                cMin, cMax);
                    }
                } else
                    // ignore possible proposal, just return the range
                    return new RangeCodeStaticAnalysis.Proposal(
                            ((CodeConstant)primitiveRange.min).value,
                            ((CodeConstant)primitiveRange.max).value);
            } else
                throw new RuntimeException("can not evaluate final range");
        } catch(CompilerException e) {
            e.completePos(getStreamPos());
            throw e;
        }
    }
    /**
     * Extracts the <code>variable</code> part from a list of field dereferences.
     * 
     * @param fields list to scan
     * @return a  ist with extracted <code>variable</code> values
     */
    public static List<CodeVariable> extract(Collection<CodeFieldDereference> fields) {
        List<CodeVariable> cv = new LinkedList<>();
        for(CodeFieldDereference r : fields)
            cv.add(r.variable);
        return cv;
    }
    /**
     * Extracts the <code>variable</code> part from a field dereference,
     * provided (1) the variable is static and belongs to <code>clazz</code>,
     * or (2) the variable is non--static but referenced by "this"
     * 
     * @param clazz a class
     * @param r dereference
     * @return a non--foreign <code>variable</code>, or null if not found
     */
    public static CodeVariable extractNonForeign(CodeClass clazz,
            CodeFieldDereference r) {
        if((r.variable.context != Context.NON_STATIC && r.variable.owner == clazz) ||
                (r.object != null && r.object.name.equals(Method.LOCAL_THIS)))
            return r.variable;
        else
            return null;
    }
    /**
     * Extracts the <code>variable</code> part from a list of field dereferences,
     * provided (1) the variable is static and belongs to <code>clazz</code>,
     * or (2) the variable is non--static but referenced by "this"
     * 
     * @param clazz a class
     * @param fields list to scan
     * @return a  list with extracted non--foreign <code>variable</code> values
     */
    public static List<CodeVariable> extractNonForeign(CodeClass clazz,
            Collection<CodeFieldDereference> fields) {
        List<CodeVariable> cv = new LinkedList<>();
        for(CodeFieldDereference r : fields) {
            CodeVariable v = extractNonForeign(clazz, r);
            if(v != null)
                cv.add(v);
        }
        return cv;
    }
    /**
     * Like toString(), but returns only names, not types.<br>
     *
     * If the variable is a static field, the returned name would also
     * be a valid runtime name of the variable.
     *
     * @return                          description of names in this
     *                                  variable
     */
    public String toNameString() {
        if(false)
            return name + "{" + super.toString() + "}";
        else {
            String s;
            if(context != Context.NON_STATIC)
                s = ((CodeClass)owner).name + ".";
            else
                s = "";
            s += name;
            return s;
        }
    }
    /**
     * Like toNameString(), but strips the part of the name that
     * begins with '#', added by the compiler. See the docs of
     * <code>Variable.getSourceName()</code> and the constructor
     * of this object for details.
     *
     * Can be used only for variables with names derived from
     * variable identifiers in source files, otherwise a runtime
     * exception is thrown.
     *
     * @return                          description of names in this
     *                                  variable
     */
    public String toSourceNameString() {
        if(false)
            return name + "{" + super.toString() + "}";
        else {
            String s;
            if(context != Context.NON_STATIC)
                s = ((CodeClass)owner).name + ".";
            else
                s = "";
            s += Variable.extractSourceName(name);
            return s;
        }
    }
    @Override
    public String toString() {
        String s = "";
        if(hasPrimitiveRange(false))
            s += primitiveRange.toString();
        if(context != Context.NON_STATIC)
            s += "static ";
        s += ( type == null ? "(no type)" : type.toJavaString(null) );
        if(proposal != null)
            s += " " + proposal.toString();
        s += " " + name;
        if(false)
            s += "{" + super.toString() + "}";
        return s;
    }
}
