/*
 * CodeVariableOwner.java
 *
 * Created on Mar 20, 2008, 11:19:10 AM
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.code;

/**
 * A compiled object that can own variables -- locals in the case of
 * CodeMethod and fields in the case of CodeClass.
 * 
 * @author Artur Rataj
 */
public interface CodeVariableOwner {
}
