/*
 * CodeVisitor.java
 *
 * Created on Mar 7, 2009, 5:50:31 PM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.code;

import pl.gliwice.iitis.hedgeelleth.compiler.code.op.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;

/**
 * A visitor interface for code ops. The parameters <code>context</code>
 * are helper values, null for none.
 *
 * @author Artur Rataj
 */
public interface CodeVisitor<R, C> {
    public R visit(C context, CodeOpAllocation op) throws TranslatorException;
    public R visit(C context, CodeOpAssignment op) throws TranslatorException;
    public R visit(C context, CodeOpBinaryExpression op) throws TranslatorException;
    public R visit(C context, CodeOpBranch op) throws TranslatorException;
    public R visit(C context, CodeOpCall op) throws TranslatorException;
    public R visit(C context, CodeOpJump op) throws TranslatorException;
    public R visit(C context, CodeOpNone op) throws TranslatorException;
    public R visit(C context, CodeOpReturn op) throws TranslatorException;
    public R visit(C context, CodeOpThrow op) throws TranslatorException;
    public R visit(C context, CodeOpSynchronize op) throws TranslatorException;
    public R visit(C context, CodeOpUnaryExpression op) throws TranslatorException;
}
