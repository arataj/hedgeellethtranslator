/*
 * Generator.java
 *
 * Created on Feb 21, 2008, 1:26:34 PM
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.code;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.Hedgeelleth;
import pl.gliwice.iitis.hedgeelleth.compiler.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.op.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.optimizer.Optimizer;
import pl.gliwice.iitis.hedgeelleth.compiler.code.optimizer.TraceValues;
import pl.gliwice.iitis.hedgeelleth.compiler.primitiverange.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Type.PrimitiveOrVoid;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.clazz.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.scope.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.CompilerUtils;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.modules.AbstractFrontend;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * Generator of the internal compiler code. Its input is <code>Compilation</code>
 * object and its output is <code>CodeCompilation</code> object.<br>
 *
 * Some docs on calling in some cases shadowing as overriding in the
 * translator, see the docs in <code>SemanticCheck</code>.<br>
 *
 * Event that during the generation of code the input <code>Compilation</code>
 * is affected by class transformations, multiple <code>CodeCompilation</code>
 * objects can be generated from a single <code>Compilation</code> object,
 * what may happen if experiments are allowed.<br>
 * 
 * @author Artur Rataj
 */
public class Generator implements Visitor<Code> {
//    /**
//     * Marker meaning that the latest result should be used as the
//     * argument in a call expression.
//     */
//    static final CodeVariable LATEST_RESULT =
//            new CodeVariable(null, null, null, null, Context.NON_STATIC, false, false,
//                false, Variable.Category.INTERNAL_NOT_RANGE);

    /**
     * If to be verbose.
     */
    boolean verbose = false;
    /**
     * Compilation whose code is to be generated.
     */
    Compilation compilation;
    /**
     * Low level output compilation.
     */
    CodeCompilation codeCompilation;
    /**
     * Current class or null if none.
     */
    CodeClass currClass;
    /**
     * Current method or null if none.
     */
    CodeMethod currMethod;
    /**
     * If <code>curMethod</code> contains argument range operations,
     * this points to the block which begins with them. Otherwise null.
     */
    Block argRangeBlock;
    /**
     * Identifier of next unique internal label.
     */
    private int nextlabelId;
    /**
     * All references to the key variable should be replaced with references
     * to the values variable. Empty for no replacement.<br>
     *
     * This is used only in joining special classes.
     * See the method <code>Generator.checkClassReplacements</code> for details.
     *
     * @see Generator.checkClassReplacements
     */
    public Map<Variable, Variable> thrownOutVariables;
    /**
     * All references to the key method should be replaced with references
     * to the values method. Empty for no replacement.<br>
     *
     * This is used only in joining special classes.
     * See the method <code>Generator.checkClassReplacements</code> for details.
     *
     * @see Generator.checkClassReplacements
     */
    public Map<Method, Method> thrownOutMethods;
    /**
     * The topmost element in this stack is the statement from which currently is
     * generated code, null for none.<br>
     *
     * Used to get local per--statement variable replacements.
     *
     * @see thrownOutVariables
     */
    Stack<AbstractStatement> currStatement;
    /**
     * This generator's options.
     */
    public GeneratorOptions options;

    /**
     * Creates a new instance of Generator.
     *
     * Generator tags are enabled by default.
     *
     * @param compilation               compilation
     */
    public Generator(Compilation compilation, GeneratorOptions options)
            throws CompilerException {
        this.compilation = compilation;
        this.options = options;
        codeCompilation = new CodeCompilation();
        currClass = null;
        currMethod = null;
        thrownOutVariables = new HashMap<>();
        thrownOutMethods = new HashMap<>();
    }
    /**
     * Converts a tree binary operator to a code binary operator with
     * possibly swapped operands.<br>
     *
     * The companion method <code>treeToCodeSwap</code> should be used
     * as well for full conversion.<br>
     *
     * The reason for swapped operands is described in
     * <ode>CodeOpBinaryExpression.operatorTreeToCode</code>.
     *
     * @param operatorType              tree operator
     * @return                          code operator, that possibly requires
     *                                  swapped operands
     * @see CodeOpBinaryExpression.operatorTreeToCode
     */
    public static BinaryExpression.Op treeToCodeSwapOperator(
            BinaryExpression.Op operator) {
        BinaryExpression.Op swappedOperator;
        switch(operator) {
            case GREATER:
                // the operator is non--existent in the code
                swappedOperator = BinaryExpression.Op.LESS;
                break;

            case GREATER_OR_EQUAL:
                // the operator is non--existent in the code
                swappedOperator = BinaryExpression.Op.LESS_OR_EQUAL;
                break;

            default:
                swappedOperator = operator;
                break;
        }
        return swappedOperator;
    }
    /**
     * Returns if a tree binary operator converted to a code binary operator
     * requires a complementary swapping of operands, because the
     * operator's meaning needed to be modified.<br>
     *
     * See the docs of the companion method <code>treeToCodeSwapOperator</code>
     * foe details.
     *
     * @param operatorType              tree operator
     * @return                          if operands should be swapped in the
     *                                  conversion
     */
    public static boolean treeToCodeSwap(BinaryExpression.Op operator) {
        boolean swapOperands;
        switch(operator) {
            case GREATER:
            case GREATER_OR_EQUAL:
                // these two operators are non--existent in the code
                swapOperands = true;
                break;

            default:
                swapOperands = false;
                break;
        }
        return swapOperands;
    }
    /**
     * Creates a new internal local variable, without a specified
     * primitive range.
     * 
     * @param type                      type
     * @param suffix                    name suffix, null for default
     * @param category                  category, must be internal
     * @return                          dereference to the new local
     *                                  variable
     */
    private CodeFieldDereference newLocal(Type type, String suffix,
            Variable.Category category) {
        return currMethod.newLocal(type, suffix, category);
    }
    /**
     * Creates a new internal local variable, without a specified
     * primitive range, and with a default name.<br>
     * 
     * This is a convenience method.
     * 
     * @param type                      type
     * @param category                  category, must be internal
     * @return                          dereference to the new local
     *                                  variable
     */
    private CodeFieldDereference newLocal(Type type, Variable.Category category) {
        return newLocal(type, null, category);
    }
    /**
     * Creates a new local of the category
     * <code>Variable.Category.INTERNAL_NOT_RANGE</code>, of the type of
     * <code>source</code> or of elements of <code>source</code>,
     * <code>sourceElement</code> is true. If <code>source</code> has a
     * primitive range, then the new local gets a range too, with:<br>
     *
     * (1) assuming <code>source</code> is a field: its own range variables,
     * and assignments
     * appended to <code>pending</code>, that copies the values in
     * <code>source</code>'s primitive range to newly created locals
     * in the primitive range of the returned local;<br>
     *
     * (2) assuming <code>source</code> is a local: range variable being the
     * same as in <code>source</code>.<br>
     *
     * Note that for (1) and (2) to work, at the end of <code>pending</code>
     * <code>source</code>'s primitive range variables must have valid values.
     *
     * @param source                    a local or field, of type being either
     *                                  primitive or an array of primitives
     * @param sourceElement             use the type of <code>source</code>'s
     *                                  element, instead of that of <code>source</code>;
     *                                  usualy used when the new variable should
     *                                  contain a value of <code>source</code>'s
     *                                  element
     * @param inheritRangeSourceLevel   if true and <code>source<code>'s range
     *                                  is source--level, then let the new local's
     *                                  range be source--level as well; if false,
     *                                  let possible range of the ne wlocal be
     *                                  never source--level
     * @param indexCount                if zero
     * @return                          a new local, possibly with a primitive
     *                                  range
     */
    CodeFieldDereference newBoundedLocal(CodeFieldDereference source, boolean sourceElement,
            Code pending, boolean inheritRangeSourceLevel) {
        boolean targetSourceLevel =
                inheritRangeSourceLevel && source.variable.hasRestrictiveSourceLevelRange();
        Type type;
        if(sourceElement)
            type = source.getResultType().getElementType(null, 0);
        else
            type = source.getResultType();
        CodeFieldDereference out = newLocal(type, Variable.Category.INTERNAL_NOT_RANGE);
        if(source.variable.hasPrimitiveRange(false)) {
            Type rangeType;
            try {
                rangeType = PrimitiveRangeDescription.getVariablePrimitiveRangeType(
                        null, source.getResultType(), null);
            } catch(ParseException e) {
                throw new RuntimeException("unexpected exception: " +
                        e.toString());
            }
            if(source.isLocal()) {
                out.variable.primitiveRange = new CodeVariablePrimitiveRange(
                        source.variable.getStreamPos(),
                        null, null,
                        targetSourceLevel);
                out.variable.primitiveRange.min = source.variable.primitiveRange.min;
                out.variable.primitiveRange.max = source.variable.primitiveRange.max;
            } else {
                CodeFieldDereference outMin = newLocal(rangeType,
                        Variable.Category.INTERNAL_VARIABLE_PR);
                CodeFieldDereference outMax = newLocal(rangeType,
                        Variable.Category.INTERNAL_VARIABLE_PR);
                out.variable.primitiveRange = new CodeVariablePrimitiveRange(
                        source.variable.getStreamPos(),
                        outMin.variable, outMax.variable,
                        targetSourceLevel);
                CodeOpAssignment aMin = new CodeOpAssignment(null);
                aMin.rvalue = new RangedCodeValue(source.variable.primitiveRange.
                        reconstructMin(source.object));
                aMin.result = outMin;
                CodeOpAssignment aMax = new CodeOpAssignment(null);
                aMax.rvalue = new RangedCodeValue(source.variable.primitiveRange.
                        reconstructMax(source.object));
                aMax.result = outMax;
                pending.ops.add(aMin);
                pending.ops.add(aMax);
            }
        }
        return out;
    }
    /**
     * Creates a new unique label.
     * 
     * @return                          reference to the new local
     *                                  variable
     */
    private CodeLabel newLabel() {
        return new CodeLabel("#l" + (nextlabelId++));
    }
    /**
     * Updates constructor names in a class which name is to be changed.
     * 
     * @param clazz                     class, with its name not yet changed
     * @param newName                   new name of the class
     */
    private void updateConstructors(ImplementingClass clazz, String newName) {
        JavaClassScope scope = clazz.scope;
        List<MethodSignature> methodList =
                new LinkedList<>(scope.methods.keySet());
        for(MethodSignature key : methodList) {
            Method method = scope.methods.get(key);
            if(method.name.equals(clazz.name)) {
                // this method is a constructor, update its name
                method.name = newName;
                MethodSignature prevSignature = method.signature;
                method.refreshSignature();
                scope.methods.remove(prevSignature);
                scope.methods.put(method.signature, method);
                int i = scope.listOfMethods.indexOf(prevSignature);
                scope.listOfMethods.remove(i);
                scope.listOfMethods.add(i, method.signature);
            }
        }
    }
    /**
     * Modifies merged code by deleting redundant assignments and calls
     * and setting up the maps of replacement variables.<br>
     *
     * Called recursively to access recursive blocks.
     *
     * @param source                    method from which the code is copied
     * @param target                    method to which the code is copied
     * @param block                     currently processed block of code
     * @param replacements              map of field replacements
     */
    private void modifyMergedCode(Method source, Method target,
            List<AbstractStatement> code, Map<Variable, Variable> replacements) {
        boolean superCallFound = false;
        for(int index = 0; index < code.size(); ++index) {
            AbstractStatement s = code.get(index);
            if(s instanceof AssignmentExpression) {
                AssignmentExpression a = (AssignmentExpression)s;
                if(a.lvalue.value.isVariableExpression()) {
                    Variable v = (Variable)a.lvalue.value.contents.get(
                            a.lvalue.value.contents.size() - 1);
                    // might be true only for the "length"
                    // fields as arguments are in <code>replacements</code>
                    if(thrownOutVariables.containsKey(v))
                        // this is an initializator assignment to
                        // one of the common length fields, skip it
                        // as one is already present and also this
                        // one points to the just deleted
                        // <code>Variable</code> object
                        s = new EmptyExpression(s.getStreamPos(),
                                (BlockScope)s.outerScope);
                }
            } else if(s instanceof CallExpression) {
                CallExpression c = (CallExpression)s;
                if(c.method.signature.extractUnqualifiedNameString().equals(AbstractJavaClass.INIT_OBJECT) ||
                    // second call to superconstructor is the redundant one
                    (c.method instanceof Constructor && superCallFound))
                    // redundant call to <code>*static</code> or
                    // <code>*object</code>
                    s = new EmptyExpression(s.getStreamPos(),
                            (BlockScope)s.outerScope);
                if(c.method instanceof Constructor)
                    superCallFound = true;
            } else if(s instanceof Block)
                modifyMergedCode(source, target, ((Block)s).code, replacements);
            s.thrownOutReplacement = new HashMap<Variable, Variable>(
                    replacements);
            if(source == null)
                throw new RuntimeException("missing counterpart to " + target.toString() + " found");
            if(target == null)
                throw new RuntimeException("missing counterpart to " + source.toString() + " found");
            s.thrownOutReplacement.put(source.objectThis, target.objectThis);
            for(int i = 0; i < source.arg.size(); ++i) {
                Variable sv = source.arg.get(i);
                Variable tv = target.arg.get(i);
                s.thrownOutReplacement.put(sv, tv);
            }
            if(source.code.code == code)
                // append top statements to the top statements in
                // the target block
                target.code.code.add(s);
            else
                // otherwise replace a statement within the scanned
                // block
                code.set(index, s);
        }
    }
    /**
     * Check if there should be something replaced in a special class,
     * like the class' name or some field name.<br>
     *
     * The replacemenets are done in special classes like the array class
     * or the string class.<br>
     *
     * Ignores undefined special classes.<br>
     *
     * Sets <code>CodeCompilation.arraysEnabled</code> and
     * <code>CodeCompilation.stringsEnabled</code> if a respective special
     * class is found, either not yet renamed, or already renamed, what
     * happens if this is not the first time code is generated from
     * <code>Compilation</code>.
     *
     * @param compilation               compilation
     * @param packageScope              scope of the class' package,
     *                                  can not be a copy, its fields
     *                                  <code>javaClasses</code> and
     *                                  <code>javaClassScopes</code> might
     *                                  be modified by this method
     * @param j                         java class to check
     * @return                          if the checked java class should
     *                                  be thrown out as another frontend
     *                                  has already defined its equivalent
     */
    private boolean checkClassReplacements(Compilation compilation,
            PackageScope packageScope, AbstractJavaClass j) throws ParseException {
        boolean thrownOut = false;
        String replacedKey = null;
        String name = j.getQualifiedName().toString();
        AbstractFrontend f = j.frontend;
        if(name.equals(f.objectClassName)) {
            // special object class
            ImplementingClass c = (ImplementingClass)j;
            replacedKey =  c.name;
            updateConstructors(c, Type.OBJECT_CLASS_NAME);
            c.name = Type.OBJECT_CLASS_NAME;
        } else if(name.equals(f.arrayClassName)) {
            // special array class
            ImplementingClass c = (ImplementingClass)j;
            replacedKey =  c.name;
            updateConstructors(c, Type.ARRAY_CLASS_NAME);
            c.name = Type.ARRAY_CLASS_NAME;
            Variable v;
            if((v = c.scope.fields.get(f.arrayClassLengthFieldName)) != null) {
                c.scope.fields.remove(f.arrayClassLengthFieldName);
                v.name = Type.ARRAY_CLASS_LENGTH_FIELD_NAME;
                c.scope.fields.put(v.name, v);
                int i = c.scope.listOfFields.indexOf(f.arrayClassLengthFieldName);
                c.scope.listOfFields.remove(i);
                c.scope.listOfFields.add(i, v.name);
            } else
                throw new RuntimeException("array length field missing");
            codeCompilation.arraysEnabled = true;
        } else if(name.equals(f.stringClassName)) {
            // special string class
            ImplementingClass c = (ImplementingClass)j;
            replacedKey =  c.name;
            updateConstructors(c, Type.STRING_CLASS_NAME);
            c.name = Type.STRING_CLASS_NAME;
            Variable v;
            if((v = c.scope.fields.get(f.stringClassLengthFieldName)) != null) {
                c.scope.fields.remove(f.stringClassLengthFieldName);
                v.name = Type.STRING_CLASS_LENGTH_FIELD_NAME;
                c.scope.fields.put(v.name, v);
                int i = c.scope.listOfFields.indexOf(f.stringClassLengthFieldName);
                c.scope.listOfFields.remove(i);
                c.scope.listOfFields.add(i, v.name);
            } else
                throw new RuntimeException("string length field missing");
            codeCompilation.stringsEnabled = true;
        } else if(name.equals(f.rootExceptionClassName)) {
            // special root exception class
            ImplementingClass c = (ImplementingClass)j;
            replacedKey =  c.name;
            updateConstructors(c, Type.ROOT_EXCEPTION_CLASS_NAME);
            c.name = Type.ROOT_EXCEPTION_CLASS_NAME;
            Variable v;
            if((v = c.scope.fields.get(f.rootExceptionClassMessageFieldName)) != null) {
                c.scope.fields.remove(f.rootExceptionClassMessageFieldName);
                v.name = Type.ROOT_EXCEPTION_CLASS_MESSAGE_FIELD_NAME;
                c.scope.fields.put(v.name, v);
                int i = c.scope.listOfFields.indexOf(f.rootExceptionClassMessageFieldName);
                c.scope.listOfFields.remove(i);
                c.scope.listOfFields.add(i, v.name);
            } else
                throw new RuntimeException("root exception message field missing");
            codeCompilation.rootExceptionEnabled = true;
        } else if(name.equals(f.uncheckedExceptionClassName)) {
            // special root exception class
            ImplementingClass c = (ImplementingClass)j;
            replacedKey =  c.name;
            updateConstructors(c, Type.UNCHECKED_EXCEPTION_CLASS_NAME);
            c.name = Type.UNCHECKED_EXCEPTION_CLASS_NAME;
            codeCompilation.uncheckedExceptionEnabled = true;
        } else if(name.equals(packageScope.getPackageName() + "." + Type.ARRAY_CLASS_NAME)) {
            codeCompilation.arraysEnabled = true;
        } else if(name.equals(packageScope.getPackageName() + "." + Type.STRING_CLASS_NAME)) {
            codeCompilation.stringsEnabled = true;
        } else if(name.equals(packageScope.getPackageName() + "." + Type.ROOT_EXCEPTION_CLASS_NAME)) {
            codeCompilation.rootExceptionEnabled = true;
        } else if(name.equals(packageScope.getPackageName() + "." + Type.UNCHECKED_EXCEPTION_CLASS_NAME)) {
            codeCompilation.uncheckedExceptionEnabled = true;
        }
        if(replacedKey != null) {
            packageScope.javaClasses.remove(replacedKey);
            packageScope.javaClassScopes.remove(replacedKey);
            PackageScope systemPackageScope = compilation.packages.packageScopes.get(
                    Type.SYSTEM_PACKAGE);
            j.scope.parent = systemPackageScope;
            // if two or more frontends define a special class,
            // only a random one is selected, the others have
            // their marker methods copied to the selected one,
            // and then are discarded
            if(!systemPackageScope.javaClasses.containsKey(j.name)) {
                systemPackageScope.javaClasses.put(j.name, j);
                systemPackageScope.javaClassScopes.put(j.name, j.scope);
            } else {
                //
                // remove the java class
                //
                AbstractJavaClass selected = systemPackageScope.javaClasses.get(j.name);
                // move fields in order
                // per--statement replacements
                Map<Variable, Variable> localReplacements = new HashMap<Variable, Variable>();
                for(String n : j.scope.listOfFields) {
                    Variable v = j.scope.fields.get(n);
                    if(!v.name.equals(Type.ARRAY_CLASS_LENGTH_FIELD_NAME) &&
                            !v.name.equals(Type.STRING_CLASS_LENGTH_FIELD_NAME) &&
                            !v.name.equals(Type.ROOT_EXCEPTION_CLASS_MESSAGE_FIELD_NAME)) {
                        if(selected.scope.fields.containsKey(v.name))
                            throw new ParseException(j.getStreamPos(), ParseException.Code.DUPLICATE,
                                "special class shares a field " + v.getSourceName() +
                                " with " + selected.getQualifiedName());
                        selected.scope.addField(v);
                    } else
                        thrownOutVariables.put(v, selected.scope.fields.get(v.name));
                }
                // move methods in order
                for(MethodSignature s : j.scope.listOfMethods) {
                    Method m = j.scope.methods.get(s);
                    /*
                    if(!source.annotations.contains(CompilerAnnotation.MARKER_STRING))
                        throw new ParseException(j.getStreamPos(), ParseException.Code.ILLEGAL,
                                "special class has non--marker method " +
                                source.getDeclarationDescription());
                     */
                    if(m.signature.extractUnqualifiedNameString().equals(AbstractJavaClass.INIT_STATIC) ||
                            m.signature.extractUnqualifiedNameString().equals(AbstractJavaClass.INIT_OBJECT) ||
                            m instanceof Constructor) {
                        // append the code
                        Method target = selected.scope.methods.get(m.signature);
                        if(target == null && m instanceof Constructor) {
                            Method selectedConstructor = null;
                            for(Method n : selected.scope.methods.values())
                                if(n instanceof Constructor) {
                                    if(selectedConstructor != null)
                                        throw new RuntimeException("can not find the according " +
                                                "constructor neither by signature nor by uniqueness");
                                    selectedConstructor = n;
                                }
                            target = selectedConstructor;
                            if(target == null)
                                throw new RuntimeException("can not find the according " +
                                        "constructor neither by signature nor by uniqueness");
                        }
                        modifyMergedCode(m, target, m.code.code, localReplacements);
                        if(m instanceof Constructor)
                            // within methods of the removed class, only constructors can
                            // still have references to them in the code
                            thrownOutMethods.put(m, target);
                    } else {
                        if(selected.scope.methods.containsKey(m.signature)) {
                            Method m2 = selected.scope.methods.get(m.signature);
                            if(!m.annotations.contains(CompilerAnnotation.MARKER_STRING) ||
                                    !m2.annotations.contains(CompilerAnnotation.MARKER_STRING))
                                throw new ParseException(j.getStreamPos(), ParseException.Code.DUPLICATE,
                                        "special class shares a method " + m.getDeclarationDescription(true) +
                                        " with ${" + selected.getQualifiedName() + "} and at least one " +
                                        " of these is not a marker method");
                            boolean equal = true;
                            for(String a : m.annotations)
                                if(!m2.annotations.contains(a)) {
                                    equal = false;
                                    break;
                                }
                            for(String a : m2.annotations)
                                if(!m.annotations.contains(a)) {
                                    equal = false;
                                    break;
                                }
                            if(!equal)    
                                throw new ParseException(j.getStreamPos(), ParseException.Code.DUPLICATE,
                                        "special class shares a marker method " + m.getDeclarationDescription(true) +
                                        " with ${" + selected.getQualifiedName() + "} but annotations differ");
                            else {
                                //j.scope.methods.remove(m2.signature);
                                //j.scope.listOfMethods.remove(m2.signature);
                                thrownOutMethods.put(m, m2);
                            }
                        } else {
                            selected.scope.addMethod(m);
                            // method is reparented
                            selected.scope.methodScopes.put(m.signature, m.scope);
                        }
                    }
                }
                // remove the source class
                thrownOut = true;
            }
        }
        return thrownOut;
    }
    /**
     * Check if the type points to a special class, and if yes, the type
     * is updated to reflects the class name change. Also looks for
     * types in parameters.<br>
     *
     * Ignores undefined special classes.<br>
     *
     * For the default frontend of <code>currClass.frontend</code>,
     * use <code>checkTypeReplacement(Type)</code>.<br>
     *
     * @param type                      type to check
     * @param frontend                  frontend to use
     */
    private void checkTypeReplacement(Type type, AbstractFrontend frontend)
            throws ParseException {
        if(type.isJava()) {
            String name = type.getJava().toString();
            switch(frontend.getSpecialClassCategory(name)) {
                case NONE:
                    // do nothing
                    break;

                case OBJECT:
                    type.type = new NameList(Type.OBJECT_QUALIFIED_CLASS_NAME);
                    break;

                case ARRAY:
                    type.type = new NameList(Type.ARRAY_QUALIFIED_CLASS_NAME);
                    break;

                case STRING:
                    type.type = new NameList(Type.STRING_QUALIFIED_CLASS_NAME);
                    break;

                case ROOT_EXCEPTION:
                    type.type = new NameList(Type.ROOT_EXCEPTION_QUALIFIED_CLASS_NAME);
                    break;

                case UNCHECKED_EXCEPTION:
                    type.type = new NameList(Type.UNCHECKED_EXCEPTION_QUALIFIED_CLASS_NAME);
                    break;

                default:
                    throw new RuntimeException("unknown category of special class");
            }
        } else {
            if(type.mutatorReference)
                throw new RuntimeException("primitive type has mutator flag");
        }
        for(Type t : type.parameters)
            checkTypeReplacement(t, frontend);
    }
    /**
     * Calls <code>checkTypeReplacement(Type, AbstractFrontend)</code>,
     * with the frontend set to <code>currClass.frontend</code>.<br>
     *
     * This is a convenience method.
     *
     * @param type                      type to check
     */
    private void checkTypeReplacement(Type type) throws ParseException {
        checkTypeReplacement(type, currClass.frontend);
    }
    /**
     * Replaces special classes and sets code classes and field code
     * variables before code generation. Updates types of fields
     * to new names of special classes.
     * 
     * @param c                         compilation
     */
    private void generateCodeObjects(Compilation c) throws ParseException {
        for(String packagE : c.packages.packageScopes.keySet()) {
            PackageScope p = c.packages.packageScopes.get(packagE);
            // make a copy as elements might be replaced in the original
            Map<String, JavaClassScope> javaClassMapCopy =
                    new HashMap<>(p.javaClassScopes);
            for(String javaClass : javaClassMapCopy.keySet()) {
                JavaClassScope j = javaClassMapCopy.get(javaClass);
                if(checkClassReplacements(c, p, (AbstractJavaClass)j.owner)) {
                    if(j.getExtends() == null && !j.listOfFields.isEmpty())
                        throw new RuntimeException("root class can not have field variables");
                    // throw this java class out
                    continue;
                }
                if(j.getExtends() != null)
                    checkTypeReplacement(j.getExtends(), ((AbstractJavaClass)j.owner).frontend);
                AbstractJavaClass clazz = (AbstractJavaClass)j.owner;
                CodeClass cc = new CodeClass(clazz.getQualifiedName().toString(),
                        clazz.getStreamPos());
                cc.frontend = clazz.frontend;
                cc.specialComment = clazz.specialComment;
                cc.tags = clazz.tags;
                cc.isFromLibrary = clazz.isFromLibrary;
                clazz.codeClass = cc;
                for(String n : j.listOfFields) {
                    Variable variable = j.fields.get(n);
                    // update field types
                    checkTypeReplacement(variable.type, cc.frontend);
                    CodeVariable cv = new CodeVariable(variable, cc);
                    // fields are always initialized, the intialization
                    // takes place in in the *static and *object methods
if(variable.toString().contains("OUT"))
    variable = variable;
                    variable.codeVariable = cv;
                    cc.fields.put(cv.name, cv);
                    cc.listOfFields.add(cv);
                }
                // complete code variables of primitive ranges of fields
                for(Variable variable : j.fields.values())
                    if(variable.primitiveRange != null) {
                        variable.codeVariable.primitiveRange =
                                new CodeVariablePrimitiveRange(
                                    // no internal variables here
                                    variable.getStreamPos(),
                                    variable.primitiveRange.min.codeVariable,
                                    variable.primitiveRange.max.codeVariable,
                                    true);
                    }
            }
        }
        // add references to extended and implemented classes
        for(String packagE : c.packages.packageScopes.keySet()) {
            PackageScope p = c.packages.packageScopes.get(packagE);
            for(String javaClass : p.javaClassScopes.keySet()) {
                AbstractScope j = p.javaClassScopes.get(javaClass);
                AbstractJavaClass clazz = (AbstractJavaClass)j.owner;
                CodeClass cc = clazz.codeClass;
                if(clazz.extendS != null) {
                    AbstractJavaClass extendS = clazz.scope.lookupJavaClassAT(
                            clazz.extendS.getJava());
                    cc.extendS = extendS.codeClass;
                }
                for(Type i : clazz.implementS) {
                    AbstractJavaClass interfaceClass = clazz.scope.lookupJavaClassAT(
                            i.getJava());
                    cc.implementS.add(interfaceClass.codeClass);
                }
            }
        }
    }
    /*
     * Tests if a variable is initialized, throws parse exception
     * otherwise. Tests any source locals within dereferences,
     * fields and arrays are ignored as they
     * are always initialized, constants are ignored too.
     *
     * @param pos                       position in the source of
     *                                  the usage of the variable
     * @param value                     value to be tested
     */
    /*
    private void checkSourcesInitialized(StreamPos pos,
            AbstractCodeValue value) throws ParseException {
        if(value instanceof AbstractCodeDereference) {
            AbstractCodeDereference dereference = (AbstractCodeDereference)
                    value;
            Collection<CodeFieldDereference> read =
                    AbstractCodeDereference.extractSourcePartsInSource(
                    dereference);
            for(CodeFieldDereference f : read) {
                CodeVariable variable = f.variable;
                if(!variable.context) {
                    if(!variable.initialized)
                        throw new ParseException(pos,
                                "attempted to use an uninitialized variable " +
                                variable.name);
                }
            }
        }
    }
     */
    /**
     * Completes the code method references. Called after all code
     * methods are generated.
     */
    private void completeMethodReferences() {
        Collection<Code> list = codeCompilation.getCode();
        for(Code c : list)
            for(AbstractCodeOp op : c.ops) {
                CodeMethodReference r;
                if(op instanceof CodeOpAllocation)
                    r = ((CodeOpAllocation)op).constructor;
                else if(op instanceof CodeOpCall)
                    r = ((CodeOpCall)op).methodReference;
                else
                    r = null;
                if(r != null)
                    r.complete();
            }
    }
    /**
     * Completes the code method references. Called after all code
     * methods are generated.
     */
    private void completeClasses() throws CompilerException {
        for(CodeClass c : codeCompilation.classes.values())
            c.complete(compilation, codeCompilation);
    }
    /**
     * Adds references to super code methods and overridings of
     * code methods.
     * 
     * @param c                         compilation
     */
    private void completeOverridings() throws ParseException {
        for(String packagE : compilation.packages.packageScopes.keySet()) {
            PackageScope p = compilation.packages.packageScopes.get(packagE);
            for(String javaClass : p.javaClassScopes.keySet()) {
                JavaClassScope j = p.javaClassScopes.get(javaClass);
                List<MethodSignature> methodList =
                        new LinkedList<MethodSignature>(j.methods.keySet());
                for(MethodSignature key : methodList) {
                    Method method = j.methods.get(key);
/*if(method.name.equals("run"))
    method = method;*/
                    CodeMethod codeMethod = method.codeMethod;
                    for(AbstractJavaClass inClass : method.overridings.keySet())
/*{if(method.overridings.get(inClass) == null)
 method = method;*/
                        codeMethod.overridings.put(inClass.codeClass,
                            method.overridings.get(inClass).
                            codeMethod);
/*}*/
                }
            }
        }
    }
    /**
     * Checks if an assigned variable is final, if yes, throws a parse
     * exception.<br>
     * 
     * If blank finals are allowed, this method does not check
     * assigning of finals that are fields of a method's owner,
     * or locals, as such finals are checked later by
     * <code>checkAssignmentOfFinals</code>.
     *
     * @param pos                       stream position in the exception
     * @param d                         dereference containing the
     *                                  assigned variable
     * @see #checkAssignmentOfFinals
     */
    protected void checkFinalVariableSimple(StreamPos pos, AbstractCodeDereference d)
            throws ParseException {
         if(compilation.options.blankFinalsAllowed &&
                 (d.getTypeSourceVariable().isLocal() ||
                 d.getTypeSourceVariable().owner == currClass))
             return;
        // <code>CodeIndexDereference</code> never causes assignment
        // to a variable, but to the variable's contents, what is not
        // controlled by the final flag
        if(d instanceof CodeFieldDereference) {
            CodeFieldDereference f = (CodeFieldDereference)d;
            if(f.variable.finaL)
                if(f.variable.isInternal() && !compilation.options.blankFinalsAllowed) {
                    if(!(compilation.options.test != null &&
                            compilation.options.test.ignoreInternalBlankFinals))
                        throw new RuntimeException("final internal variable " +
                                f.variable.toNameString() + " assigned outside declaration, " +
                                "but blank finals not allowed");
                } else
                    throw new ParseException(pos, ParseException.Code.ILLEGAL,
                            "${finalVariable} " + f.variable.toSourceNameString() + " " +
                            (compilation.options.blankFinalsAllowed ?
                                "is already assigned" :
                                "can not be assigned outside declaration"));
        }
    }
    /**
     * If blank finals are enabled, checks all finals, excluding fields, that
     * do not belong to <code>method</code>'s owner, as that part
     * is always checked by <code>checkFinalVariableSimple</code>.<br>
     * 
     * If blank finals are disabled, then does nothing,
     * as <code>checkFinalVariableSimple</code> is sufficient if there are
     * no blank finals.
     * 
     * @param method
     * @see #checkFinalVariableSimple
     */
    protected void checkAssignmentOfFinals(CodeMethod method)
            throws ParseException {
        ParseException errors = new ParseException();
         if(!compilation.options.blankFinalsAllowed)
             return;
//if(method.signature.toString().indexOf("Blank") != -1)
//    method = method;
        CodeClass clazz = method.owner;
        for(CodeVariable cv : clazz.listOfFields) {
            if(cv.finaL &&
                    // do not check for illegal assignments of non--static
                    // variables in static methods
                    (cv.context != Context.NON_STATIC || method.context == Context.NON_STATIC)) {
                try {
                    TraceValues.checkInitializedFinals(method, cv);
                } catch(ParseException e) {
                    errors.addAllReports(e);
                }
                if(((cv.context == Context.NON_STATIC &&
                            (method.isObjectInitializationMethod() || method.isConstructor())) ||
                        (cv.context != Context.NON_STATIC && method.isStaticInitializationMethod())) &&
                        // for *static, all static finals are blank in a sense;
                        // for *object, all non--static finals are blank in a sense
                        (cv.blankFinalField || method.isStaticInitializationMethod() ||
                            method.isObjectInitializationMethod()))
                    try {
                        TraceValues.checkBlankFinalInitialized(method, cv,
                                !method.isObjectInitializationMethod());
                    } catch(ParseException e) {
                        errors.addAllReports(e);
                    }
            }
        }
        // check final locals
        for(CodeVariable cv : method.variables.values()) {
            if(cv.finaL)
                try {
                    TraceValues.checkInitializedFinals(method, cv);
                } catch(ParseException e) {
                    errors.addAllReports(e);
                }
        }
        if(errors.reportsExist())
            throw errors;
    }
/**
     * Copies the property <code>canBeDead</code> if the
     * property is true, and appends tags
     * from a statement to the statement's code.<br>
     *
     * Used at the end of each <code>visit()</code> method
     * that generates code.
     *
     * @param s
     * @param code
     */
    protected static void copyProperties(AbstractStatement s, Code code) {
        if(s.canBeDead)
            code.setCanBeDead();
        code.appendTags(s.tags);
    }
    /**
     * Special checking of the *static and *object methods.<br>
     * 
     * For this method to work, initialization assignments must be
     * properly related to respective initialized fields or static
     * blocks. If there is a call, ensure that at least the fields
     * are initialized with the default value before.
     * 
     * @param cm method to test
     */
    protected void checkInitializationMethod(CodeMethod cm) throws CompilerException {
        CompilerException errors = new CompilerException();
        CodeClass clazz = cm.owner;
        int count = 0;
        boolean callFound = false;
        List<AbstractCodeOp> addOp = new LinkedList<>();
        List<Integer> addIndex = new LinkedList<>();
        for(AbstractCodeOp op : cm.code.ops) {
            if(op instanceof CodeOpCall && !callFound) {
                // the call may contain references to any of the field,
                // initialize the yet--not--initialized with default
                // values, unless it is a final variable and blank
                // finals are allowed -- as allowed blanks turn out
                // checking of precise initialization of finals in
                // <code>checkBlankFinals</code>
                //
                // the only possible jumps in an initialization method
                // never cross the border between initialiaztions of
                // two separate fields, so simply checking the operations
                // following the call works
                for(int i = count + 1; i < cm.code.ops.size(); ++i) {
                    AbstractCodeOp r = cm.code.ops.get(i);
                    Set<CodeFieldDereference> targets = r.getFieldTargets();
                    for(CodeFieldDereference f : targets) {
                        if(f.variable.owner == cm.owner &&
                                f.variable.context == cm.context) {
                            // a yet--not--initialized field found
                            if(!(compilation.options.blankFinalsAllowed &&
                                    f.variable.finaL)) {
                                CodeOpAssignment a = new CodeOpAssignment(null);
                                a.addTags(r.getTags());
                                a.result = f;
                                a.rvalue = new RangedCodeValue(
                                        new CodeConstant(null, Hedgeelleth.newDefaultInitializerLiteral(
                                            f.variable.getResultType())));
                                addOp.add(0, a);
                                addIndex.add(0, count);
                            }
                        }
                    }
                }
                callFound = true;
            }
            List<CodeVariable> fields = new LinkedList<CodeVariable>();
            for(CodeFieldDereference f : op.getFieldSources()) {
                // cm.code.ops.get(25).getFieldSources()
                CodeVariable v = f.variable;
                if(v.owner == clazz) {
                    // the case of object initialization with static variables
                    // allows for forward field references
                    if(!(cm.context == Context.NON_STATIC && v.context != Context.NON_STATIC))
                        fields.add(v);
                } else if(cm.context != Context.NON_STATIC) {
                    // possible circular references are handled in the interpreter
//                    errors.addReport(new CompilerException.Report(op.getStreamPos(),
//                            CompilerException.Code.ILLEGAL,
//                            "static initialization wih a foreign field"));
                }
            }
            for(CodeFieldDereference f : op.getFieldTargets()) {
                CodeVariable v = f.variable;
                if(v.owner == clazz) {
                    // the case of object initialization with static variables
                    // allows for forward field references
                    if(!(cm.context == Context.NON_STATIC && v.context != Context.NON_STATIC))
                        fields.add(v);
                }
            }
            for(CodeVariable s : fields)
                // if these are equal, this might be a default initializer
                if(op.getStreamPos().compareTo(s.getStreamPos()) < 0)
                    errors.addReport(new CompilerException.Report(op.getStreamPos(),
                            CompilerException.Code.ILLEGAL,
                            "illegal forward field reference"));
            ++count;
        }
        for(int i = 0; i < addOp.size(); ++i) {
            AbstractCodeOp op = addOp.get(i);
            int index = addIndex.get(i);
            cm.code.ops.add(index, op);
        }
        if(errors.reportsExist())
            throw errors;
    }
    /**
     * Checks any usage of <code>this</code> before a superclass
     * is constructed, that is, befor <code>this()</code> or
     * <code>super()</code>.
     * 
     * @param method a constructor
     */
    protected void checkThisBeforeSuper(CodeMethod method) throws
            CompilerException {
        CompilerException errors = new CompilerException();
        // a constructor of <code>Object</code> does not have
        // a superclass to be constructed
        if(!method.owner.name.equals(Type.OBJECT_QUALIFIED_CLASS_NAME)) {
            for(AbstractCodeOp op : method.code.ops) {
                if(op instanceof CodeOpCall &&
                        ((CodeOpCall)op).methodReference.firstPassMethod
                            instanceof Constructor) {
                    // this() or super() found
                    CodeOpCall call = (CodeOpCall)op;
                    // first argument is always this, omit it
                    for(int i = 1; i < call.args.size(); ++i) {
                        AbstractCodeValue v = call.args.get(i).value;
                        if(v instanceof AbstractCodeDereference) {
                            AbstractCodeDereference d = (AbstractCodeDereference)v;
                            if(d.getContainerVariable() != null &&
                                    d.getContainerVariable().name.equals(Method.LOCAL_THIS))
                                errors.addReport(new CompilerException.Report(
                                        op.getStreamPos(), CompilerException.Code.ILLEGAL,
                                        "this referenced before superclass is constructed"));
                        }
                    }
                    // no longer checking
                    break;
                }
                Set<CodeVariable> vars = op.getLocalSources();
                vars.addAll(op.getLocalTargets());
                for(CodeVariable cv : vars)
                    if(cv.name.equals(Method.LOCAL_THIS))
                        errors.addReport(new CompilerException.Report(
                                op.getStreamPos(), CompilerException.Code.ILLEGAL,
                                "this referenced before superclass is constructed"));
            }
        }
        if(errors.reportsExist())
            throw errors;
    }
    @Override
    public Code visit(Node n) {
        System.out.println("Error: visit from " + n);
        return null;
    }
    @Override
    public Code visit(AbstractExpression e) {
        System.out.println("Error: visit from " + e);
        return null;
    }
    @Override
    public Code visit(EmptyExpression e) {
        Code code = new Code();
        if(e.toNoneOp) {
            CodeOpNone n = new CodeOpNone(e.getStreamPos());
            n.annotations.add(CompilerAnnotation.STATE_STRING);
            code.ops.add(n);
        } else if(e instanceof FinalLocalDeclaration) {
            CodeOpNone n = new CodeOpFinalDeclaration(e.getStreamPos(),
                    ((FinalLocalDeclaration)e).variable.codeVariable);
            code.ops.add(n);
        }
        copyProperties(e, code);
        return code;
    }
    @Override
    public Code visit(ConstantExpression e) throws CompilerException {
        Code code = new Code();
        CodeConstant c = new CodeConstant(e.getStreamPos(), e.literal);
        checkTypeReplacement(c.getResultType());
        code.result = new RangedCodeValue(c);
        copyProperties(e, code);
        return code;
    }
    @Override
    public Code visit(AllocationExpression e) throws CompilerException {
        Code code = new Code();
        CodeOpAllocation op = new CodeOpAllocation(e.getStreamPos());
        switch(e.allocation) {
            case CONSTRUCTOR:
                e.constructor = (Constructor)replaceThrownOutMethod(
                        e.constructor);
                CodeMethodReference constructor = new CodeMethodReference(null,
                        e.constructor);
                op.constructor = constructor;
                op.arrayType = null;
                break;
                
            case ARRAY:
                op.constructor = null;
                op.arrayType = e.getResultType();
                break;
        }
        for(AbstractExpression f: e.expressions) {
            Code subCode = (Code)f.accept(this);
            code.add(subCode);
            op.args.add(subCode.result);
        }
        checkTypeReplacement(e.getResultType());
        AbstractCodeDereference result =
                // allocation has never any primitive ranges imposed
                newLocal(e.getResultType(), Variable.Category.INTERNAL_NOT_RANGE);
        op.result = result;
        code.add(op);
        code.result = new RangedCodeValue(result);
        code.resultDiscardable = true;
        copyProperties(e, code);
        return code;
    }
    /**
     * Converts a that value, as defined in <code>visit(PrimaryExpression)</code>,
     * into a list of dereferences and possibly operations. The dereferences
     * are returned in a list, the operations are appended to <code>pending</code>.
     * The returned list does not contain indexing dereferences, as such dereferences are
     * changed into operations appended to the code.<br>
     * 
     * Assumes that <code>that</code>, if not null, is to be dereferenced in
     * some following code.<br>
     * 
     * This method is called when a variable or indexing expression are found.
     * 
     * @param that                      that value, or null
     * @param pending                   a list of operations to append to
     * @param allowDereferencing        false if the list of dereferences should
     *                                  at most contain a single variable, thus,
     *                                  no variable should be dereferenced within
     *                                  the list
     * @return                          list of dereferences, empty if no
     *                                  dereferences, at most of size 1 if
     *                                  dereferencing not allowed, otherwise
     *                                  at most of size 2. Does not contain
     *                                  indexing dereferences
     */
    private List<CodeVariable> thatIntoDereferences(AbstractCodeValue that,
            Code pending, boolean allowDereferencing) {
        List<CodeVariable> dereferences = new LinkedList<>();
        if(that != null) {
            if(that instanceof AbstractCodeDereference) {
                if(that instanceof CodeFieldDereference) {
                    CodeFieldDereference f = (CodeFieldDereference)that;
                    if(allowDereferencing) {
                        if(f.object != null)
                            dereferences.add(f.object);
                        dereferences.add(f.variable);
                    } else {
                        if(f.object == null)
                            dereferences.add(f.variable);
                        else {
                            CodeOpAssignment a = new CodeOpAssignment(
                                    f.getStreamPos());
                            CodeFieldDereference rvalue =
                                    new CodeFieldDereference(f);
                            a.rvalue = new RangedCodeValue(rvalue);
                            // initialize bound
                            a.result = new CodeFieldDereference(newBoundedLocal(
                                    rvalue, false, pending, true));
                            // add the assignment with bounded rvalue
                            pending.add(a);
                            dereferences.add(((CodeFieldDereference)a.result).
                                    variable);
                        }
                    }
                } else if(that instanceof CodeIndexDereference) {
                    CodeIndexDereference i = (CodeIndexDereference)that;
                    CodeOpAssignment a = new CodeOpAssignment(
                            i.getStreamPos());
                    a.rvalue = new RangedCodeValue(i);
                    a.result = new CodeFieldDereference(
                            // initialize bound
                            //
                            // possible primitive range of an array is copied to
                            // primitive range of the array's element
                            newBoundedLocal(
                            // <code>i.object</code> must either be a local or
                            // a static field
                            new CodeFieldDereference(i.object), true, pending,
                            true));
                    // add the assignment with bounded rvalue
                    pending.add(a);
                    dereferences.add(((CodeFieldDereference)a.result).
                            variable);
                }
            } else
                throw new RuntimeException("dereferenced constant");
        }
        return dereferences;
    }
    /**
     * Converts a list of non--index dereferences into "that", as defined in
     * <code>visit(PrimaryExpression)</code>. Does not check whether
     * for the illegal condition of a dereferencing static variable,
     * so the caller should care for that itself.<br>
     *
     * The size of the input list must be 1 ... 2, what results in "that" being,
     * respectively, a single variable, or a pair of variables, one dereferencing
     * another.
     *
     * @param dereferences              list of dereferences, excluding index
     *                                  dereferences
     * @return                          a dereference or null
     */
    private CodeFieldDereference dereferencesIntoThat(List<CodeVariable> dereferences) {
        CodeFieldDereference that;
        switch(dereferences.size()) {
            case 1:
                that = new CodeFieldDereference(dereferences.get(0));
                break;

            case 2:
                that = new CodeFieldDereference(dereferences.get(0),
                        dereferences.get(1));
                break;

            default:
                throw new RuntimeException(
                        "invalid dereference list size");
        }
        return that;
    }
    /**
     * Adds some pending code, and if any of the added operations
     * do not have a source position, it is propagated from the
     * previous operation.
     *
     * @param pos                       if the base code is empty,
     *                                  use this source position
     *                                  instead
     * @param code                      base code
     * @param pendind                   appended code, with source
     *                                  positions completed using
     *                                  the base code
     */
    protected static void addWithSourcePos(StreamPos pos,
            Code code, Code pending) {
        for(AbstractCodeOp op : pending.ops) {
            if(op.getStreamPos() == null) {
                StreamPos currentPos;
                if(code.ops.isEmpty())
                    currentPos = pos;
                else {
                    AbstractCodeOp last = code.ops.get(
                            code.ops.size() - 1);
                    currentPos = last.getStreamPos();
                }
                op.setStreamPos(currentPos);
            }
            code.ops.add(op);
        }
    }
    /**
     * If the source variable is to be replaced as determined by
     * global <code>thrownOutVariables</code> or local to statement
     * <code>currStatement.thrownOutVariables</code>, then
     * the replacement variable is returned. Otherwise, the original
     * source variable is returned.
     *
     * @param source                    variable to be possibly replaced
     * @return                          replacement variable or the original
     *                                  variable
     */
    protected Variable replaceThrownOutVariable(Variable source) {
        Variable replacement = thrownOutVariables.get(source);
if(currStatement == null)
    currStatement = currStatement;
        if(replacement == null &&
                currStatement.peek().thrownOutReplacement != null)
            replacement = currStatement.peek().thrownOutReplacement.get(source);
        if(replacement != null)
            return replacement;
        else
            return source;
    }
    /**
     * If the source method is to be replaced as determined by
     * <code>thrownOutMethods</code>, then
     * the replacement method is returned. Otherwise, the original
     * source method is returned.
     *
     * @param source                    method to be possibly replaced
     * @return                          replacement method or the original
     *                                  method
     */
    protected Method replaceThrownOutMethod(Method source) {
        Method replacement = thrownOutMethods.get(source);
        if(replacement != null)
            return replacement;
        else
            return source;
    }
    /*
     * Generates a code that sets ranges of the variable, that is
     * a call's result.
     * 
     * @param call call op
     * @param that "this" of the called method, null for a static method
     * @param ret variable, to which the call's result is assigned
     * @param pos position of the call
     * @return list of operations, empty for none
     */
    /*
    List<AbstractCodeOp> methodRangeCode_(CodeOpCall call,
            AbstractCodeDereference that, AbstractCodeDereference ret,
            StreamPos pos) {
        List<AbstractCodeOp> code = new LinkedList<>();
        CodeMethod method = call.methodReference.firstPassMethod.codeMethod;
        CodeVariablePrimitiveRange range;
        if(method == null)
            pos = pos;
        if(method.returnValue != null)
            range = method.returnRange;
        else
            range = null;
        if(range != null) {
            if(that != null) {
                if(that instanceof CodeIndexDereference ||
                        ((CodeFieldDereference)that).object != null) {
                    // simplify called's "this"
                    CodeOpAssignment calledThisAssignment = new CodeOpAssignment(
                            pos);
                    calledThisAssignment.rvalue = new RangedCodeValue(
                            that, null);
                    that = new CodeFieldDereference(
                            newLocal(method.owner.type));
                    calledThisAssignment.result = that;
                }
            }
            CodeVariable min = newLocal(call.result.getResultType()).variable;
            CodeVariable max = newLocal(call.result.getResultType()).variable;
            CodeVariable methodObject;
            if(that != null)
                methodObject = ((CodeFieldDereference)that).variable;
            else
                methodObject = null;
            CodeOpAssignment minAssignment = new CodeOpAssignment(pos);
            minAssignment.rvalue = new RangedCodeValue(
                    new CodeFieldDereference(methodObject,
                    ((CodeFieldDereference)range.min).variable), null);
            minAssignment.result = new CodeFieldDereference(min);
            CodeOpAssignment maxAssignment = new CodeOpAssignment(pos);
            maxAssignment.rvalue = new RangedCodeValue(
                    new CodeFieldDereference(methodObject,
                    ((CodeFieldDereference)range.max).variable), null);
            maxAssignment.result = new CodeFieldDereference(max);
            code.add(minAssignment);
            code.add(maxAssignment);
            ((CodeFieldDereference)call.result).variable.primitiveRange =
                    new CodeVariablePrimitiveRange(
                    // source level range
                    pos, min, max, true);
        }
        return code;
    }
    */
    /**
     * Generates a code that writes ranges of the return variable into
     * locals, and then updates the return variable's range respectively,
     * as there are originally fields written to the range, in
     * <code>CodeMethod.addVariables</code>.<br>
     * 
     * @param method method
     * @return list of operations, empty for none
     */
    List<AbstractCodeOp> methodRangeCode(CodeMethod method) {
        List<AbstractCodeOp> code = new LinkedList<>();
        CodeVariablePrimitiveRange range;
        if(method.returnValue != null)
            range = method.returnValue.primitiveRange;
        else
            range = null;
        if(range != null) {
            // add code
            CodeVariable thiS = method.getThisArg();
            Type returnType = method.returnValue.getResultType();
            CodeVariable min = newLocal(returnType, Variable.Category.INTERNAL_VARIABLE_PR).
                    variable;
            CodeVariable max = newLocal(returnType, Variable.Category.INTERNAL_VARIABLE_PR).
                    variable;
            StreamPos pos = method.getStreamPos();
            CodeOpAssignment minAssignment = new CodeOpAssignment(pos);
            minAssignment.rvalue = new RangedCodeValue(
                    new CodeFieldDereference(thiS,
                        ((CodeFieldDereference)range.min).variable), null);
            minAssignment.result = new CodeFieldDereference(min);
            CodeOpAssignment maxAssignment = new CodeOpAssignment(pos);
            maxAssignment.rvalue = new RangedCodeValue(
                    new CodeFieldDereference(thiS,
                        ((CodeFieldDereference)range.max).variable), null);
            maxAssignment.result = new CodeFieldDereference(max);
            code.add(minAssignment);
            code.add(maxAssignment);
            // update retval's range
            range.min = new CodeFieldDereference(min);
            range.max = new CodeFieldDereference(max);
        }
        return code;
    }
    /**
     * Finds an implementaion of a closure method in a given class.
     * 
     * @param className name of the class, containing the implementation
     * @param type closure type
     * @return a method marked with a compiler annotation CLOSURE_BOOLEAN or
     * CLOSURE_DOUBLE
     */
    protected Method findClosure(NameList className, PrimitiveOrVoid type)
            throws ParseException {
        CompilerAnnotation a;
        switch(type) {
            case BOOLEAN:
                a  = CompilerAnnotation.CLOSURE_BOOLEAN;
                break;
                
            case DOUBLE:
                a  = CompilerAnnotation.CLOSURE_DOUBLE;
                break;
                
            default:
                throw new RuntimeException("unexpected closure type");
        }
        Method bc = compilation.specialMethods.get(currClass.frontend).
                get(a);
        if(!(bc.owner instanceof Interface))
            throw new RuntimeException("closure methods must be declared in an interface");
        // String interfaceName = bc.owner.getQualifiedName().toString();
        AbstractJavaClass clazz = compilation.packages.lookupJavaClassNA(className);
        Method im = clazz.scope.methods.get(new MethodSignature(currClass.frontend,
                bc.name, new LinkedList<Typed>()));
        if(im == null)
            throw new RuntimeException("could not find " +
                    "an implementaion of a closure getter");
        return im;
    }
    @Override
    public Code visit(PrimaryExpression e) throws CompilerException {
        Code code = new Code();
        Code rangeCode = new Code();
        CodePrimaryPrimitiveRange range;
        if(options.primaryRangeChecking && e.prefix.rangeDescription != null) {
            range = new CodePrimaryPrimitiveRange(
                    new CodeFieldDereference(e.prefix.rangeDescription.
                        min.variable.codeVariable),
                    new CodeFieldDereference(e.prefix.rangeDescription.
                        max.variable.codeVariable));
            for(AbstractExpression a : e.prefix.rangeExpressions)
                rangeCode.add((Code)a.accept(this));
        } else
            range = null;
        Code primaryCode = new Code();
        ParsedPrimary p = e.value;
        AbstractCodeValue that = null;
        CodePrimaryPrimitiveRange thatRange = null;
        // pending code, to be possibly invalidated if a static
        // field/method is encountered
        Code pending = null;
        for(int pos = 0;  pos < p.contents.size(); ++pos) {
            if(thatRange != null)
                throw new RuntimeException("unused primitive range");
            Typed t = p.contents.get(pos);
            if(t instanceof CallExpression) {
                CallExpression c = (CallExpression)t;
                c.method = replaceThrownOutMethod(c.method);
                // check for a conditional barrier
                CompilerAnnotation barrier = null;
                if(!c.method.arg.isEmpty())
                    barrier = CompilerAnnotation.getBarrierType(c.method.annotations);
                boolean staticMethod = c.method.flags.context != Context.NON_STATIC;
                if(pending != null) {
                    if(!staticMethod)
                        addWithSourcePos(e.getStreamPos(), primaryCode, pending);
                    pending = null;
                }
                if(c.independentCallThis != null)
                    throw new RuntimeException("independent \"this\" within " +
                            "a primary expression");
                Code subCode = (Code)c.accept(this);
                CodeLabel barrierLabel;
                if(barrier != null) {
                    switch(barrier) {
                        // any of the non--void types
                        case BARRIER_CONDITIONAL:
                        case BARRIER_CLOSURE:
                        case BARRIER_PRODUCE_CONDITIONAL:
                        case BARRIER_PRODUCE_CLOSURE:
                        case BARRIER_CONSUME_CONDITIONAL:
                        case BARRIER_CONSUME_CLOSURE:
                        case PAIR_BARRIER_CONDITIONAL:
                        case PAIR_BARRIER_CLOSURE:
                        case PAIR_BARRIER_PRODUCE_CONDITIONAL:
                        case PAIR_BARRIER_PRODUCE_CLOSURE:
                        case PAIR_BARRIER_CONSUME_CONDITIONAL:
                        case PAIR_BARRIER_CONSUME_CLOSURE:
                            AbstractCodeOp barrierInit = new CodeOpNone(subCode.ops.get(0).getStreamPos());
                            barrierInit.label = barrierLabel = newLabel();
                            subCode.ops.add(0, barrierInit);
                            break;
                            
                        default:
                            barrierLabel = null;
                            break;
                    }
                } else
                    barrierLabel = null;
// if(!(subCode.ops.get(subCode.ops.size() - 1) instanceof CodeOpCall))
//     subCode = subCode;
                CodeOpCall call = (CodeOpCall)subCode.ops.get(
                        subCode.ops.size() - 1);
                if(!staticMethod) {
                    // this call is a part of a primary expression,
                    // so "this" is not given in <code>CallExpression</code>
                    // but is extracted when processing the primary
                    // expression within this method
                    call.args.add(0, new RangedCodeValue(that));
                    /**
                     * return ranges are now generated in the method
                    if(call.methodReference.method != null)
                        subCode.ops.addAll(0, methodRangeCode(call,
                                (AbstractCodeDereference)that, call.getResult(), c.getStreamPos()));
                                */
                }
                if(barrier != null) {
                    switch(barrier) {
                        case BARRIER_CLOSURE:
                        case BARRIER_PRODUCE_CLOSURE:
                        case BARRIER_CONSUME_CLOSURE:
                        case PAIR_BARRIER_PRODUCE_CLOSURE:
                        case PAIR_BARRIER_CONSUME_CLOSURE:
                            // replace *_BARRIER_*_CLOSURE with *_BARRIER_*_CONDITION
                            CompilerAnnotation replacement;
                            switch(barrier) {
                                case BARRIER_CLOSURE:
                                    replacement = CompilerAnnotation.BARRIER_CONDITIONAL;
                                    break;
                                    
                                case BARRIER_PRODUCE_CLOSURE:
                                    replacement = CompilerAnnotation.BARRIER_PRODUCE_CONDITIONAL;
                                    break;
                                    
                                case BARRIER_CONSUME_CLOSURE:
                                    replacement = CompilerAnnotation.BARRIER_CONSUME_CONDITIONAL;
                                    break;
                                    
                                case PAIR_BARRIER_PRODUCE_CLOSURE:
                                    replacement = CompilerAnnotation.PAIR_BARRIER_PRODUCE_CONDITIONAL;
                                    break;
                                    
                                case PAIR_BARRIER_CONSUME_CLOSURE:
                                    replacement = CompilerAnnotation.PAIR_BARRIER_CONSUME_CONDITIONAL;
                                    break;

                                default:
                                    throw new RuntimeException("unexpected annotation");
                            }
                            AbstractCodeValue condition = call.args.get(1).value;
                            NameList javaType = condition.getResultType().getJava();
                            if(javaType != null) {
                                Method m = findClosure(javaType, Type.PrimitiveOrVoid.BOOLEAN);
                                CodeOpCall conditionCall = new CodeOpCall(call.getStreamPos());
                                conditionCall.methodReference = new CodeMethodReference(
                                        call.getStreamPos(), m);
                                conditionCall.args.add(call.args.get(1));
                                conditionCall.result = newLocal(
                                        new Type(Type.PrimitiveOrVoid.BOOLEAN),
                                        Variable.Category.INTERNAL_NOT_RANGE);
                                subCode.ops.add(subCode.ops.size() - 1, conditionCall);
                                Method cb = compilation.specialMethods.get(currClass.frontend).
                                        get(replacement);
                                CodeOpCall barrierCall = new CodeOpCall(call.getStreamPos());
                                barrierCall.methodReference = new CodeMethodReference(
                                        call.getStreamPos(), cb);
                                barrierCall.args.add(call.args.get(0));
                                barrierCall.args.add(new RangedCodeValue(conditionCall.result));
                                subCode.replace(subCode.ops.size() - 1, barrierCall, true);
                                call = barrierCall;
                                barrier = replacement;
                            }
                            break;
                            
                    }
                    switch(barrier) {
                        case BARRIER_CONDITIONAL:
                        case BARRIER_PRODUCE_CONDITIONAL:
                        case BARRIER_CONSUME_CONDITIONAL:
                        case PAIR_BARRIER_CONDITIONAL:
                        case PAIR_BARRIER_PRODUCE_CONDITIONAL:
                        case PAIR_BARRIER_CONSUME_CONDITIONAL:
                            CodeOpBranch barrierBranch = new CodeOpBranch(call.getStreamPos(), true);
                            barrierBranch.condition = new RangedCodeValue(call.args.get(1));
                            barrierBranch.labelRefFalse = new CodeLabel(barrierLabel);
                            subCode.ops.add(barrierBranch);
                            break;
                            
                        default:
                            ;
                    }
                }
if(call.args.size() == 5)
    call = call;
                call.makePreArgs();
                primaryCode.add(subCode);
                if(subCode.result.range != null)
                    throw new RuntimeException("unexpected range");
                that = call.result;
            } else if(t instanceof IndexExpression) {
                if(that == null)
                    throw new RuntimeException("dereferenced null");
                if(pending == null)
                    pending = new Code();
                IndexExpression ie = (IndexExpression)t;
                if(thatRange != null)
                    throw new RuntimeException("indexed variable has a primitive range");
                List<CodeVariable> dereferences =
                        thatIntoDereferences(that, pending, false);
                Code subCode = (Code)ie.index.accept(this);
                pending.add(subCode);
                RangedCodeValue indexResult;
                if(subCode.result.value instanceof AbstractCodeDereference) {
                    AbstractCodeDereference d = (AbstractCodeDereference)subCode.result.value;
                    if(d.object != null) {
                        // too complex for index dereference
                        CodeOpAssignment a = new CodeOpAssignment(
                                d.getStreamPos());
                        a.rvalue = subCode.result;
                        // initialize bound in <code>newBoundedLocal</code>
                        if(d instanceof CodeFieldDereference)
                            // copy possible variable's primitive range to
                            // the result
                            a.result = new CodeFieldDereference(newBoundedLocal(
                                    (CodeFieldDereference)d, false, pending, true));
                        else {
                            a.result = new CodeFieldDereference(newBoundedLocal(
                                    // get the array variable's primitive range,
                                    // copy to the array's element primitive range
                                    new CodeFieldDereference(((CodeIndexDereference)d).object),
                                    true, pending, true));
                        }
                        // add the assignment with bounded rvalue
                        pending.ops.add(a);
                        // the expression's primitive range is moved out of the result to
                        // <code>a</code>
                        indexResult = new RangedCodeValue(a.result);
                    } else
                        indexResult = subCode.result;
                } else
                    indexResult = subCode.result;
                if(indexResult.range != null)
                    throw new RuntimeException("unexpected range");
                CodeVariable array = dereferences.get(0);
                CodeIndexDereference i = new CodeIndexDereference(
                        array, indexResult);
                that = i;
            } else if(t instanceof AbstractExpression) {
                 // --> && ! index expression && ! call expression
                if(primaryCode.ops.size() != 0 || pending != null)
                    throw new RuntimeException("a generic expression not at the " +
                            "beginning");
                AbstractExpression a = (AbstractExpression)t;
                Code subCode = (Code)a.accept(this);
                that = subCode.result.value;
                thatRange = subCode.result.range;
                if(range != null && thatRange != null) {
                    // two ranges, place for one, thus use an assignment to
                    // put one of these ranges into it
                    CodeOpAssignment assignment = new CodeOpAssignment(null);
                    assignment.rvalue = new RangedCodeValue(that, thatRange);
                    CodeFieldDereference subResult = newLocal(that.getResultType(),
                            Variable.Category.INTERNAL_NOT_RANGE);
                    // avoid an internal variable with large number of states
                    subResult.variable.primitiveRange = new CodeVariablePrimitiveRange(
                            // internal variable, this range is optional, added only
                            // for a more optimized output model
                            null,
                            null, null,
                            false);
                    subResult.variable.primitiveRange.min = thatRange.min;
                    subResult.variable.primitiveRange.max = thatRange.max;
                    assignment.result = subResult;
                    subCode.ops.add(assignment);
                    that = assignment.result;
                    thatRange = null;
                }
                primaryCode.add(subCode);
            } else if(t instanceof Variable) {
if(t.toString().contains("OUT"))
        t = t;
                t = replaceThrownOutVariable((Variable)t);
                if(pending == null)
                    pending = new Code();
                CodeVariable v = ((Variable)t).codeVariable;
                List<CodeVariable> dereferences =
                        thatIntoDereferences(that, pending, true);
                dereferences.add(v);
                while(pos + 1 < p.contents.size() - 1 &&
                        p.contents.get(pos + 1) instanceof Variable) {
                    p.contents.set(pos + 1, replaceThrownOutVariable(
                            (Variable)p.contents.get(pos + 1)));
                    ++pos;
                    dereferences.add(((Variable)p.contents.get(pos)).codeVariable);
                }
                while(
                        // does not fit into "that", shorten
                        dereferences.size() > 2 ||
                        // fits already, but there is a needless
                        // object part, remove it
                        (dereferences.size() == 2 && dereferences.get(1).context != Context.NON_STATIC)) {
                    CodeOpAssignment a = new CodeOpAssignment(
                            v.getStreamPos());
                    CodeVariable dO = dereferences.get(0);
                    CodeVariable dV = dereferences.get(1);
                    if(dO.context != Context.NON_STATIC)
                        pending.ops.clear();
                    if(dV.context != Context.NON_STATIC) {
                        pending.ops.clear();
                        dereferences.remove(0);
                        continue;
                    }
                    CodeFieldDereference d = new CodeFieldDereference(dO, dV);
                    // <code>.&lt;variable&gt;</code> can not have a code primitive range
                    // defined
                    a.rvalue = new RangedCodeValue(d);
                    a.result = new CodeFieldDereference(
                            // initialize bound
                            newBoundedLocal(d, false, pending, true));
                    // add the assignment with bounded rvalue
                    pending.add(a);
                    dereferences.remove(0);
                    dereferences.remove(0);
                    dereferences.add(0, ((CodeFieldDereference)a.result).
                            variable);
                }
                CodeFieldDereference d = dereferencesIntoThat(dereferences);
                that = d;
            }
        }
        if(pending != null)
            addWithSourcePos(e.getStreamPos(), primaryCode, pending);
        code.add(rangeCode);
        code.add(primaryCode);
        if(range != null) {
            if(thatRange != null)
                throw new RuntimeException("two ranges specified");
            thatRange = range;
        }
        if(!options.primaryRangeChecking)
            thatRange = null;
        code.result = new RangedCodeValue(that, thatRange);
        copyProperties(e, code);
        return code;
    }
    /*
    int peCount = 0;
    public Object visit_(PrimaryExpression e) throws CompilerException {
        ++peCount;
        if(peCount == 12)
            peCount = peCount;
        Code code = new Code();
        // latest fragment of code
        Code latest = null;
        ParsedPrimaryExpression v = e.value;
        Iterator<Typed> i = v.contents.iterator();
        // object to be possibly dereferenced object or null if none
        CodeVariable that = null;
        for(ParsedPrimaryExpression.ItemType t : v.types) {
            Typed c = i.next();
            switch(t) {
                case EXPRESSION:
                {
                    latest = (Code)((Expression)c).accept(this);
                    if(c instanceof CallExpression) {
                        CallExpression d = (CallExpression)c;
                        if(!d.method.flags.context) {
                            CodeOpCall call = (CodeOpCall)latest.ops.get(0);
                            // check for the latest result marker
                            CodeDereference thiS =
                                    (CodeDereference)call.getThisArg();
                            if(thiS.variable == LATEST_RESULT) {
                                // replace the marker with the latest result
                                call.args.set(0,
                                        new CodeDereference(null, that));
                            }
                        }
                    }
                    if(latest == null || latest.ops == null)
                        latest = latest;
                    code.add(latest);
                    if(latest.result instanceof CodeDereference) {
                        CodeDereference reference = (CodeDereference)
                                latest.result;
                        if(reference.object == null)
                            // can continue only if the variable is local
                            that = reference.variable;
                        else
                            that = null;
                    } else
                        that = null;
                    break;
                }
                
                case VARIABLE:
                {
                    latest = new Code();
                    VariableReference r = (VariableReference)c;
                    CodeDereference reference = newCodeVariableReference(r, that);
                    // no continuation after a variable
                    that = null;
                    latest.result = reference;
                    // no code related to a variable
                    break;
                }
                    
                case INDEX:
                {
                    CodeDereference indexed =
                            (CodeDereference)latest.result;
                    checkInitialized(e.getStreamPos(), indexed);
                    code.add((Code)((Expression)c).accept(this));
                    CodeOpIndex op = new CodeOpIndex(e.getStreamPos());
                    Type type = new Type(indexed.variable.type);
                    type.setDimension(type.getDimension() - 1); // !!!
                    type = ((Expression)c).getResultType();
                    op.indexed = indexed;
                    op.index = latest.result;
                    CodeDereference result = newLocal(type);
                    op.result = result;
                    latest = new Code();
                    latest.ops.add(op);
                    latest.result = result;
                    code.add(latest);
                    that = result.variable;
                    break;
                }
                    
                case DEREFERENCE:
                {
                    VariableReference r = (VariableReference)c;
                    CodeDereference reference =
                            newCodeVariableReference(r, that);
                    checkInitialized(e.getStreamPos(), reference);
                    CodeOpAssignment op = new CodeOpAssignment(
                            e.getStreamPos());
                    op.rvalue = reference;
                    CodeDereference result = newLocal(reference.variable.type);
                    op.setResult(result);
                    latest = new Code();
                    latest.ops.add(op);
                    latest.result = result;
                    code.add(latest);
                    that = result.variable;
                    break;
                }
            }
        }
        code.result = latest.result;
        code.setTags(e.tags);
        return code;
    }
     */
    @Override
    public Code visit(UnaryExpression e) throws CompilerException {
//if(e.toString().equals("-3"))
//    e = e;
        Code code = new Code();
        Code subCode = (Code)(e.sub.accept(this));
        // checkSourcesInitialized(e.sub.getStreamPos(), subCode.result);
        code.add(subCode);
        checkTypeReplacement(e.getResultType());
        // operator causes any variable primitive range on its argument to be lost
        // during the operation, so result has not such range defined
        AbstractCodeDereference result = newLocal(e.getResultType(),
                Variable.Category.INTERNAL_NOT_RANGE);
        switch(e.operatorType) {
            case PRE_INCREMENT:
            case PRE_DECREMENT:
            case POST_INCREMENT:
            case POST_DECREMENT:
            {
                BinaryExpression.Op binaryOperatorType;
                switch(e.operatorType) {
                    case PRE_INCREMENT:
                    case POST_INCREMENT:
                        binaryOperatorType = BinaryExpression.Op.PLUS;
                        break;
                        
                    case PRE_DECREMENT:
                    case POST_DECREMENT:
                        binaryOperatorType = BinaryExpression.Op.MINUS;
                        break;
                        
                    default:
                        throw new RuntimeException("unknown operator " +
                                e.operatorType.toString());
                        
                }
                AbstractCodeOp.Op operator = CodeOpBinaryExpression.
                        operatorTreeToCode(binaryOperatorType);
                CodeOpBinaryExpression be = new CodeOpBinaryExpression(
                        operator, e.getStreamPos());
                // guaranted to be dereference in semantic check
                RangedCodeValue rv = subCode.result;
                AbstractCodeDereference v = (AbstractCodeDereference)rv.value;
                // the operators modify the operand, so check if the
                // operand is not final
                checkFinalVariableSimple(e.getStreamPos(), v);
                be.left = rv;
                be.right = new RangedCodeValue(
                        new CodeConstant(null, new Literal(1)));
                be.result = v;
                CodeOpAssignment a = new CodeOpAssignment(e.getStreamPos());
                a.rvalue = rv;
                a.result = result;
                boolean pre;
                switch(e.operatorType) {
                    case PRE_INCREMENT:
                    case PRE_DECREMENT:
                        pre = true;
                        break;
                        
                    case POST_INCREMENT:
                    case POST_DECREMENT:
                        pre = false;
                        break;
                        
                    default:
                        throw new RuntimeException("unknown operator " +
                                e.operatorType.toString());
                        
                }
                if(pre) {
                    code.add(be);
                    code.add(a);
                } else {
                    code.add(a);
                    code.add(be);
                }
                break;
            }
            default:
            {
                AbstractCodeOp.Op operator = CodeOpUnaryExpression.
                        operatorTreeToCode(e.operatorType);
                CodeOpUnaryExpression ue = new CodeOpUnaryExpression(
                        operator, e.objectType, e.getStreamPos());
                ue.sub = subCode.result;
                // result is internal, no need to check if final
                ue.result = result;
                code.add(ue);
                break;
            }
        }
        // range computing left to static analysis of code
        code.result = new RangedCodeValue(result);
        code.resultDiscardable = true;
        copyProperties(e, code);
        return code;
    }
    @Override
    public Code visit(BinaryExpression e) throws CompilerException {
        Code code = new Code();
        Code leftCode = (Code)(e.left.accept(this));
        Code rightCode = (Code)(e.right.accept(this));
        // checkSourcesInitialized(e.left.getStreamPos(), leftCode.result);
        // checkSourcesInitialized(e.right.getStreamPos(), rightCode.result);
        checkTypeReplacement(e.getResultType());
        // operator causes any variable primitive range on its argument to be lost
        // during the operation, so result has not such range defined
        // !!!
        AbstractCodeDereference result = newLocal(e.getResultType(),
                Variable.Category.INTERNAL_NOT_RANGE);
        code.add(leftCode);
        boolean conditional = (e.operatorType ==
                BinaryExpression.Op.CONDITIONAL_AND) ||
                (e.operatorType ==
                BinaryExpression.Op.CONDITIONAL_OR);
        CodeLabel omitLabel;
        if(conditional) {
            omitLabel = newLabel();
            CodeOpBranch omitBranch = new CodeOpBranch(e.getStreamPos());
            omitBranch.condition = leftCode.result;
            CodeLabel l = newLabel();
            CodeConstant c = null;
            switch(e.operatorType) {
                case CONDITIONAL_AND:
                    omitBranch.labelRefTrue = l;
                    c = new CodeConstant(null, new Literal(false));
                    break;
                    
                case CONDITIONAL_OR:
                    omitBranch.labelRefFalse = l;
                    c = new CodeConstant(null, new Literal(true));
                    break;
                    
                default:
                    throw new RuntimeException("unknown operator type: " + e.operatorType);
            }
            code.add(omitBranch);
            CodeOpAssignment a = new CodeOpAssignment(e.getStreamPos());
            a.rvalue = new RangedCodeValue(c);
            // result is internal, no need to check if final
            a.result = result;
            code.add(a);
            CodeOpJump j = new CodeOpJump(e.getStreamPos());
            j.gotoLabelRef = omitLabel;
            code.add(j);
            AbstractCodeOp o = new CodeOpNone(e.getStreamPos());
            o.label = new CodeLabel(l);
            code.add(o);
        } else
            omitLabel = null;
        code.add(rightCode);
        if(conditional) {
            CodeOpAssignment a = new CodeOpAssignment(e.getStreamPos());
            a.rvalue = rightCode.result;
            // result is internal, no need to check if final
            a.result = result;
            code.add(a);
        } else {
            BinaryExpression.Op swappedOperator =
                    Generator.treeToCodeSwapOperator(e.operatorType);
            boolean swapOperands =
                    Generator.treeToCodeSwap(e.operatorType);
            AbstractCodeOp.Op operator = CodeOpBinaryExpression.operatorTreeToCode(
                swappedOperator);
            //AbstractCodeOp.Type type = AbstractCodeOp.getOpType(e.getResultType());
            CodeOpBinaryExpression op = new CodeOpBinaryExpression(
                    operator, e.getStreamPos());
            if(swapOperands) {
                op.left = rightCode.result;
                op.right = leftCode.result;
            } else {
                op.left = leftCode.result;
                op.right = rightCode.result;
            }
            // result is internal, no need to check if final
            op.result = result;
/*if(op.toString().indexOf("protocolBit !=") != -1)
    op = op;*/
            code.add(op);
        }
        if(conditional) {
            AbstractCodeOp labelHolder = new CodeOpNone(e.getStreamPos());
            labelHolder.label = new CodeLabel(omitLabel);
            code.add(labelHolder);
        }
        // range computing left to static analysis of code
        code.result = new RangedCodeValue(result);
        code.resultDiscardable = true;
        copyProperties(e, code);
        return code;
    }
    @Override
    public Code visit(AssignmentExpression e) throws CompilerException {
        CompilerException errors = new CompilerException();
        Code code = new Code();
        Code lCode = (Code)e.lvalue.accept(this);
        Code rCode = null;
        try {
            rCode = (Code)e.rvalue.accept(this);
        } catch(CompilerException f) {
            errors.addAllReports(f);
        }
        // checkSourcesInitialized(e.rvalue.getStreamPos(), rCode.result);
        // the left side is evaluated first
        code.add(lCode);
        if(rCode != null)
            code.add(rCode);
        if(lCode.result.range != null)
            throw new RuntimeException("unexpected range on lvalue");
        AbstractCodeDereference lvalue = (AbstractCodeDereference)lCode.result.value;
        if(rCode.resultDiscardable && rCode.ops.size() == 1) {
            AbstractResultCodeOp nested = (AbstractResultCodeOp)rCode.ops.iterator().next();
            nested.result = lvalue;
        } else {
            CodeOpAssignment op = new CodeOpAssignment(e.getStreamPos());
            // lvalue is guaranteed to be dereference in the semantic check
            if(!e.acceptFinal)
                try {
                    checkFinalVariableSimple(e.getStreamPos(), lvalue);
                } catch(CompilerException f) {
                    errors.addAllReports(f);
                }
            if(errors.reportsExist())
                throw errors;
            op.result = lvalue;
            op.rvalue = rCode.result;
            code.add(op);
        }
        // lvalue can not have a range defined
        code.result = new RangedCodeValue(lvalue);
        copyProperties(e, code);
        return code;
    }
    @Override
    public Code visit(CallExpression c) throws CompilerException {
        // calls inside the primary expression are postprocessed after
        // this method finishes, for docs about calls outside primary expressions
        // see <code>CallExpression.independentCallThis</code>
/*if(c.toString().indexOf("put") != -1)
    c = c;*/
//if(c.textRange != null)
//    c = c;
        Code code = new Code();
        CodeOpCall op = new CodeOpCall(c.getStreamPos());
        op.copyTextRange(c.textRange);
        AbstractCodeDereference result;
/*if(c.getResultType() == null)
    c = c;*/
        /// !!! check type replacements in generator and parsed primary
        checkTypeReplacement(c.getResultType());
        c.method = replaceThrownOutMethod(c.method);
        CodeMethodReference method = new CodeMethodReference(null,
                c.method);
        op.methodReference = method;
        if(c.independentCallThis != null) {
            // not a part of primary expression, "this" must
            // be set
            if(c.method.flags.context != Context.NON_STATIC)
                throw new RuntimeException("independent \"this\" in a " +
                        "static call");
            Code subCode = (Code)c.independentCallThis.accept(this);
            code.add(subCode);
            op.args.add(subCode.result);
        }
        for(AbstractExpression e: c.arg) {
            Code subCode = (Code)e.accept(this);
            code.add(subCode);
            op.args.add(subCode.result);
        }
        if(c.getResultType().isVoid())
            result = null;
        else {
            CodeFieldDereference returnValue = newLocal(c.getResultType(),
                    "retval_" + c.method.name, Variable.Category.INTERNAL_NOT_RANGE);
            result = returnValue;
/*if(method.firstPassMethod.range != null)
    method = method;*/
            // method.firstPassMethod.codeMethod.returnValue.primitiveRange
        }
        // result is internal, no need to check if final
        op.result = result;
        op.directNonStatic = c.directNonStatic;
        // it is guaranteed that the last operation in the
        // code generated by <code>CallExpression</code> is
        // CodeOpCall
        code.add(op);
        code.result = new RangedCodeValue(result);
        code.resultDiscardable = true;
        copyProperties(c, code);
        return code;
    }
    @Override
    public Code visit(IndexExpression c) throws CompilerException {
        // code generation of index expression is built into
        // <code>visit(PrimaryExpression)</code>
        throw new RuntimeException("visited IndexExpression");
    }
    @Override
    public Code visit(BlockExpression b) throws CompilerException {
        Code code = (Code)b.block.accept(this);
        CodeOpAssignment a =
                (CodeOpAssignment)code.ops.get(code.ops.size() - 1);
        code.result = new RangedCodeValue(a.result);
        copyProperties(b, code);
        return code;
    }
    @Override
    public Code visit(ReturnStatement s) throws CompilerException {
        Code code = new Code();
        // first the assignment...
        if(s.sub != null) {
            Code subCode = (Code)s.sub.accept(this);
            // checkSourcesInitialized(s.sub.getStreamPos(), subCode.result);
            code.add(subCode);
            CodeVariable returnVariable =
                    s.outerScope.lookupVariableAT(
                    new NameList(Method.LOCAL_RETURN),
                    PrimaryExpression.ScopeMode.OMIT_STATIC).codeVariable;
            CodeOpAssignment a = new CodeOpAssignment(
                    s.getStreamPos());
            a.rvalue = subCode.result;
            a.setResult(new CodeFieldDereference(returnVariable));
            code.add(a);
        }
        // ...then synchronization exits...
        int synchronizationCount =
                ((BlockScope)s.outerScope).getSynchronizationCount();
        // add the synchronization endings backwards
        Code syncExitCode = new Code();
        for(int i = 0; i < synchronizationCount; ++i) {
            CodeOpSynchronize syncExit = new CodeOpSynchronize(
                    s.getStreamPos());
            syncExit.begin = false;
            syncExitCode.ops.add(0, syncExit);
        }
        code.add(syncExitCode);
        if(options.enabledGeneratorTags &&
                currMethod.containsTag(CommentTag.TAG_GENERATE_TAIL_STRING)) {
            CodeOpNone tail = new CodeOpNone(s.getStreamPos());
            tail.annotations.add(CompilerAnnotation.TAIL_STRING);
            code.add(tail);
        }
        // ... and finally return from the method
        CodeOpReturn op = new CodeOpReturn(s.getStreamPos());
        if(currMethod.returnValue != null)
            op.returnValue = new CodeFieldDereference(currMethod.returnValue);
        code.add(op);
        copyProperties(s, code);
        return code;
    }
    @Override
    public Code visit(ThrowStatement s) throws CompilerException {
        Code code = new Code();
        Code subCode = (Code)s.sub.accept(this);
        code.add(subCode);
        CodeOpThrow t = new CodeOpThrow(
                s.getStreamPos());
        if(subCode.result.value instanceof CodeConstant) {
            CodeConstant c = (CodeConstant)subCode.result.value;
            if(c.value.isNull())
                t.thrown = null;
            else
                throw new RuntimeException("unexpected literal");
        } else
            t.thrown = (AbstractCodeDereference)subCode.result.value;
        code.add(t);
        copyProperties(s, code);
        return code;
    }
    @Override
    public Code visit(LabeledStatement s) throws CompilerException {
        Code code = (Code)s.sub.accept(this);
        if(code.ops.size() == 0)
            code.add(new CodeOpNone(s.getStreamPos()));
        else if(code.ops.get(0).label != null)
            // avoid label overwrite
            code.ops.add(0, new CodeOpNone(s.getStreamPos()));
        code.ops.get(0).label = new CodeLabel(s.label);
        copyProperties(s, code);
        return code;
    }
    @Override
    public Code visit(JumpStatement s) throws CompilerException {
        Code code = new Code();
        CodeOpJump op = new CodeOpJump(s.getStreamPos());
        op.gotoLabelRef = new CodeLabel(s.label);
        code.add(op);
        copyProperties(s, code);
        return code;
    }
    @Override
    public Code visit(BranchStatement s) throws CompilerException {
        Code code = new Code();
        Code conditionCode = (Code)s.condition.accept(this);
        code.add(conditionCode);
        // checkSourcesInitialized(s.condition.getStreamPos(), conditionCode.result);
        CodeOpBranch op = new CodeOpBranch(s.getStreamPos());
        op.condition = conditionCode.result;
        if(s.labelFalse != null)
            op.labelRefFalse = new CodeLabel(s.labelFalse);
        if(s.labelTrue != null)
            op.labelRefTrue = new CodeLabel(s.labelTrue);
        code.add(op);
        copyProperties(s, code);
        return code;
    }
    @Override
    public Code visit(SynchronizedStatement e) throws CompilerException {
        Code code = (Code)e.lock.accept(this);
        CodeOpSynchronize syncStart = new CodeOpSynchronize(
                e.getStreamPos());
        // code contains an expression containing the lock
        if(code.result.range != null)
            throw new RuntimeException("unexpected range definition on dereference");
        syncStart.lock = (AbstractCodeDereference)code.result.value;
        syncStart.begin = true;
        code.add(syncStart);
        // add the lock before the synchronized code is visited
        e.block.scope.synchronization = syncStart.lock;
        code.add((Code)e.block.accept(this));
        AbstractCodeOp lastOp = code.ops.get(code.ops.size() - 1);
        if(!(lastOp instanceof CodeOpReturn)) {
            // if the code does not end with a return statement,
            // that would end the synchronization,
            // it means that the code is still synchronized
            // therefore the following operation to stop the
            // synchronization
            CodeOpSynchronize syncStop = new CodeOpSynchronize(
                    lastOp.getStreamPos());
            syncStop.lock = syncStart.lock;
            syncStop.begin = false;
            code.add(syncStop);
        }
        copyProperties(e, code);
        return code;
    }
    @Override
    public Code visit(Block b) throws CompilerException {
        CompilerException errors = new CompilerException();
        Code code = new Code();
        int count = 0;
        boolean prevIsArgRange = false;
        for(AbstractStatement s : b.code) {
            currStatement.push(s);
            try {
                boolean isArgRange =
                        b == argRangeBlock &&
                        count < ((Method)b.scope.getBoundingMethodScope().owner).
                            argRangeCodeSize;
                Code c = (Code)s.accept(this);
                if(!prevIsArgRange && isArgRange)
                    currMethod.argRangeBeg = code.ops.size();
                if(prevIsArgRange && !isArgRange)
                    currMethod.argRangeSize = code.ops.size() - currMethod.argRangeBeg;
                code.add(c);
                prevIsArgRange = isArgRange;
            } catch(CompilerException f) {
                errors.addAllReports(f);
            }
            currStatement.pop();
            ++count;
        }
        if(prevIsArgRange)
            currMethod.argRangeSize = code.ops.size() - currMethod.argRangeBeg;
        if(errors.reportsExist())
            throw errors;
        copyProperties(b, code);
        return code;
    }
    /**
     * Checks for blank finals. Code methods must be generated and overridings
     * must be registered.<br>
     * 
     * If blank finals are not allowed, this method does nothing.
     */
    protected void checkBlankFinals() throws CompilerException {
        if(compilation.options.blankFinalsAllowed) {
            CompilerException errors = new CompilerException();
            for(CodeClass cc : codeCompilation.classes.values())
                for(CodeMethod cm : cc.listOfMethods)
                    try {
//        if(cm.signature.name.equals("*static/") &&
//            cm.owner.name.equals("#default.FinalSwitchLabelStatic"))
//        cm = cm;
                        checkAssignmentOfFinals(cm);
                    } catch(ParseException e) {
                        errors.addAllReports(e);
                    }
            if(errors.reportsExist())
                throw errors;
        }
    }
    /**
     * Sets <code>argRangeBlock</code> for a given method.
     * 
     * @param method method, whose block with argyument range operations
     * is searched for
     */
    protected void findArgRangeBlock(Method method) {
        if(method.argRangeCodeSize != 0) {
            argRangeBlock = method.code;
            AbstractStatement s = argRangeBlock.code.get(0);
            if(s instanceof SynchronizedStatement)
                argRangeBlock = ((SynchronizedStatement)s).block;
        } else
            argRangeBlock = null;
    }
    /**
     * Adss arguments prototypes for all arguments, which have
     * ranges.
     * 
     * @param cm code method
     */
    protected void addArgProtos(CodeMethod cm) {
        int count = 0;
        for(CodeVariable a : cm.args)
            if(a.primitiveRange != null) {
                CodeFieldDereference aProto = newLocal(
                                        new Type(a.getResultType()),
                                        CodeMethod.PROTO_PREFIX + a.name,
                                        Variable.Category.INTERNAL_NOT_RANGE);
                cm.argProtos.put(a, aProto.variable);
                CodeOpAssignment assign = new CodeOpAssignment(
                        cm.getStreamPos());
                assign.rvalue = new RangedCodeValue(new CodeFieldDereference(aProto));
                assign.result = new CodeFieldDereference(a);
                cm.code.ops.add(cm.argRangeBeg + cm.argRangeSize + count,
                        assign);
                ++count;
            }
    }
    @Override
    public Code visit(Compilation c) throws CompilerException {
        generateCodeObjects(c);
        CompilerException generatorErrors = new CompilerException();
        for(String packagE : c.packages.packageScopes.keySet()) {
            log(0, "compiling package `" + packagE + "'");
            PackageScope p = c.packages.packageScopes.get(packagE);
            for(String javaClass : p.javaClassScopes.keySet()) {
                JavaClassScope j = p.javaClassScopes.get(javaClass);
                log(1, "compiling class `" + javaClass + "'");
                AbstractJavaClass clazz = (AbstractJavaClass)j.owner;
                CodeClass cc = clazz.codeClass;
                if(j.getExtends() != null) {
                    if(verbose)
                        log(2, "extends " + j.getExtends().toString());
                }
                currClass = cc;
                List<MethodSignature> methodList =
                        new LinkedList<MethodSignature>(j.listOfMethods);
                for(MethodSignature key : methodList) {
                    Method method = j.methods.get(key);
                    String m = "";
                    if(method.flags.context != Context.NON_STATIC)
                        m += "static ";
                    if(method.flags.synchronizeD)
                        m += "synchronized ";
                    Code code = new Code();
                    
                    if(method.signature.name.indexOf("produce") != -1)
                        method = method;
//                    if(method.owner.name.indexOf("Periodic") != -1)
//                        method = method;
                    CodeMethod cm = new CodeMethod(cc, method);
                    /*
                    if(cc.toString().indexOf("Prism") != -1 &&
                            cm.signature.name.indexOf("getSize") != -1)
                        cm = cm;
                        */
                    for(CodeVariable cv : cm.variables.values()) {
/*if(method.signature.toString().indexOf("main") != -1)
    method = method;*/
                        // update types of locals
                        checkTypeReplacement(cv.type);
                    }
                    // update return type
                    checkTypeReplacement(method.getResultType());
                    // update signatures
                    method.refreshSignature();
                    cm.signature = method.signature;
                    method.codeMethod = cm;
                    if(!method.flags.isAbstract()) {
                        currMethod = cm;
                        if(options.enabledGeneratorTags &&
                                currMethod.containsTag(CommentTag.TAG_GENERATE_HEAD_STRING)) {
                            CodeOpNone head = new CodeOpNone(method.getStreamPos());
                            head.annotations.add(CompilerAnnotation.HEAD_STRING);
                            code.add(head);
                        }
                        code.ops.addAll(methodRangeCode(cm));
                        boolean codeHasErrors = false;
                        try {
/*if(method.toString().indexOf("run") != -1)
    method = method;*/
                            findArgRangeBlock(method);
                            code.add(visit(method.code));
                        } catch(CompilerException f) {
                            generatorErrors.addAllReports(f);
                            codeHasErrors = true;
                        }
                        if(!codeHasErrors) {
                            AbstractCodeOp lastOp;
                            if(code.ops.isEmpty())
                                lastOp = null;
                            else
                                lastOp = code.ops.get(code.ops.size() - 1);
                            if(lastOp == null ||
                                    !(lastOp instanceof CodeOpReturn)) {
                                // the instruction added after the latest one
                                // is guaranteed to be
                                // not synchronized, because it is after all
                                // synchronized blocks
                                StreamPos pos;
                                if(lastOp != null)
                                    pos = lastOp.getStreamPos();
                                else
                                    pos = method.getStreamPos();
                                if(options.enabledGeneratorTags &&
                                        currMethod.containsTag(CommentTag.TAG_GENERATE_TAIL_STRING)) {
                                    CodeOpNone tail = new CodeOpNone(pos);
                                    tail.annotations.add(CompilerAnnotation.TAIL_STRING);
                                    code.add(tail);
                                }
                                CodeOpReturn r = new CodeOpReturn(pos);
                                if(currMethod.returnValue != null)
                                    r.returnValue = new CodeFieldDereference(currMethod.returnValue);
                                // mark it as internal so that it is allowed
                                // to be a dead code
                                r.internal = true;
                                r.canBeDead = true;
                                code.ops.add(r);
                            }
if(code.ops.size() > 5)
     code = code;
                            currMethod.code = code;
                            addArgProtos(currMethod);
                            Optimizer.setLabelIndices(currMethod);
                            try {
                                if(currMethod.isConstructor())
                                    checkThisBeforeSuper(currMethod);
//if(currMethod.signature.name.contains("object") &&
//        currMethod.owner.name.contains("Pr"))
//    currMethod = currMethod;
                                TraceValues.checkUninitializedAndDead(currMethod);
                            } catch(CompilerException e) {
                                generatorErrors.addAllReports(e);
                            }
                            cm.code.appendTags(cc.tags);
                            cm.code.appendTags(cm.tags);
                            if(cm.signature.extractUnqualifiedNameString().equals(AbstractJavaClass.INIT_STATIC) ||
                                    cm.signature.extractUnqualifiedNameString().equals(AbstractJavaClass.INIT_OBJECT))
                                try {
                                    checkInitializationMethod(cm);
                                } catch(CompilerException e) {
                                    generatorErrors.addAllReports(e);
                                }
                        }
                        currMethod = null;
                    }
                    cc.methods.put(cm.signature, cm);
                    cc.listOfMethods.add(cm);
                    if(verbose) {
                        log(2, "compiled method `" + method.signature.name + "' " + m);
                        log(3, cm.toString());
                        log(3, "return type " + method.getResultType().toString());
                    }
                }
                currClass = null;
                codeCompilation.classes.put(cc.name, cc);
                Type type = new Type(new NameList(cc.name));
                cc.type = type;
                // codeCompilation.classesByType.put(type, cc);
            }
        }
        if(generatorErrors.reportsExist())
            throw generatorErrors;
        completeMethodReferences();
        completeClasses();
        completeOverridings();
        // code methods and overridings must be known to check blank finals
        checkBlankFinals();
        return null; 
    }
    /**
     * Compiles and returns the generated code. The code is not optimized.
     * 
     * @return                          output code of this generator
     */
    public CodeCompilation compile() throws CompilerException {
        currStatement = new Stack<>();
        compilation.accept(this);
        return codeCompilation;
    }
    void log(int indentLevel, String s) {
        if(verbose)
            CompilerUtils.log("G", indentLevel, s);
    }
}
