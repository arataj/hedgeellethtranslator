/*
 * GeneratorOptions.java
 *
 * Created on Aug 13, 2009, 12:08:54 PM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.code;

/**
 * Generator's options.
 *
 * @author Artur Rataj
 */
public class GeneratorOptions {
    /**
     * Enables tags that generate additional CodeOpNone, default is true.
     */
    public boolean enabledGeneratorTags = true;
    /**
     * If to generate primitive range code, default is true.
     */
    public boolean primaryRangeChecking = true;
}
