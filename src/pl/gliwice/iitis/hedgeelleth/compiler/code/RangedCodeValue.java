/*
 * RangedCodeValue.java
 *
 * Created on Aug 20, 2009, 2:12:42 PM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.code;

import pl.gliwice.iitis.hedgeelleth.compiler.primitiverange.CodePrimaryPrimitiveRange;

/**
 * A code value with an optional primitive range.
 *
 * @author Artur Rataj
 */
public class RangedCodeValue implements Cloneable {
    /**
     * A value.
     */
    public AbstractCodeValue value;
    /**
     * Primitive range for the value, null for none.
     */
    public CodePrimaryPrimitiveRange range;

    /**
     * Creates a new instance of <code>RangedCodeValue</code>.
     *
     * @param value                     value to be set
     * @param range                     primitive range, null for
     *                                  none
     */
    public RangedCodeValue(AbstractCodeValue value, CodePrimaryPrimitiveRange range) {
        this.value = value;
        this.range = range;
    }
    /**
     * Creates a new instance of <code>RangedCodeValue</code>,
     * with null primitive range.<br>
     *
     * This is a convenience constructor.
     *
     * @param value                     value to be set
     */
    public RangedCodeValue(AbstractCodeValue value) {
        this(value, null);
    }
    /*
     * Creates a new instance of <code>RangedCodeValue</code>,
     * with the value being a constant, and automatically sets
     * the primitive range. The range is null for non--primitive
     * and (c, c) for a primitive value.
     *
     * @param value                     value to be set
     */
    /*
    public RangedCodeValue(CodeConstant c) {
        this(c, null);
        if(c.getResultType().isPrimitive())
            range = new CodePrimitiveRange(
                    new CodeConstant(c.getStreamPos(), c.value),
                    new CodeConstant(c.getStreamPos(), c.value));
        else
            range = null;
    }
     */
    /**
     * Creates a new instance of <code>RangedCodeValue</code>.<br>
     *
     * This is a copying constructor. Also a new copy of a possible
     * dereference in the original ranged value is created.
     *
     * @param rv                        original range code value
     */
    public RangedCodeValue(RangedCodeValue rv) {
        this(rv.value.clone(), rv.range != null ? rv.range.clone() :  null);
    }
    /**
     * If this value contains a source level primitive range, either
     * a restrictive variable one, or primary one.
     *
     * For details on restrictive source ranges, see
     * <code>CodeVariable.hasRestrictiveSourceLevelRange()</code>.
     * 
     * @return                          if contains a source level
     *                                  primitive range
     */
    public boolean hasRestrictiveSourceLevelRange() {
        if(range != null)
            // primary range on index, primary ranges are always source--level
            return true;
        return value.hasRestrictiveSourceLevelRange();
    }
    @Override
    public RangedCodeValue clone() {
        Object o;
        try {
            o = super.clone();
        } catch(CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
        RangedCodeValue rv = (RangedCodeValue)o;
        rv.value = value.clone();
        if(range != null)
            rv.range = range.clone();
        return rv;
    }
    public String toNameString() {
        String s;
        if(range != null)
            s = range.toNameString();
        else
            s = "";
        s += value.toNameString();
        return s;
    }
    @Override
    public String toString() {
        String s;
        if(range != null)
            s = range.toString();
        else
            s = "";
        s += value.toString();
        return s;
    }
}
