/*
 * AbstractCodeDereference.java
 *
 * Created on Jan 12, 2009
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.code.dereference;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.code.*;
import pl.gliwice.iitis.hedgeelleth.compiler.primitiverange.CodeVariablePrimitiveRange;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Context;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Type;

/**
 * A code dereference class, that handles either field dereference or indexing.
 * In the former case, can also represent a local or static variable.<br>
 *
 * See <code>CodeFieldDereference</code>, <code>CodeIndexDereference</code>
 * and the field <code>object</code> for details.
 *
 * This class and its subclasses can have more instances per the same
 * pair of values. Thus, <code>equals()</code> is overriden to reflect
 * only dereferenced and dereferencing items, and not the class instances
 * theirselves.
 *
 * @author Artur Rataj
 */
public abstract class AbstractCodeDereference extends AbstractCodeValue {
    /**
     * A dereferenced object.<br>
     *
     * For <code>CodeFieldDereference</code>, object that owns some referenced
     * field or null for a local or a static.<br>
     *
     * For <code>CodeIndexDereference</code>, an array object.<br>
     */
    public CodeVariable object;

    /**
     * Creates a new instance of AbstractCodeDereference. 
     */
    public AbstractCodeDereference(CodeVariable object) {
        super(null);
        this.object = object;
    }
    /**
     * If the resulting variable is volatile, that is, if it
     * can be changed by another thread or by a called method.
     * Non--local variables and, in the case of index dereferences,
     * array contents are treated as volatile.
     * It is sometimes a safe simplification, as for example if the
     * dereferenced object is not passed to any other thread or to
     * any called method, the resulting field or array element
     * is not really volatile, but reported by this method as
     * volatile.<br>
     *
     * Thus, the one non--volatile object is a field dereference with
     * null object part and variable part being a local.
     *
     * This method is to be typically used only by the optimizer, so
     * that it knows that no code outside the analysed method changes
     * some field/array element mid--time.
     *
     * @return
     */
    abstract public boolean isVolatile();
    /**
     * Returns the container variable. In the case of a field dereference, it is
     * <code>object</code>. In the case of an index dereference, null is returned,
     * as <code>object</code> is either a local or a static variable, so it
     * does not have a container variable.
     *
     * @return
     */
    abstract public CodeVariable getContainerVariable();
    /**
     * If the dereferencing value is a variable, it is returned. Otherwise,
     * that is, if it is an indexing constant, null is returned. An indexing
     * variable is also a dereferencing variable.
     * 
     * @return                          a dereferencing variable or null if
     *                                  none
     */
    abstract public CodeVariable getDereferencingVariable();
    /**
     * Returns the variable within this dereference, that 
     * defines the result type. In the case of a field dereference, it is
     * <code>variable</code>, and <code>object</code> represents the container
     * In the case of an index dereference, the type source variable is
     * <code>object</code>, which is some array variable, and
     * <code>variable</code> is just the index.<br>
     *
     * The type source variable aslo is the one that contains a possible
     * variable primitive range.
     *
     * @return                          a contained variable, never null
     */
    abstract public CodeVariable getTypeSourceVariable();
    /**
     * Creates a new field dereference, being
     * (object = getContainerVariable(), variable = getTypeSourceVariable()).
     * So, in the case of a field dereference, this method returns an
     * equivalent field dereference, and in the case of an index dereference,
     * this method returns the dereference that represents the indexed
     * variable.
     *
     * @return                          a field dereference
     */
    public CodeFieldDereference extractTypeSourceFieldDereference() {
        return new CodeFieldDereference(getContainerVariable(), getTypeSourceVariable());
    }
    /**
     * Returns all variables in this dereference wrapped in
     * <code>CodeFieldDereference</code>
     * and also returns this dereference. No repeats exist in the output
     * collection.
     *
     * @return                          a set of all involved variables
     *                                  and dereferences
     */
    /*
    public Set<AbstractCodeDereference> getAllUsed() {
        Set<AbstractCodeDereference> list =
                new HashSet<AbstractCodeDereference>();
        if(object != null)
            list.add(new CodeFieldDereference(object));
        CodeVariable variable = getDereferencingVariable();
        if(variable != null)
            list.add(new CodeFieldDereference(variable));
        list.add(this);
        return list;
    }
     */
    @Override
    public void setResultType(Type resultType) {
        throw new RuntimeException("can not set type of a dereference");
    }
    /*
     * Returns a <code>field</code> part in the case of a field dereference, or,
     * in the case of an indexing dereference, a fake "array contents" variable,
     * that represents the contents of the whole array. The fake variable
     * is extracted from the array itself.<br>
     *
     * This method is called by the optimizer, when it determines source or target
     * variables within an operation. The optimizer simplifies optimization of arrays
     * by treating all array elements as one variable, being kind--of a field
     * within the array. That exact variable is the "fake contents" variable.
     *
     * @return                          a contents variable, that can be read or
     *                                  written to, depending on the position in
     *                                  an operation
     */
    /*
    abstract CodeVariable getContentsVariable();
     */
    /**
     * Extracts the source parts of this dereference that is a target,
     * that is, it is not read but written to. The dereference
     * as a whole is never returned.<br>
     *
     * Namely: (1) all not--null object parts of the field and
     * index references, (2) all variable index parts of the
     * index references.<br>
     *
     * As the returned dereferences are only wrappers, it is guaranteed
     * that their object parts are null.
     *
     * @return                          a list of source local variables wrapped
     *                                  into dereferences, empty list is returned
     *                                  if no variables were found
     */
    public Collection<CodeVariable> extractLocalSourcePartsInTarget() {
        Collection<CodeVariable> list =
                new LinkedList<>();
        if(object != null && object.context == Context.NON_STATIC)
            list.add(object);
        if(this instanceof CodeFieldDereference) {
           CodeFieldDereference f = (CodeFieldDereference)this;
           // do nothing
        } else if(this instanceof CodeIndexDereference) {
           CodeIndexDereference i = (CodeIndexDereference)this;
           CodeVariable v = i.getDereferencingVariable();
           if(v != null && v.context == Context.NON_STATIC)
               list.add(v);
        }
        return list;
    }
    /**
     * Extracts the source parts of this dereference that is a target,
     * that is, it is not read but written to. The dereference
     * as a whole is never returned.<br>
     *
     * Namely: (1) all not--null object parts of the field and
     * index references, (2) all variable index parts of the
     * index references.<br>
     *
     * @return                          a list of source field variables,
     *                                  static variables are wrapped
     *                                  into dereferences, empty list is
     *                                  returned if no variables were found
     */
    public Collection<CodeFieldDereference> extractFieldSourcePartsInTarget() {
        Collection<CodeFieldDereference> out = new LinkedList<>();
        if(object != null) {
            if(object.context != Context.NON_STATIC)
                out.add(new CodeFieldDereference(null, object));
        }
        if(this instanceof CodeFieldDereference) {
            CodeFieldDereference f = (CodeFieldDereference)this;
            // do nothing, a variable is not read in a target
        } else {
            // index dereference, search for a static field index
            CodeIndexDereference i = (CodeIndexDereference)this;
            CodeVariable dereferencing = i.getDereferencingVariable();
            if(dereferencing != null && dereferencing.context != Context.NON_STATIC)
                out.add(new CodeFieldDereference(null, i.getDereferencingVariable()));
        }
        return out;
    }
    /**
     * If this dereference is a plain local, it is returned in the list.
     * Otherwise, an empty list is returned.
     *
     * @return                          a list that contains a single
     *                                  local or is empty
     */
    public Collection<CodeVariable> extractPlainLocal() {
        Collection<CodeVariable> out = new LinkedList<CodeVariable>();
        if(this instanceof CodeFieldDereference) {
            CodeFieldDereference f = (CodeFieldDereference)this;
            if(f.isLocal())
                out.add(f.variable);
        }
        return out;
    }
    /**
     * If this dereference is a plain field, it is returned in the list.
     * Otherwise, an empty list is returned. A plain field must
     * be <code>CodeFieldDereference</code> with non--null object
     * or static variable.
     *
     * @return                          a list that contains a single
     *                                  local or is empty
     */
    public Collection<CodeFieldDereference> extractPlainField() {
        Collection<CodeFieldDereference> out = new LinkedList<CodeFieldDereference>();
        if(this instanceof CodeFieldDereference) {
            CodeFieldDereference f = (CodeFieldDereference)this;
            if(!f.isLocal())
                out.add(f);
        }
        return out;
    }
    /**
     * Extracts the local source parts of some dereference that is a source,
     * that is, it is read but not written to.<br>
     *
     * Namely: (1) all what <code>extractLocalSourcePartsInTarget</code>
     * returns, (2) all what <code>extractPlainLocal()</code> returns.<br>
     *
     * As the returned dereferences are only wrappers, it is guaranteed
     * that their object parts are null.
     *
     * @return                          a list of source variables, empty
     *                                  if no variables were found
     */
    public Collection<CodeVariable> extractLocalSourcePartsInSource() {
        Collection<CodeVariable> s = extractLocalSourcePartsInTarget();
        s.addAll(extractPlainLocal());
        return s;
    }
    /**
     * Extracts the field source parts of some dereference that is a source,
     * that is, it is read but not written to.<br>
     *
     * Namely: (1) all what <code>extractFieldSourcePartsInTarget</code>
     * returns, (2) all what <code>extractPlainField</code> returns.<br>
     *
     * Static fields are wrapped into dereferences.
     *
     * @return                          a list of source variables, empty
     *                                  if no variables were found
     */
    public Collection<CodeFieldDereference> extractFieldSourcePartsInSource() {
        Collection<CodeFieldDereference> out = extractFieldSourcePartsInTarget();
        out.addAll(extractPlainField());
        return out;
    }
    @Override
    public boolean hasRestrictiveSourceLevelRange() {
        CodeVariable typeSourceVariable = getTypeSourceVariable();
        if(this instanceof CodeIndexDereference) {
            CodeIndexDereference i = (CodeIndexDereference)this;
            // test index's ranges as well
            if(i.index.hasRestrictiveSourceLevelRange())
                return true;
        }
        // ranges of internal variables are not specified by the user,
        // thus can be optimized out
        if(typeSourceVariable.hasRestrictiveSourceLevelRange())
            return true;
        return false;
    }
    /**
     * Returns a primitive range associated with the result type of this
     * dereference, that is, the one returned by <code>getTypeSourceVariable()</code>.
     *
     * @return                          primitive range, null for none
     */
    public CodeVariablePrimitiveRange getTypeVariablePrimitiveRange() {
        return getTypeSourceVariable().primitiveRange;
    }
    @Override
    public boolean equals(Object o) {
        if(o instanceof AbstractCodeDereference) {
            AbstractCodeDereference r = (AbstractCodeDereference)o;
            return object == r.object;
        } else
            return false;
    }
    @Override
    public int hashCode() {
        return 41 * 3 + (this.object != null ? this.object.hashCode() : 0);
    }
    /**
     * Clones this dereference, but references of the type <code>CodeVariable</code>
     * still point to the original objects.
     * 
     * @return                          a copy of this dereference
     */
    @Override
    public AbstractCodeDereference clone() {
       Object o = super.clone();
        AbstractCodeDereference out = (AbstractCodeDereference)o;
        return out;
    }
}
