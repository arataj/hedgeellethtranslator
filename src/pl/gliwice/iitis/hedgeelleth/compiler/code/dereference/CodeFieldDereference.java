/*
 * CodeField.java
 *
 * Created on Apr 12, 2008
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.code.dereference;

import pl.gliwice.iitis.hedgeelleth.compiler.code.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Context;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Type;

/**
 * A local variable or a static field or a field referenced from an object
 * or an index referenced from an array object.<br>
 *
 * To test if two references refer the same variable, use the
 * method equals().
 * 
 * @author Artur Rataj
 */
public class CodeFieldDereference extends AbstractCodeDereference {
    /**
     * Referenced variable.
     */
    public CodeVariable variable;
    
    /**
     * Creates a new instance of CodeDereference.
     * 
     * @param object                    if the variable is a field,
     *                                  then contains the object that
     *                                  owns the field, if the variable
     *                                  is a local or static then null
     * @param variable                  referenced variable
     */
    public CodeFieldDereference(CodeVariable object,
            CodeVariable variable) {
        super(object);
        this.variable = variable;
        if(this.variable == null)
            throw new RuntimeException("null variable");
    }
    /**
     * Creates a new instance of CodeFieldDereference.
     * 
     * @param variable                  referenced variable
     */
    public CodeFieldDereference(CodeVariable variable) {
        this(null, variable);
    }
    /**
     * Creates a new instance of CodeFieldDereference. This
     * is a copying constructor, the copy is shallow.
     * 
     * @param variable                  code dereference
     */
    public CodeFieldDereference(CodeFieldDereference dereference) {
        this(dereference.object, dereference.variable);
    }
    @Override
    public CodeVariable getContainerVariable() {
        return object;
    }
    @Override
    public CodeVariable getDereferencingVariable() {
        return variable;
    }
    @Override
    public CodeVariable getTypeSourceVariable() {
        return variable;
    }
    /*
    @Override
    CodeVariable getContentsVariable() {
        return variable;
    }
     */
    /**
     * If this is a dereference to a local variable.
     * 
     * @return                          if it is a local dereference
     */
    public boolean isLocal() {
        // if no dereference and not static, then it is local
        return object == null && variable.context == Context.NON_STATIC;
    }
    @Override
    public boolean isVolatile() {
        return !isLocal();
    }
    @Override
    public Type getResultType() {
        return variable.getResultType();
    }
    @Override
    public boolean equals(Object o) {
        if(o instanceof CodeFieldDereference) {
            CodeFieldDereference r = (CodeFieldDereference)o;
            return super.equals(o) && variable == r.variable;
        } else
            return false;
    }
    @Override
    public int hashCode() {
        int hash = 41 * 3 + super.hashCode();
        hash = 41 * hash + /*(this.variable != null ? */this.variable.hashCode()/* : 0)*/;
        return hash;
    }
    @Override
    public AbstractCodeDereference clone() {
        Object o = super.clone();
        AbstractCodeDereference out = (AbstractCodeDereference)o;
        return out;
    }
    @Override
    public String toNameString() {
        String s;
        if(object != null)
            s = object.toNameString() + "::";
        else
            s = "";
if(variable != null)
        s += variable.toNameString();
        return s;
    }
    @Override
    public String toString() {
        String s;
        if(object != null)
            s = object.toString() + "::";
        else
            s = "";
if(variable != null)
        s += variable.toString();
        return s;
    }
}
