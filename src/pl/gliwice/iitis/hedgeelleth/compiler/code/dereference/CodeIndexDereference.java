/*
 * CodeIndexDereference.java
 *
 * Created on Jan 12, 2009
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.code.dereference;

import pl.gliwice.iitis.hedgeelleth.compiler.code.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Type;

/**
 * This class handles indexing, as opposed to <code>Dereference</code>.
 * Thus, <code>IndexExpression</code> is transformed to an object of
 * this class.<br>
 *
 * Note that any code variables involved in this type of dereference
 * must be either locals or static fields.
 *
 * @author Artur Rataj
 */
public class CodeIndexDereference extends AbstractCodeDereference {
    /**
     * Indexing variable and a possible primitive range, see the
     * constructor for details.
     */
    public RangedCodeValue index;

    /**
     * Creates a new instance of CodeIndexDereference.<br>
     *
     * The ranged indexing value's index is the indexing variable
     * wrapped in <code>CodeFieldDereference</code> or a constant
     * being <code>CodeConstant</code>, that wrapping field
     * dereference, though, can not have a non--null object part.
     *
     * @param object                    contains an array object
     * @param index                     a ranged indexing value
     */
    public CodeIndexDereference(CodeVariable object,
            RangedCodeValue index) {
        super(object);
        if(index.value instanceof CodeFieldDereference) {
            CodeFieldDereference a = (CodeFieldDereference)index.value;
            if(a.object != null)
                throw new RuntimeException("index wrapper contains a " +
                        "non--null object");
        } else if(index.value instanceof CodeConstant) {
            // empty
        } else
            throw new RuntimeException("invalid index");
        this.index = index;
    }
    /**
     * Creates a new instance of CodeIndexDereference. This
     * is a copying constructor, the copy is shallow.
     *
     * @param variable                  code dereference
     */
    public CodeIndexDereference(CodeIndexDereference dereference) {
        this(dereference.object, dereference.index);
    }
    @Override
    public boolean isVolatile() {
        return true;
    }
    @Override
    public CodeVariable getContainerVariable() {
        return null;
    }
    @Override
    public CodeVariable getDereferencingVariable() {
        if(index.value instanceof CodeFieldDereference) {
            CodeFieldDereference f = (CodeFieldDereference)index.value;
            if(f.object != null)
                throw new RuntimeException("index wrapper contains a " +
                        "non--null object");
            return f.variable;
        } else
            return null;
    }
    @Override
    public CodeVariable getTypeSourceVariable() {
        return object;
    }
    /**
     * Returns the array element type. It is the object type, but of 
     * dimension decreased by 1.
     * 
     * @return                          type of the element of the indexed
     *                                  array
     */
    public Type getElementType() {
        Type type = new Type(object.getResultType());
        type.decreaseDimension(null);
        return type;
    }
    @Override
    public Type getResultType() {
        return getElementType();
    }
    /*
    @Override
    CodeVariable getContentsVariable() {
        return object.fakeArrayContents;
    }
     */
    @Override
    public boolean equals(Object o) {
        if(o instanceof CodeIndexDereference) {
            CodeIndexDereference r = (CodeIndexDereference)o;
            return super.equals(o) && index == r.index;
        } else
            return false;
    }
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 41 * hash + super.hashCode();
        hash = 41 * hash + (this.index != null ? this.index.hashCode() : 0);
        return hash;
    }
    @Override
    public AbstractCodeDereference clone() {
        Object o = super.clone();
        CodeIndexDereference out = (CodeIndexDereference)o;
        out.index = index.clone();
        return out;
    }
    @Override
    public String toNameString() {
        String s;
        s = object.toNameString() + "[" + index.toNameString() + "]";
        return s;
    }
    @Override
    public String toString() {
        String s;
        s = object.toString() + "[" + index.toString() + "]";
        return s;
    }
}
