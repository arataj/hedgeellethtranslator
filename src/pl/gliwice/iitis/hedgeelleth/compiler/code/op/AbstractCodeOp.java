/*
 * AbstractCodeOp.java
 *
 * Created on Feb 28, 2008, 11:48:10 AM
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.code.op;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.CommentTag;
import pl.gliwice.iitis.hedgeelleth.compiler.code.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Context;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Typed;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.AbstractExpression;

/**
 * An abstract operation.<br>
 *
 * A note about the get/replace source/target: search and replacement of
 * locals is used by the regular optimizer, search and replacement of
 * fields is used only in a special TADD--related optimization, this is
 * why replacemenet of target fields is not implement as it is not required
 * by these special optimizations.
 * 
 * @author Artur Rataj
 */
public abstract class AbstractCodeOp implements SourcePosition, Cloneable {
    /**
     * <code>NESTED_INDEX</code> is to be used exclusively by
     * <code>PTANestedExpression</code>.
     */
    public enum Op {
        NONE,
        UNARY_NEGATION,
        UNARY_CONDITIONAL_NEGATION,
        UNARY_BITWISE_COMPLEMENT,
        UNARY_CAST,
        UNARY_INSTANCE_OF,
        UNARY_INSTANCE_OF_NOT_NULL,
        UNARY_ABS,
        BINARY_PLUS,
        BINARY_MINUS,
        BINARY_MULTIPLY,
        BINARY_DIVIDE,
        BINARY_MODULUS,
        BINARY_MODULUS_POS,
        BINARY_INCLUSIVE_OR,
        BINARY_EXCLUSIVE_OR,
        BINARY_AND,
        BINARY_EQUAL,
        BINARY_INEQUAL,
        BINARY_LESS,
        BINARY_LESS_OR_EQUAL,
        BINARY_SHIFT_LEFT,
        BINARY_SHIFT_RIGHT,
        BINARY_UNSIGNED_SHIFT_RIGHT,
        JUMP,
        RETURN,
        SYNCHRONIZE,
        ASSIGNMENT,
        BRANCH,
        CALL,
        ALLOCATION,
        THROW,
        NESTED_INDEX;

        /**
         * If this is a unary operator. <code>NONE</code> is deemed a unary
         * operator by this method.
         * 
         * @return if unary
         */
        public boolean isUnary() {
            switch(this) {
                case NONE:
                case UNARY_NEGATION:
                case UNARY_CONDITIONAL_NEGATION:
                case UNARY_BITWISE_COMPLEMENT:
                case UNARY_CAST:
                case UNARY_INSTANCE_OF:
                case UNARY_INSTANCE_OF_NOT_NULL:
                case UNARY_ABS:
                    return true;
                    
                default:
                    return false;
            }
        }
        /**
         * If this is a binary operator. <code>NESTED_INDEX</code> is deemed a binary
         * operator by this method.
         * 
         * @return if binary
         */
        public boolean isBinary() {
            switch(this) {
                case BINARY_PLUS:
                case BINARY_MINUS:
                case BINARY_MULTIPLY:
                case BINARY_DIVIDE:
                case BINARY_MODULUS:
                case BINARY_INCLUSIVE_OR:
                case BINARY_EXCLUSIVE_OR:
                case BINARY_AND:
                case BINARY_EQUAL:
                case BINARY_INEQUAL:
                case BINARY_LESS:
                case BINARY_LESS_OR_EQUAL:
                case BINARY_SHIFT_LEFT:
                case BINARY_SHIFT_RIGHT:
                case BINARY_UNSIGNED_SHIFT_RIGHT:
                case NESTED_INDEX:
                    return true;
                    
                default:
                    return false;
            }
        }
    };
    
    /*
     * Arithmetic operations have TYPE_INT, TYPE_LONG, TYPE_FLOAT, TYPE_DOUBLE,
     * BINARY_PLUS has additionally TYPE_STRING. Conditional operations have
     * TYPE_BOOLEAN. NONE, JUMP, BINARY_INDEX, ASSIGNMENT,
     * BRANCH, CALL, ALLOCATION and SYNCHRONIZE
     * can have only TYPE_MIXED.
     */
    /*
    public enum Type {
        TYPE_INT,
        TYPE_LONG,
        TYPE_FLOAT,
        TYPE_DOUBLE,
        TYPE_BOOLEAN,
        TYPE_STRING,
        TYPE_MIXED,
    };
     */
    /**
     * Operation.
     */
    public Op op;
    /*
     * Operation type.
     */
    /* public Type type; */
    /**
     * Tags, empty list for no tags.
     */
    List<CommentTag> tags;
    /**
     * Label of this operation, null if none.
     */
    public CodeLabel label;
    /**
     * Position in the parsed stream, null for none.
     */
    protected StreamPos pos;
    /**
     * If this operation can be dead. Usually true for some operations
     * that are implicit in the source, like loop jumps.<br>
     *
     * The default is false.
     */
    public boolean canBeDead;
    /**
     * A text range of this operation, null for unspecified.
     */
    public TextRange textRange;
    
    /**
     * Creates a new instance of AbstractCodeOp, with an empty list of tags and
     * without a label.
     *
     * @param op                        operation
     * //param type                      operation type
     * @param pos                       position in the parsed stream
     */
    public AbstractCodeOp(Op op, /*Type type, */StreamPos pos) {
        this.op = op;
        // this.type = type;
        tags = new LinkedList<>();
        label = null;
        this.pos = pos;
    }
    /**
     * Copies a text range.
     * 
     * @param c text range to deep copy, can be null
     */
    public void copyTextRange(TextRange c) {
        if(c == null)
            textRange = null;
        else
            textRange = new TextRange(c);
    }
    /**
     * Returns all references to labels in this operation.
     * 
     * A possible label of the operation itself is not
     * a label reference, so it is not included.
     * 
     * This implementation returns an empty set.
     * 
     * @return                          collection of references
     *                                  to labels, empty if there are no
     *                                  references
     */
    public Set<CodeLabel> getLabelRefs() {
        return new HashSet<>();
    }
    /*
     * Gets a list of locals from <code>getLocalTargets()</code>,
     * merges them with a list given as an argument and returns the
     * resulting list.
     * 
     * @param sourceReferences          source references of this
     *                                  operation, added to the
     *                                  list returned by this method
     *                                  and used to avoid repetitions
     *                                  in the list
     * @return                          collection of references to
     *                                  add to the output list, empty if
     *                                  if there are no such variables
     */
    /*
    protected Collection<AbstractCodeDereference> addLocalSourcesInTargets(
            List<AbstractCodeDereference> sourceReferences) {
         HashSet<AbstractCodeDereference> list =
                 new HashSet<AbstractCodeDereference>(sourceReferences);
         Collection<AbstractCodeDereference> target =
                 getTargets();
         for(AbstractCodeDereference r : target)
            list.addAll(r.extractLocalSourcePartsInTarget());
         return list;
    }
     */
    /**
     * If a given value is a plain local variable, then the variable
     * is returned. Otherwise, null is returned.<br>
     *
     * This method uses <code>AbstractCodeDereference.extractPlainLocal</code>.
     *
     * @param value                     value to test
     * @return                          a local variable or null
     */
    protected CodeVariable extractPlainLocal(AbstractCodeValue value) {
        CodeVariable out = null;
        if(value instanceof AbstractCodeDereference) {
            Collection<CodeVariable> list = ((AbstractCodeDereference)value).
                    extractPlainLocal();
            if(!list.isEmpty())
                out = list.iterator().next();
        }
        return out;
    }
    /**
     * If a given value is a plain field variable, then the variable
     * is returned. Otherwise, null is returned.<br>
     *
     * Index dereferences are never plain fields.<br>
     *
     * This method uses <code>AbstractCodeDereference.extractPlainField</code>.
     *
     * @param value                     value to test
     * @return                          a field variable or null
     */
    protected CodeFieldDereference extractPlainField(AbstractCodeValue value) {
        CodeFieldDereference out = null;
        if(value instanceof AbstractCodeDereference) {
            Collection<CodeFieldDereference> list = ((AbstractCodeDereference)value).
                    extractPlainField();
            if(!list.isEmpty())
                out = list.iterator().next();
        }
        return out;
    }
    /**
     * Returns all ranged code values within the operation.<br>
     *
     * This implementation returns an empty list.
     *
     * @return                          all <code>RangedCodeValue</code>
     *                                  objects in this op
     */
    public Set<RangedCodeValue> getRangedValues() {
        return new HashSet<>();
    }
    /**
     * Returns index dereferences within the operation, either all
     * or only reads.<br>
     *
     * This implementation returns an empty list.
     *
     * @param alsoWrite                 true if to include reads and writes of arrays;
     *                                  false if to include only reads from arrays
     * @return                          all index dereferences
     */
    public Set<CodeIndexDereference> getIndexDereferences(boolean alsoWrite) {
        return new HashSet<>();
    }
    /**
     * Returns all index dereferences within the operation. This is a
     * convenience method.
     * @return                          all index dereferences
     */
    public Set<CodeIndexDereference> getIndexDereferences() {
        return getIndexDereferences(true);
    }
    /**
     * Replaces an index dereference with a constant.<br>
     *
     * This implementation does nothing, returns false.
     *
     * @param i dereference to replace
     * @param c replacement constant
     * @return if anything has been replaced
     */
    public boolean replaceIndexDereference(
            CodeIndexDereference i, CodeConstant c) {
        return false;
    }
    /**
     * Returns all indexed variables within the operation.<br>
     *
     * This method calls <code>getIndexDereferences()</code>
     * and extracts the indexed variables from the list.
     *
     * @return                          all indexed variables, either
     *                                  local or static
     */
    public Set<CodeVariable> getIndexed() {
        Set<CodeVariable> out = new HashSet<>();
        for(CodeIndexDereference i : getIndexDereferences())
            out.add(i.object);
        return out;
    }
    /**
     * This is a helper method to be used by <code>getLocalSources()</code>
     * to add variables read in primitive ranges, both variable and primary
     * ones.<br>
     *
     * This method scans all variables given in <code>nonBoundSources</code>
     * and also all variables from a call to <code>getLocalTargets</code>
     * for variables read within variable primitive ranges. Then, returns a merge
     * of <code>nonBoundSources</code> with the variables this method has
     * found in the variable primitive ranges, and also in any primary primitive
     * ranges found in <code>getRangedValues</code><br>
     *
     * Typical usage is <code>return extractAllLocalBoundSources(list);</code> as
     * the last statement of <code>getLocalSources</code> within each
     * <i>non--abstract</i> operation, to avoid repetition of calls
     * from within both abstract and non--abstract types.<br>
     *
     * Note that, as <code>nonBoundSources</code> constains only locals, all
     * possible primitive range variables found are also locals.
     *
     * @param nonBoundSources           local variables read in this operation, but
     *                                  not from within the primitive bounds
     * @return                          all variables read in all primitive
     *                                  bounds of this operation
     */
    protected Set<CodeVariable> extractAllLocalBoundSources(Collection<CodeVariable>
            nonBoundSources) {
        Set<CodeVariable> sources = new HashSet<>(
                nonBoundSources);
        Set<CodeVariable> all = new HashSet<>(
                nonBoundSources);
        all.addAll(getLocalTargets());
        for(CodeVariable cv : all) {
            if(cv.hasPrimitiveRange(false)) {
                if(cv.getResultType().isArray(null) &&
                        !getIndexed().contains(cv))
                    // bounds of arrays are read only if the arrays are indexed
                    continue;
                if(cv.primitiveRange.min instanceof CodeFieldDereference) {
                    CodeVariable min = ((CodeFieldDereference)cv.primitiveRange.min).variable;
                    if(!(min.owner instanceof CodeMethod))
                        throw new RuntimeException("local has a field in its primitive range");
                    sources.add(min);
                }
                if(cv.primitiveRange.max instanceof CodeFieldDereference) {
                    CodeVariable max = ((CodeFieldDereference)cv.primitiveRange.max).variable;
                    if(!(max.owner instanceof CodeMethod))
                        throw new RuntimeException("local has a field in its primitive range");
                    sources.add(max);
                }
            }
        }
        for(RangedCodeValue r : getRangedValues())
            if(r.range != null) {
                if(r.range.min instanceof CodeFieldDereference) {
                    CodeVariable min = ((CodeFieldDereference)r.range.min).variable;
                    if(min.owner instanceof CodeMethod)
                        sources.add(min);
                }
                if(r.range.max instanceof CodeFieldDereference) {
                    CodeVariable max = ((CodeFieldDereference)r.range.max).variable;
                    if(max.owner instanceof CodeMethod)
                        sources.add(max);
                }
            }
        return sources;
    }
    /**
     * Returns references to all local variables read in this operation.
     * It includes object and indexing variables in any target dereferences,
     * because they are read, not written to. You may use
     * <code>addLocalSourcesInTargets()</code> to get these variables. It is typical for
     * overridings of this method to end with
     * <code>return addLocalSourcesInTargets(list);</code> where <code>list</code>
     * are the source references.<br>
     *
     * If there are any range checking variables read by an operation, they
     * should be returned as well. There is a helper method for this,
     * <code>extractAllLocalBoundSources</code>.<br>
     *
     * This implementation returns an empty set.
     *
     * @return                          a new set of local
     *                                  source variables, empty if there
     *                                  are no such variables
     */
    public Set<CodeVariable> getLocalSources() {
        return new HashSet<>();
    }
    /**
     * This is a helper method to be used by <code>getFieldSources()</code>
     * to add variables read in primitive ranges, both variable and primary
     * ones.<br>
     *
     * This method scans all dereferences given in <code>nonBoundSources</code>
     * and also all dereferences from a call to <code>getFieldTargets</code>
     * for variables read within variable primitive ranges. Then, returns a merge
     * of <code>nonBoundSources</code> with the field dereferences this method
     * has found in the variable primitive ranges, and also in any primary primitive
     * ranges found in <code>getRangedValues</code>.<br>
     *
     * Typical usage is <code>return extractAllFieldBoundSources(list);</code> as
     * the last statement of <code>getFieldSources</code> within each
     * <i>non--abstract</i> operation, to avoid repetition of calls
     * from within both abstract and non--abstract types.<br>
     *
     * Note that, as <code>nonBoundSources</code> constains only fields, all
     * possible primitive range variables found are also fields.
     *
     * @param nonBoundSources           fields read in this operation, but
     *                                  not from within the primitive bounds
     * @return                          all fields read in all primitive
     *                                  bounds of this operation
     */
    protected Set<CodeFieldDereference> extractAllFieldBoundSources(
            Collection<CodeFieldDereference> nonBoundSources) {
        Set<CodeFieldDereference> sources = new HashSet<>(nonBoundSources);
        Set<CodeFieldDereference> all = new HashSet<>(nonBoundSources);
        all.addAll(getFieldTargets());
        for(CodeFieldDereference d : all) {
            if(d.object != null && d.object.hasPrimitiveRange(false) &&
                    !d.object.getResultType().isArray(null))
                    throw new RuntimeException("non--array object has primitive range");
            if(d.variable.hasPrimitiveRange(false)) {
                if(d.variable.getResultType().isArray(null) &&
                        !getIndexed().contains(d.variable))
                    // bounds of arrays are read only if the arrays are indexed
                    continue;
                if(d.variable.primitiveRange.min instanceof CodeFieldDereference) {
                    CodeVariable min = ((CodeFieldDereference)d.variable.primitiveRange.min).variable;
                    // primitive bound belongs to the same object as
                    // the bounded non--static field, for static fields
                    // object is null as expected
                    if(d.object == null && min.context == Context.NON_STATIC)
                        throw new RuntimeException("field has local bound");
                    // <code>CodeVariablePrimitiveBound.reconstructMin</code> could be
                    // used as well
                    sources.add(new CodeFieldDereference(d.object, min));
                }
                if(d.variable.primitiveRange.max instanceof CodeFieldDereference) {
                    CodeVariable max = ((CodeFieldDereference)d.variable.primitiveRange.max).variable;
                    // primitive bound belongs to the same object as
                    // the bounded non--static field, for static fields
                    // object is nul las expected
                    if(d.object == null && max.context == Context.NON_STATIC)
                        throw new RuntimeException("field has local bound");
                    // <code>CodeVariablePrimitiveBound.reconstructMax</code> could be
                    // used as well
                    sources.add(new CodeFieldDereference(d.object, max));
                }
            }
        }
        for(RangedCodeValue r : getRangedValues())
            if(r.range != null) {
                if(r.range.min instanceof CodeFieldDereference) {
                    CodeFieldDereference d = (CodeFieldDereference)r.range.min;
                    if(!d.isLocal())
                        sources.add(d);
                }
                if(r.range.max instanceof CodeFieldDereference) {
                    CodeFieldDereference d = (CodeFieldDereference)r.range.max;
                    if(!d.isLocal())
                        sources.add(d);
                }
            }
        return sources;
    }
    /**
     * Returns references to all field variables read in this operation.<br>
     *
     * If there are any range checking field variables assisting a field, they
     * should be returned as well. There is a helper method for this,
     * <code>extractAllFieldBoundSources</code>.<br>
     *
     * Note: index dereferences can never read a non--static field.<br>
     *
     * This implementation returns an empty set.
     *
     * @return                          a new set of field
     *                                  source variables, empty if there
     *                                  are no such variables
     */
    public Set<CodeFieldDereference> getFieldSources() {
        return new HashSet<>();
    }
    /**
     * Returns if a specific local variable is a source variable
     * of this operation.
     *
     * @param variable                  variable to test
     * @return                          if is a local source
     */
    public boolean isLocalSource(CodeVariable variable) {
        boolean isSource = false;
        Collection<CodeVariable> source =
                getLocalSources();
        for(CodeVariable sourceVariable : source)
            if(sourceVariable == variable) {
                isSource = true;
                break;
            }
        return isSource;
    }
    /**
     * Returns references to all local variables written to in this operation.<br>
     *
     * Note that, if a field or an array element is written to, it does not
     * mean that the dereferenced variable is written to. Only actual change
     * of the dereferenced variable itself counts.<br>
     *
     * The target of any operation can possibly be found only in
     * <code>AbstractResultCodeOp.result</code>.<br>
     * 
     * This implementation returns an empty set.
     * 
     *
     * @return                          a new set of local
     *                                  target variables, empty if there
     *                                  are no such variables
     */
    public Set<CodeVariable> getLocalTargets() {
        return new HashSet<>();
    }
    /**
     * Returns references to all field variables written to in this operation.<br>
     *
     * Note that, if a field is written to, it does not mean that the dereferenced
     * variable is written to. Only actual change of the dereferenced variable
     * itself counts.<br>
     *
     * All index dereferences should be ignored, as they never modify a field.<br>
     *
     * This implementation returns an empty set.
     *
     * @return                          a new set of local
     *                                  target variables, empty if there
     *                                  are no such variables
     */
    public Set<CodeFieldDereference> getFieldTargets() {
        return new HashSet<>();
    }
    /**
     * Returns if a specific local variable is a target variable
     * of this operation.
     *
     * @param variable                  variable to test
     * @return                          if is a local target
     */
    public boolean isLocalTarget(CodeVariable variable) {
        boolean isTarget = false;
        Collection<CodeVariable> target =
                getLocalTargets();
        for(CodeVariable targetVariable : target)
            if(targetVariable == variable) {
                isTarget = true;
                break;
            }
        return isTarget;
    }
    /**
     * Check if the value is a dereference, and if yes, check
     * if the source parts of that dereference are equal to a given
     * variable, and if yes, replaces the parts with given
     * replacement. The dereference is treated as a target
     * one, that is, a variable part of a field dereference is never
     * treated as a source.<br>
     *
     * The whole dereference, if it were a source, could possibly be
     * replaced also by a constant, but in such case this method
     * would not apply as it can modify only the dereference's fields,
     * as opposed to replacement of a whole dereference.
     * Thus, this method limits itself to treating dereferences as
     * target, amd is in some cases used together
     * with <code>getPlainLocal()<br>, to take into account
     * the discussed case of a constant and to treat the variable part
     * of a field dereference as a source.
     *
     * @param value                     value to possibly modify
     * @param variable                  variable to replace in the
     *                                  source part of the value
     * @param replacement               replacement variable or null if to
     * remove an object part
     * @return                          if the value was modified
     */
    protected boolean replaceLocalSourcePartsInTarget(AbstractCodeValue value,
            CodeVariable variable, AbstractCodeValue replacement) {
        boolean replaced = false;
        if(value instanceof AbstractCodeDereference) {
            AbstractCodeDereference r = (AbstractCodeDereference)value;
            if(r.object == variable) {
                if(replacement instanceof CodeFieldDereference) {
                    CodeFieldDereference q = (CodeFieldDereference)replacement;
                    if(q.object != null)
                        throw new RuntimeException("can not replace object part " +
                                "with reference that has non-null object part");
                    else
                        r.object = q.variable;
                } else if(replacement == null) {
                    r.object = null;
                } else
                    throw new RuntimeException("can not replace object part " +
                            "with a constant");
                replaced = true;
            }
            if(value instanceof CodeIndexDereference) {
                CodeIndexDereference i = (CodeIndexDereference)value;
                if(i.index.value.equals(new CodeFieldDereference(variable)) &&
                        // see <code>CodeIndexDereference.index</code>
                        // for conditions on that value
                        (replacement instanceof CodeConstant ||
                            (replacement instanceof CodeFieldDereference &&
                                ((CodeFieldDereference)replacement).isLocal()))) {
                     i.index.value = replacement;
                     replaced = true;
                }
            }
        }
        return replaced;
    }
    /**
     * Check if the value is a dereference, and if yes, check
     * if the source parts of that dereference are equal to a given
     * field, and if yes, replaces the parts with given
     * replacement. The dereference is treated as a target
     * one, that is, the object/variable combination of a field
     * dereference is never treated as a source.<br>
     *
     * The whole dereference, if it were a source, could possibly be
     * replaced also by a constant, but in such case this method
     * would not apply as it can modify only the dereference's fields,
     * as opposed to replacement of a whole dereference.
     * Thus, this method limits itself to treating dereferences as
     * targets, and in some cases is used together
     * with <code>getPlainField()<br>, to take into account
     * the discussed case of a constant, and to treat the object/variable
     * combination as a source.
     *
     * @param value                     value to possibly modify
     * @param field                     variable to replace in the
     *                                  source part of the value
     * @param replacement               replacement variable or null if to
     * remove an object part
     * @return                          if the value was modified
     */
    protected boolean replaceFieldSourcePartsInTarget(AbstractCodeValue value,
            CodeFieldDereference field, AbstractCodeValue replacement) {
        boolean replaced = false;
        if(value instanceof AbstractCodeDereference) {
            AbstractCodeDereference r = (AbstractCodeDereference)value;
            if(r instanceof CodeFieldDereference) {
                CodeFieldDereference f = (CodeFieldDereference)r;
                if(f.variable.primitiveRange != null) {
                    if(f.variable.primitiveRange.max instanceof CodeFieldDereference) {
                        CodeFieldDereference m = (CodeFieldDereference)f.variable.primitiveRange.max;
                        if(m.object != null)
                            throw new RuntimeException("unexpected");
                        if(replaceFieldSourcePartsInTarget(new CodeFieldDereference(f.object,
                                m.variable), field, replacement))
                            replaced = true;
                    }
                    if(f.variable.primitiveRange.min instanceof CodeFieldDereference) {
                        CodeFieldDereference m = (CodeFieldDereference)f.variable.primitiveRange.min;
                        if(m.object != null)
                            throw new RuntimeException("unexpected");
                        if(replaceFieldSourcePartsInTarget(new CodeFieldDereference(f.object,
                                m.variable), field, replacement))
                            replaced = true;
                    }
                }
            }
            if(field.object == null && r.object == field.variable) {
                // the field is static
                if(replacement instanceof CodeFieldDereference) {
                    CodeFieldDereference q = (CodeFieldDereference)replacement;
                    if(q.object != null)
                        throw new RuntimeException("can not replace object part " +
                                "with reference that has non-null object part");
                    else {
                        r.object = q.variable;
                        replaced = true;
                    }
                } else if(replacement == null) {
                    r.object = null;
                } else
                    throw new RuntimeException("can not replace object part " +
                            "with a constant");
            }
            if(r.equals(field)) {
                if(replacement instanceof CodeFieldDereference) {
                    CodeFieldDereference q = (CodeFieldDereference)replacement;
                    r.object = q.object;
                    ((CodeFieldDereference)r).variable = q.variable;
                } else
                    throw new RuntimeException("can not replace a dereference " +
                            "with a constant using this method");
                replaced = true;
            }
            if(value instanceof CodeIndexDereference) {
                CodeIndexDereference i = (CodeIndexDereference)value;
                if(field.object == null) {
                     // the field is static, might fit into parts of index dereference
                     if(i.index.value.equals(new CodeFieldDereference(field.variable))) {
                        i.index.value = replacement;
                        replaced = true;
                     }
                }
            }
        }
        return replaced;
    }
    /*
     * Bulk object replacement check for all target references.
     * 
     * @param variable                  variable to replace in the
     *                                  object part of the reference
     * @param replacement               replacement variable
     * @return                          if any reference was modified
     * @see #replaceSourceParts
     */
    /*
    protected boolean replaceSourcesInTargets(
            AbstractCodeDereference sourceVariable, AbstractCodeValue value) {
        boolean found = false;
        Collection<AbstractCodeDereference> list = getTargets();
        for(AbstractCodeDereference r : list)
            if(replaceSourceParts(r, sourceVariable, value))
                found = true;
        return found;
    }
     */
    /**
     * Returns if a variable is within a range of any of the variables
     * within this operation.<br>
     * 
     * As variable bounds can be shared by operations, special handling
     * is needed when replaced. See docs of <code>replaceBoundLocalSources</code>
     * and <code>replaceBoundFieldSources</code> for details.
     * 
     * @param d                         local or field
     * @return                          if is in variable bounds
     */
    public boolean belongsToVariableRange(CodeFieldDereference d) {
        if(d.isLocal()) {
            // locals can occur only in primitive ranges of other locals
            Collection<CodeVariable> all = getLocalSources();
            all.addAll(getLocalTargets());
            for(CodeVariable cv : all)
                if(cv.hasPrimitiveRange(false))
                    if(cv.primitiveRange.min.equals(d) ||
                            cv.primitiveRange.max.equals(d))
                        return true;
        } else {
            // fields can occur only in primitive ranges of fields
            Collection<CodeFieldDereference> all = getFieldSources();
            all.addAll(getFieldTargets());
            for(CodeFieldDereference f : all)
                if(f.variable.hasPrimitiveRange(false)) {
                    if(f.variable.primitiveRange.min instanceof CodeFieldDereference &&
                            new CodeFieldDereference(f.object, ((CodeFieldDereference)
                            f.variable.primitiveRange.min).variable).equals(d))
                        return true;
                    if(f.variable.primitiveRange.max instanceof CodeFieldDereference &&
                            new CodeFieldDereference(f.object, ((CodeFieldDereference)
                            f.variable.primitiveRange.max).variable).equals(d))
                        return true;
                }
        }
        return false;
    }
    /**
     * Replaces local variables in primary primitive ranges.
     * Such variables are always sources.<br>
     *
     * It scans all locals for the bounds. Local variables have also
     * local bound variables, so no need to check.<br>
     *
     * Typical usage is
     * <code>if(replaceBounds(sourceVariable, value)) found = true;</code>
     * as one of the statements of <code>replaceLocalSourceVariable</code> within each
     * <i>non--abstract</i> operation sub--class, to avoid repetition of calls
     * from within both abstract and non--abstract types.
     *
     * @param sourceVariable            variable to replace
     * @param value                     replacement value
     */
    protected boolean replaceBounds(CodeVariable sourceVariable,
            AbstractCodeValue value) {
        boolean replaced = false;
        CodeFieldDereference wrapper = new CodeFieldDereference(sourceVariable);
        for(RangedCodeValue r : getRangedValues())
            if(r.range != null) {
                if(r.range.min.equals(wrapper)) {
                    r.range.min = value;
                    replaced = true;
                }
                if(r.range.max.equals(wrapper)) {
                    r.range.max = value;
                    replaced = true;
                }
            }
        return replaced;
    }
//    /**
//     * Replaces field variables in variable and primary primitive bounds.
//     * Such variables are always sources.<br>
//     *
//     * A replaced variable in a variable primitive range can have
//     * an effect on other operations, this method handles such cases
//     * identically to what <code>replaceBoundLocalSources</code> does,
//     * see that method docs for details.<br>
//     *
//     * It scans all fields for the bounds. Field variables have also
//     * field bound variables, so no need to check.<br>
//     *
//     * Typical usage is
//     * <code>if(replaceBoundFieldSources(sourceVariable, value)) found = true;</code>
//     * as one of the statements of <code>replaceFieldSourceVariable</code> within each
//     * <i>non--abstract</i> operation, to avoid repetition of calls
//     * from within both abstract and non--abstract types.
//     *
//     * @param sourceVariable            dereference to replace
//     * @param value                     replacement value
//     * @param allowReplaceShared        if to allows replacement of a variable
//     *                                  that is possibly shared by other ops,
//     *                                  by other variable; see docs of
//     *                                  <code>replaceBoundLocalSources</code> for details
//     * @return                          if at least a single dereference
//     *                                  has been replaced
//     */
//    protected boolean replaceBoundFieldSources(CodeFieldDereference sourceVariable,
//            AbstractCodeValue value, boolean allowReplaceShared) {
//        if(allowReplaceShared)
//            throw new RuntimeException("illegal as map is needed -- " +
//                    "see replaceBoundLocalSources() for details");
//        boolean replaced = false;
//        Collection<CodeFieldDereference> all = getFieldSources();
//        all.addAll(getFieldTargets());
//        for(CodeFieldDereference d : all)
//            if(d.variable.hasPrimitiveRange(false)) {
//                if(d.variable.primitiveRange.min instanceof CodeFieldDereference &&
//                        // field and its bound variables share the same object
//                        new CodeFieldDereference(d.object, ((CodeFieldDereference)
//                        d.variable.primitiveRange.min).variable).equals(sourceVariable)) {
//                    if(!allowReplaceShared && !(value  instanceof CodeConstant))
//                        throw new RuntimeException("variable in variable range replaced by a variable");
//                    d.variable.primitiveRange.min = value;
//                    replaced = true;
//                }
//                if(d.variable.primitiveRange.max instanceof CodeFieldDereference &&
//                        // field and its bound variables share the same object
//                        new CodeFieldDereference(d.object, ((CodeFieldDereference)
//                        d.variable.primitiveRange.max).variable).equals(sourceVariable)) {
//                    if(!allowReplaceShared && !(value  instanceof CodeConstant))
//                        throw new RuntimeException("variable in variable range replaced by a variable");
//                    d.variable.primitiveRange.max = value;
//                    replaced = true;
//                }
//            }
//        for(RangedCodeValue r : getRangedValues())
//            if(r.range != null) {
//                if(r.range.min.equals(sourceVariable)) {
//                    r.range.min = value;
//                    replaced = true;
//                }
//                if(r.range.max.equals(sourceVariable)) {
//                    r.range.max = value;
//                    replaced = true;
//                }
//            }
//        return replaced;
//    }
    /**
     * Replaces any instance of a given source local
     * variable in this operation with a value.<br>
     *
     * The local sources include variables in primary primitive ranges. There is a
     * helper method for this, <code>replaceBoundLocalSources</code>.
     *
     * This implementation does nothing.
     *
     * @param sourceVariable            reference to replace
     * @param value                     replacement value
     * @return                          if at least a single dereference
     *                                  has been replaced
     */
    public boolean replaceLocalSourceVariable(CodeVariable sourceVariable,
            AbstractCodeValue value) {
        return false;
    }
    /**
     * Replaces any instance of a given source field
     * variable in this operation with a value.<br>
     *
     * If a variable that is to be replaced can not be replaced
     * by a constant but the replacement value is a constant,
     * a runtime exception is thrown.<br>
     *
     * The field sources include primitive bound field variables. There is a
     * helper method for this, <code>replaceBounds</code>.<br>
     *
     * This implementation does nothing.
     *
     * @param sourceField               field to replace
     * @param value                     replacement value
     * @return                          if at least a single dereference
     *                                  has been replaced
     */
    public boolean replaceFieldSourceVariable(CodeFieldDereference sourceField,
            AbstractCodeValue value) {
        return false;
    }
    /**
     * Replaces any instance of a given target local
     * variable in this operation with another local
     * variable.<br>
     *
     * A local variable is a target only in field dereferences
     * with the object part being null.<br>
     *
     * This implementation does nothing.
     *
     * @param sourceVariable            reference to replace
     * @param value                     replacement value
     * @return                          if at least a single reference
     *                                  has been replaced
     */
    public boolean replaceLocalTargetVariable(CodeVariable targetVariable,
            AbstractCodeDereference dereference) {
        return false;
    }
    /**
     * Sets the reference to the variable to which is written the
     * result of this operation or null if there should be no such
     * variable.<br>
     *
     * Only instances of the subclass <code>AbstractResultCodeOp</code>
     * do not throw a runtime exception when this method is called.<br>
     *
     * This implementation throws a runtime exception.
     * 
     * @param result                    dereference or null
     */
    public void setResult(AbstractCodeDereference result) {
        throw new RuntimeException("can not set result variable");
    }
    /**
     * Returns the reference to the variable to which is written
     * the result of this operation or null if there is no such
     * variable.
     * 
     * Only instances of the subclass <code>AbstractResultCodeOp</code>
     * can possibly not return null when this method is called.<br>
     *
     * This implementation returns null.
     * 
     * @return                          dereference or null
     */
    public AbstractCodeDereference getResult() {
        return null;
    }
    /**
     * If there is at least a single restrictive source level range
     * within this operation's ranges.
     * 
     * @return if a restrictive source level range is found
     */
    public boolean hasRestrictiveSourceLevelRanges() {
        Set<RangedCodeValue> ranges = getRangedValues();
        for(RangedCodeValue r : ranges)
            if(r.hasRestrictiveSourceLevelRange())
                return true;
        return false;
    }
    /*
     * Returns the ariness: 0 none, 1 unary, 2 binary, 3 tertiary,
     * -1 multiple.
     * 
     * @return                          ariness
     */
    /*
    public int getAriness() {
        int ariness = -1;
        switch(op) {
            case UNARY_NEGATION:
            case UNARY_CONDITIONAL_NEGATION:
            case UNARY_BITWISE_COMPLEMENT:
            case JUMP:
            case RETURN:
                ariness = 1;
                break;
                
            case BINARY_PLUS:
            case BINARY_MINUS:
            case BINARY_MULTIPLY:
            case BINARY_DIVIDE:
            case BINARY_MODULUS:
            case BINARY_INCLUSIVE_OR:
            case BINARY_EXCLUSIVE_OR:
            case BINARY_AND:
            case BINARY_EQUAL:
            case BINARY_INEQUAL:
            case BINARY_LESS:
            case BINARY_LESS_OR_EQUAL:
            case BINARY_SHIFT_LEFT:
            case BINARY_SHIFT_RIGHT:
            case BINARY_UNSIGNED_SHIFT_RIGHT:
            case BINARY_INDEX:
            case ASSIGNMENT:
                ariness = 2;
                break;

            case BRANCH:
                ariness = 3;
                break;
                
            case CALL:
            case ALLOCATION:
                ariness = -1;
                break;
                
            default:
                throw new RuntimeException("unknown operator: " + op);

        }
        return ariness;
    }
     */
    /*
     * Returns a code operator type that corresponds to a given expression
     * type. Throws RuntimeException if no corresponding operator type
     * exists.
     * 
     * @param type                      expression type
     * @return                          operator type
     */
    /*
    public static AbstractCodeOp.Type getOpType(pl.gliwice.iitis.hedgeelleth.compiler.tree.Type type) {
        AbstractCodeOp.Type opType;
        // in java an unary or a binary expression can have a result only of
        // either a primitive type or java.lang.String
        if(type.isString(null))
            opType = AbstractCodeOp.Type.TYPE_STRING;
        else switch(type.getPrimitive()) {
            case BOOLEAN:
                opType = AbstractCodeOp.Type.TYPE_BOOLEAN;
                break;
                
            case DOUBLE:
                opType = AbstractCodeOp.Type.TYPE_DOUBLE;
                break;
                
            case FLOAT:
                opType = AbstractCodeOp.Type.TYPE_FLOAT;
                break;
                
            case INT:
                opType = AbstractCodeOp.Type.TYPE_INT;
                break;
                
            case LONG:
                opType = AbstractCodeOp.Type.TYPE_LONG;
                break;
                
            default:
                throw new RuntimeException("invalid type: " +
                        type);
                
        }
        return opType;
    }
     */
    @Override
    public void setStreamPos(StreamPos pos) {
        this.pos = pos;
    }
    @Override
    public StreamPos getStreamPos() {
        return pos;
    }
    /**
     * Adds a tag.
     * 
     * Known tags whose number of instances does not matter are checked
     * for repetitiveness and only one is left.
     * 
     * The tags are added even if their annotation modifiers
     * do not allow it. Use filterTags() after all annotations are added
     * to filter the forbidden tags.
     * 
     * @param ct                        tag to add
     */
    public void addTag(CommentTag ct) {
        if(ct.name.equals(CommentTag.TAG_GENERATE_HEAD_STRING) ||
                ct.name.equals(CommentTag.TAG_GENERATE_TAIL_STRING))
            for(CommentTag c : tags)
                if(c.name.equals(ct.name))
                    return;
        tags.add(ct);
    }
    /**
     * Adds tags, with the policy as specified by <code>addTag</code>.
     * 
     * @param tags                      tags to add
     * @see #addTag
     */
    public void addTags(Collection<CommentTag> tags) {
        for(CommentTag ct : tags)
            addTag(ct);
    }
    /**
     * Returns tags of this operation.
     * 
     * @return                          collection of tags
     */
    public Collection<CommentTag> getTags() {
        return tags;
    }
    /**
     * Appends tags from some operation, with the policy as specified by
     * <code>addTag</code>.
     * 
     * Only tags whose recursion policy allows are added.
     * 
     * @param tags                      operation with the tags to append
     * @param inlineRecursion           if this is a recursive applying
     *                                  of the tags into an inlined method,
     *                                  what copies the allowed tag and
     *                                  increases the copy's recursion count
     * @param inlineRecursionFirstOp    if this in a first inlined operation,
     *                                  what makes tags with application
     *                                  policy INLINE_RECURSION_FIRST to
     *                                  be allowed
     * @see #addTag
     */
    public void appendTags(AbstractCodeOp op, boolean inlineRecursion,
            boolean inlineRecursionFirstOp) {
        if(inlineRecursion) {
            for(CommentTag tag : op.tags) {
                CommentTag copy = new CommentTag(tag);
                ++copy.recursiveInlineApplyCounter;
                boolean allowed;
                switch(copy.inlineRecursionPolicy) {
                    case CommentTag.INLINE_RECURSION_INFINITE:
                        allowed = true;
                        break;
                        
                    case CommentTag.INLINE_RECURSION_FIRST:
                        allowed = inlineRecursionFirstOp;
                        break;

                    default:
                        allowed = copy.inlineRecursionPolicy >=
                                copy.recursiveInlineApplyCounter;
                        break;
                        
                }
                if(allowed)
                    addTag(copy);
            }
        } else
            addTags(op.tags);
    }
    /**
     * Removes all tags whose annotation modifiers do not allow the tags
     * to mark this operation.
     * 
     * Call this operation only after all annotations are added to operations.
     * Note that some backends may also add annotations.
     * 
     * As a generic operation does not define any annotations, this
     * implementation removes all tags that require at least a single
     * annotation. If a subclass defines annotations, override this
     * method.
     */
    public void filterTags() {
        for(CommentTag ct : new LinkedList<>(tags))
            if(ct.annotationsRequired.size() > 0)
                tags.remove(ct);
    }
    /**
     * Replaces ranged code values with their copies. Also
     * shallow copies of dereferences within the ranged code
     * values are created.
     * 
     * This is a helper method to clone() of some operations.
     * 
     * @param list                      list with code values
     */
    protected void copyReferences(List<RangedCodeValue> list) {
       for(int i = 0; i < list.size(); ++i) {
           RangedCodeValue rv = new RangedCodeValue(list.get(i));
           list.set(i, rv);
       }
    }
    /**
     * An abstract accept method.
     *
     * @param c                         helper value, null for none
     * @param v                         visitor
     * @return                          helper return value, null for
     *                                  none
     */
    public abstract Object accept(Object c, CodeVisitor<? extends Object, ? extends Object> v)  throws TranslatorException;
//    /**
//     * If this is <code>CodeOpNone</code> with one of its annotations being
//     * <code>@ERROR</code>.<br>
//     * 
//     * This implementation returns false.
//     * 
//     * @return if an error code, that can not be passed by any thread
//     */
//    public boolean isError() {
//        return false;
//    }
    /**
     * This is a shallow copy except for this operation collections
     * and arrays, code dereferences, code method
     * references and code labels. If any of these exceptions
     * occur in an array or a collection, it is also copied. All
     * of these exceptions are replaced with their shallow
     * copies.<br>
     * 
     * Thus, <code>CodeVariable</code> and <code>CodeMethod</code>
     * are never copied, following their general contract, but otherwise
     * the cloned operation is independent of the original one.<br>
     * 
     * There is a helper method <code>copyReferences</code>
     * that replaces all code dereferences in a list.
     * 
     * @return                          copy of this operation
     */
    @Override
    public AbstractCodeOp clone() {
       Object o;
        try {
            o = super.clone();
        } catch(CloneNotSupportedException e) {
            // should not happen
            o = null;
        }
        AbstractCodeOp out = (AbstractCodeOp)o;
        if(label == null)
            out.label = null;
        else
            out.label = new CodeLabel(label);
        out.tags = new LinkedList<>(tags);
        return out;
    }
    /**
     * Returns a short name of a constant or a variable.
     *
     * @param v                         value
     * @return                          short name
     */
    public static String valueName(Typed v) {
        if(v == null)
            return "<null>";
        else if(v instanceof CodeConstant)
            return v.toString();
        else if (v instanceof CodeMethodReference) {
            return v.toString();
        } else {
            AbstractCodeDereference dereference = ((AbstractCodeDereference)v);
            String s = dereference.toNameString();
            return s;
        }
    }
    /**
     * Returns a short name of a constant or a variable, plus possible
     * primitive range.
     *
     * @param v                         value
     * @return                          short name
     */
    public static String valueName(RangedCodeValue rv) {
        String s = "";
        if(rv.range != null)
            s += rv.range.toString();
        s += valueName(rv.value);
        return s;
    }
    /**
     * Like toString(), but variables have names relative to the context.
     * 
     * This implementation returns description of the optional label
     * and possible tags.
     * 
     * @param method                    code method, that is the context,
     *                                  relatively to which are displayed
     *                                  the variables, as defined in
     *                                  <code>valueName</code>
     * @return                          description
     */
    public String toString(CodeMethod method) {
       String s;
       if(label != null && (label.codeIndex == -1 || CodeLabel.LABEL_DEBUG))
           s = "[" + label.toString() + "] ";
       else
           s = "";
       if(false && canBeDead)
           s += "D ";
       if(!tags.isEmpty()) {
           s += "<";
           for(CommentTag tag : tags)
               s += tag.toString() + " ";
           s = s.substring(0, s.length() - 1) + "> ";
       }
       return s;
    }
    @Override
    public String toString() {
        return toString(null);
    }
}
