/*
 * AbstractCodeOpInvocation.java
 *
 * Created on Jan 29, 2009
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.code.op;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.code.AbstractCodeValue;
import pl.gliwice.iitis.hedgeelleth.compiler.code.CodeConstant;
import pl.gliwice.iitis.hedgeelleth.compiler.code.CodeMethod;
import pl.gliwice.iitis.hedgeelleth.compiler.code.CodeVariable;
import pl.gliwice.iitis.hedgeelleth.compiler.code.RangedCodeValue;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.AbstractCodeDereference;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.CodeFieldDereference;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.CodeIndexDereference;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * An abstract invocation operation -- has a list of arguments,
 * and a flag is this is a non--static invocation, that is,
 * if the first argument is "this".<br>
 *
 * Also, has a result inherited from <code>AbstractResultCodeOp</code>,
 * as all invocations have a result.<br>
 *
 * Subclasses are method call, object allocation and
 * <code>CodeOpNone</code>.<br>
 *
 * This class does not have its own <code>toString<code>
 * method, so that a subclass can decide where to display
 * the arguments within its <code>toString</code> method.
 * This object has, though, the method
 * <code>argsToString(CodeMethod)</code> for displaying its list
 * of arguments.
 *
 * @author Artur Rataj
 */
public abstract class AbstractCodeOpInvocation extends AbstractResultCodeOp {
    /**
     * Arguments of the invoked item.
     */
    public List<RangedCodeValue> args;
    /**
     * Values copied from <code>args</code>, to preserve variables later optimised out.
     * Null if such a copy has not been made.
     */
    public List<AbstractCodeValue> preArgs;

    /**
     * Creates a new instance of AbstractCodeOpInvocation, with an empty
     * list of tags, without a label, and an empty list of arguments.
     *
     * @param op                        operation
     * //param type                      operation type
     * @param pos                       position in the parsed stream
     */
    public AbstractCodeOpInvocation(Op op, /*Type type, */StreamPos pos) {
        super(op, pos);
        args = new ArrayList<>();
    }
    /**
     * Creates <code>preArgs</code>.
     */
    public void makePreArgs() {
        preArgs = new LinkedList<>();
        for(RangedCodeValue rv : args) {
            preArgs.add(rv.value.clone());
        }
    }
    /**
     * If the invocation is static. If it is not static, then it means that
     * the first argument is "this". In the case of <code>CodeOpAllocation</code>,
     * a runtime exception is thrown.
     *
     * @return                          if this invocation is static
     */
    abstract public boolean isStatic();
    @Override
    public Set<RangedCodeValue> getRangedValues() {
        Set<RangedCodeValue> list = super.getRangedValues();
        for(RangedCodeValue v : args)
            list.add(v);
        return list;
    }
    @Override
    public Set<CodeIndexDereference> getIndexDereferences(boolean alsoWrite) {
        Set<CodeIndexDereference> indexed = super.getIndexDereferences(alsoWrite);
        for(RangedCodeValue v : args)
            if(v.value instanceof CodeIndexDereference)
                indexed.add((CodeIndexDereference)v.value);
        return indexed;
    }
    @Override
    public boolean replaceIndexDereference(
            CodeIndexDereference i, CodeConstant c) {
        boolean changed = false;
        for(int index = 0; index < args.size(); ++index) {
            RangedCodeValue v = args.get(index);
            if(v.value.equals(i)) {
                v.value = c;
                changed = true;
            }
        }
        return changed;
    }
    @Override
    public Set<CodeVariable> getLocalSources() {
        Set<CodeVariable> list =
                super.getLocalSources();
        for(RangedCodeValue v : args)
            if(v.value instanceof AbstractCodeDereference)
                list.addAll(((AbstractCodeDereference)v.value).
                        extractLocalSourcePartsInSource());
        return list;
    }
    @Override
    public Set<CodeFieldDereference> getFieldSources() {
        Set<CodeFieldDereference> list =
                super.getFieldSources();
        for(RangedCodeValue v : args)
            if(v.value instanceof AbstractCodeDereference)
                list.addAll(((AbstractCodeDereference)v.value).
                        extractFieldSourcePartsInSource());
        return list;
    }
    @Override
    public boolean replaceLocalSourceVariable(CodeVariable sourceVariable,
            AbstractCodeValue value) {
        boolean found = super.replaceLocalSourceVariable(sourceVariable, value);
        int count = 0;
        for(RangedCodeValue v : args) {
            if(extractPlainLocal(v.value) == sourceVariable) {
                // an exception, see the docs of <code>replaceLocalSourcePartsInTarget</code>
                // for details
                RangedCodeValue rv = new RangedCodeValue(value);
                args.set(count, rv);
                found = true;
            } else if(replaceLocalSourcePartsInTarget(v.value, sourceVariable, value)) {
                found = true;
            }
            ++count;
        }
        return found;
    }
    @Override
    public boolean replaceFieldSourceVariable(CodeFieldDereference sourceField,
            AbstractCodeValue value) {
        boolean found = super.replaceFieldSourceVariable(sourceField, value);
        int count = 0;
        for(RangedCodeValue v : args) {
            if(sourceField.equals(extractPlainField(v.value))) {
                // an exception, see the docs of <code>replaceLocalSourcePartsInTarget</code>
                // for details
                RangedCodeValue rv = new RangedCodeValue(value);
                args.set(count, rv);
                found = true;
            } else if(replaceFieldSourcePartsInTarget(v.value, sourceField, value)) {
                found = true;
            }
            ++count;
        }
        return found;
    }
    /**
     * For non--static invocations, returns the value to be
     * assigned to the object argument "this". If the invocation
     * is static or <code>CodeOpAllocation</code>, throws a
     * runtime exception.
     *
     * @return                          object argument "this"
     *                                  or null
     */
    public RangedCodeValue getThisArg() {
        if(isStatic())
            throw new RuntimeException(
                    "a static invocation does not have \"this\"");
        else
            return args.get(0);
    }
    /**
     * A helper method for displaying the list of arguments within
     * <code>toString(CodeMethod)</code>.
     *
     * @param method                    code method, that is the context,
     *                                  relatively to which are displayed
     *                                  the variables, as defined in
     *                                  <code>valueName</code>
     * @return                          textual description of the
     *                                  arguments, does not include any
     *                                  bounding parentheses or brackets
     */
    protected String argsToString(CodeMethod method) {
        String s = "";
        int count = 0;
        for(RangedCodeValue v : args) {
            if(count == 0) {
                if(!(this instanceof CodeOpAllocation) && !isStatic())
                    s += "(this)";
            } else
                s += ", ";
            s += valueName(v);
            ++count;
        }
        return s;
    }
    @Override
    public AbstractCodeOp clone() {
        Object o = super.clone();
        AbstractCodeOpInvocation out = (AbstractCodeOpInvocation)o;
        out.args = new ArrayList<>(args);
        copyReferences(out.args);
        if(preArgs != null) {
            out.preArgs = new ArrayList<>();
            for(AbstractCodeValue v : preArgs)
                out.preArgs.add(v.clone());
        }
        return out;
    }
}
