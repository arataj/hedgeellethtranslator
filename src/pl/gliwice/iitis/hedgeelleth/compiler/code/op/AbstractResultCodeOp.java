/*
 * AbstractResultCodeOp.java
 *
 * Created on Sep 1, 2009, 12:14:06 PM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.code.op;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.code.AbstractCodeValue;
import pl.gliwice.iitis.hedgeelleth.compiler.code.CodeConstant;
import pl.gliwice.iitis.hedgeelleth.compiler.code.CodeVariable;
import pl.gliwice.iitis.hedgeelleth.compiler.code.RangedCodeValue;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * An abstract operation that has a result.
 *
 * @author Artur Rataj
 */
public abstract class AbstractResultCodeOp extends AbstractCodeOp {
    /**
     * Result of this expression.
     */
    public AbstractCodeDereference result;

    /**
     * Creates a new instance of AbstractResultCodeOp.
     *
     * @param op                        operation
     * @param pos                       position in the parsed stream
     */
    public AbstractResultCodeOp(Op op, StreamPos pos) {
        super(op, pos);
    }
    @Override
    public void setResult(AbstractCodeDereference result) {
        this.result = result;
    }
    @Override
    public AbstractCodeDereference getResult() {
        return result;
    }
    @Override
    public Set<CodeIndexDereference> getIndexDereferences(boolean alsoWrite) {
        Set<CodeIndexDereference> indexed = super.getIndexDereferences(alsoWrite);
        if(alsoWrite && getResult() instanceof CodeIndexDereference)
            indexed.add((CodeIndexDereference)getResult());
        return indexed;
    }
    @Override
    public boolean replaceIndexDereference(
        CodeIndexDereference i, CodeConstant c) {
        if(getResult().equals(i))
            throw new RuntimeException("result can not be a constant");
        return false;
    }
    @Override
    public Set<CodeVariable> getLocalSources() {
        Set<CodeVariable> list =
                super.getLocalSources();
        if(getResult() != null)
            list.addAll(getResult().extractLocalSourcePartsInTarget());
        return list;
    }
    @Override
    public Set<CodeFieldDereference> getFieldSources() {
        Set<CodeFieldDereference> list =
                super.getFieldSources();
        if(getResult() != null)
            list.addAll(getResult().extractFieldSourcePartsInTarget());
        return list;
    }
    @Override
    public Set<CodeVariable> getLocalTargets() {
        Set<CodeVariable> list =
                super.getLocalTargets();
        CodeVariable t = extractPlainLocal(getResult());
        if(t != null)
            list.add(t);
        return list;
    }
    @Override
    public Set<CodeFieldDereference> getFieldTargets() {
        Set<CodeFieldDereference> list =
                super.getFieldTargets();
        CodeFieldDereference t = extractPlainField(getResult());
        if(t != null)
            list.add(t);
        return list;
    }
    @Override
    public boolean replaceLocalSourceVariable(CodeVariable sourceVariable,
            AbstractCodeValue value) {
        boolean found = super.replaceLocalSourceVariable(sourceVariable, value);
        if(replaceLocalSourcePartsInTarget(result, sourceVariable, value))
            found = true;
        return found;
    }
    @Override
    public boolean replaceFieldSourceVariable(CodeFieldDereference sourceField,
            AbstractCodeValue value) {
        boolean found = super.replaceFieldSourceVariable(sourceField, value);
        if(replaceFieldSourcePartsInTarget(result, sourceField, value))
            found = true;
        return found;
    }
    @Override
    public boolean replaceLocalTargetVariable(CodeVariable targetVariable,
            AbstractCodeDereference dereference) {
        boolean replaced = super.replaceLocalTargetVariable(targetVariable,
                dereference);
        if(extractPlainLocal(getResult()) == targetVariable) {
            result = dereference;
            replaced = true;
        }
        return replaced;
    }
    /**
     * True if lvalue is both non--internal and has range specified. False
     * otherwise, that is, if the lvalue does not have source--level range
     * checking.<br>
     *
     * This method is used by the optimizer. If the return value
     * is false, the lvalue is assumed to be "transparent" and thus can be
     * replaced.<br>
     *
     * @return                          if lvalue is opaque
     */
    public boolean resultIsOpaque() {
        // find the variable that can have the range specified
        return result.hasRestrictiveSourceLevelRange();
    }
    @Override
    public AbstractCodeOp clone() {
       Object o = super.clone();
       AbstractResultCodeOp out = (AbstractResultCodeOp)o;
       if(result != null)
          out.result = result.clone();
       return out;
    }
}
