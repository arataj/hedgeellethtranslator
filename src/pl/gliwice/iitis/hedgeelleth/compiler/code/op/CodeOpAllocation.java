/*
 * CodeOpAllocation.java
 *
 * Created on Mar 30, 2008
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.code.op;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.code.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Type;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * An allocation operation.
 * 
 * @author Artur Rataj
 */
public class CodeOpAllocation extends AbstractCodeOpInvocation {
    /**
     * Constructor if this is a class allocation, null otherwise.
     */
    public CodeMethodReference constructor;
    /**
     * If this is array instantiation, returns the type of the arrays.
     * Otherwise, it is null.
     */
    public Type arrayType;
    
    /**
     * Creates a new instance of CodeOpAllocation,
     * without a label.
     * 
     * @param pos                       position in the parsed stream
     */
    public CodeOpAllocation(StreamPos pos) {
        super(AbstractCodeOp.Op.ALLOCATION, /* AbstractCodeOp.Type.TYPE_MIXED,*/
                pos);
    }
    /**
     * Returns the type of the instantiated object.
     * 
     * @return type
     */
    public Type getInstantiatedType() {
        if(constructor != null) 
            return constructor.method.owner.type;
        else
            return arrayType;
    }
    @Override
    public boolean isStatic() {
        throw new RuntimeException("undefined");
    }
    @Override
    public Set<CodeVariable> getLocalSources() {
        Set<CodeVariable> list = super.getLocalSources();
        return extractAllLocalBoundSources(list);
    }
    @Override
    public Set<CodeFieldDereference> getFieldSources() {
        Set<CodeFieldDereference> list =
                super.getFieldSources();
        return extractAllFieldBoundSources(list);
    }
    @Override
    public boolean replaceLocalSourceVariable(CodeVariable sourceVariable,
            AbstractCodeValue value) {
        boolean found = super.replaceLocalSourceVariable(sourceVariable, value);
        if(replaceBounds(sourceVariable, value))
            found = true;
        return found;
    }
    @Override
    public boolean replaceFieldSourceVariable(CodeFieldDereference sourceField,
            AbstractCodeValue value) {
        boolean found = super.replaceFieldSourceVariable(sourceField, value);
        return found;
    }
    @Override
    public boolean replaceLocalTargetVariable(CodeVariable targetVariable,
            AbstractCodeDereference dereference) {
        boolean replaced = super.replaceLocalTargetVariable(targetVariable,
                dereference);
        return replaced;
    }
    @Override
    public Object accept(Object c, CodeVisitor v)  throws TranslatorException {
        return v.visit(c, this);
    }
    @Override
    public AbstractCodeOp clone() {
        Object o = super.clone();
        CodeOpAllocation out = (CodeOpAllocation)o;
        if(constructor == null)
            out.constructor = null;
        else
            out.constructor = new CodeMethodReference(constructor);
        return out;
    }
    @Override
    public String toString(CodeMethod method) {
        String s = super.toString(method);
        s = s + valueName(result) + " = new ";
        if(constructor != null) {
            // constructor allocation
            s += valueName(constructor);
            s += " (" + argsToString(null) + ")";
        } else {
            Type t = new Type(result.getTypeSourceVariable().type);
            // while(t.isArray(null))
                t.decreaseDimension(null);
            s += t.toNameString();
            s += " [" + argsToString(null) + "]";
        }
        return s;
    }
}
