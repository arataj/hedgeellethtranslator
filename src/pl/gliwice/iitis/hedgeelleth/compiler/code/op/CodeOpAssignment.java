/*
 * CodeOpAssignment.java
 *
 * Created on Mar 30, 2008
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.code.op;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.code.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * An assignment operation.
 * 
 * @author Artur Rataj
 */
public class CodeOpAssignment extends AbstractResultCodeOp {
    /**
     * Right side of this assignment. Some optimizations
     * set this variable to null to mark assignments to
     * remove.
     */
    public RangedCodeValue rvalue;
    /*
     * This assignment is an initializer of a final variable beside the variable's
     * eclaration. Used only to check validities of assignment of final variables.
     * Default is false.
     */
    /* public boolean defaultInitializer = false; */
    
    /**
     * Creates a new instance of CodeOpAssignment, without a label.
     *
     * 
     * @param pos                       position in the parsed stream
     */
    public CodeOpAssignment(StreamPos pos) {
        super(AbstractCodeOp.Op.ASSIGNMENT, /*AbstractCodeOp.Type.TYPE_MIXED,*/
                pos);
    }
    /**
     * True if there are ranges on rvalue.<br>
     *
     * Note that it is meaningless for this method if rvalue
     * is a ranged variable, as this assignment does not write anything to
     * the the variable. This is opposite to what <code>resultIsOpaque</code>
     * does.<br>
     *
     * This method is used by the optimizer. If the return value
     * is false, this assignment's rvalue is assumed "transparent" and thus
     * can be optimized out.<br>
     *
     * @return                          if this assignment is opaque
     */
    public boolean rvalueIsOpaque() {
        return rvalue != null && rvalue.range != null;
    }
    /**
     * This implementation returns true if <code>rvalueIsOpaque()</code>
     * or <code>resultIsOpaque()</code> returns true.<br>
     *
     * If the return value is false, this assignment is assumed "transparent"
     * and thus can be optimized out.<br>
     *
     * @return                          if this assignment is opaque
     */
    public boolean isOpaque() {
        if(rvalueIsOpaque())
            return true;
        return resultIsOpaque();
    }
    @Override
    public Set<RangedCodeValue> getRangedValues() {
        Set<RangedCodeValue> list = super.getRangedValues();
        if(rvalue != null)
            list.add(rvalue);
        return list;
    }
    @Override
    public Set<CodeIndexDereference> getIndexDereferences(boolean alsoWrite) {
        Set<CodeIndexDereference> indexed = super.getIndexDereferences(alsoWrite);
        if(rvalue.value instanceof CodeIndexDereference)
            indexed.add((CodeIndexDereference)rvalue.value);
        return indexed;
    }
    @Override
    public boolean replaceIndexDereference(
            CodeIndexDereference i, CodeConstant c) {
        if(rvalue.value.equals(i)) {
            rvalue.value = c;
            return true;
        }
        return false;
    }
    @Override
    public Set<CodeVariable> getLocalSources() {
        Set<CodeVariable> list =
                super.getLocalSources();
        if(rvalue != null &&
                rvalue.value instanceof AbstractCodeDereference)
            list.addAll(((AbstractCodeDereference)rvalue.value).
                        extractLocalSourcePartsInSource());
        return extractAllLocalBoundSources(list);
    }
    @Override
    public Set<CodeFieldDereference> getFieldSources() {
        Collection<CodeFieldDereference> list =
                super.getFieldSources();
        if(rvalue != null &&
                rvalue.value instanceof AbstractCodeDereference)
            list.addAll(((AbstractCodeDereference)rvalue.value).extractFieldSourcePartsInSource());
        return extractAllFieldBoundSources(list);
    }
    @Override
    public boolean replaceLocalSourceVariable(CodeVariable sourceVariable,
            AbstractCodeValue value) {
        boolean found = super.replaceLocalSourceVariable(sourceVariable, value);
        if(extractPlainLocal(rvalue.value) == sourceVariable) {
            // an exception, see the docs of <code>replaceLocalSourcePartsInTarget</code>
            // for details
            rvalue.value = value;
            found = true;
        } else if(replaceLocalSourcePartsInTarget(rvalue.value, sourceVariable, value))
            found = true;
        if(replaceBounds(sourceVariable, value))
            found = true;
        return found;
    }
    @Override
    public boolean replaceFieldSourceVariable(CodeFieldDereference sourceField,
            AbstractCodeValue value) {
        boolean found = super.replaceFieldSourceVariable(sourceField, value);
        if(sourceField.equals(extractPlainField(rvalue.value))) {
            // an exception, see the docs of <code>replaceFieldSourcePartsInTarget</code>
            // for details
            rvalue.value = value;
            found = true;
        } else if(replaceFieldSourcePartsInTarget(rvalue.value, sourceField, value))
            found = true;
        return found;
    }
    @Override
    public boolean replaceLocalTargetVariable(CodeVariable targetVariable,
            AbstractCodeDereference dereference) {
        boolean replaced = super.replaceLocalTargetVariable(targetVariable,
                dereference);
        return replaced;
    }
    @Override
    public Object accept(Object c, CodeVisitor v)  throws TranslatorException {
        return v.visit(c, this);
    }
    @Override
    public AbstractCodeOp clone() {
       Object o = super.clone();
       CodeOpAssignment out = (CodeOpAssignment)o;
       out.rvalue = rvalue.clone();
       return out;
    }
    @Override
    public String toString(CodeMethod method) {
        String s = super.toString(method);
        s = s + valueName(result) + " = " +
                (rvalue == null ? "<assignment marked as one doing nothing>" :
                    valueName(rvalue));
        return s;
    }
}
