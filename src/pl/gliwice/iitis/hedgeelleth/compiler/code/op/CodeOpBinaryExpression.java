/*
 * CodeOpBinaryExpression.java
 *
 * Created on Mar 30, 2008
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.code.op;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.BinaryExpression;
import pl.gliwice.iitis.hedgeelleth.compiler.code.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * A binary expression operation.
 * 
 * @author Artur Rataj
 */
public class CodeOpBinaryExpression extends AbstractResultCodeOp 
        implements ProbabilisticExpression {
    /**
     * Used by <code>getNewSingleEvaluationId()</code>.
     */
    static long nextSingleEvaluationId = 0;
    /**
     * If an expression that should be evaluated only a single time, e. g.
     * because it contains a field that could be modified by a concurrent thread,
     * is copied into multiple expressions, as e. g. in <code>ConstantIndexing</code>,
     * then all these instances should share this id. Once the automaton is constructed,
     * it should be assured, that all expressions with a common id exist on edges,
     * that come from a common state.<br>
     * 
     * The default of -1 means no id.<br>
     * 
     * To get a new, unique value, use <code>getNewSingleEvaluationId()</code>.
     * 
     */
    public long singleEvaluationId = -1;
    
    /**
     * Left value.
     */
    public RangedCodeValue left;
    /**
     * Right value.
     */
    public RangedCodeValue right;
    
    /**
     * Creates a new instance of CodeOpBinaryExpression,
     * without a label.
     * 
     * @param op                        operation
     * //param type                      operation type
     * @param pos                       position in the parsed stream
     */
    public CodeOpBinaryExpression(Op op, /*Type type, */StreamPos pos) {
        super(op, pos);
        switch(op) {
            case BINARY_AND:
            case BINARY_DIVIDE:
            case BINARY_EQUAL:
            case BINARY_EXCLUSIVE_OR:
            case BINARY_INCLUSIVE_OR:
            case BINARY_INEQUAL:
            case BINARY_LESS:
            case BINARY_LESS_OR_EQUAL:
            case BINARY_MINUS:
            case BINARY_MODULUS:
            case BINARY_MODULUS_POS:
            case BINARY_MULTIPLY:
            case BINARY_PLUS:
            case BINARY_SHIFT_LEFT:
            case BINARY_SHIFT_RIGHT:
            case BINARY_UNSIGNED_SHIFT_RIGHT:
                break;

            default:
                throw new RuntimeException("invalid operator for binary expression: " + op.toString());
        }
    }
    public static synchronized long getNewSingleEvaluationId() {
        return nextSingleEvaluationId++;
    }
    /**
     * Converts a tree binary expression operator to a code
     * operator.<br>
     *
     * The tree operators <code>GREATER</code> and <code>GREATER_OR_EQUAL</code>
     * do not have their code counterparts, thus swapped operators with swapped
     * operands might be used instead.
     * 
     * @param treeOperator              tree binary expression
     *                                  operator
     * @return                          code operator
     */
    public static AbstractCodeOp.Op operatorTreeToCode(BinaryExpression.Op
            treeOperator) {
        AbstractCodeOp.Op nodeOperator;
        switch(treeOperator) {
            case AND:
                nodeOperator = AbstractCodeOp.Op.BINARY_AND;
                break;
                
            case DIVIDE:
                nodeOperator = AbstractCodeOp.Op.BINARY_DIVIDE;
                break;
                
            case EQUAL:
                nodeOperator = AbstractCodeOp.Op.BINARY_EQUAL;
                break;
                
            case EXCLUSIVE_OR:
                nodeOperator = AbstractCodeOp.Op.BINARY_EXCLUSIVE_OR;
                break;

            case INCLUSIVE_OR:
                nodeOperator = AbstractCodeOp.Op.BINARY_INCLUSIVE_OR;
                break;
                
            case INEQUAL:
                nodeOperator = AbstractCodeOp.Op.BINARY_INEQUAL;
                break;
                
            case LESS:
                nodeOperator = AbstractCodeOp.Op.BINARY_LESS;
                break;
                
            case LESS_OR_EQUAL:
                nodeOperator = AbstractCodeOp.Op.BINARY_LESS_OR_EQUAL;
                break;
                
            case MINUS:
                nodeOperator = AbstractCodeOp.Op.BINARY_MINUS;
                break;
                
            case MODULUS:
                nodeOperator = AbstractCodeOp.Op.BINARY_MODULUS;
                break;

            case MODULUS_POS:
                nodeOperator = AbstractCodeOp.Op.BINARY_MODULUS_POS;
                break;
                
            case MULTIPLY:
                nodeOperator = AbstractCodeOp.Op.BINARY_MULTIPLY;
                break;
                
            case PLUS:
                nodeOperator = AbstractCodeOp.Op.BINARY_PLUS;
                break;
                
            case SHIFT_LEFT:
                nodeOperator = AbstractCodeOp.Op.BINARY_SHIFT_LEFT;
                break;
                
            case SHIFT_RIGHT:
                nodeOperator = AbstractCodeOp.Op.BINARY_SHIFT_RIGHT;
                break;
                
            case UNSIGNED_SHIFT_RIGHT:
                nodeOperator = AbstractCodeOp.Op.BINARY_UNSIGNED_SHIFT_RIGHT;
                break;
                
            case CONDITIONAL_AND:
            case CONDITIONAL_OR:
            case GREATER:
            case GREATER_OR_EQUAL:
                throw new RuntimeException("no code counterpart of " +
                        treeOperator + " exists");

            default:
                throw new RuntimeException("unknown operator " +
                        treeOperator);
                
        }
        return nodeOperator;
    }
    /**
     * Converts a code operator to a tree binary expression
     * operator.
     * 
     * @param treeOperator              code operator
     * @return                          tree binary expression
     *                                  operator
     */
    public static BinaryExpression.Op operatorCodeToTree(
            AbstractCodeOp.Op codeOperator) {
        BinaryExpression.Op treeOperator;
        switch(codeOperator) {
            case NONE:
                throw new RuntimeException("Operator NONE can not " +
                        "be converted to a tree one");

            case NESTED_INDEX:
                throw new RuntimeException("Operator NESTED_INDEX can not " +
                        "be converted to a tree one");
                
            case BINARY_PLUS:
                treeOperator = BinaryExpression.Op.PLUS;
                break;
                
            case BINARY_MINUS:
                treeOperator = BinaryExpression.Op.MINUS;
                break;
                
            case BINARY_MULTIPLY:
                treeOperator = BinaryExpression.Op.MULTIPLY;
                break;
                
            case BINARY_DIVIDE:
                treeOperator = BinaryExpression.Op.DIVIDE;
                break;
                
            case BINARY_MODULUS:
                treeOperator = BinaryExpression.Op.MODULUS;
                break;
                
            case BINARY_MODULUS_POS:
                treeOperator = BinaryExpression.Op.MODULUS_POS;
                break;
                
            case BINARY_INCLUSIVE_OR:
                treeOperator = BinaryExpression.Op.INCLUSIVE_OR;
                break;
                
            case BINARY_EXCLUSIVE_OR:
                treeOperator = BinaryExpression.Op.EXCLUSIVE_OR;
                break;
                
            case BINARY_AND:
                treeOperator = BinaryExpression.Op.AND;
                break;
                
            /*
            case BINARY_CONDITIONAL_OR:
                treeOperator = BinaryExpression.OperatorType.CONDITIONAL_OR;
                break;
                
            case BINARY_CONDITIONAL_AND:
                treeOperator = BinaryExpression.OperatorType.CONDITIONAL_AND;
                break;
             */
                
            case BINARY_EQUAL:
                treeOperator = BinaryExpression.Op.EQUAL;
                break;
                
            case BINARY_INEQUAL:
                treeOperator = BinaryExpression.Op.INEQUAL;
                break;
                
            case BINARY_LESS:
                treeOperator = BinaryExpression.Op.LESS;
                break;

                /*
            case BINARY_GREATER:
                treeOperator = BinaryExpression.OperatorType.GREATER;
                break;
                 */
                
            case BINARY_LESS_OR_EQUAL:
                treeOperator = BinaryExpression.Op.LESS_OR_EQUAL;
                break;

                /*
            case BINARY_GREATER_OR_EQUAL:
                treeOperator = BinaryExpression.OperatorType.GREATER_OR_EQUAL;
                break;
                 */
                
            case BINARY_SHIFT_LEFT:
                treeOperator = BinaryExpression.Op.SHIFT_LEFT;
                break;
                
            case BINARY_SHIFT_RIGHT:
                treeOperator = BinaryExpression.Op.SHIFT_RIGHT;
                break;
                
            case BINARY_UNSIGNED_SHIFT_RIGHT:
                treeOperator = BinaryExpression.Op.UNSIGNED_SHIFT_RIGHT;
                break;
                
            default:
                throw new RuntimeException("not a binary expression " +
                        "operator: " + codeOperator);

        }
        return treeOperator;
    }
    @Override
    public Set<RangedCodeValue> getRangedValues() {
        Set<RangedCodeValue> list = super.getRangedValues();
        list.add(left);
        list.add(right);
        return list;
    }
    @Override
    public Set<CodeIndexDereference> getIndexDereferences(boolean alsoWrite) {
        Set<CodeIndexDereference> indexed = super.getIndexDereferences(alsoWrite);
        if(left.value instanceof CodeIndexDereference)
            indexed.add((CodeIndexDereference)left.value);
        if(right.value instanceof CodeIndexDereference)
            indexed.add((CodeIndexDereference)right.value);
        return indexed;
    }
    @Override
    public boolean replaceIndexDereference(
            CodeIndexDereference i, CodeConstant c) {
        boolean changed = false;
        if(left.value.equals(i)) {
            left.value = c;
            changed = true;
        }
        if(right.value.equals(i)) {
            right.value = c;
            changed = true;
        }
        return changed;
    }
    @Override
    public Set<CodeVariable> getLocalSources() {
        Set<CodeVariable> list =
                super.getLocalSources();
        if(left.value instanceof AbstractCodeDereference)
            list.addAll(((AbstractCodeDereference)left.value).
                        extractLocalSourcePartsInSource());
        if(right.value instanceof AbstractCodeDereference)
            list.addAll(((AbstractCodeDereference)right.value).
                        extractLocalSourcePartsInSource());
        return extractAllLocalBoundSources(list);
    }
    @Override
    public Set<CodeFieldDereference> getFieldSources() {
        Set<CodeFieldDereference> list =
                super.getFieldSources();
        if(left.value instanceof AbstractCodeDereference)
            list.addAll(((AbstractCodeDereference)left.value).
                        extractFieldSourcePartsInSource());
        if(right.value instanceof AbstractCodeDereference)
            list.addAll(((AbstractCodeDereference)right.value).
                        extractFieldSourcePartsInSource());
        return extractAllFieldBoundSources(list);
    }
    @Override
    public boolean replaceLocalSourceVariable(CodeVariable sourceVariable,
            AbstractCodeValue value) {
        boolean found = super.replaceLocalSourceVariable(sourceVariable,
            value);
        if(extractPlainLocal(left.value) == sourceVariable) {
            // an exception, see the docs of <code>replaceLocalSourcePartsInTarget</code>
            // for details
            left.value = value;
            found = true;
        } else if(replaceLocalSourcePartsInTarget(left.value, sourceVariable, value))
            found = true;
        if(extractPlainLocal(right.value) == sourceVariable) {
            // an exception, see the docs of <code>replaceLocalSourcePartsInTarget</code>
            // for details
            right.value = value;
            found = true;
        } else if(replaceLocalSourcePartsInTarget(right.value, sourceVariable, value))
            found = true;
        if(replaceBounds(sourceVariable, value))
            found = true;
        return found;
    }
    @Override
    public boolean replaceFieldSourceVariable(CodeFieldDereference sourceField,
            AbstractCodeValue value) {
        boolean found = super.replaceFieldSourceVariable(sourceField, value);
        if(sourceField.equals(extractPlainField(left.value))) {
            // an exception, see the docs of <code>replaceFieldSourcePartsInTarget</code>
            // for details
            left.value = value;
            found = true;
        } else if(replaceFieldSourcePartsInTarget(left.value, sourceField,
                value))
            found = true;
        if(sourceField.equals(extractPlainField(right.value))) {
            // an exception, see the docs of <code>replaceFieldSourcePartsInTarget</code>
            // for details
            right.value = value;
            found = true;
        } else if(replaceFieldSourcePartsInTarget(right.value, sourceField, value))
            found = true;
        return found;
    }
    @Override
    public boolean replaceLocalTargetVariable(CodeVariable targetVariable,
            AbstractCodeDereference dereference) {
        boolean replaced = super.replaceLocalTargetVariable(targetVariable,
                dereference);
        return replaced;
    }
    @Override
    public Object accept(Object c, CodeVisitor v)  throws TranslatorException {
        return v.visit(c, this);
    }
    @Override
    public boolean isProbabilistic() {
        return left.value.getResultType().isProbabilistic() ||
                right.value.getResultType().isProbabilistic();
    }
    @Override
    public AbstractCodeOp clone() {
       Object o = super.clone();
       CodeOpBinaryExpression out = (CodeOpBinaryExpression)o;
       out.left = left.clone();
       out.right = right.clone();
       return out;
    }
    @Override
    public String toString(CodeMethod method) {
       String s = super.toString(method);
        String o = BinaryExpression.getOperatorName(
                CodeOpBinaryExpression.operatorCodeToTree(op));
        s = s + valueName(result) + " = " +
                valueName(left) + " " + o + " " +
                valueName(right);
        return s;
    }
}
