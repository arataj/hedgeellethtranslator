/*
 * CodeOpBranch.java
 *
 * Created on Mar 30, 2008
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.code.op;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.code.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * A branch operation. Either a regular jump, or a barrier--related one, that
 * points to the beginning of the barrier's pass conditon. The latter is specially
 * marked to be posssibly specially treated by the backend, e. g. transformed
 * into a jump--less sub--automaton.
 * 
 * @author Artur Rataj
 */
public class CodeOpBranch extends AbstractCodeOp {
    /**
     * Condition.
     */
    public RangedCodeValue condition;
    /**
     * Reference to the label of the operation to jump to if the
     * condition is false. Null means jump to the next operation.
     * If this field is null, then <code>labelRefTrue</code> can
     * not be null.
     */
    public CodeLabel labelRefFalse;
    /**
     * Reference to the label of the operation to jump to if the
     * condition is true. Null means jump to the next operation.
     * If this field is null, then <code>labelRefFalse</code> can
     * not be null.
     */
    public CodeLabel labelRefTrue;
    /**
     * False for a regular branch, true for a barrier's one.
     */
    public boolean barrier;
    
    /**
     * Creates a new instance of CodeOpBranch, without a label.
     * 
     * @param pos                       position in the parsed stream
     * @param barrier                   false for a regular branch, true for a barrier's
     *                                  one
     */
    public CodeOpBranch(StreamPos pos, boolean barrier) {
        super(AbstractCodeOp.Op.BRANCH, /*AbstractCodeOp.Type.TYPE_MIXED,*/
                pos);
        this.barrier = barrier;
    }
    /**
     * Creates a new instance of regular CodeOpBranch.<br>
     * 
     * This is a convenience constructor.
     * 
     * @param pos                       position in the parsed stream
     * @param barrier                   false for a regular branch, true for a barrier's
     *                                  one
     */
    public CodeOpBranch(StreamPos pos) {
        this(pos, false);
    }
    @Override
    public Set<CodeLabel> getLabelRefs() {
        Set<CodeLabel> list = new HashSet<>();
        if(labelRefFalse != null)
            list.add(labelRefFalse);
        if(labelRefTrue != null)
            list.add(labelRefTrue);
        return list;
    }
    @Override
    public Set<RangedCodeValue> getRangedValues() {
        Set<RangedCodeValue> list = super.getRangedValues();
        if(condition != null)
            list.add(condition);
        return list;
    }
    @Override
    public Set<CodeIndexDereference> getIndexDereferences(boolean alsoWrite) {
        Set<CodeIndexDereference> indexed = super.getIndexDereferences(alsoWrite);
        if(condition.value instanceof CodeIndexDereference)
            indexed.add((CodeIndexDereference)condition.value);
        return indexed;
    }
    @Override
    public boolean replaceIndexDereference(
            CodeIndexDereference i, CodeConstant c) {
        if(condition.value.equals(i)) {
            condition.value = c;
            return true;
        }
        return false;
    }
    @Override
    public Set<CodeVariable> getLocalSources() {
        Set<CodeVariable> list =
                super.getLocalSources();
        if(condition != null &&
                condition.value instanceof AbstractCodeDereference)
            list.addAll(((AbstractCodeDereference)condition.value).
                        extractLocalSourcePartsInSource());
        return extractAllLocalBoundSources(list);
    }
    @Override
    public Set<CodeFieldDereference> getFieldSources() {
        Set<CodeFieldDereference> list =
                super.getFieldSources();
        if(condition != null &&
                condition.value instanceof AbstractCodeDereference)
            list.addAll(((AbstractCodeDereference)condition.value).
                        extractFieldSourcePartsInSource());
        return extractAllFieldBoundSources(list);
    }
    @Override
    public boolean replaceLocalSourceVariable(CodeVariable sourceVariable,
            AbstractCodeValue value) {
        boolean found = super.replaceLocalSourceVariable(sourceVariable, value);
        if(extractPlainLocal(condition.value) == sourceVariable) {
            // an exception, see the docs of <code>replaceLocalSourcePartsInTarget</code>
            // for details
            condition.value = value;
            found = true;
        } else if(replaceLocalSourcePartsInTarget(condition.value, sourceVariable,
                value))
            found = true;
        if(replaceBounds(sourceVariable, value))
            found = true;
        return found;
    }
    @Override
    public boolean replaceFieldSourceVariable(CodeFieldDereference sourceField,
            AbstractCodeValue value) {
        boolean found = super.replaceFieldSourceVariable(sourceField, value);
        if(sourceField.equals(extractPlainField(condition.value))) {
            // an exception, see the docs of <code>replaceFieldSourcePartsInTarget</code>
            // for details
            condition.value = value;
            found = true;
        } else if(replaceFieldSourcePartsInTarget(condition.value, sourceField, value))
            found = true;
        return found;
    }
    @Override
    public Object accept(Object c, CodeVisitor v)  throws TranslatorException {
        return v.visit(c, this);
    }
    @Override
    public AbstractCodeOp clone() {
       Object o = super.clone();
        CodeOpBranch out = (CodeOpBranch)o;
        out.condition = condition.clone();
        if(labelRefFalse != null)
            out.labelRefFalse = new CodeLabel(labelRefFalse);
        else
            out.labelRefFalse = null;
        if(labelRefTrue != null)
            out.labelRefTrue = new CodeLabel(labelRefTrue);
        else
            out.labelRefTrue = null;
        out.barrier = barrier;
        return out;
    }
    @Override
    public String toString(CodeMethod method) {
        final String nullLabelString = "<null>";
        String s = super.toString(method);
        s = s + valueName(condition) + " ? " +
                (labelRefTrue != null ? labelRefTrue.toString() : nullLabelString) +
                " : " +
                (labelRefFalse != null ? labelRefFalse.toString() : nullLabelString);
        if(barrier)
            s += " <barrier>";
        return s;
    }
}
