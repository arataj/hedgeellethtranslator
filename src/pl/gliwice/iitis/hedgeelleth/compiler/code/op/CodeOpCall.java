/*
 * CodeOpCall.java
 *
 * Created on Mar 30, 2008
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.code.op;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.code.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Context;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.CallExpression;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * A call operation.
 * 
 * @author Artur Rataj
 */
public class CodeOpCall extends AbstractCodeOpInvocation {
    /**
     * Method, not always the same as the called method, because
     * of possible overridings. Use getCalled(CodeClass) to get
     * the called method.
     */
    public CodeMethodReference methodReference;
    /**
     * If this is a direct call of a non--static method, so in effect,
     * no runtime call resolution applies to this call even if it
     * has the argument "this".<br>
     * 
     * Only in calls of non--static methods this field can have true value.,
     * despite the static calls being always direct.<br>
     *
     * See the documentation of <code>isStatic</code> for details when a call
     * is virtual.
     * 
     * A call may become direct within a runtime call resolution.
     *
     * @see #isStatic
     */
    public boolean directNonStatic = false;
    
    /**
     * Creates a new instance of CodeOpCall, without a label, indirect.
     * 
     * @param pos                       position in the parsed stream
     */
    public CodeOpCall(StreamPos pos) {
        super(AbstractCodeOp.Op.CALL, /*AbstractCodeOp.Type.TYPE_MIXED,*/
                pos);
        /*
        if(pos.line == 26 && pos.column == 19)
            pos = pos;
            */
    }
    @Override
    public Set<CodeVariable> getLocalSources() {
        Set<CodeVariable> list = super.getLocalSources();
        return extractAllLocalBoundSources(list);
    }
    @Override
    public Set<CodeFieldDereference> getFieldSources() {
        Set<CodeFieldDereference> list =
                super.getFieldSources();
        return extractAllFieldBoundSources(list);
    }
    @Override
    public boolean replaceLocalSourceVariable(CodeVariable sourceVariable,
            AbstractCodeValue value) {
        boolean found = super.replaceLocalSourceVariable(sourceVariable, value);
        if(replaceBounds(sourceVariable, value))
            found = true;
        return found;
    }
    @Override
    public boolean replaceFieldSourceVariable(CodeFieldDereference sourceField,
            AbstractCodeValue value) {
        boolean found = super.replaceFieldSourceVariable(sourceField, value);
        return found;
    }
    @Override
    public boolean replaceLocalTargetVariable(CodeVariable targetVariable,
            AbstractCodeDereference dereference) {
        boolean replaced = super.replaceLocalTargetVariable(targetVariable,
                dereference);
        return replaced;
    }
    @Override
    public boolean isStatic() {
        boolean statiC;
        // at least one of firstPassMethod and method fields must be
        // not null
        if(methodReference.firstPassMethod != null)
            statiC = methodReference.firstPassMethod.flags.context != Context.NON_STATIC;
        else
            statiC = methodReference.method.context != Context.NON_STATIC;
        return statiC;
    }
    /**
     * If this call is virtual, that is, if it is neither an invocation
     * of a static method nor a direct call of a non--static method.
     *
     * @return                          if this invocation is virtual.
     */
    public boolean isVirtual() {
        return !isStatic() && !directNonStatic;
    }
    /**
     * Returns the called method, on basis of the class of
     * the runtime object of the method, and on whether this call
     * is virtual.<br>
     * 
     * Takes the runtime type as its argument.
     * 
     * @param runtimeType               runtime class of the called
     *                                  method object
     * @return                          method to call
     */
    public CodeMethod getCalled(CodeClass runtimeType) {
        if(isVirtual())
            return methodReference.method.getVirtual(runtimeType);
        else
            return methodReference.method;
    }
    @Override
    public Object accept(Object c, CodeVisitor v)  throws TranslatorException {
        return v.visit(c, this);
    }
    @Override
    public AbstractCodeOp clone() {
        Object o = super.clone();
        CodeOpCall out = (CodeOpCall)o;
        out.methodReference = new CodeMethodReference(methodReference);
        out.directNonStatic = directNonStatic;
        return out;
    }
    @Override
    public String toString(CodeMethod method) {
        String s = super.toString(method);
        s = s + valueName(result) + " = " +
                valueName(methodReference) + " ";
        s += "(" + argsToString(method) + ")";
        if(directNonStatic)
            s += " [DIRECT]";
        return s;
    }
}
