/*
 * CodeOpFinalDeclaration.java
 *
 * Created on Aug 30, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.code.op;

import pl.gliwice.iitis.hedgeelleth.compiler.code.CodeMethod;
import pl.gliwice.iitis.hedgeelleth.compiler.code.CodeVariable;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.StreamPos;

/**
 * A code equivalent of <code>FinalLocalDeclaration</code>.
 * Used only by <code>TraceValues.checkInitializedFinals()</code>,
 * then optimized out.
 * 
 * @author Artur Rataj
 */
public class CodeOpFinalDeclaration extends CodeOpNone {
    /**
     * The local final variable declared.
     */
    public CodeVariable variable;
    
    /**
     * Creates a new marker of a declaration of a final local.
     *
     * @param pos                       position in the parsed stream
     * @param variable                  variable declared
     */
    public CodeOpFinalDeclaration(StreamPos pos, CodeVariable variable) {
        super(pos);
        this.variable = variable;
    }
    @Override
    public String toString(CodeMethod method) {
        String s = super.toString(method);
        return s + " declared final " + variable.toNameString();
    }
}
