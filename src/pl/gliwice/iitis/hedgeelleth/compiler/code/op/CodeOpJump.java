/*
 * CodeOpJump.java
 *
 * Created on Mar 30, 2008
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.code.op;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.code.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.CodeFieldDereference;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * A jump operation.
 * 
 * @author Artur Rataj
 */
public class CodeOpJump extends AbstractCodeOp {
    /**
     * Reference to the label of the operation to jump to.
     */
    public CodeLabel gotoLabelRef;
    
    /**
     * Creates a new instance of CodeOpJump, without a label.
     * 
     * @param pos                       position in the parsed stream
     */
    public CodeOpJump(StreamPos pos) {
        super(AbstractCodeOp.Op.JUMP, /*AbstractCodeOp.Type.TYPE_MIXED,*/
                pos);
    }
    @Override
    public Set<CodeLabel> getLabelRefs() {
        Set<CodeLabel> list = new HashSet<>();
        list.add(gotoLabelRef);
        return list;
    }
    @Override
    public Set<CodeVariable> getLocalSources() {
        Set<CodeVariable> list = super.getLocalSources();
        return extractAllLocalBoundSources(list);
    }
    @Override
    public Set<CodeFieldDereference> getFieldSources() {
        Set<CodeFieldDereference> list =
                super.getFieldSources();
        return extractAllFieldBoundSources(list);
    }
    @Override
    public boolean replaceLocalSourceVariable(CodeVariable sourceVariable,
            AbstractCodeValue value) {
        boolean found = super.replaceLocalSourceVariable(sourceVariable, value);
        if(replaceBounds(sourceVariable, value))
            found = true;
        return found;
    }
    @Override
    public boolean replaceFieldSourceVariable(CodeFieldDereference sourceField,
            AbstractCodeValue value) {
        boolean found = super.replaceFieldSourceVariable(sourceField, value);
        return found;
    }
    @Override
    public Object accept(Object c, CodeVisitor v)  throws TranslatorException {
        return v.visit(c, this);
    }
    @Override
    public AbstractCodeOp clone() {
       Object o = super.clone();
       CodeOpJump out = (CodeOpJump)o;
       out.gotoLabelRef = new CodeLabel(gotoLabelRef);
       return out;
    }
    @Override
    public String toString(CodeMethod method) {
        String s = super.toString(method);
        s = s + "goto " + gotoLabelRef.toString();
        return s;
    }
}
