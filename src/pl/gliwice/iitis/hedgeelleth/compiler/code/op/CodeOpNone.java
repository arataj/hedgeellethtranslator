/*
 * CodeOpNone.java
 *
 * Created on Mar 30, 2008
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.code.op;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.CommentTag;
import pl.gliwice.iitis.hedgeelleth.compiler.CompilerAnnotation;
import pl.gliwice.iitis.hedgeelleth.compiler.code.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.*;
import pl.gliwice.iitis.hedgeelleth.compiler.primitiverange.CodeVariablePrimitiveRange;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * An operation that does nothing but possibly holding annotations,
 * arguments and results. Also used temporarily to hold labels.<br>
 * 
 * If does not have any annotations, can be optimized out.<br>
 * 
 * The methods for returning or replacing dereferences always
 * return nothing, as the meaning of these dereferences is
 * not defined in this class.<br>
 * 
 * Arguments are usually inherited from some other operation.
 * Empty list for none.
 *
 * @author Artur Rataj
 */
public class CodeOpNone extends AbstractCodeOpInvocation {
    /**
     * Annotations, empty collection if none. Sorted only for a deterministic
     * compiler output.<br>
     * 
     * Operations with no annotations can
     * be optimized out as ones that do nothing.
     */
    public SortedSet<String> annotations;
    /**
     * If this invocation is static. If not, the first argument
     * should be "this". If this operation replaces a method,
     * this trait is usually inherited from replaced method.
     */
    boolean statiC;
    /**
     * If this operation stems from a call to a method, then this is the method.
     * Otherwise null.
     */
    public CodeMethod calledMethod;
    
    /**
     * Creates a new instance of CodeOpNone, without a label.<br>
     *
     * This constructor is usually used for operations that are label
     * or anotation holders.
     *
     * @param pos                       position in the parsed stream
     */
    public CodeOpNone(StreamPos pos) {
        super(AbstractCodeOp.Op.NONE, /*AbstractCodeOp.Type.TYPE_MIXED,*/
                pos);
        annotations = new TreeSet<>();
        result = null;
    }
    /**
     * Creates a new instance of CodeOpNone, that inherites
     * method annotations, arguments, being static or not,
     * and result from a call operation.<br>
     *
     * This constructor is usually used for operations that
     * replace marker methods.
     *
     * @param call                      call operation whose traits
     *                                  to inherit
     * @param pos                       position in the parsed stream
     */
    public CodeOpNone(CodeOpCall call, StreamPos pos) {
        this(pos);
        annotations = new TreeSet<>(call.methodReference.
                method.annotations);
        args = new ArrayList<>(call.args);
        preArgs = new ArrayList<>(call.preArgs);
        statiC = call.isStatic();
        result = call.getResult();
        calledMethod = call.methodReference.method;
    }
    @Override
    public void filterTags() {
        for(CommentTag ct : new LinkedList<>(tags))
            for(String a : ct.annotationsRequired)
                if(!annotations.contains(a))
                    tags.remove(ct);
    }
    @Override
    public Set<CodeVariable> getLocalSources() {
        Set<CodeVariable> list = super.getLocalSources();
        return extractAllLocalBoundSources(list);
    }
    @Override
    public Set<CodeFieldDereference> getFieldSources() {
        Set<CodeFieldDereference> list =
                super.getFieldSources();
        return extractAllFieldBoundSources(list);
    }
    @Override
    public boolean replaceLocalSourceVariable(CodeVariable sourceVariable,
            AbstractCodeValue value) {
        boolean found = super.replaceLocalSourceVariable(sourceVariable, value);
        if(replaceBounds(sourceVariable, value))
            found = true;
        return found;
    }
    @Override
    public boolean replaceFieldSourceVariable(CodeFieldDereference sourceField,
            AbstractCodeValue value) {
        boolean found = super.replaceFieldSourceVariable(sourceField, value);
        return found;
    }
    @Override
    public boolean replaceLocalTargetVariable(CodeVariable targetVariable,
            AbstractCodeDereference dereference) {
        boolean replaced = super.replaceLocalTargetVariable(targetVariable,
                dereference);
        return replaced;
    }
    @Override
    public boolean isStatic() {
        return statiC;
    }
    @Override
    public Object accept(Object c, CodeVisitor v)  throws TranslatorException {
        return v.visit(c, this);
    }
    /**
     * If this operation represent a barrier, then its barrier type is
     * replaced with a given one. Otherwise, a runtime exception is
     * thrown.
     * 
     * @return a compiler annotation representing a given barrier type
     */
    public void setBarrierType(CompilerAnnotation compilerAnnotation) {
        CompilerAnnotation current = getBarrierType();
        if(current == null)
            throw new RuntimeException("not a barrier");
        annotations.remove(current.toAnnotationString());
        annotations.add(compilerAnnotation.toAnnotationString());
    }
    /**
     * If this operation represent a barrier, then a respective barrier type is
     * returned.
     * 
     * @return a compiler annotation representing a given barrier type, or null
     */
    public CompilerAnnotation getBarrierType() {
        return CompilerAnnotation.getBarrierType(annotations);
    }
//    @Override
//    public boolean isError() {
//        return annotations.contains(CompilerAnnotation.ERROR_STRING);
//    }
    /**
     * Returns the index of exclusive bound argument of <code>CodeOpNone</code> representing
     * a call to @RANDOM. The index is not fixed because Java's </code>Random.nextInt()</code> is not
     * static, while Verics' <code>unk()</code> is.
     */
    public int getRandomBoundArgIndex() {
        if(args.size() == 0)
            throw new RuntimeException("arguments missing");
        if(args.get(0).value.getResultType().isJava()) {
            if(args.size() == 1)
                throw new RuntimeException("bound missing");
            return 1;
        } else
            return 0;
    }
    @Override
    public AbstractCodeOp clone() {
        Object o = super.clone();
        CodeOpNone out = (CodeOpNone)o;
        out.annotations = new TreeSet<String>(annotations);
        out.statiC = statiC;
        return out;
    }
    @Override
    public String toString(CodeMethod method) {
        String s = super.toString(method);
        s = s + "no op";
        if(result != null)
            s += " " + valueName(result) + " =";
        for(String a : annotations)
            s += " @" + a;
        s += " (" + argsToString(method) + ")";
        return s;
    }
}
