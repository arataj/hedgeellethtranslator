/*
 * CodeOpReturn.java
 *
 * Created on Mar 30, 2008
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.code.op;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.code.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * A return operation. If the method is not void, then
 * this operation contains a return value, which is, though,
 * global for the method and not replaceable.<br>
 * 
 * See the documentation of <code>CodeMethod</code> for details.
 * 
 * @author Artur Rataj
 */
public class CodeOpReturn extends AbstractCodeOp {
    /**
     * This is a reference to the local variable
     * <code>CodeMethod.returnValue</code>. An attempt to replace
     * it throws a runtime exception.
     */
    public AbstractCodeDereference returnValue;
    /**
     * True if this operation was added by the compiler.
     */
    public boolean internal = false;
    
    /**
     * Creates a new instance of CodeOpReturn, without a label.
     * 
     * @param pos                       position in the parsed stream
     */
    public CodeOpReturn(StreamPos pos) {
        super(AbstractCodeOp.Op.RETURN,
                /*AbstractCodeOp.Type.TYPE_MIXED, */pos);
    }
    @Override
    public Set<CodeVariable> getLocalSources() {
        Set<CodeVariable> list = super.getLocalSources();
        if(returnValue != null)
            list.addAll(returnValue.extractLocalSourcePartsInSource());
        return extractAllLocalBoundSources(list);
    }
    @Override
    public Set<CodeIndexDereference> getIndexDereferences(boolean alsoWrite) {
        Set<CodeIndexDereference> indexed = super.getIndexDereferences(alsoWrite);
        if(returnValue instanceof CodeIndexDereference)
            throw new RuntimeException("indexed return value");
        return indexed;
    }
    @Override
    public boolean replaceIndexDereference(
            CodeIndexDereference i, CodeConstant c) {
        if(returnValue.equals(i))
            throw new RuntimeException("indexed return value");
        return false;
    }
    @Override
    public Set<CodeFieldDereference> getFieldSources() {
        Set<CodeFieldDereference> list =
                super.getFieldSources();
        if(returnValue != null &&
                !returnValue.extractFieldSourcePartsInSource().isEmpty())
            throw new RuntimeException("return value not a local");
        return extractAllFieldBoundSources(list);
    }
    @Override
    public boolean replaceLocalSourceVariable(CodeVariable sourceVariable,
            AbstractCodeValue value) {
        boolean found = super.replaceLocalSourceVariable(sourceVariable, value);
        if(extractPlainLocal(returnValue) == sourceVariable)
            throw new RuntimeException("return value can not be replaced");
        if(replaceBounds(sourceVariable, value))
            found = true;
        return found;
    }
    @Override
    public boolean replaceFieldSourceVariable(CodeFieldDereference sourceField,
            AbstractCodeValue value) {
        boolean found = super.replaceFieldSourceVariable(sourceField, value);
        if(extractPlainLocal(returnValue) == null)
            throw new RuntimeException("return value not a local");
        return found;
    }
    @Override
    public Object accept(Object c, CodeVisitor v)  throws TranslatorException {
        return v.visit(c, this);
    }
    @Override
    public String toString(CodeMethod method) {
        String s = super.toString(method);
        s = s + "return";
        return s;
    }
}
