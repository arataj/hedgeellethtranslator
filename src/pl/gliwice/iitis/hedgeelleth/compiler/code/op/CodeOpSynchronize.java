/*
 * CodeOpSynchronize.java
 *
 * Created on Apr 18, 2008
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.code.op;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.code.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * A synchronize operation.
 * 
 * @author Artur Rataj
 */
public class CodeOpSynchronize extends AbstractCodeOp {
    /**
     * Lock, not null only if <code>begin</code> is true, as it is
     * the only case when the dereference is guaranteed to point
     * to a correct runtime variable.
     */
    public AbstractCodeValue lock;
    /**
     * True if at the beginning of the synchronized code,
     * false if at the end of the code.
     */
    public boolean begin;
    
    /**
     * Creates a new instance of CodeOpReturn, without a label.
     * 
     * @param pos                       position in the parsed stream
     */
    public CodeOpSynchronize(StreamPos pos) {
        super(AbstractCodeOp.Op.SYNCHRONIZE,
                /*AbstractCodeOp.Type.TYPE_MIXED, */pos);
    }
    @Override
    public Set<CodeVariable> getLocalSources() {
        Set<CodeVariable> list =
                super.getLocalSources();
        if(lock instanceof AbstractCodeDereference)
            list.addAll(((AbstractCodeDereference)lock).extractLocalSourcePartsInSource());
        return extractAllLocalBoundSources(list);
    }
    @Override
    public Set<CodeIndexDereference> getIndexDereferences(boolean alsoWrite) {
        Set<CodeIndexDereference> indexed = super.getIndexDereferences(alsoWrite);
        if(lock instanceof CodeIndexDereference)
            indexed.add((CodeIndexDereference)lock);
        return indexed;
    }
    @Override
    public boolean replaceIndexDereference(
            CodeIndexDereference i, CodeConstant c) {
        if(lock.equals(i)) {
            lock = c;
            return true;
        }
        return false;
    }
    public Set<CodeFieldDereference> getFieldSources() {
        Set<CodeFieldDereference> list =
                super.getFieldSources();
        if(lock instanceof AbstractCodeDereference)
            list.addAll(((AbstractCodeDereference)lock).extractFieldSourcePartsInSource());
        return extractAllFieldBoundSources(list);
    }
    @Override
    public boolean replaceLocalSourceVariable(CodeVariable sourceVariable,
            AbstractCodeValue value) {
        boolean found = super.replaceLocalSourceVariable(sourceVariable, value);
        if(extractPlainLocal(lock) == sourceVariable) {
            // an exception, see the docs of <code>replaceLocalSourcePartsInTarget</code>
            // for details
            lock = value;
            found = true;
        } else if(replaceLocalSourcePartsInTarget(lock, sourceVariable, value))
            found = true;
        if(replaceBounds(sourceVariable, value))
            found = true;
        return found;
    }
    @Override
    public boolean replaceFieldSourceVariable(CodeFieldDereference sourceField,
            AbstractCodeValue value) {
        boolean found = super.replaceFieldSourceVariable(sourceField, value);
        if(sourceField.equals(extractPlainField(lock))) {
            // an exception, see the docs of <code>replaceFieldSourcePartsInTarget</code>
            // for details
            lock = value;
            found = true;
        } else if(replaceFieldSourcePartsInTarget(lock, sourceField,
                value))
            found = true;
        return found;
    }
    @Override
    public Object accept(Object c, CodeVisitor v)  throws TranslatorException {
        return v.visit(c, this);
    }
    @Override
    public AbstractCodeOp clone() {
        Object o = super.clone();
        CodeOpSynchronize out = (CodeOpSynchronize)o;
        if(lock != null)
            out.lock = lock.clone();
        return out;
    }
    @Override
    public String toString(CodeMethod method) {
        String s = super.toString(method);
        s = s + "sync ";
        if(begin)
            s += "begin";
        else
            s += "end";
        s += " " + valueName(lock);
        return s;
    }
}
