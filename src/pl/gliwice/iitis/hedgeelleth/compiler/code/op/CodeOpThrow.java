/*
 * CodeOpThrow.java
 *
 * Created on May 9, 2011
 *
 * Copyright (c) 2011 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.code.op;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.code.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * A throw operation. Note that catching exceptions is not implemented.
 * 
 * @author Artur Rataj
 */
public class CodeOpThrow extends AbstractCodeOp {
    /**
     * The thrown object, null for none.
     */
    public AbstractCodeValue thrown;
    /**
     *<p> If not null, this operation must be dead code before a PTA is
     * constructed, or a respective static check error is reported.</p>
     * 
     * <p>This error should not be modified by code which presents error
     * messages to the user, so use a copying constructor in such cases.</p>
     */
    public CompilerException staticCheckError;
    
    /**
     * Creates a new instance of CodeOpThrow, without a label.
     * 
     * @param pos                       position in the parsed stream
     */
    public CodeOpThrow(StreamPos pos) {
        super(AbstractCodeOp.Op.THROW,
                /*AbstractCodeOp.Type.TYPE_MIXED, */pos);
        staticCheckError = null;
    }
    @Override
    public Set<CodeVariable> getLocalSources() {
        Set<CodeVariable> list =
                super.getLocalSources();
        if(thrown instanceof AbstractCodeDereference)
            list.addAll(((AbstractCodeDereference)thrown).
                    extractLocalSourcePartsInSource());
        return extractAllLocalBoundSources(list);
    }
    @Override
    public Set<CodeIndexDereference> getIndexDereferences(boolean alsoWrite) {
        Set<CodeIndexDereference> indexed = super.getIndexDereferences(alsoWrite);
        if(thrown instanceof CodeIndexDereference)
            indexed.add((CodeIndexDereference)thrown);
        return indexed;
    }
    @Override
    public boolean replaceIndexDereference(
            CodeIndexDereference i, CodeConstant c) {
        if(thrown.equals(i)) {
            thrown = c;
            return true;
        }
        return false;
    }
    @Override
    public Set<CodeFieldDereference> getFieldSources() {
        Set<CodeFieldDereference> list =
                super.getFieldSources();
        if(thrown  instanceof CodeIndexDereference)
            list.addAll(((AbstractCodeDereference)thrown).
                    extractFieldSourcePartsInSource());
        return extractAllFieldBoundSources(list);
    }
    @Override
    public boolean replaceLocalSourceVariable(CodeVariable sourceVariable,
            AbstractCodeValue value) {
        boolean found = super.replaceLocalSourceVariable(sourceVariable, value);
        if(thrown != null) {
            if(extractPlainLocal(thrown) == sourceVariable) {
                // an exception, see the docs of <code>replaceLocalSourcePartsInTarget</code>
                // for details
                thrown = value;
                found = true;
            } else if(replaceLocalSourcePartsInTarget(thrown, sourceVariable, value))
                found = true;
        }
        if(replaceBounds(sourceVariable, value))
            found = true;
        return found;
    }
    @Override
    public boolean replaceFieldSourceVariable(CodeFieldDereference sourceField,
            AbstractCodeValue value) {
        boolean found = super.replaceFieldSourceVariable(sourceField, value);
        if(thrown != null) {
            if(sourceField.equals(extractPlainField(thrown))) {
                // an exception, see the docs of <code>replaceFieldSourcePartsInTarget</code>
                // for details
                if(value instanceof AbstractCodeDereference) {
                    AbstractCodeDereference d = (AbstractCodeDereference)value;
                    thrown = d;
                    found = true;
                } else
                    throw new RuntimeException("thrown object can not be replaced " +
                            "by a constant");
            } else if(replaceFieldSourcePartsInTarget(thrown, sourceField,
                    value))
                found = true;
        }
        return found;
    }
    @Override
    public Object accept(Object c, CodeVisitor v)  throws TranslatorException {
        return v.visit(c, this);
    }
    @Override
    public AbstractCodeOp clone() {
        Object o = super.clone();
        CodeOpThrow out = (CodeOpThrow)o;
        if(thrown != null)
            out.thrown = thrown.clone();
        return out;
    }
    @Override
    public String toString(CodeMethod method) {
        String s = super.toString(method);
        s = s + "throw";
        if(thrown != null)
            s += " " + valueName(thrown);
        if(staticCheckError != null)
            s += " " + staticCheckError.getReports().iterator().next().message;
        return s;
    }
}
