/*
 * CodeOpUnnaryExpression.java
 *
 * Created on Mar 30, 2008
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.code.op;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.code.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Type;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.UnaryExpression;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * An unary expression operation.
 * 
 * @author Artur Rataj
 */
public class CodeOpUnaryExpression extends AbstractResultCodeOp
        implements ProbabilisticExpression {
    /**
     * Subexpression value.
     */
    public RangedCodeValue sub;
    /**
     * Type to which the subexpression is casted if
     * <code>op == UNARY_CAST</code>, type to which object's
     * type is compared if
     * <code>op == UNARY_INSTANCE_OF</code>, for
     * other operators null.
     */
    public Type objectType;
    
    /**
     * Creates a new instance of CodeOpUnaryExpression,
     * without a label.
     * 
     * @param op                        operation
     * @param castType cast type, not null only if <code>op == UNARY_CAST</code>
     * <code>op == UNARY_INSTANCE_OF</code>
     * 
     * @param pos                       position in the parsed stream
     */
    public CodeOpUnaryExpression(Op op, Type castType, StreamPos pos) {
        super(op, pos);
        switch(op) {
            case UNARY_BITWISE_COMPLEMENT:
            case UNARY_CAST:
            case UNARY_CONDITIONAL_NEGATION:
            case UNARY_INSTANCE_OF:
            case UNARY_INSTANCE_OF_NOT_NULL:
            case UNARY_NEGATION:
            case UNARY_ABS:
                break;

            default:
                throw new RuntimeException("invalid operator for unary expression: " + op.toString());
        }
        this.objectType = castType;
//if(op == AbstractCodeOp.Op.UNARY_INSTANCE_OF)
//    op = op;
    }
    /**
     * Converts a tree unary expression operator to a code
     * operator.
     * 
     * @param treeOperator              tree unary expression
     *                                  operator
     * @return                          code operator
     */
    public static AbstractCodeOp.Op operatorTreeToCode(UnaryExpression.Op
            treeOperator) {
        AbstractCodeOp.Op nodeOperator;
        switch(treeOperator) {
            case NONE:
                nodeOperator = AbstractCodeOp.Op.NONE;
                break;
                
            case NEGATION:
                nodeOperator = AbstractCodeOp.Op.UNARY_NEGATION;
                break;

            case BITWISE_COMPLEMENT:
                nodeOperator = AbstractCodeOp.Op.UNARY_BITWISE_COMPLEMENT;
                break;
                
            case CONDITIONAL_NEGATION:
                nodeOperator = AbstractCodeOp.Op.UNARY_CONDITIONAL_NEGATION;
                break;

            case CAST:
                nodeOperator = AbstractCodeOp.Op.UNARY_CAST;
                break;

            case INSTANCE_OF:
                nodeOperator = AbstractCodeOp.Op.UNARY_INSTANCE_OF;
                break;

            /*
            case POST_DECREMENT:
                nodeOperator = CodeOp.Op.UNARY_POST_DECREMENT;
                break;
                
            case POST_INCREMENT:
                nodeOperator = CodeOp.Op.UNARY_POST_INCREMENT;
                break;
                
            case PRE_DECREMENT:
                nodeOperator = CodeOp.Op.UNARY_PRE_DECREMENT;
                break;
                
            case PRE_INCREMENT:
                nodeOperator = CodeOp.Op.UNARY_PRE_INCREMENT;
                break;
             */
                
            default:
                throw new RuntimeException("unknown operator " +
                        treeOperator);
                
        }
        return nodeOperator;
    }
    /**
     * Converts a code operator to a tree unary expression
     * operator.
     * 
     * @param treeOperator              code operator
     * @return                          tree unary expression
     *                                  operator
     */
    public static UnaryExpression.Op operatorCodeToTree(
            AbstractCodeOp.Op codeOperator) {
        UnaryExpression.Op treeOperator;
        switch(codeOperator) {
            case NONE:
                treeOperator = UnaryExpression.Op.NONE;
                break;
                
            case UNARY_NEGATION:
                treeOperator = UnaryExpression.Op.NEGATION;
                break;
                
            case UNARY_BITWISE_COMPLEMENT:
                treeOperator = UnaryExpression.Op.BITWISE_COMPLEMENT;
                break;

            case UNARY_CAST:
                treeOperator = UnaryExpression.Op.CAST;
                break;

            case UNARY_INSTANCE_OF:
                treeOperator = UnaryExpression.Op.INSTANCE_OF;
                break;

            case UNARY_INSTANCE_OF_NOT_NULL:
                treeOperator = UnaryExpression.Op.INSTANCE_OF_NOT_NULL;
                break;

            case UNARY_CONDITIONAL_NEGATION:
                treeOperator = UnaryExpression.Op.CONDITIONAL_NEGATION;
                break;

            case UNARY_ABS:
                throw new RuntimeException("no equivalent");
            /*    
            case UNARY_PRE_INCREMENT:
                treeOperator = UnaryExpression.OperatorType.PRE_INCREMENT;
                break;
                
            case UNARY_PRE_DECREMENT:
                treeOperator = UnaryExpression.OperatorType.PRE_DECREMENT;
                break;
                
            case UNARY_POST_INCREMENT:
                treeOperator = UnaryExpression.OperatorType.POST_INCREMENT;
                break;
                
            case UNARY_POST_DECREMENT:
                treeOperator = UnaryExpression.OperatorType.POST_DECREMENT;
                break;
             */
                
            default:
                throw new RuntimeException("not an unary expression " +
                        "operator: " + codeOperator);

        }
        return treeOperator;
    }
    @Override
    public Set<RangedCodeValue> getRangedValues() {
        Set<RangedCodeValue> list = super.getRangedValues();
        list.add(sub);
        return list;
    }
    @Override
    public Set<CodeIndexDereference> getIndexDereferences(boolean alsoWrite) {
        Set<CodeIndexDereference> indexed = super.getIndexDereferences(alsoWrite);
        if(sub.value instanceof CodeIndexDereference)
            indexed.add((CodeIndexDereference)sub.value);
        return indexed;
    }
    @Override
    public boolean replaceIndexDereference(
            CodeIndexDereference i, CodeConstant c) {
        boolean changed = false;
        if(sub.value.equals(i)) {
            sub.value = c;
            changed = true;
        }
        return changed;
    }
    @Override
    public Set<CodeVariable> getLocalSources() {
        Set<CodeVariable> list =
                super.getLocalSources();
        if(sub.value instanceof AbstractCodeDereference)
            list.addAll(((AbstractCodeDereference)sub.value).
                        extractLocalSourcePartsInSource());
        return extractAllLocalBoundSources(list);
    }
    @Override
    public Set<CodeFieldDereference> getFieldSources() {
        Set<CodeFieldDereference> list =
                super.getFieldSources();
        if(sub.value instanceof AbstractCodeDereference)
            list.addAll(((AbstractCodeDereference)sub.value).
                        extractFieldSourcePartsInSource());
        return extractAllFieldBoundSources(list);
    }
    @Override
    public boolean replaceLocalSourceVariable(CodeVariable sourceVariable,
            AbstractCodeValue value) {
        boolean found = super.replaceLocalSourceVariable(sourceVariable, value);
        if(extractPlainLocal(sub.value) == sourceVariable) {
            // an exception, see the docs of <code>replaceLocalSourcePartsInTarget</code>
            // for details
            sub.value = value;
            found = true;
        } else if(replaceLocalSourcePartsInTarget(sub.value, sourceVariable, value))
            found = true;
        if(replaceBounds(sourceVariable, value))
            found = true;
        return found;
    }
    @Override
    public boolean replaceFieldSourceVariable(CodeFieldDereference sourceField,
            AbstractCodeValue value) {
        boolean found = super.replaceFieldSourceVariable(sourceField, value);
        if(sourceField.equals(extractPlainField(sub.value))) {
            // an exception, see the docs of <code>replaceFieldSourcePartsInTarget</code>
            // for details
            sub.value = value;
            found = true;
        } else if(replaceFieldSourcePartsInTarget(sub.value, sourceField, value))
            found = true;
        return found;
    }
    @Override
    public boolean replaceLocalTargetVariable(CodeVariable targetVariable,
            AbstractCodeDereference dereference) {
        boolean replaced = super.replaceLocalTargetVariable(targetVariable,
                dereference);
        return replaced;
    }
    @Override
    public Object accept(Object c, CodeVisitor v)  throws TranslatorException {
        return v.visit(c, this);
    }
    @Override
    public boolean isProbabilistic() {
        return sub.value.getResultType().isProbabilistic();
    }
    @Override
    public AbstractCodeOp clone() {
        Object o = super.clone();
        CodeOpUnaryExpression out = (CodeOpUnaryExpression)o;
        if(objectType == null)
            out.objectType = null;
        else
            out.objectType = new Type(objectType);
        out.sub =  sub.clone();
        return out;
    }
    @Override
    public String toString(CodeMethod method) {
        String s = super.toString(method);
        String[] o = UnaryExpression.getOperatorName(
                    operatorCodeToTree(op), objectType);
        s = s + valueName(result) + " = " +
                o[0] + valueName(sub) + o[1];
        return s;
    }
}
