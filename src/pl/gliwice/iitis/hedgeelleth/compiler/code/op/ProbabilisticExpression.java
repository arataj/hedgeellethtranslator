/*
 * ProbabilisticExpression.java
 *
 * Created on Nov 30, 2014
 *
 * Copyright (c) 2014  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.code.op;

/**
 * If this is a probabilistic expression. During a static abalysis, a probabilistic expression
 * can never be evaluated to a constant.
 * 
 * @author Artur Rataj
 */
public interface ProbabilisticExpression {
    /**
     * If this expression contains random variables.
     * 
     * @return if probabilistic
     */
    public boolean isProbabilistic();
}
