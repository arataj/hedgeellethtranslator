/*
 * CodeStaticAnalysis.java
 *
 * Created on Jul 1, 2011, 9:17:49 PM
 *
 * Copyright (c) 2011 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */
package pl.gliwice.iitis.hedgeelleth.compiler.code.optimizer;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.code.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.op.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.optimizer.runtime.RuntimeStaticAnalysis;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Literal;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Method;
import pl.gliwice.iitis.hedgeelleth.interpreter.InterpretingContext;
import pl.gliwice.iitis.hedgeelleth.interpreter.RuntimeValue;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.ArrayStore;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.RuntimeMethod;
import pl.gliwice.iitis.hedgeelleth.interpreter.exception.InterpreterException;
import pl.gliwice.iitis.hedgeelleth.interpreter.op.CodeArithmetics;

/**
 * Contains a method <code>staticAnalysis</code> for static analysis
 * of a code method.<br>
 *
 * @author Artur Rataj
 */
public class CodeStaticAnalysis {
//    /**
//     * Constains information about usage of arrays and their individual
//     * elements.<br>
//     * 
//     * Used by Prism backend to purge non--needed element variables.
//     */
//    public static class ArrayAccess {
//        /**
//         * Arrays accessed a variable index.
//         */
//        public Set<ArrayStore> array = new HashSet<>();
//        /**
//         * Indices accessed using constant indexing.
//         */
//        public Set<ArrayStore.ElementRef> element = new HashSet<>();
//        
//        public ArrayAccess() {
//            array = new HashSet<>();
//            element = new HashSet<>();
//        }
//        /**
//         * If an element is used.<br>
//         * 
//         * If an element belongs to an array, that has been accessed by
//         * a variable, then the element is assumed to be accessed as well,
//         * irrespective of its position.
//         * 
//         * @param ref element to test
//         * @return if used
//         */
//        public boolean used(ArrayStore.ElementRef ref) {
//            return
//                    // an array written to using a variable index makes all its elements
//                    // possibly variable
//                    !array.contains(ref.store) &&
//                    !element.contains(ref);
//        }
//    };
    /**
     * Performs a static analysis of an operation, and if
     * it can be changed into an assignment of a constant,
     * then the assignment is returned. Otherwise, the original
     * operation is returned.
     * 
     * @param ic optional interpreting context, if not null used if the
     * interpreted is needed to evaluate some expression
     * @param op operation to analyse
     * @param possibility can not be null if the context allows for using
     * local variables
     * @return either <code>op</code> or a new assignment
     */
    public static AbstractCodeOp staticAnalysis(InterpretingContext ic,
            AbstractCodeOp op/*, CodeValuePossibility possibility*/) throws InterpreterException {
        Literal result;
        try {
// if(ic != null)
//     ic = ic;
            if(op instanceof CodeOpUnaryExpression) {
                CodeOpUnaryExpression ue = (CodeOpUnaryExpression)op;
                if(ue.isProbabilistic())
                    result = null;
                else
                    result = CodeArithmetics.computeValue(ic, ue, null);
            } else if(op instanceof CodeOpBinaryExpression) {
                CodeOpBinaryExpression be = (CodeOpBinaryExpression)op;
                if(be.isProbabilistic())
                    result = null;
                else
                    result = CodeArithmetics.computeValue(ic, be);
            } else
                result = null;
            if(!op.hasRestrictiveSourceLevelRanges() && result != null) {
                CodeOpAssignment a = new CodeOpAssignment(op.getStreamPos());
                a.label = op.label;
                a.addTags(op.getTags());
                // the result's range check has already been done
                a.rvalue = new RangedCodeValue(new CodeConstant(null, result), null);
                a.setResult(op.getResult());
                return a;
            } else
                return op;
        } catch(InterpreterException e) {
            if(!e.getRawMessage().startsWith(CodeArithmetics.RUNTIME_CLASS_CAST_MESSAGE) /*||
                    ic.useVariables != InterpretingContext.UseVariables.NO*/)
                throw e;
            // class cast exception during a static analysis may happen
            // if e.g. an operation is evaluated, which would never be
            // executed in a normal process due to the control flow
            //
            // unlike a range exception which is treated as a general assertion,
            // invalid class casts are ignored in such a case
            return op;
        }
    }
    /**
     * Computes expressions that can be evaluated statically, and then
     * replaces them with assignments. This is similar to
     * <code>StaticAnalysis</code>, but works on code.<br>
     * 
     * Can throw <code>InterpreterException</code> if a variable is outside
     * its bounds. It may mean, that the static analysis is incomplete,
     * but can be ignored.<br>
     * 
     * Performs only a single iteration. If the iteration made new
     * expressions constant, further calls of this method might
     * evaluate new expressions.
     * 
     * @param ic optional interpreting context, if not null used if the
     * interpreted is needed to evaluate some expression
     * @param method method
     * @param possibility if not null, used to find values of locals
     * @return if any expression has been replaced by assignment
     */
    public static boolean staticAnalysisSingle(InterpretingContext ic,
            CodeMethod method, CodeValuePossibility possibility) throws InterpreterException {
        boolean stable = true;
        if(ic != null) {
            if((ic.useVariables != InterpretingContext.UseVariables.NO) != (possibility != null))
                throw new RuntimeException("mismatch between variable usage and presence of variable possibility");
            if(ic.useVariables == InterpretingContext.UseVariables.ALL)
                throw new RuntimeException("no field values available");
        }
        if(method.code != null) {
if(method.getKey().contains("First"))
    method = method;
            List<AbstractCodeOp> ops = method.code.ops;
            int index = 0;
            for(AbstractCodeOp op : ops) {
                InterpretingContext.UseVariables uv;
                Map<CodeVariable, RuntimeValue> tmpValues;
                if(ic != null) {
                    uv = ic.useVariables;
                    tmpValues = ic.rm.values;
                    ic.rm.values = new HashMap<>();
                    for(CodeVariable cv : tmpValues.keySet())
                        if(cv.name.equals(Method.LOCAL_THIS))
                            ic.rm.values.put(cv, tmpValues.get(cv));
                } else {
                    uv = InterpretingContext.UseVariables.NO;
                    tmpValues = null;
                }
//if(index == 13)
//    index = index;
                if(uv == InterpretingContext.UseVariables.LOCAL) {
                    boolean noLocalValue = false;
                    for(CodeVariable local : op.getLocalSources()) {
                        if(!local.name.equals(Method.LOCAL_THIS)) {
                            CodeFieldDereference d = new CodeFieldDereference(local);
                            // mostly to propagate in-pta allocated objects, as primitive
                            // constants are also propagated elsewhere
                            AbstractCodeValue v = possibility.getSingleM1(
                                    index - 1, d);
                            if(v == null || !(v instanceof CodeConstant) ||
                                    // probabilistic values serve only for describing
                                    // probabilistic branches, they are not real
                                    // values which can be assigned
                                    v.getResultType().isProbabilistic())
                                noLocalValue = true;
                            else {
                                CodeConstant c = (CodeConstant)v;
                                ic.interpreter.setValue(d, new RuntimeValue(c.value), ic.rm);
                            }
                        }
                    }
                    if(noLocalValue)
                        // disable usage of variables for this operation, as the
                        // local trace is not sufficient and the variables set in
                        // previous operations might already have wrong values
                        ic.useVariables = InterpretingContext.UseVariables.NO;
                }
                // not using the interpreter context, as it might contain
                // invalid values, for example an argument with initial value,
                // which would be not valid for some operation
                AbstractCodeOp replacement = staticAnalysis(ic, op);
//if(replacement != op)
//    op = op;
                if(ic != null) {
                    // restore
                    ic.rm.values = tmpValues;
                    ic.useVariables = uv;
                }
                if(replacement != op) {
                    ops.set(index, replacement);
                    stable = false;
                }
                ++index;
            }
        }
        return !stable;
    }
    /**
     * Repeats <code>staticAnalysisSingle</code> until it is stable.
     * 
     * @param ic optional interpreting context, if not null used if the
     * interpreted is needed to evaluate some expression
     * @param method method
     */
    public static boolean staticAnalysisLooped(InterpretingContext ic,
            CodeMethod method) throws InterpreterException {
        boolean stable = true;
        while(staticAnalysisSingle(ic, method, null))
            stable = false;
        return !stable;
    }
    /**
     * <p>Finds array elements, that have constant indexings, but also are
     * never accessedElement to. Replaces these constant indexings with respective
     * initial values.</p>
     * 
     * <p>All array variables must already point to known constants. No dead code
     * allowed.</p>
     * 
     * <p>This method is relatively fast, as opposed to tracing of values. So it is
     * implemented outside <code>RuntimeStaticAnalysis</code>, as possibly
     * usable in other places.</p>
     * 
     * @param sa runtime static analysis; kept up to date by this method
     * @param rmList list of all methods, taht possibly write to the elements
     * @return if any code changed
     */
    public static boolean findConstantElements(RuntimeStaticAnalysis sa,
            List<RuntimeMethod> rmList) throws InterpreterException {
        boolean totalStable = true;
        // arrays written to using a variable index
        Set<ArrayStore> writtenArray = new HashSet<>();
        Set<ArrayStore.ElementRef> writtenElement = new HashSet<>();
        for(RuntimeMethod rm : rmList) {
            int index = 0;
            for(AbstractCodeOp op : rm.method.code.ops) {
                if(op.getResult() instanceof CodeIndexDereference) {
                    CodeIndexDereference i = (CodeIndexDereference)op.getResult();
                    ArrayStore.ElementRef ref = sa.getElementRef(rm, index, i);
                    if(ref != null)
                        writtenElement.add(ref);
                    else {
                        ArrayStore as = sa.getArrayStore(rm, index, i);
                        writtenArray.add(as);
                    }
                }
                ++index;
            }
        }
        for(RuntimeMethod rm : rmList) {
            boolean stable = true;
            int index = 0;
            for(AbstractCodeOp op : rm.method.code.ops) {
                Set<CodeIndexDereference> ii = op.getIndexDereferences();
                for(CodeIndexDereference i : ii) {
/*if(i.toString().indexOf("c4") != -1)
    i = i;*/
                    ArrayStore.ElementRef ref;
                    if(i.object.isLocal() && sa.getTrace(rm).getSingleM1(
                            index - 1, new CodeFieldDereference(i.object)) == null) {
                        // within dead code, no trace available
                        ref = null;
                        throw new RuntimeException("dead code found?");
                    } else
                        try {
                            ref = sa.getElementRef(rm, index, i);
                        } catch(InterpreterException e) {
                            // might be dead code, ignore
                            ref = null;
                        }
                    if(ref != null &&
                            // an array written to using a variable index makes all its elements
                            // possibly variable
                            !writtenArray.contains(ref.store) &&
                            !writtenElement.contains(ref)){
                        RuntimeValue rv;
                        try {
                            rv = ref.store.getElement(sa.getInterpreter(), rm, i,
                                    ref.index);
                        } catch(InterpreterException e) {
                            InterpreterException f = new InterpreterException(
                                    e.getStreamPos(),
                                    "indexing of array " + ref.store.getUniqueName() + ": " +
                                    e.getRawMessage());
                            if(f.getStreamPos() == null)
                                f.setStreamPos(op.getStreamPos());
                            throw f;
                        }
                        op.replaceIndexDereference(i, new CodeConstant(
                                i.getStreamPos(),
                                rv));
                        stable = false;
                    }
                }
                ++index;
            }
            if(!stable) {
                sa.traceRuntimeValues(rm);
                totalStable = false;
            }
        }
        return !totalStable;
    }
//    /**
//     * Finds array elements, that are read or written to.<br>
//     * 
//     * If an element belongs to an array, that has been accessed by
//     * a variable, then the element is assumed to be accessed as well,
//     * irrespective of its position. So this method should be run best on
//     * a code that has already been processed using
//     * <code>ConstantIndexing</code>.
//     * 
//     * @param sa runtime static analysis; kept up to date by this method
//     * @param rmList list of all methods, taht possibly write to the elements
//     * @return list of usage
//     */
//    public static ArrayAccess findUsedElements(RuntimeStaticAnalysis sa,
//            List<RuntimeMethod> rmList) throws InterpreterException {
//        ArrayAccess access = new ArrayAccess();
//        for(RuntimeMethod rm : rmList) {
//            int index = 0;
//            for(AbstractCodeOp op : rm.method.code.ops) {
//                Set<CodeIndexDereference> ii = op.getIndexDereferences();
//                for(CodeIndexDereference i : ii) {
//                    ArrayStore.ElementRef ref = sa.getElementRef(rm, index, i);
//                    if(ref != null)
//                        access.element.add(ref);
//                    else {
//                        ArrayStore as = sa.getArrayStore(rm, index, i);
//                        access.array.add(as);
//                    }
//                }
//            }
//        }
//        return access;
//    }
}
