/*
 * CodeValuePossibility.java
 *
 * Created on May 1, 2008
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.code.optimizer;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.code.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Context;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Variable;

/**
 * Stores possible values that a dereference may contain at a
 * given index in the code.<br>
 * 
 * Also stores if a given operation is not a dead code.<br>
 * 
 * The index in the map of values is the index of operation
 * <i>after</i> which a mapping is valid or -1 for an initial mapping
 * valid before the code is executed.<br>
 *
 * You can use <code>index - 1</code> to check variable values
 * before an operation at <code>index</code> only for an unoptimized
 * code, where no--ops are still targets of jumps.<br>
 * 
 * An uninitialized variable contains only one value <code>null<code>.
 * A possibly uninitialized variable contains more values, of whose
 * one is <code>null<code>. One or more unknown values are
 * represented by <code>UNKNOWN_VALUE</code>.<br>
 *
 * Local variables are always wrapped into <code>CodeFieldDereference</code>
 * within this class.<br>
 *
 * The runtime value of a field might be different when read
 * from different operations, because of changes to the field in other threads.
 * Such changes are not traced, though. So, if the traced value is a field,
 * to guarantee that the tracing is reliable, additional checking must be done.
 * Typical check is for presence of any writings to the field within some possibly
 * simultaneously executed code.
 *
 * @author Artur Rataj
 */
public class CodeValuePossibility {
    /**
     * An element representing an unknown value.
     */
    public static final AbstractCodeValue UNKNOWN_VALUE = new CodeFieldDereference(
            new CodeVariable(null, null, "#CodeValueSet.UNKNOWN_VALUE",
            null, Context.EMPTY, true, false, false, Variable.Category.INTERNAL_NOT_RANGE));
    
    /**
     * Values that may exists. The first key is the index of the
     * operation after which the possibility is tested, the second
     * key is the dereference, and the value is a set of possible
     * values.
     */
    Map<Integer, Map<AbstractCodeDereference,
            Set<AbstractCodeValue>>> values;
    /**
     * If at a given index a given variable was traced at least once.
     */
    Map<Integer, HashSet<AbstractCodeDereference>> visited;
    /**
     * Stores indices of operations that are not a dead code.
     */
    Set<Integer> liveOp;
    /**
     * Maximum index among the elements, -2 for no elements, -1 for
     * only initialization elements.
     */
    int maxIndex;
    
    /**
     * Creates a new instance of CodeValuePossibility. It is initialized with
     * a given value at the index -1, that is, before the whole
     * code.
     */
    public CodeValuePossibility() {
        values = new HashMap<>();
        visited = new HashMap<>();
        liveOp = new HashSet<>();
        maxIndex = -2;
    }
    /**
     * Adds an element at a given index.
     * 
     * @param index                     index of the instruction after
     *                                  which the code value is to be
     *                                  declared as one that may occur
     * @param r                         dereference whose possible value
     *                                  to add
     * @param v                         the value to add, null for an
     *                                  uninitialized variable
     */
    protected void add(int index, AbstractCodeDereference r, AbstractCodeValue v) {
        Map<AbstractCodeDereference, Set<AbstractCodeValue>> m =
                values.get(index);
        if(m == null)
            m = new HashMap<>();
        Set<AbstractCodeValue> s = m.get(r);
        if(s == null)
            s = new HashSet<>();
        s.add(v);
        m.put(r, s);
        values.put(index, m);
        if(maxIndex < index)
            maxIndex = index;
    }
    /**
     * <p>Returns the values that are possible at given dereference at a
     * given index. May, amongst other, contain uninitialized value, denoted by null,
     * or unknown values, denoted by <code>UNKNOWN_VALUE</code>.</p>
     *
     * <p>M1 means "minus 1" and is a warning about the unusual meaning
     * of <code>index</code>.</p>
     * 
     * @param index                     index of the operation after
     *                                  which the the test is performed, -1 for before the
     *                                  first operation
     * @param r                         reference whose values are tested
     * @return                          a list of values, empty for no
     *                                  values
     */
    public Set<AbstractCodeValue> getValuesM1(int index, AbstractCodeDereference r) {
        Set<AbstractCodeValue> out = new HashSet<>();
        Map<AbstractCodeDereference, Set<AbstractCodeValue>> m =
                values.get(index);
        if(m != null && m.get(r) != null)
            out.addAll(m.get(r));
        return out;
    }
    /**
     * Returns if a given element is present at a given index.
     * 
     * @param index                     index of the operation after
     *                                  which the the test is performed, -1 for before the
     *                                  first operation
     * @param r                         dereference whose values are tested
     * @param v                         the value whose presence is
     *                                  tested
     * @return                          if the value exists
     */
    public boolean contains(int index, AbstractCodeDereference r, AbstractCodeValue v) {
        Set<AbstractCodeValue> s = getValuesM1(index, r);
        if(s == null)
            return false;
        else
            return s.contains(v);
    }
    /**
     * Returns if the element representing an unknown value
     * is present at a given index.
     * 
     * @param index                     index of the operation after
     *                                  which the the test is performed, -1 for before the
     *                                  first operation
     * @param r                         dereference whose unknown value
     *                                  is tested
     * @return                          if the unknown value exists
     */
    public boolean containsUnknown(int index, AbstractCodeDereference r) {
        return contains(index, r, UNKNOWN_VALUE);
    }
    /**
     * Returns how many values is possible at given dereference at a
     * given index, unless at least one of these values marks an unitilialized
     * or possibly uninitialized variable, as defined in
     * <code>CodeValuePossibility</code>, in such a case -1 is returned.
     * If at least one of the values is unknown, the number of values is set to
     * Integer.MAX_VALUE.
     * 
     * @param index                     index of the operation after
     *                                  which the the test is performed, -1 for before the
     *                                  first operation
     * @param r                         reference whose values are tested
     * @return                          number of assigned values or
     *                                  Integer.MAX_VALUE or -1
     */
    public int getNumValues(int index, AbstractCodeDereference r) {
        Set<AbstractCodeValue> s = getValuesM1(index, r);
        for(AbstractCodeValue v : s)
            if(v == null)
                return -1;
        if(containsUnknown(index, r))
            return Integer.MAX_VALUE;
        return s.size();
    }
    /**
     * <p>If there is a single value possible at a given dereference at
     * a given index, then the value is returned. Otherwise, that is,
     * if there is no value, there are several values or there
     * is an unknown value, or at least one of these values denotes
     * an uninitialized variable, null is returned.</p>
     * 
     * <p>M1 means "minus 1" and is a warning about the unusual meaning
     * of <code>index</code>.</p>
     * 
     * @param index                     index of the operation after
     *                                  which the the test is performed, -1 for before the
     *                                  first operation
     * @param r                         dereference whose values are tested
     * @return                          a value or null
     */
    public AbstractCodeValue getSingleM1(int index, AbstractCodeDereference r) {
        if(containsUnknown(index, r))
                return null;
        Set<AbstractCodeValue> s = getValuesM1(index, r);
        if(s.size() == 1)
            return s.iterator().next();
        else
            return null;
    }
    /**
     * Registers that a given dereference was at least once traced at a given
     * index.
     *
     * @param index                     index of the operation after
     *                                  which the the test is performed, -1 for before the
     *                                  first operation
     * @param r                         dereference
     */
    protected void setVisited(int index, AbstractCodeDereference r) {
        HashSet<AbstractCodeDereference> traced = visited.get(index);
        if(traced == null) {
            traced = new HashSet<>();
            visited.put(index, traced);
        }
        traced.add(r);
    }
    /**
     * Returns if a given dereference was at least once traced at a given index.
     *
     * @param index                     index of the instruction after
     *                                  which the test is performed
     * @param r                         dereference, can be null if this is
     *                                  only dead code tracing
     * @return                          if the dereference was traced at the
     *                                  index
     */
    public boolean isVisited(int index, AbstractCodeDereference r) {
        HashSet<AbstractCodeDereference> traced = visited.get(index);
        if(traced == null)
            return false;
        else
            return traced.contains(r);
    }
    /**
     * Registeres an operation at a given index as not a dead code.
     * 
     * @param index                     index of the operation
     */
    protected void setLive(int index) {
        liveOp.add(index);
    }
    /**
     * Returns if an operation at a given index is not a dead code.
     * 
     * @param index                     index of the operation
     */
    public boolean isLive(int index) {
        return liveOp.contains(index);
    }
    /**
     * Replaces all instances of <code>prev</code> in the trace
     * with <code>curr</code>, as if such a replacement has been
     * done in the code, plus false positives. The false positives
     * come from the fact that the possibility sets are simply
     * merged, while a certain replacement might invalidate the
     * values of the replaced variable, what is not taken into
     * account by this method.<br>
     * 
     * This method makes <code>visited</code> outdated.
     *
     * @param prev                      variable to be replaced
     * @param curr                      replacement variable
     */
    public void replaceFalsePositive(AbstractCodeDereference prev, AbstractCodeDereference curr) {
            // replace keys first, possibly merging sets
            for(int index : values.keySet()) {
                Map<AbstractCodeDereference, Set<AbstractCodeValue>> valuesAtCopy =
                        new HashMap<>(values.get(index));
                for(AbstractCodeDereference key : valuesAtCopy.keySet())
                    if(key.equals(prev)) {
                        values.get(index).remove(prev);
                        Set<AbstractCodeValue> set = values.get(index).get(curr);
                        if(set == null)
                            set = new HashSet<>();
                        for(AbstractCodeValue v : valuesAtCopy.get(key))
                            set.add(v);
                        values.get(index).put(curr, set);
                    }
            }
            // then look into the sets
            for(int index : values.keySet()) {
                Map<AbstractCodeDereference, Set<AbstractCodeValue>> valuesAt =
                        values.get(index);
                for(AbstractCodeDereference key : valuesAt.keySet()) {
                    HashSet<AbstractCodeValue> setCopy =
                            new HashSet<AbstractCodeValue>(valuesAt.get(key));
                    for(AbstractCodeValue v : setCopy)
                        if(v != null && v.equals(prev)) {
                            valuesAt.get(key).remove(prev);
                            valuesAt.get(key).add(curr);
                        }
                }
            }
    }
    /**
     * Compares two possibilities, takes into accound only value trace and dead code,
     * no visits.
     * 
     * @param other possibility to compare with
     * @param maxIndex max index an liveness may differ, if the additional lines are empty
     * @return if the same
     */
    public boolean traceEqual(CodeValuePossibility other,
            boolean maxIndexMayDiffer) {
        if(!maxIndexMayDiffer && maxIndex != other.maxIndex)
            return false;
        for(int index = -1; index <= maxIndex; ++index) {
            if(!maxIndexMayDiffer && isLive(index) != other.isLive(index))
                return false;
            Map<AbstractCodeDereference, Set<AbstractCodeValue>> set =
                    values.get(index);
            Map<AbstractCodeDereference, Set<AbstractCodeValue>> otherSet =
                    other.values.get(index);
            if(!maxIndexMayDiffer && ((set != null) ^ (otherSet != null)))
                return false;
            if(set != null || otherSet != null) {
                if(set == null)
                    set = new HashMap<>();
                if(otherSet == null)
                    otherSet = new HashMap<>();
                if(set.size() != otherSet.size())
                    return false;
                if(!set.keySet().equals(otherSet.keySet()))
                    return false;
                for(AbstractCodeDereference r : set.keySet()) {
                    if(!set.get(r).equals(otherSet.get(r)))
                        return false;
                }
            }
        }
        return true;
    }
    @Override
    public String toString() {
        String s = "";
        for(int index = -1; index <= maxIndex; ++index) {
            String l = "" + index;
            while(l.length() < 3)
                l = " " + l;
            if(isLive(index))
                l += " ";
            else
                l += "D";
            Map<AbstractCodeDereference, Set<AbstractCodeValue>> set =
                    values.get(index);
            if(set != null)
                for(AbstractCodeDereference r : set.keySet()) {
if(r.toString().indexOf("#t") == -1/* && r.toString().indexOf("pr") == -1*/)
    continue;
if(isVisited(index, r)) l += "(v)";
                    l += " " + r.toNameString() + " = { ";
                    for(AbstractCodeValue v : set.get(r)) {
                        String value;
                        if(v == null)
                            value = "(-)";
                        else if(v == UNKNOWN_VALUE)
                            value = "(?)";
                        else
                            value = v.toNameString();
                        l += value + " ";
                    }
                    l += "}";
                }
            s += l + "\n";
        }
        return s;
    }
}
