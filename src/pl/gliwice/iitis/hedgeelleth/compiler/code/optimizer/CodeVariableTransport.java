/*
 * CodeVariableTransport.java
 *
 * Created on Mar 26, 2009, 11:17:44 AM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.code.optimizer;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.code.AbstractCodeValue;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.AbstractCodeDereference;

/**
 * Stores meaningfull transports of values by code variables.<br>
 * 
 * Local variables are always wrapped into <code>CodeFieldDereference</code>
 * within this class.<br>
 *
 * A variable can transport a value <i>to</i> an operation, <i>from</i>
 * an operation, or <i>through</i> the whole operation. These cases
 * differ in whether there was a meaningfull transport respectively
 * (1) only when the operation began, (2) only when the operation finished,
 * and (3) both when the operation began and finished. In other words,
 * <i>To</i> means that the variable only delivers the value
 * to the operation as a source variable of the operation, and the value is
 * no longer needed when the operation finishes. <i>From</i> means that the variable
 * only receives the value from the operation as the operation's target
 * variable. <i>Through</i> means that the variable transported a meaningfull
 * value both when the operation began to execute, and when the operation
 * finished its execution. It does not imply that the value was really
 * delivered or received from the operation, which is possible, though.<br>
 *
 * To reflect the three cases, the so called double indices are used. An
 * operation at an index <i>i</i> has two double indices: <i>2i</i> and
 * <i>2i + 1</i>. In the <i>to</i> case, only the double index <i>2i</i>
 * is marked. In the <i>from</i> case, only the double index <i>2i + 1</i>
 * is marked. In the case <i>through</i>, both of these double indices
 * are marked. In the other words, <i>2i</i> represents the transport when
 * the operation exactly at the beging of the execution and <i>2i + 1</i> represents
 * the transport exactly when the operation finished the execution.<br>
 *
 * In all methods that involve indices in this class, a double index is meant.
 * The exception is the textual output of the method <code>toString</code>,
 * that displays <i>2i</i> as <i>i</i> with attached `_' and displays
 * <i>2i + 1</i> as <i>i</i> with attached '^'.<br>
 *
 * If both the double index <i>2i</i> and the double index <i>2i + 1</i>
 * are marked, then, as it has already been discussed, it is not known how
 * the variable is related to the operation at the index <i>i</i>.
 * Being a source or a target local variable of an operation can be tested,
 * though, using <code>AbstractCodeOp.isLocalSource()</code> and
 * <code>AbstractCodeOp.isLocalTarget()</code>.
 *
 * @author Artur Rataj
 */
public class CodeVariableTransport {
    /**
     * If from/through/to an operation at a given double index a
     * given variable
     * was transporting a meaningfull data. Null if the transport
     * information was not collected.
     */
    Map<Integer, Set<AbstractCodeDereference>> transport;

    /**
     * Creates a new instance of CodeValueTransport. 
     */
    public CodeVariableTransport() {
        transport = new HashMap<>();
    }
    /**
     * Returns all dereferences, that transfer a meaningfull value
     * value from/through/to the operation at a given index.
     * 
     * @param doubleIndex double index of the instruction
     * @return a set of dereferences; empty if no dereferences
     */
    public Set<AbstractCodeDereference> get(int doubleIndex) {
        Set<AbstractCodeDereference> t = transport.get(doubleIndex);
        if(t == null)
            t = new HashSet<>();
        return t;
    }
    /**
     * Registers that a given dereference transported a meaningfull
     * value from/though/to operation at a given index.<br>
     *
     * @param doubleIndex                     double index of the instruction
     * @param r                         dereference
     */
    protected void setTransported(int doubleIndex, AbstractCodeDereference r) {
        Set<AbstractCodeDereference> t = transport.get(doubleIndex);
        if(t == null) {
            t = new HashSet<>();
            transport.put(doubleIndex, t);
        }
        t.add(r);
    }
    /**
     * Returns if a given dereference transported a meaningfull
     * value from/through/to the operation at a given index.
     *
     * @param doubleIndex                     double index of the instruction
     * @param r                         dereference
     * @return                          if transported
     */
    public boolean isTransported(int doubleIndex, AbstractCodeDereference r) {
        Set<AbstractCodeDereference> t = get(doubleIndex);
        return t.contains(r);
    }
    /**
     * Returns if a given dereference transported a meaningfull
     * value from/though/to an operation anywhere in the traced
     * method.
     *
     * @param r                         dereference
     * @return                          if transported
     */
    public boolean isTransportedAnywhere(AbstractCodeDereference r) {
        boolean found = false;
        for(int doubleIndex : transport.keySet()) {
            Set<AbstractCodeDereference> set = get(doubleIndex);
            if(set.contains(r)) {
                found = true;
                break;
            }
        }
        return found;
    }
    /**
     * Returns if two variables overlap, that is, if there is at least
     * a single operation, from/through/to which both of the variables
     * transported a meaningfull value.<br>
     *
     * Additionally,
     * if <code>possibility</code> is not null, then, if the variables
     * were found to overlap, it is checked if they have the same
     * value in the overlapping region. If yes, they are reported as not
     * overlapping. Only a simple case of a single, known value is tested,
     * for more complicated traces the variables are reported as
     * overlapping ones.
     *
     * @param v1                        first variable
     * @param v2                        second variable
     * @param possibility               trace or null, can be incomplete
     *                                  as only <code>CodeValuePossibility.getSingle()</code>
     *                                  is used on it
     * @return                          if the variables overlap
     */
    public boolean overlap(AbstractCodeDereference v1, AbstractCodeDereference v2,
            CodeValuePossibility possibility) {
        boolean overlap = false;
        SEARCH:
        for(int doubleIndex : transport.keySet())
            if(isTransported(doubleIndex, v1) && isTransported(doubleIndex, v2)) {
                if(possibility == null) {
                    overlap = true;
                    break SEARCH;
                } else {
                    int possibilityIndex = (doubleIndex + 1)/2 - 1;
                    AbstractCodeValue s1 = possibility.getSingleM1(possibilityIndex, v1);
                    AbstractCodeValue s2 = possibility.getSingleM1(possibilityIndex, v2);
                    // null means more vaules, as both values are guaranteed
                    // to be initialized in overlapping transport region
                    if(s1 == null || s2 == null ||
                            !s1.equals(s2)) {
                        overlap = true;
                        break SEARCH;
                    }
                }
            }
        return overlap;
    }
    /**
     * Replaces all instances of <code>prev</code> in the sets
     * with <code>curr</code>.
     *
     * @param prev                      variable to be replaced
     * @param curr                      replacement variable
     */
    public void replace(AbstractCodeDereference prev, AbstractCodeDereference curr) {
        for(int doubleIndex : transport.keySet())
            if(isTransported(doubleIndex, prev)) {
                Set<AbstractCodeDereference> t = get(doubleIndex);
                t.remove(prev);
                t.add(curr);
            }
    }
    @Override
    public String toString() {
        String s = "";
        int maxDoubleIndex = -1;
        for(int doubleIndex : transport.keySet())
            if(maxDoubleIndex < doubleIndex)
                maxDoubleIndex = doubleIndex;
        for(int doubleIndex = 0; doubleIndex <= maxDoubleIndex;
                ++doubleIndex) {
            String l = "" + doubleIndex/2;
            if(doubleIndex%2 == 0)
                l += "_";
            else
                l += "^";
            while(l.length() < 3)
                l = " " + l;
            for(AbstractCodeDereference r : get(doubleIndex)) {
                if(isTransported(doubleIndex, r))
                    l += " " + r;
            }
            s += l + "\n";
        }
        return s;
    }
}
