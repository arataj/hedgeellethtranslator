/*
 * ConditionalLiteral.java
 *
 * Created on Mar 6, 2015
 *
 * Copyright (c) 2015  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.code.optimizer;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.Literal;

/**
 * <p>A boolean literal used only within <code>TraceValues</code>. Used in
 * false/true branches of <code>CodeOpBranch</code>. It cancels itself
 * if such a branch turns out to be dead code.</p>
 * 
 * <p>Its existence depends on an existence of only an unconditional or
 * different--origin--conditional literal of a different value at the original branch.</p>
 * 
 * @author Artur Rataj
 */
public class ConditionalLiteral extends Literal {
    /**
     * <p>Index of <code>CodeOpBranch</code>, from which starts a branch,
     * at whose start this conditional literal starts to be propagated.</p>
     * 
     * <p>Two conditional literals are of different origin iff this value is different
     * in each.</p>
     */
    final public int INDEX;
    
    /**
     * Creates a new instance of a conditional literal.
     * 
     * @param value boolean value
     * @param index index of <code>CodeOpBranch</code>, which is an origin
     * of the branch, within which this literal starts to be propagated
     */
    public ConditionalLiteral(boolean value, int index) {
        super(value);
        INDEX = index;
    }
    @Override
    public boolean equals(Object o) {
        if(!(o instanceof ConditionalLiteral))
            return false;
        Literal l = (ConditionalLiteral)o;
        return getBoolean() == l.getBoolean() &&
                INDEX == ((ConditionalLiteral)o).INDEX;
    }
    @Override
    public int hashCode() {
        return 23*super.hashCode() + INDEX;
    }
    @Override
    public String toString() {
        return "(cond. " + INDEX + ")" + super.toString();
    }
}
