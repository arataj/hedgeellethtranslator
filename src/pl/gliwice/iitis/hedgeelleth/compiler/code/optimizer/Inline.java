/*
 * Inline.java
 *
 * Created on Jul 4, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.code.optimizer;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.CompilerAnnotation;
import pl.gliwice.iitis.hedgeelleth.compiler.code.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.op.*;
import pl.gliwice.iitis.hedgeelleth.compiler.primitiverange.CodeVariablePrimitiveRange;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Context;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.MethodSignature;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.CompilerException;

/**
 * Tools for inlining methods.
 * 
 * @author Artur Rataj
 */
public class Inline {
    /**
     * Prefix of the prefix of locals of inlined functions.
     * 
     * The only method that prefixes locals with this prefix
     * must be Inline.inline(), because the function uses
     * the resulting names as keys for optimization.
     * 
     * The same method inlined twice can use the same subset
     * of locals, unless one of the inlines contains another.
     * But in such a case the more nested inline has the prefix
     * with the number of instances N of the method in the inlined
     * methods stack, so subsets are two. If two inlines of the
     * same method have the same nesting level, one can not use
     * another and N is the same, thus the subset is the same.
     * 
     * If two inlines of the same method have a different N,
     * but one does not contains another, the optimization is not
     * performed, that is, these inlines use different subsets even
     * if they could use a single one.
     */
    public static final String INLINE_PREFIX_PREFIX = "i#";

    /**
     * Performs local variable replacement, including ranges.<br>
     * 
     * If a dereference that is to be replaced
     * by a constant happens to be a target variable of some
     * operation, a runtime exception is throw.<br>
     * 
     * @param ops                       operations with the variables
     *                                  to replace 
     * @param replacements              keys are variables to be replaced,
     *                                  values are replacement variables
     */
    public static void replaceVariables(CodeMethod method,
            Map<CodeVariable, CodeVariable> replacements) {
        // replace variables in primitive ranges
        for(CodeVariable r : replacements.values()) {
            CodeVariablePrimitiveRange pr = r.primitiveRange;
            if(pr != null) {
                if(pr.min instanceof CodeFieldDereference) {
                    CodeFieldDereference fpr = (CodeFieldDereference)pr.min;
                    CodeVariable nr = replacements.get(fpr.variable);
                    if(nr != null)
                        fpr.variable = nr;
                }
                if(pr.max instanceof CodeFieldDereference) {
                    CodeFieldDereference fpr = (CodeFieldDereference)pr.max;
                    CodeVariable nr = replacements.get(fpr.variable);
                    if(nr != null)
                        fpr.variable = nr;
                }
            }
        }
        for(AbstractCodeOp op : method.code.ops) {
/*if(op instanceof CodeOpCall)
    op = op;*/
            List<CodeVariable> all =
                    new ArrayList(op.getLocalSources());
            int minTargetIndex = all.size();
            all.addAll(op.getLocalTargets());
            for(int index = 0; index < all.size(); ++index) {
                CodeVariable v = all.get(index);
                CodeVariable r = replacements.get(v);
                // PR bounds are already replaced
                if(r != null) {
                    CodeFieldDereference d =
                            new CodeFieldDereference(r);
                    boolean isSource = index < minTargetIndex;
                    if(isSource) {
                        if(!op.replaceLocalSourceVariable(v, d)) {
                            // a variable within other variable's PR
                            // may not necessarily be present in the op
                            // outside of a variable range
                            if(!v.isVariablePRBound())
                                throw new RuntimeException("no replacement");
                        }
                    } else {
                        if(!op.replaceLocalTargetVariable(v, d))
                            throw new RuntimeException("no replacement");
                    }
                }
            }
        }
    }
    /**
     * Returns a prefix to add to variables or labels of an inlined
     * variable.
     * 
     * @param method                    method to inline
     * @param uniquenessIdentifier      for two instances of the inlined method
     *                                  that share variables it should be the
     *                                  same if this method returns variable
     *                                  prefix, otherwise it should be unique
     *                                  within a method the instances belong to
     *                                  
     * @return
     */
    protected static String getInlinePrefix(CodeMethod method,
            int uniquenessIdentifier) {
        String prefix = INLINE_PREFIX_PREFIX + uniquenessIdentifier +
                method.owner.name + "_" +
                method.signature.name + "_";
        // replace the low--readability "/" in the names of inlined variables
        int pos = prefix.indexOf(MethodSignature.SIGNATURE_ARGUMENTS_SEPARATOR);
        prefix = prefix.substring(0, pos) + "_" + prefix.substring(pos + 1);
        return prefix;
    }
    /**
     * Replaces a given call with an inlined method. The <i>called</i>
     * methods being inlined are not affected, and none of their operations
     * are used, but cloned instead.<br>
     * 
     * If the inlined method has annotations, inlined code is
     * prefixed with CodeOpNone operation with these annotations.<br>
     * 
     * Possible label indices are modified as required.<br>
     * 
     * Tags of an inlined call are inherited by an inlined method.<br>
     * 
     * The code after inlining is not optimized, it may contain
     * jumps, assignments and dereferences apt for optimizing.<br>
     * 
     * Inlined method's annotations are lost.
     * 
     * @param method                    method with the call to
     *                                  replace
     * @param callIndex                 index of the call to replace
     * @param inlinedStack              stack of inlined methods,
     *                                  empty for the top method,
     *                                  string is the method's key
     *                                  containing the method's class
     *                                  and signature
     */
    public static void inline(CodeMethod method, int callIndex,
            Stack<String> inlinedStack) {
        CodeOpCall call = (CodeOpCall)method.code.ops.get(callIndex);
        // there must be at least the return operation after
        // a call
        AbstractCodeOp afterCall = method.code.ops.get(callIndex + 1);
        CodeMethod calledMethodCopy = new CodeMethod(
                call.methodReference.method);
        for(AbstractCodeOp op : calledMethodCopy.code.ops)
            op.appendTags(call, true, false);
        // count methods with the same origin in the source code as this
        // one in the stack of inlined method
        int inlineCount = 0;
        for(String key : inlinedStack)
            if(key.equals(calledMethodCopy.getKey()))
                ++inlineCount;
        String variablePrefix = getInlinePrefix(calledMethodCopy, inlineCount);
        // instances of inlined methods can sometimes share variables,
        // but they never should share labels, thus a distinct label prefix
        // is needed
        String labelPrefix = getInlinePrefix(calledMethodCopy, callIndex);
        // if inlined returns void, this label marks the next operation after
        // the inline
        CodeLabel afterLabel;
        if(afterCall.label == null) {
            afterLabel = new CodeLabel(labelPrefix + "return");
            afterCall.label = afterLabel;
            afterCall.label.codeIndex = callIndex + 1;
        } else
            afterLabel = new CodeLabel(afterCall.label.name);
        // map of variable replacements
        List<AbstractCodeOp> preamble = new ArrayList<>();
        Map<CodeVariable, CodeVariable> replacements = new HashMap<>();
        // prepare assignments of arguments to the replacement
        // local variables
        for(CodeVariable local : calledMethodCopy.variables.values()) {
            String replacementName = variablePrefix + local.name;
            // the variable name is used as a key, see the documentation
            // of INLINE_PREFIX_PREFIX for details.
            //
            CodeVariable replacementVariable = method.variables.get(
                    replacementName);
            // in case of shared variables, prevent duplicate definition
            if(replacementVariable == null) {
                replacementVariable = new CodeVariable(local.getStreamPos(),
                        method, replacementName, local.type,  Context.NON_STATIC,
                        local.finaL, false, false, local.category);
                if(local.primitiveRange != null)
                    replacementVariable.primitiveRange =
                            new CodeVariablePrimitiveRange(
                                local.primitiveRange.getStreamPos(),
                                ((CodeFieldDereference)local.primitiveRange.min).variable,
                                ((CodeFieldDereference)local.primitiveRange.max).variable,
                                local.primitiveRange.isSourceLevel());
 // replacementVariable.primitiveRange = local.primitiveRange;
                // method.variables.keySet().contains(replacementVariable.name)
                method.variables.put(replacementVariable.name,
                        replacementVariable);
            }
            replacements.put(local, replacementVariable);
            int argNum = calledMethodCopy.getWrittenNum(local);
            if(argNum != -1) {
                RangedCodeValue value = call.args.get(argNum);
                // copies call's argument into replacement variable
                CodeOpAssignment a = new CodeOpAssignment(
                        call.getStreamPos());
                a.appendTags(call, true, false);
                a.rvalue = value;
                a.setResult(new CodeFieldDereference(replacementVariable));
                preamble.add(a);
            }
        }
        Code inlined = calledMethodCopy.code;
        // ensure the first inlined operation does not
        // have a label, because it may need to inherit
        // the label of the replaced call
        CodeOpNone n = new CodeOpNone(call.getStreamPos());
        /*
        n.annotations = new HashSet<String>(
                calledMethodCopy.annotations);
         */
        inlined.ops.add(0, n);
        inlined.offsetLabelRefIndices(0, 1);
        boolean returnsValue = call.getResult() != null;
        if(returnsValue) {
            // store the return value of the inlined method
            // in a respective local of this method
            CodeOpAssignment a = new CodeOpAssignment(call.getStreamPos());
            a.appendTags(call, true, false);
            a.rvalue = new RangedCodeValue(
                    new CodeFieldDereference(calledMethodCopy.returnValue));
            AbstractCodeDereference result = call.getResult();
            a.setResult(result);
            afterLabel = new CodeLabel(afterLabel.name + "_rval");
            afterLabel.codeIndex = inlined.ops.size();
            /*
            if(calledMethodCopy.returnValue.hasPrimitiveRange(false)) {
                // //return value's primitive range also needs to be copied
                String prefix = "#range#" + variablePrefix;
                CodeVariable sourceMin = ((CodeFieldDereference)calledMethodCopy.
                        returnValue.primitiveRange.min).variable;
                CodeVariable targetMin = new CodeVariable(sourceMin.getStreamPos(),
                        method, prefix + sourceMin.name, sourceMin.type,  Context.NON_STATIC,
                        sourceMin.finaL, false, false);
                method.variables.put(targetMin.name, targetMin);
                CodeVariable sourceMax = ((CodeFieldDereference)calledMethodCopy.
                        returnValue.primitiveRange.max).variable;
                CodeVariable targetMax = new CodeVariable(sourceMax.getStreamPos(),
                        method, prefix + sourceMax.name, sourceMin.type,  Context.NON_STATIC,
                        sourceMin.finaL, false, false);
                method.variables.put(targetMax.name, targetMax);
                CodeOpAssignment aMin = new CodeOpAssignment(call.getStreamPos());
                aMin.appendTags(call, true, false);
                aMin.rvalue = new RangedCodeValue(new CodeFieldDereference(sourceMin));
                aMin.setResult(new CodeFieldDereference(targetMin));
                CodeOpAssignment aMax = new CodeOpAssignment(call.getStreamPos());
                aMax.appendTags(call, true, false);
                aMax.rvalue = new RangedCodeValue(new CodeFieldDereference(sourceMax));
                aMax.setResult(new CodeFieldDereference(targetMax));
                inlined.add(aMin);
                inlined.add(aMax);
                ((CodeFieldDereference)result).variable.primitiveRange =
                        new CodeVariablePrimitiveRange(
                            new CodeFieldDereference(targetMin),
                            new CodeFieldDereference(targetMax));
                aMin.label = afterLabel;
            } else
                a.label = afterLabel;
             */
            a.label = afterLabel;
            inlined.add(a);
        } else {
            afterLabel = new CodeLabel(afterLabel);
            afterLabel.codeIndex = inlined.ops.size();
        }
        // replace returns with jumps at the end of the inlined
        // code, prefix the labels and label references in the
        // inlined code
        int count = 0;
        for(AbstractCodeOp op : new LinkedList<>(inlined.ops)) {
            if(op instanceof CodeOpReturn) {
                CodeOpJump jump = new CodeOpJump(
                        op.getStreamPos());
                jump.appendTags(op, true, false);
                jump.gotoLabelRef = new CodeLabel(afterLabel);
                inlined.replace(count, jump, false);
                op = jump;
            }
            if(op.label != null)
                op.label.name = labelPrefix + op.label.name;
            for(CodeLabel labelRef : op.getLabelRefs())
                // if label points to an op outside the inlined method,
                // then do not prefix it
                if(returnsValue || !labelRef.equals(afterLabel))
                    labelRef.name = labelPrefix + labelRef.name;
            ++count;
        }
        // add the assignments before the inlined code
        inlined.ops.addAll(0, preamble);
        // replace locals of the inlined method with the locals
        // of this method
        replaceVariables(calledMethodCopy, replacements);
        // correct the labels in the inlined code
        inlined.offsetLabelRefIndices(0, callIndex + preamble.size());
        // correct the label indices in this method if needed
        method.code.offsetLabelRefIndices(callIndex + 1,
                inlined.ops.size() - 1);
        // preserve a possible label
        CodeLabel label = method.code.ops.get(callIndex).label;
        inlined.ops.get(0).label = label;
        // and replace the call
        method.code.ops.remove(callIndex);
        for(AbstractCodeOp op : inlined.ops)
            if(!Optimizer.doesNothing(inlined.ops, op)) {
                op.appendTags(call, true, true);
                break;
            }
        method.code.ops.addAll(callIndex, inlined.ops);
    }
    /**
     * Recursively inlines calls. None of the input methods
     * is affected, including the root one.<br>
     * 
     * Possible label indices are modified as required.<br>
     * 
     * Methods with IGNORE or MARKER annotations are not inlined.
     * Instead, call to such a method is replaced with CodeOpNone
     * with all of the method's annotations, unless the method has
     * IGNORE annotation and ignoredMethodAnnotations is false, what
     * ignores the method completely.<br>
     * 
     * Any directly or indirectly recursive calls, and virtual
     * calls, are left in the code
     * instead of replacing them. Method <code>containsCalls</code> can
     * be used to detect if there are any calls left in the code.<br>
     * 
     * Tags of an inlined call are inherited by an inlined method
     * as specified by the tags' modifiers.<br>
     * 
     * Inlined method's annotations are lost.<br>
     * 
     * If this inlining is run on an already inlined method, then the method is required to have
     * the same owner and name each time.
     * 
     * @param method                    input method
     * @param ignoredMethodAnnotations  true if there should be
     *                                  even no inheriting
     *                                  <code>CodeOpNone</code> operation
     *                                  if a method has IGNORE
     *                                  annotation, but instead an empty
     *                                  <code>CodeOpNone</code>
     * @param onlyReplaceMarkerMethods  only replace marker methods,
     *                                  calls to non--marker methods
     *                                  are not altered
     * @return                          method with inlined code
     * @see #containsCalls
     */
    public static CodeMethod inline(CodeMethod method,
            boolean ignoredMethodAnnotations,
            boolean onlyReplaceMarkerMethods) {
        // String prefix = method.owner.name + "_" +
        //         method.signature.name + "_";
//if(method.getKey().contains("Periodic.run"))
//    method = method;
        CodeMethod inlined = new CodeMethod(method);
        Stack<String> inlinedStack = new Stack<>();
        // exit points of the inlined method
        List<Integer> exits = new LinkedList<>();
        for(int index = 0; index < inlined.code.ops.size(); ++index) {
            AbstractCodeOp op = inlined.code.ops.get(index);
            Iterator<Integer> exitsIterator = exits.iterator();
            while(exitsIterator.hasNext()) {
                int i = exitsIterator.next();
                if(i == index) {
                    inlinedStack.pop();
                    exitsIterator.remove();
                }
            }
            if(op instanceof CodeOpCall) {
                boolean ignore = false;
                boolean marker = false;
                CodeOpCall c = (CodeOpCall)op;
                CodeMethod called = c.methodReference.method;
                for(String s : called.annotations)
                    switch(CompilerAnnotation.parse(s)) {
                        case IGNORE:
                            ignore = true;
                            break;

                        case MARKER:
                            marker = true;
                            break;

                        default:
                            /* empty */
                            break;

                    }
                if(ignore || marker) {
                    CodeOpNone n;
                    if(ignoredMethodAnnotations && ignore)
                        n = new CodeOpNone(op.getStreamPos());
                    else {
                        n = new CodeOpNone(c, op.getStreamPos());
                        n.copyTextRange(c.textRange);
                    }
                    n.appendTags(op, true, true);
                    inlined.code.replace(index, n, false);
                } else if(!onlyReplaceMarkerMethods &&
                        !inlinedStack.contains(called.getKey()) &&
                        !c.isVirtual()) {
                    int lengthDiff = inlined.code.ops.size();
                    exits.add(index + 1);
/*if(index == 26)
    index = index;*/
                    inline(inlined, index, inlinedStack);
                    inlinedStack.add(called.getKey());
                    lengthDiff = inlined.code.ops.size() - lengthDiff;
                    for(int i = 0; i < exits.size(); ++i) {
                        int exitIndex = exits.get(i);
                        if(exitIndex > index)
                            exits.set(i, exitIndex + lengthDiff);
                    }
                    // the first operation of the inlined method can
                    // be a call
                    --index;
                } else {
                    AbstractCodeOp replaced = inlined.code.ops.get(index);
                    CodeOpThrow recursionThrow = new CodeOpThrow(
                            replaced.getStreamPos());
                    recursionThrow.staticCheckError = new CompilerException(
                            recursionThrow.getStreamPos(),
                            CompilerException.Code.ILLEGAL,
                            "\"direct or indirect recursive call\"");
                    recursionThrow.label = replaced.label;
                    inlined.code.ops.remove(index);
                    inlined.code.ops.add(index, recursionThrow);
                }
            }
        }
        return inlined;
    }
    /**
     * If there are any CodeOpCall operations in a code.
     * 
     * @param method                    code to test
     * @return                          null if there are no calls in the
     *                                  method, otherwise first CodeOpCall
     *                                  in the list
     */
    public static CodeOpCall containsCalls(Code code) {
        for(AbstractCodeOp op : code.ops)
            if(op instanceof CodeOpCall)
                return (CodeOpCall)op;
        return null;
    }
}
