/*
 * MergeVariables.java
 *
 * Created on Sep 3, 2009, 2:23:06 PM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.code.optimizer;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.CompilerAnnotation;
import pl.gliwice.iitis.hedgeelleth.compiler.code.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.op.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.*;
import pl.gliwice.iitis.hedgeelleth.compiler.primitiverange.CodeVariablePrimitiveRange;
import pl.gliwice.iitis.hedgeelleth.compiler.sa.RangeCodeStaticAnalysis;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Literal;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Type;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.CompilerException;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * Provides method for optimizing a method by merging its local variables.<br>
 *
 * Array classes must already be converted to
 * <code>Type.ARRAY_QUALIFIED_CLASS_NAME</code> for this class to be used.
 *
 * @author Artur Rataj
 */
public class MergeVariables {
    /**
     * Returns a common, evaluated primitive range of two code variables,
     * whose type is either primitive or array of primitives.<br>
     *
     * The ranges in the variables must both be evaluated, otherwise a
     * class cast exception is thrown. An exception is a missing range,
     * existing proposal and <code>useProposals</code> being true. The
     * exception is discussed in the method <code>rangeCanBeConstant</code>.<br>
     *
     * The variables must be of the same
     * type, being arithmetic or boolean.<br>
     *
     * Note that, if <code>useProposals</code> is true, this method can
     * create primitive range in a variable resulting from a merge, even
     * if not both or none of the input variables had their primitive ranges.
     *
     * @param v1                        first variable
     * @param v2                        second variable
     * @param useProposals              take into account proposals, if any,
     *                                  when constructing the common range;
     *                                  if false, finding a proposal causes a
     *                                  runtime exception
     * @return                          range of a variable being the merge
     *                                  of the two variables
     */
    protected static CodeVariablePrimitiveRange commonPrimitiveRange(
            CodeVariable v1, CodeVariable v2, boolean useProposals) {
        Type v1Type = v1.type.isArray(null) ? v1.type.getElementType(null, 0) : v1.type;
        Type v2Type = v2.type.isArray(null) ? v2.type.getElementType(null, 0) : v2.type;
        CodeVariablePrimitiveRange p1 = v1.primitiveRange;
        CodeVariablePrimitiveRange p2 = v2.primitiveRange;
        StreamPos commonPos;
        if(p1 != null && p2 != null && p1.getStreamPos() != null &&
                p1.getStreamPos().equals(p2.getStreamPos()))
            // the ranges being merged stem from a single range, let the
            // resulting range inherit the common position
            commonPos = new StreamPos(p1.getStreamPos());
        else
            commonPos = null;
        CodeVariablePrimitiveRange r = new CodeVariablePrimitiveRange(
                commonPos,
                null, null,
                false);
        if(!useProposals &&
                (v1.proposal != null || v2.proposal != null))
            throw new RuntimeException("unexpected proposal");
        try {
            RangeCodeStaticAnalysis.Proposal proposal1 = v1.getFinalConstantRange(useProposals);
            RangeCodeStaticAnalysis.Proposal proposal2 = v2.getFinalConstantRange(useProposals);
        /*
        boolean extremesFound = false;
        if(proposal1.isEmpty() && proposal2.isEmpty()) {
            // both empty, set some minimal range
            r.min = new CodeConstant(null, new Literal(0));
            r.max = new CodeConstant(null, new Literal(0));
            extremesFound = true;
        } else if(proposal1.isEmpty() != proposal2.isEmpty()) {
            // one is empty, copy the other to it
            if(proposal1.isEmpty())
                proposal1 = new RangeCodeStaticAnalysis.Proposal(proposal2);
            else
                proposal2 = new RangeCodeStaticAnalysis.Proposal(proposal1);
        } else if(!proposal1.rangeKnown() || !proposal2.rangeKnown()) {

            extremesFound = true;
        }
         */
        /* if(!extremesFound) */
            if(v1Type.isOfIntegerTypes() || v1Type.isOfBooleanType()) {
                if(v1Type.isOfIntegerTypes() != v2Type.isOfIntegerTypes())
                    throw new RuntimeException("type mismatch");
                long min1 = proposal1.min.getMaxPrecisionInteger();
                long max1 = proposal1.max.getMaxPrecisionInteger();
                long min2 = proposal2.min.getMaxPrecisionInteger();
                long max2 = proposal2.max.getMaxPrecisionInteger();
                r.min = new CodeConstant(null, new Literal(Math.min(min1, min2)));
                r.max = new CodeConstant(null, new Literal(Math.max(max1, max2)));
            } else if(v1Type.isOfFloatingPointTypes()) {
                if(!v2Type.isOfFloatingPointTypes())
                    throw new RuntimeException("type mismatch");
                double min1 = proposal1.min.getMaxPrecisionFloatingPoint();
                double max1 = proposal1.max.getMaxPrecisionFloatingPoint();
                double min2 = proposal2.min.getMaxPrecisionFloatingPoint();
                double max2 = proposal2.max.getMaxPrecisionFloatingPoint();
                r.min = new CodeConstant(null, new Literal(Math.min(min1, min2)));
                r.max = new CodeConstant(null, new Literal(Math.max(max1, max2)));
            }
        } catch(CompilerException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
        return r;
    }
    /**
     * Estimates if it is cost--effective to replace two variables
     * with one with resulting range, in the context of the efefctive number
     * of states. It is assumed to be always true for floating point variables, as
     * such a variable can have an infinite number of states if the range is not
     * a point. If the variable is integer, the evaluation is performed as follows.
     * 
     * A variable without evaluated
     * primitive range is treated as one without the primitive range by
     * this method, as the only thing known about such range is that it fits
     * into the variable's type range, just like it is for a variable without
     * a primitive range at all.<br>
     *
     * Let l_i be the length of primitive range r_i (&lt;min_i, max_i&gt;) of
     * integer variable v_i, where length is the number of values in the range
     * max_i - min_1 + 1. Let there be two variables v_1 and v_2 are tested
     * for evaluate if they should be merged. The resulting variable v_3
     * would have the range (&lt;min(min_1, min_2), max(max_1, max_2)^&gt;).
     * The variables are candidates for merging iff
     * l_3 &lt;= l_2*l_3*<code>mergingRatio</code>. A typical value for
     * <code>mergingRatio</code> is 1.<br>
     *
     * If one variable has the primitive range and the other doesn't, or
     * both do not have te primitive range, then the first
     * one's range is known to fit into the other variable's range, and thus the
     * merge variable would have only so many states as the latter variable,
     * thus, this method returns true, as the first variable is likely to have
     * substantially more than one state. Estimation using <code>mergingRatio</code>
     * is not performed in the case, as the effective range of a variable without
     * custom range is backend--dependent, unknown by this method.<br>
     *
     * @param v1                        first variable to merge
     * @param v2                        second variable to merge
     * @param mergingRatio              merging ratio, see the docs of this method
     *                                  for details
     * @param useProposals              passed to <code>commonPrimitiveRange()</code>,
     *                                  see that methods for details
     * @return                          if the merging is estimated as cost--
     *                                  effective
     */
    protected static boolean compactsStates(CodeVariable v1, CodeVariable v2,
            double mergingRatio, boolean useProposals) {
        if(v1.type.isOfFloatingPointTypes() || v2.type.isOfFloatingPointTypes())
            return true;
        boolean effective = false;
        boolean evaluated1 = v1.rangeCanBeConstant(useProposals);
        boolean evaluated2 = v2.rangeCanBeConstant(useProposals);
        if(evaluated1 && evaluated2) {
            CodeVariablePrimitiveRange common = commonPrimitiveRange(v1, v2,
                    useProposals);
            try {
                effective = common.getStatesNum() <=
                        v1.getFinalConstantRange(useProposals).getStatesNum()*
                        v2.getFinalConstantRange(useProposals).getStatesNum()*
                        mergingRatio;
            } catch(CompilerException e) {
                throw new RuntimeException("unexpected: " + e.toString());
            }
        } else
            effective = true;
        return effective;
    }
    /**
     * 
     * If a local variable is replaceable by another.<br>
     * 
     * The argument and return variables are not replaceable,
     * unless an argyment has a prototype, then the argument is
     * replaceable but the proto is not.
     * 
     * @param method method, to which belongs the local
     * @param local local
     * @return if can be replaced
     */
    static boolean localIsReplaceable(CodeMethod method, CodeVariable local) {
        return !method.interactsExternally(local);
    }
    protected static boolean isBranchOrConditionalBarrier(AbstractCodeOp op) {
        if(op instanceof CodeOpBranch)
            return true;
        if(op instanceof CodeOpNone) {
            CodeOpNone n = (CodeOpNone)op;
            for(String s : n.annotations)
                if(CompilerAnnotation.parse(s) == CompilerAnnotation.BARRIER_CONDITIONAL)
                    return true;
        }
        return false;
    }
    /**
     * An optimization, that deduces the number of local variables in a method,
     * by searching for pairs of variables of the same type, that are used in
     * disjunctive code areas, and replacing them with only one of these two
     * variables.<br>
     *
     * Should be run only if there are no reads of uninitialized variables.<br>
     *
     * Does not replace any non--internal variable, the reasons are given in
     * the docs of <code>CodeVariable</code>.<br>
     *
     * Does not replace variables within variable primitive bounds, see
     * <code>AbstractCodeOp.belongsToVariableRange</code> for details.
     *
     * The method used for estimating if merging two variables is cost--effective
     * is <code>compactsStates()</code>, see the method's docs for details.<br>
     *
     * If primitive ranges are in use, it might be better to specify the ranges
     * on internal variables before running this method, and then evaluate the
     * range variables within the ranges, as merging some variables
     * may radically increase the ranges, for example merging -10...10 with 128...132
     * results in -10...132. If the range or ranges do not exist or are
     * not evaluated, respective variable pairs are always merged as
     * <code>compactStates()</code> returns true. See that method's docs for
     * details.<br>
     *
     * @param method                    method to optimize its local variables
     * @param mergingRatio              coefficient for computing merge/not merge
     *                                  tradeoff, see the docs of
     *                                  <code>compactsStates()</code> for details,
     *                                  <code>Double.NaN</code> for default, which
     *                                  is currently 1.0
     * @param onlyInternal              if only the locals added by the compiler
     *                                  should be reduced, according to docs
     *                                  of <code>CodeVariable</code>, this parameter
     *                                  should generally be true
     * @param useProposals              passed to <code>compactStates()</code> and
     *                                  <code>commonPrimitiveRange()</code>
     *                                  see those methods for details; if false and
     *                                  a proposal is found in a local, a runtime
     *                                  exception is thrown
     * @param notArrayReferences        merge only non--array variables; this option is for
     *                                  use by backends that replace array references with
     *                                  direct array variables anyway, and thus merging
     *                                  of these reference variables would not only not decrease
     *                                  the number of backend variables, but might also
     *                                  loose primitive ranges when common primitive
     *                                  range of two merged variables is computed; if this
     *                                  parameter is true, it is assumed that array class is
     *                                  <code>Type.ARRAY_QUALIFIED_CLASS_NAME</code>
     * @param notBranchSources          do not touch sources of branches or
     *                                  of a conditional barrier, if the previous
     *                                  operation assigns to that source; used if TADDs are
     *                                  generated, as merging of branch sources makes them scattered
     *                                  over a method, and it in turn prevents branch merging in
     *                                  <code>GraphTADDGenerator.generateExpression()</code>
     * @param useHeuristics             if true, the method will <i>likely</i> be much faster
     *                                  in general, but <i>possibly</i> its effectiveness
     *                                  will be smaller; a typical value is <code>true</code>
     * @param possibility               a pre--computed possibility or null, if this method
     *                                  should compute it itself; can be incomplete
     *                                  as only <code>CodeValuePossibility.getSingle()</code>
     *                                  is used on it; if true is returned, then this possibility is no more current
     * @return                          if any locals have been reduced
     */
    public static boolean reduceLocals(CodeMethod method, double mergingRatio,
            boolean onlyInternal, boolean useProposals, boolean notArrayReferences,
            boolean notBranchSources, boolean useHeuristics, CodeValuePossibility possibility) {
        boolean stable = true;
// if(method.code.ops.size() == 149)
//     method = method;
        int iterationsNum;
        if(useHeuristics)
            iterationsNum = 2;
        else
            iterationsNum = 1;
        for(int iteration = 0; iteration < iterationsNum; ++iteration) {
            if(Double.isNaN(mergingRatio))
                mergingRatio = 1.0;
/*if(method.signature.toString().indexOf("obj/G") != -1 &&
        method.owner.name.indexOf("OctreeLeafSparse") == -1)
    method = method;*/
            // find the used variables
            Set<CodeVariable> used = new HashSet<>();
            for(AbstractCodeOp op : method.code.ops) {
                used.addAll(op.getLocalSources());
                used.addAll(op.getLocalTargets());
            }
            // ignore PR variables -- they would be merged along with
            // the variables they bound
            for(CodeVariable u : new HashSet<>(used))
                if(u.isVariablePRBound())
                    used.remove(u);
            if(notBranchSources) {
                AbstractCodeOp prev = null;
                for(AbstractCodeOp op : method.code.ops) {
                    if(isBranchOrConditionalBarrier(op) &&
                            // conditional barriers with conditions optimized to
                            // constants might have no preceeding operations
                            prev != null) {
                        for(CodeVariable cv : op.getLocalSources())
                            if(prev.getLocalTargets().contains(cv) &&
                                    cv.getResultType().isBoolean())
                                used.remove(cv);
                    }
                    prev = op;
                }
            }
            CodeVariableTransport transport = TraceValues.traceTransport(method);
            Map<CodeVariable, AbstractCodeValue> args =
                    new HashMap<>();
            if(possibility == null || !stable)
                // no need for a complete trace
                possibility = TraceValues.traceValuesWithoutCasts(method, args, true);
            Set<Type> types = new HashSet<>();
            for(CodeVariable v : method.variables.values()) {
                if((!onlyInternal || v.isInternal()) && used.contains(v)) {
                    Type t = v.getResultType();
                    types.add(t);
                }
                if(!useProposals && v.proposal != null)
                    throw new RuntimeException("unexpected proposal");
            }
            for(Type t : types) {
                if(notArrayReferences && t.isArray(null))
                    continue;
                // variables can be reduces only within the same type
                // the set is sorted just to make the results identical
                // for two identical methods
                Map<String, CodeVariable> sortedSet = new TreeMap<>();
                for(CodeVariable v : method.variables.values())
                    if((!onlyInternal || v.isInternal()) && used.contains(v) &&
                            v.type.equals(t))
                        sortedSet.put(v.name, v);
                Map<String, CodeVariable> setCopy = new TreeMap<>(sortedSet);
                for(CodeVariable v1 : setCopy.values())
                    if(sortedSet.containsKey(v1.name)) {
                        CodeFieldDereference d1 = new CodeFieldDereference(v1);
                        for(CodeVariable v2 : setCopy.values()) {
                            if(localIsReplaceable(method, v2) &&                                
                                    sortedSet.containsKey(v2.name) &&
                                    // check every pair only once
                                    (v1.name.compareTo(v2.name) < 0 ||
                                        !localIsReplaceable(method, v1))) {
                                CodeFieldDereference d2 = new CodeFieldDereference(v2);
// if(d1.variable.name.indexOf("ignore") != -1 ||
//         d2.variable.name.indexOf("ignore") != -1)
//     d2 = d2;
// v1.isInternal();
                                if(!transport.overlap(d1, d2, possibility) &&
                                        compactsStates(v1, v2, mergingRatio, useProposals)) {
                                    // check for compatible ranges
                                    boolean compatibleRanges = true;
                                    if(v1.hasPrimitiveRange(useProposals) && v2.hasPrimitiveRange(useProposals) &&
                                        (!v1.rangeCanBeConstant(useProposals) ||
                                        !v2.rangeCanBeConstant(useProposals)))
                                            // this method determines only compatibility of
                                            // two ranges that have been evaluated as constants,
                                            // or can be evaluated as constants, given there is a
                                            // proposal
                                            compatibleRanges = false;
                                    else if(v1.hasRestrictiveSourceLevelRange() || v2.hasRestrictiveSourceLevelRange()) {
                                        try {
                                            if(!v1.hasPrimitiveRange(useProposals) || !v2.hasPrimitiveRange(useProposals))
                                                compatibleRanges = false;
                                            else
                                                compatibleRanges = v1.getFinalConstantRange(useProposals).isEqual(
                                                        v2.getFinalConstantRange(useProposals));
                                                        //v1.primitiveRange.arithmeticConstantsEqual(
                                                        //v2.primitiveRange);
                                        } catch(CompilerException e) {
                                            throw new RuntimeException("unexpected: " + e.toString());
                                        }
                                    }
                                    if(compatibleRanges) {
                                        // let d2 be replaced with d1
                                        for(AbstractCodeOp op : method.code.ops) {
                                            // the parameter <code>allowReplaceShared</code>
                                            // can be true as the replacement is for a whole
                                            // method, and within its own locals
                                            op.replaceLocalSourceVariable(v2, d1);
                                            op.replaceLocalTargetVariable(v2, d1);
                                        }
                                        // modify the replacement variable's range, if needed
                                        if(v1.hasRestrictiveSourceLevelRange() || v2.hasRestrictiveSourceLevelRange())
                                            // the ranges are equal, as <code>compatibleRanges</code>
                                            // is true
                                            ;
                                        else if(v1.hasPrimitiveRange(useProposals) || v2.hasPrimitiveRange(useProposals)) {
                                            if(!v1.hasPrimitiveRange(useProposals) || !v2.hasPrimitiveRange(useProposals))
                                                // one lacks range, propagate the lack to the replacement one
                                                v1.primitiveRange = null;
                                            else
                                                // both have ranges, compute the common one
                                                v1.primitiveRange = commonPrimitiveRange(v1, v2, useProposals);
                                        }
                                        if(useProposals) {
                                            if(v1.proposal == null || !v1.proposal.rangeKnown() ||
                                                    v2.proposal == null || !v2.proposal.rangeKnown())
                                                // nothing is known now about the merged proposal
                                                v1.proposal = null;
                                            else
                                                v1.proposal = v1.proposal.
                                                        newExtendedToInclude(v2.proposal.min).
                                                        newExtendedToInclude(v2.proposal.max);
                                        }
                                        // v2 does not exists any more in the code
                                        sortedSet.remove(v2.name);
                                        method.variables.remove(v2.name);
                                        transport.replace(d2, d1);
                                        
                                        // refresh the trace
                                        if(useHeuristics)
                                            // false positives may decrease the effectiveness,
                                            // but won't cause incorrect optimization
                                            possibility.replaceFalsePositive(d2, d1);
                                        else
                                            // <code>traceValues</code> is not used due to its
                                            // computational complexity
                                            // no need for a complete trace
                                            possibility = TraceValues.traceValuesWithoutCasts(
                                                    method, args, true);
                                        stable = false;
                                    }
                                }
                            }
                        }
                }
            }
            if(stable)
                break;
        }
        return !stable;
    }
}
