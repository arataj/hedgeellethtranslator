/*
 * Optimizer.java
 *
 * Created on Mar 30, 2008
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.code.optimizer;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.CommentTag;
import pl.gliwice.iitis.hedgeelleth.compiler.code.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.op.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.*;
import pl.gliwice.iitis.hedgeelleth.compiler.primitiverange.CodeVariablePrimitiveRange;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Literal;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Method;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Type;
import pl.gliwice.iitis.hedgeelleth.compiler.util.CompilerUtils;
import pl.gliwice.iitis.hedgeelleth.interpreter.exception.InterpreterException;

/**
 * Optimizations of the generated code.<br>
 *
 * The class <code>TraceValues</code> also contains a number
 * of optimizations.<br>
 *
 * Some methods may invalidate label indices, and only some require the
 * indices to be valid. See docs of individual methods for details
 * of that.<br>
 *
 * Note: for optimization efficiency, non source--level variable primitive
 * ranges are deemed redundant by this methods in this class.
 * Please adhere to the optimization limitations rules given
 * in <code>CodeVariable</code> so that this approach won't cause
 * errors.
 *
 * @author Artur Rataj
 */
public class Optimizer {
    /**
     * If to be verbose.
     */
    boolean verbose = false;

    /**
     * Available optimization sets, note that the sets do not cover
     * all available optimizations:<br>
     * 
     * BASIC -- deletes redundant
     * labelRefs, removes no ops, optimizes branches with
     * static conditions, compacts jump and assignment
     * chains.<br>
     * 
     * TRACED -- uses flow analysis, and is more time--consuming;
     * not used by the TADD backend, as it may remove information
     * important for that backend; the backend uses its own,
     * specialized version of flow analysis optimizations.<nr>
     * 
     * STATIC -- static analysis of the code. The TADD
     * backend performs a runtime variant of this optimization.<br>
     */
    public enum Optimization {
        BASIC,
        TRACED,
        STATIC,
    };
    public static class Tuning {
        /**
         *   If to tune the optimizations for further generation of TADDs.
         *   The default is false.
         */
        public boolean tuneToTADDs = false;
        /**
            * If ranged locals should be preserved. The default is false.
            * <i>Should be true only for certain compiler tests</i>.
            */
        public boolean preserveRangedLocals = false;
        /**
         * If to check transport of a local primitive variable, in order to
         * delete assignments that do not transport anything.
         * The default is true. <i>Should be false only for certain compiler tests</i>.
         */
        public boolean assignmentTransportCheck = true;
        /**
         * Copied to RuntimeCallResolution.TRACE_ALLOC. False only for
         * tests.
         */
        public boolean traceAllocInCallResolution = true;
    }
    /**
     * Optimizations to perform.
     */
    List<Optimization> optimizations;
    
    /**
     * Creates a new instance of Optimizer. Note that the sets defined
     * by <code>Optimization</code> objects do not cover all available
     * optimizations.<br>
     *
     * Instead of instantiating the object and then running
     * <code>optimize</code>, static methods <code>optimizeBasic</code>
     * and <code>optimizeTraced</code> can be invoked. In such a case
     * care should be taken for conditions on the label indices,
     * though.
     * 
     * @param optimizations             optimizations to perform,
     *                                  the list is copied to another
     *                                  one so that the caller can
     *                                  modify it after calling the
     *                                  constructor. The order of
     *                                  optimizations
     *                                  is determined by this class
     *                                  as necessary, the order in this
     *                                  list is meaningless
     */
    public Optimizer(List<Optimization> optimizations) {
        this.optimizations = new LinkedList<>(optimizations);
    }
    /**
     * Optimizes a given compilation, using the optimizations
     * declared in the constructor. Runs the declared optimizations
     * in a correct order.<br>
     *
     * Label indices do not need to be valid. After the optimisation,
     * they are guaranteed to be valid.<br>
     *
     * 
     * @param compilation               compilation to optimize
     * @param tuning                    options for the optimizer
     * 
     */
    public void optimize(CodeCompilation compilation,
            Tuning tuning) {
        // check if all optimizations are known by this method
        for(Optimization o : Optimization.values())
            switch(o) {
                case BASIC:
                case TRACED:
                case STATIC:
                    break;

                default:
                    throw new RuntimeException("unknown optimization: " + o);
            }
        // optimization BASIC
        Collection<CodeMethod> methods =
                compilation.getMethods();
        if(optimizations.contains(Optimization.BASIC)) {
            log(0, "Optimization " + Optimization.BASIC);
            for(CodeMethod m : methods) {
//if(m.owner.name.contains("Periodic") && m.signature.name.contains("run"))
//    m = m;
                optimizeBasic(m, true, tuning);
//if(m.owner.name.contains("Ternary") && m.signature.name.contains("run"))
//    m = m;
            }
        } else
            setLabelIndices(compilation);
        // optimization TRACED
        if(optimizations.contains(Optimization.TRACED)) {
            log(0, "Optimization " + Optimization.TRACED);
            for(CodeMethod m : methods) {
//if(m.signature.name.contains("produce"))
//    m = m;
                if(m.code != null) {
                    Map<CodeVariable, AbstractCodeValue> arguments =
                            new HashMap<>();
                    // it is not a complete trace
                    CodeValuePossibility possibility = TraceValues.traceValuesWithoutCasts(
                            m, arguments, true);
                    if(TraceValues.propagateValues(m, arguments, possibility,
                            null, null, null)) {
                        optimizeBasic(m, true, tuning);
                        // it is not a complete trace
                        possibility = TraceValues.traceValuesWithoutCasts(
                            m, arguments, true);
                    }
                    if(optimizeDeadAndLocals(m, false, false, tuning.tuneToTADDs, possibility)) {
                        optimizeBasic(m, true, tuning);
                        // it is not a complete trace
                        possibility = TraceValues.traceValuesWithoutCasts(
                            m, arguments, true);
                    }
//if(m.owner.name.contains("Ternary") && m.signature.name.contains("run"))
//    m = m;
                    if(TraceValues.removeRedundantAssignments(m, null, possibility,
                            tuning.assignmentTransportCheck)) {
                        optimizeBasic(m, true, tuning);
                        // it is not a complete trace
                        possibility = TraceValues.traceValuesWithoutCasts(
                            m, new HashMap<CodeVariable, AbstractCodeValue>(),
                            true);
                    }
//if(m.owner.name.contains("Ternary") && m.signature.name.contains("run"))
//    m = m;
// if(m.signature.toString().indexOf("denoise") != -1)
//     m = m;
                    if(TraceValues.forwardJumpsWithBranches(m, null, possibility))
                        optimizeBasic(m, true, tuning);
                }
            }
        }
        // optimization STATIC, run after TRACED so that there is more
        // chance of optimizing something, that <code>StaticAnalysis</code>
        // did not
        if(optimizations.contains(Optimization.STATIC)) {
            log(0, "Optimization " + Optimization.STATIC);
            for(CodeMethod m : methods)
                try {
                    CodeStaticAnalysis.staticAnalysisLooped(null, m);
                } catch(InterpreterException e) {
                    /* variable value out of bounds, ignore */
                }
        }
        if(verbose)
            log(1, "optimized code:\n" +
                    methods.toString());
    }
    /**
     * Sets up label indices in a code method so that
     * the code can be executed by the interpreter, translated,
     * or optimized by methods that require the indices to
     * be valid.<br>
     * 
     * The code from the generator does not have the indices
     * set up at all, and some optimizations may invalidate
     * the indices.
     * 
     * @param method                    code method
     */
    public static void setLabelIndices(CodeMethod method) {
        CodeLabel.setIndices(method.code);
    }
    /**
     * Sets up label indices in the entire compilation so that
     * the code can be executed.
     * 
     * @param compilation               code compilation
     */
    public static void setLabelIndices(CodeCompilation compilation) {
        Collection<CodeMethod> methods =
                compilation.getMethods();
        for(CodeMethod m : methods)
            CodeLabel.setIndices(m.code);
    }
    /**
     * If an operation does nothing, that is, it is either a no op without
     * annotations or a jump to some operation in the following sequence
     * of operations that do nothing, or an unused goto, or a branch
     * whose both labels are null, or appropriately marked assignments
     * and casts.<br>
     * 
     * Assignments and casts are marked to be recognized by this method as ones
     * that do nothing by setting their right values to null.
     * 
     * @param ops                       list of operations
     * @param op                        operation in the list to test
     * @return                          if the operation does nothing
     */
    static boolean doesNothing(List<AbstractCodeOp> ops, AbstractCodeOp op) {
        if(op instanceof CodeOpNone)
            return ((CodeOpNone)op).annotations.isEmpty();
        else if(op instanceof CodeOpJump) {
            int index = ops.indexOf(op);
            AbstractCodeOp next;
            // check if not jumping to next operation
            int i = index;
            do {
                ++i;
                if(i == ops.size())
                    break;
                next = ops.get(i);
                if(next.label != null &&
                        ((CodeOpJump)op).gotoLabelRef.equals(next.label))
                    return true;
            }  while(doesNothing(ops, next));
            // check if not an unused goto after another goto or return
            // or an unrelated branch
            if(index > 0) {
                AbstractCodeOp prev = ops.get(index - 1);
                boolean unrelatedBranch = false;
                if(prev instanceof CodeOpBranch) {
                    CodeOpBranch b = (CodeOpBranch)prev;
                    if(b.labelRefFalse != null && b.labelRefTrue != null) {
                        if(op.label == null)
                            unrelatedBranch = true;
                        else {
                            boolean fJumps = b.labelRefFalse.equals(op.label);
                            boolean tJumps = b.labelRefTrue.equals(op.label);
                            if(!fJumps && !tJumps)
                                unrelatedBranch = true;
                        }
                    }
                }
                if(prev instanceof CodeOpJump ||
                        prev instanceof CodeOpReturn ||
                        prev instanceof CodeOpThrow ||
                        unrelatedBranch) {
                    boolean labelRefFound = false;
                    if(op.label != null)
                        LABEL_REF_SCAN:
                        for(AbstractCodeOp o : ops)
                            for(CodeLabel labelRef : o.getLabelRefs())
                                if(labelRef.equals(op.label)) {
                                    labelRefFound = true;
                                    break LABEL_REF_SCAN;
                                }
                   if(!labelRefFound)
                       return true;
                }
            }
            return false;
        } else if(op instanceof CodeOpBranch) {
            CodeOpBranch b = (CodeOpBranch)op;
            return b.labelRefFalse == null && b.labelRefTrue == null;
        } else if(op instanceof CodeOpAssignment) {
            CodeOpAssignment a = (CodeOpAssignment)op;
            boolean doesNothing = a.rvalue == null;
            /*
            // optimizer can assign opaque  assignments for deletion if
            // the opaqueness is moved to another operation, so this
            // check is not used any more
            if(doesNothing && a.isOpaque())
                throw new RuntimeException("opaque assignment marked as one " +
                        "that does nothing");
             */
            return doesNothing;
        } else if(op instanceof CodeOpUnaryExpression) {
            CodeOpUnaryExpression u = (CodeOpUnaryExpression)op;
            if(u.op != CodeOpUnaryExpression.Op.UNARY_CAST)
                return false;
            else
                return u.result == null;
        } else
            return false;
    }
    /**
     * Optimizes branches with static conditions.<br>
     *
     * This is a part of the BASIC optimizations.
     *
     * @param code                      code to optimize
     * @return                          if anything changed
     */
    public static boolean optimizeStaticBranches(Code code) {
        boolean stable = true;
        List<AbstractCodeOp> opsCopy = new LinkedList<>(code.ops);
        int index = 0;
        for(AbstractCodeOp op : opsCopy) {
            if(op instanceof CodeOpBranch) {
                CodeOpBranch b = (CodeOpBranch)op;
                if(!b.condition.hasRestrictiveSourceLevelRange() &&
                        b.condition.value instanceof CodeConstant) {
                    Literal l = (Literal)((CodeConstant)b.condition.value).value;
                    CodeLabel destinationLabel;
                    if(l.getBoolean())
                        destinationLabel = b.labelRefTrue;
                    else
                        destinationLabel = b.labelRefFalse;
                    AbstractCodeOp newOp;
                    if(destinationLabel == null) {
                        newOp = new CodeOpNone(b.getStreamPos());
                    } else {
                        CodeOpJump jump = new CodeOpJump(
                                b.getStreamPos());
                        jump.gotoLabelRef = new CodeLabel(destinationLabel);
                        newOp = jump;
                    }
                    code.replace(index, newOp, true);
                    stable = false;
                }
            }
            ++index;
        }
        return !stable;
    }
    /**
     * Optimizes branches with null labels, that are immediately
     * followed by jumps, by replacing the null labels with
     * that of the jumps. If a label, in turn, point to a
     * next operation that is not a jump, then the label
     * is replaced with null.<br>
     *
     * This is a part of the BASIC optimizations.
     *
     * @param code                      code to optimize
     * @return                          if the code has been changed
     *                                  by this method
     */
    public static boolean optimizeNullBranches(Code code) {
        boolean stable = true;
        for(int index = 0; index < code.ops.size() - 1; ++index) {
            AbstractCodeOp op = code.ops.get(index);
            if(op instanceof CodeOpBranch) {
                CodeOpBranch b = (CodeOpBranch)op;
                op = code.ops.get(index + 1);
                if(op instanceof CodeOpJump) {
                    CodeOpJump j = (CodeOpJump)op;
                    if(b.labelRefFalse == null) {
                        b.labelRefFalse = new CodeLabel(j.gotoLabelRef);
                        stable = false;
                    }
                    if(b.labelRefTrue == null) {
                        b.labelRefTrue = new CodeLabel(j.gotoLabelRef);
                        stable = false;
                    }
                    if(!code.isJumpTarget(index + 1)) {
                        CodeOpNone newOp = new CodeOpNone(j.getStreamPos());
                        code.replace(index + 1, newOp, true);
                        stable = false;
                    }
                } else if(op.label != null){
                    if(b.labelRefFalse != null && b.labelRefFalse.equals(op.label))
                        b.labelRefFalse = null;
                    if(b.labelRefTrue != null && b.labelRefTrue.equals(op.label))
                        b.labelRefTrue = null;
                }
            }
        }
        return !stable;
    }
    /**
     * Optimizes labels, no ops, jumps to a subsequent used
     * operation, unused jumps and operations that do nothing
     * according to <code>doesNothing()</code>.
     * If a branch label jumps to
     * a subsequent operation, it is set to null.<br>
     * 
     * This is a part of the BASIC optimizations.<br>
     * 
     * Invalidates possible label indices.
     * 
     * @param code                      code to optimize
     * @return                          if anything has changed
     */
    public static boolean optimizeLabels(Code code) {
        boolean stable = true;
        HashMap<CodeLabel, CodeLabel> labelRefs =
                new HashMap<>();
        for(int index = 0; index < code.ops.size(); ++index) {
            AbstractCodeOp op = code.ops.get(index);
            if(op instanceof CodeOpBranch && index < code.ops.size() - 1) {
                CodeOpBranch b = (CodeOpBranch)op;
                AbstractCodeOp next = code.ops.get(index + 1);
                CodeLabel nextLabel = next.label;
                if(nextLabel != null) {
                    // set not--null branch labels to null if they point to the
                    // next operation
                    if(b.labelRefFalse != null &&
                            b.labelRefFalse.equals(nextLabel)) {
                        b.labelRefFalse = null;
                        stable = false;
                    }
                    if(b.labelRefTrue != null &&
                            b.labelRefTrue.equals(nextLabel)) {
                        b.labelRefTrue = null;
                        stable = false;
                    }
                }
            }
            for(CodeLabel l : op.getLabelRefs())
                labelRefs.put(l, l);
        }
        //
        // remove unused labelRefs
        //
        for(AbstractCodeOp op : code.ops)
            if(op.label != null && !labelRefs.containsKey(op.label)) {
                op.label = null;
                stable = false;
            }
        boolean[] forRemoval = new boolean[code.ops.size()];
        //
        // remove no ops, jumps to a subsequent used operation and
        // unused jumps
        //
        int count = 0;
        for(AbstractCodeOp op : code.ops) {
            forRemoval[count] = doesNothing(code.ops, op);
            ++count;
        }
        // pairs label to move, new value
        List<CodeLabel[]> labelRefsToMove = new LinkedList<>();
        count = 0;
        Iterator<AbstractCodeOp> i = code.ops.iterator();
        AbstractCodeOp nextOp = null;
        while(i.hasNext()) {
            AbstractCodeOp op;
            if(nextOp == null)
                op = i.next();
            else {
                op = nextOp;
                nextOp = null;
            }
            if(forRemoval[count]) {
                if(op.label != null || !op.getTags().isEmpty()) {
                    if(i.hasNext()) {
                        CodeLabel current = op.label;
                        Collection<CommentTag> tags = op.getTags();
                        i.remove();
                        nextOp = i.next();
                        if(op.label != null)
                            if(nextOp.label == null)
                                nextOp.label = current;
                            else {
                                CodeLabel[] p = {
                                    labelRefs.get(current),
                                    nextOp.label,
                                };
                                for(CodeLabel[] q : labelRefsToMove)
                                    if(q[1].equals(p[0]))
                                        q[1] = p[1];
                                labelRefsToMove.add(p);
                            }
                        nextOp.addTags(tags);
                    }
                } else
                    i.remove();
                stable = false;
            }
            ++count;
        }
        // scan the label references backwards
        for(CodeLabel[] p : labelRefsToMove) {
            CodeLabel moved = new CodeLabel(p[0]);
            CodeLabel target = p[1];
            for(AbstractCodeOp op : code.ops)
                for(CodeLabel l : op.getLabelRefs())
                    if(l.equals(moved))
                        l.set(target);
        }
        return !stable;
    }
    /**
     * Computes label to operation number map. Used by
     * <code>optimizeAssignments</code>.
     * 
     * @param code                      code for which the index
     *                                  is to be computed
     * @return                          map of labels
     */
    protected static Map<CodeLabel, Integer> computeLabelIndex(Code code) {
        HashMap<CodeLabel, Integer> labelIndex =
                new HashMap<CodeLabel, Integer>();
        int index = 0;
        for(AbstractCodeOp op : code.ops) {
            if(op.label != null)
                labelIndex.put(op.label, index);
            ++index;
        }
        return labelIndex;
    }
    /**
     * If there is a sequence of two operations at a given index,
     * the former is an assignment of a constant
     * to a java type local <i>d</i>, and
     * the latter is an assignment to a field, and the field's
     * object is <i>d</i>.
     * 
     * @param code code
     * @param index beginning of the sequence
     * @return if the sequence does occur
     */
    private static boolean determinedField(Code code, int index) {
        if(code.ops.get(index) instanceof CodeOpAssignment) {
            CodeOpAssignment objectAssignment = (CodeOpAssignment)code.ops.get(index);
            if(objectAssignment.rvalue == null)
                // an optimized--out assignment 
                return false;
            AbstractCodeDereference object = objectAssignment.getResult();
            if(objectAssignment.rvalue.value instanceof CodeConstant &&
                    object instanceof CodeFieldDereference &&
                    ((CodeFieldDereference)object).isLocal() &&
                    code.ops.get(index + 1) instanceof CodeOpAssignment) {
                CodeVariable o = ((CodeFieldDereference)object).variable;
                CodeOpAssignment a = (CodeOpAssignment)code.ops.get(index + 1);
                if(a.getResult() instanceof CodeFieldDereference) {
                    CodeFieldDereference r = (CodeFieldDereference)a.getResult();
                    return r.object == o;
                }
            }
        }
        return false;
    }
    /**
     * Optimizes assignments and casts. This is a part of the BASIC
     * optimizations.<br>
     *
     * If operations' result is used only as rvalue in some assignment,
     * then the assignment is removed, and the operation's result is
     * changed to the assignment's result. Additionally, removes unnecessary
     * assignments, casts and unused results of call operations.<br>
     *
     * In a sense, it works in a opposite direction to propagation of
     * values in <code>TraceValues</code>, in this way, the variables
     * removed are possibly more often the internal ones, created along
     * with operations that have a result.<br>
     * 
     * It assumes that fields can be accessed in methods and other
     * threads, therefore it optimizes them only in certain cases.<br>
     * 
     * Invalidates possible label indices.
     * 
     * @param code                      code to optimize
     * @return                          if anything has been changed
     */
    public static boolean optimizeAssignments(Code code) {
        boolean totalStable = true;
        boolean stable;
        do {
// if(code.ops.size() >= 9)
//     code = code;
            stable = true;
            Map<CodeLabel, Integer> labelIndex =
                    computeLabelIndex(code);
            List<AbstractCodeOp> backwards = new LinkedList<>();
            for(AbstractCodeOp op : code.ops)
                backwards.add(0, op);
            // traces assignments of one variable to another
            //
            // no key -- not copied, key with dereference --
            // copied only to the variable, key with null -- other
            //
            // the keys are only locals, as only writes to them
            // are optimized
            Map<CodeVariable, AbstractCodeDereference> copied = new HashMap<>();
            // traces usages
            //
            // Key is present if a variable was used, value expresses,
            // down to which index the usage is meaningfull. For
            // example, if op at 10 uses x, is not a jump target and
            // op at 9 is not a jump target and op at 8 writes to x,
            // then the index would be 8, as no op at 7 or less
            // can influence on the op at 10 through x.
            //
            // the elements are only locals, as only writes to them
            // are optimized
            Map<CodeVariable, Integer> usages = new HashMap<>();
            // if copied map returns a variable for a given key, then this
            // map returns the greatest index of the assignments that
            // use the key
            // this is to prevent deletion of assignments of non--locals
            // that are not the directly following operation of the
            // assignment with the modified result
            Map<CodeVariable, Integer> copiedMaxIndex = new HashMap<>();
            // index of the current operation
            int currIndex = backwards.size() - 1;
            // minimum index of target instructions of the jumps found so
            // far
            int jumpMinIndex = currIndex + 1;
            /*
            if(code.ops.size() > 3)
                code = code;
                */
            // previous operation in this loop, that is, next operation
            // in the list of operations
            // this variable is needed for an additional optimization:
            // if the next operation writes exactly to the same variable
            // as this one and does not have that variable in its sources,
            // it is known that this operation's result is not used
            AbstractCodeOp prevOp = null;
            for(AbstractCodeOp op : backwards) {
                Collection<CodeVariable> source = new LinkedList<>(
                        op.getLocalSources());
                AbstractCodeDereference result = op.getResult();
/*if(op.toString().equals("i#0example.MicrogridFast_getNumJobs__#retval = i#0example.MicrogridFast_getNumJobs__numJobs"))
    op = op;*/
/*
if(op.toString().equals("i#1 = #c26"))
    op = op;
    */
/*
if(result instanceof CodeFieldDereference &&
        ((CodeFieldDereference)result).variable.name.contains("replace"))
    result = result;
    */
                if(result != null && result instanceof CodeFieldDereference &&
                        ((CodeFieldDereference)result).isLocal()) {
                    CodeVariable fResult = ((CodeFieldDereference)result).variable;
                    // check the special condition described in the comment
                    // of prevOp
                    boolean instantOverwrite = prevOp != null &&
                            prevOp.getResult() != null &&
                            prevOp.getResult().equals(result) &&
                            !prevOp.getLocalSources().contains(fResult);
                    // optimize only writes to locals
                    AbstractCodeDereference copiedBy = copied.get(fResult);
                    if(copiedBy != null || !usages.keySet().contains(fResult) ||
                            instantOverwrite) {
                        // copied to a single variable or no usages at all
                        boolean foundPreceedingUsages = false;
                        if(!instantOverwrite && jumpMinIndex <= currIndex) {
                            // but there were jumps before the current operation
                            // check if they do not contain the usages
                            SEARCH_PRECEEDING_USAGES:
                            for(int i = jumpMinIndex; i <= currIndex; ++i) {
                                AbstractCodeOp o = code.ops.get(i);
                                // if this is a jump, check if within the
                                // scanned range, otherwise do not optimize
                                // the assignment because the jumps are not
                                // traced
                                if(o instanceof CodeOpJump ||
                                        o instanceof CodeOpBranch) {
                                    for(CodeLabel ref : o.getLabelRefs())
{if(!labelIndex.containsKey(ref))
    o = o;
                                        if(labelIndex.get(ref) < jumpMinIndex) {
                                            foundPreceedingUsages = true;
                                            break SEARCH_PRECEEDING_USAGES;
                                        }
}
                                }
                                Collection<CodeVariable> s = o.getLocalSources();
                                for(CodeVariable used : s)
                                    if(used == fResult) {
                                        foundPreceedingUsages = true;
                                        break SEARCH_PRECEEDING_USAGES;
                                    }
                            }
                        }
                        if(!foundPreceedingUsages) {
                            if(!instantOverwrite && copiedBy != null) {
                                // there are one or more assignments of this variable
                                // that are all to a single another variable
                                //
                                // if the target is volatile, it is required that
                                // there is only one copying instruction and
                                // it is the next one, because a field can be
                                // modified within called methods or other threads
                                int maxIndex = copiedMaxIndex.get(fResult);
                                boolean determinedField = false;
                                if(!copiedBy.isVolatile() || maxIndex - currIndex == 1 ||
                                        (maxIndex - currIndex == 2 &&
                                            (determinedField = determinedField(code, currIndex + 1) &&
                                            !op.getLocalSources().contains(code.ops.get(currIndex + 1).
                                                getResult().extractPlainLocal().iterator().next())))) {
                                    boolean interveningJump = false;
                                    // first check if there is a jump to an operation
                                    // in the region from after the current operation to
                                    // the furthest copying assignment, from outside of
                                    // the region, or if any of the operations is not
                                    // a jump to outside itself, as it might make the target
                                    // assignment needed
                                    CHECK_JUMPS:
                                    for(int jt = currIndex + 1; jt <= maxIndex; ++jt) {
                                        AbstractCodeOp o = code.ops.get(jt);
                                        if(o.label != null) {
                                            // so the op has a label, check jumps
                                            // to the label
                                            for(int js = 0; js < code.ops.size(); ++js) {
                                                AbstractCodeOp p = code.ops.get(js);
                                                for(CodeLabel ref : p.getLabelRefs())
                                                    if(ref.equals(o.label) && (js < currIndex + 1 ||
                                                            js > maxIndex)) {
                                                        interveningJump = true;
                                                        break CHECK_JUMPS;
                                                    }
                                            }
                                        }
                                        // check if this op is not a jump
                                        if(o instanceof CodeOpJump ||
                                                o instanceof CodeOpBranch) {
                                            for(CodeLabel ref : o.getLabelRefs())
                                                if(labelIndex.get(ref) <= currIndex ||
                                                        labelIndex.get(ref) > maxIndex) {
                                                    interveningJump = true;
                                                    break CHECK_JUMPS;
                                                }
                                        }
                                    }
                                    if(!interveningJump) {
                                        // check if <code>copiedBy</code> is used anywhere
                                        // in between in a manner different than
                                        // <code>copiedBy = result</code>
                                        boolean copiedUsedElsewhere = false;
                                        // if <code>copiedBy</code> is not a local,
                                        // then the copying assignment is just the next
                                        // operation or the next two operations in the case of
                                        // a determined field, so <code>copiedBy</code> can not
                                        // be used in between
                                        if(!copiedBy.extractPlainLocal().isEmpty()) {
                                            CodeVariable c = copiedBy.extractPlainLocal().iterator().next();
                                            for(int i = currIndex + 1; i < maxIndex; ++i) {
                                                AbstractCodeOp o = code.ops.get(i);
                                                Collection<CodeVariable> u = o.getLocalSources();
                                                u.addAll(o.getLocalTargets());
                                                if(u.contains(c)) {
                                                    if(!(o instanceof CodeOpAssignment) ||
                                                            !o.getResult().equals(copiedBy)) {
                                                        copiedUsedElsewhere = true;
                                                        break;
                                                    } else {
                                                        if(((CodeOpAssignment)o).rvalue == null) {
                                                            copiedUsedElsewhere = true;
                                                            break;
                                                        }
                                                        AbstractCodeValue r = ((CodeOpAssignment)o).rvalue.value;
                                                        if(!(r instanceof CodeFieldDereference)) {
                                                            copiedUsedElsewhere = true;
                                                            break;
                                                        } else {
                                                            CodeFieldDereference f = (CodeFieldDereference)r;
                                                            if(f.extractPlainLocal().isEmpty() ||
                                                                    f.extractPlainLocal().iterator().next() != fResult) {
                                                                copiedUsedElsewhere = true;
                                                                break;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        if(!copiedUsedElsewhere) {
                                            boolean opaqueNotCompatibleFound = false;
                                            CodeVariablePrimitiveRange opaqueButPerhapsCompatible = null;
                                            // search for opaque lvalue on this operation
                                            if(    // <code>op</code> has a non--null result, therefore
                                                   // it must be an instance of <code>AbstractResultCodeOp</code>
                                                    ((AbstractResultCodeOp)op).resultIsOpaque()) {
                                                // this operation has source--level range check on its result,
                                                // thus, the result can not be replaced
                                                opaqueNotCompatibleFound = true;
                                                // try one exception -- field dereference or index not opaque
                                                // and replacement has the same range
                                                if(opaqueNotCompatibleFound &&
                                                        (op.getResult() instanceof CodeFieldDereference ||
                                                        !((CodeIndexDereference)op.getResult()).index.hasRestrictiveSourceLevelRange())) {
                                                    opaqueButPerhapsCompatible = op.getResult().getTypeVariablePrimitiveRange();
                                                    if(!opaqueButPerhapsCompatible.isEvaluatedAsConstant())
                                                        // only constant ranges are compared by this method
                                                        opaqueButPerhapsCompatible = null;
                                                }
                                            }
                                            if(!opaqueNotCompatibleFound || opaqueButPerhapsCompatible != null)
                                                // search for opaque assignments
                                                for(AbstractCodeOp o : backwards)
                                                    if(o == op)
                                                        break;
                                                    else if(o instanceof CodeOpAssignment) {
                                                        CodeOpAssignment a = (CodeOpAssignment)o;
                                                        if(a.rvalue != null && a.rvalue.value.equals(result)) {
                                                            CodeVariablePrimitiveRange aRange =
                                                                    a.getResult().getTypeVariablePrimitiveRange();
                                                            if(a.rvalue.range != null) {
                                                                // can not remove that range, thus, can not remove
                                                                // the assignment
                                                                opaqueNotCompatibleFound = true;
                                                            } else if(opaqueButPerhapsCompatible != null) {
                                                                if(aRange == null)
                                                                    // this assignments's
                                                                    // lvalue's range can not replace op's result
                                                                    // range, as lvalue does not have one
                                                                    opaqueNotCompatibleFound = true;
                                                                else if(!aRange.isEvaluatedAsConstant() ||
                                                                        !opaqueButPerhapsCompatible.arithmeticConstantsEqual(
                                                                        aRange))
                                                                    // lvalue's range can not replace op's result
                                                                    // range, as lvalue has the range arithmetically
                                                                    // different, or one that can not be compared
                                                                    // using
                                                                    // <code>CodeVariablePrimitiveRange.arithmeticConstantsEqual</code>
                                                                    opaqueNotCompatibleFound = true;
                                                            }
                                                            // if result of <code>op</code> is not opaque, then
                                                            // opaqueness of lvalue might still exist, as the lvalue is
                                                            // transferred to <code>op</code> anyway, thus, possible
                                                            // variable primitive range would be transferred there too;
                                                            // there is one problem here, though -- the variable primitive
                                                            // range should not contain <i>local</i> range variables,
                                                            // as the variables
                                                            // might have different values at <code>op<code>, what is not
                                                            // checked by this method -- test the range for variables;
                                                            // the variable primitive range can contain <i>field</i> variables,
                                                            // though, as it means that this assignment is to a field
                                                            // too, thus, it is volatile, thus, it is next after
                                                            // <code>op</code> and no intervening jump, thus, no way to modify
                                                            // the field variables in lvalue's variable range in between these
                                                            // two ops
                                                            if(aRange != null && !aRange.isEvaluatedAsConstant()) {
                                                                if(a.getResult().getTypeSourceVariable().isLocal())
                                                                    opaqueNotCompatibleFound = true;
                                                            }
                                                            if(opaqueNotCompatibleFound) {
                                                                // at least one assignment with opaque rvalue
                                                                // which uses this op's result, and opaqueness does not match,
                                                                // or is not known to match by this method
                                                                //
                                                                // can not be deleted, and thus, this op can not be modified
                                                                // to substitute the assignments, and so the assignments that
                                                                // use this op's results as their rvalue are left intact as
                                                                // well
                                                                break;
                                                            }
                                                        }
                                                    }
                                            if(!opaqueNotCompatibleFound) {
                                                // modify the current assignment,
                                                // mark the assignments to delete
                                                for(AbstractCodeOp o : backwards) {
                                                    // !! 2 lines added
                                                    if(o.getResult() != null && o.getResult().equals(result))
                                                        o.setResult(copiedBy);
                                                    if(o == op)
                                                        break;
                                                    else if(o instanceof CodeOpAssignment) {
                                                        CodeOpAssignment a = (CodeOpAssignment)o;
                                                        if(a.rvalue != null && a.rvalue.value.equals(result))
                                                            a.rvalue = null;
                                                    }
                                                }
                                                stable = false;
                                                // !! was op.setResult(copiedBy);
                                                // assignment changed or maked for deletion,
                                                // the usages are no more actual
                                                copied.values().remove(result);
                                                /*
                                                for(AbstractCodeOp o : backwards)
                                                    if(o == op)
                                                        break;
                                                    else if(o instanceof CodeOpAssignment) {
                                                        CodeOpAssignment a = (CodeOpAssignment)o;
                                                        if(a.rvalue != null && a.rvalue.equals(result))
                                                            a.rvalue = null;
                                                    }
                                                 */
                                                if(determinedField) {
                                                    // a special case -- move the assignment after the
                                                    // assignment to an object variable
                                                    code.ops.remove(currIndex + 2);
                                                    code.ops.add(currIndex + 2, op);
                                                    code.replace(currIndex, new CodeOpNone(
                                                            op.getStreamPos()), true);
                                                    op.label = null;
                                                    result = null;
                                                } else
                                                    result = copiedBy;
                                            }
                                        }
                                    }
                                }
                            } else {
                                // there were no usages at all
                                // of the assigned value within
                                // this method
                                if(op instanceof CodeOpAssignment) {
                                    CodeOpAssignment a = (CodeOpAssignment)op;
                                    if(!(a.result instanceof CodeFieldDereference &&
                                            ((CodeFieldDereference)a.result).variable.name.equals(
                                            Method.LOCAL_RETURN))) {
                                        // nothing is using the result
                                        // check if opaque
                                        if(!a.isOpaque()) {
                                            a.rvalue = null;
                                            stable = false;
                                            // this assignment will be deleted,
                                            // ignore its sources
                                            source.clear();
                                        }
                                    }
                                } else if(op instanceof CodeOpUnaryExpression) {
                                    CodeOpUnaryExpression u = (CodeOpUnaryExpression)op;
                                    if(u.op == CodeOpUnaryExpression.Op.UNARY_CAST &&
                                            !(u.result instanceof CodeFieldDereference &&
                                            ((CodeFieldDereference)u.result).variable.name.equals(
                                            Method.LOCAL_RETURN))) {
                                        // nothing is using the result
                                        // check if has ranges
                                        if(u.sub.range == null && !u.resultIsOpaque()) {
                                            u.result = null;
                                            stable = false;
                                            // this assignment will be deleted,
                                            // ignore its sources
                                            source.clear();
                                        }
                                    }
                                } else if(op instanceof CodeOpCall) {
                                    CodeOpCall c = (CodeOpCall)op;
                                    if(!(c.result instanceof CodeFieldDereference &&
                                            ((CodeFieldDereference)c.result).variable.name.equals(
                                            Method.LOCAL_RETURN))) {
                                        // nothing is using the result
                                        // check if has ranges
                                        boolean hasRanges = false;
                                        for(RangedCodeValue rv : c.args)
                                            if(rv.range != null) {
                                                hasRanges = true;
                                                break;
                                            }
                                        if(!hasRanges && !c.resultIsOpaque()) {
                                            c.result = null;
                                            stable = false;
                                            // a call whose result is not used still can
                                            // have side effects, thus, take its sources
                                            // into account
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if(op instanceof CodeOpJump ||
                        op instanceof CodeOpBranch) {
                    /*
                    // this optimization does not track jumps,
                    // invalidate all copies
                    for(CodeVariable key : new HashSet<CodeVariable>(copied.keySet()))
                        copied.put(key, null);
                     */
                    // update the jump min index
                    if(op instanceof CodeOpJump) {
                        CodeOpJump jump = (CodeOpJump)op;
                        if(jumpMinIndex > labelIndex.get(jump.gotoLabelRef))
                            jumpMinIndex = labelIndex.get(jump.gotoLabelRef);
                    } else if(op instanceof CodeOpBranch) {
                        CodeOpBranch branch = (CodeOpBranch)op;
                        if(branch.labelRefFalse != null &&
                                jumpMinIndex > labelIndex.get(branch.labelRefFalse))
                            jumpMinIndex = labelIndex.get(branch.labelRefFalse);
                        if(branch.labelRefTrue != null &&
                                jumpMinIndex > labelIndex.get(branch.labelRefTrue))
                            jumpMinIndex = labelIndex.get(branch.labelRefTrue);
                    }
                }
                for(CodeVariable used : source) {
                    /*
                    Collection<CodeFieldDereference> sList =
                            AbstractCodeDereference.extractSourcePartsInSource(used);
                    for(CodeFieldDereference s : sList) {
                        if(!s.variable.context) {
                            // not static, thus local
                            usages.add(used.object);
                            copied.put(used.object, null);
                        }
                    }
                     */
                    // find all locals, and use them to update
                    // <code>usages</code> and <code>copied<code>
                    //
                    // see docs on <code>usages</code> on details about
                    // <code>minIndex</code>
                    int minIndex = 0;
                    if(!code.isJumpTarget(currIndex)) {
                        SCAN:
                        for(int i = currIndex - 1; i >= 0; --i) {
                            AbstractCodeOp p = code.ops.get(i);
                            if(p.isLocalTarget(used)) {
                                minIndex = i;
                                break SCAN;
                            }
                            if(code.isJumpTarget(i)) {
                                minIndex = 0;
                                break SCAN;
                            }
                        }
                        Integer prevMinIndex = usages.get(used);
                        if(prevMinIndex != null)
                            minIndex = Math.min(minIndex, prevMinIndex);
                    }
                    usages.put(used, minIndex);
                    /*if(used.name.contains("replace"))
                        result = result;*/
                    // update the copied list
                    if(op instanceof CodeOpAssignment &&
                            // check if it is the variable whose contents
                            // is copied
                            ((CodeOpAssignment)op).rvalue != null &&
                            new CodeFieldDereference(used).equals(
                                ((CodeOpAssignment)op).rvalue.value)) {
                        AbstractCodeDereference assignedTo = result;
                        boolean usedPreviously = copied.containsKey(
                                used);
                        if(!usedPreviously) {
                            copied.put(used, assignedTo);
                            copiedMaxIndex.put(used, currIndex);
                        } else {
                            AbstractCodeDereference usedPreviouslyBy = copied.get(used);
                            if(assignedTo != usedPreviouslyBy)
                                copied.put(used, null);
                            // no need to update max index, as the operations
                            // are scanned backwards
                        }
                    } else
                        copied.put(used, null);
                }
                // purge outdated usages
                for(CodeVariable u : new HashSet<>(usages.keySet())) {
                    int i = usages.get(u);
                    if(i == currIndex)
                        usages.remove(u);
                }
                --currIndex;
                prevOp = op;
            }
            // delete assignments and casts that were optimized out
            if(optimizeLabels(code))
                stable = false;
            if(!stable)
                totalStable = false;
        } while(!stable);
        return !totalStable;
    }
    /**
     * A simplified version of <code>optimizeAssignments()</code>,
     * works only in trivial cases but is much faster. This method
     * is specifically tailored to be used after loop unrolling followed by few
     * optimizations, as in the method
     * <code>AbstractTADDGenerator.generateTADD()</code>.<br>
     * 
     * Traces a code after an assignment to a local, provided that the
     * following operations are doing nothing or are forward goto'es.
     * // and also
     * // are never targets of jumps.
     * If another assignment of the same
     * variable is found, then the former is removed.<br>
     * 
     * Code indices must be valid. Invalidates possible label indices.
     * 
     * @param code code to optimise
     * @return if the code has been modified
     */
    public static boolean optimizeAssignmentsCheap(Code code) {
        boolean stable = true;
        int index = 0;
        for(AbstractCodeOp op : code.ops) {
            if(op instanceof CodeOpAssignment) {
                CodeOpAssignment a = (CodeOpAssignment) op;
                if(!a.isOpaque() && a.getResult() instanceof CodeFieldDereference &&
                        ((CodeFieldDereference)a.getResult()).isLocal()) {
                    CodeVariable lvalue = ((CodeFieldDereference)a.getResult()).
                            variable;
//                    Set<Integer> followedJumps = new HashSet<>();
                    FOLLOW:
                    for(int i = index + 1; i < code.ops.size(); ++i) {
                        AbstractCodeOp f = code.ops.get(i);
//                        if(!followedJumps.containsAll(code.getJumpSources(i)))
//                            // some source has not been followed
//                            break FOLLOW;
                        if(f instanceof CodeOpJump) {
                            CodeOpJump j = (CodeOpJump)f;
                            if(j.gotoLabelRef.codeIndex <= i)
                                break FOLLOW;
                            else {
//                                followedJumps.add(i);
                                // will be incremented by the loop's step statement
                                i = j.gotoLabelRef.codeIndex - 1;
                            }
                        } else if(doesNothing(code.ops, f))
                            ;
                        else if(f instanceof CodeOpAssignment &&
                                f.getResult() instanceof CodeFieldDereference &&
                                ((CodeFieldDereference)f.getResult()).isLocal()) {
                            CodeVariable lvalue2 = ((CodeFieldDereference)f.
                                    getResult()).variable;
                            if(lvalue == lvalue2) {
                                // the assignment does nothing
                                a.rvalue = null;
                                stable = false;
                            }
                            break FOLLOW;
                        } else
                            break FOLLOW;
                    }
                }
            }
            ++index;
        }
        if(!stable) {
            optimizeLabels(code);
            return true;
        } else
            return false;
    }
    /**
     * Traces a chain of jumps, finding the destination.
     * 
     * @param labeled                   map of labeled operators
     * @param label                     label to a possible
     *                                  chain of jumps
     * @param visited                   set of already visited labels,
     *                                  to detect loops, empty for the
     *                                  top call
     * @return                          destination label, if no
     *                                  chain of jump was found, the
     *                                  label passed as the argument
     *                                  is returned
     */
    public static CodeLabel findDestination(Map<CodeLabel, AbstractCodeOp> labeled,
            CodeLabel label, Set<AbstractCodeOp> visited) {
        AbstractCodeOp op = labeled.get(label);
        visited.add(op);
        if(op instanceof CodeOpJump) {
            label = new CodeLabel(((CodeOpJump)op).gotoLabelRef);
            if(visited.contains(labeled.get(label)))
                // a loop
                return label;
            else
                return findDestination(labeled, label, visited);
        } else
            return label;
    }
    /**
     * Optimizes jump chains. This is a part of the BASIC optimizations.
     *
     * Invalidates possible label indices.
     *
     * @param code                      code to optimize
     * @return                          if anything has changed
     */
    public static boolean optimizeJumpChains(Code code) {
        boolean stable = true;
        Map<CodeLabel, AbstractCodeOp> labeled = new HashMap<>();
        for(AbstractCodeOp op : code.ops)
            if(op.label != null)
                labeled.put(op.label, op);
        for(AbstractCodeOp op : code.ops)
            for(CodeLabel l : op.getLabelRefs()) {
                CodeLabel destinationLabel = findDestination(labeled, l,
                        new HashSet<>());
                if(!l.equals(destinationLabel)) {
                    l.set(destinationLabel);
                    stable = false;
                }
            }
        // delete jumps that were optimized out
        if(optimizeLabels(code))
            stable = false;
        return !stable;
    }
    /**
     * Deletes unused local variables that are not arguments.
     *
     * This is a part of the BASIC optimizations.
     * 
     * @param method                    method to optimize
     * @return                          if any variable has been removed
     */
    public static boolean optimizeUnusedLocals(CodeMethod method) {
        boolean stable = true;
        Set<CodeVariable> used = new HashSet<>();
        for(AbstractCodeOp op : method.code.ops) {
            Collection<CodeVariable> source =
                    op.getLocalSources();
            Collection<CodeVariable> target =
                    op.getLocalTargets();
            Collection<CodeVariable> referencesUsed = source;
            referencesUsed.addAll(target);
            for(CodeVariable v : referencesUsed)
                used.add(v);
        }
        for(CodeVariable v : new LinkedList<CodeVariable>(
                method.variables.values()))
            if(!used.contains(v)) {
                String name = v.name;
                if(method.argNames.contains(name))
                    continue;
                method.variables.remove(name);
                stable = false;
            }
        return !stable;
    }
    /**
     * Optimizes given code, using the BASIC optimization.<br>
     *
     * Label indices do not need to be valid. If
     * <code>setLabelIndices</code> is false, then this method
     * invalidates possible label indices.
     *
     * @param method                    method to optimize
     * @param setLabelIndices           if to set the label indices
     *                                  after the optimizations
     * @param tuning                    options for the optimizer,
     *                                  only <code>preserveRangedLocals</code>
     *                                  is used by this method
     * @return                          if the code has been modified
     */
    public static boolean optimizeBasic(CodeMethod method,
            boolean setLabelIndices, Tuning tuning) {
        boolean stable = true;
        Code code = method.code;
        if(code != null) {
//if(code.ops.size() > 4)
//    code = code;
//if(method.signature.name.contains("run/") &&
//        method.owner.name.contains("Ternary"))
//    method = method;
            if(optimizeStaticBranches(code))
                stable = false;
            if(optimizeNullBranches(code))
                stable = false;
            if(optimizeLabels(code))
                stable = false;
            if(!tuning.preserveRangedLocals &&
                    removeRedundantRanges(code))
                stable = false;
            if(!tuning.preserveRangedLocals &&
                    optimizeAssignments(code))
                stable = false;
            if(optimizeJumpChains(code)) // CodeLabel.clearIndices_(method.code)
                stable = false;
            if(optimizeUnusedLocals(method))
                stable = false;
            if(setLabelIndices)
                Optimizer.setLabelIndices(method);
        }
        return !stable;
    }
    /**
     * Removes dead code and reduces the number of internal
     * locals.<br>
     *
     * Label indices must be valid, and are also valid after
     * this method returns.<br>
     *
     * Note: uses <code>MergeVariables.reduceLocals</code> which
     * has special rules about evaluation of primitive ranges, see
     * the method for details.
     *
     * @param method                    method to optimize
     *                                  after the optimizations
     * @param useProposals              passed to
     *                                  <code>MergeVariables.reduceLocals()</code>,
     *                                  see that method for details
     * @param notArrayReferences        passed to
     *                                  <code>MergeVariables.reduceLocals()</code>,
     *                                  see that method for details
     * @param notBranchSources          passed to
     *                                  <code>MergeVariables.reduceLocals()</code>,
     *                                  see that method for details
     * @param possibility               a pre--computed possibility or null, if this method
     *                                  should compute it itself; can be incomplete
     *                                  as defined in <code>TraceValues</code>; if true is returned, then
     *                                  this possibility is no more current
     * <i>should be true only for certain compiler tests</i>
     * @return                          if anything has been optimized
     */
    public static boolean optimizeDeadAndLocals(CodeMethod method,
            boolean useProposals, boolean notArrayReferences,
            boolean notBranchSources, CodeValuePossibility possibility) {
        boolean stable = true;
        if(method.code != null) {
            boolean[] changed = new boolean[1];
            TraceValues.trimDeadCode(method, true, changed, possibility);
            if(changed[0]) {
                Optimizer.setLabelIndices(method);
                possibility = null;
                stable = false;
            }
            if(MergeVariables.reduceLocals(method, Double.NaN, true,
                    useProposals, notArrayReferences, notBranchSources,
                    true, possibility))
                stable = false;
        }
        return !stable;
    }
    /**
     * Performs basic optimizations. Instantiates Optimizer
     * object with <code>Optimizer.Optimization.BASIC</code>
     * and then runs method its method optimize(CodeCompilation).
     *
     * @param compilation               compilation with the
     *                                  code to optimize
     * @param tuning                    options for the optimizer
     */
    public static void optimizeBasic(CodeCompilation compilation,
            Tuning tuning) {
        List<Optimizer.Optimization> optimizations =
                new LinkedList<>();
        optimizations.add(Optimizer.Optimization.BASIC);
        Optimizer optimizer = new Optimizer(optimizations);
        optimizer.optimize(compilation, tuning);
    }
    /*
     * Performs optimizations from the class <code>TraceValues</code>
     * by instatiating the Optimizer
     * object with <code>Optimizer.Optimization.TRACED</code>
     * and then runnning method its method optimize(CodeCompilation).
     *
     * @param compilation               compilation with the
     *                                  code to optimize
     */
    /*
    private void optimizeTraced(CodeCompilation compilation) {
        List<Optimizer.Optimization> optimizations = new LinkedList
                <Optimizer.Optimization>();
        optimizations.add(Optimizer.Optimization.TRACED);
        Optimizer optimizer = new Optimizer(optimizations);
        optimizer.optimize(compilation);
    }
     */
    /**
     * Removes ranges from arithmetic locals, which as targets are solely
     * results of assignments, and these ranges have constant limits,
     * and also the rvalue is a fitting constant.
     * 
     * @param code code to browse
     * @return if some reduntant range has been removed
     */
    public static boolean removeRedundantRanges(Code code) {
        boolean stable = true;
        Set<CodeVariable> fittingAssignmentTarget = new HashSet<>();
        Set<CodeVariable> sideTarget = new HashSet<>();
        for(AbstractCodeOp op : code.ops) {
            if(op instanceof CodeOpAssignment) {
                CodeOpAssignment a = (CodeOpAssignment)op;
                CodeVariable result = a.getResult().getTypeSourceVariable();
                if(result.owner instanceof CodeMethod &&
                        result.type.numericPrecision() != 0) {
                    // an arithmetic local
                    boolean fitting = false;
                    CodeVariablePrimitiveRange range = result.primitiveRange;
                    if(range != null && range.isEvaluatedAsConstant() &&
                            a.rvalue.range == null &&
                            a.rvalue.value instanceof CodeConstant) {
                        CodeConstant constant = (CodeConstant)a.rvalue.value;
                        CodeConstant min = (CodeConstant)range.min;
                        CodeConstant max = (CodeConstant)range.max;
                        if(constant.value.type.isAnnotation()) {
                            Type.TypeRange rRange = constant.value.getAnnotationValue().
                                    range;
                            if((min.value.arithmeticEqualTo(rRange.getMinLiteral()) ||
                                        min.value.arithmeticLessThan(rRange.getMinLiteral())) &&
                                    !max.value.arithmeticLessThan(rRange.getMaxLiteral())) {
                                fittingAssignmentTarget.add(result);
                                fitting = true;
                            }
                        } else {
                            if((min.value.arithmeticEqualTo(constant.value) ||
                                        min.value.arithmeticLessThan(constant.value)) &&
                                    !max.value.arithmeticLessThan(constant.value)) {
                                fittingAssignmentTarget.add(result);
                                fitting = true;
                            }
                        }
                    }
                    if(!fitting)
                        sideTarget.add(result);
                }
            } else {
                sideTarget.addAll(op.getLocalTargets());
            }
        }
        for(CodeVariable v : fittingAssignmentTarget)
            if(!sideTarget.contains(v))
                v.primitiveRange = null;
        return !stable;
    }
    void log(int indentLevel, String s) {
        if(verbose) {
            Scanner sc = CompilerUtils.newScanner(s).useDelimiter("\\n");
            while(sc.hasNext()) {
                System.out.print("O ");
                for(int i = 0; i < indentLevel; ++i)
                    System.out.print("  ");
                System.out.println(sc.next());
            }
        }
    }
}
