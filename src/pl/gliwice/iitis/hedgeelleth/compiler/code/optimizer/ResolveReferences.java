/*
 * ResolveReferences.java
 *
 * Created on Oct 19, 2016
 *
 * Copyright (c) 2016  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.code.optimizer;

import java.util.*;
import pl.gliwice.iitis.hedgeelleth.Hedgeelleth;

import pl.gliwice.iitis.hedgeelleth.compiler.code.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.AbstractCodeDereference;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.CodeFieldDereference;
import pl.gliwice.iitis.hedgeelleth.compiler.code.op.*;
import pl.gliwice.iitis.hedgeelleth.compiler.primitiverange.CodeVariablePrimitiveRange;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Context;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Literal;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Method;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.MethodSignature;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Type;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Variable;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.clazz.AbstractJavaClass;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.CompilerException;
import pl.gliwice.iitis.hedgeelleth.interpreter.AbstractInterpreter;
import pl.gliwice.iitis.hedgeelleth.interpreter.InterpretingContext;
import pl.gliwice.iitis.hedgeelleth.interpreter.RuntimeValue;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.RuntimeMethod;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.RuntimeObject;
import pl.gliwice.iitis.hedgeelleth.interpreter.exception.InterpreterException;

/**
 * Attempt to find allocations which can bew replaced with a refreshing of
 * a constant pool of objects by re-running their constructors.
 * 
 * @author Artur Rataj
 */
public class ResolveReferences {
    /**
     * Qualified name of the Random class, which is the only one allowed to be
     * have a constructor in a PTA.
     */
    public static final String RANDOM_CLASS_NAME = "java.util.Random";
    /**
     * Prefix of a local variable, which points to an in--pta allocated object. The rest
     * of the variable's name is the objects's serial number.
     */
    private static final String INPTA_OBJECT_PREFIX = "#alloc";

    /**
     * Runtime owner of the method to analyze.
     */
    RuntimeValue owner;
    /**
     * Runtime of the method to analyse.
     */
    RuntimeMethod rm;
    /**
     * Method to analyse.
     */
    CodeMethod cm;
    /**
     * Interpreter.
     */
    final AbstractInterpreter interpreter;
    /**
     * Possibility, with <code>allocationUnique</code> on.
     */
    CodeValuePossibility possibility;
    /**
     * Transport.
     */
    CodeVariableTransport transport;
    
    public ResolveReferences(RuntimeValue owner, RuntimeMethod rm, AbstractInterpreter interpreter) {
        this.owner = owner;
        this.rm = rm;
        cm = rm.method;
        this.interpreter = interpreter;
    }
    /**
     * If this allocation should be ignored, because it instantiates some special class.
     * 
     * @param a allocation
     * @return if to ignore
     */
    private boolean specialConstructor(CodeOpAllocation a) {
        String name = a.constructor.method.owner.name;
        return name.equals(RANDOM_CLASS_NAME) ||
                name.startsWith(Type.SYSTEM_PACKAGE);
    }
    /**
     * Inserts Nops after each allocation, so that <code>CodeValuePossibility</code> does
     * not merge values after an allocation yet begore a potential jump target. The insertion
     * has also another purpose: a pair (allocation, nop) can be replaced in <code>replace()</code>
     * by equivalent (call, assignment) so that indices do not change.
     * 
     * @return if any allocations have been found, i.e. if nops were inserted
     */
    private boolean insertsNops() {
        boolean found = false;
        int num = cm.code.ops.size();
        for(int i = 0; i < num; ++i) {
            AbstractCodeOp op = cm.code.ops.get(i);
            if(op instanceof CodeOpAllocation &&
                    !specialConstructor((CodeOpAllocation)op)) {
                cm.code.ops.add(i + 1,
                        new CodeOpNone(op.getStreamPos()));
                ++i;
                ++num;
                found = true;
            }
        }
        if(found)
            CodeLabel.setIndices(cm.code);
        return found;
    }
    /**
     * If an object allocated at a given index ever leaks outside this method's
     * locals.
     * 
     * @return if there is a leakage
     */
    private boolean externalStorage(int index) {
        if(!cm.code.ops.get(index).getResult().extractPlainField().isEmpty())
            // spill
            return true;
        else {
            int i = 0;
            for(AbstractCodeOp op : cm.code.ops) {
                if(op instanceof CodeOpAssignment) {
                    CodeOpAssignment a = (CodeOpAssignment)op;
                    if(a.getResult().getResultType().isJava() && !a.getResult().extractPlainField().isEmpty()) {
                        if(a.rvalue.value instanceof CodeFieldDereference) {
                            CodeFieldDereference f = (CodeFieldDereference)a.rvalue.value;
                            if(f.isLocal()) {
                                CodeVariable v = f.variable;
                                for(AbstractCodeValue w : possibility.getValuesM1(i - 1, f)) {
                                    if(w instanceof CodeConstant) {
                                        CodeConstant c = (CodeConstant)w;
                                        if(c.getResultType().isOfIntegerTypes() &&
                                                c.value.getInteger() == index) {
                                            // spill
                                            return true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else if(op instanceof CodeOpAllocation) {
                    // ignore
                } else
                    for(CodeFieldDereference f : op.getFieldTargets())
                        if(f.getResultType().isJava())
                            throw new RuntimeException("non--assignment and non--allocation " +
                                    "writes to a field which has a Java return type");
                ++i;
            }
        }
        return false;
    }
    /**
     * Check each allocation, if it can be replaced by re-calling a constructor on a
     * preallocated object.
     * 
     * @param error any errors are put into this exception
     * @return indices of allocations, which can be replaced.
     */
    private List<Integer> analyzeFlow(CompilerException error) {
        List<Integer> replaceableAllocations = new LinkedList<>();
        int num = cm.code.ops.size();
        for(int index = 0; index < num; ++index) {
            AbstractCodeOp op = cm.code.ops.get(index);
            if(op instanceof CodeOpAllocation && !op.getLocalTargets().isEmpty() &&
                    !specialConstructor((CodeOpAllocation)op)) {
                if(op.getLocalTargets().size() != 1)
                    throw new RuntimeException("unexpected");
                boolean prevAllocationPossiblyLeft = false;
                CodeVariable target = op.getLocalTargets().iterator().next();
                CHECK:
                for(CodeVariable v : cm.variables.values())
                    if(v.getResultType().isJava() && v != target) {
                        Set<AbstractCodeValue> set = possibility.getValuesM1(
                                index, new CodeFieldDereference(v));
                        for(AbstractCodeValue w : set)
                            if(w instanceof CodeConstant) {
                                CodeConstant c = (CodeConstant)w;
                                if(c == CodeValuePossibility.UNKNOWN_VALUE)
                                    // bah!
                                    prevAllocationPossiblyLeft = true;
                                else if(c.getResultType().isOfIntegerTypes() &&
                                        c.value.getInteger() == index) {
                                    if(transport.isTransported(index*2, new CodeFieldDereference(v)))
                                        // bah!
                                        prevAllocationPossiblyLeft = true;
                                }
                                if(prevAllocationPossiblyLeft)
                                    break CHECK;
                            }
                    }
                if(!prevAllocationPossiblyLeft) {
                    if(!externalStorage(index)) {
                        replaceableAllocations.add(index);
                    } else
                        error.addReport(new CompilerException.Report(op.getStreamPos(),
                            CompilerException.Code.ILLEGAL,
                            "could not inline allocation: external leak"));
                } else
                    error.addReport(new CompilerException.Report(op.getStreamPos(),
                        CompilerException.Code.ILLEGAL,
                        "could not inline allocation: could not exclude simultaneous storage " +
                                "of previous allocation"));
            }
        }
        return replaceableAllocations;
    }
    /**
     * Replaces an allocation. Does not change indices, see <code>insertNops()</code>
     * for details.
     * 
     * @param index its index
     */
    private void replace(int index) {
        CodeOpAllocation a = (CodeOpAllocation)cm.code.ops.get(index);
        RuntimeObject runtime = (RuntimeObject)owner.value;
        CodeClass cc = runtime.codeClass;
        Type type = a.getInstantiatedType();
        CodeVariable field = new CodeVariable(cc.getStreamPos(),
            cc, INPTA_OBJECT_PREFIX + index, type, Context.NON_STATIC, true, false,
            false, Variable.Category.INTERNAL_NOT_RANGE);
        cc.fields.put(field.name, field);
        cc.listOfFields.add(field);
        CodeVariable localThis = cm.variables.get(Method.LOCAL_THIS);
        CodeFieldDereference d = new CodeFieldDereference(localThis, field);
        CodeOpCall call = new CodeOpCall(a.getStreamPos());
        call.label = a.label;
        call.methodReference = a.constructor;
        call.args = new LinkedList<>(a.args);
        call.args.add(0, new RangedCodeValue(d));
        call.result = null;
        call.textRange = a.textRange;
        call.directNonStatic = true;
        cm.code.ops.remove(index);
        cm.code.ops.add(index, call);
        try {
            CodeClass newCc = interpreter.getCodeClass(type);
            RuntimeValue that = interpreter.newRuntimeObject(
                    newCc, type);
            interpreter.setValue(d, that, rm);
            InterpretingContext ic = new InterpretingContext(interpreter,
                    null, false, rm, InterpretingContext.UseVariables.NO);
            List<RuntimeValue> objectArgs = new LinkedList<>();
            objectArgs.add(that);
            interpreter.interpret(ic, newCc.lookupMethod(new MethodSignature(null,
                    AbstractJavaClass.INIT_OBJECT, new LinkedList<>())), objectArgs);
            for(CodeVariable cv : newCc.listOfFields) {
                // the non--final variables are initialised in *object
                if(cv.finaL)
                    that.getReferencedObject().setValue(interpreter, rm, cv, new RuntimeValue(
                        Hedgeelleth.newDefaultInitializerLiteral(cv.getResultType())));
            }
            CodeOpAssignment r = new CodeOpAssignment(a.getStreamPos());
            r.result = a.result;
            r.rvalue = new RangedCodeValue(d);
            cm.code.ops.remove(index + 1);
            cm.code.ops.add(index + 1, r);
        } catch(InterpreterException e) {
            throw new RuntimeException("unexpected");
        }
    }
    /**
     * Resolves the references.
     * 
     * @param error any errors are put into this exception
     * @return if anything changed
     */
    public boolean resolve(CompilerException error) {
        boolean changed = false;
        if(insertsNops()) {
            changed = true;
            TraceValues.Options options;
            possibility = TraceValues.getUniqueAllocationTraceRecursive(cm,
                    interpreter);
            transport = TraceValues.traceTransport(cm, true);
            List<Integer> replaceable = analyzeFlow(error);
            for(int index : replaceable)
                replace(index);
            // CodeLabel.setIndices(cm.code);
        }
        return changed;
    }
    /**
     * Attempt to recursively find the serial number of an in--pta object having a given
     * local variable.
     * 
     * @param d dereference, whose value is to be determined
     * @param index index, at which to determine the name
     * @return serial number of an in--pta object or -1 if not a searched type or variable or
     * not succeded at finding its serial number; the serial number is equal to the index of an
     * allocation which allocated the object
     */
    private int findSerial(CodeFieldDereference d, int index) {
        Variable value = null;
        while(!d.variable.name.startsWith(INPTA_OBJECT_PREFIX)) {
//if(d.variable.name.indexOf("EPSILON") != -1)
//    d = d;
            if(!d.isLocal() && d.object == null)
                // a static, i.e. neither a traceable local, nor the non--static field which could be the
                // searched target -- discard
                return -1;
            AbstractCodeValue v = possibility.getSingleM1(index - 1,
                    d.isLocal() ?
                            new CodeFieldDereference(d) : new CodeFieldDereference(d.object));
            if(v instanceof CodeFieldDereference) {
                d = (CodeFieldDereference)v;
            } else
                return -1;
        }
        return Integer.parseInt(d.variable.name.substring(
                INPTA_OBJECT_PREFIX.length()));
    }
    /**
     * If a serial number of an in--pta object having a given local variable is found,
     * then the local replacing variable is created, if not yet exists, and returned. 
     *
     * @param d dereference, whose value is to be determined
     * @param index index, at which to determine the name
     * @param newLocals ma if new locals created so far; modifed by this method if a new
     * local is created
     * @return a replacing local variable or null for none
     */
    private CodeVariable findLocal(CodeFieldDereference s, int index,
            SortedMap<String, CodeVariable> newLocals) {
        int serial = findSerial(s, index);
        if(serial != -1) {
            String newLocalName = INPTA_OBJECT_PREFIX + serial + "#" + s.variable.name;
            CodeVariable local;
            if(!newLocals.containsKey(newLocalName)) {
                // create a new local
                CodeVariable newLocal = new CodeVariable(cm.code.ops.get(index).getStreamPos(),
                        cm, newLocalName, s.getResultType(), Context.NON_STATIC, false,
                        false, false, s.variable.category);
                newLocals.put(newLocalName, newLocal);
                local = newLocal;
            } else
                local = newLocals.get(newLocalName);
            if(s.variable.primitiveRange != null) {
                CodeVariablePrimitiveRange r = s.variable.primitiveRange;
                CodeVariable min = findLocal(new CodeFieldDereference(s.object,
                        ((CodeFieldDereference)r.min).variable), index, newLocals);
                CodeVariable max = findLocal(new CodeFieldDereference(s.object,
                        ((CodeFieldDereference)r.max).variable), index, newLocals);
                local.primitiveRange = new CodeVariablePrimitiveRange(
                        s.variable.primitiveRange.getStreamPos(),
                        min, max, s.variable.primitiveRange.isSourceLevel());
                        
            }
            return local;
        }
        return null;
    }
    /**
     * After <code>resolve()</code> the method can be externally flattened.
     * Then, using this method, the fields of the in--pta allocated objects can be
     * transformed into locals in to order to faciliate certain further optimisations.
     * 
     * @param method runtime of the method, where to localise field of objects created
     * within <p>resolve()</p>; its owner should not change, when compared to one
     * from <code>resolve()</code>
     */
    public void localize(RuntimeMethod rm) {
        this.rm = rm;
        cm = rm.method;
        possibility = TraceValues.traceValuesWithCasts(
                cm, new HashMap<>(), new TraceValues.Options(interpreter, false,
                        false, null));
        transport =null;
        SortedMap<String, CodeVariable> newLocals = new TreeMap<>();
        int num = cm.code.ops.size();
        for(int index = 0; index < num; ++index) {
            AbstractCodeOp op = cm.code.ops.get(index);
            Set<CodeFieldDereference> sources = op.getFieldSources();
            for(CodeFieldDereference s : sources) {
                if(!s.getResultType().isJava()) {
                    CodeVariable local = findLocal(s, index, newLocals);
                    if(local != null) {
//if(index == 64)
//    index = index;
                        if(!op.replaceFieldSourceVariable(s, new CodeFieldDereference(local)) &&
                                // variable primary range variables can disappear earlier along with
                                // their ranged host
                                local.category != Variable.Category.INTERNAL_VARIABLE_PR)
                            throw new RuntimeException("could not replace");
                    }
                }
            }
            Set<CodeFieldDereference> targets = op.getFieldTargets();
            for(CodeFieldDereference t : targets) {
                if(!t.getResultType().isJava()) {
                    CodeVariable local = findLocal(t, index, newLocals);
                    if(local != null) {
                        boolean changed = false;
                        if(op instanceof AbstractResultCodeOp) {
                            AbstractResultCodeOp r = (AbstractResultCodeOp)op;
                            if(r.getResult() != null && r.getResult().equals(t)) {
                                r.setResult(new CodeFieldDereference(local));
                                changed = true;
                            }
                        }
                        if(!changed &&
                                // variable primary range variables can disappear earlier along with
                                // their ranged host
                                local.category != Variable.Category.INTERNAL_VARIABLE_PR)
                            throw new RuntimeException("could not replace");
                    }
                }
            }
        }
        /*int count = 0;*/
        for(CodeVariable v : newLocals.values()) {
            // the initialisations are provided by the integrated code
            /*String name = v.name.substring(INPTA_OBJECT_PREFIX.length());
            int serial = Integer.parseInt(name.substring(0, name.indexOf('#')));
            RuntimeObject runtime = (RuntimeObject)owner.value;
            CodeVariable field = runtime.codeClass.fields.get(INPTA_OBJECT_PREFIX + serial);
            RuntimeObject inPtaObject = runtime.getValue(interpreter, rm, field).getReferencedObject();
            CodeClass cc = interpreter.getCodeClass(inPtaObject.type);
            String origName = name.substring(name.indexOf('#') + 1);
            RuntimeValue initialValue = inPtaObject.getValue(interpreter, rm, cc.fields.get(origName));
            CodeOpAssignment init = new CodeOpAssignment(cm.getStreamPos());
            init.rvalue = new RangedCodeValue(new CodeConstant(cm.getStreamPos(), initialValue));
            init.setResult(new CodeFieldDereference(v));*/
            cm.addLocal(v, false);
            /*cm.code.ops.add(count, init);
            ++count;*/
        }
    }
}
