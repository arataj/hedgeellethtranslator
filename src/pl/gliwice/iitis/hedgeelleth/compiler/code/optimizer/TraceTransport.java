/*
 * TraceTransport.java
 *
 * Created on Mar 25, 2009, 5:09:26 PM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.code.optimizer;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.code.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.op.AbstractCodeOp;

/**
 * Transport information for <code>TraceValues.traceValues</code>.<br>
 *
 * It uses double indices at certain places, as described in
 * <code>CodeVariableTransport</code>. No double indices are
 * explicitly commented as single indices.
 *
 * @author Artur Rataj
 */
public class TraceTransport {
    /**
     * A region where variable transport exists.
     */
    public static class Region {
        int first;
        int last;

        public Region(int first, int last) {
            this.first = first;
            this.last = last;
        }
        /**
         * If the index falls into this region.
         * 
         * @param index index to test
         * @return if in this region, boundaries included
         */
        public boolean within(int index) {
            return index >= first && index <= last;
        }
        public String toString() {
            return "<" + first + ", " + last + ">";
        }
    }
    /**
     * Transport is known to be limited into this region. Specified
     * at construction time.
     */
    public Region region;
    /**
     * Current trace of some transported value.<br>
     *
     * Contains a list of <i>double indices</i> of traced operations,
     * since the last assignment of the traced variable,
     * if no read of the variable occured after the last
     * assignment. If the read occured, the list is
     * registered using
     * <code>CodeValuePossibility.setTransported</code>
     * and then cleared.
     */
    public Set<Integer> transportTrace;
    /**
     * The first key is the <i>single index</i> of the current operation
     * in <code>traceBranch</code>. The set in turn either (1)
     * contains double indices of all operations within
     * <code>transportTrace<code>for all instances so far
     * of <code>traceBranch</code> and the given
     * index, or (2) is null what means that the indices
     * in the transport just before the operation at the index
     * will be never registered anyway, because it is already known that
     * there is further no read of the current value in the current branch.<br>
     * 
     * The null is set by the method <code>setStop</code>.
     */
    protected Map<Integer, Set<Integer>> visited;

    /**
     * Creates a new instance of <code>TraceTransport</code>,
     * with empty list of indices.
     * 
     * @param region transport is known to be limited into this region
     */
    public TraceTransport(Region region) {
        this.region = region;
        transportTrace = new HashSet<>();
        visited  = new HashMap<>();
    }
    /**
     * Creates a new instance of <code>TraceTransport</code>,
     * with the list of indices in the transport trace being
     * a copy of the list of indices from another object.<br>
     *
     * The visit information is shared with that
     * of the another object, as opposed to copied.
     */
    public TraceTransport(TraceTransport source) {
        region = source.region;
        transportTrace = new HashSet<>(source.transportTrace);
        visited = source.visited;
    }
    /**
     * Computes a region or its bound, where tracing a variable is important.
     * 
     * @param method method to analyse
     * @param local a local
     * @return a region or its upper bound, null if the variable is not used
     */
    public static Region getRegion(CodeMethod method, CodeVariable local) {
        int num = method.code.ops.size();
        int min = num + 1;
        int max = -1;
        // usage region
        for(int index = 0; index < num; ++index) {
            AbstractCodeOp op = method.code.ops.get(index);
            Set<CodeVariable> source = op.getLocalSources();
            Set<CodeVariable> target = op.getLocalTargets();
            if(source.contains(local) || target.contains(local)) {
                if(min > index)
                    min = index;
                if(max < index)
                    max = index;
            }
        }
        // add jump--into region
        for(int index = min; index <= max; ++index)
            for(int intoIndex : method.code.getJumpSources(index)) {
                if(min > intoIndex)
                    min = intoIndex;
                if(max < intoIndex)
                    max = intoIndex;
            }
        if(min > max)
            return null;
        else {
            // see if the region starts with an assignment, so that any value
            // of the local before is unimportant
            boolean start = false;
            AbstractCodeDereference d = method.code.ops.get(min).getResult();
            if(d instanceof CodeFieldDereference) {
                CodeFieldDereference f = (CodeFieldDereference)d;
                if(f.isLocal() && f.variable == local)
                    start = true;
            }
            if(!start) {
                min = 0;
                max = num - 1;
            }
            return new Region(min, max);
        }
    }
    /**
     * Adds a double index from the trace to <code>visited</code>.
     *
     * @param currentIndex              single index of the current operation
     * @param transportIndex            double index from the trace
     */
    public void addVisited(int currentIndex, int transportIndex) {
        if(!visited.containsKey(currentIndex)) {
            Set s = new HashSet<>();
            visited.put(currentIndex, s);
        }
        Set s = visited.get(currentIndex);
        s.add(transportIndex);
    }
    /**
     * Puts <code>null</code> as a set for a given index in <code>visited</code>.
     * See docs of <code>visited</code> for details.
     *
     * @param currentIndex              single index of the current operation
     */
    public void setStop(int currentIndex) {
        visited.put(currentIndex, null);
    }
    /**
     * Returns if there is <code>null</code> as a set for a given index
     * in <code>visited</code>. See docs of <code>visited</code> for details.
     *
     * @param currentIndex              single index of the current operation
     */
    public boolean isStop(int currentIndex) {
        return visited.containsKey(currentIndex) &&
                visited.get(currentIndex) == null;
    }
    /**
     * Checks for presence of an index from the transport trace in
     * <code>visited</code>.
     *
     * @param currentIndex              single index of the current operation
     * @param transportIndex            double index from the trace
     */
    public boolean isVisited(int currentIndex, int transportIndex) {
        Set s = visited.get(currentIndex);
        if(s == null)
            return false;
        else
            return s.contains(transportIndex);
    }
    /**
     * Checks for presence of at least a single occurence of an index
     * in <code>visited</code>, or, in the other words, if the operation at
     * <code>currentIndex</code> has already been visited at least once.
     *
     * @param currentIndex              index of the current operation
     */
    public boolean isVisitedAny(int currentIndex) {
        return visited.containsKey(currentIndex);
    }
}
