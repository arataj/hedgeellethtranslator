/*
 * TraceValues.java
 *
 * Created on Mar 25, 2009, 11:53:07 AM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.code.optimizer;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.code.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.op.*;
import pl.gliwice.iitis.hedgeelleth.compiler.primitiverange.CodeVariablePrimitiveRange;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Context;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Literal;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Method;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;
import pl.gliwice.iitis.hedgeelleth.interpreter.AbstractInterpreter;
import pl.gliwice.iitis.hedgeelleth.interpreter.RuntimeValue;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.RuntimeMethod;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.RuntimeObject;
import pl.gliwice.iitis.hedgeelleth.interpreter.exception.InterpreterException;

/**
 * Methods for tracing values of variables within a method,
 * and tools that use the tracing: value propagation, finding
 * dead code.<br>
 *
 * These method should never be called on a code, where the
 * jump target no--ops are optimised out, see
 * <code>CodeValuePossibility</code> for default.
 * Also, optimizing code may lead to situations legal for the backend,
 * but illegal for the frontent, what might lead to spurious compile
 * errors.<br>
 * 
 * Note: for optimization efficiency, range checking of internal
 * variables is redundant by methods in this class, that is,
 * not required on the level of the source code.
 * Please adhere to the optimization limitation rules given
 * in <code>CodeVariable</code> so that this approach won't cause
 * errors.
 *
 * Label indices must be valid for all methods in this class, and are also
 * valid after the methods exit.<br>
 *
 * An <i>incomplete</i> possibility is here defined as one run with
 * <code>onlyGetSingle</code> being true in the methods
 * <code>traceBranchValue()</code> or <code>traceValues()</code>.
 * 
 * @author Artur Rataj
 */
public class TraceValues {
    /**
     * Options.
     */
    public static class Options {
        /** If true, a cast of an objects is treated as a plain assignment; enabling it may
         *  cause the optimizer to optimize out the casts and use improper variable types;
         *  thus, such propagation is usually used only for tracing runtime values.
         */
        AbstractInterpreter propagateJavaCasts;
        /** If to perform incomplete tracing -- if a variable has already multiple values 
         * or an unknown value, then do not add any new ones except for possible "uninitialized",
         *  as only <code>CodeValuePossibility.getSingle()</code>,
         * <code>CodeValuePossibility.getNumValues() == -1</code>,
         * <code>CodeValuePossibility.getNumValues() > 1</code>,
         * or other cases where a complete trace is not needed will be used on the trace by the
         * outside code; typically true only to make the trace faster.
         */
        boolean onlyGetSingle;
        /**
         * False to treat allocations as assignments of unknown values, true if to treat them
         * as assignments of uniques value. Attemps to substitute variables with constants and
         * ignores non--Java variables. Used only to trace Java types.
         */
        boolean allocationUnique;
        /**
         * Not null only if <code>allocationUnique</code>
         * is true, contains then the previous trace; allows for iterative substitutions
         * of variables with constants.
         */
        CodeValuePossibility previousRun;
//        /**
//         * Not an option, but modified within tracing of allocations to true
//         * if anything changed in the trace.
//         */
//        boolean allocationTraceChanged;
        
        /**
         * Creates new set of options. //Sets <code>allocationTraceChanged</code> to
         * false.
         * 
         * @param propagateJavaCasts copied to a respective field
         * @param onlyGetSingle copied to a respective field
         * @param allocationUnique copied to a respective field
         * @param previousRun copied to a respective field
         */
        public Options(AbstractInterpreter propagateJavaCasts, boolean onlyGetSingle,
                boolean allocationUnique, CodeValuePossibility previousRun) {
            this.propagateJavaCasts = propagateJavaCasts;
            this.onlyGetSingle = onlyGetSingle;
            this.allocationUnique = allocationUnique;
            this.previousRun = previousRun;
//            this.allocationTraceChanged = false;
        }
        /**
         * <p>Creates new set of options, with no tracing of allocations.</p>
         * 
         * <p>This is a convenience constructor.</p>
         * 
         * @param propagateJavaCasts copied to a respective field
         * @param onlyGetSingle copied to a respective field
         */
        public Options(AbstractInterpreter propagateJavaCasts, boolean onlyGetSingle) {
            this(propagateJavaCasts, onlyGetSingle, false, null);
        }
    }
    /**
     * If a condition of a branch is a local, then returns that local.
     * Otherwise returns null.
     * 
     * @param branch branch
     * @return local or null
     */
    static CodeVariable conditionLocal(CodeOpBranch branch) {
        if(branch.condition.value instanceof CodeFieldDereference) {
            CodeFieldDereference d = (CodeFieldDereference)branch.condition.value;
            if(d.isLocal())
                return d.variable;
        }
        return null;
    }
    /**
     * Cancels conditionals, that originate from dead branches.
     * 
     * @param possibility trace
     * @param method traced method
     */
    private static void cancelConditionals(CodeValuePossibility possibility,
            CodeMethod method, CodeFieldDereference variable) {
        int maxIndex = method.code.ops.size() - 1;
        boolean stable;
        do {
            stable = true;
            for(int index = -1; index <= maxIndex; ++index) {
                Map<AbstractCodeDereference, Set<AbstractCodeValue>> set =
                        possibility.values.get(index);
                if(set != null) {
                    Set<AbstractCodeValue> removals = null;
                    Set<AbstractCodeValue> s = set.get(variable);
                    for(AbstractCodeValue v : s) {
                        if(v instanceof CodeConstant) {
                            Literal l = ((CodeConstant)v).value;
                            if(l instanceof ConditionalLiteral) {
                                ConditionalLiteral cl = (ConditionalLiteral)l;
                                Set<AbstractCodeValue> values = possibility.getValuesM1(cl.INDEX - 1, variable);
                                Set<AbstractCodeValue> valuesFiltered = new HashSet<>();
                                Literal lastConstant = null;
                                for(AbstractCodeValue atOrigin : values) {
                                    if(atOrigin instanceof CodeConstant) {
                                        Literal constantAtOrigin = ((CodeConstant)atOrigin).value;
                                        if(constantAtOrigin instanceof ConditionalLiteral) {
                                            ConditionalLiteral conditionalAtOrigin = (ConditionalLiteral)constantAtOrigin;
                                            if(conditionalAtOrigin.INDEX == cl.INDEX)
                                                // filter out from <code>valuesFiltered</code>
                                                continue;
                                        }
                                        lastConstant = constantAtOrigin;
                                    }
                                    valuesFiltered.add(atOrigin);
                                }
                                if(valuesFiltered.size() == 1 && lastConstant != null) {
    // if(method.getKey().contains("First"))
    //         method = method;
                                    if(lastConstant.getBoolean() != l.getBoolean()) {
                                        if(removals == null)
                                            removals = new HashSet<>();
                                        removals.add(v);
                                        stable = false;
                                    }
                                }
                            }
                        }
                    }
                    if(removals != null) {
                        for(AbstractCodeValue v : removals)
                            s.remove(v);
                    }
                }
            }
            // removed literals may make <code>valuesFiltered.size() == 1</code> at
            // additional places, and so repeat if unstable
        } while(!stable);
    }
    /**
     * Replaces conditional literals with unconditional ones of the same
     * value.
     * 
     * @param possibility trace
     * @param method traced method
     */
    private static void removeConditionality(CodeValuePossibility possibility,
            CodeMethod method, CodeFieldDereference variable) {
        int maxIndex = method.code.ops.size() - 1;
        for(int index = -1; index <= maxIndex; ++index) {
            Map<AbstractCodeDereference, Set<AbstractCodeValue>> set =
                    possibility.values.get(index);
            if(set != null) {
                Set<AbstractCodeValue> replacements = null;
                Set<AbstractCodeValue> s = set.get(variable);
                for(AbstractCodeValue v : s) {
                    if(v instanceof CodeConstant) {
                        Literal l = ((CodeConstant)v).value;
                        if(l instanceof ConditionalLiteral) {
                            if(replacements == null)
                                replacements = new HashSet<>();
                            replacements.add(v);
                        }
                    }
                }
                if(replacements != null) {
                    for(AbstractCodeValue v : replacements) {
                        ConditionalLiteral cl = (ConditionalLiteral)
                                ((CodeConstant)v).value;
                        CodeConstant replacement = new CodeConstant(
                                ((CodeConstant)v).getStreamPos(),
                                new Literal(cl.getBoolean()));
                        s.remove(v);
                        s.add(replacement);
                    }
                }
            }
        }
    }
    /**
     * A root call to <code>traceBranchValue</code> should go through
     * this wrapped, as it later cleans the trace from conditional literals
     * which cancel theirselves.
     * 
     * @param possibility possibility, stores values of method's locals
     * @param ops passed to <code>traceBranchValue</code>
     * @param variable passed to <code>traceBranchValue</code>
     * @param value passed to <code>traceBranchValue</code>
     * @param options options
     */
    private static void traceBranchValueRoot(CodeValuePossibility possibility,
            CodeMethod method, CodeVariable variable,
            AbstractCodeValue value, Options options) {
        if(options.allocationUnique) {
            List<AbstractCodeValue> valueList = new LinkedList<>();
            valueList.add(value);
            traceBranchValueAllocation(possibility, method, 0, variable, valueList,
                    options, 0);
        } else
            traceBranchValue(possibility, method, 0, variable, value,
                    options, 0);
        if(variable != null && !options.allocationUnique) {
// if(method.code.ops.size() > 10)
//     method = method;
            CodeFieldDereference dereference = new CodeFieldDereference(variable);
            cancelConditionals(possibility, method, dereference);
            removeConditionality(possibility, method, dereference);
        }
    }
    /**
     * Called by <code>traceValues()</code> to trace values of local variables
     * along a single branch. The tracing is terminated if the propagated value
     * is found to be already traced before at a given position in the code.<br>
     * 
     * There is a copy <code>traceBranchValueAllocation</code> of this mrthod, made for
     * performance reasons. Certain non--specific modifications should thus be made in both
     * methods at once.<br>
     * 
     * Also used to find the dead code at the same time.<br>
     *
     * Respects <code>CodeOpFinalDeclaration</code>.<br>
     *
     * @param possibility               possibility, stores values of method's
     *                                  locals
     * @param ops                       traced method
     * @param index                     index of the operation where
     *                                  to begin this branch
     * @param variable                  the traced local variable, or null
     *                                  if it is only a search for the dead code, or, but only
     *                                  if this method is a constructor or static initializer,
     *                                  a final field
     * @param value                     a currently traced value of the variable
     *                                  at the beginning of this branch,
     *                                  can be null if a variable is uninitialized
     *                                  or it is only a search for dead code
     * @param options options
     * @param depth                     recursion depth; for testing
     */
    private static void traceBranchValue(CodeValuePossibility possibility,
            CodeMethod method, int index, CodeVariable variable,
            AbstractCodeValue value, Options options, int depth) {
        if(method.code == null)
            // skip an abstract method, implemented with a marker
            return;
        ++depth;
        try {
            List<AbstractCodeOp> ops = method.code.ops;
            boolean field = variable != null ?
                    variable.owner instanceof CodeClass : false;
            AbstractCodeDereference dereference;
            if(variable != null) {
                dereference = new CodeFieldDereference(variable);
            } else
                dereference = null;
            for(; index < ops.size(); ++index) {
 if(ops.size() > 20 /*&& index == 190 && index > 60*/ && variable != null && variable.name.indexOf("c2#k") != -1)
    index = index;
                if(possibility.isVisited(index, dereference)) {
                    if(options.onlyGetSingle && value != null) {
                        Set<AbstractCodeValue> set = possibility.getValuesM1(index - 1, dereference);
                        if(set.size() > 1 ||
                                set.contains(CodeValuePossibility.UNKNOWN_VALUE))
                            // thanks to previous two traces with different values
                            // or one with an unknown value, continuing the trace won't
                            // change what <code>getSingle()</code> returns down the trace,
                            // even if jumps from outside are there
                            /*return*/;
                    }
                    // not the first time at the index, so
                    // check if this branch traces a new value
                    // at the already visited index
                    boolean foundNew = false;
                    if(dereference != null) {
                        if(!possibility.contains(index - 1, dereference, value))
                            foundNew = true;
                    }
                    if(!foundNew)
                        // no reason to continue, end this branch
                        return;
                    // this index is entered with
                    // with some new values of the variable
                } else {
                    possibility.setLive(index);
                    possibility.setVisited(index, dereference);
                    // first time at this index, continue regardless
                    // of the traced value
                }
                if(dereference != null) {
    // if(index == 18 && value == null)
    //     index = index;
                    // store the value before the current op
                    
                    possibility.add(index - 1, dereference, value);
                }
    // if(index == -1)
    //     index = index;
                AbstractCodeOp op = ops.get(index);
                if(dereference != null) {
                    // if the possible variable value is another
                    // variable that gets modified
                    // in the meantime, then it has no more the original
                    // value, so for a default operation set the
                    // unknown value
                    Collection<CodeVariable> targets;
                    if(field)
                        targets = CodeVariable.extractNonForeign(method.owner,
                                op.getFieldTargets());
                    else
                        targets = op.getLocalTargets();
                    for(CodeVariable r : targets)
                        if(value != null && value.equals(new CodeFieldDereference(r))) {
                                value = CodeValuePossibility.UNKNOWN_VALUE;
                        }
                }
                boolean unknownOp = true;
                if(op instanceof CodeOpAssignment && dereference != null) {
                    CodeOpAssignment a = (CodeOpAssignment)op;
                    if(a.getResult() instanceof CodeFieldDereference) {
                        CodeFieldDereference target = (CodeFieldDereference)
                                a.getResult();
                        if((target.isLocal() || field) &&
                                target.variable == variable) {
                            boolean assignmentToItself = false;
                            if(a.rvalue.value instanceof CodeFieldDereference) {
                                CodeFieldDereference f = (CodeFieldDereference)a.rvalue.value;
                                if((f.isLocal() || field) && f.variable == variable)
                                    assignmentToItself = true;
                            }
                            if(!assignmentToItself) {
//if(variable.name.indexOf("c8") != -1)
//    variable = variable;
                                value = a.rvalue.value;
                            }
                        }
                    }
                    unknownOp = false;
                } else if(op instanceof CodeOpAllocation && dereference != null) {
                    CodeOpAllocation a = (CodeOpAllocation)op;
                    if(a.getResult() instanceof CodeFieldDereference) {
                        CodeFieldDereference target = (CodeFieldDereference)
                                a.getResult();
                        if((target.isLocal() || field) &&
                                target.variable == variable) {
                            a = a;
                        }
                    }
                } else if(op instanceof CodeOpUnaryExpression && dereference != null) {
                    CodeOpUnaryExpression u = (CodeOpUnaryExpression)op;
                    if(options.propagateJavaCasts != null && u.op == AbstractCodeOp.Op.UNARY_CAST &&
                            u.objectType.isJava()) {
                        // cast of object reference to one of other type
                        if(u.getResult() instanceof CodeFieldDereference) {
                            CodeFieldDereference target = (CodeFieldDereference)
                                    u.getResult();
                            if((target.isLocal() || field) &&
                                    target.variable == variable) {
                                boolean assignmentToItself = false;
                                if(u.sub.value instanceof CodeFieldDereference) {
                                    CodeFieldDereference f = (CodeFieldDereference)u.sub.value;
                                    if(f.isLocal() && f.variable == variable)
                                        assignmentToItself = true;
                                }
                                if(!assignmentToItself) {
                                    boolean invalidCast = true;
                                    if(u.sub.value instanceof CodeConstant) {
                                        RuntimeObject referencedObject = ((RuntimeValue)
                                                ((CodeConstant)u.sub.value).value).
                                                    getReferencedObject();
                                        if(referencedObject != null &&
                                                referencedObject.codeClass.
                                                    isEqualToOrExtending(options.propagateJavaCasts.getCodeClass(
                                                            u.objectType)))
                                            invalidCast = false;
                                    }
                                    if(!invalidCast || true) {
                                        // assume the runtime cast of a non--constant is correct, it will
                                        // be verified during TADD building anyway
                                        value = u.sub.value;
                                        unknownOp = false;
                                    } else
                                        // mark the result of an invalid cast as an unknown value,
                                        // so that it is not reused in any way
                                        unknownOp = true;
                                }
                            }
                        }
                    }
                } else if(op instanceof CodeOpFinalDeclaration) {
                    CodeOpFinalDeclaration d = (CodeOpFinalDeclaration) op;
                    if(d.variable == variable) {
                        // from now, uninitialized
                        value = null;
                        unknownOp = false;
                    }
                } else if(op instanceof CodeOpBranch) {
                    // fork this branch into two other
                    CodeOpBranch b = (CodeOpBranch)op;
                    boolean variableIsCondition = conditionLocal(b) == variable;
    /*if(variableIsCondition)
        b = b;*/
                    if(b.labelRefFalse != null) {
                        int indexFalse = b.labelRefFalse.codeIndex;
                        traceBranchValue(possibility, method, indexFalse,
                                variable, variableIsCondition ?
                                    new CodeConstant(null, new ConditionalLiteral(false, index)) : value,
                                options, depth);
                    } else if(variableIsCondition)
                        value = new CodeConstant(null, new ConditionalLiteral(false, index));
                    if(b.labelRefTrue != null) {
                        int indexTrue = b.labelRefTrue.codeIndex;
                        traceBranchValue(possibility, method, indexTrue,
                                variable,  variableIsCondition ?
                                    new CodeConstant(null, new ConditionalLiteral(true, index)) : value,
                                options, depth);
                    } else if(variableIsCondition)
                        value = new CodeConstant(null, new ConditionalLiteral(true, index));
                    if(b.labelRefFalse != null &&
                            b.labelRefTrue != null)
                        // the current branch ends
                        return;
                    unknownOp = false;
                } else if(op instanceof CodeOpJump) {
                    // this branch changes into another one
                    int targetIndex = ((CodeOpJump)op).gotoLabelRef.codeIndex;
                    traceBranchValue(possibility, method, targetIndex,
                            variable, value, options,
                            depth);
                    return;
                } else if(op instanceof CodeOpReturn ||
                        op instanceof CodeOpThrow) {
                    // this branch ends
                    return;
                }
                if(unknownOp && dereference != null) {
                    Collection<CodeVariable> target;
                    if(field)
                        target = CodeVariable.extractNonForeign(method.owner,
                                op.getFieldTargets());
                    else
                        target = op.getLocalTargets();
                    for(CodeVariable targetVariable : target)
                        if(targetVariable == variable) {
                            // so some operation that is not an assignment
                            // modifies the traced variable, add the unknown
                            // value to the list of possibilities
                            value = CodeValuePossibility.UNKNOWN_VALUE;
                        }
                }
                // the updated values will either be stored in the next
                // repetition of this loop or they denote a position which
                // is not reached by this branch
            }
        } catch(StackOverflowError e) {
            System.out.println(
                    method.signature.getDeclarationDescription() + ": " +
                    "out of stack when tracing code: nesting level " + depth + "; " +
                    "method too complex, disabling array emulation " +
                    "or increasing maximum stack size may help");
            System.exit(1);
        }
    }
    /**
     * If rvalue contains a variable, attempt to get possible allocation indices using
     * this or previous traces. If not possible, return null to show, that the current
     * trace must stop early due to insufficient data.
     * 
     * @param index index of the current operation
     * @param rvalue rvalue of an assignment or of as cast
     * @param possibility possibility
     * @param options options
     * @return extracted values or null if not possible
     */
    private static List<AbstractCodeValue> extractFromVariable(int index,
            AbstractCodeValue rvalue, CodeValuePossibility possibility, Options options) {
        List<AbstractCodeValue> values = new LinkedList<>();
        CodeFieldDereference d = (CodeFieldDereference)rvalue;
        if(!d.isLocal()) {
            values.clear();
            values.add(CodeValuePossibility.UNKNOWN_VALUE);
        } else {
            CodeValuePossibility p;
            if(options.previousRun != null)
                p = options.previousRun;
            else
                p = possibility;
            SortedMap<String, AbstractCodeValue> sorted = new TreeMap<>();
            for(AbstractCodeValue v : p.getValuesM1(index, d))
                sorted.put(v == null ? "" : v.toString(), v);
            values = new LinkedList<>(sorted.values());
            if(values.isEmpty())
                // incomplete trace, stop the branch early
                values = null;
        }
        return values;
    }
    /**
     * Called by <code>traceValues()</code> to trace values of local variables
     * assigned by allocations, along a single branch. The tracing is terminated
     * if the propagated value is found to be already traced before at a given position
     * in the code.<br>
     * 
     * This is a modified copy of <code>traceBranchValue</code>, made for performance
     * reasons. Certain non--specific modifications should thus be made in both methods
     * at once. Some non--specific comments are removed from this modified copy.<br>
     * 
     * Respects <code>CodeOpFinalDeclaration</code>.<br>
     *
     * @param possibility               possibility, stores values of method's
     *                                  locals
     * @param ops                       traced method
     * @param index                     index of the operation where
     *                                  to begin this branch
     * @param variable                  the traced local variable, or null
     *                                  if it is only a search for the dead code, or, but only
     *                                  if this method is a constructor or static initializer,
     *                                  a final field
     * @param value                     a currently traced value of the variable
     *                                  at the beginning of this branch,
     *                                  can be null if a variable is uninitialized
     *                                  or it is only a search for dead code
     * @param options options
     * @param depth                     recursion depth; for testing
     */
    private static void traceBranchValueAllocation(CodeValuePossibility possibility,
            CodeMethod method, int index, CodeVariable variable,
            List<AbstractCodeValue> values, Options options, int depth) {
        ++depth;
        try {
            List<AbstractCodeOp> ops = method.code.ops;
            AbstractCodeDereference dereference = new CodeFieldDereference(variable);
            for(; index < ops.size(); ++index) {
// if(ops.size() > 20 && index == 190 && index > 60 && variable != null && variable.name.indexOf("#s6") != -1)
//    index = index;
                if(possibility.isVisited(index, dereference)) {
                    // not the first time at the index, so
                    // check if this branch traces a new value
                    // at the already visited index
                    boolean foundNew = false;
                    for(AbstractCodeValue value : values)
                        if(!possibility.contains(index - 1, dereference, value)) {
                            foundNew = true;
                            break;
                        }
                    if(!foundNew)
                        // no reason to continue, end this branch
                        return;
                    // this index is entered with
                    // with some new values of the variable
                } else {
                    ///possibility.setLive(index);
                    possibility.setVisited(index, dereference);
                    // first time at this index, continue regardless
                    // of the traced value
                }
                // store the value before the current op
                for(AbstractCodeValue value : values)
                    possibility.add(index - 1, dereference, value);
                AbstractCodeOp op = ops.get(index);
                ///
                // if the possible variable value is another
                // variable that gets modified
                // in the meantime, then it has no more the original
                // value, so for a default operation set the
                // unknown value
                Collection<CodeVariable> targets = op.getLocalTargets();
                for(CodeVariable r : targets)
                    for(AbstractCodeValue value : values)
                        if(value != null && value.equals(new CodeFieldDereference(r)))
                            throw new RuntimeException("unexpected non--constant");
                ///
                boolean unknownOp = true;
                if(op instanceof CodeOpAssignment) {
                    CodeOpAssignment a = (CodeOpAssignment)op;
                    if(a.getResult() instanceof CodeFieldDereference) {
                        CodeFieldDereference target = (CodeFieldDereference)
                                a.getResult();
                        if(target.isLocal() && target.variable == variable) {
                            boolean assignmentToItself = false;
                            if(a.rvalue.value instanceof CodeFieldDereference) {
                                CodeFieldDereference f = (CodeFieldDereference)a.rvalue.value;
                                if(f.isLocal() && f.variable == variable)
                                    assignmentToItself = true;
                            }
                            if(!assignmentToItself) {
//if(variable.name.indexOf("c8") != -1)
//    variable = variable;
                                AbstractCodeValue rvalue = a.rvalue.value;
                                if(rvalue instanceof CodeFieldDereference) {
                                    values = extractFromVariable(index, rvalue, possibility,
                                            options);
                                    if(values == null)
                                        // incomplete trace, stop this branch early
                                        return;
                                } else {
                                    values.clear();
                                    values.add(rvalue);
                                }
                            }
                        }
                    }
                    unknownOp = false;
                } else if(op instanceof CodeOpAllocation) {
                    CodeOpAllocation a = (CodeOpAllocation)op;
                    if(a.getResult() instanceof CodeFieldDereference) {
                        CodeFieldDereference target = (CodeFieldDereference)
                                a.getResult();
                        if(target.isLocal() && target.variable == variable) {
//if(variable.name.indexOf("c8") != -1)
//    variable = variable;
                            values.clear();
                            values.add(new CodeConstant(a.getStreamPos(), new Literal(index)));
                        }
                    }
                    unknownOp = false;
                } else if(op instanceof CodeOpUnaryExpression) {
                    CodeOpUnaryExpression u = (CodeOpUnaryExpression)op;
                    if(options.propagateJavaCasts != null && u.op == AbstractCodeOp.Op.UNARY_CAST &&
                            u.objectType.isJava()) {
                        // cast of object reference to one of other type
                        if(u.getResult() instanceof CodeFieldDereference) {
                            CodeFieldDereference target = (CodeFieldDereference)
                                    u.getResult();
                            if(target.isLocal() && target.variable == variable) {
                                boolean assignmentToItself = false;
                                if(u.sub.value instanceof CodeFieldDereference) {
                                    CodeFieldDereference f = (CodeFieldDereference)u.sub.value;
                                    if(f.isLocal() && f.variable == variable)
                                        assignmentToItself = true;
                                }
                                if(!assignmentToItself) {
                                    boolean invalidCast = true;
                                    if(u.sub.value instanceof CodeConstant) {
                                        RuntimeObject referencedObject = ((RuntimeValue)
                                                ((CodeConstant)u.sub.value).value).
                                                    getReferencedObject();
                                        if(referencedObject != null &&
                                                referencedObject.codeClass.
                                                    isEqualToOrExtending(options.propagateJavaCasts.getCodeClass(
                                                            u.objectType)))
                                            invalidCast = false;
                                    }
                                    if(!invalidCast || true) {
                                        // assume the runtime cast of a non--constant is correct, it will
                                        // be verified during TADD building anyway
                                        AbstractCodeValue rvalue = u.sub.value;
                                        if(rvalue instanceof CodeFieldDereference) {
                                            values = extractFromVariable(index, rvalue, possibility,
                                                    options);
                                            if(values == null)
                                                // incomplete trace, stop this branch early
                                                return;
                                        } else {
                                            values.clear();
                                            values.add(rvalue);
                                        }
                                        unknownOp = false;
                                    } else
                                        // mark the result of an invalid cast as an unknown value,
                                        // so that it is not reused in any way
                                        unknownOp = true;
                                }
                            }
                        }
                    }
                } else if(op instanceof CodeOpFinalDeclaration) {
                    CodeOpFinalDeclaration d = (CodeOpFinalDeclaration) op;
                    if(d.variable == variable) {
                        // from now, uninitialized
                        values.clear();
                        values.add(null);
                        unknownOp = false;
                    }
                } else if(op instanceof CodeOpBranch) {
                    // fork this branch into two other
                    CodeOpBranch b = (CodeOpBranch)op;
                    if(conditionLocal(b) == variable)
                        throw new RuntimeException("unexpected");
                    if(b.labelRefFalse != null) {
                        int indexFalse = b.labelRefFalse.codeIndex;
                        traceBranchValueAllocation(possibility, method, indexFalse,
                                variable, values, options, depth);
                    }
                    if(b.labelRefTrue != null) {
                        int indexTrue = b.labelRefTrue.codeIndex;
                        traceBranchValueAllocation(possibility, method, indexTrue,
                                variable, values, options, depth);
                    }
                    if(b.labelRefFalse != null &&
                            b.labelRefTrue != null)
                        // the current branch ends
                        return;
                    unknownOp = false;
                } else if(op instanceof CodeOpJump) {
                    // this branch changes into another one
                    int targetIndex = ((CodeOpJump)op).gotoLabelRef.codeIndex;
                    traceBranchValueAllocation(possibility, method, targetIndex,
                            variable, values, options,
                            depth);
                    return;
                } else if(op instanceof CodeOpReturn ||
                        op instanceof CodeOpThrow) {
                    // this branch ends
                    return;
                }
                if(unknownOp) {
                    Collection<CodeVariable> target = op.getLocalTargets();
                    for(CodeVariable targetVariable : target)
                        if(targetVariable == variable) {
                            // so some operation that is not an assignment
                            // modifies the traced variable, add the unknown
                            // value to the list of possibilities
                            values.clear();
                            values.add(CodeValuePossibility.UNKNOWN_VALUE);
                        }
                }
                // the updated values will either be stored in the next
                // repetition of this loop or they denote a position which
                // is not reached by this branch
            }
        } catch(StackOverflowError e) {
            System.out.println(
                    method.signature.getDeclarationDescription() + ": " +
                    "out of stack when tracing code: nesting level " + depth + "; " +
                    "method too complex, disabling array emulation " +
                    "or increasing maximum stack size may help");
            System.exit(1);
        }
    }
    /**
     * Traces values of local variables, taking into account execution
     * flow.<br>
     *
     * Can be also used to find the dead code.<br>
     *
     * Can be used to check initialization of variables. To declare that
     * arguments are initialized, their values in <code>arguments</code>
     * can simply be bogus, for example by assigning
     * <code>Generator.BOGUS_CODE_VALUE</code> to them.<br>
     *
     * @param method                    method with the code to trace
     * @param arguments                 values of this method's
     *                                  arguments, treated as a set of
     *                                  assignments before the method code;
     *                                  empty list for none
     * @param computeTransport          if to compute transport information
     * @param options options
     * @return                          possibility of
     *                                  values of method's locals
     */
    public static CodeValuePossibility traceValuesWithCasts(CodeMethod method,
            Map<CodeVariable, AbstractCodeValue> arguments, Options options) {
        CodeValuePossibility possibility = new CodeValuePossibility();
        // now trace values of all locals of this method
        if(method.variables.values().isEmpty())
            traceBranchValueRoot(possibility, method,
                    null, null, options);
        else
            for(CodeVariable v : method.variables.values()) {
                AbstractCodeValue value;
                // assign an argument value or a mark of an
                // uninitialized variable before the code
                value = arguments.get(v);
                // for each variable, begin its branch at
                // the beginning of the method's code
// if(v.name.indexOf("_m__") != -1 && method.code.ops.size() > 15)
//     v = v;
               if(!options.allocationUnique || v.getResultType().isJava())
                    traceBranchValueRoot(possibility, method,
                            v, value, options);
            }
        return possibility;
    }
    /**
     * Calls <code>traceValues(method, arguments, false)</code>.<br>
     * 
     * This is a convenience method, for the usual case of not tracing
     * casts of objects.
     *
     * @param method                    method with the code to trace
     * @param arguments                 values of this method's
     *                                  arguments, treated as a set of
     *                                  assignments before the method code;
     *                                  empty list for none
     * @param onlyGetSingle             if to perform incomplete tracing --
     *                                  if a variable has already multiple values 
     *                                  or an unknown value, then do not add any new ones
     *                                  except for possible "uninitialized", as only
     *                                  <code>CodeValuePossibility.getSingle()</code>,
     *                                  <code>CodeValuePossibility.getNumValues() == -1</code>,
     *                                  <code>CodeValuePossibility.getNumValues() > 1</code>,
     *                                  or other cases where a complete trace is not needed
     *                                  will be used on the trace by the outside code;
     *                                  typically true only to make the trace faster
     * @return                          possibility of
     *                                  values of method's locals
     */
    public static CodeValuePossibility traceValuesWithoutCasts(CodeMethod method,
            Map<CodeVariable, AbstractCodeValue> arguments, boolean onlyGetSingle) {
        return traceValuesWithCasts(method, arguments,
                new TraceValues.Options(null, onlyGetSingle));
    }
    public static CodeValuePossibility getUniqueAllocationTraceRecursive(CodeMethod cm,
            AbstractInterpreter interpreter) {
        CodeValuePossibility possibility = null;
        boolean stable = true;
        do {
            TraceValues.Options options = new TraceValues.Options(interpreter, false, true,
                            possibility);
            possibility = TraceValues.traceValuesWithCasts(
                    cm, new HashMap<>(), options);
            if(options.previousRun == null)
                stable = false;
            else {
                stable = possibility.traceEqual(options.previousRun, true);
            }
        } while(!stable);
        return possibility;
    }
    /**
     * Searches for reads of uninitialized variables and for a
     * dead code that is illegal to be dead. Also checks for
     * non--void methods that reach the code's end but
     * a source--level return is missing.
     * 
     * @param method method to check
     */
    public static void checkUninitializedAndDead(CodeMethod method)
            throws ParseException {
        ParseException errors = new ParseException();
        Map<CodeVariable, AbstractCodeValue> arguments =
                new HashMap<>();
        for(CodeVariable v : method.args) {
            CodeVariable vProto = method.argProtos.get(v);
            if(vProto != null)
                v = vProto;
            arguments.put(v, AbstractCodeValue.
                BOGUS_CODE_VALUE);
        }
/*if(method.signature.toString().indexOf("setIndex") != -1)
    method = method;*/
        // no need for a complete trace
        CodeValuePossibility possibility = TraceValues.traceValuesWithoutCasts(
                method, arguments, true);
        // System.out.println(possibility.toString());
        for(int index = 0; index < method.code.ops.size(); ++index) {
            AbstractCodeOp op = method.code.ops.get(index);
//if(index == 36)
//    index = index;
            if(!possibility.isLive(index) && !op.canBeDead)
                errors.addAllReports(
                        new ParseException(op.getStreamPos(),
                        ParseException.Code.ILLEGAL,
                        "unreachable code"));
            if(method.returnValue != null &&
                    index == method.code.ops.size() - 1 && possibility.isLive(index) &&
                    !(op instanceof CodeOpJump) &&
                    !(op instanceof CodeOpBranch) &&
                    !(op instanceof CodeOpThrow) &&
                    !(op instanceof CodeOpReturn && !((CodeOpReturn)op).internal)) {
                // missing source--level return statement at the last index,
                // even that the end is reached and the method does
                // not return void
                errors.addAllReports(
                        new ParseException(method.getStreamPos(),
                          ParseException.Code.ILLEGAL,
                          "method returns a value, missing return statement"));
            }
            Collection<CodeVariable> source =
                    op.getLocalSources();
            for(CodeVariable v : source) {
                if(possibility.getNumValues(index - 1,
                                new CodeFieldDereference(v)) == -1) {
                    String messagePostfix;
                    if(possibility.getValuesM1(index - 1,
                                new CodeFieldDereference(v)).size() != 1)
                        messagePostfix = "might be used";
                    else
                        messagePostfix = "is used";
                    messagePostfix += " uninitialized in this expression";
                    if(v.isInternal()) {
                        if(!errors.reportsExist())
                            // there should not be any problems with internal
                            // variables if the code is valid
                            throw new RuntimeException("unexpected: " + v.toString() + " " +
                                    messagePostfix);
                    } else
                        errors.addAllReports(new
                                ParseException(op.getStreamPos(),
                                    ParseException.Code.ILLEGAL,
                                    "${variable} " + v.toSourceNameString() + " " +
                                    messagePostfix));
                }
            }
        }
        if(errors.reportsExist())
            throw errors;
    }
    /**
     * Searches for writes to possibly initialised finals.
     * 
     * @param method a method
     * @param v a final variable, field or local
     */
    public static void checkInitializedFinals(CodeMethod method,
            CodeVariable cv) throws ParseException {
        if(method.code == null)
            // skip an abstract method
            return;
        ParseException errors = new ParseException();
        CodeValuePossibility possibility = new CodeValuePossibility();
/*if(method.signature.toString().indexOf("method") != -1)
    cv = cv;*/
        if(!cv.finaL)
            throw new RuntimeException("not a final");
        // no need for a complete trace
        traceBranchValueRoot(possibility, method,
                cv, null, new TraceValues.Options(null, true));
        for(int index = 0; index < method.code.ops.size(); ++index) {
            AbstractCodeOp op = method.code.ops.get(index);
            Collection<CodeVariable> target;
            if(cv.isLocal())
                target = op.getLocalTargets();
            else
                target = CodeVariable.extractNonForeign(method.owner,
                        op.getFieldTargets());
            if(target.contains(cv)) {
                String message = null;
                Set<AbstractCodeValue> inside;
                boolean externalInitializer =
                    (
                        !cv.isLocal() &&
                        (
                            // a static field, blank or not, must have been initialized after
                            // class construction
                            (cv.context != Context.NON_STATIC && !method.isStaticInitializationMethod()) ||
                            // a non--static field, blank or not, must have been initialized
                            // after object instantiation
                            (cv.context == Context.NON_STATIC &&
                                !(
                                    method.isObjectInitializationMethod() ||
                                    method.isConstructor())
                                ) ||
                            // additionally, non--blanks can not be written to outside
                            // respective initialization methods
                            (!cv.blankFinalField &&
                                !(
                                    (cv.context != Context.NON_STATIC && method.isStaticInitializationMethod()) ||
                                    (cv.context == Context.NON_STATIC && method.isObjectInitializationMethod())
                                )
                            )
                        )
                    ) ||
                    (
                        cv.isLocal() &&
                        // an argument, initialized within a call
                        method.writtenExternally(cv)
                    );
                if(externalInitializer)
                    // external initializer, no need for trace
                    inside = null;
                else
                    inside = possibility.getValuesM1(index - 1,
                            new CodeFieldDereference(cv));
                if(externalInitializer || !inside.contains(null))
                    message = "is already initialized";
                else if(inside.size() > 1)
                    message = "might already be initialized";
                if(message != null) {
                    errors.addAllReports(new
                            ParseException(op.getStreamPos(),
                                ParseException.Code.ILLEGAL,
                                "${finalVariable} " + cv.toSourceNameString() + " " +
                                message));
                }
            }
        }
        if(errors.reportsExist())
            throw errors;
    }
    /**
     * Checks for validity of an operation except for
     * <code>AbstractCodeOpInvocation</code>, at which a blank might
     * be uninitialized.
     * 
     * @param blank blank final variable
     * @param owner owner of the method, to which belongs <code>op</code>
     * @param op operation to check
     * @return null if the operation is valid, otherwise a descriptive message
     */
    protected static String checkUninitializedBlankOp(CodeVariable blank,
            CodeClass owner, AbstractCodeOp op) {
        if(op instanceof AbstractCodeOpInvocation)
            throw new RuntimeException("invocations are not checked by this method");
        String message = null;
        // check for <code>blank</code>
        List<CodeVariable> fields = CodeVariable.extractNonForeign(
                owner, op.getFieldSources());
        for(CodeVariable field : fields)
            if(field == blank)
                return "statement at " + op.getStreamPos() + " reads " +
                        blank.name + " before"; //c
        // check for "this"
        if(op instanceof CodeOpAssignment) {
            CodeOpAssignment a = (CodeOpAssignment)op;
            if(a.rvalue.value instanceof CodeFieldDereference) {
                CodeFieldDereference d = (CodeFieldDereference)a.rvalue.value;
                if(d.isLocal() && d.variable.name.equals(Method.LOCAL_THIS))
                    return "statement at " + op.getStreamPos() + " copies this";
            }
        }
        return message;
    }
    /**
     * Checks if a given blank has been read in a given call tree. If not,
     * null is returned. If yes, a descriptive message is returned. If this
     * method is unable to search through the tree, a respective message is returned
     * as well.
     * 
     * @param method method
     * @param call root of the call tree
     * @param blank blank to check
     */
    protected static String checkReads(CodeMethod method,
            AbstractCodeOpInvocation invocation, CodeVariable blank) {
        CodeOpCall call;
        if(invocation instanceof CodeOpCall)
            call = (CodeOpCall)invocation;
        else
            call = null;
        /*
        CodeOpAllocation allocation;
        if(invocation instanceof CodeOpAllocation)
            allocation = (CodeOpAllocation)invocation;
        else
            allocation = null;
            */
        boolean self;
        if(invocation instanceof CodeOpCall && invocation.isStatic()) {
            CodeClass superClass = ((CodeOpCall)invocation).methodReference.method.owner;
            self = method.owner.isEqualToOrExtending(superClass);
        } else if(invocation instanceof  CodeOpCall) {
            CodeFieldDereference thiS = (CodeFieldDereference)invocation.args.get(0).value;
            self = thiS.isLocal() && thiS.variable.name.equals(Method.LOCAL_THIS);
        } else
            // a constructor of the same class is filtered out by the caller
            self = false;
        if(call != null && call.isVirtual() && call.methodReference.firstPassMethod.flags.allowsOverride &&
                self)
            return "call at " + invocation.getStreamPos() + " is virtual and calls an overridable method"; //c
        else {
            CodeMethod called;
            if(call != null)
                called = call.methodReference.method;
            else
                called = null;
            int argIndex = 0;
            boolean traceCalled = false;
            /*
            // super(), this() and *object are safe
            if(     // a constructor in a call must be super() or this() 
                    !called.isConstructor() &&
                    !called.isObjectInitializationMethod())
                    */
            for(RangedCodeValue r : invocation.args) {
                if(r.value instanceof CodeFieldDereference &&
                        ((CodeFieldDereference)r.value).isLocal() &&
                        ((CodeFieldDereference)r.value).variable.name.
                            equals(Method.LOCAL_THIS)) {
                    if(call == null)
                        // any "this" in an invocation is a spill
                        return "object creation at " + invocation.getStreamPos() + " spills this"; //c
                    else if(called != null) {
                        if(called.context != Context.NON_STATIC || argIndex != 0) {
                            // "this" passed, but not as an object reference to a non--static method
                            return "call at " + invocation.getStreamPos() + " spills this"; //c
                        }
                        if(called.context == Context.NON_STATIC || argIndex == 0) {
                            // a non--static method within the same class
                            traceCalled = true;
                        }
                        if(called.context != Context.NON_STATIC)
                            traceCalled = true;
                    }
                }
                if(r.value instanceof CodeFieldDereference) {
                    CodeFieldDereference arg = (CodeFieldDereference)r.value;
                    if(arg.variable == blank &&
                            (blank.context != Context.NON_STATIC || arg.object.name.equals(Method.LOCAL_THIS)))
                        return "passed as parameter at " + invocation.getStreamPos(); //c
                }
                ++argIndex;
            }
            if(blank.context != Context.NON_STATIC && called != null && called.context != Context.NON_STATIC)
                // any static method can potentially read a static field
                traceCalled = true;
            // do not trace static methods and foreign methods,
            // as they can not access <code>blank</code> anyway
            // if "this" has not been spilled
            if(traceCalled) {
                for(int index = 0; index < called.code.ops.size(); ++index) {
                    AbstractCodeOp op = called.code.ops.get(index);
                    if(op instanceof AbstractCodeOpInvocation) {
                        String s = checkReads(method, (AbstractCodeOpInvocation)op, blank);
                        if(s != null)
                            return s;
                    } else {
                        String s = checkUninitializedBlankOp(blank, called.owner, op);
                        if(s != null)
                            return s;
                    }
                }
            }
        }
        return null;
    }
    /**
     * Checks if the constructor or a static initializer <code>method</code>
     * initializes a blank final <code>blank</code> as it should, before
     * it exits, or spills "this" directly or within calls, or the blank is read.
     * Also denotes <i>any</i> finals, if in object initializers.<br>
     * 
     * The discussed read of the blank is only checked within calls, as same--method reads
     * should either (1) be found by normal initialization checking if
     * a constructor of a static initializer, (2) object initializer does not contain
     * such reads, beside possibly within calls or of fields declared earlier in a class.
     * 
     * @param method constructor or initialization method
     * @param blank blank final, staticity matches the constructor; for initializers,
     * the final does not need to be generally blank, see
     * <code>Generator.checkAssignmentOfFinals()</code> for details
     * @param testExits if exits should be tested, too; true for static initializers and
     * constructors, false for object initializers as they exit to constructors anyway
     */
    public static void checkBlankFinalInitialized(CodeMethod method, CodeVariable blank,
            boolean testExits) throws ParseException {
//if(method.signature.toString().equals("*static/") &&
//        method.owner.name.equals("#default.VericsPrinter"))
//    method = method;
        if(!method.isConstructor() && !method.isStaticInitializationMethod() &&
                !method.isObjectInitializationMethod())
            throw new RuntimeException("not a constructor or a static initializer or an object initializer");
        if(method.context != blank.context)
            throw new RuntimeException("staticity mismatch");
        if(blank.isLocal())
            throw new RuntimeException("must be a field");
        if(!blank.blankFinalField && !
                (method.isStaticInitializationMethod() || method.isObjectInitializationMethod()))
            throw new RuntimeException("must be blank if method is not object initialization");
        CodeValuePossibility possibility = new CodeValuePossibility();
        traceBranchValueRoot(possibility, method,
                blank, null, new TraceValues.Options(null, true));
        for(int index = 0; index < method.code.ops.size(); ++index) {
            AbstractCodeOp op = method.code.ops.get(index);
            Set<AbstractCodeValue> inside = possibility.getValuesM1(index - 1,
                    new CodeFieldDereference(blank));
            if(inside.contains(null)) {
                boolean violates = false;
                String message = "";
                if((op instanceof CodeOpReturn && testExits) ||
                        op instanceof AbstractCodeOpInvocation) {
                    if(op instanceof CodeOpReturn)
                        violates = true;
                    else if(op instanceof AbstractCodeOpInvocation) {
                        if(op instanceof CodeOpCall && ((CodeOpCall)op).methodReference.method.isConstructor()) {
                            CodeClass superClass = ((CodeOpCall)op).methodReference.method.owner;
//                            if(superClass.name.indexOf("Agent") != -1)
//                                op = op;
                            if(!method.owner.isEqualToOrExtending(superClass))
                                throw new RuntimeException("call to an invalid constructor");
                            if(superClass.isEqualToOrExtending((CodeClass)blank.owner))
                                // assuming, that the called constructor initializes the blank correctly;
                                // if not, it will be revealed when <code>checkBlankFinalInitialized</code>
                                // is called on that constructor
                                break;
                        }
                        AbstractCodeOpInvocation inv = (AbstractCodeOpInvocation)op;
                        String s = checkReads(method, inv, blank);
                        if(s != null) {
                            message = ", reason: " + s; //c
                            violates = true;
                        }
                    } else {
                        // source "this" in a non--invocation assumed to be a spill
                        Set<CodeVariable> locals = op.getLocalSources();
                        for(CodeVariable local : locals)
                            if(local.name.equals(Method.LOCAL_THIS)) {
                                message  = ", but spilled this at " + op.getStreamPos();
                                violates = true;
                            }
                    }
                } else {
                    String s = checkUninitializedBlankOp(blank, method.owner, op);
                    if(s != null) {
                        message = ", reason: " + s;
                        violates = true;
                    }
                }
                if(violates) {
                    StreamPos streamPos;
                    if(method.isStaticInitializationMethod() ||
                            method.isObjectInitializationMethod())
                        // initialization methods have no definite position,
                        // replace with the position of the variable
                        streamPos = blank.getStreamPos();
                    else
                        streamPos = method.getStreamPos();
                    // only one error message per blank per constructor
                    throw new ParseException(streamPos,
                        ParseException.Code.ILLEGAL, 
                        "${finalVariable} " + blank.toSourceNameString() + " " +
                        "might be used uninitialized" + message);
                }
            }
        }
    }
    /**
     * An optimization method that deletes unused code.<br>
     *
     * Searches for dead code. If <code>removeAll</code> is false,
     * then a dead code that is illegal to be dead
     * causes position of the code to be returned, otherwise
     * the code is removed. If <code>removeAll</code> is true,
     * all dead code is removed. The latter should be used if
     * dead code is already tested for being legal and optimizations
     * were introduced, that make additional code to be dead.
     *
     * Does not check for branches with constant conditions.
     * If it is desired these must be optimized before this
     * optimization.<br>
     *
     * If the last operation is a return and it is a dead code,
     * then, if it was put by the compiler it is removed,
     * otherwise its source position is returned.<br>
     *
     * If the code was earlier verified by
     * <code>Generator.checkUninitializedAndDead</code> and no
     * dead code was found, then this method should always
     * return null.<br>
     *
     * Invalidates possible label indices.
     * 
     * @param method                    method with the code to
     *                                  check and modify
     * @param removeAll                 remove all dead code, do
     *                                  not check if it is legal
     * @param changed                   its first element is set to
     *                                  if something has been optimized
     * @param possibility               cached possibility, can be
     *                                  incomplete; this method can invalidate
     *                                  it
     * @return                          if <code>removeAll</code> is
     *                                  true, then false; otherwise,
     *                                  the position in the source stream
     *                                  if a dead code that was present
     *                                  in the source was found,
     *                                  as opposed to the code added
     *                                  later by the compiler;
     *                                  otherwise, that is, if no such
     *                                  code was found, null
     */
    public static StreamPos trimDeadCode(CodeMethod method, boolean removeAll,
            boolean[] changed, CodeValuePossibility possibility) {
        changed[0] = false;
        StreamPos pos = null;
        if(possibility == null)
            // no need fo a complete trace
            possibility = traceValuesWithoutCasts(method,
                // only dead code is searched for, the traced values are
                // ignored
                new HashMap<CodeVariable, AbstractCodeValue>(), true);
        for(int index = method.code.ops.size() - 1; index >= 0; --index)
            if(!possibility.isLive(index)) {
                boolean found = false;
                AbstractCodeOp op = method.code.ops.get(index);
                if(removeAll || index == method.code.ops.size() - 1) {
                    if(!removeAll &&
                            (!(op instanceof CodeOpReturn) ||
                            !((CodeOpReturn)op).internal))
                        found = true;
                    else {
                        method.code.ops.remove(index);
                        changed[0] = true;
                    }
                } else
                    found = true;
                if(found)
                    pos = op.getStreamPos();
            }
        return pos;
    }
    /**
     * If the object in a object::field is <code>c</code>, and d = c::field is a
     * constant as well, then replace object::field with d.
     * 
     * @param value to replace
     * @param r variable to replace
     * @param c replacement java constant
     * @param interpreter interpreter
     * @param rm runtime method
     * @return if anything has been replaced
     */
    private static boolean replaceFieldDereference(RangedCodeValue rangedValue,
            CodeVariable r, CodeConstant c, AbstractInterpreter interpreter,
            RuntimeMethod rm) {
        boolean changed = false;
        if(rangedValue.value instanceof CodeFieldDereference) {
            CodeFieldDereference f = (CodeFieldDereference)rangedValue.value;
            if(f.object == r) {
                boolean knownAlready = rm.getVariables().contains(f.object);
                try {
                    if(!knownAlready)
                        rm.setValue(interpreter, rm, f.object, new RuntimeValue(c.value));
                    RuntimeValue newConstant = interpreter.getValue(rangedValue, rm);
                    if(newConstant.type.isJava()) {
                        // java--type fields are assumed to be constant already, so
                        // their values may propagate
                        rangedValue.value = new CodeConstant(rangedValue.value.getStreamPos(),
                                newConstant);
                        changed = true;
                    }
                } catch(InterpreterException e) {
                    // an invalid code can produce exceptions, ignore
                    // as it will be detected later anyway
                } finally {
                    if(!knownAlready)
                        rm.removeVariable(f.object);
                }
            }
        }
        return changed;
    }
    /**
     * If it can be determined, that a given read of a given variable
     * is the single, which uses the read value.
     * 
     * @param method method
     * @param transport transport
     * @param r variable
     * @param index operation which reads the variable
     * @return if it is known, that the variable passes its current
     * value only to the operation
     */
    protected static boolean knownSingleUse(CodeMethod method,
            CodeVariableTransport transport, CodeVariable r, int index) {
        if(index > 0) {
            CodeFieldDereference d = new CodeFieldDereference(r);
            AbstractCodeOp op = method.code.ops.get(index);
            for(int i = index - 1; i >= 0; --i) {
                AbstractCodeOp prevOp = method.code.ops.get(i);
        //if(op.getResult() != null && op.getResult().equals(d))
        //    op = op;
                boolean safeOp = prevOp instanceof CodeOpAssignment ||
                            prevOp instanceof CodeOpUnaryExpression ||
                            prevOp instanceof CodeOpBinaryExpression ||
                            prevOp instanceof CodeOpThrow;
                if(!safeOp) {
                    if(prevOp instanceof CodeOpBranch) {
                        CodeOpBranch b = (CodeOpBranch)prevOp;
                        boolean falseIn = b.labelRefFalse == null ||
                                (b.labelRefFalse.codeIndex >= i && b.labelRefFalse.codeIndex <= index);
                        boolean trueIn = b.labelRefTrue == null ||
                                (b.labelRefTrue.codeIndex >= i && b.labelRefTrue.codeIndex <= index);
                        safeOp = falseIn && trueIn;
                    } else if(prevOp instanceof CodeOpJump) {
                        CodeOpJump j = (CodeOpJump)prevOp;
                        safeOp = j.gotoLabelRef == null ||
                                (j.gotoLabelRef.codeIndex >= i && j.gotoLabelRef.codeIndex <= index);
                    }
                }
                if(!method.code.isJumpTarget(index) &&
                        (i < index - 1 ||
                            !transport.isTransported(index*2 + 1, d) ||
                            (op.getResult() != null && op.getResult().equals(d))) &&
                        safeOp) {
                    if(prevOp.getResult() != null &&
                            prevOp.getResult().equals(d))
                        return true;
                    else if(prevOp.getLocalSources().contains(r))
                        break;
//if(true)
//    i = i;
                } else
                    break;
            }
        }
        return false;
    }
    /**
     * An optimization that simplifies dependencies between
     * values in a method.<br>
     *
     * Traces local variable values, taking into account execution flow,
     * and then propagates these values, replacing variables with what
     * is known they contain, like a constant or the value of another
     * variable.<br>
     *
     * Removes meaningless assignments with <code>CodeOpNone</code>.
     * 
     * No reads of uninitialized or
     * possibly uninitialized variables should exist, so
     * <code>Generator.checkUninitializedAndDead</code> should
     * not throw an exception.<br>
     *
     * Source--level primitive ranges are not optimized out. If
     * variable primitive ranges are optimized down to constants,
     * this tracing may be more effective.
     *
     * @param method                    method with the code to modify
     * @param arguments                 values of this method
     *                                  arguments, treated as a set of
     *                                  assignments before the method
     *                                  code; empty list for none
     * @param possibility               pre--computed trace, null to
     *                                  compute inside this method; does not
     *                                  need to be complete; if
     *                                  this method returns true, the
     *                                  trace might be no longer actual
     * @param transport                 transport or null; if not null, allows
     *                                  some fields to be propagated as well
     * @param interpreter if not null, allows for propagating java type
     * constants, if class cast can be verified
     * @param rm if interpreter is not null and this argumetn is not
     * null, can use <code>replaceFieldDereference()</code>
     * @return                          if anything has been changed in the
     *                                  code
     */
    public static boolean propagateValues(CodeMethod method,
            Map<CodeVariable, AbstractCodeValue> arguments,
            CodeValuePossibility possibility, CodeVariableTransport transport,
            AbstractInterpreter interpreter, RuntimeMethod rm) {
if(method.code.ops.size() > 10)
    method = method;
        boolean totalStable = true;
        boolean stable;
        do {
            stable= true;
            // check for pre--computed trace
            if(!(totalStable && possibility != null))
                // get a mapping of locals
                // no need for a complete trace
                possibility = traceValuesWithCasts(method, arguments,
                        new TraceValues.Options(interpreter, true));
            // System.out.println(possibility.toString());
            for(int index = 0; index < method.code.ops.size(); ++index) {
                AbstractCodeOp op = method.code.ops.get(index);
                boolean hasOnlyDirectLocals =
                        op.getFieldSources().isEmpty() &&
                        op.getFieldTargets().isEmpty() &&
                        op.getIndexDereferences().isEmpty();
                Collection<CodeVariable> source;
                if(op instanceof CodeOpReturn)
                    // stay away from <code>return</code>, as its
                    // return value is not replaceable
                    source = new LinkedList<>();
                else
                    source = CodeVariablePrimitiveRange.noRangeFirstV(
                            op.getLocalSources());
                /*
                if(source.size() == 6)
                    op.getLocalSources();
                    */
                for(CodeVariable r : source) {
                    AbstractCodeValue value =
                            possibility.getSingleM1(index - 1,
                                new CodeFieldDereference(r));
                    if(value instanceof AbstractCodeDereference) {
                        AbstractCodeDereference v = (AbstractCodeDereference)value;
                        boolean local = v instanceof CodeFieldDereference &&
                                ((CodeFieldDereference)v).isLocal();
                        boolean nearField = false;
                        if(!local) {
                            // check if the value of the variable that propagates the field
                            // is used only in this operation, and that the value has been
                            // assigned in the previous operation, which did not have any
                            // side effects
                            //
                            // if all this is true, then it is safe to move the reading of the
                            // field to this op
                            CodeFieldDereference d = new CodeFieldDereference(r);
                            AbstractCodeOp prevOp = method.code.ops.get(index - 1);
//if(op.getResult() != null && op.getResult().equals(d))
//    op = op;
                            if(transport != null && index > 0 &&
                                    !method.code.isJumpTarget(index) &&
                                    (!transport.isTransported(index*2 + 1, d) ||
                                        (op.getResult() != null && op.getResult().equals(d))) &&
                                    (prevOp instanceof CodeOpAssignment ||
                                        prevOp instanceof CodeOpUnaryExpression ||
                                        prevOp instanceof CodeOpBinaryExpression) &&
                                    prevOp.getResult() != null &&
                                    prevOp.getResult().equals(d))
                                nearField = true;
                        }
                        if((local || nearField) && !r.rangeWhenReadImportant(null) &&
                                // disallow move of range variables, unless a near field
                                !(v.getTypeVariablePrimitiveRange() != null &&
                                    !v.getTypeVariablePrimitiveRange().isEvaluatedAsConstant() &&
                                    !nearField) &&
                                // a variable primitive range might be shared
                                // by other ops, with different values of
                                // <code>v</code>, this trace does not check for
                                // that
                                !op.belongsToVariableRange(new CodeFieldDereference(r))) {
                            // the replacement variable exists and is a local or a near field
                            if(!(v instanceof CodeFieldDereference) ||
                                    r != ((CodeFieldDereference)v).variable) {
                                // never join field accesses from different ops
                                // into a single op
                                if(!nearField || hasOnlyDirectLocals) {
                                    if(op.replaceLocalSourceVariable(r, v))
                                        stable = false;
                                    else {
                                        // fields can not always replace a local
                                        if(!nearField)
                                            throw new RuntimeException("no replacement");
                                    }
                                }
                            }
                        }
                    } else if(value instanceof CodeConstant) {
                        CodeConstant c = (CodeConstant)value;
                        // do not propagate nulls, as they can be assigned to
                        // variables of java class type, what in turn might
                        // cause an attempt to replace an object part of a
                        // dereference with a constant, what is not allowed
                        //
                        // by the way, such an attempt would mean, that the
                        // dereference would cause null pointer exception when
                        // executed
                        //
                        // if this propagation is at runtime, for example as
                        // a part of <code>RuntimeStaticAnalysis</code>, it might happen
                        // that an object part has a known constant, but, as
                        // already discussed, this is not allowed, so such constant is
                        // not propagated as well; it can be found in the trace map
                        // instead, if needed -- in such a way extracts it
                        // <code>RuntimeStaticAnalysis.getRuntimeField</code>
                        if(!c.value.isNull() && !r.rangeWhenReadImportant(null) &&
                                // in case this tracing is at runtime
                                (!c.value.type.isJava() || (interpreter != null /*&& hasOnlyDirectLocals*/)) &&
                                // a probabilistic value can be propagated only if it is known that
                                // it is used once
                                (!c.getResultType().isProbabilistic() ||
                                    (transport != null && knownSingleUse(method, transport, r, index)))) {
                            boolean invalidClassCast = c.value.type.isJava() &&
                                ((RuntimeValue)c.value).getReferencedObject() != null &&
                                !((RuntimeValue)c.value).getReferencedObject().codeClass.
                                        isEqualToOrExtending(interpreter.getCodeClass(r.getResultType()));
if(invalidClassCast)
    c = c;

                            if(!invalidClassCast) {
                                if(c.value.type.isJava() && op instanceof CodeOpUnaryExpression) {
                                    CodeOpUnaryExpression u = (CodeOpUnaryExpression)op;
if(u.op == AbstractCodeOp.Op.UNARY_CAST)
    u = u;
                                    if(u.op == AbstractCodeOp.Op.UNARY_CAST &&
                                                !((RuntimeValue)c.value).getReferencedObject().codeClass.
                                                    isEqualToOrExtending(interpreter.getCodeClass(u.objectType)))
                                            invalidClassCast = true;
                                }
if(c.value.type.isJava())
    c = c;
                                if(!invalidClassCast) {
if(op instanceof CodeOpUnaryExpression &&
        ((CodeOpUnaryExpression)op).op == AbstractCodeOp.Op.UNARY_CAST)
    op = op;
                                    if(r.isVariablePRBound()) {
                                        if(!method.replaceBound(r, c))
                                            throw new RuntimeException("no replacement");
                                        stable = false;
                                    } else {
                                        if(!c.value.type.isJava() || hasOnlyDirectLocals) {
                                            if(!op.replaceLocalSourceVariable(r, c))
                                                throw new RuntimeException("no replacement");
                                            stable = false;
                                        } else if(rm != null) {
                                            if(op instanceof CodeOpUnaryExpression) {
                                                CodeOpUnaryExpression u = (CodeOpUnaryExpression)op;
                                                if(replaceFieldDereference((RangedCodeValue)u.sub, r, c,
                                                        interpreter, rm))
                                                    stable = false;
                                            } else if(op instanceof CodeOpBinaryExpression) {
                                                CodeOpBinaryExpression b = (CodeOpBinaryExpression)op;
                                                if(replaceFieldDereference((RangedCodeValue)b.left, r, c,
                                                        interpreter, rm))
                                                    stable = false;
                                                if(replaceFieldDereference((RangedCodeValue)b.right, r, c,
                                                        interpreter, rm))
                                                    stable = false;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        /*
                        if(op instanceof CodeOpAssignment && !method.code.isJumpTarget(index)) {
                            CodeOpAssignment a = (CodeOpAssignment)op;
                            Collection<CodeVariable> l = a.getResult().extractPlainLocal();
                            if(!l.isEmpty()) {
                                CodeVariable left = l.iterator().next();
                                if(left == 
                                        a.rvalueIsOpaque() && a.rvalue.value instanceof CodeConstant) {
                                    CodeConstant right = (CodeConstant)a.rvalue.value;
                                    if(right.equals(c))
                                }
                            }
                        }
                         */
                    } else if(op instanceof CodeOpAssignment &&
                            ((CodeOpAssignment)op).isOpaque()) {
                        /*
                        CodeFieldDereference d = new CodeFieldDereference(r);
                        Set<AbstractCodeValue> s = possibility.getValuesM1(index - 1, d);
                        if(s.size() > 1) {
                            boolean ok = true;
                            for(AbstractCodeValue v : s) {
                                if(v == null || v == CodeValuePossibility.UNKNOWN_VALUE) {
                                    ok = false;
                                    break;
                                }
                            }
                            if(ok) {
                                if(!transport.isTransported(index*2 + 1, d)) {
                                    
                                    ok = ok;
                                }
                            }
                        }
                        */
                    }
                }
            }
            if(!stable)
                totalStable = false;
        } while(!stable);
        return !totalStable;
    }
    /**
     * Removes assignments, that assign a value to a local variable that the
     * variable already has, or that is no further used.<br>
     * 
     * The value must be local or a constant, as dependencies
     * between threads are not checked.
     *
     * @param method                    method with the code to modify
     * @param arguments                 optional values of this method
     *                                  arguments, treated as a set of
     *                                  assignments before the method
     *                                  code; used only if <code>possibility</code>
     *                                  is null
     * @param possibility               pre--computed trace, null to
     *                                  compute inside this method; can be incomplete; if
     *                                  this method returns true, the
     *                                  trace might be no longer actual
     * @param transportCheck remove assignments to primitive locals,
     * that result in no transport; should generally be false only to save
     * time on computing the transport, or for compiler testing
     * @return                          if anything has been changed in the
     *                                  code
     */
    public static boolean removeRedundantAssignments(CodeMethod method,
            Map<CodeVariable, AbstractCodeValue> arguments,
            CodeValuePossibility possibility, boolean transportCheck) {
        boolean totalStable = true;
        boolean stable = true;
        //
        // test, if the variable already has a given value
        //
        if(possibility == null)
            // no need for a complete trace
            possibility = traceValuesWithoutCasts(method, arguments, true);
        for(int index = 0; index < method.code.ops.size(); ++index) {
            AbstractCodeOp op = method.code.ops.get(index);
            if(op instanceof CodeOpAssignment) {
                CodeOpAssignment a = (CodeOpAssignment)op;
                // result can be opaque if this assignment does nothing,
                // as the result can contain only a variable primitive range,
                // which obviously must have already been used against the
                // rvalue before, if it has already been assigned to the variable
                if(!a.rvalueIsOpaque()) {
                    AbstractCodeDereference result = a.getResult();
                    if(result instanceof CodeFieldDereference) {
                        CodeVariable vResult = ((CodeFieldDereference)result).variable;
                        if(vResult.isLocal()) {
                            // result is a local
                            if(a.rvalue.value instanceof CodeFieldDereference) {
                                CodeFieldDereference f = (CodeFieldDereference)a.rvalue.value;
                                if(f.isLocal() && f.variable == vResult) {
                                    // assignment to itself
                                    // such assignments can sometimes be undetected by the
                                    // following "value before" check, as that check,
                                    // as opposed to this one, requires the values to
                                    // be traceable by this class
                                    a.rvalue = null;
                                    stable = false;
                                }
                            }
                            if(a.rvalue != null) {
                                // get its value before the assignment
                                AbstractCodeValue valueBefore =
                                        possibility.getSingleM1(index - 1,
                                            new CodeFieldDereference(vResult));
                                if(valueBefore instanceof CodeConstant ||
                                        (valueBefore instanceof CodeFieldDereference &&
                                        ((CodeFieldDereference)valueBefore).isLocal())) {
                                    // value before is constant or local
                                    if(!method.code.isJumpTarget(index)) {
                                        if(a.rvalue.value.equals(valueBefore)) {
                                            // this assignment does nothing
                                            a.rvalue = null;
                                            stable = false;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        if(!stable) {
            Optimizer.optimizeLabels(method.code);
            Optimizer.setLabelIndices(method);
            stable = true;
            totalStable = false;
        }
        if(transportCheck) {
            //
            // test, if the variable transports anything
            //
            CodeVariableTransport t = traceTransport(method);
            for(int index = 0; index < method.code.ops.size(); ++index) {
                AbstractCodeOp op = method.code.ops.get(index);
                if(op instanceof CodeOpAssignment) {
                    CodeOpAssignment a = (CodeOpAssignment)op;
                    if(!a.isOpaque() && !a.getResult().isVolatile() &&
                            a.getResult().extractPlainLocal().iterator().next().getResultType().isPrimitive() &&
                            !t.isTransported(index*2 + 1, a.getResult())) {
                        a.rvalue = null;
                        stable = false;
                    }
                }
            }
            if(!stable) {
                Optimizer.optimizeLabels(method.code);
                Optimizer.setLabelIndices(method);
                totalStable = false;
            }
        }
        return !totalStable;
    }
    /**
     * Checks if a given operation <i>i</i>, whose jump labels are to
     * be possibly forwarded, has a label pointing to a branch. If
     * that branch's condition is known when <i>i</i> jumps there,
     * then <i>i</i> is modified as discussed in
     * <code>forwardJumpsWithBranches</code>.
     * 
     * @param method method
     * @param index index of the operation <i>i</i> to modify; must be
     * a jump or a branch
     * @param forward index of the possible target branch; if there is
     * not a branch at that index, then this method does nothing
     * @param branchCondition meaningful only if <i>i</i> is a branch;
     * denotes, if the label to modify is that for a true condition or
     * for a false condition in <i>i</i>
     * @param possibility pre--computed trace, does not need to be complete
     * as only <code>CodeValuePossibility.getSingle()<code> is used on it
     * @return if anything changed in <i>i</i>
     */
    static boolean forwardIfBranch(CodeMethod method, int index, int forward,
            boolean branchCondition, CodeValuePossibility possibility) {
        boolean stable = true;
        AbstractCodeOp op = method.code.ops.get(index);
        AbstractCodeOp target = method.code.ops.get(
                forward);
        if(target instanceof CodeOpBranch) {
            CodeOpBranch b =(CodeOpBranch)target;
            CodeVariable local = conditionLocal(b);
            if(local != null && !b.condition.hasRestrictiveSourceLevelRange()) {
                AbstractCodeValue value =
                        possibility.getSingleM1(index - 1,
                            new CodeFieldDereference(local));
                if(op instanceof CodeOpBranch &&
                        (value == null || !(value instanceof CodeConstant)) &&
                        conditionLocal((CodeOpBranch)op) == local) {
                    // <code>op</code> is a branch, whose condition is
                    // the same as that of <code>target</code>, make
                    // use of that
                    value = new CodeConstant(null, new Literal(branchCondition));
                }
                if(value != null && value instanceof CodeConstant) {
                    CodeConstant c = (CodeConstant)value;
                    if(c.value.getBoolean()) {
                        if(b.labelRefTrue != null)
                            forward = b.labelRefTrue.codeIndex;
                        else
                            ++forward;
                    } else {
                        if(b.labelRefFalse != null)
                            forward = b.labelRefFalse.codeIndex;
                        else
                            ++forward;
                    }
                    AbstractCodeOp forwardOp = method.code.ops.get(forward);
                    if(forwardOp.label == null) {
                        forwardOp.label = method.newLabel();
                        forwardOp.label.codeIndex = forward;
                    }
                    CodeLabel label = new CodeLabel(forwardOp.label);
                    if(op instanceof CodeOpJump) {
                        CodeOpJump j = (CodeOpJump)op;
                        j.gotoLabelRef = label;
                    } else if(op instanceof CodeOpBranch) {
                        CodeOpBranch j = (CodeOpBranch)op;
                        if(branchCondition)
                            j.labelRefTrue = label;
                        else
                            j.labelRefFalse = label;
                    } else
                        throw new RuntimeException("unknown operation");
                    stable = false;
                }
            }
        }
        return !stable;
    }
    /**
     * If there is a jump or a branch, after whose execution some boolean
     * variable has a known value, and the jump is to a branch that uses
     * that variable, then the jump can be forwarded using the branch's
     * targets.
     *
     * @param method                    method with the code to modify
     * @param arguments                 optional values of this method
     *                                  arguments, treated as a set of
     *                                  assignments before the method
     *                                  code; used only if <code>possibility</code>
     *                                  is null
     * @param possibility               pre--computed trace, null to
     *                                  compute inside this method; can be incomplete; if
     *                                  this method returns true, the
     *                                  trace might be no longer actual
     * @return                          if anything has been changed in the
     *                                  code
     */
    public static boolean forwardJumpsWithBranches(CodeMethod method,
            Map<CodeVariable, AbstractCodeValue> arguments,
            CodeValuePossibility possibility) {
        boolean totalStable = true;
        if(possibility == null)
            // no need for a complete trace
            possibility = traceValuesWithoutCasts(method, arguments, true);
        boolean stable;
        do {
            stable = true;
            for(int index = 0; index < method.code.ops.size(); ++index) {
                AbstractCodeOp op = method.code.ops.get(index);
                if(op instanceof CodeOpJump) {
                    if(forwardIfBranch(method, index,
                            ((CodeOpJump)op).gotoLabelRef.codeIndex,
                            false, possibility))
                        stable = false;
                } else if(op instanceof CodeOpBranch) {
                    CodeOpBranch branch = (CodeOpBranch)op;
                    if(branch.labelRefFalse != null &&
                            forwardIfBranch(method, index,
                                branch.labelRefFalse.codeIndex,
                                false, possibility))
                        stable = false;
                    if(branch.labelRefTrue != null &&
                            forwardIfBranch(method, index,
                                branch.labelRefTrue.codeIndex,
                                true, possibility))
                        stable = false;
                }
            }
            if(!stable) {
                totalStable = false;
                Optimizer.optimizeJumpChains(method.code);
                Optimizer.setLabelIndices(method);
                // it is a loop because label optimization may enable
                // new branch optimizations
                while(Optimizer.optimizeNullBranches(method.code)) {
                    Optimizer.optimizeLabels(method.code);
                    Optimizer.setLabelIndices(method);
                }
                if(arguments == null)
                    arguments = new HashMap<>();
                // no need for a complete trace
                possibility = traceValuesWithoutCasts(method, arguments, true);
            }
        } while(!stable);
        return !totalStable;
    }
    /**
     * Traces the transport of values by
     * local variables along a single branch. The tracing is terminated if
     * there are no new indices in transport trace or if it is known that the
     * indices won't be registered anyway.<br>
     *
     * @param transport                 transport data
     * @param ops                       list of operations of the
     *                                  method
     * @param index                     index of the operation where
     *                                  to begin this branch, 0 also
     *                                  symbolizes writes to arguments
     * @param variable                  the traced local variable, or null
     *                                  if it is only a search for dead
     *                                  code
     * @param trace                     transport trace
     */
    private static void traceBranchTransport(CodeVariableTransport transport,
            CodeMethod method, int index, CodeVariable variable,
            TraceTransport trace) {
        List<AbstractCodeOp> ops = method.code.ops;
        // local copy so that different branches do not interfere
        trace = new TraceTransport(trace);
        AbstractCodeDereference dereference = new CodeFieldDereference(variable);
        // either index since which there were no reads of the variable,
        // or <code>&gt; index</code>
        int noReadIndex = index;
        for(; index <= Math.min(ops.size() - 1, trace.region.last); ++index) {
            // add "to" double index to the trace
//if(variable.toString().indexOf("c33") != -1 &&
//        index == 58)
//    index = index;
            trace.transportTrace.add(index*2 + 0);
            if(trace.isVisitedAny(index)) {
                // not the first time at the index, so
                // check if this branch traces anything new
                boolean foundNew = false;
                // perhaps the transport data has some new indices
                //
                // if the visit is stopped, it means that
                // the transport is unimportant, as it would not be
                // registered anyway
                if(!trace.isStop(index))
                    for(int i : trace.transportTrace)
                        if(!trace.isVisited(index, i)) {
                            foundNew = true;
                            break;
                        }
                if(!foundNew) {
//                    if(transport.get(index*2 + 0).contains(dereference)) {
//                        transport = transport;
//                    } else {
//                        // mark that there won't be any reads before
//                        // this branch ends as a redundant one
//                        // for(int i = noReadIndex; i <= index; ++i)
//                        //    trace.setStop(i);
//                    }
                    // no reason to continue, end this branch
                    return;
                }
                // this index is entered with
                // with some new double indices in the trace
                // and it is not known if the trace will be registered
                // by a read
            }
            // mark all double indices as visited at the current index
            for(int i : trace.transportTrace)
                trace.addVisited(index, i);
            AbstractCodeOp op = ops.get(index);
            // check if the double indices in the transform trace
            // should be registered because
            // the current variable transports meaningful data
            if(op.isLocalSource(variable)) {
                // value read
                // save the transport and then clear it
                // the trace already contains "to" of the current op
                for(int i : trace.transportTrace)
                    transport.setTransported(i, dereference);
                trace.transportTrace.clear();
                noReadIndex = index + 1;
            }
            // extend the trace by "from" and save it to visited
            trace.transportTrace.add(index*2 + 1);
            trace.addVisited(index, index*2 + 1);
            boolean writtenTo = false;
            if(op instanceof CodeOpAssignment) {
                // assignment is a special case because it can be an assignment
                // to itself, which should be ignored as an operation that does
                // nothing
                CodeOpAssignment a = (CodeOpAssignment)op;
                if(a.getResult() instanceof CodeFieldDereference) {
                    CodeFieldDereference target = (CodeFieldDereference)
                            a.getResult();
                    if(target.isLocal() &&
                            target.variable == variable) {
                        boolean assignmentToItself = false;
                        if(a.rvalue.value instanceof CodeFieldDereference) {
                            CodeFieldDereference f = (CodeFieldDereference)a.rvalue.value;
                            if(f.isLocal() && f.variable == variable)
                                assignmentToItself = true;
                        }
                        if(!assignmentToItself)
                            writtenTo = true;
                    }
                }
            } else if(op instanceof CodeOpBranch) {
                // fork this branch into two other
                CodeOpBranch b = (CodeOpBranch)op;
                if(b.labelRefFalse != null) {
                    int indexFalse = b.labelRefFalse.codeIndex;
                    if(trace.region.within(indexFalse))
                        traceBranchTransport(transport, method, indexFalse,
                                variable, trace);
                }
                if(b.labelRefTrue != null) {
                    int indexTrue = b.labelRefTrue.codeIndex;
                    if(trace.region.within(indexTrue))
                        traceBranchTransport(transport, method, indexTrue,
                                variable, trace);
                }
                if(b.labelRefFalse != null &&
                        b.labelRefTrue != null) {
                    // the current branch ends
                    if((trace.isStop(b.labelRefFalse.codeIndex) || !trace.region.within(b.labelRefFalse.codeIndex)) &&
                            (trace.isStop(b.labelRefTrue.codeIndex) || !trace.region.within(b.labelRefTrue.codeIndex)))
                        // there are also no reads in both of the branch targets
                        // so mark that there won't be any reads
                        for(int i = noReadIndex; i <= index; ++i)
                            trace.setStop(i);
                    return;
                }
                if(b.labelRefFalse != null && !(trace.isStop(b.labelRefFalse.codeIndex) ||
                                    !trace.region.within(b.labelRefFalse.codeIndex)) ||
                        b.labelRefTrue != null && !(trace.isStop(b.labelRefTrue.codeIndex) ||
                                    !trace.region.within(b.labelRefTrue.codeIndex))) {
                    noReadIndex = index + 1;
                }
            } else if(op instanceof CodeOpJump) {
                // this branch changes into another one
                int targetIndex = ((CodeOpJump)op).gotoLabelRef.codeIndex;
                if(trace.region.within(targetIndex))
                    traceBranchTransport(transport, method, targetIndex,
                            variable, trace);
                if(trace.isStop(targetIndex) || !trace.region.within(targetIndex))
                    // there are also no reads in the jump target
                    // so mark that there won't be any reads
                    for(int i = noReadIndex; i <= index; ++i)
                        trace.setStop(i);
                return;
            } else if(op instanceof CodeOpReturn ||
                    op instanceof CodeOpThrow) {
                // this branch ends so
                // mark that there won't be any reads
                for(int i = noReadIndex; i <= index; ++i)
                    trace.setStop(i);
                return;
            } else {
                if(op.isLocalTarget(variable))
                    writtenTo = true;
            }
            if(index == 0 && method.writtenExternally(variable))
                // symbolizes writes of a caller's value to a local
                // that is an argument
                trace.transportTrace.add(-1);
            if(writtenTo) {
                // this operation deletes any previously transported
                // value by writing to the variable, mark it in
                // <code>visited</code>
                //
                // note that if this operation reads the variable,
                // this loop will do nothing
                for(int i = noReadIndex; i <= index; ++i)
                    trace.setStop(i);
                noReadIndex = index + 1;
                trace.transportTrace.clear();
                // extend the trace by "from" as the previous
                // "from" has just been cleared
                //
                // visited is stopped anyway so no need to register
                trace.transportTrace.add(index*2 + 1);
            }
        }
    }
    /**
     * Traces transport of values of local variables, taking into account
     * execution flow. Used to reduce the number of locals by
     * <code>reduceLocals()</code>.
     *
     * @param method                    method with the code to trace
     * @param onlyJavaType trace only non--primitive variables
     * @return                          transport of the method's locals
     */
    public static CodeVariableTransport traceTransport(CodeMethod method,
            boolean onlyJavaType) {
        CodeVariableTransport transport = new CodeVariableTransport();
//System.out.println("TRANSPORT START java = " + onlyJavaType);
        for(CodeVariable v : method.variables.values())
            if(!onlyJavaType || v.getResultType().isJava()) {
//if(v.name.equals("i#0verics.lang.AllocatingRecursive_AllocatingRecursive_D_#q13"))
//    v = v;
                TraceTransport.Region g = TraceTransport.getRegion(method, v);
                TraceTransport trace = new TraceTransport(g);
//if(g == null)
//    System.out.println("\t0%");
//else
//    System.out.println("\t" + (int)((g.last - g.first + 1)*100.5/method.code.ops.size()) + "%" +
//            v.name);
                if(g != null)
                    traceBranchTransport(transport, method, g.first, v,
                            trace);
            }
//System.out.println("TRANSPORT STOP java");
        return transport;
    }
    /**
     * <p>Traces transport of values of local variables, taking into account
     * execution flow. //, but treat allocations as asisgnments of unknown values.
     * Used to reduce the number of locals by <code>reduceLocals()</code>.</p>
     * 
     * <p>This is a convenience method.</p>
     *
     * @param method                    method with the code to trace
     * @return                          transport of the method's locals
     */
    public static CodeVariableTransport traceTransport(CodeMethod method) {
        return traceTransport(method, false);
    }
    /**
     * If there is an assignment to a boolean local, then the local is inverted,
     * and then is a branch condition, after which the local's transport end,
     * remove the inversion and invert the branch. The operations except
     * for the first can not be jump targets. Some neutral operations may intersperse.
     *
     * @param method                    method with the code to modify
     * @param transport                 transport
     * @return                          if anything has been changed in the
     *                                  code
     */
    public static boolean invertBranches(CodeMethod method,
            CodeVariableTransport transport) {
        // result of a start operation, but also a marker,
        // that within a sequence
        CodeVariable result = null;
        // result of the inverting expression
        CodeVariable invertResult = null;
        boolean negated = false;
        boolean stable = true;
        int indexAssignment = -1;
        int indexInversion = -1;
        Deque<Integer> restarts = new ArrayDeque<>();
        Set<Integer> alreadyStarted = new HashSet<>();
        // Iterator<AbstractCodeOp> iterator = method.code.ops.iterator();
        for(int index = 0; index < method.code.ops.size(); ++index) {
            boolean breakSequence = false;
/*if(index == 28)
    index = index;*/
            AbstractCodeOp op = method.code.ops.get(index);
            if(method.code.isJumpTarget(index)) {
                result = null;
                negated = false;
            }
            if(Optimizer.doesNothing(method.code.ops, op))
                /* empty */;
            else if(op instanceof CodeOpAssignment || op instanceof CodeOpCall ||
                        op instanceof CodeOpNone || op instanceof CodeOpBinaryExpression ||
                        (op instanceof CodeOpUnaryExpression &&
                            (!(((CodeOpUnaryExpression)op).sub.value instanceof CodeFieldDereference) ||
                            // the variable is never null, thus sequence start or possibly a neutral expression,
                            // but not the negation of <code>result</code>
                            ((CodeFieldDereference)((CodeOpUnaryExpression)op).sub.value).variable != result))) {
                AbstractResultCodeOp a = (AbstractResultCodeOp)op;
                Collection<CodeVariable> l;
                if(a.getResult() == null)
                    l = new LinkedList<>();
                else
                    l = a.getResult().extractPlainLocal();
                CodeVariable currResult;
                if(!l.isEmpty())
                    currResult = l.iterator().next();
                else
                    currResult = null;
                if(result != null &&
                        // is this a neutral operation within the current sequence?
                        !a.getLocalSources().contains(result) &&
                        !a.getLocalSources().contains(invertResult) &&
                        currResult != result && (currResult == null || currResult != invertResult)) {
                    // try to continue the current sequence
                    if(!alreadyStarted.contains(index) && currResult != null &&
                            currResult.getResultType().isBoolean())
                        // but also save the current operation as a possible start of
                        // another sequence
                        restarts.add(index);
                } else if(currResult != null && currResult.getResultType().isBoolean() &&
                        !alreadyStarted.contains(index)) {
                    // start a new sequence
                    result = currResult;
                    invertResult = null;
                    negated = false;
                    indexAssignment = index;
                    alreadyStarted.add(index);
                } else {
                    alreadyStarted.add(index);
                    breakSequence = true;
                }
            } else if(op instanceof CodeOpUnaryExpression &&
                    result != null) {
                CodeOpUnaryExpression u = (CodeOpUnaryExpression)op;
                if(u.op == CodeOpUnaryExpression.Op.UNARY_CONDITIONAL_NEGATION &&
                        u.sub.value instanceof CodeFieldDereference &&
                        ((CodeFieldDereference)u.sub.value).variable == result &&
                        u.getResult() instanceof CodeFieldDereference &&
                        ((CodeFieldDereference)u.getResult()).isLocal()) {
                    invertResult = ((CodeFieldDereference)u.getResult()).variable;
                    negated = true;
                    indexInversion = index;
                } else
                    breakSequence = true;
            } else if(op instanceof CodeOpBranch && result != null &&
                    negated && !transport.get(index*2 + 1).contains(
                        new CodeFieldDereference(invertResult))) {
                CodeOpBranch branch = (CodeOpBranch)op;
                if(!branch.condition.hasRestrictiveSourceLevelRange() &&
                        conditionLocal(branch) == invertResult) {
                    AbstractCodeDereference prevResult = method.code.ops.
                            get(indexAssignment).getResult();
                    if(!transport.get(index*2 + 1).contains(prevResult) &&
                            !prevResult.hasRestrictiveSourceLevelRange())
                        // prefer leaving the branch--related variable variable,
                        // as is is known to be not merged with other locals --
                        // see <code>Optimizer.optimizeDeadAndLocals</code>
                        // for details
                        method.code.ops.get(indexAssignment).setResult(
                                new CodeFieldDereference(invertResult));
                    else if(!invertResult.hasRestrictiveSourceLevelRange())
                        // remove the branch--related variable, as the other is
                        // used elsewhere
                        branch.condition.value = prevResult;
                    else
                        breakSequence = true;
                    if(!breakSequence) {
                        method.code.replace(indexInversion, new CodeOpNone(null),
                                // the operation will be removed anyway
                                false);
                        CodeLabel ref = branch.labelRefFalse;
                        branch.labelRefFalse = branch.labelRefTrue;
                        branch.labelRefTrue = ref;
                        breakSequence = true;
                        stable = false;
                    }
                }
            } else
                breakSequence = true;
            if(breakSequence) {
                result = null;
                invertResult = null;
                negated = false;
                int pendingIndex;
                do {
                    if(restarts.isEmpty()) {
                        pendingIndex = -1;
                        break;
                    }
                    pendingIndex = restarts.poll();
                } while(pendingIndex >= index + /* loop increment */1
                        /* discard such index -- the loop will go there anyway */);
                if(pendingIndex != -1)
                    index = pendingIndex - /* loop increment */1;
            }
        }
        if(!stable) {
            Optimizer.optimizeLabels(method.code);
            Optimizer.setLabelIndices(method);
        }
        return !stable;
    }
}
