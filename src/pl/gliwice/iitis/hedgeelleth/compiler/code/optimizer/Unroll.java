/*
 * Unroll.java
 *
 * Created on Sep 11, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.code.optimizer;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.code.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.op.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Literal;
import pl.gliwice.iitis.hedgeelleth.compiler.util.UniquePrefix;
import pl.gliwice.iitis.hedgeelleth.interpreter.exception.InterpreterException;
import pl.gliwice.iitis.hedgeelleth.interpreter.exception.InterpreterFatalException;

/**
 * Unrolling of loops.
 * 
 * @author Artur Rataj
 */
public class Unroll {
    /**
     * Maximum number of values, which the loop counter may
     * have for the loop to be unrolled.
     */
    final int MAX_VALUES;
    /**
     * A number of operations within the loop's body, multiplied by the number
     * of iterations, that can not be exceeded for the loop to be unrolled. The loop's
     * own condition, increment and jump operations do not count
     */
    final int MAX_BODY_UNROLLED;
    /**
     * If an explicit constant indexing inside the loop, using the loop's counter,
     * forces the loop to be unrolled, irrespective of <code>MAX_VALUES</code>
     * and <code>MAX_BODY</code>.
     */
    final boolean CONSTANT_INDEXING_FORCES;
    /**
     * If declarations of variables having ranges are apt for unrolling.
     */
    final boolean UNROLL_VARIABLE_RANGES;
    /**
     * If to optimize the unrolling. The optimizations are relatively fast and
     * effective.
     */
    final boolean OPTIMIZE;

    /**
     * Tests if the loop's body is apt for unrolling: no jumps from the outside
     * and no assignments to the loop counter.
     * 
     * @param method method with the loop
     * @param bodyBeg beginning of the loop's body
     * @param bodyEnd end of the loop's body
     * @param loopCounter the loop's counter
     * @param unrollVariableRanges if declarations of variables with ranges
     * are apt for unrolling
     * @return if apt for unrolling
     */
    protected boolean testBody(CodeMethod method, int bodyBeg, int bodyEnd,
            CodeVariable loopCounter) {
        // check if no jumps from the outside
        // of the loop to the loop body
        boolean inconsistentJumps = false;
        SortedSet<Integer> jumpsInto = new TreeSet<>();
        for(int i = bodyBeg; i <= bodyEnd; ++i)
            jumpsInto.addAll(
                method.code.getJumpSources(i));
        for(int sourceIndex : jumpsInto)
            if(sourceIndex < bodyBeg ||
                    sourceIndex > bodyEnd) {
                inconsistentJumps = true;
                break;
            }
        if(inconsistentJumps)
            return false;
        for(int i = bodyBeg; i <= bodyEnd; ++i) {
            AbstractCodeOp op = method.code.ops.get(i);
            if(op instanceof AbstractResultCodeOp) {
                AbstractCodeDereference result =
                        ((AbstractResultCodeOp)op).getResult();
                if(result instanceof CodeFieldDereference) {
                    CodeFieldDereference f = (CodeFieldDereference)result;
                    if(f.isLocal()) {
                        if(f.variable == loopCounter)
                            return false;
                        if(!UNROLL_VARIABLE_RANGES && f.variable.isVariablePRBound())
                            return false;
                    }
                }
            }
        }
        return true;
    }
    /**
     * Description of a constant indexing, already introduced by
     * <code>ConstantIndexing</code>, or alternately, one that
     * will likely need to be introduced because the indexing array
     * has Java--type elements.
     */
    public static class ConstantIndexingInfo {
        /**
         * A map of constant indexing. Keyed with the index of an operation replaced,
         * the value is the set of local variables removed from respective indexings.<br>
         * 
         * Constant indexing with the index being not a local is ignored.
         */
        public SortedMap<Integer, Set<CodeVariable>> insertions = new TreeMap<>();
        
        /**
         * Returnn information about indexing, using locals, of arrays, whose
         * elements are Java--type.
         * 
         * @param cm method to scan
         * @return where arrays with non--primitive elements are indexed, and with
         * which indices
         */
        public static ConstantIndexingInfo findJavaTypeElements(CodeMethod cm) {
            ConstantIndexingInfo ciInfo = new ConstantIndexingInfo();
            int index = 0;
            for(AbstractCodeOp op : cm.code.ops) {
                Set<CodeVariable> localIndices = new HashSet<>();
                Set<CodeIndexDereference> ii = op.getIndexDereferences();
                for(CodeIndexDereference i : ii)
                    if(i.object.type.getElementType(null, 0).isJava() &&
                            i.index.value instanceof CodeFieldDereference) {
                        CodeFieldDereference f = (CodeFieldDereference)i.index.value;
                        if(f.isLocal())
                            localIndices.add(f.variable);
                    }
                if(!localIndices.isEmpty())
                    ciInfo.insertions.put(index, localIndices);
                ++index;
            }
            return ciInfo;
        }
    };
    /**
     * Data of a loop to unroll.<br>
     * 
     * The loop's body excludes any of the loop's own instrumentation of the loop variable,
     * like the condition or the increment.
     */
    protected static class Loop {
        /**
         * Index of the beginning of the loop, including the loop variable manipulating
         * operations. It defines the beginning of the area to be replaced by the unrolled body.
         */
        public int loopBeg;
        /**
         * Index of the rnd of the loop, including the loop variable manipulating operations.
         * It defines the beginning of the area to be replaced by the unrolled body.
         */
        public int loopEnd;
        /**
         * Index of the first operation in the loop's body.
         */
        public int bodyBeg;
        /**
         * Index of the last operation in the loop's body.
         */
        public int bodyEnd;
        /**
         * The loop's counter, a local.
         */
        public CodeVariable counter;
        /**
         * Values of the counter, that should be sequentially feed to the loop's body.
         */
        List<Long> values;
        /**
         * Initial assignment of the counter variable.
         */
        CodeOpAssignment initial;
        /**
         * Step expression.
         */
        CodeOpBinaryExpression step;
        /**
         * Final value of the counter, i. e. the one rejected by the condition.
         */
        Long finalCounter;
        /**
         * If the branch taken, when the loop condition is not satisfied,
         * does not point directly after the loop, then points to the
         * index, where that branch points. Otherwise, null.
         */
        CodeLabel falseBranchNotAfter;
        
        /**
         * Instantiates a new <code>Loop</code>. The parameters are copied to the
         * respective fields of the same name.
         * 
         * @param loopBeg copied to a field
         * @param loopEnd copied to a field
         * @param bodyBeg copied to a field
         * @param bodyEnd copied to a field
         * @param counter copied to a field
         * @param values  copied to a field
         * @param initial  copied to a field
         * @param step  copied to a field
         * @param finalCounter copied to a field
         * @param falseBranchNotAfter copied to a field
         */
        public Loop(int loopBeg, int loopEnd, int bodyBeg, int bodyEnd,
                CodeVariable counter, List<Long> values,
                CodeOpAssignment initial, CodeOpBinaryExpression step,
                Long finalCounter, CodeLabel falseBranchNotAfter) {
            this.loopBeg = loopBeg;
            this.loopEnd = loopEnd;
            this.bodyBeg = bodyBeg;
            this.bodyEnd = bodyEnd;
            this.counter = counter;
            this.values = values;
            this.initial = initial;
            this.step = step;
            this.finalCounter = finalCounter;
            this.falseBranchNotAfter = falseBranchNotAfter;
        }
        @Override
        public String toString() {
            return counter.toNameString() + "(" + loopBeg + " " + bodyBeg +
                    "," + bodyEnd + " " + loopEnd + ")" + values.toString();
        }
    };
    /**
     * Creates a new instance of the loop unroller.
     * 
     * @param maxValues maximum number of values, which the loop counter may
     * have for the loop to be unrolled
     * @param maxBodyUnrolled a number of operations within the loop's
     * body, multiplied by the number of iterations, that can not be exceeded
     * for the loop to be unrolled; the loop's own condition, increment
     * and jump operations do not count
     * @param constantIndexingForces if an explicit constant indexing inside the loop,
     * using the loop's counter, forces the loop to be unrolled, irrespective of
     * <code>maxValues</code> and <code>maxBody</code>
     * @param unrollVariableRanges if declarations of variables with ranges
     * are apt for unrolling
     * @param optimize if to optimize the unrolling; the optimizations are relatively
     * fast and effective
     */
    public Unroll(int maxValues, int maxBodyUnrolled, boolean constantIndexingForces,
            boolean unrollVariableRanges, boolean optimize) {
        MAX_VALUES = maxValues;
        MAX_BODY_UNROLLED = maxBodyUnrolled;
        CONSTANT_INDEXING_FORCES = constantIndexingForces;
        UNROLL_VARIABLE_RANGES = unrollVariableRanges;
        OPTIMIZE = optimize;
    }
    /**
     * Unrolls loops in a method, given certain conditions specified
     * in the constructor.<br>
     * 
     * Label indices must be valid, and are valid after this method exits.
     * 
     * @param method method to optimize
     * @param ciInfo information about explicit constant indexing, null for
     * no information; kept up to date by this method
     */
    public boolean transform(CodeMethod method, ConstantIndexingInfo ciInfo) {
        boolean totalStable = true;
        boolean stable;
        int indexBeg = 0;
        do {
            stable = true;
            Loop loop = null;
            SCAN:
            for(int index = indexBeg; index < method.code.ops.size(); ++index) {
                AbstractCodeOp op = method.code.ops.get(index);
                if(op instanceof CodeOpAssignment) {
                    // do not test a possible loop's initial assignment twice
                    indexBeg = index + 1;
                    CodeOpAssignment a = (CodeOpAssignment)op;
                    if(a.getResult() instanceof CodeFieldDereference &&
                            a.rvalue.range == null && a.rvalue.value instanceof CodeConstant) {
                        CodeFieldDereference d = (CodeFieldDereference)a.getResult();
                        if(d.isLocal() && d.getResultType().isOfIntegerTypes()) {
                            long loopInitial = ((CodeConstant)a.rvalue.value).value.getMaxPrecisionInteger();
                            CodeVariable loopCounter = d.variable;
                            // search for a for-- or while--like pattern
if(index + 1 >= method.code.ops.size())
    index = index;
                            AbstractCodeOp op2 = method.code.ops.get(index + 1);
                            if(op2 instanceof CodeOpBinaryExpression) {
                                CodeOpBinaryExpression loopCondition = (CodeOpBinaryExpression)op2;
                                if(loopCondition.left.range == null &&
                                        loopCondition.right.range == null &&
                                        (loopCondition.op ==
                                                CodeOpBinaryExpression.Op.BINARY_LESS ||
                                         loopCondition.op ==
                                                CodeOpBinaryExpression.Op.BINARY_LESS_OR_EQUAL)) {
                                    Long limit = null;
                                    boolean limitEqual;
                                    boolean limitLower = false;
                                    if(loopCondition.left.value.equals(d)) {
                                        if(loopCondition.right.value instanceof CodeConstant) {
                                            limit = ((CodeConstant)loopCondition.right.value).value.
                                                    getMaxPrecisionInteger();
                                            limitLower = true;
                                        }
                                    } else if(loopCondition.right.value.equals(d)) {
                                        if(loopCondition.left.value instanceof CodeConstant) {
                                            limit = ((CodeConstant)loopCondition.left.value).value.
                                                    getMaxPrecisionInteger();
                                            limitLower = false;
                                        }
                                    }
                                    if(limit != null) {
                                        limitEqual = loopCondition.op ==
                                                CodeOpBinaryExpression.Op.BINARY_LESS_OR_EQUAL;
                                        long limitMin;
                                        long limitMax;
                                        if(limitLower) {
                                            limitMin = loopInitial;
                                            if(limitEqual)
                                                limitMax = limit;
                                            else
                                                limitMax = limit - 1;
                                        } else {
                                            if(limitEqual)
                                                limitMin = limit;
                                            else
                                                limitMin = limit + 1;
                                            limitMax = loopInitial;
                                        }
                                        if(limitMin <= limitMax || true) {
                                            AbstractCodeOp op3 = method.code.ops.get(index + 2);
                                            if(op3 instanceof CodeOpBranch &&
                                                    !method.code.isJumpTarget(index + 2)) {
                                                CodeOpBranch loopBranch = (CodeOpBranch)op3;
                                                if(loopBranch.labelRefTrue == null &&
                                                        loopBranch.labelRefFalse != null) {
                                                    int loopExit = loopBranch.labelRefFalse.codeIndex;
                                                    CodeLabel falseBranchNotAfter;
                                                    if(loopExit <= index) {
                                                        falseBranchNotAfter = new CodeLabel(
                                                                loopBranch.labelRefFalse);
                                                        loopExit = checkForForwardedExit(method.code, index, loopExit);
                                                    } else
                                                        falseBranchNotAfter = null;
                                                    boolean ignoreLimits = false;
                                                    if(CONSTANT_INDEXING_FORCES && ciInfo != null) {
                                                        for(int i = index + 3; i < loopExit - 1; ++i) {
                                                            Set<CodeVariable> set = ciInfo.insertions.get(i);
                                                            if(set != null && set.contains(loopCounter)) {
                                                                ignoreLimits = true;
                                                                break;
                                                            }
                                                        }
                                                    }
                                                    // check if the loop has a non--empty body
                                                    if(loopExit >= index + 4 + 1 &&
                                                            (ignoreLimits ||
                                                                (loopExit - (index + 4))*(limitMax + 1 - limitMin) <= 
                                                                    MAX_BODY_UNROLLED)) {
                                                        AbstractCodeOp op4 = method.code.ops.get(loopExit - 1);
                                                        if(op4 instanceof CodeOpJump &&
                                                                !method.code.isJumpTarget(loopExit - 1)) {
                                                            CodeOpJump loopJump = (CodeOpJump)op4;
                                                            if(loopJump.gotoLabelRef.codeIndex == index  + 1 &&
                                                                    method.code.getJumpSources(index + 1).size() == 1) {
                                                                AbstractCodeOp op5 = method.code.ops.get(loopExit - 2);
                                                                if(op5 instanceof CodeOpBinaryExpression) {
                                                                    CodeOpBinaryExpression loopStep = (CodeOpBinaryExpression)op5;
                                                                    if(loopStep.getResult().equals(d) &&
                                                                            loopStep.left.range == null &&
                                                                            loopStep.right.range == null &&
                                                                            (loopStep.left.value.equals(d) ||
                                                                             loopStep.right.value.equals(d)) &&
                                                                            (loopStep.op ==
                                                                                CodeOpBinaryExpression.Op.BINARY_PLUS ||
                                                                             loopStep.op ==
                                                                                CodeOpBinaryExpression.Op.BINARY_MINUS)) {
                                                                        Long step = null;
                                                                        if(loopStep.right.value instanceof CodeConstant) {
                                                                            step = ((CodeConstant)loopStep.right.value).value.
                                                                                    getMaxPrecisionInteger();
                                                                            if(loopStep.op ==
                                                                                CodeOpBinaryExpression.Op.BINARY_MINUS)
                                                                                step *= -1;
                                                                        } else if(loopStep.left.value instanceof CodeConstant &&
                                                                                loopStep.op ==
                                                                                    CodeOpBinaryExpression.Op.BINARY_PLUS) {
                                                                            step = ((CodeConstant)loopStep.left.value).value.
                                                                                    getMaxPrecisionInteger();
                                                                        }
                                                                        if(step != null &&
                                                                                (loopInitial == limitMin && step > 0 ||
                                                                                 loopInitial == limitMax && step < 0) &&
                                                                                (ignoreLimits ||
                                                                                    limitMax - limitMin + 1 <= MAX_VALUES)) {
                                                                            List<Long> values = new LinkedList<>();
                                                                            // assigns a meaningless value
                                                                            long finalCounter = Long.MIN_VALUE;
                                                                            if(limitMin > limitMax)
                                                                                finalCounter = loopInitial;
                                                                            else if(step > 0) {
                                                                                    for(long i = limitMin; i <= limitMax; i += step) {
                                                                                        values.add(i);
                                                                                        finalCounter = i;
                                                                                    }
                                                                                } else {
                                                                                    for(long i = limitMax; i >= limitMin; i += step) {
                                                                                        values.add(i);
                                                                                        finalCounter = i;
                                                                                    }
                                                                                }
                                                                            finalCounter += step;
                                                                            int bodyBeg = index  + 3;
                                                                            int bodyEnd = loopExit - 3;
                                                                            if(testBody(method, bodyBeg, bodyEnd,
                                                                                    loopCounter)) {
                                                                                loop = new Loop(index, loopExit - 1,
                                                                                        bodyBeg, bodyEnd,
                                                                                        loopCounter, values,
                                                                                        a, loopStep, finalCounter,
                                                                                        falseBranchNotAfter);
// if(loopExit - 2 == 156)
//     step = step;
                                                                                stable = false;
                                                                                break SCAN;
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if(!stable) {
                List<AbstractCodeOp> unroll = new LinkedList<>();
                SortedMap<Integer,Set<CodeVariable>> newInsertions = new TreeMap<>();
                // initial assignment of the counter + body size + optional label holder
                int unrollSize = 1 + (loop.bodyEnd - loop.bodyBeg + 1) +
                    (loop.step.label != null ? 1 : 0);
                for(long v : loop.values) {
                    String labelPrefix = UniquePrefix.get(
                            "unroll_" + loop.counter.name + "_" + loop.loopBeg + "_" +
                            v + "_");
//if(labelPrefix.equals("unroll_i#0mirela.AbstractNode_produce__i_96_0_")) {
//    CodeLabel.clearIndices_(method.code);
//    v = v;
//    CodeLabel.setIndices(method.code);
//}
                    CodeOpAssignment a = new CodeOpAssignment(
                            method.code.ops.get(loop.bodyBeg).getStreamPos());
                    a.result = new CodeFieldDereference(loop.counter);
                    CodeConstant indexValue = new CodeConstant(
                            a.getStreamPos(), new Literal(v));
                    a.rvalue = new RangedCodeValue(indexValue);
                    unroll.add(a);
                    CodeOpNone labelHolder;
                    if(loop.step.label != null) {
                        // inherit the label
                        labelHolder = new CodeOpNone(loop.step.getStreamPos());
                        labelHolder.label = new CodeLabel(labelPrefix + loop.step.label.name);
                    } else
                        labelHolder = null;
                    AbstractCodeOp nextOp = null;
                    for(int i = loop.bodyBeg; i <= loop.bodyEnd; ++i) {
                        AbstractCodeOp op;
                        if(nextOp == null)
                            op = method.code.ops.get(i).clone();
                        else {
                            op = nextOp;
                            nextOp = null;
                        }
                        if(op.label != null)
                            op.label.name = labelPrefix + op.label.name;
                        for(CodeLabel ref : op.getLabelRefs())
                            // jumps from the body to the outside of the loop
                            // are allowable, do not rename the respective
                            // label references, unless the jump is just
                            // after the body, where an appropriate label
                            // holder, attached to the unrolled body, would
                            // exist in such a case
                            if(ref.codeIndex >= loop.bodyBeg &&
                                    ref.codeIndex <= loop.bodyEnd
                                    + (labelHolder != null ? 1 : 0))
                                ref.name = labelPrefix + ref.name;
                        if(OPTIMIZE && op.getLocalSources().contains(loop.counter)) {
                            // optimize branches, taking advantage
                            // of the knowledge, that the index variable
                            // has a constant value within the unrolled
                            // loop's body
                            op.replaceLocalSourceVariable(loop.counter,
                                    indexValue);
                            boolean replaced;
                            try {
                                AbstractCodeOp replacement = CodeStaticAnalysis.
                                        staticAnalysis(null, op);
                                replaced = replacement != op;
                                op = replacement;
                            } catch(InterpreterFatalException | InterpreterException e) {
                                /* some unknown variable */
                                replaced = false;
                            }
                            int nextIndex = i + 1;
                            if(replaced && op.getResult() instanceof CodeFieldDereference &&
                                    ((CodeFieldDereference)op.getResult()).isLocal() &&
                                    nextIndex <= loop.bodyEnd) {
                                CodeVariable lvalue = ((CodeFieldDereference)op.getResult()).
                                        variable;
                                CodeConstant rvalue = (CodeConstant)
                                        ((CodeOpAssignment)op).rvalue.value;
                                nextOp = method.code.ops.get(nextIndex).
                                        clone();
                                if(nextOp instanceof CodeOpBranch &&
                                        !method.code.isJumpTarget(nextIndex)) {
                                    CodeOpBranch b = (CodeOpBranch)nextOp;
                                    if(b.condition.value instanceof CodeFieldDereference) {
                                        CodeFieldDereference c = (CodeFieldDereference)
                                                b.condition.value;
                                        if(c.isLocal() && c.variable == lvalue) {
                                            b.condition.value = rvalue;
                                        }
                                    }
                                }
                            }
                        }
                        unroll.add(op);
                        if(ciInfo != null) {
                            Set<CodeVariable> set = ciInfo.insertions.get(i);
                            if(set != null) {
                                // split the insertion into the unrolled loops
                                ciInfo.insertions.remove(i);
                                for(int j = 0; j < loop.values.size(); ++j)
                                    newInsertions.put(
                                            // subtract loop prefix and add the initial assignment
                                            (i - 3 + 1) +
                                            j*unrollSize,
                                            set);
                            }
                        }
                    }
                    if(labelHolder != null)
                        unroll.add(labelHolder);
                }
                CodeOpAssignment a = new CodeOpAssignment(
                        method.code.ops.get(loop.bodyEnd).getStreamPos());
                a.result = new CodeFieldDereference(loop.counter);
                a.rvalue = new RangedCodeValue(new CodeConstant(
                        a.getStreamPos(), new Literal(loop.finalCounter)));
                unroll.add(a);
                // inherit the label
                unroll.get(0).label = loop.initial.label;
                int offset =
                        // size of the unrolled loop, including the final assignment
                        (loop.values.size()*unrollSize + 1) -
                        // size of the initial loop
                        (loop.loopEnd - loop.loopBeg + 1);
                if(ciInfo != null) {
                    for(int i = method.code.ops.size() - 1; i > loop.bodyEnd; --i) {
                        Set<CodeVariable> set = ciInfo.insertions.get(i);
                        if(set != null) {
                            // split the insertions to the place in the method
                            // with the loop unrolled
                            ciInfo.insertions.remove(i);
                            ciInfo.insertions.put(i + offset, set);
                        }
                    }
                    ciInfo.insertions.putAll(newInsertions);
                }
                for(int i = loop.loopBeg; i <= loop.loopEnd +
                        // if to reuse the final goto
                        (loop.falseBranchNotAfter == null ? 0 : -1); ++i)
                    method.code.ops.remove(loop.loopBeg);
                if(loop.falseBranchNotAfter != null)
                    ((CodeOpJump)method.code.ops.get(loop.loopBeg)).gotoLabelRef =
                            loop.falseBranchNotAfter;
                int counter = loop.loopBeg;
                for(AbstractCodeOp op : unroll) {
                    method.code.ops.add(counter, op);
                    ++counter;
                }
                Optimizer.setLabelIndices(method);
                totalStable = false;
            }
        } while(!stable);
        if(!totalStable && OPTIMIZE) {
            Optimizer.optimizeStaticBranches(method.code);
            Optimizer.setLabelIndices(method);
        }
        return !totalStable;
    }
    /**
     * If a loop is before a backward jump, the exit can be forwarded. In
     * effect, <code>loopExit</code> does not really point after the last
     * operation of the loop. This method attempt to correct that. If unsuccesfull,
     * <code>loopExit</code> is returned.
     * 
     * @param code the method's code
     * @param index index of the loop's condition
     * @param loopExit a loop exit, forwarded before the condition
     * @return an index after the last operation of the hypothesized loop, or
     * <code>loopExit</code>
     */
    private int checkForForwardedExit(Code code, int index, int loopExit) {
        int afterLoop = loopExit;
        int condition = index + 1;
        int branch = index + 2;
        for(int i = branch + 1; i < code.ops.size(); ++i) {
            AbstractCodeOp op = code.ops.get(i);
            if(op instanceof CodeOpJump) {
                CodeOpJump j = (CodeOpJump)op;
                int t = j.gotoLabelRef.codeIndex;
                if(t == condition) {
                    afterLoop = i + 1;
                    break;
                }
            }
        }
        return afterLoop;
    }
}
