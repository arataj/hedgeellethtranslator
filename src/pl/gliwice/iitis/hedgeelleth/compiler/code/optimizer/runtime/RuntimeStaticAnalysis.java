/*
 * TADDStaticAnalysis.java
 *
 * Created on Sep 16, 2009, 1:25:05 PM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.code.optimizer.runtime;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.code.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.op.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.optimizer.CodeStaticAnalysis;
import pl.gliwice.iitis.hedgeelleth.compiler.code.optimizer.CodeValuePossibility;
import pl.gliwice.iitis.hedgeelleth.compiler.code.optimizer.Optimizer;
import pl.gliwice.iitis.hedgeelleth.compiler.code.optimizer.TraceValues;
import pl.gliwice.iitis.hedgeelleth.compiler.primitiverange.CodeVariablePrimitiveRange;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Context;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Literal;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Type;
import pl.gliwice.iitis.hedgeelleth.compiler.util.CompilerUtils;
import pl.gliwice.iitis.hedgeelleth.interpreter.AbstractInterpreter;
import pl.gliwice.iitis.hedgeelleth.interpreter.InterpretingContext;
import pl.gliwice.iitis.hedgeelleth.interpreter.RuntimeValue;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.*;
import pl.gliwice.iitis.hedgeelleth.interpreter.exception.InterpreterException;

/**
 * Static analysis of code after some runtime values are known.<br>
 *
 * This object needs all code <i>c</i> that will possibly be interpreted or
 * translated in the future. If some of such code would be missing, then
 * some runtime fields might mistakenly be classified as constants, and
 * in effect an invalid optimization might occur.<br>
 *
 * Finds runtime field variables that are read--only within <i>c</i>, then
 * replaces all references to these fields with constants in <i>c</i>.
 * Then performs normal value propagation in <i>c</i>.<br>
 *
 * This object assumes that all Java--type fields are constant in <i>c</i>.
 * If they are not, this analysis returns an error.<br>
 *
 * The interpreter exception is thrown if some value is found outside
 * its range.<br>
 *
 * For handling of ambiguous or unknown dereferences, see the docs
 * of <code>getRuntimeFields</code>. Clearly, ambiguous dereferences can not
 * be replaced by constants.
 *
 * // As indexing dereferences are not replaced by constants in this class, no
 * // need to register the replacements using <code>ArrayRangeRegister<code>.<br>
 *
 * Array class name must already be translated to
 * <code>Type.ARRAY_QUALIFIED_CLASS_NAME</code>.<br>
 *
 * @author Artur Rataj
 */
public class RuntimeStaticAnalysis {
    /**
     * Interpreter.
     */
    protected AbstractInterpreter interpreter;
    /**
     * List of all possibly executed or translated methods in the future.
     */
    protected List<RuntimeMethod> methods;
    /**
     * Trace, for determining to which runtime field a static dereference
     * points. The map is build and updated during the analysis.
     */
    protected Map<RuntimeMethod, CodeValuePossibility> trace;
    /**
     * Replaced fields.
     */
    protected Map<RuntimeField, Literal> replaced;
    /**
     * If anything was change in the code within this analysis.
     */
    protected boolean changed;

    /**
     * <p>Creates a new instance of RuntimeStaticAnalysis. The methods are analyzed,
     * code is transformed, and the map <code>trace</code> is build, to be used
     * later by <code>getRuntimeField()</code>.</p>
     * 
     * <p>Calls <code>clearChanged()</code></p> before the analysis.
     * 
     * @param interpreter               interpreter, needed only for evaluating
     *                                  possible primitive ranges or method's
     *                                  arguments by <code>constructArgs</code>;
     *                                  see that method for details
     * @param methods                   list of all possibly executed or
     *                                  translated methods in the future
     */
    public RuntimeStaticAnalysis(AbstractInterpreter interpreter,
            List<RuntimeMethod> methods) throws InterpreterException {
        this.interpreter = interpreter;
        this.methods = methods;
        trace = new HashMap<>();
        replaced = new HashMap<>();
        clearChanged();
        for(RuntimeMethod rm : methods)
            analyse(rm);
    }
    /**
     * Analyses only a single method, refreshing its trace.
     * 
     * @param rm
     * @throws InterpreterException 
     */
    public final void analyse(RuntimeMethod rm) throws InterpreterException {
        trace.remove(rm);
        boolean localChanged = false;
        boolean stable = true;
        boolean firstRun = true;
        do {
            if(localChanged)
                stable = false;
            do {
                if(localChanged)
                    stable = false;
                if(firstRun) {
                    // make all traces, because <code>replaceConstantPrimitiveFields</code>
                    // requires that
                    for(RuntimeMethod rm2 : methods)
                        traceRuntimeValues(rm2);
                    firstRun = false;
                } else
                    // only update the trace of the modified method, the other traces
                    // are up to date
                    traceRuntimeValues(rm);
                // iteratively, as placing constants might make more fields
                // traceable
            } while(localChanged = replaceConstantFields(rm /*, arr*/));
            if(optimize(rm, true))
                stable = false;
        // iteratively, as replacing expressions with constants might
        // make more fields traceable
        //
        // if returns true, trace map is updated later in this loop;
        // if returns false, trace map is updated after this loop
        } while(localChanged = runtimeStaticAnalysis(rm));
        // current trace of <code>rm.method</code> might still be needed by
        // <code>replaceConstantPrimitiveFields</code> or after this constructor
        // finishes
        traceRuntimeValues(rm);
        if(!stable)
            this.changed = true;
    }
    /**
     * If anything has been changed in the code within this analysis.
     * 
     * @return is the code has been modified
     */
    public void clearChanged() {
        changed = false;
    }
    /**
     * If anything was changes in the code since the last
     * <code>clearChanged()</code>.
     * 
     * @return is the code has been modified
     */
    public boolean isChanged() {
        return changed;
    }
    /**
     * Constructs arguments of a method, to be passed to
     * <code>TraceValues.traceValues</code>.
     *
     * @param interpreter               interpreter, can be null if not
     *                                  required to evaluate primitive
     *                                  bounds; used by
     *                                  <code>AbstractRuntimeContainer.getValue</code>;
     *                                  see that method for details
     * @param rm                        runtime method, should contain
     *                                  only values of arguments as if the
     *                                  method's code would not be executed yet
     * @return                          list of arguments
     */
    protected Map<CodeVariable, AbstractCodeValue> constructArgs(AbstractInterpreter interpreter,
            RuntimeMethod rm) {
        Map<CodeVariable, AbstractCodeValue> args = new HashMap<>();
         for(CodeVariable cv : rm.getVariables()) {
             RuntimeValue rv = rm.getValue(interpreter, rm, cv);
             args.put(cv, new CodeConstant(null, rv));
         }
         return args;
    }
    /**
     * Traces runtime values in a method.<br>
     *
     * The runtime should contain values of arguments of the method,
     * and no values of the method's locals. That is, it should be in such
     * a state as if the method would be about to be called.<br>
     *
     * If a runtime value of some <code>object</code> within field
     * dereference <i>d</i> was ambiguous or could not be traced by
     * <code>TraceValues.traceValues</code>, no runtime field
     * is assigned to <i>d</i>.<br>
     *
     * The traced values update the map <code>trace</code>.
     *
     * @param rm                        method to trace
     */
    public void traceRuntimeValues(RuntimeMethod rm) {
        Map<CodeVariable, AbstractCodeValue> args =
                constructArgs(interpreter, rm);
// if(rm.method.toString().indexOf("Scheduler") != -1)
//     rm = rm;
         trace.put(rm, TraceValues.traceValuesWithCasts(rm.method, args,
                 new TraceValues.Options(interpreter, false)));
    }
    /**
     * <p>Checks, if an actual runtime value of an object can be cast to the
     * variable's type. If not, returns null.</p>
     * 
     * <p>Class cast errors are ignored by this class, as in a real process such an
     * error might possibly be prevented by the flow of control.</p>
     * 
     * @param object runtime value
     * @param field field variable dereference
     * @return a runtime field or null
     */
    private RuntimeField checkCast(RuntimeValue object, CodeFieldDereference dereference) {
        CodeVariable field = dereference.variable;
        //CodeClass fieldOwner = interpreter.getCodeClass(
        //        dereference.getContainerVariable().getResultType());
        CodeClass fieldOwner = (CodeClass)field.owner;
        CodeClass objectClass;
        if(object.getReferencedObject() == null)
            // unknown type
            objectClass = null;
        else
            objectClass = object.getReferencedObject().codeClass;
        if(objectClass == null || !objectClass.isEqualToOrExtending(fieldOwner))
            return null;
        else
            return new RuntimeField(object, field);
    }
    /**
     * Extracts runtime field on basis of a field dereference in a method.<br>
     *
     * Returns a field only if it could be traced by
     * <code>traceRuntimeValues</code>.<br>
     *
     * Static field do not require any tracing, and thus can always be resolved
     * by this method.<br>
     *
     * References can not have the unknown value represented by
     * <code>CodeValuePossibility.UNKNOWN_VALUE</code> or an
     * uninitialized value represented by null --  in these cases
     * a runtime exception is thrown. Ambiguous dereferences cause that
     * the return set contain several values.<br>
     *
     * If the dereference contains a local, then the local is ignored.
     *
     * @param runtimeMethod                 method
     * @param index                         index of the operation that
     *                                      contains the field dereferences
     * @param dereference                   dereference to estimate
     * @return                              runtime fields or empty list
     *                                      if no runtime field could be found
     */
    public Set<RuntimeField> getRuntimeFields(RuntimeMethod rm,
            int index, CodeFieldDereference dereference)
            throws InterpreterException {
        Set<RuntimeField> rf = new HashSet<>();
        CodeVariable object = dereference.object;
        if(object != null) {
            // non--static field
            Set<AbstractCodeValue> objectValues = new HashSet<>();
            if(object.context != Context.NON_STATIC) {
                // referenced by a static field
                //
                // fields are not traced, get value from the interpreter
                try {
                    objectValues.add(new CodeConstant(null,
                            interpreter.getValue(new RangedCodeValue(
                            new CodeFieldDereference(object)), rm)));
                } catch(InterpreterException e) {
                    throw new RuntimeException("unexpected interpreter exception: " +
                            e.toString());
                }
            } else {
                 // referenced by a local
                 //
                 // get from trace
                 Set<AbstractCodeValue> values = trace.get(rm).getValuesM1(
                    index - 1, new CodeFieldDereference(object));
                 for(AbstractCodeValue v : values)
                     objectValues.add(v);
            }
            for(AbstractCodeValue v : objectValues) {
                if(v == CodeValuePossibility.UNKNOWN_VALUE)
                    // ignore, might be dead code
                    ;
                else if(v instanceof CodeFieldDereference) {
                    // not traced down to the constant, perhaps
                    // <code>replaceConstantPrimitiveFields</code>
                    // will make it possible later
                    /* do nothing */
                } else if(v instanceof CodeConstant) {
                    Literal c = ((CodeConstant)v).value;
                    if(!(c instanceof RuntimeValue)) {
                        if(!c.isNull())
                            throw new RuntimeException("unknown constant");
                    } else {
                        RuntimeValue rv = (RuntimeValue)c;
                        if(rv.getReferencedObject() != null) {
    //                        throw new InterpreterException(
    //                                rm.method.code.ops.get(index).getStreamPos(),
    //                                "null pointer dereference");
                            RuntimeField f = checkCast(rv, dereference);
                            if(f != null)
                                rf.add(f);
                        }
                    }
                } else if(v instanceof CodeIndexDereference) {
                    CodeIndexDereference i = (CodeIndexDereference)v;
                    if(i.index.value instanceof CodeConstant) {
                        int elementIndex = CompilerUtils.toInt(
                                ((CodeConstant)i.index.value).value.
                                    getMaxPrecisionInteger());
                        AbstractCodeValue array = getConstant(
                                rm, index, new CodeFieldDereference(i.object));
                        // AbstractCodeValue array = trace.get(rm).getSingle(index - 1,
                        //         new CodeFieldDereference(i.object));
                        if(array instanceof CodeConstant) {
                            ArrayStore store = (ArrayStore)((RuntimeValue)
                                ((CodeConstant)array).value).value;
                            if(store == null)
                                throw new InterpreterException(
                                        rm.method.code.ops.get(index).getStreamPos(),
                                        "null array dereference");
                            RuntimeValue element = store.getElement(
                                    interpreter, rm, null, elementIndex);
                            RuntimeField f = checkCast(element, dereference);
                            if(f != null)
                                rf.add(f);
                        }
                    }
                } else
                    throw new RuntimeException("unknown value");
            }
        } else {
            if(dereference.variable.context != Context.NON_STATIC)
                // static
                rf.add(new RuntimeField(null, dereference.variable));
        }
        return rf;
    }
    /**
     * It is a convenience method, that calls <code>getRuntimeFields</code>
     * and returns non--null only if exactly one runtime field has been found.<br>
     *
     * If the dereference contains a local, then null lis returned.<br>
     *
     * For details, see the docs of the called method.
     *
     * @param runtimeMethod                 method
     * @param index                         index of the operation that
     *                                      contains the field dereferences
     * @param dereference                   dereference to estimate
     * @return                              runtime field, or null if the field
     *                                      could not be determined or more than
     *                                      one runtime field has been found
     */
    public RuntimeField getSingleRuntimeField(RuntimeMethod rm,
            int index, CodeFieldDereference dereference) throws InterpreterException {
        Set<RuntimeField> fields;
        try {
            fields = getRuntimeFields(rm, index, dereference);
        } catch(InterpreterException e) {
            // might be dead code, ignore
            return null;
        }
        if(fields.size() != 1)
            return null;
        else
            return fields.iterator().next();
    }
    /**
     * It works like <code>getRuntimeField<code>, but for a collection
     * instead of a single value.
     *
     * @param runtimeMethod                 method
     * @param index                         index of the operation that
     *                                      contains the field dereferences
     * @param dereferences                  dereferences to estimate
     * @return                              list of runtime fields, empty list
     *                                      for none
     */
    protected Set<RuntimeField> getSingleRuntimeField(RuntimeMethod rm,
            int index, Collection<CodeFieldDereference> dereferences) throws InterpreterException {
        Set<RuntimeField> fields = new HashSet<>();
        for(CodeFieldDereference f : dereferences) {
            RuntimeField rf = getSingleRuntimeField(rm, index, f);
            if(rf != null)
                fields.add(rf);
        }
        return fields;
    }
    /**
     * Returns the analyzed methods.
     *
     * @return                          a collection of methods
     */
    public List<RuntimeMethod> getMethods() {
        return methods;
    }

    /**
     * Returns the trace of given method. The method must exist
     * in <code>getMethods()</code>.
     *
     * @param rm                        runtime method
     * @return                          trace of the runtime method
     */
    public CodeValuePossibility getTrace(RuntimeMethod rm) {
        return trace.get(rm);
    }
    /**
     * Determines which runtime fields are constants after
     * the main thread finishes.<br>
     *
     * This method refrains from stating that a non--primitive
     * runtime field is constant if there are any non--resolved
     * target dereferences left to the respective code field. Thus, as
     * dereferences are iteratively resolved, new constant
     * primitive runtime fields might be returned. Java
     * type fields are assumed to be constant, thus any runtime
     * counterpart found of such a field is immediately returned.<br>
     *
     * This method requires all traces to be current.
     *
     * @return                          set with runtime fields
     */
    protected Set<RuntimeField> findConstantFields() throws InterpreterException {
        Set<RuntimeField> constants = new HashSet<>();
        Set<RuntimeField> sources = new HashSet<>();
        Set<RuntimeField> runtimeTargets = new HashSet<>();
        Set<CodeVariable> unresolvedPrimitiveCodeTargets = new HashSet<>();
        for(RuntimeMethod rm : methods) {
            int index = 0;
            for(AbstractCodeOp op : rm.method.code.ops) {
                Collection<CodeFieldDereference> s = op.getFieldSources();
                Collection<CodeFieldDereference> t = op.getFieldTargets();
                try {
                    sources.addAll(getSingleRuntimeField(rm, index, s));
                } catch(InterpreterException e) {
                    // could be dead code, ignore
                }
                for(CodeFieldDereference f : t) {
                    if(interpreter.unknownConstants.contains(f.variable))
                        throw new RuntimeException("unknown constant written to, " +
                                "but expected to be final");
                    if(f.getResultType().isJava())
                        throw new InterpreterException(op.getStreamPos(),
                                "write to non--primitive field illegal in this thread");
                    RuntimeField rf = getSingleRuntimeField(rm, index, f);
                    if(rf != null)
                        runtimeTargets.add(rf);
                    else if(f.getResultType().isPrimitive())
                        unresolvedPrimitiveCodeTargets.add(f.variable);
                }
                ++index;
            }
        }
        for(RuntimeField rf : sources) {
            if(!runtimeTargets.contains(rf) &&
                    !unresolvedPrimitiveCodeTargets.contains(rf.field) &&
                    !interpreter.unknownConstants.contains(rf.field))
                constants.add(rf);
        }
        return constants;
    }
    /**
     * Finds value of a runtime fields. It is assumed that the
     * field is already initialized, otherwise an interpreter
     * exception is thrown.
     *
     * @param field                     runtime field to evaluate
     * @return                          value of the field
     */
    public CodeConstant findValueOfRuntimeField(RuntimeField runtimeField)
            throws InterpreterException {
        RuntimeValue v;
if(runtimeField == null /*|| runtimeField.toString().endsWith("sync")*/)
    v = null;
        if(runtimeField.object != null) {
            // non--static field
            //
            // not using interpreter's <code>getValue</code>, because the
            // container is known and not the runtime
            RuntimeObject o = runtimeField.object.getReferencedObject();
            if(o == null)
                throw new InterpreterException(runtimeField.field.getStreamPos(),
                        "field accessed through uninitialized reference");
            // no interpreter as no runtime is known
            v = o.getValue(null, null, runtimeField.field);
        } else {
            // static field
            v = interpreter.getValueUnchecked(new RangedCodeValue(
                    new CodeFieldDereference(runtimeField.field)), null);
        }
        if(v == null)
            throw new RuntimeException("uninitialized field");
        return new CodeConstant(null, v);
    }
    /**
     * Finds values of runtime fields. It is assumed that all
     * of the fields are already initialized, otherwise an intepreter
     * exception is thrown. This method calls
     * <code>findValueOfRuntimeField</code> for each field.
     *
     * @param fields                    fields to evaluate
     * @return                          values of the fields
     */
    protected Map<RuntimeField, CodeConstant> findValuesOfRuntimeFields(
            Set<RuntimeField> fields) throws InterpreterException {
        Map<RuntimeField, CodeConstant> values = new HashMap<>();
        for(RuntimeField rf : fields)
            values.put(rf, findValueOfRuntimeField(rf));
        return values;
    }
    /*
     * Checks if a variable primitive range of a field is evaluated as
     * constants. If not, evaluates the range as constants. It can be done
     * as the primitive range variables of fields are known to be constant
     * once their objects are initialized.
     *
     * @param rm                        analyzed method
     * @param field                     dereference
     * @param index                     index of the dereference within
     *                                  the method
     * @param values                    values of runtime fields, usually
     *                                  obtained using
     *                                  <code>findValuesOfRuntimeFields</code>
     */
    /*
    protected void evaluateRange(RuntimeMethod rm, CodeFieldDereference field,
            int index, Map<RuntimeField, CodeConstant> values) {
        CodeVariablePrimitiveRange range = field.getTypePrimitiveRange();
        if(range != null) {
            if(field.variable.getResultType().isJava() &&
                    !field.variable.getResultType().isArray(null))
                throw new RuntimeException("non--primitive type has " +
                        "variable primitive range but is not an array");
            if(!range.isEvaluatedAsConstant()) {
                // the constants are needed now, but have not been evaluated.
                AbstractCodeValue min = range.reconstructMin(field.object);
                AbstractCodeValue max = range.reconstructMax(field.object);
                if(!(min instanceof CodeConstant)) {
                    CodeFieldDereference minV = (CodeFieldDereference)min;
                    RuntimeField minF = getRuntimeField(rm, index, minV);
                    CodeConstant minC = values.get(minF);
                    range.min = minC;
                }
                if(!(max instanceof CodeConstant)) {
                    CodeFieldDereference maxV = (CodeFieldDereference)max;
                    RuntimeField maxF = getRuntimeField(rm, index, maxV);
                    CodeConstant maxC = values.get(maxF);
                    range.max = maxC;
                }
            }
        }
    }
     */
    /**
     * Replaces sources within all operations in some method,
     * that point to constant
     * fields, with the respective constant values.<br>
     *
     * This is similar to <code>TraceValues.propagateValues</code>, but
     * bothers not with locals but with fields that are constant within the code
     * specified while constructing this object.<br>
     *
     * Replaced fields, together with replacing constant, are added
     * to <code>replaced</code>.<br>
     *
     * This method requires all traces to be current, because it uses
     * <code>findConstantFields</code>.
     *
     * @param rm                        method to analyze
     * @return                          if anything has been replaced
     */
    protected boolean replaceConstantFields(RuntimeMethod rm)
            throws InterpreterException {
        boolean stable = true;
        Set<RuntimeField> constants = findConstantFields();
        Map<RuntimeField, CodeConstant> values =
                findValuesOfRuntimeFields(constants);
        int index = 0;
        for(AbstractCodeOp op : rm.method.code.ops) {
            Collection<CodeFieldDereference> sources = op.getFieldSources();
            for(CodeFieldDereference d : CodeVariablePrimitiveRange.noRangeFirstD(sources)) {
                RuntimeField rf;
                try {
                    rf = getSingleRuntimeField(rm,
                            index, d);
                } catch(InterpreterException e) {
                    // could be dead code, ignore
                    rf = null;
                }
                if(rf != null && constants.contains(rf) &&
                        // variables in ranges of fields should generally not be replaced with
                        // constants, as they are per--instance values; there is no need
                        // for this anyway, as the variables are known to be constant after
                        // objects are initialized
                        !(!d.isLocal() && op.belongsToVariableRange(d))) {
                    CodeConstant c = values.get(rf);
                    if(d.getResultType().isPrimitive()) {
                        if(!op.replaceFieldSourceVariable(d, c))
                            throw new RuntimeException("no replacement");
                        replaced.put(rf, c.value);
                        stable = false;
                    } else {
                        // replacement of Java type variable with constant is a special case,
                        // as the variable might be at object part, thus not replaceable.
                        // Thus, replace only rvalues of assignments and casts -- this may help the
                        // tracer, and in effect additional Java type constants may help
                        // finding the runtime fields which are pointed by dereferences
                        if(op instanceof CodeOpAssignment) {
                            CodeOpAssignment a = (CodeOpAssignment)op;
                            if(a.rvalue.value == d) {
                                /// evaluateRange(rm, d, index, values);
                                /// if(d.getTypePrimitiveRange() != null)
                                ///     arr.register(rf, d.getTypePrimitiveRange());
                                a.rvalue.value = c;
                                replaced.put(rf, c.value);
                                stable = false;
                            }
                        } else if(op instanceof CodeOpUnaryExpression) {
                            CodeOpUnaryExpression ue = (CodeOpUnaryExpression)op;
                            if(ue.op == AbstractCodeOp.Op.UNARY_CAST) {
                                if(ue.sub.value == d) {
                                    /// evaluateRange(rm, d, index, values);
                                    /// if(d.getTypePrimitiveRange() != null)
                                    ///     arr.register(rf, d.getTypePrimitiveRange());
                                    ue.sub.value = c;
                                    replaced.put(rf, c.value);
                                    stable = false;
                                }
                            }
                        }
                    }
                }
            }
            ++index;
        }
        return !stable;
    }
    /**
     * Computes expressions that can be evaluated statically, and then
     * replaces them with assignments. This is similar to
     * <code>StaticAnalysis</code>, but works on code. If any expressions
     * has been optimized out, a number of optimizations is performed,
     * including removing of unused locals. This simplifies code, this
     * way making possible further iterative tracing within this object
     * faster.<br>
     *
     * If this method outdates the trace of <code>rm</code>, then it is
     * guaranteed that the method's return value is true.<br>
     *
     * Does not need method traces.
     *
     * @param rm                        method to analyze
     * @return                          if any expression was replaced by
     *                                  assignment
     */
    protected boolean runtimeStaticAnalysis(RuntimeMethod rm)
            throws InterpreterException {
        InterpretingContext ic = new InterpretingContext(interpreter,
                null, false, rm, InterpretingContext.UseVariables.LOCAL);
        boolean stable = !CodeStaticAnalysis.staticAnalysisSingle(
                ic, rm.method, trace.get(rm));
        if(!stable)
            // might outdate the trace, but <code>stable</code> is false,
            // thus, the trace would be updated anyway if needed by the
            // caller
            optimize(rm, false);
        return !stable;
    }
    /**
     * Tests each locals if its range, if can be evaluated as constants, is valid,
     * i. e. min is not &gt; max. If not, an interpreter exception is thrown.
     * 
     * @param method method, whole locals are to be tested
     */
    void checkValidRanges(CodeMethod method) throws InterpreterException {
        for(CodeVariable cv : method.variables.values())
            if(cv.primitiveRange != null &&
                    cv.primitiveRange.isEvaluatedAsConstant()) {
                Literal min = ((CodeConstant)cv.primitiveRange.min).value;
                Literal max = ((CodeConstant)cv.primitiveRange.max).value;
                if(max.arithmeticLessThan(min))
                    throw new InterpreterException(
                            cv.getStreamPos() != null ? cv.getStreamPos() :
                                (method.getStreamPos() != null ? method.getStreamPos() :
                                    method.owner.getStreamPos()),
                            "invalid bounds of " + cv.toNameString() + " detected: " +
                            cv.primitiveRange.toString());
            }
    }
    /**
     * A set of optimizations of a method, after static analysis or
     * replacement of fields with constants.
     * 
     * @param rm                        method to optimize
     * @param keepTraceCurrent          if the trace of the method should
     *                                  be kept current
     * @return if anything changed
     */
    boolean optimize(RuntimeMethod rm, boolean keepTraceCurrent)
            throws InterpreterException {
        Map<CodeVariable, AbstractCodeValue> args =
                constructArgs(interpreter, rm);
        // might outdate the trace map
        boolean stable = !TraceValues.propagateValues(rm.method, args,
                null, null, null, null);
        checkValidRanges(rm.method);
        if(Optimizer.optimizeAssignments(rm.method.code)) {
            CodeLabel.setIndices(rm.method.code);
            stable = false;
        }
        /*
         * do not merge variables before range proposals are computed
        // some variables might be merged, if their ranges became
        // evaluated as equal ones
        MergeVariables.reduceLocals(rm.method, Double.NaN, true);
         */
        // some variables might be no longer used
        if(Optimizer.optimizeUnusedLocals(rm.method))
            stable = false;
        if(TraceValues.removeRedundantAssignments(rm.method, args, null,
                false))
            stable = false;
        if(!stable && keepTraceCurrent)
            traceRuntimeValues(rm);
        return !stable;
    }
    /**
     * Returns the mapping of replaced fields to replacement
     * constants.
     * 
     * @return                          replaced fields
     */
    public Map<RuntimeField, Literal> getReplacedFields() {
        return replaced;
    }
    /**
     * Sets the mapping of replaced fields to replacement
     * constants. Typically used to copy the list from another
     * <code>RuntimeStaticAnalysis</code>.
     * 
     * @param replaced map of replacements
     */
    public void setReplacedFields(Map<RuntimeField, Literal> replaced) {
        this.replaced = replaced;
    }
    /**
     * Returns the interpreter used by this object.<br>
     * 
     * To be used, for example, to access elements of array constants
     * returned by <code>findValueOfRuntimeField</code>.
     * 
     * @return                          an interpreter
     */
    public AbstractInterpreter getInterpreter() {
        return interpreter;
    }
    /**
     * Returns a constant, if known, of a local or a field.
     * 
     * Locals are traced to fields.
     * 
     * @param rm runtime method
     * @param index index of the operation, at which the object has the
     * desired value
     * @param object the local or field, whose value is requested
     * @return 
     */
    public CodeConstant getConstant(RuntimeMethod rm, int index,
            CodeFieldDereference object) throws InterpreterException {
        if(object.isLocal()) {
            // a local has an unknown runtime type when this method is called,
            // so try to trace it to a field
            AbstractCodeValue v = getTrace(rm).getSingleM1(
                    index - 1, new CodeFieldDereference(object));
            if(v instanceof CodeConstant) {
                return (CodeConstant)v;
            } else if(v instanceof CodeFieldDereference)
                object = (CodeFieldDereference)v;
            else
                throw new InterpreterException(
                        rm.method.code.ops.get(index).getStreamPos(),
                        "value of " + object.toString() + " is not yet known");
        }
//if(object.object.name.contains("c14"))
//    object = object;
        RuntimeField rf = getSingleRuntimeField(rm, index, object);
        if(rf == null)
            throw new InterpreterException(rm.method.code.ops.get(index).getStreamPos(),
                    "can not evaluate to a constant: " + object.toString());
        return findValueOfRuntimeField(rf);
    }
    /**
     * Returns an array store, indexed in a given dereference.
     * If the dereference is null, an interpreted exception is thrown.
     * 
     * @param rm runtime method
     * @param codeIndex index in the method's code of <code>i</code> 
     * @param i index dereference
     * @return array store
     */
    public ArrayStore getArrayStore(RuntimeMethod rm,
            int codeIndex, CodeIndexDereference i) throws InterpreterException {
        CodeFieldDereference array = i.extractTypeSourceFieldDereference();
        CodeConstant arrayConst = getConstant(rm, codeIndex, array);
        ArrayStore as = ((ArrayStore)
                ((RuntimeValue)arrayConst.value).getReferencedObject());
        if(as == null)
            throw new InterpreterException(rm.method.code.ops.get(
                    codeIndex).getStreamPos(),
                    "dereference on a null array");
        return as;
    }
    /**
     * Returns the description of a store's element, which is
     * referred by a given index dereference.
     * 
     * @param rm runtime method
     * @param codeIndex index in the method's code of <code>i</code> 
     * @param i index dereference
     * @return a description or null, if <code>i</code> does not
     * consist of constants
     */
    public ArrayStore.ElementRef getElementRef(RuntimeMethod rm,
            int codeIndex, CodeIndexDereference i) throws InterpreterException {
        if(i.index.value instanceof CodeConstant) {
            CodeConstant indexConst = (CodeConstant)i.index.value;
            ArrayStore as = getArrayStore(rm, codeIndex, i);
            int elementIndex = CompilerUtils.toInt(
                    indexConst.value.getMaxPrecisionInteger());
            return new ArrayStore.ElementRef(as, elementIndex);
        } else
            return null;
    }
}
