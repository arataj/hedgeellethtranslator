/*
 * SplitAmbiguous.java
 *
 * Created on Sep 7, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.code.optimizer.runtime;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.code.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.op.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.optimizer.CodeValuePossibility;
import pl.gliwice.iitis.hedgeelleth.compiler.code.optimizer.Optimizer;
import pl.gliwice.iitis.hedgeelleth.compiler.util.UniquePrefix;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.StreamPos;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.RuntimeMethod;
import pl.gliwice.iitis.hedgeelleth.interpreter.exception.InterpreterException;

/**
 * An optimization that tries to split an ambigous dereference or
 * a floating point variable that possibly has many values, and then
 * move the pieces into places, where the dereference is no longer
 * ambiguous or the floating point variable can be replaced with
 * a constant.<br>
 * 
 * Ambiguous dereferences are forbidden in PTAs, and floating point
 * variables are not allowed in the Prism format.
 *
 * @author Artur Rataj
 */
public class SplitAmbiguous {
    /**
     * Runtime static analysis.
     */
    protected final RuntimeStaticAnalysis sa;
    /**
     * If ambiguous dereferences should be splitted.
     */
    protected final boolean splitDereferences;
    /**
     * If floating point variables that can not be reduced to constants
     * should be splitted.
     */
    protected final boolean splitFpVariables;
    /**
     * If assignments of bounds of variable ranges should be splitted.
     */
    protected final boolean splitVariableRanges;

    /**
     * Defines a block of code, that ends with an operation wit the
     * ambiguous dereference, and can be safely moved together
     * with that dereference.
     */
    protected static class Prefix {
        /**
         * Index of the first operation.
         */
        public int indexBeg;
        /**
         * Index of the last operation.
         */
        public int indexEnd;
        /**
         * Where to move the prefix.
         */
        public SortedSet<Integer> move;

        /**
         * If a jump at a given target is internal for this prefix.
         * 
         * @param target the jump's target
         * @return if the jump is internal
         */
        public boolean internalJump(int target) {
            return target >= indexBeg && target <= indexEnd;
        }
    };
    
    /**
     *  Creates a new instance.
     * 
     * @param sa runtime static analysis; changes to methods
     * in <code>SplitAmbiguous.transform()</code> are reflected in this object's trace
     * @param dereferences copied to the field <code>splitDereferences</code>
     * @param fpVariables copied to the field <code>splitFpVariables</code>
     * @param variablesRanges copied to the field <code>splitVariableRanges</code>
     */
    public SplitAmbiguous(RuntimeStaticAnalysis sa, boolean dereferences,
            boolean fpVariables, boolean variableRanges) {
        this.sa = sa;
        splitDereferences = dereferences;
        splitFpVariables = fpVariables;
        splitVariableRanges = variableRanges;
    }
    /**
     * Assure, that there are no uninitialized or unknown
     * values. Otherwise, false is returned, unless the
     * dereference is a java type, where an interpreter exception is thrown,
     * as object variables must be not null or ambiguous.
     * 
     * @param pos position of the source stream, to put in a possible
     * exception
     * @param d variable, whose values are tested
     * @param values values to test
     * @return if no unknown and uninitialized values were found
     */
    protected static boolean filterValues(StreamPos pos,
            CodeFieldDereference d, Set<AbstractCodeValue> values)
            throws InterpreterException {
        for(AbstractCodeValue v : values)
            if(v == null) {
                if(d.getResultType().isJava())
                    throw new InterpreterException(pos,
                        "null dereference of " + d.toNameString());
                else
                    return false;
            }
            else if(v == CodeValuePossibility.UNKNOWN_VALUE) {
                if(d.getResultType().isJava())
                    throw new InterpreterException(pos,
                        "unresolved dereference of " + d.toNameString());
                else
                    return false;
            }
        return true;
    }
    /**
     * Searches for a prefix of a given operation <i>i</i> with an
     * ambiguous dereference.
     * 
     * @param rm runtime method
     * @param object the object, whose ambiguous dereference is resolved;
     * must be a local
     * @param indexEnd index of <i>i</i>; can be prolonged to a single
     * further operation
     * @param ambiguous the ambiguous dereferenced values
     * @param arbitraryJumpsOutside if to allows jumps from the prefix to anywhere;
     * if true, the splitting procedure must test jumps in the prefix and not rename those,
     * whose target is not internal; if false, then the splitting procedure just renames all
     * jumps within the prefix
     * @return 
     */
    protected Prefix findPrefix(RuntimeMethod rm, CodeFieldDereference object,
            int indexEnd, Set<AbstractCodeValue> ambiguous,
            boolean arbitraryJumpsOutside) throws InterpreterException {
        Prefix p = new Prefix();
        // still unknown
        p.indexBeg = -1;
        p.indexEnd = indexEnd;
        // check for evaluation/branch combos, as they need to be together
        // for certain optimizations in PTA
        if(indexEnd < rm.method.code.ops.size() - 1) {
            AbstractCodeDereference prevResult = rm.method.code.ops.get(indexEnd).getResult();
            if(prevResult instanceof CodeFieldDereference &&
                    ((CodeFieldDereference)prevResult).isLocal()) {
                CodeVariable r = prevResult.extractPlainLocal().iterator().next();
                AbstractCodeOp next = rm.method.code.ops.get(indexEnd + 1);
                if(r.getResultType().isBoolean() &&
                        next instanceof CodeOpBranch &&
                        next.getLocalSources().contains(r))
                    // include the following branch
                    ++p.indexEnd;
            }
        }
        CodeMethod method = rm.method;
        // jumps, that must be internal
        Set<Integer> internalJumps = new HashSet<>();
        for(int i = p.indexEnd; i >= 0; --i) {
            AbstractCodeOp op = method.code.ops.get(i);
            if(i != p.indexEnd) {
                // check the preceeding operations for interference
                //
                // check if a jump outside the prefix
                if(op instanceof CodeOpReturn ||
                        op instanceof CodeOpThrow)
                    break;
                if(!arbitraryJumpsOutside)
                    internalJumps.addAll(
                        method.code.getJumpTargets(i));
                AbstractCodeValue result = op.getResult();
                if(result instanceof CodeFieldDereference) {
                    CodeFieldDereference f = (CodeFieldDereference)result;
                    // check if the local <code>d</code> is modified
                    if(f.variable == object.variable)
                        break;
                    if(!splitVariableRanges && f.variable.isVariablePRBound())
                        break;
                }
            }
            SortedSet<Integer> sources = method.code.getJumpSources(i);
            boolean sourcesUnequivocal = true;
            Set<AbstractCodeValue> sourcesValues = new HashSet<>();
            for(int j : sources) {
                Set<AbstractCodeValue> there = sa.getTrace(rm).getValuesM1(j - 1, object);
                if(!filterValues(method.code.ops.get(j).getStreamPos(), object, there) ||
                        there.size() != 1)
                    sourcesUnequivocal = false;
                else {
                    sourcesValues.add(there.iterator().next());
                }
                if(there.isEmpty())
                    throw new RuntimeException("unexpected missing values " +
                            "at source");
            }
            if(sourcesUnequivocal && sourcesValues.equals(ambiguous)) {
                if(i == 0 ||
                        // check if the operation before the prefix possibly passes control
                        // to the prefix, but possibly not as a jump, i. e. not registered in
                        // <code>internalJumps</code>
                        !method.code.possiblyFollowedByNext(i - 1)) {
                    p.indexBeg = i;
                    p.move = sources;
                    break;
                }
            } else
                // a prefix to be moved can not have
                // uncontrolled jumps from the outside into it
                internalJumps.addAll(sources);
        }
        if(p.indexBeg != -1) {
            // now check, if the internal jumps are really internal
            for(int target : internalJumps)
                if(!p.internalJump(target)) {
                    p = null;
                    break;
                }
        } else
            p = null;
        return p;
    }
    /**
     * Splits and moves a prefix into locations, where the dereferences are
     * not ambiguous.
     * 
     * @param method the method, that contains the prefix
     * @param prefix prefix to split
     */
    protected void splitPrefix(CodeMethod method, Prefix prefix) {
        int offset = 0;
        for(int targetIndex : prefix.move) {
            for(int opIndex = prefix.indexBeg; opIndex <= prefix.indexEnd;
                    ++opIndex) {
                AbstractCodeOp op = method.code.ops.get(opIndex + offset).clone();
                String labelPrefix = UniquePrefix.get(
                        "split_" + targetIndex + "_" + prefix.indexBeg + "_");
                if(op.label != null)
                    if(opIndex == prefix.indexBeg)
                        // no longer needed
                        op.label = null;
                    else
                        op.label.name = labelPrefix + op.label.name;
                for(CodeLabel ref : op.getLabelRefs())
                    // rename only internal jumps
                    //
                    // all used indiced are pre--split ones here
                    if(prefix.internalJump(ref.codeIndex))
                        ref.name = labelPrefix + ref.name;
                method.code.ops.add(targetIndex + offset, op);
                ++offset;
            }
        }
        for(int opIndex = prefix.indexBeg; opIndex <= prefix.indexEnd;
                ++opIndex)
            method.code.replace(opIndex + offset, new CodeOpNone(
                    method.code.ops.get(opIndex + offset).getStreamPos()),
                    true);
        Optimizer.setLabelIndices(method);
    }
    /**
     * Processes a method.
     * 
     * @param method                    a runtime method with the code to modify
     * @return                          if anything has been changed in the
     *                                  code of <code>method</code>
     */
    public boolean transform(RuntimeMethod rm) throws InterpreterException {
        boolean totalStable = true;
        boolean stable;
        int scanBeg = 0;
        do {
            stable= true;
            // CodeValuePossibility possibility = sa.getTrace(rm);
            CodeMethod method = rm.method;
            // System.out.println(possibility.toString());
            SCAN:
            for(int index = scanBeg; index < method.code.ops.size(); ++index) {
                AbstractCodeOp op = method.code.ops.get(index);
                // variables to be checked for multiple values
                List<CodeVariable> vars = new LinkedList<>();
                if(splitDereferences) {
                    Collection<CodeFieldDereference> allFields = op.getFieldSources();
                    allFields.addAll(op.getFieldTargets());
                    for(CodeFieldDereference f : allFields)
                        if(f.object != null && f.object.isLocal())
                            vars.add(f.object);
                }
                if(splitFpVariables) {
                    Collection<CodeVariable> localSources = op.getLocalSources();
                    for(CodeVariable l : localSources)
                        if(l.type.isOfFloatingPointTypes())
                            vars.add(l);
                }
                for(CodeVariable v : vars) {
                    CodeFieldDereference object = new CodeFieldDereference(v);
                    Set<AbstractCodeValue> values = sa.getTrace(rm).getValuesM1(index -1,
                            object);
                    if(filterValues(op.getStreamPos(), object, values) &&
                            values.size() > 1 &&
                            // splits only locals
                            object.isLocal()) {
                        Prefix prefix = findPrefix(rm, object, index, values, true);
                        if(prefix != null) {
                            splitPrefix(rm.method, prefix);
                            sa.traceRuntimeValues(rm);
                            stable = false;
                            scanBeg = prefix.move.first();
                            break SCAN;
                        }
                    }
                }
            }
            if(!stable)
                totalStable = false;
        } while(!stable);
        return !totalStable;
    }

}
