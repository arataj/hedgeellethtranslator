/*
 * AbstractCodePrimitiveRange.java
 *
 * Created on Nov 5, 2009, 3:02:43 PM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.primitiverange;

import pl.gliwice.iitis.hedgeelleth.compiler.code.AbstractCodeValue;
import pl.gliwice.iitis.hedgeelleth.compiler.code.CodeConstant;
import pl.gliwice.iitis.hedgeelleth.compiler.code.CodeVariable;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.*;

/**
 * Abstract primitive range in the code, either variable or primary one.<br>
 * 
 * If bounds in this range are variables, then these variables must be
 * internal and without their own bounds.
 *
 * @author Artur Rataj
 */
public abstract class AbstractCodePrimitiveRange {
    /**
     * Value holding the minimum value of this range.<br>
     *
     * The rest of this description is only for the case when this object
     * is a variable primitive range:
     *
     * The value can either be <code>CodeFieldDereference</code> or
     * <code>CodeConstant</code>. In the former case, the object part must
     * be null, as the dereference is only a wrapper of a code variable.
     * The runtime object is supplied by the variable that owns this
     * primitive range. The method <code>reconstructMin</code> can be
     * used for that.<br>
     *
     * The object is not given directly in this field, as this primitive
     * range is, as in the source code, tied to a code variable, and not to
     * <code>CodeFieldDereference</code>.
     *
     * @see #reconstructMin
     */
    public AbstractCodeValue min;
    /**
     * Variable holding the maximum value of this range. For the rules that
     * apply, see <code>min</code>.
     *
     * @see #reconstructMax
     */
    public AbstractCodeValue max;

    /**
     * Creates a new instance of AbstractCodePrimitiveRange. If any of the
     * parameters <code>min</code> and <code>max</code> is null, then the
     * respective fields must be set later.
     *
     * @param min                       minimum value, see docs of the field <code>min</code>
     *                                  for details on the container
     * @param max                       maximum value, see docs of the field <code>min</code>
     *                                  for details on the container
     */
    public AbstractCodePrimitiveRange(AbstractCodeValue min, AbstractCodeValue max) {
        this.min = min;
        this.max = max;
        check(this.min);
        check(this.max);
    }
    /**
     * Checks, if <code>bound</code> is a valid bound. If not, a runtime
     * exception is thrown.
     * 
     * @param bound bound to check
     */
    private void check(AbstractCodeValue bound) {
        if(bound instanceof CodeFieldDereference) {
            if(((CodeFieldDereference)bound).object != null)
                throw new RuntimeException("unexpected non--static field");
            CodeVariable v = ((CodeFieldDereference)bound).variable;
            if(!v.isInternal())
                throw new RuntimeException("internal variable expected");
            if(this instanceof CodeVariablePrimitiveRange && !v.isVariablePRBound())
                throw new RuntimeException("variable primitive range's bound expected");
        }
        if(bound instanceof CodeIndexDereference)
            throw new RuntimeException("unexpected indexing");
    }
    /**
     * Makes this range an intersection of this range and another
     * range.
     *
     * @param range                     the other range
     */
    public void limit(CodePrimaryPrimitiveRange range) {
        throw new UnsupportedOperationException("not implemented");
    }
    /**
     * If this range is evaluated as constant, that is, both <code>min</code>
     * and <code>max</code> are constants.
     *
     * @return
     */
    public boolean isEvaluatedAsConstant() {
        return min instanceof CodeConstant &&
                max instanceof CodeConstant;
    }
    /**
     * If this range is evaluated as constant and the other range is
     * evaluated as constant too, this method can compare these two
     * ranges. If the comparison can not be done, a runtime exception
     * is throw.<br>
     *
     * This comparison is done arithmetically, that is, it is not checked
     * if types match, only if arithmetic values match.
     *
     * @param other                     the range to compare to this range
     * @return                          the constants in the ranges match
     */
    public boolean arithmeticConstantsEqual(AbstractCodePrimitiveRange other) {
/*if(other == null)
    other = other;*/
        if(!isEvaluatedAsConstant() || !other.isEvaluatedAsConstant())
            throw new RuntimeException("this method can compare " +
                    "only ranges with known constants");
        CodeConstant thisMin = (CodeConstant)min;
        CodeConstant thisMax = (CodeConstant)max;
        CodeConstant thatMin = (CodeConstant)other.min;
        CodeConstant thatMax = (CodeConstant)other.max;
        return thisMin.value.arithmeticEqualTo(thatMin.value) &&
                thisMax.value.arithmeticEqualTo(thatMax.value);
    }
    /**
     * Returns the number of states of a variable that has this primitive
     * range. The number of states equals to the number of all allowed
     * values of the variable = (range_max - range_min + 1). The variable
     * is assumed to be of integer types.<br>
     *
     * This range must be evaluated and the evaluations must be integer,
     * otherwise a runtime exception is thrown.<br>
     *
     * This method is similar to
     * <code>AbstractCodePrimitiveRange.getStatesNum()</code>.
     *
     * @return                          number of possible states
     */
    public long getStatesNum() {
        if(!isEvaluatedAsConstant())
            throw new RuntimeException("not evaluated");
        CodeConstant minC = (CodeConstant)min;
        CodeConstant maxC = (CodeConstant)max;
        if(!minC.value.type.isOfIntegerTypes() ||
                !maxC.value.type.isOfIntegerTypes())
            throw new RuntimeException("evaluation not integer");
        long minV = minC.value.getMaxPrecisionInteger();
        long maxV = maxC.value.getMaxPrecisionInteger();
        return maxV - minV + 1;
    }
    /**
     * Returns the image of the token, that prefixes this range.
     */
    abstract public String getPrefixString();
    /**
     * Returns the image of the token, that suffixes this range.
     */
    abstract public String getSuffixString();
    /**
     * Like toString(), but returns only names, not types.
     *
     * @return                          description of this value,
     *                                  excluding types
     */
    public String toNameString() {
        return getPrefixString() +
                min.toNameString() + ", " + max.toNameString() +
                getSuffixString();
    }
    @Override
    public String toString() {
        return getPrefixString() +
                min.toString() + ", " + max.toString() +
                getSuffixString();
    }
}
