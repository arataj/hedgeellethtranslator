/*
 * CodePrimaryPrimitiveRange.java
 *
 * Created on Aug 18, 2009, 2:02:38 PM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.primitiverange;

import pl.gliwice.iitis.hedgeelleth.compiler.code.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Literal;
import pl.gliwice.iitis.hedgeelleth.interpreter.AbstractInterpreter;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.RuntimeMethod;
import pl.gliwice.iitis.hedgeelleth.interpreter.exception.InterpreterException;
import pl.gliwice.iitis.hedgeelleth.interpreter.exception.InterpreterMissingException;

/**
 * Holds minimum and maximum values of a primary expression's primitive range.
 *
 * @author Artur Rataj
 */
public class CodePrimaryPrimitiveRange extends AbstractCodePrimitiveRange
        implements Cloneable {
    /**
     * Creates a new instance of CodePrimaryPrimitiveRange.
     *
     * @param min                       minimum value
     * @param max                       maximum value
     */
    public CodePrimaryPrimitiveRange(AbstractCodeValue min, AbstractCodeValue max) {
        super(min, max);
    }
    /**
     * If a given value falls within this range. If not, an interpreter
     * exception is thrown.<br>
     *
     * Note that variable's primitive bound, as opposed to code's primitive range,
     * is tested by containers. Both are tested by the interpreter, that uses
     * this method and container's <code>getValue</code>.
     *
     * Throws <code>InterpreterMissingException</code> if the bound can not be
     * evaluated because it contains variables.
     *
     * @param l                         value to range check, must be boolean,
     *                                  of integer or of floating point types
     * @param interpreter               interpreter, can be null if this
     *                                  range returns true for
     *                                  <code>isEvaluatedAsConstant()</code>
     * @param rm                        runtime method, can be null in the same
     *                                  conditions when <code>interpreter</code>
     *                                  can be null
     */
    public void check(Literal l, AbstractInterpreter interpreter, RuntimeMethod rm)
            throws InterpreterException {
        if(l.type.isOfIntegerTypes()) {
            long i = l.getMaxPrecisionInteger();
            long minI;
            long maxI;
            if(min instanceof CodeConstant)
                minI = ((CodeConstant)min).value.getMaxPrecisionInteger();
            else {
                if(interpreter == null)
                    throw new InterpreterMissingException("can not evaluate");
                minI = interpreter.getValue(
                        new RangedCodeValue(min), rm).getMaxPrecisionInteger();
            }
            if(max instanceof CodeConstant)
                maxI = ((CodeConstant)max).value.getMaxPrecisionInteger();
            else {
                if(interpreter == null)
                    throw new InterpreterMissingException("can not evaluate");
                maxI = interpreter.getValue(
                        new RangedCodeValue(max), rm).getMaxPrecisionInteger();
            }
            if(i < minI || i > maxI)
                throw new InterpreterException(null, "range check error, value is " + i + ", " +
                        "should be in " + getPrefixString() + minI + ", " +
                        maxI + getSuffixString());
        } else if(l.type.isOfFloatingPointTypes()) {
            double f = l.getMaxPrecisionFloatingPoint();
            double minF;
            double maxF;
            if(min instanceof CodeConstant)
                minF = ((CodeConstant)min).value.getMaxPrecisionFloatingPoint();
            else
                minF = interpreter.getValue(
                        new RangedCodeValue(min), rm).getMaxPrecisionFloatingPoint();
            if(max instanceof CodeConstant)
                maxF = ((CodeConstant)max).value.getMaxPrecisionFloatingPoint();
            else
                maxF = interpreter.getValue(
                        new RangedCodeValue(max), rm).getMaxPrecisionFloatingPoint();
            if(f < minF || f > maxF)
                throw new InterpreterException(null, "range check error, value is " + f + ", " +
                        "should be in " + getPrefixString() + minF + ", " +
                        maxF + getSuffixString());
        } else if(l.type.isOfBooleanType()) {
            boolean i = l.getBoolean();
            boolean minB;
            boolean maxB;
            if(min instanceof CodeConstant)
                minB = ((CodeConstant)min).value.getBoolean();
            else
                minB = interpreter.getValue(
                        new RangedCodeValue(min), rm).getBoolean();
            if(max instanceof CodeConstant)
                maxB = ((CodeConstant)max).value.getBoolean();
            else
                maxB = interpreter.getValue(
                        new RangedCodeValue(max), rm).getBoolean();
            if(minB == maxB && i != minB)
                throw new InterpreterException(null, "range check error, value is " + i + ", " +
                        "should be in " + getPrefixString() + minB + ", " +
                        maxB + getSuffixString());
        } else
            throw new RuntimeException("invalid type");
    }
    @Override
    public String getPrefixString() {
        return PrimitiveRangeDescription.PRIMARY_LEFT_STRING;
    }
    @Override
    public String getSuffixString() {
        return PrimitiveRangeDescription.PRIMARY_RIGHT_STRING;
    }
    @Override
    public CodePrimaryPrimitiveRange clone() {
        Object o;
        try {
            o = super.clone();
        } catch(CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
        CodePrimaryPrimitiveRange rv = (CodePrimaryPrimitiveRange)o;
        rv.min = min.clone();
        rv.max = max.clone();
        return rv;
    }
}
