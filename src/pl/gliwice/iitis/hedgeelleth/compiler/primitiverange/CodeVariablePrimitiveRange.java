/*
 * CodeVariablePrimitiveRange.java
 *
 * Created on Sep 3, 2009, 2:41:40 PM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.primitiverange;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.code.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.optimizer.runtime.RuntimeStaticAnalysis;
import pl.gliwice.iitis.hedgeelleth.compiler.sa.RangeCodeStaticAnalysis.Proposal;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Context;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Literal;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Type;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Type.TypeRange;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.TypeRangeMap;
import pl.gliwice.iitis.hedgeelleth.interpreter.AbstractInterpreter;
import pl.gliwice.iitis.hedgeelleth.interpreter.RuntimeValue;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.AbstractRuntimeContainer;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.RuntimeMethod;
import pl.gliwice.iitis.hedgeelleth.interpreter.exception.InterpreterException;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.RuntimeField;

/**
 * A primitive range of a code variable, as opposed to a primitive range of
 * a primary expression. The range is evaluated once, at the time of
 * declaration of tthe ranged variable.<br>
 *
 * Can also hold evaluations of the range variables.<br>
 *
 * If the respective code variable is an array, then this range denotes
 * the array's elements, just like in the case of
 * <code>VariablePrimitiveRange</code>.
 *
 * @author Artur Rataj
 */
public class CodeVariablePrimitiveRange extends AbstractCodePrimitiveRange
        implements SourcePosition {
    /**
     * Position in the stream of the variable this range belongs to, null for none.
     * If not null, then also means source level range as defined in
     * <code>CodeVariable.hasRestrictiveSourceLevelRange()</code>.
     */
    protected StreamPos pos;
    /**
     * If this is a source--level range.
     */
    boolean sourceLevel;

    /**
     * Creates a new instance of code variable's primitive range.
     * 
     * See docs of the field <code>min</code> for details on the container
     * of the variables <code>min</code> and <code>max</code>.
     *
     * @param pos                       place in the source code of the variable
     *                                  this range belongs to, a source level range
     *                                  as defined in
     *                                  <code>CodeVariable.hasRestrictiveSourceLevelRange()</code>
     *                                  must have it not null
     * @param min                       variable holding the minimum
     *                                  value of the created range; if null, then
     *                                  the field <code>min</code> must be set later
     * @param max                       variable holding the maximum
     *                                  value of the created range; if null, then
     *                                  the field <code>max</code> must be set later
     * @param sourceLevel               if this is a source--level range
     */
    public CodeVariablePrimitiveRange(StreamPos pos,
            CodeVariable min, CodeVariable max, boolean sourceLevel) {
        super(min != null ? new CodeFieldDereference(min) : null,
                max != null ? new CodeFieldDereference(max) : null);
        setStreamPos(pos);
        this.sourceLevel = sourceLevel;
        if(pos == null && this.sourceLevel)
            throw new RuntimeException("a source--level range lacks position");
    }
    /**
     * Creates a new instance of code variable's primitive range, without
     * position, and not of the source level. These two fields can be modified
     * later if needed.
     *
     * @param min                       value of the minimum bound
     * @param max                       value of the maximum bound
     */
    public CodeVariablePrimitiveRange(AbstractCodeValue min, AbstractCodeValue max) {
        super(min, max);
/*if(min instanceof CodeConstant &&
        ((CodeConstant)min).value.getMaxPrecisionFloatingPoint() > 60)
    min = min;*/
        setStreamPos(null);
        sourceLevel = false;
    }
    
    /**
     * Creates a new instance of code variable's primitive range, without
     * position, and not of the source level. The range is the maximum
     * range of a given primitive.
     *
     * @param primitive                 primitive; if represents a void, runtime
     *                                  exception is thrown
     * @param allowBoolean              passed to the constructor
     *                                  <code>Type.TypeRange(PrimitiveOrVoid, boolean)</code>,
     *                                  see that constructor for details
     */
    public CodeVariablePrimitiveRange(Type.PrimitiveOrVoid primitive, boolean allowBoolean) {
        super(null, null);
        Type.TypeRange range = new Type.TypeRange(primitive, allowBoolean);
        min = new CodeConstant(null, range.getMinLiteral());
        max = new CodeConstant(null, range.getMaxLiteral());
    }
    @Override
    public void setStreamPos(StreamPos pos) {
        this.pos = pos;
    }
    @Override
    public StreamPos getStreamPos() {
        return pos;
    }
    /**
     * Sets, if range is a source--level one. See the docs of <code>CodeVariable</code>
     * for details on source--level ranges.
     * 
     * @param sourceLevel               if this range is source--level
     */
    public void setSourceLevel(boolean sourceLevel) {
        this.sourceLevel = sourceLevel;
    }
    /**
     * If this is a source--level range. See the docs of <code>CodeVariable</code>
     * for details on source--level ranges.
     * 
     * @return                          if this range is source--level
     */
    public boolean isSourceLevel() {
        return sourceLevel;
    }
    /**
     * It it can be determined currently, that this primitive range
     * is a superset of a proposal. It his range's limits are not constants,
     * this method always returns false.
     * 
     * @param proposal a proposal, if null or its range is unknown, this method
     * always returns false
     * @return if is a known superset
     */
    public boolean knownSuperset(Proposal proposal) {
        if(proposal != null && proposal.rangeKnown() &&
                min instanceof CodeConstant &&
                max instanceof CodeConstant)  {
            CodeConstant sourceMin = (CodeConstant)min;
            CodeConstant sourceMax = (CodeConstant)max;
            if((sourceMin.value.arithmeticEqualTo(proposal.min) ||
                    sourceMin.value.arithmeticLessThan(proposal.min)) &&
                (proposal.max.arithmeticEqualTo(sourceMax.value) ||
                    proposal.max.arithmeticLessThan(sourceMax.value)))
                return true;
        }
        return false;
    }
    /**
     * Reconstructs one of the bounds in this primitive range, by
     * adding <code>object</code> if the bound is not constant.<br>
     *
     * See the docs of <code>min<code> and <code>max<code> for details.
     *
     * @param object                    object that specifies which
     *                                  container to use, null for
     *                                  locals in the primitive bound
     * @param bound                     either <code>min</code> or
     *                                  <code>max</code>
     * @return                          reconstructed local, field or
     *                                  constant
     */
    protected AbstractCodeValue reconstruct(CodeVariable object,
            AbstractCodeValue bound) {
        if(bound instanceof CodeConstant)
            return bound;
        else {
            object = ((CodeFieldDereference)bound).variable.context ==
                Context.NON_STATIC ? object : null;
            return new CodeFieldDereference(object,
                    ((CodeFieldDereference)bound).variable);
        }
    }
    /**
     * Reconstructs <code>min</code> by calling <code>reconstruct(object, min)</code>,
     * see the docs of the called method for details.
     *
     * @param object                    object that specifies which
     *                                  container to use, null for
     *                                  locals in the primitive range;
     *                                  static variables ignore the container
     * @return                          reconstructed local, field or
     *                                  constant
     */
    public AbstractCodeValue reconstructMin(CodeVariable object) {
        return reconstruct(object, min);
    }
    /**
     * Reconstructs <code>max</code> by calling <code>reconstruct(object, max)</code>,
     * see the docs of the called method for details.
     *
     * @param object                    object that specifies which
     *                                  container to use, null for
     *                                  locals in the primitive range;
     *                                  static variables ignore the container
     * @return                          reconstructed local, field or
     *                                  constant
     */
    public AbstractCodeValue reconstructMax(CodeVariable object) {
        return reconstruct(object, max);
    }
    /**
     * Evaluates a minimum value of this primitive range,
     * given the position of the parent operation in the code.
     * 
     * @param sa runtime static analysis
     * @param rm runtime of a method
     * @param index index of the operation in the method;
     * it can not modify the range itself
     * @param container passed to <code>reconstructMin</code>,
     * see docs of that method for details
     * @return a constant, null if not found
     */
    public Literal evaluateMin(RuntimeStaticAnalysis sa,
            RuntimeMethod rm, int index, CodeVariable container)
            throws InterpreterException  {
        Literal out = null;
        if(min instanceof CodeConstant)
            out = ((CodeConstant)min).value;
        else {
            RuntimeField rf = sa.getSingleRuntimeField(rm, index,
                    (CodeFieldDereference)reconstructMin(container));
            if(rf != null)
                // it is a field indeed
                out = sa.findValueOfRuntimeField(rf).value;
        }
        return out;
    }
    /**
     * Evaluates a maximum value of this primitive range,
     * given the position of the parent operation in the code.
     * 
     * @param sa runtime static analysis
     * @param rm runtime of a method
     * @param index index of the operation in the method
     * @param container passed to <code>reconstructMax</code>,
     * see docs of that method for details
     * @return a constant, null if not found
     */
    public Literal evaluateMax(RuntimeStaticAnalysis sa,
            RuntimeMethod rm, int index, CodeVariable container)
            throws InterpreterException {
        Literal out = null;
        if(max instanceof CodeConstant)
            out = ((CodeConstant)max).value;
        else {
            RuntimeField rf = sa.getSingleRuntimeField(rm, index,
                    (CodeFieldDereference)reconstructMax(container));
            if(rf != null)
                // it is a field indeed
                out = sa.findValueOfRuntimeField(rf).value;
        }
        return out;
    }
    /**
     * Checks if value fits into a primitive range of a variable.
     * If not, <code>InterpreterException</code> is thrown.
     * If the variable is null or does not have a primitive range,
     * this method does nothing.<br>
     *
     * Sometimes a runtime container is known but a variable that points to it
     * is not; this method can handle that -- see the parameters <code>dereference</code>
     * and <code>container</code> for details.<br>
     *
     * @param interpreter               needed only if required for
     *                                  evaluation of primitive range, if
     *                                  null and the range is not null, the
     *                                  range can only contain contants or
     *                                  <code>rangeContainer</code> must be not null;
     *                                  if both of these conditions are not
     *                                  fulfilled,
     *                                  a class cast exception is thrown
     * @param rm                        runtime method, needed only if
     *                                  <code>interpreter</code> is not null,
     *                                  the primitive range contains
     *                                  locals and <code>rangeContainer</code>
     *                                  is null
     * @param dereference               dereference that defines the variable,
     *                                  possibly with a primitive
     *                                  range, or null; if container is missing,
     *                                  that is,
     *                                  <code>dereference.getContainerVariable</code>
     *                                  returns null even if the variable is
     *                                  non--static field, a runtime container
     *                                  must be supplied in <code>rangeContainer</code>
     * @param rangeContainer            if runtime container of the primitive range
     *                                  variables is known, it can be supplied
     *                                  here; required if <code>dereference</code>
     *                                  is incomplete; this method has priority
     *                                  over possible container specification in
     *                                  <code>dereference</code>
     * @param typeRangeMap              type range map, to check against
     *                                  type bounds; if interpreter is not null, then typically
     *                                  <code>interpreter.typeRangeMap</code> is used
     *                                  here
     * @param value                     value to check, if fits in the range
     */
    public static void check(AbstractInterpreter interpreter, RuntimeMethod rm,
            AbstractCodeDereference dereference, AbstractRuntimeContainer rangeContainer,
            TypeRangeMap typeRangeMap, RuntimeValue value) throws InterpreterException {
        // an array's range is related to the array's elements,
        // not to the array variable itself, thus, it is checked
        // if the value to check is not an array reference or null
if(value == null)
    value = value;
        if(!value.type.isArray(null) && !value.type.isNull() &&
                dereference != null) {
            CodeVariable variable = dereference.getTypeSourceVariable();
            if(typeRangeMap != null &&
                    variable.getResultType().isPrimitive() &&
                    // no other type is converted to boolean,
                    // so no need for checking type range
                    !variable.getResultType().isBoolean()) {
                Type type = variable.getResultType();
                TypeRange range = typeRangeMap.get(type.getPrimitive());
                Literal min = range.getMinLiteral();
                Literal max = range.getMaxLiteral();
                if((value.arithmeticLessThan(min) || max.arithmeticLessThan(value)) &&
                        // infinite floating point value can fit into any other
                        // floating point type
                        !(type.isOfFloatingPointTypes() && value.type.isOfFloatingPointTypes() &&
                            Double.isInfinite(value.getMaxPrecisionFloatingPoint())))
                    throw new InterpreterException(null,
                            "value " + value.toString() + " does not fit into type " +
                            "of variable " +
                            dereference.getTypeSourceVariable().toNameString());
            }
            if(variable.hasPrimitiveRange(false)) {
                Literal min;
                Literal max;
                if(!value.type.isPrimitive())
                        throw new RuntimeException("primitive range on non--primitive value");
                CodeVariablePrimitiveRange primitiveRange = variable.primitiveRange;
                if(rangeContainer != null) {
                    AbstractCodeValue inMin = primitiveRange.min;
                    AbstractCodeValue inMax = primitiveRange.max;
                    if(inMin instanceof CodeConstant)
                        min = ((CodeConstant)inMin).value;
                    else
                        // no runtime method needs to be given,
                        // as the primitive range does not have another primitive
                        // range
                        min = rangeContainer.getValue(interpreter, null,
                                ((CodeFieldDereference)inMin).variable);
                    if(inMax instanceof CodeConstant)
                        max = ((CodeConstant)inMax).value;
                    else
                        // no runtime method needs to be given,
                        // as the primitive range does not have another primitive
                        // range
                        max = rangeContainer.getValue(interpreter, null,
                                ((CodeFieldDereference)inMax).variable);
                } else if(interpreter != null) {
                    CodeVariable object = dereference.getContainerVariable();
                    min = interpreter.getValue(
                            // bound in primitive value does not have its own
                            // primitive value
                            new RangedCodeValue(primitiveRange.reconstructMin(object)),
                            rm);
                    max = interpreter.getValue(
                            // bound in primitive value does not have its own
                            // primitive value
                            new RangedCodeValue(primitiveRange.reconstructMax(object)),
                            rm);
                } else {
                    min = ((CodeConstant)primitiveRange.min).value;
                    max = ((CodeConstant)primitiveRange.max).value;
                }
                if(value.arithmeticLessThan(min) || max.arithmeticLessThan(value))
                    throw new InterpreterException(null,
                            "value " + value.toString() + " is out of " +
                            "range " + primitiveRange.getPrefixString() + min.toString() + ", " +
                            max.toString() + primitiveRange.getSuffixString() + " of variable " +
                            dereference.getTypeSourceVariable().toNameString());
            }
        }
    }
    @Override
    public String getPrefixString() {
        return PrimitiveRangeDescription.VARIABLE_LEFT_STRING;
    }
    @Override
    public String getSuffixString() {
        return PrimitiveRangeDescription.VARIABLE_RIGHT_STRING;
    }
    /**
     * Moves the variables that do not have primitive bounds first.<br>
     * 
     * This is to ensure that if a variable with a bound is replaced,
     * then possible replacements of that variable's bound variables
     * are already done, thus, no "no replacement" errors occur.
     * 
     * @param dereferences                    collection of dereferences
     * @return                          reordered list
     */
    public static List<CodeFieldDereference> noRangeFirstD(
            Collection<CodeFieldDereference> dereferences) {
        List<CodeFieldDereference> out = new LinkedList<>();
        for(CodeFieldDereference f : dereferences)
            if(f.getTypeSourceVariable().hasPrimitiveRange(false))
                out.add(f);
            else
                out.add(0, f);
        return out;
    }
    /**
     * Moves the variables that do not have primitive bounds first.<br>
     * 
     * This is to ensure that if a variable with a bound is replaced,
     * then possible replacements of that variable's bound variables
     * are already done, thus, no "no replacement" errors occur.<br>
     * 
     * This is a convenience method.
     * 
     * @param variables                 collection of variables
     * @return                          reordered list
     */
    public static List<CodeVariable> noRangeFirstV(
            Collection<CodeVariable> variables) {
        List<CodeVariable> out = new LinkedList<>();
        for(CodeVariable f : variables)
            if(f.hasPrimitiveRange(false))
                out.add(f);
            else
                out.add(0, f);
        return out;
    }
    @Override
    public String toString() {
        String s = super.toString();
        if(isSourceLevel())
            s = "[src]" + s;
        return s;
    }
}
