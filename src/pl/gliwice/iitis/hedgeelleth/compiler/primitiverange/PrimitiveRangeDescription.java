/*
 * PrimitiveRangeDescription.java
 *
 * Created on Aug 10, 2009
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.primitiverange;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.Type;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Variable;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.AbstractExpression;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.PrimaryPrefix;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * Description of a primitive range as found in the parsed stream.
 * Also can contain result of parsing of the text.<br>
 *
 * Additionally provides some utility methods for checking type of
 * variable or primary primitive range.
 *
 * @author Artur Rataj
 */
public class PrimitiveRangeDescription implements Cloneable {
    /**
     * Left token marking the beginning of a variable primitive range modifier.
     */
    public static final String VARIABLE_LEFT_STRING = "<<";
    /**
     * Right token marking the end of a variable primitive range modifier.
     */
    public static final String VARIABLE_RIGHT_STRING = ">>";
    /**
     * Left token marking the beginning of a primitive range modifier.
     */
    public static final String PRIMARY_LEFT_STRING = "<<"; // formerly "(<"
    /**
     * Right token marking the end of a primitive range modifier.
     */
    public static final String PRIMARY_RIGHT_STRING = ">>"; // formerly ">)"

    /**
     * Either a minimum of or maximum of a primitive variable's
     * range.
     */
    public static class VariableBound implements Cloneable {
        /**
         * An initializer of the variable that holds this bound's value.
         */
        public AbstractExpression value;
        /**
         * Variable that holds this bound's value.
         */
        public Variable variable;

        @Override
        public VariableBound clone() {
            VariableBound copy;
            try {
                copy = (VariableBound)super.clone();
            } catch(CloneNotSupportedException e) {
                throw new RuntimeException("unexpected");
            }
            copy.value = value.clone();
            return copy;
        }
        @Override
        public String toString() {
            String s;
            if(value == null)
                s = "<no value>";
            else
                s = value.toString();
            if(variable != null)
                s = variable.toString() + " = " + s;
            return s;
        }
    };

    /**
     * Text of the primitive type range, null for none.
     */
    public String text;
    /**
     * Position of <code>rangeText</code> in the main stream, null if
     * <code>rangeText</code> is null.
     */
    public StreamPos textPos;
    /**
     * Is this primitive range has really been used.<br>
     * 
     * Unused primitive ranges are considered stray ones and reported
     * in compiler's errors.
     */
    public boolean used = false;
    /**
     * Minimum bound.
     */
    public VariableBound min = null;
    /**
     * Maximum bound.
     */
    public VariableBound max = null;

    /**
     * Creates a new instance of <code>PrimitiveRange</code>. The objects
     * <code>min</code> and <code>max</code> are created by this
     * constructor.
     */
    public PrimitiveRangeDescription() {
        min = new VariableBound();
        max = new VariableBound();
    }
    /**
     * Returns the type of primitive range variables, based on
     * the type of the initialized variable. Throws a parse exception
     * if the type of the variable is invalid for a variable primitive
     * range.
     *
     * @param rangePos                  position of primitive range
     *                                  definition in the input stream
     * @param initializedVariable       type of the variable bounded by
     *                                  the primitive range, must either
     *                                  be primitive or
     *                                  array of primitives, otherwise a
     *                                  parse exception is thrown
     * @param arrayClassName            name of the array class in the current
     *                                  frontend,
     * @return                          type of variables in the primitive
     *                                  bound
     */
    public static Type getVariablePrimitiveRangeType(StreamPos rangePos,
            Type boundedVariableType, String arrayClassName) throws ParseException {
        Type rangeType;
        if(boundedVariableType.isArray(arrayClassName))
            rangeType = boundedVariableType.getElementType(
                    arrayClassName, 0);
        else
            rangeType = boundedVariableType;
        if(!rangeType.isPrimitive())
            throw new ParseException(rangePos, ParseException.Code.ILLEGAL,
                    "primitive range not on a primitive type");
        return rangeType;
    }
    /**
     * If a given type is valid for a primitive range, that is, if the
     * type is boolean, integer or floating point.<br>
     * 
     * This method just returns <code>t.isPrimitive()</code>.
     *
     * @param t                         type to test
     * @return                          if valid for primitive range
     */
    public static boolean validPrimaryRangeType(Type t) {
        return t.isPrimitive();
    }
    @Override
    public PrimitiveRangeDescription clone() {
        PrimitiveRangeDescription copy;
        try {
            copy = (PrimitiveRangeDescription)super.clone();
        } catch(CloneNotSupportedException e) {
            throw new RuntimeException("unexpected");
        }
        copy.textPos = new StreamPos(textPos);
        copy.min = min.clone();
        copy.max = max.clone();
        return copy;
    }
    @Override
    public String toString() {
        return VARIABLE_LEFT_STRING +
                min.toString() + ", " + max.toString() +
                VARIABLE_RIGHT_STRING;
    }
}
