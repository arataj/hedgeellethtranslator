/*
 * VariablePrimitiveRange.java
 *
 * Created on Sep 3, 2009, 2:41:40 PM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.primitiverange;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.Variable;

/**
 * A primitive range of a variable, as opposed to a primitive range of
 * a primary expression.
 *
 * If the respective variable is an array, then this range denotes
 * the array's elements.
 *
 * @author Artur Rataj
 */
public class VariablePrimitiveRange {
    /**
     * Variable holding the minimum value of this range.
     */
    public Variable min;
    /**
     * Variable holding the maximum value of this range.
     */
    public Variable max;

    /**
     * Creates a new instance of variable's primitive range.
     * 
     * @param min                       variable holding the minimum
     *                                  value of the created range
     * @param max                       variable holding the maximum
     *                                  value of the created range
     */
    public VariablePrimitiveRange(Variable min, Variable max) {
        this.min = min;
        this.max = max;
    }
    @Override
    public String toString() {
        return PrimitiveRangeDescription.VARIABLE_LEFT_STRING +
                min.toString() + ", " + max.toString() +
                PrimitiveRangeDescription.VARIABLE_RIGHT_STRING;
    }
}
