/*
 * Backup.java
 *
 * Created on Jun 27, 2009, 4:23:07 PM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.sa;

import java.util.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.languages.IntegerSetExpression;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.*;

/**
 * Recovers the tree's state from before the static analysis. This is
 * for multiple static analysis of a tree, for example to be used with
 * experiments enabled in <code>HedgeellethCompiler.allowExperiments</code>.
 *
 * @author Artur Rataj
 */
public class Backup {
    /**
     * Child type of some statement with its child replaced.
     */
    public enum ChildType {
        /**
         * For statements that have only a single child.
         * No unary expressions or primary expressions,
         * as these are replaced as a whole, without changing
         * their children.
         */
        SUB,
        /**
         * Left subexpression of a binary expression.
         */
        LEFT,
        /**
         * Right subexpression of a binary expression,
         * or rvalue of an assignment expression.
         */
        RIGHT,
        /**
         * Element in a list of expressions.
         */
        ELEMENT,
    };

    /**
     * List of statements, whose children changed.
     */
    List<AbstractStatement> statement;
    /**
     * What child type of a respective statement in <code>statement</code>
     * changed.
     */
    List<ChildType> childType;
    /**
     * If <code>ChildType</code> is <code>ELEMENT</code>, then the element's
     * index. Otherwise, -1.
     */
    List<Integer> childIndex;
    /**
     * What child expression of respective statement in <code>statement</code>
     * was before the static analysis.
     */
    List<AbstractExpression> child;

    /**
     * Creates a new instance of Backup. 
     */
    public Backup() {
        statement = new ArrayList<AbstractStatement>();
        childType = new ArrayList<ChildType>();
        childIndex = new ArrayList<Integer>();
        child = new ArrayList<AbstractExpression>();
    }
    /**
     * Add all contents from a temporary static analysis.
     * 
     * @param sa static analysis
     */
    void addAll(StaticAnalysis sa) {
        statement.addAll(sa.backup.statement);
        childType.addAll(sa.backup.childType);
        childIndex.addAll(sa.backup.childIndex);
        child.addAll(sa.backup.child);
    }
    /**
     * Registers a change, made during the static analysis, if
     * that change is meaningful.<br>
     * 
     * Only a single change can be registered per <code>s</code>.<br>
     * 
     * The child type dispatch code must be in sync with that in
     * <code>backup()</code>.
     * 
     * @param s                         statement whose child is replaced
     * @param t                         type of the replaced child
     * @param i                         index if <code>t</code> is
     *                                  <code>ELEMENT</code>, -1 otherwise
     * @param original                  the original, replaced child
     * @param replacement               the new child, used only to check
     *                                  if this is a meaningful change,
     *                                  that is, if <code>original</code> !=
     *                                  <code>replacement</code>
     */
    public void register(AbstractStatement s, ChildType t, int i,
            AbstractExpression original, AbstractExpression replacement) {
        if(!original.equals(replacement)) {
            int index = statement.indexOf(s);
            if(index != -1 && childType.get(index) == t) {
                if(t != ChildType.ELEMENT || childIndex.get(index) == i)
                    throw new RuntimeException("second meaningful change");
            }
            statement.add(s);
            childType.add(t);
            if(t == ChildType.ELEMENT) {
                if(i < 0)
                    throw new RuntimeException("invalid index");
            } else {
                if(i != -1)
                    throw new RuntimeException("unexpected index");
            }
            childIndex.add(i);
            child.add(original);
        }
    }
    /**
     * Restores the tree to the state from before the static analysis.<br>
     * 
     * The child type dispatch code must be in sync with that in
     * <code>register()</code>.
     */
    public void restore() {
        Iterator<ChildType> tI = childType.iterator();
        Iterator<Integer> iI = childIndex.iterator();
        Iterator<AbstractExpression> cI = child.iterator();
        for(AbstractStatement s : statement) {
            ChildType t = tI.next();
            int i = iI.next();
            AbstractExpression c = cI.next();
//if(c.toString().contains("DAYS"))
//    c = c;
            if(s instanceof BinaryExpression) {
                switch(t) {
                    case LEFT:
                        ((BinaryExpression)s).left = c;
                        break;

                    case RIGHT:
                        ((BinaryExpression)s).right = c;
                        break;

                    default:
                        throw new RuntimeException("invalid child type");
                }
            } else if(s instanceof AssignmentExpression) {
                switch(t) {
                    case RIGHT:
                        ((AssignmentExpression)s).rvalue = c;
                        break;

                    default:
                        throw new RuntimeException("invalid child type");
                }
            } else if(s instanceof CallExpression) {
                switch(t) {
                    case ELEMENT:
                        ((CallExpression)s).arg.set(i, c);
                        break;

                    default:
                        throw new RuntimeException("invalid child type");
                }
            } else if(s instanceof AllocationExpression) {
                switch(t) {
                    case ELEMENT:
                        ((AllocationExpression)s).expressions.set(i, c);
                        break;

                    default:
                        throw new RuntimeException("invalid child type");
                }
            } else if(s instanceof ReturnStatement) {
                switch(t) {
                    case SUB:
                        ((ReturnStatement)s).sub = c;
                        break;

                    default:
                        throw new RuntimeException("invalid child type");
                }
            } else
                throw new RuntimeException("unknown statement type");
        }
    }
}
