/*
 * RangeCodeStaticAnalysis.java
 *
 * Created on Nov 2, 2009, 2:17:35 PM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.sa;

import java.util.*;
import java.lang.ref.WeakReference;
import pl.gliwice.iitis.hedgeelleth.compiler.CompilerAnnotation;

import pl.gliwice.iitis.hedgeelleth.compiler.code.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.AbstractCodeDereference;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.CodeFieldDereference;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.CodeIndexDereference;
import pl.gliwice.iitis.hedgeelleth.compiler.code.op.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.optimizer.runtime.RuntimeStaticAnalysis;
import pl.gliwice.iitis.hedgeelleth.compiler.primitiverange.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Context;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Literal;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Type;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Variable;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.CompilerException;
import pl.gliwice.iitis.hedgeelleth.interpreter.RuntimeValue;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.*;
import pl.gliwice.iitis.hedgeelleth.interpreter.exception.InterpreterException;
import pl.gliwice.iitis.hedgeelleth.interpreter.op.CodeArithmetics;

/**
 * Tries to evaluate bounds on results of assignments,
 * unary and binary expressions, and, on basis of that, may
 * create proposals for code variables. The proposals should later be
 * used by the backend to shrink variable primitive ranges on variables.
 * Initial values of runtime fields are included in the proposals.<br>
 *
 * Can evaluate a new primary range only if all arguments of the
 * respective expression has its variable or primary ranges evaluated
 * as constants.<br>
 *
 * The analysis is done at the code level, as opposed to the tree level,
 * as possibly more ranges are evaluated as constants at that level.<br>
 *
 * Setting of field proposals is on the level of code variables, not runtime fields,
 * what may decrease efficiency if runtime fields, referred by the same field
 * code variable, would have variable ranges or initial values different within
 * each other.<br>
 *
 * For boolean types, it is assumed that the logical value of false is 0, and the
 * logical value of true is 1.<br>
 *
 * It is assumed that array classes are already converted to
 * <code>Type.ARRAY_QUALIFIED_CLASS_NAME</code>, and that none of
 * dereferences to fields points to
 * <code>CodeValuePossibility.UNKNOWN_VALUE</code>. It is assumed that
 * field references are never modified in the analyzed methods. It is
 * assumed that no allocations of arrays occur in the code.
 *
 * @author Artur Rataj
 */
public class RangeCodeStaticAnalysis {
    /**
     * A proposal of a variable primitive range.
     */
    public static class Proposal {
        /**
         * Minimum bound, null for a proposal without range defined
         * or for a proposal that represents an empty set.
         */
        public Literal min;
        /**
         * Maximum bound, null for a proposal without range defined
         * or for a proposal that represents an empty set.
         */
        public Literal max;
        /**
         * True for a proposal representing an empty set.
         */
        public boolean empty;

        /**
         * Creates a new instance of Proposal, with unspecified minimum and
         * maximum values. Such proposal means, that (1) if <code>empty<ode>
         * is false, no range is defined or (2) if <code>empty<ode> is true,
         * an empty proposal. By default, this constructor makes <code>empty<ode>
         * false.<br>
         *
         * A proposal with undefined range is for example because of an
         * assignment with unevaluated range of values.<br>
         */
        public Proposal() {
            min = null;
            max = null;
            empty = false;
        }
        /**
         * Creates a new instance of Proposal, with specified minimum and
         * maximum values. An empty set proposal can not be created with this
         * constructor.
         *
         * @param min                   minimum value
         * @param max                   maximum value
         */
        public Proposal(Literal min, Literal max) throws CompilerException {
            this();
            this.min = min;
            this.max = max;
//if(max.getMaxPrecisionInteger() == 11)
//    max = max;
            if(this.max.arithmeticLessThan(this.min))
                throw new CompilerException(null,
                        CompilerException.Code.INVALID,
                        "invalid range: " + toString());
        }
        /**
         * A copying constructor, or a constructor of an
         * empty set proposal.
         *
         * @param proposal              proposal to deep copy, or null
         *                              for a proposal representing an
         *                              empty set
         */
        public Proposal(Proposal proposal) {
            this();
            if(proposal == null)
                empty = true;
            else {
                if(proposal.min != null)
                    min = proposal.min.clone();
                if(proposal.max != null)
                    max = proposal.max.clone();
                empty = proposal.empty;
                if(rangeKnown() && !isEmpty() && max.arithmeticLessThan(this.min))
                    throw new RuntimeException("min > max");
            }
        }
        /**
         * If this proposal represents an empty set.
         * @return
         */
        public boolean isEmpty() {
            return empty;
        }
        /**
         * Returns if the range of values in this proposal is specified,
         * including an empty set.
         *
         * If the range is not known, this proposal represents "any possible"
         * range, being usually a whole range of a given type.
         * 
         * @return                      if range specified
         */
        public boolean rangeKnown() {
            return isEmpty() || (min != null && max != null);
        }
        /**
         * If this proposal fully overlaps some other proposal. That is, if any
         * values in the other proposal are present in this proposal.<br>
         *
         * Unknown range is treated as defined in <code>rangeKnown()</code>.
         * If both proposals have undefined ranges, this method assumes the
         * ranges are equal, and thus returns true.<br>
         *
         * One empty set is a superset of another empty set.
         *
         * @param other                 the other proposal
         * @return                      if this fully overlaps the other
         */
        public boolean isSuperset(Proposal other) {
            if(rangeKnown() && !other.rangeKnown())
                // some to all
                return false;
            if(!rangeKnown() && other.rangeKnown())
                // all to some
                return true;
            if(!rangeKnown() && !other.rangeKnown())
                // all to all
                return true;
            // both ranges known
            if(isEmpty() && !other.isEmpty())
                // none to some
                return false;
            if(!isEmpty() && other.isEmpty())
                // some to none
                return true;
            if(isEmpty() && other.isEmpty())
                // none to none
                return true;
            return (min.arithmeticEqualTo(other.min) || min.arithmeticLessThan(other.min)) &&
                    !max.arithmeticLessThan(other.max);
        }
        /**
         * If this proposal fully overlaps some other proposal. That is, if any
         * values in the other proposal are present in this proposal.<br>
         *
         * Unknown range is treated as defined in <code>rangeKnown()</code>.
         * If both proposals have undefined ranges, this method assumes the
         * ranges are equal, and thus returns true.<br>
         *
         * One empty set is a superset of another empty set.
         *
         * @param other                 the other proposal
         * @return                      if this fully overlaps the other
         */
        public boolean isEqual(Proposal other) {
            if(rangeKnown() != other.rangeKnown())
                // all to some or some to all
                return false;
            if(!rangeKnown() && !other.rangeKnown())
                // all to all
                return true;
            if(isEmpty() && other.isEmpty())
                // none to none
                return true;
            // some to some
            return min.arithmeticEqualTo(other.min) &&
                    max.arithmeticEqualTo(other.max);
        }
        /**
         * Creates a new <code>Proposal<code>, that has the range of this
         * proposal, extended to include a given value. If this proposal
         * does not have a known range, then so is for the new proposal.
         *
         * @param v                     value to include in the range of the
         *                              created proposal
         * @return                      a new proposal
         */
        public Proposal newExtendedToInclude(Literal v) {
            Proposal p = new Proposal(this);
            if(p.rangeKnown()) {
                if(p.isEmpty()) {
                    p.min = v.clone();
                    p.max = v.clone();
                    p.empty = false;
                } else if(v.arithmeticLessThan(p.min))
                    p.min = v.clone();
                else if(p.max.arithmeticLessThan(v))
                    p.max = v.clone();
            }
            return p;
        }
        /**
         * Returns the number of values covered by this proposal. The number
         * of states equals to the number of all allowed
         * values of the variable = (range_max - range_min + 1). The proposal
         * is assumed to have bounds of integer types.<br>
         *
         * This method is similar to
         * <code>RangeCodeStaticAnalysis.Proposal.getStatesNum()</code>.
         *
         * @return                          number of possible values
         */
        public long getStatesNum() {
            if(!min.type.isOfIntegerTypes() ||
                    !max.type.isOfIntegerTypes())
                throw new RuntimeException("bounds not integer");
            long minV = min.getMaxPrecisionInteger();
            long maxV = max.getMaxPrecisionInteger();
            return maxV - minV + 1;
        }
        @Override
        public String toString() {
            String s;
            if(rangeKnown()) {
                if(isEmpty())
                    s = "∅";
                else
                    s = PrimitiveRangeDescription.VARIABLE_LEFT_STRING +
                        min.toString() + ", " + max.toString() +
                        PrimitiveRangeDescription.VARIABLE_RIGHT_STRING;
            } else
                s = "?";
           return "[" + s + "]";
        }
    };
    /**
     * Runtime static analysis, to obtain methods and their traces.
     */
    protected RuntimeStaticAnalysis sa;
    /**
     * Map of proposals regarding fields, in the current iteration.
     */
    protected Map<CodeVariable, Collection<Proposal>> fieldProposals;
    /**
     * Map of proposals regarding stores, in the current iteration.
     */
    protected Map<ArrayStore, Collection<Proposal>> arrayStoreProposals;
    /**
     * Number of final iteration, -1 for unknown.
     */
    int finalIteration;
    /**
     * Proposals in subsequent iterations, of both locals and fields.<br>
     *
     * The method <code>modifyVariableRanges()</code> puts data here
     * for current iteration, to be used in another iteration.
     */
    protected List<Map<CodeVariable, Proposal>> iterativeProposals;
    /**
     * Proposals in subsequent iterations, of array stores.
     */
    protected List<Map<ArrayStore, Proposal>> iterativeStoreProposals;

    /**
     * Creates a new instance of RangeCodeStaticAnalysis. It can perform
     * the analysis of both locals and fields.<br>
     *
     * It is assumed that no variable in the compilation has any proposals
     * before this object is constructed.
     *
     * @param sa                        method runtimes and their traces; defines a
     *                                  a collection of methods to analyze; if
     *                                  <code>analyzeFields</code> is true, it usually
     *                                  contains <i>complete</i> code that can <i>still</i> be
     *                                  executed
     * @param analyzeFields             true for analysis of fields, can be true
     *                                  only if <code>methods</code> contain
     *                                  all methods that will possibly write to the
     *                                  fields, otherwise the fields' ranges might
     *                                  be wrongly evaluated by not taking into
     *                                  account all possible writings
     */
    public RangeCodeStaticAnalysis(RuntimeStaticAnalysis sa, boolean analyzeFields)
            /*throws TranslatorException*/ {
        this.sa = sa;
        // iteratively find the proposals
        iterativeProposals = new ArrayList<>();
        iterativeStoreProposals = new ArrayList<>();
        finalIteration = -1;
        int iteration = -1;
        do {
            ++iteration;
            iterativeProposals.add(new HashMap<CodeVariable, Proposal>());
            iterativeStoreProposals.add(new HashMap<ArrayStore, Proposal>());
            analyzeMethods(analyzeFields, iteration);
            if(analyzeFields)
                modifyVariableRanges(fieldProposals, iteration);
        } while(analyzeFields && iterationShrunk(iteration, true));
        // write the proposals to variables
        ++iteration;
        finalIteration = iteration;
        analyzeMethods(analyzeFields, iteration);
        if(analyzeFields)
            modifyVariableRanges(fieldProposals, iteration);
    }
    /**
     * If any new proposal have been added in this iteration, or any proposals
     * have been shrunk in this iteration.<br>
     *
     * If some proposal, that was present in the previous iteration, in this
     * iteration is absent or has any additional values, a runtime exception is
     * throws.
     *
     * @param currNum                   number of current iteration, 0 for first
     *                                  iteration
     * @param checkFields               if to analyze fields as well; do not set to
     *                                  true if the previous iteration analyzed field
     *                                  proposals and this iteration did not do that
     *                                  yet, as it may cause a runtime exception
     * @return                          if this iteration added/shrunk any range in
     *                                  any of analyzed locals or fields
     */
    protected boolean iterationShrunk(int currNum, boolean checkFields) {
        Map<CodeVariable, Proposal> prev;
        if(currNum == 0)
            // no previous iteration, use an empty set
            prev = new HashMap<>();
        else
            prev = iterativeProposals.get(currNum - 1);
        Map<CodeVariable, Proposal> curr = iterativeProposals.get(currNum);
        // check for illegal changes
        for(CodeVariable cv : prev.keySet()) {
            if(!checkFields && !cv.isLocal())
                continue;
            Proposal prevP = prev.get(cv);
            Proposal currP = curr.get(cv);
            if(currP == null || !prevP.isSuperset(currP))
                throw new RuntimeException("unexpected modification");
        }
        // check for new proposals or shrinks
        for(CodeVariable cv : curr.keySet()) {
            if(!checkFields && !cv.isLocal())
                continue;
            Proposal prevP = prev.get(cv);
            Proposal currP = curr.get(cv);
            if(prevP == null || !currP.isSuperset(prevP))
                return true;
        }
        // no changes in variables, so check stores
        Map<ArrayStore, Proposal> prevStores;
        if(currNum == 0)
            // no previous iteration, use an empty set
            prevStores = new HashMap<>();
        else
            prevStores = iterativeStoreProposals.get(currNum - 1);
        Map<ArrayStore, Proposal> currStores = iterativeStoreProposals.get(currNum);
        // check for illegal changes
        for(ArrayStore a : prevStores.keySet()) {
            Proposal prevP = prevStores.get(a);
            Proposal currP = currStores.get(a);
            if(currP == null || !prevP.isSuperset(currP))
                throw new RuntimeException("unexpected modification");
        }
        // check for new proposals or shrinks
        for(ArrayStore a : currStores.keySet()) {
            Proposal prevP = prevStores.get(a);
            Proposal currP = currStores.get(a);
            if(prevP == null || !currP.isSuperset(prevP))
                return true;
        }
        return false;
    }
    /**
     * If type is suitable for range analysis. True for primitives and
     * arrays of primitives.<br>
     *
     * //The parameter <code>indexing</code> set to true denotes, that the
     * //type origins from an indexing dereference. Such a case makes analyzable
     * //also array types. An array type in a field dereference means simply
     * //assignment to a reference variable, and not to array contents, thus,
     * //is not arithmetical, thus, not analyzable.
     *
     * @param t                         type
     * @param indexing                  if the type relates to an indexed target
     * @return                          if suitable for range analysis
     */
    protected boolean typeAnalyzable(Type t) {
        return t.isPrimitive() ||
                (t.isArray(null) && t.getElementType(null, 0).isPrimitive());
    }
    /**
     * All array stores never written to within the code, have the proposals
     * set to their initial values in this method.
     */
    protected void completeStoreProposals() {
        for(WeakReference<RuntimeObject> r : sa.getInterpreter().getObjects()) {
            RuntimeObject o = r.get();
            if(o != null) {
                if(o instanceof ArrayStore && typeAnalyzable(o.type)) {
                    ArrayStore a = (ArrayStore)o;
                    Collection<Proposal> proposals = arrayStoreProposals.get(a);
                    if(proposals == null) {
                        Proposal empty = new Proposal();
                        empty.empty = true;
                        Proposal p = extendToIncludeStoreValues(empty, a);
                        Collection<Proposal> list = new LinkedList<>();
                        list.add(p);
                        arrayStoreProposals.put(a, list);
                    }
                }
            }
        }
    }
    /**
     * Analyzes a set of methods. Creates a new set of <code>fieldProposals</code>
     * every time, discarding possible previous set. After analysis by calling
     * <code>analyze(RuntimeMethod, int)</code> for all methods, calls
     * <code>completeFieldProposals()</code>.
     *
     * @param iteration                 iteration number, <code>finalIteration</code>
     *                                  for final pass, where actual variable
     *                                  proposals are modified
     * @return                          if anything has been modified
     */
    protected void analyzeMethods(boolean analyzeFields, int iteration) {
        if(analyzeFields)
            fieldProposals = new HashMap<>();
        arrayStoreProposals = new HashMap<>();
        for(RuntimeMethod rm : sa.getMethods())
            analyze(rm, iteration);
        // store proposals are needed only for possible next iteration
        if(iteration != finalIteration) {
            completeStoreProposals();
            if(!sa.getMethods().isEmpty())
                // <code>rm</code> is not needed, as bounds
                storeArrayStoreRanges(sa.getMethods().get(0), arrayStoreProposals, iteration);
        }
    }
    /**
     * Analyzes a method. Can add or modify variable primitive range
     * proposals. Deletes any existing proposals within locals of this
     * method.<br>
     *
     * Called iteratively, until stability is reached. It assures propagation
     * of proposals.
     *
     * @param method                    method to analyze
     * @param iteration                 iteration number, <code>finalIteration</code>
     *                                  for final pass, where actual variable
     *                                  proposals are modified
     */
    public void analyze(RuntimeMethod rm, int iteration) {
        try {
            CodeMethod method = rm.method;
            boolean stable = true;
            Map<CodeVariable, Collection<Proposal>> localProposals =
                    new HashMap<>();
            int index = 0;
            for(AbstractCodeOp op : method.code.ops) {
                Map<CodeVariable, Proposal> map;
                if(op instanceof CodeOpUnaryExpression) {
                    CodeOpUnaryExpression ue = (CodeOpUnaryExpression)op;
                    Type t = ue.result.getTypeSourceVariable().type;
                    if(typeAnalyzable(t/*, ue.result instanceof CodeIndexDereference*/))
                        map = analyze(rm, index, (CodeOpUnaryExpression)op, iteration);
                    else
                        map = null;
                } else if(op instanceof CodeOpBinaryExpression) {
//if(((CodeOpBinaryExpression)op).op == AbstractCodeOp.Op.BINARY_DIVIDE)
//        op = op;
                    map = analyze(rm, index, (CodeOpBinaryExpression)op, iteration);
    // if(!map.values().iterator().next().rangeKnown() && op.toString().indexOf("c5") != -1)
    //     map = analyze(rm, index, (CodeOpBinaryExpression)op, iteration);
                } else if(op instanceof CodeOpAssignment) {
                    CodeOpAssignment a = (CodeOpAssignment)op;
                    Type t = a.result.getTypeSourceVariable().type;
                    if(typeAnalyzable(t/*, a.result instanceof CodeIndexDereference*/))
                        map = analyze(rm, index, a, iteration);
                    else
                        map = null;
                } else if(op instanceof CodeOpNone) {
                    CodeOpNone n = (CodeOpNone)op;
                    if(n.result != null) {
                        Type t = n.result.getTypeSourceVariable().type;
                        if(typeAnalyzable(t/*, a.result instanceof CodeIndexDereference*/))
                            map = analyze(rm, index, n, iteration);
                        else
                            map = null;
                    } else
                        map = null;
                } else if(op instanceof CodeOpAllocation &&
                        op.getResult().getTypeSourceVariable().type.isArray(null))
                    throw new RuntimeException("no allocation of arrays allowed");
                else
                    map = null;
                if(map != null && !map.isEmpty()) {
                    if(map.size() != 1)
                        throw new RuntimeException("only one mapping expected");
                    CodeVariable v = map.keySet().iterator().next();
                    Proposal p = map.get(v);
                    Collection<Proposal> set = localProposals.get(v);
                    if(set == null) {
                        set = new LinkedList<>();
                        localProposals.put(v, set);
                    }
                    set.add(p);
                }
                ++index;
            }
            modifyVariableRanges(localProposals, iteration);
        } catch(CompilerException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    /**
     * Computes range on basis of a list of proposals. The output range
     * covers ranges in all of the input proposals. If at least one of the
     * proposals has false <code>rangeKnown()</code>, that is, represents assignment with
     * unspecified range, this method returns null.<br>
     *
     * The type may be integer types, boolean type with assumption described
     * in <code>Proposal</code>, or floating point type.
     * 
     * @param type                      type of the variable, decides
     *                                  about the type of resulting range
     * @param proposals                 proposals
     * @return                          resulting range, null if unknown;
     *                                  can be empty; for no input proposals,
     *                                  is always empty; if not null, range
     *                                  is known
     */
    Proposal computeRange(Type type, Collection<Proposal> set) {
        try {
            Proposal out = null;
            if(type.isArray(null))
                type = type.getElementType(null, 0);
            if(type.isOfIntegerTypes() || type.isBoolean()) {
                long min = -1;
                long max = -1;
                boolean first = true;
                for(Proposal p : set) {
                    if(!p.rangeKnown())
                        return null;
                    if(p.isEmpty())
                        // empty set won't enlarge the range
                        continue;
                    if(!p.min.type.isOfIntegerTypes() ||
                            !p.max.type.isOfIntegerTypes())
                        throw new RuntimeException("type mismatch");
                    long currMin = p.min.getMaxPrecisionInteger();
                    long currMax = p.max.getMaxPrecisionInteger();
                    if(first) {
                        min = currMin;
                        max = currMax;
                        first = false;
                    } else {
                        if(min > currMin)
                            min = currMin;
                        if(max < currMax)
                            max = currMax;
                    }
                }
                if(!first)
                    out = new Proposal(new Literal(min), new Literal(max));
                else
                    // no input proposals, then output proposal in an empty set
                    out = new Proposal(null);
            } else if(type.isOfFloatingPointTypes()) {
                double min = -1;
                double max = -1;
                boolean first = true;
                for(Proposal p : set) {
                    if(!p.rangeKnown())
                        return null;
                    if(p.isEmpty())
                        // empty set won't enlarge the range
                        continue;
                    /*
                     * floating--point variables allow for both integer and fp
                     * ranges
                    if(!p.min.type.isOfFloatingPointTypes() ||
                            !p.max.type.isOfFloatingPointTypes())
                        throw new RuntimeException("type mismatch");
                        */
                    double currMin = p.min.getMaxPrecisionFloatingPoint();
                    double currMax = p.max.getMaxPrecisionFloatingPoint();
                    if(first) {
                        min = currMin;
                        max = currMax;
                        first = false;
                    } else {
                        if(min > currMin)
                            min = currMin;
                        if(max < currMax)
                            max = currMax;
                    }
                }
                if(!first)
                    out = new Proposal(new Literal(min), new Literal(max));
                else
                    // no input proposals, then output proposal in an empty set
                    out = new Proposal(null);
            } else
                throw new RuntimeException("invalid type");
            return out;
        } catch(CompilerException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    /**
     * Modifies variable primitive ranges, according to given proposals.
     * The proposals may concern both local variables and field variables.<br>
     * 
     * In the case of a local, if the local is also an argument, its range won't
     * be modified, as it is assumed, that nothing was known about the argument's
     * possible values when constructing the proposals.<br>
     *
     * //In the case of a field, the current value of the field, that is, usually
     * //the field's initial value, is taken into account by adding a proposal that
     * //covers the initial value or values. The collection <code>proposals</code>
     * //is not modified, though.<br>
     *
     * If a variable already had some proposals, and they are to be modified by this
     * method, then the original proposals are obeyed, that is, the
     * new proposals wil always overlap the previous ones.<br>
     *
     * Note that, in the case of a field, all methods that use a given field
     * should usually be taken into account when constructing the proposals
     * given to this method.
     * 
     * @param proposals                 proposals
     * @param iteration                 iteration number, <code>finalIteration</code>
     *                                  for final pass, where actual variable
     *                                  proposals are modified
     */
    protected void modifyVariableRanges(Map<CodeVariable, Collection<Proposal>> proposals,
            int iteration) {
        for(CodeVariable v : proposals.keySet()) {
            CodeMethod methodOwner;
            if(v.isLocal()) {
                methodOwner = (CodeMethod)v.owner;
            } else {
                methodOwner = null;
            }
            boolean ok = true;
            if(methodOwner != null &&
                    methodOwner.interactsExternally(v))
                ok = false;
            if(ok) {
                Proposal proposed = computeRange(v.type, proposals.get(v));
                if(proposed != null) {
                    RangeCodeStaticAnalysis.Proposal original;
                    if(iteration == finalIteration)
                        // obey the original proposal
                        original = v.proposal;
                    else
                        // the variable's proposal is not modified at this time, but
                        // instead, <code>iterativeProposals</code> are modified,
                        // so no need to bother with the variable's original proposal
                        // yet
                        original = null;
                    if(original == null || original.rangeKnown()) {
                        if(original == null) {
                            if(proposed.rangeKnown()) {
                                original = new Proposal(proposed);
                                if(iteration == finalIteration)
                                    v.proposal = original;
                                else
                                    iterativeProposals.get(iteration).put(v, original);
                            } else
                                throw new RuntimeException("unknown range in tightest proposal");
                        } else {
                            // enlarge the range if needed
                            if(original.isEmpty()) {
                                original.min = proposed.min;
                                original.max = proposed.max;
                            } else {
                                // optimisations like merging of variables may cause
                                // loosening of proposals, as their resulting range might
                                // be wider
                                if(!original.min.arithmeticEqualTo(proposed.min) &&
                                        original.min.arithmeticLessThan(proposed.min)) {
                                    original.min = proposed.min;
                                }
                                if(!original.max.arithmeticEqualTo(proposed.max) &&
                                        !original.max.arithmeticLessThan(proposed.max)) {
                                    original.max = proposed.max;
                                }
                            }
                        }
                    }
                } else {
                    // report that assignment with unknown range has been found
                    Proposal unknown = new Proposal();
                    if(iteration == finalIteration)
                        v.proposal = unknown;
                    else
                        iterativeProposals.get(iteration).put(v, unknown);
                }
            }
        }
    }
    /**
     * Stores proposals for array stores for a given iteration.<br>
     *
     * The proposals are extended to include initial values within the stores.<br>
     *
     * As these proposals are usable only in possible next iteration, a
     * runtime exception is throw if <code>iteration == finalIteration</code>.
     *
     * @param rm                        runtime method
     * @param proposals                 map of proposals
     * @param iteration                 current iteration
     */
    protected void storeArrayStoreRanges(RuntimeMethod rm,
            Map<ArrayStore, Collection<Proposal>> proposals, int iteration) {
        if(iteration == finalIteration)
            throw new RuntimeException("final iteration");
        for(ArrayStore a : proposals.keySet()) {
            Proposal proposed = computeRange(a.type, proposals.get(a));
            if(proposed != null) {
                proposed = extendToIncludeStoreValues(proposed, a);
                iterativeStoreProposals.get(iteration).put(a, proposed);
            }
        }
    }
    /**
     * Returns the tightest bound evaluation of a ranged value. Can evaluate
     * only integer or floating point values or arrays of these, being
     * constant or having ranges reduced to constants.<br>
     *
     * It takes into account (1) variable primitive range, (2) primary
     * primitive range and (3) iterative proposals made by this object.<br>
     * 
     * If a constant bound is found, a respective value in <code>v</code> is
     * updated.
     *
     * @param rm                        runtime method, to evaluate variable
     *                                  primitive ranges of fields
     * @param index                     index of the current operation within
     *                                  method
     * @param v                         ranged value
     * @param iteration                 iteration num
     * @return                          tightest bound evaluation, null for
     *                                  unknown or not applicable
     */
    protected Proposal getTightest(RuntimeMethod rm, int index,
            RangedCodeValue v, int iteration) throws InterpreterException {
        try {
            Literal min = null;
            Literal max = null;
            Type t = v.value.getResultType();
            if(t.isArray(null)) {
                /*
                if(!(v.value instanceof CodeConstant) &&
                        !(v.value instanceof CodeIndexDereference))
                    throw new RuntimeException("analyzable array must be a constant or " +
                            "in indexing dereference");
                 */
                t = t.getElementType(null, 0);
            }
            if(!t.isOfIntegerTypes() &&
                    !t.isOfFloatingPointTypes())
                return null;
            else {
                if(v.value instanceof CodeConstant) {
                    Literal l = ((CodeConstant)v.value).value;
                    if(l instanceof RuntimeValue &&
                            ((RuntimeValue)l).type.isJava()) {
                        // if <code>t</code> is analyzable and the constant's type
                        // is java, it must be an array store
                        ArrayStore a = (ArrayStore)((RuntimeValue)l).getReferencedObject();
                        if(iteration > 0) {
                            Proposal i = iterativeStoreProposals.get(iteration - 1).get(a);
                            if(i != null && i.rangeKnown()) {
                                if(i.isEmpty()) {
                                    // this will produce an empty proposal
                                    min = new Literal(0);
                                    max = new Literal(-1);
                                } else {
                                    min = i.min;
                                    max = i.max;
                                }
                            } else
                                // no proposals for this store in previous iterations
                                // with known ranges
                                return null;
                        } else
                            // no previous iteration to get possible proposals
                            return null;
                    } else if(l.type.isAnnotation()) {
                        Type.TypeRange range = l.getAnnotationValue().
                                range;
                        min = range.getMinLiteral();
                        max = range.getMaxLiteral();
                    } else
                        max = min = l;
                } else {
                    CodeVariable object = ((AbstractCodeDereference)v.value).getContainerVariable();
                    // locals can have their ranges reduced to constants, but fields
                    // always have dereferences in the ranges, the referenced range
                    // variables are just guaranteed to be constant
                    CodeVariablePrimitiveRange vr = ((AbstractCodeDereference)v.value).
                            getTypeVariablePrimitiveRange();
                    if(vr != null) {
                        min = vr.evaluateMin(sa, rm, index, object);
                        max = vr.evaluateMax(sa, rm, index, object);
                    }
                    if(v.value instanceof CodeIndexDereference) {
                        CodeIndexDereference i = (CodeIndexDereference)v.value;
                        ArrayStore as = sa.getArrayStore(rm, index, i);
                        if(iteration > 0) {
                            Proposal p = iterativeStoreProposals.get(iteration - 1).get(as);
                            if(p != null && p.rangeKnown()) {
                                if(p.isEmpty()) {
                                    // this will produce an empty proposal
                                    min = new Literal(0);
                                    max = new Literal(-1);
                                } else {
                                    // tighten
                                    if(min == null || min.arithmeticLessThan(p.min))
                                        min = p.min;
                                    if(max == null || !max.arithmeticLessThan(p.max))
                                        max = p.max;
                                }
                            }
                        }
                    }
                    if(v.range != null) {
                        CodePrimaryPrimitiveRange pr = v.range;
                        if(pr.min instanceof CodeConstant) {
                            Literal pMin = ((CodeConstant)pr.min).value;
                            if(min == null || min.arithmeticLessThan(pMin))
                                min = pMin;
                        }
                        if(pr.max instanceof CodeConstant) {
                            Literal pMax = ((CodeConstant)pr.max).value;
                            if(max == null || !max.arithmeticLessThan(pMax))
                                max = pMax;
                        }

                    }
                    if(iteration > 0) {
                        // check for proposals in previous iteration
                        Proposal i = iterativeProposals.get(iteration - 1).get(
                                ((AbstractCodeDereference)v.value).getTypeSourceVariable());
                        if(i != null) {
                            if(i.isEmpty()) {
                                // this will produce an empty proposal
                                min = new Literal(0);
                                max = new Literal(-1);
                            } else {
                                if(i.min != null)
                                    if(min == null || min.arithmeticLessThan(i.min))
                                        min = i.min;
                                if(i.max != null)
                                    if(max == null || !max.arithmeticLessThan(i.max))
                                        max = i.max;
                            }
                        }
                    }
                }
                Proposal out;
                if(min != null && max != null) {
                    if(!max.arithmeticLessThan(min))
                        out = new Proposal(min, max);
                    else
                        out = new Proposal(null);
                } else
                    out = null;
                return out;
            }
        } catch(CompilerException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
}
    /**
     * Store a proposal for this array store. Only for actual writings to the array
     * contents, that is, when the result of an operation is
     * <code>CodeIndexDereference</code>.
     *
     * @param s                         array store
     * @param p                         proposal
     * @param iteration                 current iteration
     */
    protected void registerArrayContentsWrite(ArrayStore s, Proposal p) {
        Collection<Proposal> list = arrayStoreProposals.get(s);
        if(list == null) {
            list = new LinkedList<>();
            arrayStoreProposals.put(s, list);
        }
        list.add(p);
    }
    /**
     * Adds a proposal to local proposals.<br>
     *
     * Fields are initialized within the code, instead of having initial values,
     * so unlike in <code>addToFieldProposals</code>, no proposal is extended.<br>
     *
     * If a local unambiguously points to an array, then a proposal for the
     * respective array store is registered as well. If a local ambiguously
     * points to an array, a runtime exception is thrown.
     *
     * @param localProposals            local proposals
     * @param rm                        runtime method, to evaluate initial values
     *                                  of locals
     * @param index                     index of the operation within method, for
     *                                  which <code>v</code>'s runtime value is
     *                                  evaluated
     * @param v                         variable
     * @param p                         proposal
     * @param iteration                 iteration number
     */
    protected void addToLocalProposals(Map<CodeVariable, Proposal> localProposals,
            RuntimeMethod rm, int index, CodeVariable v, Proposal p, int iteration) {
        if(v.type.isArray(null)) {
            if(rm.method.code.ops.get(index).getResult() instanceof CodeIndexDereference) {
                AbstractCodeValue target = sa.getTrace(rm).getSingleM1(index - 1,
                        new CodeFieldDereference(v));
                if(target == null || !(target instanceof CodeConstant))
                    throw new RuntimeException("ambiguous reference");
                ArrayStore store = (ArrayStore)((RuntimeValue)((CodeConstant)target).value).
                        getReferencedObject();
                registerArrayContentsWrite(store, p);
            }
        }
        localProposals.put(v, p);
    }
    /**
     * Extends a proposal to include values of some array.
     *
     * @param original                  original proposal, it is left unchanged
     * @param store                     store with the values to include
     * @return                          extended proposal
     */
    protected Proposal extendToIncludeStoreValues(Proposal original, ArrayStore store) {
        Proposal p = original;
        for(int elementIndex = 0; elementIndex < store.getLength(); ++elementIndex) {
            Literal elementValue = store.getElementUnchecked(
                    sa.getInterpreter(), null, null, elementIndex);
            p = p.newExtendedToInclude(elementValue);
        }
        return p;
    }
    /**
     * Adds a proposal to field proposals.<br>
     *
     * In the case of a field, the proposal's range is extended to include the
     * respective runtime field's values. This way, this class' output
     * proposals always take into account these values, which usually are treated
     * as initial values by the translator.<br>
     *
     * If a field points to an array, and <code>fieldReferencesGuaranteedConstant</code>
     * is true, then a proposal for the respective array store is registered as well.
     *
     * @param rm                        runtime method, to evaluate initial values
     *                                  of fields
     * @param index                     index of the operation within method, after
     *                                  which <code>d</code>'s runtime value is
     *                                  evaluated
     * @param d                         dereference pointing to the field
     * @param p                         proposal
     * @param iteration                 iteration number
     */
    protected void addToFieldProposals(RuntimeMethod rm, int index,
            AbstractCodeDereference d, Proposal p, int iteration)
            throws InterpreterException {
        CodeVariable object;
/*if(index == 17 && d.toString().indexOf("::long f") != -1)
    index = index;*/
        Set<RuntimeField> fields = sa.getRuntimeFields(rm, index,
                d.extractTypeSourceFieldDereference());
        if(fields.isEmpty())
// {
// sa.getRuntimeFields(rm, index,
//                 d.extractTypeSourceFieldDereference());
            throw new InterpreterException(rm.method.code.ops.get(index).getStreamPos(),
                    "ambiguos dereference " + d.toNameString());
//            throw new RuntimeException("no runtime field found, perhaps an uninitialized local or"
//                    + " ambiguous dereference that should have already be solved but is not");
// }
//        if(fields.size() > 1)
//            throw new InterpreterException(rm .method.code.ops.get(index).getStreamPos(),
//                    "ambiguos dereference " + d.toNameString());
        for(RuntimeField rf : fields) {
            Literal value = sa.findValueOfRuntimeField(rf).value;
            if(value.type.isBoolean())
                if(value.getBoolean())
                    value = new Literal(1);
                else
                    value = new Literal(0);
            if(rf.field.type.isArray(null)) {
                // a number of initial values
                // an array constant means a runtime value
                ArrayStore store = (ArrayStore)((RuntimeValue)value).getReferencedObject();
                p = extendToIncludeStoreValues(p, store);
                if(rm.method.code.ops.get(index).getResult() instanceof CodeIndexDereference)
                    registerArrayContentsWrite(store, p);
            } else {
                p = p.newExtendedToInclude(value);
            }
        }
        CodeVariable v = d.getTypeSourceVariable();
        Collection<Proposal> set = fieldProposals.get(v);
        if(set == null) {
            set = new LinkedList<>();
            fieldProposals.put(v, set);
        }
        set.add(p);
    }
    /**
     * Analyzes an unary expression. Can return a proposal
     * to the result variable if (1) the result is local,
     * (2) the result is a field and <code>fieldProposals</code>
     * is not null.
     *
     * @param rm                        runtime method, to evaluate variable
     *                                  primitive ranges and initial values of
     *                                  fields
     * @param index                     index of <code>ue</code> within method
     * @param ue                        unary expression to analyze
     * @param iteration                 iteration number, <code>finalIteration</code>
     *                                  for final pass, where actual variable
     *                                  proposals are modified
     * @return                          proposals for locals, empty set for none
     */
    protected Map<CodeVariable, Proposal> analyze(RuntimeMethod rm, int index,
            CodeOpUnaryExpression ue, int iteration) throws CompilerException {
        try {
            Map<CodeVariable, Proposal> localProposals = new HashMap<>();
            AbstractCodeDereference d = ue.result;
            CodeVariable v = d.getTypeSourceVariable();
            Proposal sub = getTightest(rm, index, ue.sub, iteration);
            Proposal out = null;
            if(sub != null) {
                if(sub.isEmpty())
                    // this expression won't ever be realized
                    out = new Proposal(null);
                else {
                    switch(ue.op) {
                        case UNARY_CONDITIONAL_NEGATION:
                            if(sub.min != sub.max)
                                out = sub;
                            else if(sub.min.getBoolean())
                                out = new Proposal(new Literal(0), new Literal(0));
                            else
                                out = new Proposal(new Literal(1), new Literal(1));
                            break;

                        case UNARY_NEGATION:
                        case UNARY_ABS:
                        {
                            CodeOpUnaryExpression e = new CodeOpUnaryExpression(
                                    ue.op,
                                    null, null);
                            try {
                                e.sub = new RangedCodeValue(new CodeConstant(null, sub.max));
                                Literal outMin = CodeArithmetics.computeValue(e, null);
                                e.sub = new RangedCodeValue(new CodeConstant(null, sub.min));
                                Literal outMax = CodeArithmetics.computeValue(e, null);
                                out = new Proposal(outMin, outMax);
                            } catch(InterpreterException f) {
                                throw new RuntimeException("unexpected exception: " + f.toString());
                            }
                            break;
                        }
                        case UNARY_CAST:
                            out = new Proposal(
                                    new Literal((int)sub.min.getMaxPrecisionFloatingPoint()),
                                    new Literal((int)sub.max.getMaxPrecisionFloatingPoint()));
                            break;
                            
                        default:
                            // this operator is not analysed
                            out = new Proposal();
                            break;
                    }
                }
            } else
                out = new Proposal();
            if(out != null)
                if(v.isLocal())
                    addToLocalProposals(localProposals, rm, index, v, out, iteration);
                else if(ue.result instanceof CodeFieldDereference)
                    addToFieldProposals(rm, index, d, out, iteration);
            return localProposals;
        } catch(InterpreterException e) {
            throw new CompilerException(e.getStreamPos(),
                    CompilerException.Code.ILLEGAL,
                    e.getMessage());
        }
    }
    /* REPLACED BY Literal.abs()
     *
     * Computes an absolute value of a literal.
     * 
     * @param value                     argument
     * @return                          absolute value of the argument
     */
    /*
    protected Literal abs(Literal value) throws InterpreterException {
            CodeOpUnaryExpression n = new CodeOpUnaryExpression(
                    AbstractCodeOp.Op.UNARY_NEGATION,
                    null, null);
            n.sub = new RangedCodeValue(new CodeConstant(null, value));
            Literal negatedValue = CodeArithmetics.computeValue(n, null);
            return Literal.max(value, negatedValue);
    }
     */
    /**
     * Analyzes a binary expression. Can return a proposal
     * to the result variable if (1) the result is local,
     * (2) the result is a field and <code>fieldProposals</code>
     * is not null.
     *
     * @param rm                        runtime method, to evaluate variable
     *                                  primitive ranges  and initial values of
     *                                  fields
     * @param index                     index of <code>be</code> within method
     * @param be                        binary expression to analyze
     * @param iteration                 iteration number, <code>finalIteration</code>
     *                                  for final pass, where actual variable
     *                                  proposals are modified
     * @return                          proposals for locals; if some local
     *                                  is assigned with value of unknown range,
     *                                  a proposal with its <code>rangeKnown()</code>
     *                                  being false is added
     */
    protected Map<CodeVariable, Proposal> analyze(RuntimeMethod rm, int index,
            CodeOpBinaryExpression be, int iteration) throws CompilerException {
        try {
            Map<CodeVariable, Proposal> localProposals = new HashMap<>();
            AbstractCodeDereference d = be.result;
/*if(d.toString().indexOf("G_#c6") != -1)
    d = d;*/
            CodeVariable v = d.getTypeSourceVariable();
/*if(be.op == CodeOpBinaryExpression.Op.BINARY_MODULUS)
    be = be;*/
            Proposal left = getTightest(rm, index, be.left, iteration);
            Proposal right = getTightest(rm, index, be.right, iteration);
            Proposal out = null;
            if(// left operand of a modulus operator has no meaning when computing
               // the proposal
                    (left != null || be.op == CodeOpBinaryExpression.Op.BINARY_MODULUS) &&
                    right != null) {
                if((left != null && left.isEmpty()) || right.isEmpty())
                    // this expression won't ever be realized
                    out = new Proposal(null);
                else {
                    switch(be.op) {
                        case BINARY_PLUS:
                        case BINARY_MINUS:
                        {
                            CodeOpBinaryExpression e = new CodeOpBinaryExpression(
                                    AbstractCodeOp.Op.BINARY_PLUS,
                                    null);
                            try {
                                Literal rightMin;
                                Literal rightMax;
                                switch(be.op) {
                                    case BINARY_PLUS:
                                        rightMin = right.min;
                                        rightMax = right.max;
                                        break;

                                    case BINARY_MINUS:
                                        CodeOpUnaryExpression n = new CodeOpUnaryExpression(
                                                AbstractCodeOp.Op.UNARY_NEGATION,
                                                null, null);
                                        n.sub = new RangedCodeValue(new CodeConstant(null, right.min));
                                        rightMax = CodeArithmetics.computeValue(n, null);
                                        n.sub = new RangedCodeValue(new CodeConstant(null, right.max));
                                        rightMin = CodeArithmetics.computeValue(n, null);
                                        break;

                                    default:
                                        throw new RuntimeException("unexpected operator");
                                }
                                e.left = new RangedCodeValue(new CodeConstant(null, left.min));
                                e.right = new RangedCodeValue(new CodeConstant(null, rightMin));
                                Literal outMin = CodeArithmetics.computeValue(e);
                                e.left = new RangedCodeValue(new CodeConstant(null, left.max));
                                e.right = new RangedCodeValue(new CodeConstant(null, rightMax));
                                Literal outMax = CodeArithmetics.computeValue(e);
                                out = new Proposal(outMin, outMax);
                            } catch(InterpreterException f) {
                                throw new RuntimeException("unexpected exception: " + f.toString());
                            }
                            break;
                        }
                        case BINARY_MULTIPLY:
                        case BINARY_DIVIDE:
                        {
                            CodeOpBinaryExpression e = new CodeOpBinaryExpression(
                                    be.op,
                                    null);
                            try {
                                boolean divisionByZero = false;
                                if(be.op == AbstractCodeOp.Op.BINARY_DIVIDE) {
                                    if(right.min.arithmeticEqualTo(new Literal(0)) ||
                                            right.max.arithmeticEqualTo(new Literal(0)))
                                        divisionByZero = true;
                                    else {
                                        if(right.min.arithmeticLessThan(new Literal(0)) !=
                                                right.max.arithmeticLessThan(new Literal(0)))
                                            // none is 0, so they must contain 0
                                            divisionByZero = true;
                                        else {
                                            // both are negative or both are positive
                                            // do nothing, as the value closer to 0 is
                                            // already in either <code>right.min</code> or
                                            // <code>right.max</code>
                                        }
                                    }
                                }
                                if(divisionByZero)
                                    out = new Proposal();
                                else {
                                    e.left = new RangedCodeValue(new CodeConstant(null, left.min));
                                    e.right = new RangedCodeValue(new CodeConstant(null, right.min));
                                    Literal outmm = CodeArithmetics.computeValue(e);
                                    e.left = new RangedCodeValue(new CodeConstant(null, left.min));
                                    e.right = new RangedCodeValue(new CodeConstant(null, right.max));
                                    Literal outmM = CodeArithmetics.computeValue(e);
                                    e.left = new RangedCodeValue(new CodeConstant(null, left.max));
                                    e.right = new RangedCodeValue(new CodeConstant(null, right.min));
                                    Literal outMm = CodeArithmetics.computeValue(e);
                                    e.left = new RangedCodeValue(new CodeConstant(null, left.max));
                                    e.right = new RangedCodeValue(new CodeConstant(null, right.max));
                                    Literal outMM = CodeArithmetics.computeValue(e);
                                    Literal outMin = Literal.min(outmm, Literal.min(outmM, Literal.min(outMm, outMM)));
                                    Literal outMax = Literal.max(outmm, Literal.max(outmM, Literal.max(outMm, outMM)));
                                    out = new Proposal(outMin, outMax);
                                }
                            } catch(InterpreterException f) {
                                throw new RuntimeException("unexpected exception: " + f.toString());
                            }
                            break;
                        }
                        case BINARY_MODULUS:
                        {
                            try {
                                Literal outMax = Literal.max(
                                        right.min.abs(), right.max.abs());
                                CodeOpBinaryExpression e = new CodeOpBinaryExpression(
                                        AbstractCodeOp.Op.BINARY_MINUS,
                                        null);
                                e.left = new RangedCodeValue(new CodeConstant(null, outMax));
                                e.right = new RangedCodeValue(new CodeConstant(null, new Literal(1)));
                                outMax = CodeArithmetics.computeValue(e);
                                Literal outMin = outMax.neg();
                                if(left != null && !left.isEmpty()) {
                                    outMin = Literal.max(outMin, Literal.min(new Literal(0), left.min));
                                    outMax = Literal.min(outMax, Literal.max(new Literal(0), left.max));
                                }
                                out = new Proposal(outMin, outMax);
                            } catch(InterpreterException f) {
                                throw new RuntimeException("unexpected exception: " + f.toString());
                            }
                            break;
                        }
                        case BINARY_EQUAL:
                        case BINARY_INEQUAL:
                        case BINARY_LESS:
                        case BINARY_LESS_OR_EQUAL:
                        {
                            // boolean operators
                            out = new Proposal(new Literal(0), new Literal(1));
                            break;
                        }
                        default:
                            throw new RuntimeException("unknown operator");
                    }
                }
            } else
                out = new Proposal();
            if(out == null)
                throw new RuntimeException("no proposal");
            if(v.isLocal())
                addToLocalProposals(localProposals, rm, index, v, out, iteration);
            else
                addToFieldProposals(rm, index, d, out, iteration);
            return localProposals;
        } catch(InterpreterException e) {
            throw new CompilerException(e.getStreamPos(),
                    CompilerException.Code.ILLEGAL,
                    e.getMessage());
        }
    }
    /**
     * Analyzes an assignment. Can return a proposal
     * to the result variable if (1) the result is local,
     * (2) the result is a field and <code>fieldProposals</code>
     * is not null.
     *
     * @param rm                        runtime method, to evaluate variable
     *                                  primitive ranges and initial values of
     *                                  fields
     * @param index                     index of <code>a</code> within method
     * @param a                         assignment to analyze
     * @param iteration                 iteration number, <code>finalIteration</code>
     *                                  for final pass, where actual variable
     *                                  proposals are modified
     * @return                          proposals for locals, empty set for none
     */
    protected Map<CodeVariable, Proposal> analyze(RuntimeMethod rm, int index,
            CodeOpAssignment a, int iteration) throws CompilerException {
        try {
            Map<CodeVariable, Proposal> localProposals = new HashMap<>();
            AbstractCodeDereference d = a.result;
            CodeVariable v = d.getTypeSourceVariable();
// if(a.result.toString().indexOf("s0") != -1)
//     a = a;
            Proposal rvalue = getTightest(rm, index, a.rvalue, iteration);
            Proposal out = null;
            if(rvalue != null) {
                if(rvalue.isEmpty())
                    // this assignment won't ever be realized
                    out = new Proposal(null);
                else
                    out = new Proposal(rvalue.min, rvalue.max);
            } else
                out = new Proposal();
            if(out != null)
                if(v.isLocal())
                    addToLocalProposals(localProposals, rm, index, v, out, iteration);
                else
                    addToFieldProposals(rm, index, d, out, iteration);
            return localProposals;
        } catch(InterpreterException e) {
            throw new CompilerException(e.getStreamPos(),
                    CompilerException.Code.ILLEGAL,
                    e.getMessage());
        }
    }
    /**
     * Analyzes a call. Can return a proposal to the result variable if (1) the result is local,
     * (2) the result is a field and <code>fieldProposals</code> is not null.
     *
     * @param rm                        runtime method, to evaluate variable
     *                                  primitive ranges and initial values of
     *                                  fields
     * @param index                     index of <code>c</code> within method
     * @param c                         call to analyze
     * @param iteration                 iteration number, <code>finalIteration</code>
     *                                  for final pass, where actual variable
     *                                  proposals are modified
     * @return                          proposals for locals, empty set for none
     */
    protected Map<CodeVariable, Proposal> analyze(RuntimeMethod rm, int index,
            CodeOpNone n, int iteration) throws CompilerException {
        try {
            Map<CodeVariable, Proposal> localProposals = new HashMap<>();
            AbstractCodeDereference d = n.result;
            CodeVariable v = d.getTypeSourceVariable();
            CodeVariable fake = new CodeVariable(
                                n.calledMethod.getStreamPos(), n.calledMethod.owner,
                                "fake", n.calledMethod.returnValue.type,
                                n.calledMethod.returnValue.context, true,
                                false, false, Variable.Category.OTHER);
            fake.primitiveRange = n.calledMethod.range;
            Proposal rvalue;
            if(n.calledMethod.annotations.contains(CompilerAnnotation.RANDOM_STRING) &&
                    // the method @RANDOM is non--static in Java, static in Verics
                    n.args.get(n.getRandomBoundArgIndex()).value instanceof CodeConstant) {
                CodeConstant c = (CodeConstant)n.args.get(n.getRandomBoundArgIndex()).value;
                long upperInclusive = c.value.getMaxPrecisionInteger() - 1;
                rvalue = new Proposal(new Literal(0), new Literal(upperInclusive));
            } else if(n.calledMethod.annotations.contains(CompilerAnnotation.DIST_ARRAY_STRING) &&
                    iteration > 0) {
                rvalue = null;
                CodeConstant arrayConst = sa.getConstant(rm, index,
                        (CodeFieldDereference)n.args.get(1).value);
                if(arrayConst != null) {
                    ArrayStore as = ((ArrayStore)
                            ((RuntimeValue)arrayConst.value).getReferencedObject());
                    rvalue = iterativeStoreProposals.get(iteration - 1).get(as);
                }
                if(rvalue == null)
                    rvalue = getTightest(rm, index,
                        new RangedCodeValue(
                            new CodeFieldDereference(
                                n.calledMethod.returnValue.context == Context.NON_STATIC ?
                                    rm.method.getThisArg() : null,
                                fake),
                            null),
                            iteration);
            } else
                rvalue = getTightest(rm, index,
                    new RangedCodeValue(
                        new CodeFieldDereference(
                            n.calledMethod.returnValue.context == Context.NON_STATIC ?
                                rm.method.getThisArg() : null,
                            fake),
                        null),
                        iteration);
            Proposal out = null;
            if(rvalue != null) {
                if(rvalue.isEmpty())
                    throw new RuntimeException("method has an empty return range");
                else
                    out = new Proposal(rvalue.min, rvalue.max);
            } else
                out = new Proposal();
            if(out != null)
                if(v.isLocal())
                    addToLocalProposals(localProposals, rm, index, v, out, iteration);
                else
                    addToFieldProposals(rm, index, d, out, iteration);
            return localProposals;
        } catch(InterpreterException e) {
            throw new CompilerException(e.getStreamPos(),
                    CompilerException.Code.ILLEGAL,
                    e.getMessage());
        }
    }
}
