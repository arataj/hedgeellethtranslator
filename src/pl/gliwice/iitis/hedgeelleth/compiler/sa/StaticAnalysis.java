/*
 * StaticAnalysis.java
 *
 * Created on Apr 22, 2008
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.sa;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Type.PrimitiveOrVoid;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.scope.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.TypeRangeMap;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * Static analysis. Performed after the semantic check.<br>
 * 
 * If the return type of a visitor method is ConstantExpression,
 * it means that if the method returns a value that is not null, the
 * value should replace the respective node.<br>
 * 
 * If the return type is Object, such a method always returns null.<br>
 *  
 * Thus, the return value of any accept() method can be cast
 * to <code>ConstantExpression</code>.<br>
 *
 * Keeps <code>Compilation.uniqueExpressions</code> up--to--date,
 * thus, uniqueness semantic check can be normally run after this
 * static analysis.
 * 
 * @author Artur Rataj
 */
public class StaticAnalysis implements Visitor<ConstantExpression> {
    /**
     * If to be verbose.
     */
    boolean verbose = false;
    
    /**
     * Map of all replaced expressions.
     */
    Map<AbstractExpression, ConstantExpression> replacements;
    /**
     * Data to restore the tree to the form apt for next static
     * analysis, with values of <code>ConstantExpression</code>
     * leaves possibly changed.<br>
     * 
     * For solitary methods, it is null.
     */
    Backup backup;
    /**
     * Variables with known constant values, null for none. Only used for
     * solitary methods, as values of its variables are always context--free, unlike
     * non--static fields.
     */
    protected Map<Variable, Literal> knowns;
    /**
     * Contains results of the analysis of each subsequent statement in the
     * solitary method. A null entry if a given expression could not be evaluated.<br>
     * 
     * Always null if this object has not been instantiated using
     * <code>StaticAnalysis(Method, Map&lt;Variable, Literal&gt;)</code>.
     */
    List<Literal> solitaryConstants;

    /**
     * Performs static analysis and then checks for the uniqueness of
     * expressions within <code>Compilation.uniqueExpressions</code>.<br>
     * 
     * Restoration is possible.
     * 
     * @param compilation compilation for static analysis, null
     * for none, e. g. when there is only some object to check, ouside
     * any compilation
     */
    public StaticAnalysis(Compilation compilation) throws CompilerException {
        replacements = new HashMap<>();
        backup = new Backup();
        this.knowns = null;
        try {
            compilation.accept(this);
        } catch(CompilerException e) {
            for(CompilerException.Report r : e.getReports())
                if(r.code != CompilerException.Code.ARITHMETIC)
                    // should not happen
                    throw new RuntimeException("unexpected compiler exception during " +
                            "static analysis: " + e.toString());
            throw e;
        }
        testUniqueExpressions(compilation);
    }
    /**
     * Crates an object for solitary analysis, but does not analyse
     * anything. Use <code>accept()</code> or
     * <code>checkSolitary()</code> for that.
     * 
     * @param knowns variables with known constant values,
     * null for none
     */
    public StaticAnalysis(Map<Variable, Literal> knowns) throws CompilerException {
        replacements = new HashMap<>();
        backup = new Backup();
        this.knowns = knowns;
        solitaryConstants = new LinkedList<>();
    }
    /**
     * Perfroms a static analysis of a solitary method. Use the
     * constructor <code>StaticAnalysis(Map<Variable, Literal> knowns)</code>
     * to set the knowns.
     * 
     * @param method to analyse
     */
    public final void checkSolitary(Method method) throws CompilerException {
        try {
            for(AbstractStatement s: method.code.code) {
                AbstractExpression expr = (AbstractExpression)s.accept(this);
                if(expr instanceof ConstantExpression) {
                    ConstantExpression c = (ConstantExpression)expr;
                    solitaryConstants.add(c.literal);
                } else
                    solitaryConstants.add(null);
            }
        } catch(CompilerException e) {
            // should not happen
            throw new RuntimeException("Compiler exception during " +
                    "static analysis: " + e.toString());
        }
    }
    /**
     * Returns the results of static analysis of a solitary method.
     * 
     * @return a list of entries for each statement in the solitary method;
     * null entry if a given statement could not be evaluated
     */
    public List<Literal> getSolitaryConstants() {
        return solitaryConstants;
    }
    /**
     * Restores the analyzed tree, as described in
     * <code>Backup.restore()</code>.
     */
    public void restore() {
        backup.restore();
    }
    /**
     * Registers expression replacement for later translation/
     * simplification of <code>Compilation.uniqueExpressions</code>
     * in <code>testUniqueExpressions</code>.<br>
     * 
     * The expression <code>former</code> must really be replaced
     * only if it belongs to unique expressions. This allows for a
     * special case of an lvalue that is a final variable with a known
     * value -- it may still be registered by a method for which it is effectively
     * a constant, even that the variable will not be replaced. It is not
     * a problem, though, as variables never belong to the unique expressions,
     * because they obviously are not expected to be constant.
     * 
     * @param former                    an expression to be replaced
     * @param current                   replacement constant expression
     */
    protected void replace(AbstractExpression former, ConstantExpression current) {
/*if(former.toString().equals("true"))
    former = former;*/
        if(replacements.containsKey(former))
            throw new RuntimeException("expression already replaced, perhaps " +
                    "an illegal double evaluation");
        replacements.put(former,
                // copy the constant, as the original might be modified
                new ConstantExpression(current.getStreamPos(),
                        (BlockScope)current.outerScope, current.literal));
    }
    /**
     * Tests <code>Compilation.uniqueExpressions</code>, taking into 
     * account the replacements registered via <code>replace</code>.
     * See the field's docs for details.<br>
     *
     * As the uniqueness testing can be done only after static analysis,
     * it is not performed within <code>SemanticCheck</code>.
     */
    final protected void testUniqueExpressions(Compilation compilation)
            throws CompilerException {
        CompilerException errors = new CompilerException();
        for(Set<AbstractExpression> set : compilation.uniqueExpressions) {
            Map<Long, ConstantExpression> unique = new HashMap<>();
            SortedMap<StreamPos, AbstractExpression> sorted =
                    new TreeMap<>();
            for(AbstractExpression e : set)
                sorted.put(e.getStreamPos(), e);
            for(AbstractExpression e : sorted.values()) {
                ConstantExpression c;
                if(replacements.containsKey(e)) {
                    // set.remove(e);
                    c = replacements.get(e);
                    // set.add(c);
                } else if(e instanceof ConstantExpression)
                    c = (ConstantExpression)e;
                else {
                    c = null;
                    errors.addReport(new CompilerException.Report(e.getStreamPos(),
                            ParseException.Code.ILLEGAL,
                            "only constant expression allowed here"));
                }
                if(c != null) {
                    if(c.literal.type.isOfIntegerTypes()) {
                        long value = c.literal.getMaxPrecisionInteger();
                        if(unique.containsKey(value)) {
                            errors.addReport(new CompilerException.Report(c.getStreamPos(),
                                    ParseException.Code.DUPLICATE,
                                    "duplicate constant, first occurence at " +
                                    unique.get(value).getStreamPos()));
                        } else
                            unique.put(value, c);
                    } else
                        errors.addReport(new CompilerException.Report(c.getStreamPos(),
                                ParseException.Code.ILLEGAL,
                                "only constant integer expression allowed here"));
                }
            }
        }
        if(errors.reportsExist())
            throw errors;
    }
    /**
     * Static analysis of a list of expressions.
     *
     * @param modified                  the expression whose list is to be
     *                                  modified
     * @param list                      expressions to analyze
     */
    protected void analyze(AbstractExpression modified,
            List<AbstractExpression> list) throws CompilerException {
        List<AbstractExpression> listCopy = new LinkedList<>(
                list);
        int count = 0;
        for(AbstractExpression e : listCopy) {
            ConstantExpression cExpression =
                    (ConstantExpression)e.accept(this);
            if(cExpression != null) {
                backup.register(modified, Backup.ChildType.ELEMENT, count,
                        e, cExpression);
                list.set(count, cExpression);
            }
            ++count;
        }
    }
    @Override
    public ConstantExpression visit(Node n) {
        throw new RuntimeException("Error: visit from " + n);
    }
    @Override
    public ConstantExpression visit(AbstractExpression e) {
        throw new RuntimeException("Error: visit from " + e);
    }
    @Override
    public ConstantExpression visit(EmptyExpression e) {
        return null;
    }
    @Override
    public ConstantExpression visit(ConstantExpression e) {
        return e;
    }
    @Override
    public ConstantExpression visit(AllocationExpression e) throws CompilerException {
        analyze(e, e.expressions);
        return null;
    }
    @Override
    public ConstantExpression visit(PrimaryExpression e) throws CompilerException {
        boolean hasRanges = false;
        for(AssignmentExpression a : e.prefix.rangeExpressions) {
            // the top expressions in the list are not surely
            // replaced by constants, as they are assignment
            // expressions
            a.accept(this);
            hasRanges = true;
        }
        if(!hasRanges) {
            // check if this primary contains only a constant expression 
            ParsedPrimary v = e.value;
            // check if the last element is a final variable
            if(v.contents.size() <= 2) {
                Typed typed = v.contents.get(v.contents.size() - 1);
                if(typed instanceof Variable) {
/*if(typed.toString().indexOf("int y") != -1)
    typed = typed;*/
                    Variable variable = (Variable)typed;
                    if(knowns != null && knowns.containsKey(variable)) {
                        // only solitary expressions, they have a flat model of variables
                        ConstantExpression c = new ConstantExpression(e.getStreamPos(),
                                (BlockScope)e.outerScope, knowns.get(variable));
                        replace(e, c);
                        return c;
                    } else if(variable.type.isPrimitive() && variable.flags.isFinal() &&
                            variable.initializers != null && variable.initializers.size() == 1) {
                        boolean valid = true;
                        if(v.contents.size() == 2) {
                            // check if the other element is "this"
                            Typed first = v.contents.get(0);
                            if(first instanceof Variable) {
                                Variable firstVariable = (Variable)first;
                                if(!firstVariable.name.equals(Method.LOCAL_THIS))
                                    valid = false;
                            } else
                                valid = false;
                        }
                        if(valid) {
                            // as the type is primitive, the initializer
                            // can be only an assignment to <code>variable</code>
                            // check if the assignment's right side is a constant
                            AssignmentExpression a = variable.initializers.get(0);
                            AbstractExpression original = a.rvalue;
                            StaticAnalysis sa = new StaticAnalysis(
                                    new HashMap<>());
                            AbstractExpression sub;
                            try {
                                sub = (AbstractExpression)
                                    original.accept(sa);
                                backup.addAll(sa);
                            } catch(CompilerException f) {
                                // expression contains variables, refrain from
                                // evaluating it, as the evaluation is out--of--place
                                sub = null;
                            }
                            if(sub instanceof ConstantExpression) {
                                // the rvalue went to unique expression's
                                // replacement list, so replace it indeed,
                                // even that it would eventually be replaced
                                // anyway
                                backup.register(a, Backup.ChildType.RIGHT, -1, original, sub);
                                a.rvalue = sub;
                                // report that the primary can be replaced as well
                                ConstantExpression c = ((ConstantExpression)sub).clone();
                                replace(e, c);
                                return c;
                            } else
                                return null;
                            /*if(original instanceof ConstantExpression) {
                                ConstantExpression c = (ConstantExpression)original;
                                replace(e, c);
                                return c;
                            }*/
                        }
                    }
                }
            }
            if(v.contents.size() == 1) {
                Typed typed = v.contents.get(0);
                if(typed instanceof AbstractExpression) {
                    AbstractExpression original = (AbstractExpression)typed;
                    AbstractExpression sub = (AbstractExpression)
                            original.accept(this);
                    if(sub instanceof ConstantExpression) {
                        ConstantExpression c = ((ConstantExpression)sub).clone();
                        replace(e, c);
                        return c;
                    }
                }
            }
        }
        return null;
    }
    @Override
    public ConstantExpression visit(UnaryExpression e) throws CompilerException {
        try {
            ConstantExpression c = (ConstantExpression)e.sub.accept(this);
            if(c != null) {
                switch(e.operatorType) {
                    case BITWISE_COMPLEMENT:
                    {
                        PrimitiveOrVoid p = e.getResultType().getPrimitive();
                        switch(p) {
                            case INT:
                                c.literal = new Literal((int)~
                                        c.literal.getMaxPrecisionInteger());
                                break;

                            case LONG:
                                c.literal = new Literal((long)~
                                        c.literal.getMaxPrecisionInteger());
                                break;

                            default:
                                throw new RuntimeException("unknown or invalid type: " + p);
                        }
                        break;
                    }
                    case CONDITIONAL_NEGATION:
                    {
                        c.literal = new Literal(!c.literal.getBoolean());
                        break;
                    }
                    case NEGATION:
                    {
                        switch(e.getResultType().getPrimitive()) {
                            case INT:
                                c.literal = new Literal(Math.toIntExact(-
                                        c.literal.getMaxPrecisionInteger()));
                                break;

                            case LONG:
                                c.literal = new Literal(-
                                        c.literal.getMaxPrecisionInteger());
                                break;

                            case FLOAT:
                                c.literal = new Literal((float)-
                                        c.literal.getMaxPrecisionFloatingPoint());
                                break;

                            case DOUBLE:
                                c.literal = new Literal((double)-
                                        c.literal.getMaxPrecisionFloatingPoint());
                                break;

                            default:
                                throw new RuntimeException("invalid type");
                        }
                        break;
                    }
                    case CAST:
                    {
                        if(e.sub.getResultType().isNull())
                            // cast of null constant, can not be replaced
                            // because the null constant itself does not
                            // have a type
                            c = null;
                        else {
                            Type.PrimitiveOrVoid p = e.getResultType().getPrimitive();
                            TypeRangeMap castRanges = new TypeRangeMap(p, false);
                            c.literal = castRanges.computeCastModulo(p, c.literal);
                            /*
                            switch(e.getResultType().getPrimitive()) {
                                case INT:
                                    c.literal = new Literal((int)
                                            c.literal.getMaxPrecisionInteger());
                                    break;

                                case LONG:
                                    c.literal = new Literal((long)
                                            c.literal.getMaxPrecisionInteger());
                                    break;

                                case FLOAT:
                                    c.literal = new Literal((float)
                                            c.literal.getMaxPrecisionFloatingPoint());
                                    break;

                                case DOUBLE:
                                    c.literal = new Literal((double)
                                            c.literal.getMaxPrecisionFloatingPoint());
                                    break;

                                default:
                                    throw new RuntimeException("invalid type");
                            }
                            */
                        }
                        break;
                    }
                    case INSTANCE_OF:
                        c = null;
                        break;

                    case PRE_INCREMENT:
                    case PRE_DECREMENT:
                    case POST_INCREMENT:
                    case POST_DECREMENT:
                        c = null;
                        break;

                    default:
                        throw new RuntimeException("illegal operator for " +
                                "a constant expression");
                }
            }
            if(c != null) {
                replace(e, c);
            }
            return c;
        } catch(ArithmeticException f) {
            throw new CompilerException(e.getStreamPos(),
                                CompilerException.Code.ARITHMETIC,
                                "integer overflow");
        }
    }
    @Override
    public ConstantExpression visit(BinaryExpression e) throws CompilerException {
        try {
            ConstantExpression cLeft = (ConstantExpression)e.left.accept(this);
            ConstantExpression cRight = (ConstantExpression)e.right.accept(this);
            Literal l = null;
            if(cLeft != null && cRight != null) {
                String stringClassName = e.outerScope.getBoundingJavaClass().frontend.
                        stringClassName;
                if(e.left.getResultType().isBoolean() &&
                        e.right.getResultType().isBoolean()) {
                    boolean left = cLeft.literal.getBoolean();
                    boolean right = cRight.literal.getBoolean();
                    boolean result = false;
                    switch(e.operatorType) {
                        case CONDITIONAL_AND:
                            result = left && right;
                            break;

                        case CONDITIONAL_OR:
                            result = left || right;
                            break;

                        case EQUAL:
                            result = left == right;
                            break;

                        case INEQUAL:
                            result = left != right;
                            break;

                        default:
                            throw new RuntimeException("invalid operator " +
                                    "for boolean operands");
                    }
                    l = new Literal(result);
                } if(e.left.getResultType().isString(stringClassName) ||
                        e.right.getResultType().isString(stringClassName)) {
                    String left = cLeft.literal.toJavaString();
                    String right = cRight.literal.toJavaString();
                    String result = null;
                    if(e.operatorType == BinaryExpression.Op.PLUS)
                        result = left + right;
                    else
                        throw new RuntimeException("invalid operator for " +
                                stringClassName);
                    l = new Literal(result, stringClassName);
                } else {
                    int maxOperandPrecision = Math.max(
                            e.left.getResultType().numericPrecision(),
                            e.right.getResultType().numericPrecision());
                    if(maxOperandPrecision >= Type.NumericPrecision.FLOAT) {
                        double left = cLeft.literal.getMaxPrecisionFloatingPoint();
                        double right = cRight.literal.getMaxPrecisionFloatingPoint();
                        double result = Double.NaN;
                        boolean booleanResult = false;
                        boolean resultIsBoolean = false;
                        switch(e.operatorType) {
                            case DIVIDE:
                                result = left/right;
                                break;

                            case MINUS:
                                result = left - right;
                                break;

                            case MODULUS:
                                result = left%right;
                                break;

                            case MODULUS_POS:
                                if(left >= 0.0)
                                    result = left%right;
                                else
                                    result = (left + 1)%right + (right - 1);
                                break;

                            case MULTIPLY:
                                result = left*right;
                                break;

                            case PLUS:
                                result = left + right;
                                break;

                            case EQUAL:
                                booleanResult = left == right;
                                resultIsBoolean = true;
                                break;

                            case GREATER:
                                booleanResult = left > right;
                                resultIsBoolean = true;
                                break;

                            case GREATER_OR_EQUAL:
                                booleanResult = left >= right;
                                resultIsBoolean = true;
                                break;

                            case INEQUAL:
                                booleanResult = left != right;
                                resultIsBoolean = true;
                                break;

                            case LESS:
                                booleanResult = left < right;
                                resultIsBoolean = true;
                                break;

                            case LESS_OR_EQUAL:
                                booleanResult = left <= right;
                                resultIsBoolean = true;
                                break;

                            default:
                                throw new RuntimeException("invalid operator " +
                                        "for floating point precision");
                        }
                        if(resultIsBoolean)
                            l = new Literal(booleanResult);
                        else
                            switch(maxOperandPrecision) {
                                case Type.NumericPrecision.FLOAT:
                                    l = new Literal((float)result);
                                    break;

                                case Type.NumericPrecision.DOUBLE:
                                    l = new Literal(result);
                                    break;

                                default:
                                    throw new RuntimeException("unknown or invalid precision: " +
                                            maxOperandPrecision);
                            }
                    } else if(maxOperandPrecision > 0) {
                        long left = cLeft.literal.getMaxPrecisionInteger();
                        long right = cRight.literal.getMaxPrecisionInteger();
                        long result = -1;
                        boolean divisionByZero = false;
                        boolean booleanResult = false;
                        boolean resultIsBoolean = false;
                        switch(e.operatorType) {
                            case DIVIDE:
                                if(right == 0)
                                    divisionByZero = true;
                                else
                                    result = left/right;
                                break;

                            case EXCLUSIVE_OR:
                                result = left^right;
                                break;

                            case INCLUSIVE_OR:
                                result = left|right;
                                break;

                            case MINUS:
                                result = Math.subtractExact(left, right);
                                break;

                            case MODULUS:
                                result = left%right;
                                break;

                            case MODULUS_POS:
                                if(left >= 0.0)
                                    result = left%right;
                                else
                                    result = Math.addExact(Math.addExact(left, 1)%right,
                                        Math.subtractExact(right, 1));
                                break;

                            case MULTIPLY:
                                result = Math.multiplyExact(left, right);
                                break;

                            case PLUS:
                                result = Math.addExact(left, right);
                                break;

                            case SHIFT_LEFT:
                                if(right >= 62)
                                    throw new CompilerException(e.getStreamPos(),
                                                        CompilerException.Code.ARITHMETIC,
                                                        "integer overflow");
                                result = Math.multiplyExact(left, 1L << right);
                                break;

                            case SHIFT_RIGHT:
                                result = left>>right;
                                break;

                            case UNSIGNED_SHIFT_RIGHT:
                                result = left>>>right;
                                break;

                            case EQUAL:
                                booleanResult = left == right;
                                resultIsBoolean = true;
                                break;

                            case GREATER:
                                booleanResult = left > right;
                                resultIsBoolean = true;
                                break;

                            case GREATER_OR_EQUAL:
                                booleanResult = left >= right;
                                resultIsBoolean = true;
                                break;

                            case INEQUAL:
                                booleanResult = left != right;
                                resultIsBoolean = true;
                                break;

                            case LESS:
                                booleanResult = left < right;
                                resultIsBoolean = true;
                                break;

                            case LESS_OR_EQUAL:
                                booleanResult = left <= right;
                                resultIsBoolean = true;
                                break;

                            default:
                                throw new RuntimeException("invalid operator " +
                                        "type: " + e.operatorType);
                        }
                        if(resultIsBoolean)
                            l = new Literal(booleanResult);
                        else if(!divisionByZero)
                            switch(maxOperandPrecision) {
                                case Type.NumericPrecision.CHAR:
                                case Type.NumericPrecision.SHORT:
                                case Type.NumericPrecision.INT:
                                    l = new Literal(Math.toIntExact(result));
                                    break;

                                case Type.NumericPrecision.LONG:
                                    l = new Literal(result);
                                    break;

                                default:
                                    throw new RuntimeException("unknown or invalid precision: " +
                                            maxOperandPrecision);
                            }
                    }
                }
            }
            ConstantExpression c;
            if(l == null) {
                if(cLeft != null) {
                    backup.register(e, Backup.ChildType.LEFT, -1, e.left, cLeft);
                    e.left = cLeft;
                }
                if(cRight != null) {
                    backup.register(e, Backup.ChildType.RIGHT, -1, e.right, cRight);
                    e.right = cRight;
                }
                c = null;
            } else
                c = new ConstantExpression(e.getStreamPos(),
                        (BlockScope)e.outerScope, l);
            if(c != null)
                replace(e, c);
            return c;
        } catch(ArithmeticException f) {
            throw new CompilerException(e.getStreamPos(),
                                CompilerException.Code.ARITHMETIC,
                                "integer overflow");
        }
    }
    @Override
    public ConstantExpression visit(AssignmentExpression e) throws CompilerException {
        // lvalue can not be replaced even if it is a final variable with
        // a known value
        e.lvalue.accept(this);
        ConstantExpression c = (ConstantExpression)e.rvalue.accept(this);
        if(c != null) {
            backup.register(e, Backup.ChildType.RIGHT, -1, e.rvalue, c);
            e.rvalue = c;
        }
        return null;
    }
    @Override
    public ConstantExpression visit(CallExpression c) throws CompilerException {
        analyze(c, c.arg);
        return null;
    }
    @Override
    public ConstantExpression visit(IndexExpression i) throws CompilerException {
        i.index.accept(this);
        return null;
    }
    @Override
    public ConstantExpression visit(BlockExpression b) throws CompilerException {
        b.block.accept(this);
        return null;
    }
    @Override
    public ConstantExpression visit(ReturnStatement s) throws CompilerException {
        if(s.sub != null) {
            ConstantExpression c = (ConstantExpression)s.sub.accept(this);
            if(c != null) {
                backup.register(s, Backup.ChildType.SUB, -1, s.sub, c);
                s.sub = c;
            }
        }
        return null;
    }
    @Override
    public ConstantExpression visit(ThrowStatement s) throws CompilerException {
        // the expression should return an object reference,
        // which can not be replaced by a constant
        s.sub.accept(this);
        return null;
    }
    @Override
    public ConstantExpression visit(LabeledStatement s) throws CompilerException {
        s.sub.accept(this);
        return null;
    }
    @Override
    public ConstantExpression visit(JumpStatement s) throws CompilerException {
        return null;
    }
    @Override
    public ConstantExpression visit(BranchStatement s) throws CompilerException {
        s.condition.accept(this);
        return null;
    }
    @Override
    public ConstantExpression visit(SynchronizedStatement e) throws CompilerException {
        e.lock.accept(this);
        e.block.accept(this);
        return null;
    }
    @Override
    public ConstantExpression visit(Block b) throws CompilerException {
        for(AbstractStatement s: b.code) {
            s.accept(this);
        }
        return null;
    }
    /**
     * Does static analysis of the compilation.
     * 
     * @param c                         compilation
     */
    @Override
    public ConstantExpression visit(Compilation c) throws CompilerException {
        for(String packagE : c.packages.packageScopes.keySet()) {
            log(0, "analysisng package `" + packagE + "'");
            PackageScope p = c.packages.packageScopes.get(packagE);
            for(String javaClass : p.javaClassScopes.keySet()) {
                log(1, "analysing java class `" + javaClass + "'");
                JavaClassScope j = p.javaClassScopes.get(javaClass);
                for(MethodSignature key : new LinkedList<>(j.methods.keySet())) {
                    Method method = j.methods.get(key);
                    log(2, "analysing method `" + method.signature.
                            toString() + "'");
                    if(!method.flags.isAbstract())
                        visit(method.code);
                }
            }
        }
        return null;
    }
    void log(int indentLevel, String s) {
        if(verbose) {
            System.out.print("A ");
            for(int i = 0; i < indentLevel; ++i)
                System.out.print("  ");
            System.out.println(s);
        }
    }
}
