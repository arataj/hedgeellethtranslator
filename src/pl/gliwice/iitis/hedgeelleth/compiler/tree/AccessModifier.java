/*
 * AccessModifier.java
 *
 * Created on Oct 10, 2008
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree;

/**
 * Access modifier.
 *
 * Without INTERNAL -- package membership has no effect, with INTERNAL --
 * same package membership gives access.
 *
 * Current level of access implementation:
 * 
 *
 * @author Artur Rataj
 */
public enum AccessModifier {
    /*
    PRIVATE,
    DEFAULT,
    PROTECTED,
    PUBLIC,
     */
    /**
     * Equivalent of Java's private.
     */
    PRIVATE,
    /**
     * Only derived types, no equivalent in Java.
     */
    PROTECTED,
    /**
     * World, equivalent of Java's public.
     */
    PUBLIC,
    /**
     * World and also the application's public API.
     */
    PUBLIC_API,
    /**
     * Only package, equivalent of Java's default.
     */
    INTERNAL,
    /**
     * Derived types and package, equivalent of Java's protected.
     */
    PROTECTED_INTERNAL;

    /**
     * If this modifier allows protected access. True for PUBLIC,
     * PROTECTED and PROTECTED_INTERNAL.
     * @return
     */
    public boolean hasProtectedAccess() {
        switch(this) {
            case PROTECTED:
            case PUBLIC:
            case PUBLIC_API:
            case PROTECTED_INTERNAL:
                return true;

            case PRIVATE:
            case INTERNAL:
                return false;

            default:
                throw new RuntimeException("unknown access modifier");
        }
    }
    /**
     * If this modifier allows package access. True for PUBLIC,
     * INTERNAL and PROTECTED_INTERNAL.
     * @return
     */
    public boolean hasPackageAccess() {
        switch(this) {
            case PUBLIC:
            case PUBLIC_API:
            case INTERNAL:
            case PROTECTED_INTERNAL:
                return true;

            case PRIVATE:
            case PROTECTED:
                return false;

            default:
                throw new RuntimeException("unknown access modifier");
        }
    }
    /**
     * If this modifier allows public access. True for PUBLIC and
     * PUBLIC_API.
     * @return
     */
    public boolean hasPublicAccess() {
        switch(this) {
            case PUBLIC:
            case PUBLIC_API:
                return true;

            case INTERNAL:
            case PRIVATE:
            case PROTECTED:
            case PROTECTED_INTERNAL:
                return false;

            default:
                throw new RuntimeException("unknown access modifier");
        }
    }
    /**
     * If this modifier has access privileges that are greater than these of
     * some other modifier.
     *
     * @param am                        given modifier
     * @return                          if greater than or equal to
     */
    public boolean privilegesGreaterThan(AccessModifier am) {
        boolean lesser;
        switch(this) {
            case PRIVATE:
            {
                switch(am) {
                    case PRIVATE:
                    case PROTECTED:
                    case PUBLIC:
                    case PUBLIC_API:
                    case INTERNAL:
                    case PROTECTED_INTERNAL:
                        lesser = false;
                        break;

                    default:
                        throw new RuntimeException("unknown access modifier");
                }
                break;
            }
            case PROTECTED:
            {
                switch(am) {
                    case PRIVATE:
                    case INTERNAL:
                        lesser = true;
                        break;

                    case PROTECTED:
                    case PUBLIC:
                    case PUBLIC_API:
                    case PROTECTED_INTERNAL:
                        lesser = false;
                        break;

                    default:
                        throw new RuntimeException("unknown access modifier");
                }
                break;
            }
            case PUBLIC:
            {
                switch(am) {
                    case PRIVATE:
                    case PROTECTED:
                    case INTERNAL:
                    case PROTECTED_INTERNAL:
                        lesser = true;
                        break;

                    case PUBLIC:
                    case PUBLIC_API:
                        lesser = false;
                        break;

                    default:
                        throw new RuntimeException("unknown access modifier");
                }
                break;
            }
            case PUBLIC_API:
            {
                switch(am) {
                    case PRIVATE:
                    case PROTECTED:
                    case INTERNAL:
                    case PROTECTED_INTERNAL:
                    case PUBLIC:
                        lesser = true;
                        break;

                    case PUBLIC_API:
                        lesser = false;
                        break;

                    default:
                        throw new RuntimeException("unknown access modifier");
                }
                break;
            }
            case INTERNAL:
            {
                switch(am) {
                    case PRIVATE:
                    case PROTECTED:
                        lesser = true;
                        break;

                    case PUBLIC:
                    case PUBLIC_API:
                    case INTERNAL:
                    case PROTECTED_INTERNAL:
                        lesser = false;
                        break;

                    default:
                        throw new RuntimeException("unknown access modifier");
                }
                break;
            }
            case PROTECTED_INTERNAL:
            {
                switch(am) {
                    case PRIVATE:
                    case PROTECTED:
                    case INTERNAL:
                        lesser = true;
                        break;

                    case PUBLIC:
                    case PUBLIC_API:
                    case PROTECTED_INTERNAL:
                        lesser = false;
                        break;

                    default:
                        throw new RuntimeException("unknown access modifier");
                }
                break;
            }
            default:
                throw new RuntimeException("unknown access modifier");
        }
        return lesser;
    }
    @Override
    public String toString() {
        switch(this) {
            case PRIVATE:
                return "private";

            case PROTECTED:
                return "protected";

            case PUBLIC:
                return "public";

            case PUBLIC_API:
                return "public api";

            case INTERNAL:
                return "internal";

            case PROTECTED_INTERNAL:
                return "protected internal";

            default:
                throw new RuntimeException("unknown access modifier");
        }
    }
};
