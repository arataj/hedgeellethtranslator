/*
 * Compilation.java
 *
 * Created on Jan 16, 2008, 1:59:09 PM
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.CompilerAnnotation;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.clazz.AbstractJavaClass;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.scope.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.AbstractExpression;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.interpreter.ModelControl;
import pl.gliwice.iitis.hedgeelleth.modules.AbstractFrontend;

/**
 * Structures for the whole compilation.
 * 
 * @author Artur Rataj
 */
public class Compilation implements Node {
    /**
     * Options of the compilation, needed by <code>Hedgeelleth</code> or
     * other classes.
     */
    public CompilationOptions options;
    /**
     * Scope of packages, top level.
     */
    public TopScope packages;
    /**
     * All imports. Used only to find invalid imports of packages that do not exist.<br>
     * 
     * Imports for actually searching for classes are within <code>JavaClassScope</code>.
     */
    public List<Import> imports;
    /**
     * A list of sets such that, within each set are expressions
     * that should be integer constants after static analysis and
     * should have unique values within the set.<br>
     *
     * Used for semantic check of expressions within entities like
     * switch statements.<br>
     * 
     * See <code>StaticAnalysis.testUniqueExpressions</code> for details.
     *
     * @see StaticAnalysis.testUniqueExpressions
     */
    public List<Set<AbstractExpression>> uniqueExpressions;
    /**
     * Methods with unique compiler annotations , as defined in
     * <code>Hedgeelleth.checkSpecialMethod()</code>.
     */
    public Map<AbstractFrontend, Map<CompilerAnnotation, Method>> specialMethods;
    /**
     * Model control, null if not applicable. Default is null.
     */
    public ModelControl modelControl;
    
    /**
     * Creates a new instance of Compilation. Creates the system package.
     */
    public Compilation(CompilationOptions options) {
        this.options = options;
        packages = new TopScope();
        PackageScope systemPackageScope = new PackageScope(packages,
                Type.SYSTEM_PACKAGE);
        packages.packageScopes.put(Type.SYSTEM_PACKAGE,
                systemPackageScope);
        imports = new LinkedList<>();
        uniqueExpressions = new LinkedList<>();
        specialMethods = new HashMap<>();
        modelControl = null;
    }
    /**
     * Returns a list of all classes within this compilation.
     * 
     * @return                          list of classes
     */
    public List<AbstractJavaClass> getClasses() {
        List<AbstractJavaClass> out = new LinkedList<>();
        for(String packagE : packages.packageScopes.keySet()) {
            PackageScope p = packages.packageScopes.get(packagE);
            for(String javaClass : p.javaClassScopes.keySet()) {
                JavaClassScope j = p.javaClassScopes.get(javaClass);
                AbstractJavaClass currentJavaClass = (AbstractJavaClass)j.owner;
                out.add(currentJavaClass);
            }
        }
        return out;
    }
    @Override
    public Object accept(Visitor v)  throws CompilerException {
        return v.visit(this);
    }
}
