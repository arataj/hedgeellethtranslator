/*
 * CompilationOptions.java
 *
 * Created on Jun 28, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree;

import java.util.List;
import java.util.Map;
import pl.gliwice.iitis.hedgeelleth.modules.CliConstants;
import pl.gliwice.iitis.hedgeelleth.modules.HedgeellethCompiler;

/**
 * Compilation options needed by <code>Hedgeelleth</code> or
 * other classes.
 * 
 * @author Artur Rataj
 */
public class CompilationOptions {
    /**
     * Test options, not used outside compiler testing.
     */
    public static class Test {
        /**
         * Used in <code>Generator.checkFinalVariableSimple()</code>.
         * 
         * The default is false.
         */
        public boolean ignoreInternalBlankFinals = false;
        /**
         * If constant elements of arrays should not be replaced with their values.
         * Used in <code>AbstracTADDGenerator.generateTADD()</code>.
         * 
         * The default is false.
         */
        public boolean ignoreConstantElements = false;
        /**
         * If to enable optimisations, which may optimize too much in certain tests, and thus
         * decrease testing quality. Set to false only in compiler tests.
         * 
         * The default is true.
         */
        public boolean extraOptimizations = true;
    };
    /**
     * False if blank finals are not allowed. True, if they are allowed and
     * can be assigned. If static fields, the assignments must be within static
     * blocks. If non--static fields, the assignments must be within constructors.
     * 
     * The default is <code>true</code>.
     */
    public final boolean blankFinalsAllowed;
    /**
     * Command--line defined constants, copied from 
     * <code>HedgeellethCompiler.Options.constants</code>.
     */
    public final CliConstants constants;
    /**
     * Compiler test options, null outside compiler tests.
     */
    public Test test;
    
    /**
     * Creates an instance of <code>CompilationOptions</code>, with
     * options as specified in <code>options</code> or, if not specified,
     * having default values.
     * 
     * @param options  compiler's options
     */
    public CompilationOptions(HedgeellethCompiler.Options options) {
        blankFinalsAllowed  = options.blankFinalsAllowed;
        constants = options.constants;
        test = options.test;
        // blankFinalsAllowed = false;
    }
}
