/*
 * Constructor.java
 *
 * Created on Dec 13, 2007, 1:23:46 PM
 *
 * Copyright (c) 2007 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.VariableInitializer;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.clazz.ImplementingClass;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.flags.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 *
 * @author Artur Rataj
 */
public class Constructor extends Method {
    /**
     * Creates a new instance of Constructor, see docs of <code>Method</code>
     * for details.
     * 
     * @param pos                   position in the parsed stream
     * @param clazz                 class of this constructor
     * @param initializers          arguments
     * @param flags                 modifiers
     */
    public Constructor(StreamPos pos, ImplementingClass clazz, String name,
            List<VariableInitializer> initializers, MethodFlags flags) throws ParseException {
        super(pos, clazz, name, Type.newVoid(), initializers, flags, null);
    }
}
