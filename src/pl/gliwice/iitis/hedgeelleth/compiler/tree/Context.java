/*
 * Context.java
 *
 * Created on Jul 11, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree;

/**
 * <p>Defines context: empty, static or non--static.</p>
 * 
 *  <p>Non--static is the equivalent of objective in Verics.</p>
 * 
 * <p>Empty and static are similar, but it is forced during semantic check that empty can
 * use only empty members. Empty is changed into STATIC earky during the parsing, plus there
 * is an extra flag, that "EMPTY" limitations must be obeyed. Parts of the translator that do not
 * differentiate between empty and static treat thus both as static.</p>
 * 
 * @author Artur Rataj
 */
public enum Context {
    EMPTY,
    STATIC,
    NON_STATIC;
};
