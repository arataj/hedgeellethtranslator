/*
 * Import.java
 *
 * Created on Oct 5, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.StreamPos;

/**
 * Describes an import or a static import.
 * 
 * @author Artur Rataj
 */
public class Import {
    /**
     * Position of this import in an input stream.
     */
    public StreamPos pos;
    /**
     * If this import is static.
     */
    public boolean statiC;
    /**
     * Name, that represents this import.
     */
    public NameList name;

    /**
     * Creates a new import decription.
     * 
     * @param pos position in the input stream, null for none
     * @param statiC if this import is static
     * @param name name, that represents this import
     */
    public Import(StreamPos pos, boolean statiC, NameList name) {
        this.pos = pos;
        this.statiC = statiC;
        this.name = name;
    }
    /**
     * Extracts a list of names from a list of imports.
     * 
     * @param imports list of imports to scan, empty for none
     * @return list of names, empty for none
     */
    public static List<NameList> getNames(List<Import> imports) {
        List<NameList> out = new LinkedList<>();
        for(Import i : imports)
            out.add(i.name);
        return out;
    }
}
