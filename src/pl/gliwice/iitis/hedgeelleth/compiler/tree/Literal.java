/*
 * Value.java
 *
 * Created on Jan 21, 2009
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.CompilerAnnotation;
import pl.gliwice.iitis.hedgeelleth.compiler.code.optimizer.ConditionalLiteral;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Type.PrimitiveOrVoid;
import pl.gliwice.iitis.hedgeelleth.compiler.util.CompilerUtils;
import pl.gliwice.iitis.hedgeelleth.interpreter.RuntimeValue;

/**
 * A generic abstract class for holding a constant value of some type.<br>
 *
 * Note: the method <code>equals()<code> returns if two values represent
 * exactly the same instance,  not just if arithmetic values are equal. For the latter,
 * use <code>arithmeticEqualTo(Literal)</code>.
 *
 * @author Artur Rataj
 */
public class Literal implements Cloneable {
    /**
     * A special value for the annotation type.
     */
    public static class AnnotationValue {
        /**
         * Arguments of the call. If all explicitly constant, then
         * <code>constArgs</code> is true and this evaluates to doubles
         * or to a string.
         */
        public List<Object> args;
        /**
         * If the arguments are explicitly constant. This field is used
         * if it needs to be decided, if some value that uses this object can be
         * treated as a constant.
         */
        public boolean constArgs;
        /**
         * Expected range of arithmetic values, assumed to be correct,
         * thus not requiring checking. Null for none.
         */
        public Type.TypeRange range;
        
        /**
         * Creates a new special value, with constant double arguments.
         * The field <code>constArgs</code> is set to true.
         * 
         * @param args a string
         */
        public AnnotationValue(String arg) {
            this.args = new LinkedList<>();
            this.args.add(arg);
            constArgs = true;
            this.range = null;
        }
        /**
         * Creates a new special value, with a single constant string
         * argument. The field <code>constArgs</code> is set to false.
         * 
         * @param args arguments of the replaced call, evaluated to constants
         * @param range range covering all possible evaluations of this
         * special value, null for none
         */
        public AnnotationValue(List<Double> args, Type.TypeRange range) {
            this.args = new LinkedList<>();
            for(Double d : args)
                this.args.add(d);
            constArgs = true;
            this.range = range;
        }
        /**
         * Creates a new special value, with arbitrary arguments.
         * The field <code>constArgs</code> is set to false.
         * 
         * @param args arguments of the replaced call
         * @param range range covering all possible evaluations of this
         * special value, null for none
         * @param erasureDifferentiator meaningless
         */
        public AnnotationValue(List<Object> args, Type.TypeRange range,
                int erasureDifferentiator) {
            this.args = args;
            constArgs = false;
            this.range = range;
        }
        /*
        public boolean equals(Object other) {
            AnnotationValue v = (AnnotationValue)other;
        }*/
        @Override
        public String toString() {
            String s = (range != null ? range.toString() : "") + "(";
            if(constArgs) {
                for(Object d : args)
                    s += ((Double)d) + " ";
            } else {
                for(Object d : args)
                    s += d.toString() + " ";
            }
            s = s.trim() + ")";
            return s;
        }
    };
    /**
     * Type of this abstract value.
     */
    public Type type;
    /**
     * Value.
     */
    public Object value;

    /**
     * Creates a new instance of Value, type INTEGER.
     *
     * @param value                     integer value
     */
    public Literal(int value) {
        type = new Type(Type.PrimitiveOrVoid.INT);
        this.value = new Integer(value);
    }
    /**
     * Creates a new instance of Value, type BYTE.
     *
     * @param value                     byte value
     */
    public Literal(byte value) {
        type = new Type(Type.PrimitiveOrVoid.BYTE);
        this.value = new Byte(value);
    }
    /**
     * Creates a new instance of Value, type SHORT.
     *
     * @param value                     short value
     */
    public Literal(short value) {
        type = new Type(Type.PrimitiveOrVoid.SHORT);
        this.value = new Short(value);
    }
    /**
     * Creates a new instance of Value, type LONG.
     *
     * @param value                     long value
     */
    public Literal(long value) {
        type = new Type(Type.PrimitiveOrVoid.LONG);
        this.value = new Long(value);
    }
    /**
     * Creates a new instance of Value, type FLOAT.
     *
     * @param value                     floating value
     */
    public Literal(float value) {
        type = new Type(Type.PrimitiveOrVoid.FLOAT);
        this.value = new Float(value);
    }
    /**
     * Creates a new instance of Value, type DOUBLE.
     *
     * @param value                     double value
     */
    public Literal(double value) {
        type = new Type(Type.PrimitiveOrVoid.DOUBLE);
        this.value = new Double(value);
    }
    /**
     * Creates a new instance of Value, type CHARACTER.
     *
     * @param value                     character value
     */
    public Literal(char value) {
        type = new Type(Type.PrimitiveOrVoid.CHAR);
        this.value = new Character(value);
    }
    /**
     * Creates a new instance of Value, type String.
     *
     * @param value                     string value
     * @param stringClassName           name of the special string class,
     *                                  null for that defined in <code>Type</code>
     */
    public Literal(String value, String stringClassName) {
        if(stringClassName == null)
            stringClassName = Type.STRING_QUALIFIED_CLASS_NAME;
        // the string class does not allow mutator methods
        type = new Type(new NameList(stringClassName));
//        if(value == null)
//            this.value = null;
//        else
         this.value = new String(value);
    }
    /**
     * Creates a new instance of Value, type BOOLEAN.
     *
     * @param value                     boolean value
     */
    public Literal(boolean value) {
        type = new Type(Type.PrimitiveOrVoid.BOOLEAN);
        this.value = new Boolean(value);
    }
    /**
     * Creates a new instance of Value, type NULL.
     */
    public Literal() {
        type = new Type();
        value = null;
    }
    /**
     * Creates an integer literal, of a given integer type. It is checked
     * if the value is within the range defined by the type, if not,
     * a runtime exception is thrown.
     *
     * @param t                         integer type of the literal
     * @param v                         value of the literal
     */
    public Literal(Type t, long v) {
        if(t.isOfIntegerTypes()) {
            type = new Type(t);
            switch(t.getPrimitive()) {
                case BYTE:
                    if(v < Byte.MIN_VALUE || v > Byte.MAX_VALUE)
                        throw new RuntimeException("out of range");
                    value = new Byte((byte)v);
                    break;

                case CHAR:
                    if(v < Character.MIN_VALUE || v > Character.MAX_VALUE)
                        throw new RuntimeException("out of range");
                    value = new Character((char)v);
                    break;

                case SHORT:
                    if(v < Short.MIN_VALUE || v > Short.MAX_VALUE)
                        throw new RuntimeException("out of range");
                    value = new Short((short)v);
                    break;

                case INT:
                    value = new Integer(CompilerUtils.toInt(v));
                    break;

                case LONG:
                    value = new Long(v);
                    break;

                default:
                    throw new RuntimeException("unknown type");
            }
        } else
            throw new RuntimeException("not integer type");
    }
    /**
     * Creates a floating point literal, of a given floating point
     * type. It is checked
     * if the value is within the range defined by the type, if not,
     * a runtime exception is thrown.
     *
     * @param t                         floating point type of the literal
     * @param v                         value of the literal, may include
     *                                  infinities, which will be converted
     *                                  to infinities of the type <code>t</code>
     *                                  regardless of the precision of
     *                                  <code>t</code>
     */
    public Literal(Type t, double v) {
        if(t.isOfFloatingPointTypes()) {
            type = new Type(t);
            switch(t.getPrimitive()) {
                case FLOAT:
                    if(v == Double.POSITIVE_INFINITY)
                        value = new Float(Float.POSITIVE_INFINITY);
                    else if(v == Double.NEGATIVE_INFINITY)
                        value = new Float(Float.NEGATIVE_INFINITY);
                    else if(v < -Float.MAX_VALUE || v > Float.MAX_VALUE)
                        throw new RuntimeException("out of range");
                    value = new Float((float)v);
                    break;

                case DOUBLE:
                    value = new Double(v);
                    break;

                default:
                    throw new RuntimeException("unknown type");
            }
        } else
            throw new RuntimeException("not integer type");
    }
    /**
     * This literal replaces a call to a marker method with an annotation
     * <code>a</code>, and all its parameters evaluated to arithmetic
     * constants, passed in <code>args</code>. The replaced marker
     * method is required to return an arithmetic constant. The literal
     * is expected to evemtually evaluate, i. e. at runtime, to an arithmetic
     * value as well.<br>
     * 
     * This call is used only by the backends, which want certain
     * marker calls to be constants for them.<br>
     * 
     * If a marker call has more than a single annotation, the one chosen
     * should be the one expected by the rest of the backend. It is typically the
     * most characteristic for a given marker method.<br>
     * 
     * Even that the annotation types occur only in the backends, the backends may
     * use the optimizer, so the optimizing routines should be aware of these
     * special types of constants.<br>
     * 
     * @param a annotation of the method called by the replaced marker call
     * @param value arguments of the call and expected range
     */
    public Literal(CompilerAnnotation a, AnnotationValue value) {
        type = new Type(new NameList(a.toAnnotationString()));
        this.value = value;
    }
    /**
     * A copying constructor. Consider using <code>clone()</code>
     * instead, as <code>Literal</code> has subclasses.
     *
     * @param l                         value to copy
     */
    public Literal(Literal l) {
        if(l instanceof RuntimeValue && !(this instanceof RuntimeValue))
            throw new RuntimeException("can not create a runtime value out of " +
                    "a literal");
        type = new Type(l.type);
        value = l.value;
    }
    /**
     * Returns the value of an INTEGER literal.
     *
     * @return                          integer value
     */
    public byte getByte() {
        return ((Byte)value).byteValue();
    }
    /**
     * Returns the value of a SHORT literal.
     *
     * @return                          short value
     */
    public short getShort() {
        return ((Short)value).shortValue();
    }
    /**
     * Returns the value of an INTEGER literal.
     *
     * @return                          integer value
     */
    public int getInteger() {
        return ((Integer)value).intValue();
    }
    /**
     * Returns the value of a LONG literal.
     *
     * @return                          long value
     */
    public long getLong() {
        // the type is checked because the object references
        // also have Long as values
        if(type.getPrimitive() == Type.PrimitiveOrVoid.LONG)
            return ((Long)value).longValue();
        else
            throw new RuntimeException("not a primitive LONG literal");
    }
    /**
     * Returns the value of a FLOAT literal.
     *
     * @return                          float value
     */
    public float getFloat() {
        return ((Float)value).floatValue();
    }
    /**
     * Returns the value of a DOUBLE literal.
     *
     * @return                          double value
     */
    public double getDouble() {
        return ((Double)value).doubleValue();
    }
    /**
     * Returns the value of a CHARACTER literal.
     *
     * @return                          character value
     */
    public char getCharacter() {
        return ((Character)value).charValue();
    }
    /**
     * Returns the value of a STRING literal.
     *
     * @return                          string value
     */
    public String getString() {
        return (String)value;
    }
    /**
     * Returns the value of a BOOLEAN literal.
     *
     * @return                          boolean value
     */
    public boolean getBoolean() {
if(!(value instanceof Boolean))
    value = value;
        return ((Boolean)value).booleanValue();
    }
    /**
     * Returns the ANNOTATION value.
     *
     * @return                          annotation value
     */
    public AnnotationValue getAnnotationValue() {
        return (AnnotationValue)value;
    }
    /**
     * Returns if this value represents the constant "null".
     *
     * @return                          if is the constant "null"
     */
    public boolean isNull() {
        return type.isNull();
    }
    /*
     * If this literal has an integer value, that is, if it is one of the
     * following primitive types: CHAR, SHORT, INT or LONG.
     * 
     * @return
     */
    /*
    public boolean isOfIntegerValue() {
        if(type.isPrimitive()) {
            switch(type.getPrimitive()) {
                case BYTE:
                case CHAR:
                case SHORT:
                case INT:
                case LONG:
                    return true;

                default:
                    return false;
            }
        } else
            return false;
     }
     */
    /**
     * Returns the value of any integer literal as a long int value.<br>
     *
     * If this literal is not of integer types, a runtime exception
     * is thrown.
     *
     * @return                          long value
     */
    public long getMaxPrecisionInteger() {
        long out;
/*if(type == null || type.getPrimitive() == null)
    type = type;*/
        PrimitiveOrVoid p = type.getPrimitive();
        if(p == null)
            throw new RuntimeException("not an arithmetic type");
        switch(p) {
            case BYTE:
                out = getByte();
                break;

            case CHAR:
                out = getCharacter();
                break;

            case SHORT:
                out = getShort();
                break;

            case INT:
                out = getInteger();
                break;

            case LONG:
                out = getLong();
                break;

            default:
                throw new RuntimeException("literal is not of integer types");
        }
        return out;
    }
    /**
     * Returns the value of any integer or floating point literal as a
     * double value.<br>
     *
     * If this literal is not of integer or floating point types, a
     * runtime exception is thrown.
     *
     * @return                          long value
     */
    public double getMaxPrecisionFloatingPoint() {
        double out;
        PrimitiveOrVoid p = type.getPrimitive();
        if(p == null) {
            type.isProbabilistic();
            throw new RuntimeException("not an arithmetic type");
        }
        switch(p) {
            case BYTE:
            case CHAR:
            case SHORT:
            case INT:
            case LONG:
                out = getMaxPrecisionInteger();
                break;

            case FLOAT:
                out = getFloat();
                break;

            case DOUBLE:
                out = getDouble();
                break;

            default:
                throw new RuntimeException("literal is not of integer or floating point types");
        }
        return out;
    }
    /**
     * Returns if this literal has arithmetic value lesser than that of
     * the other literal. Both literals must be of arithmetic type,
     * or a runtime exception is thrown.<br>
     *
     * This method, unlike methods in <code>CodeArithmetics</code>, does not
     * require an operation to be constructed, and is generally used only for
     * checking primitive bounds.
     *
     * @param right                     compared literal
     * @return                          if this literal is &lt; <code>right</code>
     */
    public boolean arithmeticLessThan(Literal right) {
if(type == null || right == null || right.type == null)
    type = type;
        int maxOperandPrecision = Math.max(
                type.numericPrecision(),
                right.type.numericPrecision());
        if(maxOperandPrecision >= Type.NumericPrecision.FLOAT) {
            // float operation
            double leftD = getMaxPrecisionFloatingPoint();
            double rightD = right.getMaxPrecisionFloatingPoint();
            return leftD < rightD;
        } else if(maxOperandPrecision > 0) {
            // integer operation
            long leftL = getMaxPrecisionInteger();
            long rightL = right.getMaxPrecisionInteger();
            return leftL < rightL;
        } else
            throw new RuntimeException("not arithmetic types");
    }
    /**
     * Returns if this literal has arithmetic value lesser than that of
     * the other literal. Both literals must be of arithmetic type,
     * or a runtime exception is thrown.<br>
     *
     * This method, unlike methods in <code>CodeArithmetics</code>, does not
     * require an operation to be constructed, and is generally used only for
     * checking primitive bounds.
     *
     * @param right                     compared literal
     * @return                          if this literal is &lt; <code>right</code>
     */
    public boolean arithmeticEqualTo(Literal right) {
        int maxOperandPrecision = Math.max(
                type.numericPrecision(),
                right.type.numericPrecision());
        if(maxOperandPrecision >= Type.NumericPrecision.FLOAT) {
            // float operation
            double leftD = getMaxPrecisionFloatingPoint();
            double rightD = right.getMaxPrecisionFloatingPoint();
            return leftD == rightD;
        } else if(maxOperandPrecision > 0) {
            // integer operation
            long leftL = getMaxPrecisionInteger();
            long rightL = right.getMaxPrecisionInteger();
            return leftL == rightL;
        } else
            throw new RuntimeException("not arithmetic types");
    }
    /**
     * Returns the lesser of two literals.
     *
     * @param v1                        first literal
     * @param v2                        second literal
     * @return                          if <code>v1</code> &lt; <code>v2</code>
     *                                  then <code>v1</code>, otherwise <code>v2</code>
     */
    public static Literal min(Literal v1, Literal v2) {
        if(v1.arithmeticLessThan(v2))
            return v1;
        else
            return v2;
    }
    /**
     * Returns the greater of two literals.
     *
     * @param v1                        first literal
     * @param v2                        second literal
     * @return                          if <code>v1</code> &gt;= <code>v2</code>
     *                                  then <code>v1</code>, otherwise <code>v2</code>
     */
    public static Literal max(Literal v1, Literal v2) {
        if(!v1.arithmeticLessThan(v2))
            return v1;
        else
            return v2;
    }
    /**
     * Returns a negated value of this literal. The returned literal
     * will be of the same type.
     *
     * @return                          negated value
     */
    public Literal neg() {
        int precision = type.numericPrecision();
        if(precision >= Type.NumericPrecision.FLOAT)
            // float operation
            return new Literal(type, -getMaxPrecisionFloatingPoint());
        else if(precision > 0)
            // integer operation
            return new Literal(type, -getMaxPrecisionInteger());
        else
            throw new RuntimeException("not arithmetic types");
    }
    /**
     * Returns an absolute value of this literal. The returned literal
     * will be of the same type.
     *
     * @return                          absolute value
     */
    public Literal abs() {
        Literal negated = neg();
        return Literal.max(this, negated);
    }
    @Override
    public boolean equals(Object o) {
        if(o instanceof ConditionalLiteral)
            return false;
        if(!(o instanceof Literal))
            throw new RuntimeException("invalid comparison");
        boolean equals = true;
        Literal l = (Literal)o;
        if(!type.equals(l.type))
            equals = false;
        else {
            if((value == null) != (l.value == null))
                equals = false;
            else if(value != null && l.value != null) {
                if(!value.equals(l.value))
                    equals = false;
            }
        }
        return equals;
    }
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 23 * hash + (this.type != null ? this.type.hashCode() : 0);
        hash = 23 * hash + (this.value != null ? this.value.hashCode() : 0);
        return hash;
    }
    /**
     * Returns what Java would return, if this literal would be
     * added to "".
     * 
     * @return Java--style string representation
     */
    public String toJavaString() {
        if(type.isPrimitive())
            switch(type.getPrimitive()) {
                case INT:
                    return String.valueOf(getInteger());

                case BYTE:
                    return String.valueOf(getByte());

                case SHORT:
                    return String.valueOf(getShort());

                case LONG:
                    return String.valueOf(getLong());

                case FLOAT:
                    return String.valueOf(getFloat());

                case DOUBLE:
                    return String.valueOf(getDouble());

                case CHAR:
                    return String.valueOf(getCharacter());

                case BOOLEAN:
                    return String.valueOf(getBoolean());

                default:
                    throw new RuntimeException("unknown type");
            }
        else if(// type.type instanceof NameList &&
                // type.getJava().toString().equals("java.lang.String") &&
                value instanceof String)
            return getString();
        else if(isNull())
            // the constant "null"
            return "null";
        else
            throw new RuntimeException("unknown type of a literal");
    }
    @Override
    public Literal clone() {
       Object o;
        try {
            o = super.clone();
        } catch(CloneNotSupportedException e) {
            // should not happen
            o = null;
        }
        Literal copy = (Literal)o;
        copy.type = new Type(type);
        copy.value = value;
        return copy;
    }
    @Override
    public String toString() {
        if(type.isPrimitive())
            switch(type.getPrimitive()) {
                case INT:
                    return String.valueOf(getInteger());

                case BYTE:
                    return String.valueOf(getByte()) + "B";

                case SHORT:
                    return String.valueOf(getShort()) + "S";

                case LONG:
                    return String.valueOf(getLong()) + "L";

                case FLOAT:
                    return String.valueOf(getFloat()) + "f";

                case DOUBLE:
                    return String.valueOf(getDouble());

                case CHAR:
                    return "'" + String.valueOf(getCharacter()) + "'c";

                case BOOLEAN:
                    return String.valueOf(getBoolean());
                    
                default:
                    throw new RuntimeException("unknown type");
            }
        else if(// type.type instanceof NameList &&
                // type.getJava().toString().equals("java.lang.String") &&
                value instanceof String)
            return "\"" + getString() + "\"";
        else if(isNull())
            // the constant "null"
            return "<null>";
        else if(type.isAnnotation()) {
            String name = ((NameList)type.type).toString();
            return name + "[" + value.toString() + "]";
        } else
            throw new RuntimeException("unknown type of a literal");
    }
}
