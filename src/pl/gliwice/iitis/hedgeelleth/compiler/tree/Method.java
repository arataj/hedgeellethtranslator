/*
 * Method.java
 *
 * Created on Jan 16, 2008, 11:15:21 AM
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.CommentTag;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.clazz.AbstractJavaClass;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.flags.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.Block;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.scope.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.CodeMethod;
import pl.gliwice.iitis.hedgeelleth.compiler.VariableInitializer;
import pl.gliwice.iitis.hedgeelleth.compiler.primitiverange.PrimitiveRangeDescription;
import pl.gliwice.iitis.hedgeelleth.compiler.primitiverange.VariablePrimitiveRange;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * A method or a constructor.<br>
 * 
 * If a method is non--static, it expects its object as the first
 * argument. The argument is not reflected in the list of arguments
 * of the method, nor in the method signature. The argument is
 * not also present in the list of arguments of <code>CallExpression</code>.
 * It is inserted into <code>CodeOpCall</code> when generating code.<br>
 * 
 * If a method returns a non--void value, it has an additional
 * variable named <code>Method.LOCAL_RETURN</code> that contains the return
 * value.<br>
 *
 * Throughout the translator, by overriding often the following is meant:
 * for abstract methods implementing, for non--abstract methods overriding,
 * as both implementing and overriding is often treated equally within
 * the translation. In particular, <code>MethodFlags.allowOverride</code>
 * and the field <code>overridings</code> of this class treat these two
 * concepts equally, the difference can be found in testing if the
 * supermethod is abstract.
 * The compiler error messages differentiate these two concepts though,
 * that is, implementing is never called overriding in these messages.<br>
 *
 * Natural order of the java classes is based on their stream positions.
 *
 * @author Artur Rataj
 */
public class Method implements Typed, SourcePosition, VariableOwner /*, Comparable<Method>*/ {
    /**
     * Name of a local variable containing "this" object of
     * a non--static method.
     */
    public static final String LOCAL_THIS = "#this";
    /**
     * Name of a local variable containing the returned value.
     */
    public static final String LOCAL_RETURN = "#retval";

    /**
     * Class of this method.
     */
    public AbstractJavaClass owner;
    /**
     * Scope of this method, contains arguments. Children of the scope
     * contain local variables.
     */
    public MethodScope scope;
    /**
     * Name of this method.
     */
    public String name;
    /**
     * Return type.
     */
    public Type ret;
    /**
     * Arguments. Does not contain "this" of non--static methods.
     */
    public List<Variable> arg;
    /**
     * A return variable, null for void methods.
     */
    public Variable returnLocal;
    /**
     * Argument "this" of non--static methods. It is not in the list
     * <code>arg</code>.
     */
    public Variable objectThis;
    /**
     * Code of this method, null for an abstract method.
     */
    public Block code;
    /**
     * Size of the argument range code at the beginning of a
     * non--synchronised method, or in thr case of a synchronised
     * method, at the beginning of the block directly nested
     * within <code>SynchronizedStatement</code>, which in
     * turn is at the beginning of such a synchronised method.
     */
    public int argRangeCodeSize;
    /**
     * Signature of this method, must be local.
     */
    public MethodSignature signature;
    /**
     * Flags of this method.
     */
    public MethodFlags flags;
    /**
     * Annotations, empty if none.
     */
    public List<String> annotations;
    /**
     * Primitive range of this method return value, null for none.<br>
     *
     * This is copied within the constructor into primitive range of the
     * return variable.
     */
    public VariablePrimitiveRange range;

    /**
     * Methods implementing or overriding this one, a key is the class
     * of an implementing/overriding method. Set during semantic check.<br>
     *
     * Only a non--abstract method can implement or override another one,
     * thus, this list does not contain abstract methods.
     *
     * This map is used when choosing a correct method to call.<br>
     *
     * The keys before interpreting points only to classes that actually have
     * a method with the same signature declared. During interpreting the
     * keys are completed in <code>CodeMethod.getVirtual</code> as described
     * in docs of <code>CodeMethod.getVirtual</code>.
     */
    public Map<AbstractJavaClass, Method> overridings;
    /**
   * All super methods, including the interface ones, that are direct,
   * that is, there is no intervening method in some intervening class.
   * Empty list for none. Used for semantic check. The condition of
   * being direct is to reduce the number of possible error messages.
   */
    public Map<AbstractJavaClass, Method> directSuperMethods;
    /**
   * All submethods, including the interface ones, that are direct,
   * that is, there is no intervening method in some intervening class.
   * Empty list for none. Used for semantic check. The condition of
   * being direct is to reduce the number of possible error messages.
   */
    public Map<AbstractJavaClass, Method> directSubMethods;
    /**
     * A special comment directly before the method, in the case of
     * Java a `**' comment. Null if none. Its only use is to be copied
     * to <code>CodeMethod.specialComment</code>.
     */
    public String specialComment;
    /**
     * Tags, empty list for no tags. By default an empty list.
     */
    public List<CommentTag> tags;
    /**
     * Position of this method in the parsed stream.
     */
    protected StreamPos pos;
    /**
     * A counter for creating internal local variables.
     */
    int newInternalLocalId;
    /**
     * Code object of this method.
     */
    public CodeMethod codeMethod;

    /**
     * Creates a new method that is just an unique value of some
     * constant.<br>
     * 
     * @param name                  a name that represents the method
     */
    public Method(String name) {
        initializeMethod();
        this.name = name;
     }
    /**
     * Creates a new instance of Method.
     * 
     * @param pos                   position in the parsed stream
     * @param javaClass             java class this method belongs to
     * @param name                  name of this method, can not contain
     *                              any class information
     * @param ret                   return type
     * @param initializers          arguments, can be null for no arguments,
     *                              the variables are added to this method's scope,
     *                              variable owners of the variables are
     *                              set to this method, "this" of non--static
     *                              methods should not be on this list, it is
     *                              added later to <code>CodeMethod</code>
     * @param range                     primitive range of the return value,
     *                              null for none
     * @param flags                 modifiers
     */
    public Method(StreamPos pos, AbstractJavaClass javaClass, String name,
                Type ret, List<VariableInitializer> initializers, MethodFlags flags,
                VariablePrimitiveRange range) throws ParseException {
        initializeMethod();
        setStreamPos(pos);
        if(name.indexOf('.') != -1)
            throw new RuntimeException("invalid class name");
        this.owner = javaClass;
        this.name = name;
        setResultType(ret);
        if(initializers == null)
           initializers = new LinkedList<>();
        this.flags = flags;
        arg = new LinkedList<>();
        for(VariableInitializer v : initializers)
            arg.add(v.variable);
        refreshSignature();
        scope = new MethodScope(this, javaClass.scope,
               signature.name);
        if(this.flags.context != Context.NON_STATIC)
            objectThis = null;
        else {
            // non--static method has a local "this" denoting
            // the method's object
            objectThis = new Variable(null, this,
                    new Type(javaClass.getQualifiedName()),
                    new LocalFlags(), Method.LOCAL_THIS,
                    true, Variable.Category.INTERNAL_NOT_RANGE);
            scope.addLocalInThis(objectThis);
        }
        if(!ret.isVoid()) {
            // method returning a value have a local LOCAL_RETURN
            // denoting the returned value
            returnLocal = new Variable(null, this,
                    new Type(ret), new LocalFlags(), LOCAL_RETURN,
                    true, Variable.Category.INTERNAL_NOT_RANGE);
            if(range != null) {
                returnLocal.primitiveRange = range;
            }
            scope.addLocalInThis(returnLocal);
        }
        for(VariableInitializer v : initializers) {
            scope.addLocalInThis(v.variable);
            // getVariableOwner() returns this method as the owner
            v.variable.owner = scope.getBoundingVariableOwner();
        }
    }
    /**
     * Common inititialization used by constructors.
     */
    private void initializeMethod() {
        newInternalLocalId = 0;
        specialComment = null;
        tags = new LinkedList<CommentTag>();
        directSuperMethods = new HashMap<AbstractJavaClass, Method>();
        directSubMethods = new HashMap<AbstractJavaClass, Method>();
        annotations = new LinkedList<String>();
        overridings = new HashMap<AbstractJavaClass, Method>();
    }
    /**
     * Refreshes the signature on basis of current arguments.<br>
     * 
     * Creates a new signature object, as opposed to modifying the
     * previous signature object.
     */
    public void refreshSignature() {
        if(owner == null) {
            // a signature of a fake method
            signature = new MethodSignature(null, name, arg);
        } else
            signature = new MethodSignature(owner.frontend, name, arg);
    }
    /**
     * Returns a unique identifier for creating internal local
     * variables.
     */
    public synchronized String getNewInternalLocalId() {
        return "#t" + newInternalLocalId++;
    }
    /**
     * If this method matches a call. Throws parse exception if some
     * type has not been found.
     * 
     * @param identifier            signature representing the call
     * @return                      if this method matches a given
     *                              call
     */
    public boolean matchesCall(MethodSignature signature) throws ParseException {
        List<Type> types = signature.getTypes();
        if(types.size() != arg.size())
            return false;
        else {
            Iterator<Variable> i = arg.iterator();
            boolean castable = true;
            for(Type t : types) {
                Variable v = i.next();
                if(!Type.castable(scope, t, v.getResultType(), null)) {
                    castable = false;
                    break;
                }
            }
            return castable;
        }
    }
    /**
     * Returns a textual method declaration description, given its
     * name, list of arguments and annotations.<br>
     * 
     * Wraps non--Java types to be entries from <code>MessageStyle</code>.
     * 
     * @param name                  name
     * @param arg                   arguments
     * @param bracket passed to <code>Type.getNameString()</code>
     * @return                      description
     */
    public static String getDeclarationDescription(String name,
            List<? extends Typed> arg, List<String> annotations,
            boolean bracket) {
        String s = name + "(";
        if(arg != null) {
            boolean first = true;
            for(Typed v : arg) {
                if(!first)
                    s += ", ";
                String t = v.getResultType().toNameString(bracket);
                //if(!v.getResultType().isJava())
                //    t = "${" + t + "}";
                s = s + t;
                first = false;
            }
        }
        s += ")";
        if(annotations != null) {
            for(String a : annotations)
                s = s + " @" + a;
        }
        return s;
    }
    /**
     * Returns a textual declaration description of
     * this method. The description contains the method's name,
     * arguments and return type. The name is not qualified.<br>
     * 
     * Wraps non--Java types to be entries from <code>MessageStyle</code>.
     * 
     * @param bracket passed to <code>Type.getNameString()</code>
     * @return                      textual description
     */
    public String getDeclarationDescription(boolean bracket) {
        return getResultType().toNameString() + " " +
            getDeclarationDescription(name, arg, annotations, bracket);
    }
    /**
     * Recursively searches a scope for local variables.
     * 
     * @param scope root scope
     * @return locals
     */
    protected List<Variable> getLocals(AbstractLocalOwnerScope scope) {
        List<Variable> outVariables = new LinkedList<Variable>(
                scope.locals.values());
        if(scope instanceof MethodScope) {
            BlockScope blockScope = ((MethodScope)scope).blockScope;
            if(blockScope != null)
                // this method is not abstract, search for variables
                // in the code
                outVariables.addAll(
                        getLocals(blockScope));
        } else
            for(BlockScope s : ((BlockScope)scope).innerBlockScopes)
                outVariables.addAll(
                    getLocals(s));
        return outVariables;
    }
    /**
     * Returns all locals of this method, including arguments.
     * 
     * @return locals
     */
    public List<Variable> getLocals() {
        return getLocals(scope);
    }
    @Override
    public final void setResultType(Type resultType) {
        ret = resultType;
    }
    @Override
    public final Type getResultType() {
        return ret;
    }
    @Override
    public final void setStreamPos(StreamPos pos) {
        this.pos = pos;
    }
    @Override
    public final StreamPos getStreamPos() {
        return pos;
    }
    /*
    @Override
    public int compareTo(Method o) {
        return getStreamPos().compareTo(o.getStreamPos());
    }
     */
    @Override
    public String toString() {
        if(signature == null)
            // this is just a constant
            return name;
        else
            return ret.toString() + " " + signature + " " +
                    (code == null ? "abstract" : code.toString());
    }
}
