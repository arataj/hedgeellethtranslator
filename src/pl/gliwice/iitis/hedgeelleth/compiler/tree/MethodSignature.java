/*
 * Signature.java
 *
 * Created on Jan 27, 2009
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.clazz.AbstractJavaClass;
import pl.gliwice.iitis.hedgeelleth.compiler.util.CompilerUtils;
import pl.gliwice.iitis.hedgeelleth.modules.AbstractFrontend;

/**
 * A method signature. A signature can be either qualified or
 * partial. The former contains a qualified class name, the
 * latter contains an unqualified class name or no class name
 * at all. A signature that does not contain the class name
 * is a local signature. A local signature is often
 * used to key a method, as there is only one possible contents
 * of a local signature per method with a java class.<br>
 *
 * Because the signature format is somewhat complex, it has its
 * own type, and not just uses <code>String</code> or
 * <code>NameList</code>. Still, the whole instance of a signature
 * is fully presented only by the <code>name</code> field.<br>
 *
 * The method <code>equals</code> discriminates objects only by
 * the contents of by the <code>name</code> field.
 *
 * @author Artur Rataj
 */
public class MethodSignature {
    /**
     * Separator between the method name and the arguments in a
     * method signature.
     */
    public static final char SIGNATURE_ARGUMENTS_SEPARATOR = '/';

    /**
     * Frontend of the language this signature belongs to, can be null if
     * after semantic check or the signature does not contain any java class
     * arguments.<br>
     * 
     * It is needed to resolve inter--language field and method references.
     */
    public AbstractFrontend frontend;
    /**
     * Name representing this signature, that is, a method name and
     * subsequent types of arguments.
     */
    public String name;

    /**
     * Creates a new instance of Signature.
     *
     * @param frontend                  frontend of the language
     *                                  the signature belongs to,
     *                                  can be null if after semantic check
     * @param name                      name representing this
     *                                  signature
     */
    public MethodSignature(AbstractFrontend frontend, String name) {
        initMethodSignature(frontend, name);
    }
    /**
     * Returns a method signature, given a name and arguments.
     *
     * @param frontend                  frontend of the language
     *                                  the signature belongs to,
     *                                  can be null if after semantic check
     * @param name                      method name, can contain
     *                                  the class part
     * @param arg                       arguments
     * @return                          signature string
     */
    public MethodSignature(AbstractFrontend frontend, String methodName,
            List<? extends Typed> args) {
        String s = methodName + SIGNATURE_ARGUMENTS_SEPARATOR;
        if(args != null) {
            int count = 0;
            for(Typed v : args) {
                if(count != 0)
                    s = s + ",";
                s = s + v.getResultType().toString();
                ++count;
            }
        }
        initMethodSignature(frontend, s);
    }
    /**
     * Initialization routines for a signature.
     *
     * @param frontend                  frontend of the language
     *                                  the signature belongs to,
     *                                  can be null if after semantic check
     * @param name                      name representing this
     *                                  signature
     */
    private void initMethodSignature(AbstractFrontend frontend, String name) {
        this.frontend = frontend;
        this.name = name;
    }
    /**
     * Returns the arguments part of the signature, without the
     * separator. If a method does not have any arguments, an
     * empty string is returned.
     *
     * @return                          arguments part of the
     *                                  signature string
     */
    public String extractArgString() {
        return name.substring(
                name.indexOf(SIGNATURE_ARGUMENTS_SEPARATOR) + 1,
                name.length());
    }
    /**
     * Returns the class part of the signature, without the
     * separator. If the signature does not have a class specified,
     * an empty string is returned.
     *
     * @return                          class part of the
     *                                  signature string
     */
    public String extractClassString() {
        String s = name.substring(
                0, name.indexOf(SIGNATURE_ARGUMENTS_SEPARATOR));
        int pos = s.lastIndexOf('.');
        if(pos == -1)
            s = "";
        else
            s = s.substring(0, pos);
        return s;
    }
    /*
     * It is a wrapper to <code>extractClassString(String)</code>,
     * that changes both the argument and the return value
     * to <code>NameList</code>.
     *
     * @return                          class part of the
     *                                  signature string, given as
     *                                  name list
     */
    /*
    public static NameList extractClassString_(NameList signature) {
        return new NameList(new MethodSignature(
                signature.toString()).extractClassString());
    }
     */
    /**
     * Strips the class part of the signature, returning thus a
     * string that begins with a method's unqualified name.
     *
     * @return                          signature part that begins with
     *                                  method's unqualified name
     */
    public String extractLocalString() {
        String s = name.substring(
                0, name.indexOf(SIGNATURE_ARGUMENTS_SEPARATOR));
        int pos = s.lastIndexOf('.');
        if(pos == -1)
            s = name;
        else
            s = name.substring(pos + 1);
        return s;
    }
    /**
     * Strips the arguments part of the signature, returning thus a
     * string that ends with a method's name, without the
     * separator after the name.
     *
     * @return                          signature part that ends with
     *                                  method's name
     */
    public String extractQualifiedNameString() {
        return name.substring(
                0, name.indexOf(SIGNATURE_ARGUMENTS_SEPARATOR));
    }
    /**
     * Gets the unqualified method name part of the signature,
     * that is, what <code>extractQualifiedNameString()</code> returns,
     * but clipped the qualifying path.
     *
     * @return                          unqualified method name
     */
    public String extractUnqualifiedNameString() {
        String s = extractQualifiedNameString();
        int pos = s.lastIndexOf(".");
        if(pos == -1)
            return s;
        else
            return s.substring(pos + 1);
    }
    /**
     * Returns if this signature is local.
     *
     * @return                          if this signature is local
     */
    public boolean isLocal() {
        String s = name.substring(
                0, name.indexOf(SIGNATURE_ARGUMENTS_SEPARATOR));
        int pos = s.lastIndexOf('.');
        return pos == -1;
    }
    /**
     * Creates a new local signature out of this signature. The
     * returned signature contains thus the name as returned by
     * <code>extractLocalString()</code>.
     *
     * @return                          a local signature
     */
    public MethodSignature newLocalSignature() {
        return new MethodSignature(frontend, extractLocalString());
    }
    /**
     * Creates a fully qualified signature out of this signature,
     * given the method's owner.
     *
     * @param clazz                     class, must be already parsed
     * @return                          a fully qualified signature
     */
    public MethodSignature newQualifiedSignature(AbstractJavaClass clazz) {
        return new MethodSignature(frontend,
                clazz.getQualifiedName() + "." + extractLocalString());
    }
    /**
     * Returns the list of types of arguments of a method, given
     * the method's signature.<br>
     *
     * @return                      list of types, the types'
     *                              objects are newly created
     */
    public List<Type> getTypes() {
        List<Type> types = new LinkedList<Type>();
        String argString = extractArgString();
        Scanner sc = CompilerUtils.newScanner(argString).useDelimiter(",");
        while(sc.hasNext()) {
            String s = sc.next();
            Type t = Type.newTypeFromString(s);
            types.add(t);
        }
        return types;
    }
    /**
     * Translates this signature to new signature in another frontend.
     * It means that the types of arguments will be translated
     * to these in another language. The new signature's frontend
     * is changed to <code>newFrontend</code> as well. This signature
     * is not affected by this method.<br>
     *
     * The names in the types never refer to the unified names assigned
     * during code generation. Thus, this method should only be used
     * before code generation.
     *
     * @param newFrontend               new frontend
     * @return                          newly created, translated signature
     */
    public MethodSignature newTranslationToOtherFrontend(
            AbstractFrontend newFrontend) {
        List<Type> types = getTypes();
        for(Type t : types) {
            if(t.isJava()) {
                String n = frontend.translateClassName(t.getJava().toString(),
                        newFrontend);
                t.type = new NameList(n);
            }
        }
        return new MethodSignature(newFrontend, extractQualifiedNameString(),
                types);
    }
    /**
     * Returns a textual declaration description if a method that
     * has this signature. Strips any class information.<br>
     *
     * As this method does not use scopes, as opposed to
     * <code>getTypes</code>, then no actual type lookup is performed,
     * and the returned type names are in the
     * exact form as found in the signature.<br>
     *
     * Wraps non--Java types to be entries from <code>MessageStyle</code>.
     * Note that the signature does not contain possible
     * method annotations.<br>
     *
     * @param signature             signature
     * @return                      description
     */
    public String getDeclarationDescription() {
        String out = newLocalSignature().extractQualifiedNameString() + "(";
        List<Type> types = getTypes();
        boolean first = true;
        for(Type t : types) {
            if(!first)
                out += ", ";
            String s = t.toNameString();
            //if(!t.isJava())
            //    s = "${" + s + "}";
            out += s;
            first = false;
        }
        return out + ")";
    }
    @Override
    public boolean equals(Object o) {
        if(o instanceof MethodSignature) {
            MethodSignature s = (MethodSignature)o;
            return name.equals(s.name);
        } else
            return false;
    }
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + (this.name != null ? this.name.hashCode() : 0);
        return hash;
    }
    @Override
    public String toString() {
        return name;
    }
}
