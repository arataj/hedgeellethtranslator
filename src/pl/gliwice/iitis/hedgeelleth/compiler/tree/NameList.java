/*
 * NameList.java
 *
 * Created on Jan 16, 2008, 12:46:55 PM
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree;

import java.util.*;

/**
 * A list of strings, representing a name.<br>
 * 
 * Line and column are also used to determine if the respective object
 * referenced by this name is not further in the stream within some scope.
 * 
 * The equality is determined only by the list of strings.
 * 
 * @author Artur Rataj
 */
public class NameList {
    /**
     * Strings.
     */
    public List<String> list;
    /**
     * Line of this name in the parser stream, or -1 for none.
     */
    public int line = -1;
    /**
     * Column of this name in the parser stream, or -1 for none.
     */
    public int column = -1;
    
    /**
     * Creates a new instance of NameList. The list is
     * copied into this object list.
     * 
     * @param list                      list of String
     */
    public NameList(List<String> list) {
        this.list = new LinkedList<>(list);
    }
    /**
     * Creates a new instance of NameList. The list is
     * copied into this object list.
     * 
     * @param list                      list of String
     */
    public NameList(List<String> list, int line, int column) {
        this.list = new LinkedList<String>(list);
        this.line = line;
        this.column = column;
    }
    /**
     * Creates a new instance of NameList.
     * 
     * @param name                      NameList
     */
    public NameList(NameList name) {
        this(name.list);
    }
    /**
     * Creates a new instance of NameList.
     * 
     * @param string                    String, delimiter is '.'
     * @param delimiter                 delimiter, null for none
     */
    public NameList(String string, Character delimiter) {
        /*
        // <code>Scanner</code> was too slow
        List<String> list2 = new LinkedList<String>();
        Scanner s = new Scanner(string).useDelimiter("\\.");
        while(s.hasNext())
            list2.add(s.next());
            */
        list = new LinkedList<>();
        if(delimiter == null)
            list.add(string);
        else {
            int pos = 0;
            if(!string.isEmpty())
                do {
                    int prevPos = pos;
                    pos = string.indexOf(delimiter, pos);
                    if(pos == -1)
                        pos = string.length();
                    if(prevPos == pos)
                        if(pos == 0)
                            throw new RuntimeException("`.' at the beginning");
                        else
                            throw new RuntimeException("adjacent `.'");
                    list.add(string.substring(prevPos, pos));
                    ++pos;
                    if(pos == string.length())
                        throw new RuntimeException("trailing `.'");
                } while(pos < string.length());
        }
    }
    /**
     * Creates a new instance of NameList, with the
     * delimiter of `.'.
     * 
     * @param string                    String, delimiter is '.'
     * @param delimiter                 delimiter, null for none
     */
    public NameList(String string) {
        this(string, '.');
    }
    /**
     * If this name is fully qualified and/or a nested class name.
     * 
     * @return                          if is fully qualified
     */
    public boolean isQualifiedOrNC() {
        return list.size() > 1;
    }
    /**
     * If there is some left part of this name list whose
     * String representation equals to prefix.
     * 
     * @param prefix                    prefix string
     * @param full                      name list
     * @return                          -1 if the part is not found,
     *                                  otherwise the number of segments
     *                                  that together are equal to
     *                                  the prefix
     */
    public int prefixEquals(String prefix) {
        int segmentsEqual = -1;
        String s = "";
        for(int segmentNum = 0; segmentNum < list.size(); ++segmentNum) {
            if(s.length() != 0)
                s = s + ".";
            s = s + list.get(segmentNum);
            if(s.equals(prefix)) {
                segmentsEqual = segmentNum + 1;
                break;
            }
        }
        return segmentsEqual;
    }
    /**
     * Returns the last n segments of this name list.
     * 
     * @param n                         number of segments
     * @return                          postfix name list
     */
    public NameList last(int n) {
        List<String> postfix = new LinkedList<String>();
        for(int segment = list.size() - n; segment < list.size(); ++segment)
            postfix.add(list.get(segment));
        return new NameList(postfix);
    }
    /**
     * Returns the last segment of this name list.
     * 
     * @return                          last string
     */
    public String last() {
        return list.get(list.size() - 1);
    }
    @Override
    public boolean equals(Object o) {
        if(!(o instanceof NameList))
            throw new RuntimeException("invalid comparison");
        boolean equals = true;
        NameList l = (NameList)o;
        if(list.size() != l.list.size())
            equals = false;
        else {
            Iterator<String> i = l.list.iterator();
            for(String s1 : list) {
                String s2 = i.next();
                if(!s1.equals(s2)) {
                    equals = false;
                    break;
                }
            }
        }
        return equals;
    }
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + (this.list != null ? this.list.hashCode() : 0);
        return hash;
    }
    @Override
    public String toString() {
        String s = "";
        for(String t : list) {
            if(s.length() != 0)
                s = s + ".";
            s = s + t;
        }
        return s;
    }
}
