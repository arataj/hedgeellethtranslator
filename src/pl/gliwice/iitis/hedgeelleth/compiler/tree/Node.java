/*
 * Node.java
 *
 * Created on Jan 15, 2008, 12:44:03 PM
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree;

import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;

/**
 * An abstract node of the syntax tree.
 * 
 * @author Artur Rataj
 */
public interface Node<T> {
    /**
     * Verbose compilation.
     */
    public final boolean VERBOSE = false;
    
    public T accept(Visitor v) throws CompilerException;
}
