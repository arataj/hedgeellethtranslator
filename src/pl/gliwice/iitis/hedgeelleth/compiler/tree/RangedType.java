/*
 * RangedType.java
 *
 * Created on Jul 17, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree;

import pl.gliwice.iitis.hedgeelleth.compiler.primitiverange.VariablePrimitiveRange;

/**
 * A type with an optional primitive range.
 * 
 * @author Artur Rataj
 */
public class RangedType implements Typed {
    /**
     * Type.
     */
    public Type type = null;
    /**
     * If this ranged type uses a primitive bound, then this field references
     * variable that holds the bound's value when this variable is
     * initialized.<br>
     *
     * As opposed to inherent type's bounds which are ignored except for
     * assignments of constants, this bound is
     * checked statically or at runtime if such checking is enabled.<br>
     *
     * This field can be non--null only for primitive types.
     */
    public VariablePrimitiveRange primitiveRange;
    
    @Override
    public final void setResultType(Type resultType) {
        type = resultType;
    }
    @Override
    public final Type getResultType() {
        return type;
    }
}
