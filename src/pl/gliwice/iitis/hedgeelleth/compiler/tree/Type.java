/*
 * Type.java
 *
 * Created on Jan 9, 2008, 1:45:39 PM
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.CompilerAnnotation;
import pl.gliwice.iitis.hedgeelleth.compiler.primitiverange.PrimitiveRangeDescription;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.clazz.AbstractJavaClass;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.scope.AbstractScope;
import pl.gliwice.iitis.hedgeelleth.compiler.util.CompilerUtils;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.modules.AbstractFrontend;

/**
 * A variable type or return type.<br>
 * 
 * To make an array type, construct this with a respective element type
 * and then set the dimension. It will convert a primitive to am array
 * type if needed.<br>
 * 
 * Note that void type and null type are two different things. Void
 * is constructed by using PrimitiveOrVoid.VOID and null by using a
 * constructor without any arguments.<br>
 * 
 * The names of special array and string classes are dependent on
 * the compilation stage -- if parser and semantic check use language--specific
 * names, then the names should be used on that stages. In and after code
 * generation standard names defined in <code>Type</code> are used.<br>
 *
 * Checking for equality in the case of Java types is guaranteed to work
 * only after the semantic check, with the exception of locals that are never
 * used in the code. It is because then it is guaranteed that the names of
 * java classes are already fully qualified.<br>
 * 
 * The system or special classes or packages can possess the `#' character in
 * their names.
 *
 * @author Artur Rataj
 */
public final class Type implements Typed {
    /**
     * System package. Special classes are moved to the package
     * during code generation.
     */
    public static final String SYSTEM_PACKAGE = "#system";
    /**
     * Name of the special object class.
     */
    public static final String OBJECT_CLASS_NAME = "#Object";
    /**
     * Fully qualified name of the special object class.
     */
    public static final String OBJECT_QUALIFIED_CLASS_NAME =
            SYSTEM_PACKAGE + "." + OBJECT_CLASS_NAME;
    /**
     * Name of the special array class.
     */
    public static final String ARRAY_CLASS_NAME = "#Array";
    /**
     * Fully qualified name of the special array class.
     */
    public static final String ARRAY_QUALIFIED_CLASS_NAME =
            SYSTEM_PACKAGE + "." + ARRAY_CLASS_NAME;
    /**
     * Name of the length field in the special array class.
     */
    public static final String ARRAY_CLASS_LENGTH_FIELD_NAME = "length";
    /**
     * Name of the special string class.
     */
    public static final String STRING_CLASS_NAME = "#String";
    /**
     * Fully qualified name of the special string class.
     */
    public static final String STRING_QUALIFIED_CLASS_NAME =
            SYSTEM_PACKAGE + "." + STRING_CLASS_NAME;
    /**
     * Name of the length field in the special string class.
     */
    public static final String STRING_CLASS_LENGTH_FIELD_NAME = "length";
    /**
     * Name of the special root exception class.
     */
    public static final String ROOT_EXCEPTION_CLASS_NAME = "#Exception";
    /**
     * Fully qualified name of the special root exception class.
     */
    public static final String ROOT_EXCEPTION_QUALIFIED_CLASS_NAME =
            SYSTEM_PACKAGE + "." + ROOT_EXCEPTION_CLASS_NAME;
    /**
     * Name of the message field in the special root exception class.
     */
    public static final String ROOT_EXCEPTION_CLASS_MESSAGE_FIELD_NAME = "message";
    /**
     * Name of the special unchecked exception class.
     */
    public static final String UNCHECKED_EXCEPTION_CLASS_NAME = "#Unchecked";
    /**
     * Fully qualified name of the special unchecked exception class.
     */
    public static final String UNCHECKED_EXCEPTION_QUALIFIED_CLASS_NAME =
            SYSTEM_PACKAGE + "." + UNCHECKED_EXCEPTION_CLASS_NAME;

    /**
     * Primitives types and VOID type.
     */
    public enum PrimitiveOrVoid {
        VOID,
        BOOLEAN,
        CHAR,
        BYTE,
        SHORT,
        INT,
        LONG,
        FLOAT,
        DOUBLE;

        /**
         * Returns the range of values of a given type. Throws a
         * runtime exception if not applicable.<br>
         * 
         * As defined in Java, boolean is not treated
         * as an arithmetic type. If treating it as one is required,
         * use directly the constructor <code>TypeRange(PrimitiveOrVoid,
         * boolean)</code>.
         *
         * @return                      minimum value
         */
        public TypeRange getRange() {
            return new TypeRange(this, false);
        }
    };
    /**
     * A type wildcard -- NONE, EXTENDS or SUPER.
     * 
     * @author Artur Rataj
     */
    public enum Wildcard {
        NONE,
        EXTENDS,
        SUPER,
    };
    /**
     * Defines arithmetic range of some primitive type, either custom
     * or according to Java.
     */
    public static class TypeRange {
        /**
         * Minimum value of integer type.
         */
        long minI;
        /**
         * Maximum value of integer type.
         */
        long maxI;
        /**
         * Minimum value of floating point type.
         */
        double minF;
        /**
         * Maximum value of floating point type.
         */
        double maxF;
        /**
         * True for floating point type, false for integer type.
         */
        boolean floating;

        /**
         * Constructs a new tye range, with bounds that default to 0.
         *
         * @param floating              true for floating point types
         *                              false for integer types
         */
        public TypeRange(boolean floating) {
            this.floating = floating;
        }
        /**
         * Constructs a new type range of some integer type.
         *
         * @param min                   minimum of the integer range
         * @param max                   maximum of the integer range
         */
        public TypeRange(long min, long max) {
            if(min > max)
                throw new RuntimeException("min must be <= max");
            minI = min;
            maxI = max;
            floating = false;
        }
        /**
         * Constructs a new type range of some floating point type.
         *
         * @param min                   minimum of the floating point range
         * @param max                   maximum of the floating point range
         */
        public TypeRange(double min, double max) {
            if(min > max)
                throw new RuntimeException("min must be <= max");
            minF = min;
            maxF = max;
            floating = true;
        }
        /**
         * Constructs a new type range, equal to a full range
         * of some primitive type, according to Java.<br>
         *
         * While boolean is not an arithmetic type in Java, it might
         * be in some backends, thus the parameter <code>allowBoolean</code>.
         *
         * @param p                     primitive type that has defined
         *                              range
         * @param allowBoolean          true if boolean should return
         *                              (&lt;0, 1&gt;), false if boolean
         *                              should cause a runtime exception;
         */
        public TypeRange(PrimitiveOrVoid p, boolean allowBoolean) {
            floating = false;
            switch(p) {
                case BYTE:
                    minI = -(1 << 7);
                    maxI = -minI - 1;
                    break;

                case CHAR:
                    minI = 0;
                    maxI = (1 << 16) - 1;
                    break;

                case SHORT:
                    minI = -(1 << 15);
                    maxI = -minI - 1;
                    break;

                case INT:
                    minI = -(1L << 31);
                    maxI = -minI - 1;
                    break;

                case LONG:
                    minI = -(1L << 63);
                    maxI = -minI - 1;
                    break;

                case FLOAT:
                    minF = -Float.MAX_VALUE;
                    maxF = -minF;
                    floating = true;
                    break;

                case DOUBLE:
                    minF = -Double.MAX_VALUE;
                    maxF = -minF;
                    floating = true;
                    break;

                case BOOLEAN:
                    if(allowBoolean) {
                        minI = 0;
                        maxI = 1;
                        floating = false;
                        break;
                    }
                    // fall through
                default:
                    throw new RuntimeException("no arithmetic range defined for type " +
                            p);
            }
        }
        /**
         * Creates a new instance of type range. This is a copying
         * constructor.
         *
         * @param r                     source type range
         */
        public TypeRange(TypeRange r) {
            minI = r.minI;
            maxI = r.maxI;
            minF = r.minF;
            maxF = r.maxF;
            floating = r.floating;
        }
        /**
         * Returns if is of a floating point type.
         * 
         * @return if <code>floating</code> is true
         */
        public boolean isFloating() {
            return floating;
        }
        /**
         * Sets minimum value of an integer type. If this range is not defined
         * for an integer type, a runtime exception is returned.
         *
         * @param min                   new minimum value
         */
        public void setMinI(long min) {
            if(!floating)
                minI = min;
            else
                throw new RuntimeException("not integer range");
        }
        /**
         * Sets maximum value of an integer type. If this range is not defined
         * for an integer type, a runtime exception is returned.
         *
         * @param max                   new maximum value
         */
        public void setMaxI(long max) {
            if(!floating)
                maxI = max;
            else
                throw new RuntimeException("not integer range");
        }
        /**
         * Sets minimum value of a floating point type. If this range is not defined
         * for a floating point type, a runtime exception is returned.
         *
         * @param min                   new minimum value
         */
        public void setMinF(double min) {
            if(floating)
                minF = min;
            else
                throw new RuntimeException("not floating point range");
        }
        /**
         * Sets maximum value of a floating point type. If this range is not defined
         * for a floating point type, a runtime exception is returned.
         *
         * @param max                   new maximum value
         */
        public void setMaxF(double max) {
            if(floating)
                maxF = max;
            else
                throw new RuntimeException("not floating point range");
        }
        /**
         * Returns minimum value of an integer type. If this range is not defined
         * for an integer type, a runtime exception is returned.
         *
         * @return                      minimum value
         */
        public long getMinI() {
            if(!floating)
                return minI;
            else
                throw new RuntimeException("not integer range");
        }
        /**
         * Returns maximum value of an integer type. If this range is not defined
         * for an integer type, a runtime exception is returned.
         *
         * @return                      maximum value
         */
        public long getMaxI() {
            if(!floating)
                return maxI;
            else
                throw new RuntimeException("not integer range");
        }
        /**
         * Returns minimum value of a floating point type. If this range is not defined
         * for a floating point type, a runtime exception is returned.
         *
         * @return                      minimum value
         */
        public double getMinF() {
            if(floating)
                return minF;
            else
                throw new RuntimeException("not floating point range");
        }
        /**
         * Returns maximum value of a floating point type. If this range is not defined
         * for a floating point type, a runtime exception is returned.
         *
         * @return                      maximum value
         */
        public double getMaxF() {
            if(floating)
                return maxF;
            else
                throw new RuntimeException("not floating point range");
        }
        /**
         * Returns value of minimum in the form of floating point value, irrespective
         * of whether it is an integer range or a floating point range.
         *
         * @return                      minimum value casted to floating point,
         *                              what may cause precision loss if this
         *                              range is integer and the value is large
         */
        public double getMinFloatingPoint() {
            if(floating)
                return minF;
            else
                return minI;
        }
        /**
         * Returns value of maximum in the form of floating point value, irrespective
         * of whether it is an integer range or a floating point range.
         *
         * @return                      maximum value casted to floating point,
         *                              what may cause precision loss if this
         *                              range is integer and the value is large
         */
        public double getMaxFloatingPoint() {
            if(floating)
                return maxF;
            else
                return maxI;
        }
        /**
         * Returns the minimum value wrapped in a literal.
         *
         * @return                      literal
         */
        public Literal getMinLiteral() {
            if(floating)
                return new Literal(minF);
            else
                return new Literal(minI);
        }
        /**
         * Returns the maximum value wrapped in a literal.
         *
         * @return                      literal
         */
        public Literal getMaxLiteral() {
            if(floating)
                return new Literal(maxF);
            else
                return new Literal(maxI);
        }
        /**
         * If two ranges are arithmetically equal. This can be true even if
         * one range is integer and the other is floating point, however,
         * precision loss is possible in such case, if some integer value
         * is large.
         *
         * @param r                     range to compare this range to
         * @return                      if arithmetically equal, taking into
         *                              account possible precision loss
         */
        public boolean arithmeticEqualTo(Type.TypeRange r) {
            if(floating || r.floating) {
                double minThis = getMinFloatingPoint();
                double minThat = r.getMinFloatingPoint();
                if(minThis != minThat)
                    return false;
                else {
                    double maxThis = getMaxFloatingPoint();
                    double maxThat = r.getMaxFloatingPoint();
                    return maxThis == maxThat;
                }
            } else
                return minI == r.minI &&
                        maxI == r.maxI;
        }
        /**
         * If this range fits into another one. Both ranges must be either
         * integer or floating point, otherwise a runtime exception is thrown.
         *
         * @param bound                 bounding range
         * @return                      if this range fits to some bounding
         *                              range
         */
        public boolean fitsTo(TypeRange bound) {
            if(floating != bound.floating)
                throw new RuntimeException("range mismatch");
            else {
                if(!floating)
                    return getMinI() >= bound.getMinI() && getMaxI() <= bound.getMaxI();
                else
                    return getMinF() >= bound.getMinF() && getMaxF() <= bound.getMaxF();
            }
        }
        @Override
        public String toString() {
            String s = PrimitiveRangeDescription.VARIABLE_LEFT_STRING;
            if(floating)
                s += getMinF() + ", " + getMaxF();
            else
                s += getMinI() + ", " + getMaxI();
            s += PrimitiveRangeDescription.VARIABLE_RIGHT_STRING;
            return s;
        }
    }

    /**
     * Note that BYTE and CHAR have the same precision value declared
     * here, and still no conversion between them is possible --
     * thus special code in <code>castable()</code> to check for that.
     */
    public static class NumericPrecision {
        public static final int CHAR = 1;
        public static final int BYTE = 1;
        public static final int SHORT = 2;
        public static final int INT = 3;
        public static final int LONG = 4;
        public static final int FLOAT = 5;
        public static final int DOUBLE = 6;

        /**
         * Converts numeric precision to type. The precision can not
         * be BYTE or CHAR, as these precision values are mutually
         * indistinguishable.
         *
         * @param precision             numeric precision
         * @return                      respective type
         */
        public static Type precisionToType(int precision) {
            if(precision == Type.NumericPrecision.BYTE)
                throw new RuntimeException("not indistinguishable from CHAR");
            else if(precision == Type.NumericPrecision.CHAR)
                throw new RuntimeException("not indistinguishable from BYTE");
            else if(precision == Type.NumericPrecision.SHORT)
                return new Type(Type.PrimitiveOrVoid.SHORT);
            else if(precision == Type.NumericPrecision.INT)
                return new Type(Type.PrimitiveOrVoid.INT);
            else if(precision == Type.NumericPrecision.LONG)
                return new Type(Type.PrimitiveOrVoid.LONG);
            else if(precision == Type.NumericPrecision.FLOAT)
                return new Type(Type.PrimitiveOrVoid.FLOAT);
            else if(precision == Type.NumericPrecision.DOUBLE)
                return new Type(Type.PrimitiveOrVoid.DOUBLE);
            else
                throw new RuntimeException("unknown type");
        }
    }
    // promote char, byte and short to integer
    public static final int BINARY_NUMERIC_PROMOTION_LEVEL = 3;
    
    /**
     * Type, either <code>PrimitiveOrVoid</code> or <code>NameList</code>.
     * For a primitive type or void it is the former, for a java class or an annotation type
     * it is the latter. For no type specified it is null. Once this object is complete, this field
     * should never be null.<br>
     * 
     * If <code>NameList</code> begins with @, then it represents one
     * of the annotation string constants in <code>CompilerAnnotation</code>.
     * Such a type can be assigned only to <code>Literal</code>, and only when
     * the literal replaces a marker call to certain methods with a respective
     * annotation.<br>
     * 
     * See <code>Literal(CompilerAnnotation, List&lt;Double&gt;)</code> for
     * more detailed docs on the annotation types.
     */
    public Object type;
    /**
     * A wildcard, always NONE for the root type.
     */
    public Wildcard wildcard;
    /**
     * If this type is nullable.
     */
    public boolean nullableReference;
    /**
     * If this is a mutator reference. The referred class must allow
     * mutator flags.
     */
    public boolean mutatorReference;
    /**
     * Parameters of this type, empty list for no parameters.
     */
    public List<Type> parameters;
    /**
     * For java class, if the java class has been semantically
     * found.
     */
    public boolean javaClassFound = false;
//    /**
//     * Runtime range of an arithmetic  primitive type, null for other types.
//     */
//    public RuntimeRange runtimeRange;
    /**
     * If this type is of a source--level constant, this literal can represent
     * that constant.<br>
     * 
     * Used only in special cases of precision loss checking. A non--null
     * value enables for implicit arithmetic cast, that would normally be
     * impossible due to type conversion rules, if the constant fits
     * into the target type.<br>
     * 
     * Regarding Java, such a special case denotes assignments and return
     * values, it can not be used for matching of method calls.<br>
     * 
     * This field is read only in <code>Type.castable()</code>.
     */
    public Literal constant;

    /**
     * Creates a new instance of Type, being null, NONE wildcard.<br>
     *
     * Also, an initialization constructor called by all other constructors.
     */
    public Type() {
        type = null;
        parameters = new LinkedList<Type>();
        wildcard = Wildcard.NONE;
        nullableReference = false;
        mutatorReference = false;
//        runtimeRange = null;
    }
    /**
     * Creates a new instance of Type, being a primitive type.<br>
     * 
     * A primitive type can have only a wildcard NONE.
     *
     * @param primitive                 primitive type, PrimitiveOrVoid.VOID for
     *                                  void
     */
    public Type(PrimitiveOrVoid primitive) {
        this();
        type = primitive;
//        switch(primitive) {
//            case BYTE:
//            case CHAR:
//            case DOUBLE:
//            case FLOAT:
//            case INT:
//            case LONG:
//            case SHORT:
//                runtimeRange = primitive.getRange();
//                break;
//
//            case BOOLEAN:
//            case VOID:
//                break;
//
//            default:
//                throw new RuntimeException("unknown primitive type");
//        }
    }
//    /**
//     * Creates a new instance of Type, being a primitive type, with a custom
//     * type range. If the custom range does not fit into the maximum
//     * range of given type, a runtime exception is thrown.
//     *
//     * @param primitive                 primitive type, PrimitiveOrVoid.VOID for
//     *                                  void
//     * @param runtimeRange              custom type range
//     */
//    public Type(Primitive primitive, RuntimeRange runtimeRange) {
//        this(primitive);
//        if(runtimeRange.fits(primitive.getRange()))
//            this.runtimeRange = runtimeRange;
//        else
//            throw new RuntimeException(
//                    "custom range does not fit into the maximum range");
//
//    }
    /**
     * Creates a new instance of Type.
     * 
     * @param NameList                  name of java class
     * @param wildcard                  type of wildcard
     * @param nullableReference true if this represents a nullable
     * reference, false otherwise
     * @param mutatorReference          true if this represents a mutator
     *                                  reference, false otherwise
     */
    public Type(NameList java, Wildcard wildcard,
            boolean nullableReference, boolean mutatorReference) {
        this();
        type = java;
        this.wildcard= wildcard;
        this.nullableReference = nullableReference;
        this.mutatorReference = mutatorReference;
    }
    /**
     * Creates a new instance of Type, with the wildcard being NONE,
     * no nullable reference and no mutator reference.<br>
     * 
     * This is a convenience constructor.
     * 
     * @param NameList                  name of java class
     */
    public Type(NameList java) {
        this(java, Wildcard.NONE, false, false);
    }
    /**
     * Creates a new instance of Type, being a copy of another type.
     * 
     * @param t                     type to copy         
     */
    public Type(Type t) {
        this();
        if(t.type == null)
            type = null;
        else if(t.isPrimitiveOrVoid())
            type = t.getPrimitiveOrVoid();
        else {
            if(t.getAnnotation() != null)
                type = new NameList((NameList)t.type);
            else
                type = new NameList(t.getJava());
        }
        wildcard = t.wildcard;
        nullableReference = t.nullableReference;
        mutatorReference = t.mutatorReference;
        for(Type p : t.parameters)
            parameters.add(new Type(p));
        javaClassFound = t.javaClassFound;
//        if(t.runtimeRange != null)
//            runtimeRange = new RuntimeRange(t.runtimeRange);
    }
    /**
     * If this is a primitive or void type.
     *
     * @return                          is primitive or void type
     */
    public boolean isPrimitiveOrVoid() {
        return type instanceof PrimitiveOrVoid;
    }
    /**
     * If this is a primitive type.
     *
     * @return                          is primitive type
     */
    public boolean isPrimitive() {
        return isPrimitiveOrVoid() &&
                !(type == PrimitiveOrVoid.VOID);
    }
    /**
     * If this type represents a java class.
     * 
     * @return                          if represents a java class
     */
    public boolean isJava() {
        return type instanceof NameList &&
                !((NameList)type).list.get(0).startsWith("@");
    }
    /**
     * If this type is an annotation one.
     * 
     * @return                          if an annotation type
     */
    public boolean isAnnotation() {
        return type instanceof NameList &&
                ((NameList)type).list.get(0).startsWith("@");
    }
    /**
     * If this is a void type.
     * 
     * @return                          is void type
     */
    public boolean isVoid() {
        return type == PrimitiveOrVoid.VOID;
    }
    /**
     * If this type represents a random variable.
     * 
     * @return if a probabilistic type
     */
    public boolean isProbabilistic() {
        boolean probabilistic = false;
        if(isAnnotation())
            switch(CompilerAnnotation.parse(((NameList)type).toString())) {
                case DIST_UNI:
                case DIST_NXP:
                case DIST_ARRAY:
                    probabilistic = true;
                    break;
                    
            }
        return probabilistic;
    }
    /**
     * If this is a null value.
     * 
     * @return                          is null
     */
    public boolean isNull() {
        return type == null;
    }
    /**
     * Returns primitive or void type or null if this is a java class.
     *
     * @return                          primitive or void type, or null
     */
    public PrimitiveOrVoid getPrimitiveOrVoid() {
        if(isPrimitiveOrVoid())
            return (PrimitiveOrVoid)type;
        else
            return null;
    }
    /**
     * Returns primitive type or null if this is void or a java class.
     *
     * @return                          primitive type or null
     */
    public PrimitiveOrVoid getPrimitive() {
        if(isPrimitive())
            return (PrimitiveOrVoid)type;
        else
            return null;
    }
    /**
     * Returns name of the java class represented by this type, or null
     * if this is void or a primitive type.
     * 
     * @return                          java class or null
     */
    public NameList getJava() {
        if(isJava())
            return (NameList)type;
        else
            return null;
    }
    public CompilerAnnotation getAnnotation() {
        if(isAnnotation())
            return CompilerAnnotation.parse(
                    ((NameList)type).toString());
        else
            return null;
    }
    /**
     * If this type is one of primitive integer ones.
     *
     * @return                          is CHAR, BYTE, SHORT,
     *                                  INT or LONG
     */
    public boolean isOfIntegerTypes() {
        int precision = numericPrecision();
        return precision != 0 && precision <= NumericPrecision.LONG;
    }
    /**
     * If this type is one of floating point ones.
     *
     * @return                          is FLOAT or DOUBLE
     */
    public boolean isOfFloatingPointTypes() {
        int precision = numericPrecision();
        return precision == NumericPrecision.FLOAT ||
                precision == NumericPrecision.DOUBLE;
    }
    /**
     * If this type is boolean.
     *
     * @return                          is BOOLEAN
     */
    public boolean isOfBooleanType() {
        return isPrimitive() && getPrimitive() == PrimitiveOrVoid.BOOLEAN;
    }
    /**
     * If this is a an array.<br>
     *
     * The array class name is required as this method can not perform
     * any lookup to get the frontend, as the method can be used
     * before semantic check.
     *
     * @param arrayClassName            fully qualified name of the
     *                                  array class, null for
     *                                  <code>ARRAY_QUALIFIED_CLASS_NAME</code>
     * @return                          is an array
     */
    public boolean isArray(String arrayClassName) {
        if(arrayClassName == null)
            arrayClassName = ARRAY_QUALIFIED_CLASS_NAME;
        return isJava() &&
                getJava().toString().equals(arrayClassName);
    }
    /**
     * Increases the dimension. It means making the type represented
     * by this object a parameter of an array type, and then assigning
     * the resulting type as the new type represented by this
     * object.<br>
     *
     * The array class name is required as explained in <code>isArray</code>.
     *
     * @param arrayClassName            fully qualified name of the
     *                                  array class, null for
     *                                  <code>ARRAY_QUALIFIED_CLASS_NAME</code>
     */
    public void increaseDimension(String arrayClassName) {
        // create the new type
        if(arrayClassName == null)
            arrayClassName = ARRAY_QUALIFIED_CLASS_NAME;
        // the array class does not allow mutator methods
        Type array = new Type(new NameList(arrayClassName));
        array.parameters.add(new Type(this));
        // make this object represent the new type
        type = array.type;
        parameters = array.parameters;
        javaClassFound = false;
    }
    /**
     * Decreases the dimension. Works only on array types, otherwise a
     * runtime exception is throw. The decrease means taking out the
     * parameter type and then making this object represent the taken
     * out type.<br>
     *
     * The array class name is required as explained in <code>isArray</code>.
     *
     * @param arrayClassName            fully qualified name of the
     *                                  array class, null for
     *                                  <code>ARRAY_QUALIFIED_CLASS_NAME</code>
     */
    public void decreaseDimension(String arrayClassName) {
        if(isArray(arrayClassName)) {
            // create the new type
            Type element = new Type(parameters.get(0));
            type = element.type;
            parameters = element.parameters;
            javaClassFound = false;
        } else
            throw new RuntimeException("can not decrease dimension of " +
                    "non--array");
    }
    /**
     * If this is a an array, returns the toal dimension including possible
     * nested arrays. If this is not an array, returns 0.<br>
     *
     * The array class name is required as explained in <code>isArray</code>.
     *
     * @param arrayClassName            fully qualified name of the
     *                                  array class, null for
     *                                  <code>ARRAY_QUALIFIED_CLASS_NAME</code>
     * @return                          is an array
     */
    public int getDimension(String arrayClassName) {
        if(arrayClassName == null)
            arrayClassName = ARRAY_QUALIFIED_CLASS_NAME;
        int dimension = 0;
        Type current = this;
        while(current.isArray(arrayClassName)) {
            ++dimension;
            if(current.parameters.size() != 1)
                throw new RuntimeException("array type must have exactly one parameter");
            else
                current = current.parameters.get(0);
        }
        return dimension;
    }
    /**
     * Returns the type of an element of an array. Throw a runtime
     * exception if this type is not an array of an adequate dimension.
     *
     * @param arrayClassName            fully qualified name of the
     *                                  array class, null for
     *                                  <code>ARRAY_QUALIFIED_CLASS_NAME</code>
     * @param nested                    0 for direct elements, &gt;0 for elements
     *                                  at a agiven nesting level
     * @return                          type
     */
    public Type getElementType(String arrayClassName, int nested) {
        Type current = this;
        while(nested-- >= 0) {
            if(!current.isArray(arrayClassName))
                throw new RuntimeException("requested element type of non--array type");
            if(current.parameters.size() != 1)
                throw new RuntimeException("array type must have exactly one parameter");
            else
                current = current.parameters.get(0);
        }
        return current;
    }
    /**
     * Returns a new type of void.
     * 
     * @return                          new type being a void
     */
    public static Type newVoid() {
        return new Type(PrimitiveOrVoid.VOID);
    }
    /**
     * Returns a new type of string.
     *
     * @param stringClassName           fully qualified name of the
     *                                  string class, null for
     *                                  <code>STRING_QUALIFIED_CLASS_NAME</code>
     * @return                          new type being a string
     */
    public static Type newString(String stringClassName) {
        if(stringClassName == null)
            stringClassName = STRING_QUALIFIED_CLASS_NAME;
        // the string class does not allow mutator methods
        return new Type(new NameList(stringClassName));
    }
    /**
     * Is this type is boolean.
     * 
     * @return                          if is a string
     */
    public boolean isBoolean() {
        return getPrimitive() == PrimitiveOrVoid.BOOLEAN;
    }
    /**
     * Is this type is java.lang.String.
     * 
     * @param stringClassName           fully qualified name of the
     *                                  string class, null for
     *                                  <code>STRING_QUALIFIED_CLASS_NAME</code>
     * @return                          if is a string
     */
    public boolean isString(String stringClassName) {
        if(isJava()) {
            if(stringClassName == null)
                stringClassName = STRING_QUALIFIED_CLASS_NAME;
            return stringClassName.equals(getJava().toString());
        } else
            return false;
    }
    /**
     * Returns numeric precision level of a numeric type, or 0 if this
     * is not a numeric type. If numeric types in an expression
     * have precision lower that BINARY_NUMERIC_PROMOTION_LEVEL then the
     * resulting type must be promoted to BINARY_NUMERIC_PROMOTION_TYPE.
     * 
     * @return                          numeric precision level or 0 for
     *                                  a non--numeric type
     */
    public int numericPrecision() {
        int precision = 0;
        if(type instanceof PrimitiveOrVoid) {
            switch((PrimitiveOrVoid)type) {
                case CHAR:
                    precision = NumericPrecision.CHAR;
                    break;

                case BYTE:
                    precision = NumericPrecision.BYTE;
                    break;

                case SHORT:
                    precision = NumericPrecision.SHORT;
                    break;

                case INT:
                    precision = NumericPrecision.INT;
                    break;

                case LONG:
                    precision = NumericPrecision.LONG;
                    break;

                case FLOAT:
                    precision = NumericPrecision.FLOAT;
                    break;

                case DOUBLE:
                    precision = NumericPrecision.DOUBLE;
                    break;

                case VOID:
                case BOOLEAN:
                    /* empty */
                    break;

                default:
                    throw new RuntimeException("unknown primitive type: " +
                            (PrimitiveOrVoid)type);
            }
        } else if(isProbabilistic()) {
            switch(CompilerAnnotation.parse(((NameList)type).toString())) {
                case DIST_UNI:
                case DIST_NXP:
                case DIST_ARRAY:
                    precision = NumericPrecision.DOUBLE;
                    break;

                default:
                    throw new RuntimeException("unknown probabilistic type");
            }
        }
        return precision;
    }
    /**
     * Returns a numeric type of a given precision. The precision must
     * be greater than or equal to BINARY_NUMERIC_PROMOTION_LEVEL.
     * 
     * @param precision                 numeric precision
     * @return                          type, or null if precision is
     *                                  less than
     *                                  BINARY_NUMERIC_PROMOTION_LEVEL
     */
    static Type getNumericPrecisionType(int precision) {
        if(precision < BINARY_NUMERIC_PROMOTION_LEVEL)
            return null;
        else {
            PrimitiveOrVoid primitive = null;
            switch(precision) {
                case NumericPrecision.INT:
                    primitive = PrimitiveOrVoid.INT;
                    break;
                    
                case NumericPrecision.LONG:
                    primitive = PrimitiveOrVoid.LONG;
                    break;
                    
                case NumericPrecision.FLOAT:
                    primitive = PrimitiveOrVoid.FLOAT;
                    break;
                    
                case NumericPrecision.DOUBLE:
                    primitive = PrimitiveOrVoid.DOUBLE;
                    break;
                    
                default:
                    throw new RuntimeException("unknown numeric precision: " +
                            precision);
            }
            if(primitive != null)
                return new Type(primitive);
            else
                return null;
        }
    }
    /**
     * Promotes types of an unary expression into the resulting type.
     * In the case of the cast operator, there is no promotion and
     * this method simply returns <code>ue.castType</code> if the
     * resulting type is possibly castable to it, null otherwise. In the case
     * of <code>instanceof</code> operator, this method always returns
     * boolean.
     *
     * @param ue                        unary expression
     * @return                          resulting type or null
     *                                  if could not be determined
     */
    public static Type promoteUnary(UnaryExpression ue)
            throws ParseException {
        Type type = ue.sub.getResultType();
        UnaryExpression.Op operator = ue.operatorType;
        if(operator == UnaryExpression.Op.CAST) {
            // any numeric is castable to any other numeric
            if(type.numericPrecision() != 0 && ue.objectType.numericPrecision() != 0)
                return ue.objectType;
            else {
                // numeric -- java class conversion is impossible
                if(type.numericPrecision() != 0 || ue.objectType.numericPrecision() != 0)
                    return null;
                else {
                    // java class to java class
                    if(possiblyCastable(ue.outerScope, type, ue.objectType, null))
                        return ue.objectType;
                    else
                        return null;
                }
            }
        } else if(operator == UnaryExpression.Op.INSTANCE_OF) {
            return new Type(Type.PrimitiveOrVoid.BOOLEAN);
        } else {
            int precision = type.numericPrecision();
            if(precision > 0) {
                int promotedPrecision = Math.max(
                        BINARY_NUMERIC_PROMOTION_LEVEL, precision);
                Type promotedType = getNumericPrecisionType(promotedPrecision);
                switch(operator) {
                    case NEGATION:
                    case POST_DECREMENT:
                    case PRE_DECREMENT:
                    case POST_INCREMENT:
                    case PRE_INCREMENT:
                        return promotedType;

                    case BITWISE_COMPLEMENT:
                        if(precision >= NumericPrecision.FLOAT)
                            return null;
                        else
                            return promotedType;

                    case CONDITIONAL_NEGATION:
                        return null;
                        
                    default:
                        throw new RuntimeException("unknown operator type: " +
                                operator);
                }
            } else {
                if(operator.isBoolean() && type.isBoolean())
                    return type;
                else
                    return null;
            }
        }
    }
    /**
     * Computes the most tight bounding precision of two arithmetic
     * precisions, promoted to integer. The promotion stems from the
     * assumption there is no short int or byte arithmetic operations
     * used by the backends.
     *
     * @param precision1                first arithmetic precision
     * @param precision2                second arithmetic precision
     * @return                          most tight bounding arithmetic
     *                                  precision, promoted to integer.
     */
    public static int mostTightPrecisionPromoted(int precision1, int precision2) {
        // two numeric types
        int precision = Math.max(precision1, precision2);
        // promote to integer if needed
        precision = Math.max(BINARY_NUMERIC_PROMOTION_LEVEL, precision);
        return precision;
    }
    /**
     * Computes the most tight bounding of either two arithmetic types
     * or two Java types. The scope is needed only if the types are Java
     * ones.<br>
     *
     * A parse exception is thrown only if the types are not found in the
     * scope.
     *
     * @param scope                     scope or null
     * @param t1                        first type
     * @param t2                        second type
     * @return                          common most tight bounding type
     *                                  or null if does not exist
     */
    public static Type mostTightBounding(AbstractScope scope, Type t1, Type t2)
            throws ParseException {
        int precision1 = t1.numericPrecision();
        int precision2 = t2.numericPrecision();
        if(precision1 > 0 && precision2 > 0) {
            int precision = mostTightPrecisionPromoted(precision1, precision2);
            return getNumericPrecisionType(precision);
        } else if(precision1 == 0 && precision2 == 0) {
            if(t1.getPrimitive() == Type.PrimitiveOrVoid.BOOLEAN &&
                    t2.getPrimitive() == Type.PrimitiveOrVoid.BOOLEAN)
                return t1;
            NameList n1 = t1.getJava();
            NameList n2 = t2.getJava();
            if(n1 == null || n2 == null)
                return null;
            if(castable(scope, t1, t2, null))
                return t2;
            else if(castable(scope, t2, t1, null))
                return t1;
            else {
                Type extended = t1;
                do {
                    AbstractJavaClass clazz = scope.lookupJavaClassNA(
                            extended.getJava());
                    extended = clazz.extendS;
                    if(extended == null)
                        throw new RuntimeException(t1.toNameString() + " and " +
                                t2.toNameString() + " are both java types but " +
                                "do not have a common supertype");
                } while(!castable(scope, t2, extended, null));
                return extended;
            }
        } else
            // arithmetic and java types do not have a common
            // bounding
            return null;
    }
    /**
     * Promotes types of a binary expression into the resulting type.<br>
     *
     * If the equation is equality/inequality of two object references,
     * this method does not check if the types are convertible, due to lack
     * of error reporting and lack of scope. The convertibility must be
     * checked separately by the caller.
     * 
     * @param left                      left side type
     * @param right                     right side type
     * @param operator                  operator type
     * @param stringClassName           fully qualified name of the
     *                                  string class, null for
     *                                  <code>STRING_QUALIFIED_CLASS_NAME</code>
     * @return                          resulting type or null
     *                                  if could not be determined
     */
    public static Type promoteBinary(Type left, Type right,
                BinaryExpression.Op operator, String stringClassName)
            throws CompilerException {
        if(stringClassName == null)
            stringClassName = STRING_QUALIFIED_CLASS_NAME;
        if(left.getResultType().isVoid() || right.getResultType().isVoid())
              return null;
        int precisionLeft = left.numericPrecision();
        int precisionRight = right.numericPrecision();
        if(precisionLeft > 0 && precisionRight > 0) {
            if(operator.isRelational())
                // can compare every combination of numeric types
                return new Type(Type.PrimitiveOrVoid.BOOLEAN);
            else {
                int precision = mostTightPrecisionPromoted(
                        precisionLeft, precisionRight);
                switch(operator) {
                    case EXCLUSIVE_OR:
                    case INCLUSIVE_OR:
                    case SHIFT_LEFT:
                    case SHIFT_RIGHT:
                    case UNSIGNED_SHIFT_RIGHT:
                        if(precision >= NumericPrecision.FLOAT)
                            return null;
                        // fallthrough
                    case AND:
                    case DIVIDE:
                    case MINUS:
                    case MODULUS:
                    case MODULUS_POS:
                    case MULTIPLY:
                    case PLUS:
                        return getNumericPrecisionType(precision);

                    default:
                        throw new RuntimeException("unknown operator type: " +
                                operator);
                }
            }
        } else if(operator == BinaryExpression.Op.PLUS &&
                (left.isString(stringClassName) || right.isString(stringClassName))) {
            // every type can be promoted to String
            return Type.newString(stringClassName);
        } else if(left.isBoolean() && right.isBoolean() &&
                (operator.isRelational() || operator.isBoolean()))
            return new Type(Type.PrimitiveOrVoid.BOOLEAN);
        else if((operator == BinaryExpression.Op.EQUAL ||
                operator == BinaryExpression.Op.INEQUAL) &&
                (precisionLeft == 0 && precisionRight == 0)) {
            // comparison of two references, convertibility
            // is not checked
            return new Type(Type.PrimitiveOrVoid.BOOLEAN);
        }
        return null;
    }
    /**
     * If one type can be casted into another. Takes into account
     * numeric precision, type inheritance and frontend type
     * translation.<br>
     *
     * Two voids are non--castable one to another.<br>
     * 
     * The parameter <code>root</code> is important, because
     * with roots, subclassing always works, while parameters need
     * respective wildcards for a subclassing.
     *
     * @param scope                     scope
     * @param from                      casted type
     * @param to                        target type
     * @param covariant                 if it is enough that the types
     *                                  are castable, as opposed to exactly
     *                                  the same; false only if
     *                                  the types are parameters of another type,
     *                                  that is not an array, what results
     *                                  from java covariance rules
     * @param ignoreNarrowingType       ignore that the target type is
     *                                  narrower; true for explicit casts; overflowing
     *                                  constants are still not allowed
     * @param errorStr                  error message buffer, used if not castable,
     *                                  can be null
     * @return                          if the cast is possible
     */
    public static boolean castable(AbstractScope scope, Type from, Type to,
            boolean covariant, boolean ignoreNarrowingType,
            StringBuilder errorStr) throws ParseException {
        if(errorStr == null)
            errorStr = new StringBuilder();
        int precisionFrom = from.numericPrecision();
        int precisionTo = to.numericPrecision();
        if(precisionFrom > 0 && precisionTo > 0) {
            // numeric operands
            boolean b;
            if(covariant)
                b = precisionFrom <= precisionTo;
            else
                b = precisionFrom == precisionTo;
            // illegal conversion between BYTE and CHAR
            if(precisionFrom == NumericPrecision.CHAR &&
                    precisionTo == NumericPrecision.CHAR &&
                   (from.getPrimitive() == PrimitiveOrVoid.BYTE) !=
                   (to.getPrimitive() == PrimitiveOrVoid.BYTE))
                b = false;
            if(!b) {
                boolean reported = false;
                if(precisionFrom <= NumericPrecision.LONG &&
                        precisionTo <= NumericPrecision.LONG &&
                        from.constant != null) {
                    long source = from.constant.getMaxPrecisionInteger();
                    TypeRange target = to.getPrimitive().getRange();
                    if(source < target.minI || source > target.maxI) {
                        errorStr.append("loss of precision: " +
                                "conversion from " + from.toNameString() +
                                " constant of " + source + " to " +
                                to.toNameString());
                        reported = true;
                    } else
                        b = true;
                }
                if(!b && !reported) {
                    if(ignoreNarrowingType)
                        b = true;
                    else
                        errorStr.append("possible loss of precision: " +
                                "conversion from " + from.toNameString() + " to " +
                                to.toNameString());
                }
            }
            return b;
        } else if(precisionFrom != precisionTo) {
            // one is numeric, the second is not
            errorStr.append("can not convert between arithmetic and " +
                    "non--arithmetic type: " +
                    from.toNameString() + " to " +
                    to.toNameString());
            return false;
        } else if(from.isBoolean() || to.isBoolean()) {
            if(from.isBoolean() != to.isBoolean()) {
                errorStr.append("can not convert between boolean and " +
                        "non--boolean type: " +
                        from.toNameString() + " to " +
                        to.toNameString());
                return false;
            }
            return true;
        } else if(to.isVoid()) {
            errorStr.append("can not convert " +
                    from.toNameString() + " to a void type");
            return false;
        } else {
            if(from.isNull())
                // null can be assigned to a reference variable
                return true;
            else if(from.isVoid()) {
                errorStr.append("can not assign a void type");
                return false;
            } else try {
                // both are non--null java classes
                NameList fromName = from.getJava();
                NameList toName = to.getJava();
                AbstractJavaClass fromClass = scope.lookupJavaClassNA(fromName);
                AbstractJavaClass toClass = scope.lookupJavaClassNA(toName);
                AbstractFrontend fromFrontend = fromClass.frontend;
                AbstractFrontend toFrontend = toClass.frontend;
                // make the two names be defined within the same frontend
                if(fromFrontend != toFrontend) {
                    // if two classes are special or none is special,
                    // which one is translated does not matter
                    //
                    // if one is special and the other not, translate
                    // the special one as there is a chance the other
                    // one is extending a special class, what might
                    // make the normal class be castable to the translated
                    // special class
                    if(fromFrontend.getSpecialClassCategory(fromName.toString()) !=
                            AbstractFrontend.SpecialClassCategory.NONE) {
                        fromName = new NameList(fromFrontend.translateClassName(
                                fromName.toString(), toFrontend));
                        fromClass = scope.lookupJavaClassNA(fromName);
                    } else {
                        toName = new NameList(toFrontend.translateClassName(
                                toName.toString(), fromFrontend));
                        toClass = scope.lookupJavaClassNA(toName);
                    }
                }
                if(!to.nullableReference && from.nullableReference) {
                    errorStr.append("can not convert " +
                            new Type(fromName, Wildcard.NONE, from.nullableReference, from.mutatorReference).toNameString() + " to " +
                            new Type(toName, Wildcard.NONE, to.nullableReference, to.mutatorReference).toNameString() + ": " +
                            "target type does not accept nullability");
                    return false;
                }
                if(to.mutatorReference && !from.mutatorReference) {
                    errorStr.append("can not convert " +
                            new Type(fromName, Wildcard.NONE, from.nullableReference, from.mutatorReference).toNameString() + " to " +
                            new Type(toName, Wildcard.NONE, to.nullableReference, to.mutatorReference).toNameString() + ": " +
                            "target type requires mutator flag");
                    return false;
                }
/*
if(fromClass.name.indexOf("Real8Voxel") != -1 &&
        toClass.name.indexOf("Vector") != -1)
    fromClass = fromClass;
  */
                if(fromClass.equals(toClass) ||
                        (covariant &&
                            (fromClass.isEqualToOrExtending(scope, toClass) ||
                                fromClass.isImplementing(scope, toClass)))) {
                    if(to.parameters.isEmpty())
                        return true;
                    if(!fromClass.equals(toClass))
                        throw new RuntimeException("subclassing of parameterized " +
                                "types not implemented");
                    // !!! assumes fromClass == toClass
                    boolean isArray = from.isArray(fromClass.frontend.arrayClassName);
                    if(from.parameters.size() != to.parameters.size())
                        throw new RuntimeException("the same type with " +
                                "a different number of parameters");
                    Iterator<Type> fromP = from.parameters.iterator();
                    Iterator<Type> toP = to.parameters.iterator();
                    while(fromP.hasNext()) {
                        Type f = fromP.next();
                        Type t = toP.next();
                        if(!castable(scope, f, t, isArray, ignoreNarrowingType, errorStr))
                            return false;
                    }
                    return true;
                } else {
                    errorStr.append("can not convert " +
                            new Type(fromName, Wildcard.NONE, from.nullableReference, from.mutatorReference).toNameString() + " to " +
                            new Type(toName, Wildcard.NONE, to.nullableReference, to.mutatorReference).toNameString());
                    return false;
                }
            } catch(CompilerException e) {
                throw new RuntimeException(e.toString());
            }
        }
    }
    /**
     * If one type can be casted into another. Takes into account
     * numeric precision, type inheritance and frontend type
     * translation. Assumes the type is a root one, as opposed
     * to being another type's parameter. Checks for narrowing type,
     * and thus not suitable for explicit casts.<br>
     *
     * Two voids are non--castable one to another.<br>
     * 
     * This is a convenience method.
     *
     * @param scope                     scope
     * @param from                      casted type
     * @param to                        target type
     * @param errorStr                  error message buffer, used if not castable,
     *                                  can be null
     * @return                          if the cast is possible
     */
    public static boolean castable(AbstractScope scope,
            Type from, Type to, StringBuilder errorStr) throws ParseException {
        return castable(scope, from, to, true, false, errorStr);
    }
    /**
     * Calls <code>castable()</code> for <code>from</code>,
     * but also for each of <code>from</code>'s subclasses,
     * then checks for interface implementations.
     * Returns true, if at leat once within the calls true is returned.
     * 
     * @param scope                     scope
     * @param from                      casted type
     * @param to                        target type
     * @param errorStr                  error message buffer, used if not castable,
     *                                  can be null
     * @return                          if the cast might in some cases be possible
     */
    public static boolean possiblyCastable(AbstractScope scope,
            Type from, Type to, StringBuilder errorStr) throws ParseException {
        if(castable(scope, from, to, errorStr)) 
            return true; // to = new Type(new NameList("mirela.Delay"));
        NameList fromName = from.getJava();
        AbstractJavaClass fromClass = scope.lookupJavaClassNA(fromName);
// if(from.getJava().toString().indexOf("AbstractVoxel")  != -1)
//     to = to;
        for(AbstractJavaClass subclass : fromClass.subclasses)
            if(castable(scope, new Type(subclass.getQualifiedName()), to, errorStr))
                return true;
        // check interface implementations
        AbstractJavaClass toClass = scope.lookupJavaClassNA(to.getJava());
        for(Type ifaceT : toClass.implementS) {
            //AbstractJavaClass iface = scope.lookupJavaClassNA(ifaceT.getJava());
            if(castable(scope, from, ifaceT, errorStr))
                return true;
        }
        return false;
    }
    /**
     * If two java types are convertible, that is, at least
     * one of them, or its subclasses, can be cast to another.
     * Takes into account frontend type translation.<br>
     *
     * Two voids are not convertible.
     *
     * @param scope                     scope
     * @param type1                     first type
     * @param type2                     second type
     * @return                          if the types are convertible
     */
    public static boolean convertible(AbstractScope scope,
            Type type1, Type type2) throws ParseException {
        if(!type1.isJava() || !type2.isJava())
            throw new RuntimeException("convertible can be only java types");
        return possiblyCastable(scope, type1, type2, null) ||
                possiblyCastable(scope, type2, type1, null);
    }
    /**
     * Converts a set of types to minimum bounding type.
     *
     * @param precisions            set of numeric precisions
     * @return                      respective type
     */
    public static Type integerTypesToBounding(Set<Type> types) {
        Type outType;
        if(types.isEmpty())
            outType = new Type(Type.PrimitiveOrVoid.BYTE);
        else {
            int maxPrecision = 0;
            for(Type t : types) {
                int p = t.numericPrecision();
                if(maxPrecision < p)
                    maxPrecision = p;
            }
            if(maxPrecision == Type.NumericPrecision.BYTE) {
                // not known, if BYTE or CHAR
                boolean byteFound = false;
                boolean charFound = false;
                for(Type t : types) {
                    switch(t.getPrimitive()) {
                        case BYTE:
                            byteFound = true;
                            break;

                        case CHAR:
                            charFound = true;
                            break;

                        default:
                            throw new RuntimeException("invalid type");
                    }
                }
                if(byteFound && !charFound)
                    outType = new Type(Type.PrimitiveOrVoid.BYTE);
                else if(charFound && !byteFound)
                    outType = new Type(Type.PrimitiveOrVoid.CHAR);
                else
                    // mixed CHAR/BYTE, set resulting to SHORT
                    // as it covers both
                    outType = new Type(Type.PrimitiveOrVoid.SHORT);
            } else
                outType = Type.NumericPrecision.precisionToType(
                        maxPrecision);
        }
        return outType;
    }
    /**
     * Returns the identifier representing a given <code>PrimitiveOrVoid</code>
     * in the source code.
     * 
     * @param primitive                 primitive
     * @return                          identifier
     */
    public static String getPrimitiveName(PrimitiveOrVoid primitive) {
        String s = null;
        switch(primitive) {
            case VOID:
                s = "void";
                break;

            case BOOLEAN:
                s = "boolean";
                break;

            case CHAR:
                s = "char";
                break;

            case BYTE:
                s = "byte";
                break;

            case SHORT:
                s = "short";
                break;

            case INT:
                s = "int";
                break;

            case LONG:
                s = "long";
                break;

            case FLOAT:
                s = "float";
                break;

            case DOUBLE:
                s = "double";
                break;

            default:
                throw new RuntimeException("unknown primitive type: " +
                        primitive);
        }
        return s;
    }
    /**
     * Returns <code>PrimitiveOrVoid</code> represented by a character returned by
     * toString().
     * 
     * @param c                         character
     * @return                          primitive
     */
    public static PrimitiveOrVoid getPrimitive(char c) {
        PrimitiveOrVoid p;
        switch(c) {
            case 'V':
                p = PrimitiveOrVoid.VOID;
                break;
                
            case 'O':
                p = PrimitiveOrVoid.BOOLEAN;
                break;
                
            case 'C':
                p = PrimitiveOrVoid.CHAR;
                break;
                
            case 'B':
                p = PrimitiveOrVoid.BYTE;
                break;
                
            case 'S':
                p = PrimitiveOrVoid.SHORT;
                break;
                
            case 'I':
                p = PrimitiveOrVoid.INT;
                break;
                
            case 'G':
                p = PrimitiveOrVoid.LONG;
                break;
                
            case 'F':
                p = PrimitiveOrVoid.FLOAT;
                break;
                
            case 'D':
                p = PrimitiveOrVoid.DOUBLE;
                break;
                
            default:
                p = null;
        }
        return p;
    }
    @Override
    public void setResultType(Type resultType) {
        throw new RuntimeException("can not set type");
    }
    @Override
    public Type getResultType() {
        return this;
    }
    @Override
    public boolean equals(Object o) {
        if(!(o instanceof Type))
            throw new RuntimeException("invalid comparison");
        boolean equals = true;
        Type t = (Type)o;
        if((type == null) != (t.type == null))
            equals = false;
        else if(type != null && t.type != null) {
            if(isPrimitive() != t.isPrimitive() ||
                    !type.equals(t.type))
                equals = false;
        }
        if(equals) {
            if(wildcard != t.wildcard ||
                    nullableReference != t.nullableReference ||
                    mutatorReference != t.mutatorReference)
                equals = false;
            else if(parameters.size() != t.parameters.size())
                equals = false;
            else {
                Iterator<Type> i = t.parameters.iterator();
                for(Type p : parameters) {
                    Type tP = i.next();
                    if(!p.equals(tP)) {
                        equals = false;
                        break;
                    }
                }
            }
        }
        return equals;
    }
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + (this.type != null ? this.type.hashCode() : 0);
        hash = 89 * hash + this.wildcard.hashCode();
        hash = 89 * hash + (this.nullableReference ? 41 : 0);
        hash = 89 * hash + (this.mutatorReference ? 41 : 0);
        for(Type t : parameters)
            hash = 89 * hash + t.hashCode();
        return hash;
    }
    /**
     * <p>Returns the full textual name of this type, for error messages.
     * Individual types are enclosed in <code>${}</code>, for further
     * processing. Mutator references are appended if present.</p>
     * 
     * <p>Arrays are shown as parameterized types. If this is undesired,
     * <code>toJavaString</code> can be used instead.</p>
     * 
     * Example: <code>${boolean}~</code>
     *
     * @param bracket if to wrap with brackets, suitable for error messages
     * @return textual description
     */
    public String toNameString(boolean bracket) {
        String s = "";
        if(type == null)
            s = "null";
        else if(type instanceof PrimitiveOrVoid) {
            s = getPrimitiveName((PrimitiveOrVoid)type);
        } else {
            s = ((NameList)type).toString();
        }
        if(bracket)
            s = "${" + s + "}";
        switch(wildcard) {
            case NONE:
                break;
                
            case EXTENDS:
                s = "? extends " + s;
                break;
                
            case SUPER:
                s = "? super " + s;
                break;
                
            default:
                throw new RuntimeException("unknown wildcard");
        }
        if(nullableReference)
            s += "?";
        if(mutatorReference)
            s += "~";
        if(!parameters.isEmpty()) {
            s += "<";
            boolean first = true;
            for(Type t: parameters) {
                if(!first)
                    s += ", ";
                s += t.toNameString();
                first = false;
            }
            s += ">";
        }
        return s;
    }
    /**
     * <p>Calls <code>toNameString(true)</code>. This is a convenience
     * method.</p>
     * 
     * @return textual description
     */
    public String toNameString() {
        return toNameString(true);
    }
    /**
     * Returns the full textual name of this type, specific to
     * the Java language,. Like <code>toNameString()<code>, but
     * does not show arrays as parameterized types and does not
     * pu the types into brackets. Not to be used with error messages.<br>
     *
     * The array class name is required as this method can not perform
     * any lookup to get the frontend, because the method can be used
     * before semantic check.
     *
     * @param arrayClassName            fully qualified name of the
     *                                  array class, null for
     *                                  <code>ARRAY_QUALIFIED_CLASS_NAME</code>
     * @return                          textual description
     */
    public String toJavaString(String arrayClassName) {
        String s = "";
        if(isArray(arrayClassName)) {
            if(parameters.size() != 1)
                throw new RuntimeException("array expected to have a single parameter");
            Type element = parameters.get(0);
            return element.toJavaString(arrayClassName) + "[]";
        } else {
            if(type == null)
                s = "null";
            else if(type instanceof PrimitiveOrVoid)
                s = getPrimitiveName((PrimitiveOrVoid)type);
            else
                s = ((NameList)type).toString();
            switch(wildcard) {
                case NONE:
                    break;

                case EXTENDS:
                    s = "? extends " + s;
                    break;

                case SUPER:
                    s = "? super " + s;
                    break;

                default:
                    throw new RuntimeException("unknown wildcard");
            }
            if(nullableReference)
                s += "?";
            if(mutatorReference)
                s += "~";
            if(!parameters.isEmpty()) {
                s += "<";
                boolean first = true;
                for(Type t: parameters) {
                    if(!first)
                        s += ", ";
                    s += t.toNameString();
                    first = false;
                }
                s += ">";
            }
        }
        return s;
    }
    /**
     * Creates a new type, given the string in the format as defined by
     * <code>Type.toString()</code>.<br>
     * 
     * Not for parsing the source file. Currently used only to parse
     * method signatures. Currently wildcards not implemented.
     *
     * @param in                        string that codes the type
     * @return                          new type
     */
    public static Type newTypeFromString(String in) {
        Type baseType;
        String baseStr;
        String parametersStr;
        int pos = in.indexOf('<');
        if(pos == -1) {
            baseStr = in;
            parametersStr = null;
        } else {
            baseStr = in.substring(0, pos);
            parametersStr = in.substring(pos + 1, in.length() - 1);
        }
        boolean mutator = false;
        if(baseStr.charAt(baseStr.length() - 1) == '~') {
            baseStr = baseStr.substring(0, baseStr.length() - 1);
            mutator = true;
        }
        boolean nullable = false;
        if(baseStr.charAt(baseStr.length() - 1) == '?') {
            baseStr = baseStr.substring(0, baseStr.length() - 1);
            mutator = true;
        }
        if(baseStr.equals("N"))
            baseType = new Type();
        else if(baseStr.charAt(0) == 'L') {
            baseType = new Type(new NameList(baseStr.substring(1)));
        } else {
            // a primitive type
            Type.PrimitiveOrVoid p = Type.getPrimitive(baseStr.charAt(0));
            baseType = new Type(p);
        }
        if(parametersStr != null) {
            // the type is parameterized
            Scanner sc = CompilerUtils.newScanner(parametersStr).useDelimiter(",");
            while(sc.hasNext()) {
                String p = sc.next();
                Type parameterType = newTypeFromString(p);
                baseType.parameters.add(parameterType);
            }
        }
        baseType.nullableReference = nullable;
        baseType.mutatorReference = mutator;
        return baseType;
    }
    @Override
    public String toString() {
        String s = "";
        char c = 0;
        if(type == null)
            s = "N";
        else if(type instanceof PrimitiveOrVoid) {
            switch((PrimitiveOrVoid)type) {
                case VOID:
                    c = 'V';
                    break;

                case BOOLEAN:
                    c = 'O';
                    break;

                case CHAR:
                    c = 'C';
                    break;

                case BYTE:
                    c = 'B';
                    break;

                case SHORT:
                    c = 'S';
                    break;

                case INT:
                    c = 'I';
                    break;

                case LONG:
                    c = 'G';
                    break;

                case FLOAT:
                    c = 'F';
                    break;

                case DOUBLE:
                    c = 'D';
                    break;

                default:
                    throw new RuntimeException("unknown primitive type: " +
                            (PrimitiveOrVoid)type);
            }
            s = "" + c;
        } else {
            c = 'L';
            s = c + ((NameList)type).toString();
        }
        switch(wildcard) {
            case NONE:
                break;
                
            case EXTENDS:
                s = "? extends " + s;
                break;
                
            case SUPER:
                s = "? super " + s;
                break;
                
            default:
                throw new RuntimeException("unknown wildcard");
        }
        if(nullableReference)
            s += "?";
        if(mutatorReference)
            s += "~";
        if(!parameters.isEmpty()) {
            s += "<";
            boolean first = true;
            for(Type t: parameters) {
                if(!first)
                    s += ", ";
                s += t.toString();
                first = false;
            }
            s += ">";
        }
        return s;
    }
}
