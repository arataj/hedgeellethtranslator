/*
 * TypeRangeMap.java
 *
 * Created on Sep 24, 2009, 3:58:32 PM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree;

import java.util.*;

/**
 * A map of primitive type ranges.
 *
 * Provides a method for arithmetic casts.<br>
 *
 * None of the ranges can exceed Java default for a given type.
 *
 * @author Artur Rataj
 */
public class TypeRangeMap {
    /**
     * Range for given primitive type.
     */
    public Map<Type.PrimitiveOrVoid, Type.TypeRange> map;

    /**
     * Creates a new instance of <code>TypeRangeMap</code>, with maps
     * being empty.
     */
    public TypeRangeMap() {
        map = new HashMap<>();
    }
    /**
     * Creates a new instance of <code>TypeRangeMap</code>, with maps
     * having default Java values of a given primitive.<br>
     *
     * The default is read from <code>Type.TypeRange</code>.<br>
     *
     * While boolean is not an arithmetic type in Java, it might
     * be in some backends, thus the parameter <code>allowBoolean</code>.
     *
     * @param p                     primitive type that has defined
     *                              range
     * @param allowBoolean          true if boolean should return
     *                              (&lt;0, 1&gt;), false if boolean
     *                              should cause a runtime exception;
     */
    public TypeRangeMap(Type.PrimitiveOrVoid p, boolean allowBoolean) {
        this();
        // get Java default for the cast type
        add(p, new Type.TypeRange(p, allowBoolean));
    }
    public void add(Type.PrimitiveOrVoid p, Type.TypeRange tr) {
        map.put(p, tr);
    }
    public Type.TypeRange get(Type.PrimitiveOrVoid p) {
        return map.get(p);
    }
    /**
     * Applies a cast operator by modulo to a given literal, and returns
     * the result.
     * 
     * @param p                         primitive that specifies
     *                                  the type of the cast
     * @param in                        argument
     * @return                          result
     */
    public Literal computeCastModulo(Type.PrimitiveOrVoid p, Literal in) {
        Literal out = null;
        Type outType = new Type(p);
        if(outType.isOfIntegerTypes()) {
            long minL = get(p).getMinI();
            long maxL = get(p).getMaxI();
            if(in.type.isOfIntegerTypes()) {
                long inL = in.getMaxPrecisionInteger();
                long outL;
                switch(outType.getPrimitive()) {
                    case BYTE:
                        outL = (byte)inL;
                        break;

                    case CHAR:
                        outL = (char)inL;
                        break;

                    case SHORT:
                        outL = (short)inL;
                        break;

                    case INT:
                        outL = (int)inL;
                        break;

                    case LONG:
                        outL = inL;
                        break;

                    default:
                        throw new RuntimeException("unknown arithmetic type");
                }
                out = new Literal(outType, outL);
            } else {
                double inD = in.getMaxPrecisionFloatingPoint();
                long outL;
                switch(outType.getPrimitive()) {
                    case BYTE:
                        outL = (byte)inD;
                        break;

                    case CHAR:
                        outL = (char)inD;
                        break;

                    case SHORT:
                        outL = (short)inD;
                        break;

                    case INT:
                        outL = (int)inD;
                        break;

                    case LONG:
                        outL = (long)inD;
                        break;

                    default:
                        throw new RuntimeException("unknown arithmetic type");
                }
                out = new Literal(outType, outL);
            }
        } else {
            double minD = get(p).getMinF();
            double maxD = get(p).getMaxF();
            double inD;
            if(in.type.isOfIntegerTypes())
                inD = in.getMaxPrecisionInteger();
            else
                inD = in.getMaxPrecisionFloatingPoint();
            double outD;
            if(inD < minD)
                outD = Double.NEGATIVE_INFINITY;
            else if(inD > maxD)
                outD = Double.POSITIVE_INFINITY;
            else
                outD = inD;
            out = new Literal(outType, outD);
        }
        return out;
    }
}
