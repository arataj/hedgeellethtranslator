/*
 * Typed.java
 *
 * Created on Jan 23, 2008, 7:43:11 PM
 *
 * This file is copyright (c) 2008 Artur Rataj.
 *
 * This software is provided under the terms of the GNU Library General
 * Public License, either version 2 of the license or, at your option,
 * any later version. See http://www.gnu.org for details.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree;

/**
 * An item having a resulting type.
 * 
 * @author Artur Rataj
 */
public interface Typed {
    
    /**
     * Sets the resulting type.
     * 
     * @param resultType                type, if null then set to void
     */
    public void setResultType(Type resultType);
    /**
     * Returns the resulting type. It must return the reference to the
     * original object.
     * 
     * @return                          type
     */
    public Type getResultType();
}
