/*
 * Unit.java
 *
 * Created on December 6, 2007, 11:30 AM
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or,
 * at your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.scope.*;
import pl.gliwice.iitis.hedgeelleth.modules.AbstractFrontend;

/**
 * A compilation unit.
 *
 * @author Artur Rataj
 */
public class Unit {
    /**
     * Frontend of this compilation unit.<br>
     */
    public AbstractFrontend frontend;
    /**
     * Top scope, containing packages.
     */
    public TopScope topScope;
    /**
     * Scope of this unit's package.
     */
    public PackageScope packageScope;
    /**
     *  Package name, or null if default.
     */
    public NameList packageName;
    /**
     * Non--static import declarations.
     */
    public List<Import> importDeclarations;
    /**
     * Static Import declarations.
     */
    public List<Import> staticImportDeclarations;
    
    /**
     * Creates a new instance of Unit.
     * 
     * @param frontend                  frontend of this compilation
     *                                  unit
     * @param topScope                  top scope
     * @param defaultImports            default packages, that should
     *                                  exist within each unit,
     *                                  null for none
     */
    public Unit(AbstractFrontend frontend, TopScope topScope, String[] defaultImports) {
        this.frontend = frontend;
        this.topScope = topScope;
        packageName = null;
        importDeclarations = new LinkedList<>();
        if(defaultImports != null)
            for(String name : defaultImports)
                importDeclarations.add(new Import(null, false, new NameList(name)));
        staticImportDeclarations = new LinkedList<>();
        // the scope will be decided once package of this unit is known
        packageScope = null;
    }
}
