/*
 * Variable.java
 *
 * Created on Dec 18, 2007, 11:21:34 AM
 *
 * Copyright (c) 2007 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.clazz.AbstractJavaClass;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.flags.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.CodeVariable;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.AbstractExpression;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.AssignmentExpression;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * A variable.
 * 
 * @author Artur Rataj
 */
public class Variable extends RangedType implements SourcePosition {
    /**
     * Category of a variable.
     */
    public static enum Category {
        /**
         * Source--level.
         */
        SOURCE,
        /**
         * Added by compiler, not a bound in a variable primitive range.
         */
        INTERNAL_NOT_RANGE,
        /**
         * Added by compiler, a bound in a variable primitive range.
         */
        INTERNAL_VARIABLE_PR,
        /**
         * Added by compiler, a bound in a primary primitive range.
         */
        INTERNAL_PRIMARY_PR,
        /**
         * For languages that do their own semantic check, and for fake
         * variables, i. e. used only as arguments.
         */
        OTHER;
        
        /**
         * If this variable is internal.
         * 
         * @return if not a source level, but added by the compiler
         */
        public boolean isInternal() {
            switch(this) {
                case SOURCE:
                    return false;
                    
                case INTERNAL_NOT_RANGE:
                case INTERNAL_VARIABLE_PR:
                case INTERNAL_PRIMARY_PR:
                    return true;
                    
                case OTHER:
                    throw new RuntimeException("not applicable");
                    
                default:
                    throw new RuntimeException("unknown category");
            }
        }
        /**
         * If this variable is a bound in a variable primitive range.
         * 
         * @return if <code>INTERNAL_VARIABLE_PR</code>
         */
        public boolean isVariablePRBound() {
            switch(this) {
                case SOURCE:
                case INTERNAL_NOT_RANGE:
                case INTERNAL_PRIMARY_PR:
                    return false;
                    
                case INTERNAL_VARIABLE_PR:
                    return true;
                    
                case OTHER:
                    throw new RuntimeException("not applicable");
                    
                default:
                    throw new RuntimeException("unknown category");
            }
        }
        /**
         * If this variable is a bound in a primary primitive range.
         * 
         * @return if <code>INTERNAL_PRIMARY_PR</code>
         */
        public boolean isPrimaryPRBound() {
            switch(this) {
                case SOURCE:
                case INTERNAL_NOT_RANGE:
                case INTERNAL_VARIABLE_PR:
                    return false;
                    
                case INTERNAL_PRIMARY_PR:
                    return true;
                    
                case OTHER:
                    throw new RuntimeException("not applicable");
                    
                default:
                    throw new RuntimeException("unknown category");
            }
        }
    }
    
    /**
     * Owner of this variable.
     */
    public VariableOwner owner;
    /**
     * Name.
     */
    public String name;
    /**
     * Category of this variable.
     */
    public Category category;
    /**
     * Flags of this variable. For locals, it is an
     * instance of <code>LocalFlags</code>, for fields
     * it is an instance of <code>FieldFlags</code>.
     */
    public AbstractVariableModifyFlags flags;
    /**
     * Position of this variable in the parsed stream.
     */
    protected StreamPos pos;
    /**
     * A special comment directly before the field, in the case of
     * Java a `**' comment. Null if none. Its only use is to be copied
     * to <code>CodeVariable.specialComment</code>. Locals have this
     * field always equal to null.
     */
    public String specialComment;
    /**
     * Initializing expressions in the declaration, empty list for none.
     */
    public List<AssignmentExpression> initializers;
    /**
     * Code object of this variable.
     */
    public CodeVariable codeVariable;
    
    /**
     * Creates a new instance of Variable.<br>
     *
     * 
     * @param pos                   position in the parsed stream
     * @param owner                 owner of this variable,
     *                              AbstractJavaClass for fields or Method
     *                              for locals, if not a runtime exception
     *                              is thrown
     * @param type                  type, can be null only as lvalue in a special
     *                              case of <code>AssignmentExpression</code>,
     *                              see the expression's docs for details
     * @param flags                 modifiers, FieldFlags for fields or
     *                              LocalFlags for locals, if not a runtime
     *                              exception is thrown
     * @param name                  name
     * @param local                 true for a local, false for a field
     * @param category                  category of this variable
     */
    public Variable(StreamPos pos, VariableOwner owner, Type type,
            AbstractVariableModifyFlags flags, String name, boolean local,
            Category category) {
        setStreamPos(pos);
        if(local) {
            if(!(owner instanceof Method) && owner != null)
                throw new RuntimeException("owner of local must be Method or, " +
                        "if not yet determined, null");
        } else {
            if(!(owner instanceof AbstractJavaClass))
                throw new RuntimeException("owner of field must be JavaClass");
        }
        this.owner = owner;
        setResultType(type);
        if(local)
            this.flags = new LocalFlags((LocalFlags)flags);
        else
            this.flags = new FieldFlags((FieldFlags)flags);
        this.name = name;
        this.category = category;
        this.specialComment = null;
        initializers = new LinkedList<>();
    }
    /**
     * Creates a new simple instance of Variable.<br>
     *
     * It is a simple version, for languages that do their own
     * semantic check.
     * 
     * @param pos                   position in the parsed stream
     * @param owner                 owner of this variable,
     *                              AbstractJavaClass for fields or Method
     *                              for locals, null for a global owner
     * @param type                  type, can be null
     * @param name                  name
     */
    public Variable(StreamPos pos, VariableOwner owner, Type type,
            String name) {
        setStreamPos(pos);
        this.owner = owner;
        this.type = type;
        this.name = name;
        this.category = Category.OTHER;
    }
    /**
     * Creates a new simple instance of Variable.<br>
     *
     * It is a simple version, for languages that do their own
     * semantic check. Assumes no owner and no type.
     * 
     * @param pos                   position in the parsed stream
     * @param name                  name
     */
    public Variable(StreamPos pos, String name) {
        this(pos, null, null, name);
    }
    @Override
    public final void setStreamPos(StreamPos pos) {
        this.pos = pos;
    }
    @Override
    public final StreamPos getStreamPos() {
        return pos;
    }
    /**
     * Appends a list of expressions to <code>initializers</code>.
     * 
     * @param list list of expressions, that must be assignments
     */
    public void addInitializers(List<AbstractExpression> assignments) {
        for(AbstractExpression e : assignments)
            initializers.add((AssignmentExpression)e);
    }
    /**
     * If this variable is local, that is, owned by a method. Or, if this is a simple
     * variable, that does not have an owner. The latter is the case only
     * for languages that do not use Hedgeelleth's semantic check.
     * 
     * @return                          if this variable is a local one
     */
    public boolean isLocal() {
        return owner == null || owner instanceof Method;
    }
    /**
     * Returns the original variable name in the source file, from which
     * the name of the variable given as the parameter was derived.
     * If the name was not derived from a source file variable name,
     * a runtime exception is thrown.<br>
     *
     * For this method to work, the rules of specifying this variable name,
     * described in the constructor of this object, must be obeyed.
     *
     * @param variableName              variable name, derived from a source
     *                                  code variable identifier
     * @return                          variable identifier from a
     *                                  source file
     */
    public static String extractSourceName(String variableName) {
        int pos = variableName.indexOf('#');
        if(pos == -1)
            return variableName;
        else if(pos > 0)
            return variableName.substring(0, pos);
        else
            throw new RuntimeException("source variable name not exists: " +
                    variableName);
    }
    /**
     * Returns the original variable name in the source file, from which
     * the name of this variable has been derived. If the name was not
     * derived from a source file variable name, a runtime exception is
     * thrown.<br>
     *
     * It uses the method <code>extractSourceName()</code> to process
     * this variable name. See the method docs for details.
     *
     * @return                          variable identifier from a
     *                                  source file
     */
    public String getSourceName() {
        return extractSourceName(name);
    }
    @Override
    public String toString() {
        String s = "";
        if(primitiveRange != null)
            s += primitiveRange.toString();
        if(flags instanceof FieldFlags) {
            if(((FieldFlags)flags).context != Context.NON_STATIC)
                s += "static ";
        } else if(flags instanceof LocalFlags)
            s += "local ";
        else
            throw new RuntimeException("unknown type of flags");
        if(flags.isFinal())
            s += "final ";
        String t;
        if(type != null)
            t = type.toNameString();
        else
            t = "(no type)";
        s += t + " " + name;
        if(false)
            s += "{" + super.toString() + "}";
        return s;
    }
}
