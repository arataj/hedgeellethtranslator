/*
 * CodeVariableOwner.java
 *
 * Created on Apr 12, 2008
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree;

import pl.gliwice.iitis.hedgeelleth.compiler.code.*;

/**
 * An object that can own a variable, either <code>AbstractJavaClass</code>
 * for fields or <code>Method</code> for locals.
 * 
 * @author Artur Rataj
 */
public interface VariableOwner {
}
