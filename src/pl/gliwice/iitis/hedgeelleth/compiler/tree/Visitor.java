/*
 * Visitor.java
 *
 * Created on Jan 15, 2008, 12:39:00 PM
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.AbstractExpression;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;

/**
 * A visitor interface.
 * 
 * @author Artur Rataj
 */
public interface Visitor<T> {
    public T visit(Node n) throws CompilerException ;
    public T visit(EmptyExpression e) throws CompilerException ;
    public T visit(ConstantExpression e) throws CompilerException ;
    public T visit(AllocationExpression e) throws CompilerException ;
    public T visit(PrimaryExpression e) throws CompilerException ;
    public T visit(UnaryExpression e) throws CompilerException ;
    public T visit(BinaryExpression e) throws CompilerException ;
    public T visit(AssignmentExpression e) throws CompilerException ;
    public T visit(CallExpression e) throws CompilerException ;
    public T visit(IndexExpression e) throws CompilerException ;
    public T visit(BlockExpression e) throws CompilerException ;
    public T visit(AbstractExpression e) throws CompilerException ;
    public T visit(ReturnStatement s) throws CompilerException ;
    public T visit(ThrowStatement s) throws CompilerException ;
    public T visit(LabeledStatement s) throws CompilerException ;
    public T visit(JumpStatement s) throws CompilerException ;
    public T visit(BranchStatement s) throws CompilerException ;
    public T visit(SynchronizedStatement s) throws CompilerException ;
    public T visit(Block b) throws CompilerException ;
    public T visit(Compilation c)  throws CompilerException ;
}
