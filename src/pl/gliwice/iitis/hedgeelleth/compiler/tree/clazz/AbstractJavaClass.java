/*
 * AbstractJavaClass.java
 *
 * Created on Dec 12, 2007, 12:16:35 PM
 *
 * Copyright (c) 2007 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree.clazz;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.CodeClass;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.flags.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.scope.*;
import pl.gliwice.iitis.hedgeelleth.modules.AbstractFrontend;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * A java class definition. Its subclasses are either a class
 * <code>ImplementingClass</code> or an interface <code>Interface</code>.<br>
 * 
 * Each java class is assigned to a given compiler frontend, what decides about
 * the semantic check of all <code>BlockExpression</code> objects within the
 * methods' code. See <code>AbstractFrontend</code> for details.<br>
 *
 * Natural order of the java classes is based on their stream positions.
 *
 * @author Artur Rataj
 */
public abstract class AbstractJavaClass implements Node, SourcePosition,
        VariableOwner /*, Comparable<AbstractJavaClass>*/ {
    /**
     * Name of the method for initilization of static fields.
     */
    public static final String INIT_STATIC = "*static";
    /**
     * Name of the method for initilization of object fields.
     */
    public static final String INIT_OBJECT = "*object";

    /**
     * Frontend of this java class.<br>
     *
     * See this class docs for details.
     */
    public AbstractFrontend frontend;
    /**
     * Scope of the Java type, contains fields and methods.
     * The method named INIT_STATIC is for initialization
     * of static fields, the method named INIT_OBJECT if for
     * initialization of object fields.
     */
    public JavaClassScope scope;
    /**
     * Flags of this class.
     */
    public JavaClassFlags flags;
    /**
     * Name of this type.
     */
    public String name;
    /**
     * Names of parameters, empty list for none.
     */
    public List<String> parameterNames;
    /**
     * Tags, empty list for no tags. By default an empty list.
     */
    public List<CommentTag> tags;
    /**
     * If this class belongs to the standard library;
     */
    public boolean isFromLibrary;
    /**
     * A type of the java class that is extended by this
     * java class, or null if this java class is the special
     * class object.
     */
    public Type extendS;
   /**
     * Types of the interfaces that are implemented by this
     * java class. Empty list for none.
     */
    public List<Type> implementS;
    /**
     * Subclasses of this class. Completed at the beginning
     * of the semantic check.
     */
    public Set<AbstractJavaClass> subclasses;
    /**
     * A special comment directly before the class, in the case of
     * Java a `**' comment. Null if none. Its only use is to be copied
     * to <code>CodeClass.specialComment</code>.
     */
    public String specialComment;
    /**
     * Position of this class in the parsed stream.
     */
    protected StreamPos pos;
    /**
     * If signatures have already been parsed.
     */
    boolean signaturesParsed;
    /**
     * Code object of this class.
     */
    public CodeClass codeClass;
    /**
     * Current unique identifier of a field, that determines a part of 
     * a primitive range of some method.
     */
    public int currReturnFieldId = 0;
    
    /**
     * Creates a new instance of AbstractJavaClass.
     *
     * @param pos                       position in the parsed stream
     * @param unit                      compile unit
     * @param name                      name of this java class
     */
    public AbstractJavaClass(StreamPos pos, Unit unit, String name) {
        setStreamPos(pos);
        frontend = unit.frontend;
        this.name = name;
        flags = new JavaClassFlags();
        scope = new JavaClassScope(this, unit.packageScope, name);
        scope.classImports = Import.getNames(unit.importDeclarations);
        scope.staticImports = Import.getNames(unit.staticImportDeclarations);
        tags = new LinkedList<>();
        isFromLibrary = false;
        extendS = null;
        implementS = new LinkedList<>();
        specialComment = null;
    }
    /**
     * Finds java classs to make signatures containing classes right.
     * A method's key in the scope is updated if needed.<br>
     *
     * If the signatures have already been parsed for this type, does
     * nothing.
     */
    public void parseSignatures() throws ParseException {
        if(!signaturesParsed) {
            for(MethodSignature key : new LinkedList<MethodSignature>(scope.methods.keySet())) {
                Method m = scope.methods.get(key);
                if(SemanticCheck.findJavaClass(scope, m)) {
                    // change the method key if the signature has
                    // possibly changed
                    scope.methods.remove(key);
                    scope.methods.put(m.signature, m);
                    int i = scope.listOfMethods.indexOf(key);
                    scope.listOfMethods.remove(i);
                    scope.listOfMethods.add(i, m.signature);
                }
            }
            signaturesParsed = true;
        }
    }
    /**
     * Returns a fully qualified name of this java class.
     * 
     * @return                          fully qualified name
     */
    public NameList getQualifiedName() {
        NameList n;
        if(scope.parent instanceof PackageScope)
            // this is not a nested class
            n = new NameList(((PackageScope)scope.parent).
                    getPackageName());
        else
            // this is a nested class
            n = new NameList(((AbstractJavaClass)scope.parent.owner).
                    getQualifiedName());
        n.list.add(name);
        return n;
    }
    /**
     * If this class is equal to or extends another one.<br>
     *
     * See docs of <code>Interface</code> on extending and implementing
     * classes by interfaces.<br>
     *
     * @param scope                     scope
     * @param clazz                     java class
     * @return                          if extends the java class
     */
    public boolean isEqualToOrExtending(AbstractScope scope, AbstractJavaClass clazz)
            throws ParseException {
        if(extendS != null) {
            AbstractJavaClass t = scope.lookupJavaClassNA(extendS.getJava());
            while(t != null) {
                if(t.equals(clazz))
                    return true;
                if(t.extendS == null)
                    break;
                t = scope.lookupJavaClassNA(t.extendS.getJava());
            }
        }
        return false;
    }
    /**
     * If this java class is implementing an interface.<br>
     *
     * See docs of <code>Interface</code> on extending and implementing
     * classes by interfaces.
     *
     * @param scope                     scope
     * @param clazz                     java class, if an instance of
     *                                  <code>ImplementingClass</code>
     *                                  then this method always returns
     *                                  false
     * @return                          if implements the interface
     */
    public boolean isImplementing(AbstractScope scope, AbstractJavaClass clazz)
            throws ParseException {
        if(clazz instanceof ImplementingClass)
            return false;
        else {
            boolean isImplementing = false;
            Interface searchedInterface = (Interface)clazz;
            Queue<Interface> interfaces = new LinkedList<>();
            for(Type t : implementS) {
                Interface i = (Interface)scope.lookupJavaClassNA(t.getJava());
                interfaces.add(i);
            }
            Interface currInterface;
            while((currInterface = interfaces.poll()) != null) {
                for(Type t : currInterface.implementS) {
                    Interface i = (Interface)searchedInterface.scope.lookupJavaClassAT(t.getJava());
                    interfaces.add(i);
                }
                if(currInterface == searchedInterface) {
                    isImplementing = true;
                    break;
                }
            }
            return isImplementing;
        }
    }
    @Override
    public Object accept(Visitor v) throws CompilerException {
        if(VERBOSE)
            System.out.println("Java type " + this);
        return v.visit(this);
    }
    @Override
    public final void setStreamPos(StreamPos pos) {
        this.pos = pos;
    }
    @Override
    public final StreamPos getStreamPos() {
        return pos;
    }
    /**
     * Returns the grammatical name of this java class.
     * 
     * @return a name
     */
    abstract public String getGrammaticalName();
    /*
    @Override
    public int compareTo(AbstractJavaClass o) {
        return getStreamPos().compareTo(o.getStreamPos());
    }
     */
    /**
     * Returns <code>currReturnFieldId</code> and the advances it.
     * 
     * @return an unique identifier of a return fieldf
     */
    public int nextReturnFieldId() {
        return currReturnFieldId++;
    }
}
