/*
 * ImplementingClass.java
 *
 * Created on Apr 26, 2009, 1:23:58 PM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree.clazz;

import pl.gliwice.iitis.hedgeelleth.compiler.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.flags.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.Block;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * A class definition.
 *
 * @author Artur Rataj
 */
public class ImplementingClass extends AbstractJavaClass {
    /**
     * Creates a new instance of ImplementingClass.
     *
     * @param pos                       position in the parsed stream
     * @param unit                      compile unit
     * @param name                      name of this java class
     */
    public ImplementingClass(StreamPos pos, Unit unit, String name) {
        super(pos, unit, name);
        MethodFlags staticFlags = new MethodFlags();
        staticFlags.context =  Context.STATIC;
        MethodFlags objectFlags = new MethodFlags();
        try {
            Method s = new Method(new StreamPos(name + " initialization of static fields"),
                    this, INIT_STATIC, Type.newVoid(), null, staticFlags, null);
            s.code = new Block(pos, s.scope);
            Method o = new Method(new StreamPos(name + " initialization of non--static fields"),
                    this, INIT_OBJECT, Type.newVoid(), null, objectFlags, null);
            o.code = new Block(pos, o.scope);
            scope.addMethod(s);
            scope.addMethod(o);
        } catch(CompilerException e) {
            // should not happen
            throw new RuntimeException(e.toString());
        }
    }
    @Override
    public String getGrammaticalName() {
        return "class";
    }
    @Override
    public String toString() {
        return getGrammaticalName() + " " + getQualifiedName();
    }
}
