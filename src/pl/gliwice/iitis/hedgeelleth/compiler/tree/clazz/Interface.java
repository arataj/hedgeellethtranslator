/*
 * Interface.java
 *
 * Created on Dec 13, 2007, 12:36:59 PM
 *
 * Copyright (c) 2007 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree.clazz;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * An interface -- pure abstract class, that contains only methods.<br>
 *
 * Interfaces never extend anything but the object class. Instead, they
 * can implement another interface.
 *
 * @author Artur Rataj
 */
public class Interface extends AbstractJavaClass {
    /**
     * Creates a new instance of Interface. Flags are set to abstract.
     *
     * @param pos                       position in the parsed stream
     * @param unit                      compile unit
     * @param name                      name of this java class
     */
    public Interface(StreamPos pos, Unit unit, String name) {
        super(pos, unit, name);
        flags.setAbstract(true);
    }
    @Override
    public String getGrammaticalName() {
        return "interface";
    }
    @Override
    public String toString() {
        return getGrammaticalName() + " " + getQualifiedName();
    }
}
