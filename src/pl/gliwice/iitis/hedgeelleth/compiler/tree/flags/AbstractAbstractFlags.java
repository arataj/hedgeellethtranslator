/*
 * AbstractAbstractFlags.java
 *
 * Created on Mar 31, 2009, 1:03:28 PM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree.flags;

/**
 * Abstraction flag, implemented by <code>JavaClassFlags</code> and
 * <code>MethodFlags</code>. All implementers make the abstract flag
 * false by default.
 *
 * @author Artur Rataj
 */
public interface AbstractAbstractFlags {
    /**
     * Set if is abstract.
     *
     * @param abstracT                  sets if is abstract
     */
    public void setAbstract(boolean abstracT);
    /**
     * If is abstract.
     *
     * @return                          if is abstract
     */
    public boolean isAbstract();
}
