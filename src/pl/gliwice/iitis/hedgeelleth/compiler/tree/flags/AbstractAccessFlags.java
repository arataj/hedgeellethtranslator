/*
 * AbstractAccessFlags.java
 *
 * Created on Jan 22, 2008, 6:54:37 PM
 *
 * This file is copyright (c) 2008 Artur Rataj.
 *
 * This software is provided under the terms of the GNU Library General
 * Public License, either version 2 of the license or, at your option,
 * any later version. See http://www.gnu.org for details.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree.flags;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;

/**
 * An abstract container for java class, field and method modifiers.
 * It contains only access modifiers. The class <code>LocalFlags</code>
 * is outside of the hierarchy below this class.
 * 
 * @author Artur Rataj
 */
public abstract class AbstractAccessFlags {
    /**
     * Access modifier. For classes, it can only be PRIVATE,
     * INTERNAL or PUBLIC. The default is PRIVATE, also for all
     * subclasses.
     */
    public AccessModifier am = AccessModifier.PRIVATE;

    /**
     * Creates flags with default values.
     */
    public AbstractAccessFlags() {
        /* empty */
    }
    /**
     * Creates flags with values copied form another object.
     *
     * @param s                         source flags whose values are copied
     *                                  to these flags
     */
    public AbstractAccessFlags(AbstractAccessFlags s) {
        am = s.am;
    }
}
