/*
 * AbstractMemberFlags.java
 *
 * Created on Mar 31, 2009, 12:25:41 PM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree.flags;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.Context;

/**
 * Flags for java class contents: fields and methods. Contains
 * the context flag. Extended by <code>FieldFlags</code> and
 * <code>MethodFlags</code>.
 *
 * @author Artur Rataj
 */
public abstract class AbstractMemberFlags extends AbstractAccessFlags {
    /**
     * Context, the default is non--static, and must be so also in all subclasses.
     * 
     * In the case of the context EMPTY, this field is set to NON_STATIC, and
     * a special flag <code>empty</code> within <code>MethodFlags</code>
     * is set to true. See the docs of <code>Context</code> for details on why
     * it is so.
     */
    public Context context = Context.NON_STATIC;
    /**
     * Creates a new instance of AbstractMemberFlags.
     */
    public AbstractMemberFlags() {
        super();
    }
    /**
     * Creates flags with values copied form another object.
     *
     * @param s                         source flags whose values are copied
     *                                  to these flags
     */
    public AbstractMemberFlags(AbstractMemberFlags s) {
        super(s);
        context = s.context;
    }
}
