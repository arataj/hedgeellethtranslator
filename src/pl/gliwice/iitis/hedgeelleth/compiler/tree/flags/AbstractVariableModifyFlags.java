/*
 * AbstractVariableModifyFlags.java
 *
 * Created on Mar 31, 2009, 12:48:35 PM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree.flags;

/**
 * Variable modification flag -- final. Implemented by
 * <code>FieldFlags</code> and <code>LocalFlags</code>.
 *
 * @author Artur Rataj
 */
public interface AbstractVariableModifyFlags {
    /**
     * Sets the value of the <code>finaL</code> flag.
     *
     * @param finaL                     sets if is final
     */
    public abstract void setFinal(boolean finaL);
    /**
     * Returns the value of the <code>finaL</code> flag. The
     * default is false for all implementors.
     *
     * @return                          if is final
     */
    public abstract boolean isFinal();
    /**
     * Returns is the value of <code>am</code> is
     * <code>AccessModifier.PUBLIC</code>. Throws a runtime
     * exception if a given kind of variable can never be public.<br>
     * 
     * This method is typically used only by the code objects, which set
     * set their API property.
     *
     * @return                          if is public
     */
    public abstract boolean isPublic();
    /*
     * Sets the value of the <code>consT</code> flag. Non--null
     * can be only for java class variables.
     *
     * @param consT                     sets if is constant
     */
    // public abstract void setConst(boolean consT);
    /*
     * Returns the value of the <code>consT</code> flag. Works
     * only for java class variables, otherwise is null, so that
     * this method throws a runtime exception. The default is
     * null for all implementors.
     *
     * @return                          if is constant
     */
    // public abstract boolean isConst();
}
