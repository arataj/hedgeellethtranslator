/*
 * FieldFlags.java
 *
 * Created on Mar 31, 2009, 12:45:28 PM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree.flags;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.AccessModifier;

/**
 * Flags for field. Implements final, adds blank final.
 *
 * @author Artur Rataj
 */
public class FieldFlags extends AbstractMemberFlags implements AbstractVariableModifyFlags {
    /**
     * If the variable can <i>not</i> be assigned more than once,
     * not counting the default initializer. The default is false.
     */
    private boolean finaL = false;
    /**
     * If the variable is <code>finaL</code>, but has no intializer,
     * and thus can once be initialized by a source-level assignment.
     * The default is false.
     */
    private boolean blankFinal = false;
    // /**
    //  * If the java class variable can <i>not</i> have its non--const
    //  * methods used, the default is null, what causes a runtime exception
    //  * to be thrown by <code>getConst</code>. Can be not null only for
    //  * java class variables.
    //  */
    // private Boolean consT = null;

    /**
     * Creates a new instance of FieldFlags. The defaults are non--final,
     * non--const non--static private field.
     */
    public FieldFlags() {
        super();
    }
    /**
     * Creates flags with values copied form another object.
     *
     * @param s                         source flags whose values are copied
     *                                  to these flags
     */
    public FieldFlags(FieldFlags s) {
        super(s);
        finaL = s.finaL;
        blankFinal = s.blankFinal;
        // consT = s.consT;
    }
    @Override
    public void setFinal(boolean finaL) {
        this.finaL = finaL;
    }
    @Override
    public boolean isFinal() {
        return finaL;
    }
    @Override
    public boolean isPublic() {
        return am.hasPublicAccess();
    }
    /*
    @Override
    public void setConst(boolean consT) {
        this.consT = consT;
    }
    @Override
    public boolean isConst() {
        return consT;
    }
     */
    /**
     * Sets the value of the <code>blankFinal</code> flag.
     *
     * @param blankFinal                sets if is a blank final
     */
   public void setBlankFinal(boolean blankFinal) {
        this.blankFinal = blankFinal;
    }
    /**
     * Returns the value of the <code>blankFinal</code> flag.
     *
     * @return                          if is a blank final
     */
    public boolean isBlankFinal() {
        return blankFinal;
    }
}
