/*
 * JavaClassFlags.java
 *
 * Created on Mar 31, 2009, 12:28:36 PM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree.flags;

/**
 * Flags for a java class.
 *
 * @author Artur Rataj
 */
public class JavaClassFlags extends AbstractAccessFlags
        implements AbstractAbstractFlags {
    /**
     * If this java class can be extended, the default is true.
     */
    public boolean extendable = true;
    /**
     * If this java class is abstract, the default is false.
     */
    private boolean abstracT = false;
    /**
     * If mutator flags on methods are allowed by this class. If true, all
     * subclasses must allow it also. It is false for the special object
     * class. The default is false.
     */
    public boolean allowsMutatorFlag = false;

    /**
     * Creates a new instance of JavaClassFlags. The defaults are
     * private, non--abstract extendable class.
     */
    public JavaClassFlags() {
        super();
    }
    /**
     * Creates flags with values copied form another object.
     *
     * @param s                         source flags whose values are copied
     *                                  to these flags
     */
    public JavaClassFlags(JavaClassFlags s) {
        super(s);
        extendable = s.extendable;
        abstracT = s.abstracT;
        allowsMutatorFlag = s.allowsMutatorFlag;
    }
    @Override
    public void setAbstract(boolean abstracT) {
        this.abstracT = abstracT;
    }
    @Override
    public boolean isAbstract() {
        return abstracT;
    }
}
