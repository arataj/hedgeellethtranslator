/*
 * FieldFlags.java
 *
 * Created on Mar 31, 2009, 12:45:28 PM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree.flags;

/**
 * Flags for local variables. Implements final.
 *
 * @author Artur Rataj
 */
public class LocalFlags implements AbstractVariableModifyFlags {
    /**
     * If the variable can be modified after initialization,
     * the default is true.
     */
    private boolean finaL = false;
    // /**
    //  * If the java class variable can <i>not</i> have its non--const
    //  * methods used, the default is null, what causes a runtime exception
    //  * to be thrown by <code>getConst</code>. Can be not nul only for
    //  * java class variables.
    //  */
    // private Boolean consT = null;

    /**
     * Creates a new instance of LocalFlags. The defaults are non--final,
     * non--const local.
     */
    public LocalFlags() {
        super();
    }
    /**
     * Creates flags with values copied form another object.
     *
     * @param s                         source flags whose values are copied
     *                                  to these flags
     */
    public LocalFlags(LocalFlags s) {
        finaL = s.finaL;
        // consT = s.consT;
    }
    @Override
    public void setFinal(boolean finaL) {
        this.finaL = finaL;
    }
    @Override
    public boolean isFinal() {
        return finaL;
    }
    @Override
    public boolean isPublic() {
        throw new RuntimeException("local can never be public");
    }
    /*
    @Override
    public void setConst(boolean consT) {
        this.consT = consT;
    }
    @Override
    public boolean isConst() {
        return consT;
    }
     */
}
