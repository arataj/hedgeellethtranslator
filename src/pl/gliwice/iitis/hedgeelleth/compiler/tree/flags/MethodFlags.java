/*
 * MethodFlags.java
 *
 * Created on Mar 31, 2009, 12:21:58 PM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree.flags;

/**
 * Flags specific for a method.
 *
 * @author Artur Rataj
 */
public class MethodFlags extends AbstractMemberFlags
         implements AbstractAbstractFlags {
    /**
     * Values of the <code>override</code> flag.
     */
    public enum Overrides {
        /**
         * Do not check.
         */
        NEUTRAL,
        /**
         * Does not override a method.
         */
        FALSE,
        /**
         * Overrides a method.
         */
        TRUE,
    };
    /**
     * Values of the <code>implementS</code> flag.
     */
    public enum Implements {
        /**
         * Do not check.
         */
        NEUTRAL,
        /**
         * Does not implement an abstract method.
         */
        FALSE,
        /**
         * Implements an abstract method.
         */
        TRUE,
    };
    /**
     * If synchronized, used only by methods.
     */
    public boolean synchronizeD = false;
    /**
     * If a method overrides another, non--abstract one. Used only by
     * non--abstract methods. The default is <code>NEUTRAL</code>.
     */
    public Overrides overrides = Overrides.NEUTRAL;
    /**
     * If allows being implemented or overridden. The default is true.
     */
    public boolean allowsOverride = true;
    /**
     * If a method implements an abstract one. If the declaration of
     * an abstract method is repeated in a subclass, then the submethod
     * must this field set to <code>NEUTRAL</code>.
     * The default is <code>NEUTRAL</code>.
     */
    public Implements implementS = Implements.NEUTRAL;
    /**
     * If this method is abstract, the default is false.
     */
    private boolean abstracT = false;
    /**
     * If this method is an explicit mutator. Can be true only if this method's
     * owner <code>AbstractJavaClass.allowsMutatorModifier</code> is true.<br>
     * Overriding methods must match this flag.<br>
     *
     * The default is false.
     */
    public boolean mutator = false;
    /**
     * If limitations of the empty context hold.
     */
    public boolean empty = false;

    /**
     * Creates a new instance of MethodFlags. The defaults are non--abstract,
     * non--const non--static private method, that does not declare if
     * overrides or implements and allows for override.
     */
    public MethodFlags() {
        super();
    }
    /**
     * Creates flags with values copied form another object.
     *
     * @param s                         source flags whose values are copied
     *                                  to these flags
     */
    public MethodFlags(MethodFlags s) {
        super(s);
        synchronizeD = s.synchronizeD;
        overrides = s.overrides;
        allowsOverride = s.allowsOverride;
        implementS = s.implementS;
        abstracT = s.abstracT;
        mutator = s.mutator;
        empty = s.empty;
    }
    @Override
    public void setAbstract(boolean abstracT) {
        this.abstracT = abstracT;
    }
    @Override
    public boolean isAbstract() {
        return abstracT;
    }
}
