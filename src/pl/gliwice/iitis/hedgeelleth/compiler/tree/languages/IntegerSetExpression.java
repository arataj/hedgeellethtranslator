/*
 * IntegerSetExpression.java
 *
 * Created on Jun 25, 2009, 1:48:19 PM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree.languages;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.Literal;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Type;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.scope.BlockScope;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.ConstantExpression;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * An expression definining a set of integer values of type LONG.<br>
 *
 * To be used as an initializer of a field or an element of
 * the field. Those in turn are used to define experiments.
 *
 * @author Artur Rataj
 */
public class IntegerSetExpression extends ConstantExpression implements Cloneable {
    /**
     * End position of this expression in the stream.<br>
     * 
     * Used to replace the integer set expressions in source
     * files with individual elements in generated source
     * files, suitable for compiling.
     */
    public StreamPos endPos;
    /**
     * Values in the set defined by this expression.
     */
    public SortedSet<Long> values;
    /**
     * Fully qualified name of the initialized static field.
     */
    public String fullyQualifiedFieldName;

    /**
     * Creates a new instance of IntegerSetExpression. The argument's
     * contents is copied to another set.<br>
     *
     * Type of this constant is the minimum one that bounds all values.
     *
     * @param pos                       position in the parsed stream
     * @param endPos                    position of the expression's last
     *                                  character in the parsed stream
     * @param outerScope                scope this expression belongs to
     * @param values                    set of values
     * @param fullyQualifiedFieldName   fully qualified name of the
     *                                  initialized static field, or null
     *                                  if this identifier does not have to
     *                                  be set; a known usage is sorting
     *                                  the expressions and naming files if
     *                                  experiments are allowed
     */
    public IntegerSetExpression(StreamPos beginPos, StreamPos endPos,
            BlockScope outerScope, Collection<Literal> values, String fullyQualifiedFieldName) {
        super(beginPos, outerScope, null);
        Set<Type> types = new HashSet<>();
        this.values = new TreeSet<>();
        for(Literal l : values) {
            types.add(l.type);
            this.values.add(l.getMaxPrecisionInteger());
        }
        setResultType(Type.integerTypesToBounding(types));
        this.endPos = endPos;
        this.fullyQualifiedFieldName = fullyQualifiedFieldName;
    }
    @Override
    public IntegerSetExpression clone() {
        IntegerSetExpression copy = (IntegerSetExpression)super.clone();
        copy.values = new TreeSet<>(copy.values);
        return copy;
    }
}
