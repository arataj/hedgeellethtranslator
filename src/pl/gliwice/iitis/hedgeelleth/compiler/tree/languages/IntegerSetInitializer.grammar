/*
 * A sorted integer set containing values for initializing of fields, for
 * defining experiments.
 *
 * Grammar:
 *
 *     IntegerSetInitializer ::= IntegerSetInitializerElement
 *             ( "#" IntegerSetInitializerElement )*
 *     IntegerSetInitializerElement ::= SignedLiteral |
 *              IntegerSetInitializerRange
 *     IntegerSetInitializerRange ::= SignedLiteral "~"
 *             SignedLiteral [ ":" SignedLiteral ]
 *     SignedLiteral ::= [ - ] HedgeellethLiteral
 *
 * <code>HedgeellethLiteral.type.isOfIntegerTypes()</code> must be true
 * for all literals within this grammar.
 *
 * Provided productions: IntegerSetInitializer
 *         IntegerSetInitializerElement IntegerSetInitializerRange
 * Top productions: IntegerSetInitializer
 *
 * Parameters:
 *
 *     none
 *
 * Requires: HedgeellethLiteral, hedgeelleth's classUniqueIdentifier to be initialized
 * with fully qualified class' name.
 *
 * Example:
 *
 *     "-2~10:2#20" defines set {-2, 0, 2, 4, 6, 8, 10, 20}
 *
 * Copyright 2009, 2015 Artur Rataj.
 * Parts of the code that are taken from JavaCC demo grammars
 * are copyright (c) 2008 Sun Microsystem.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

/**
  * Top production, returns <code>IntegerSetExpression</code>.
  *
  * In the case of parse error, reports the error and returns null.
  *
  * @param initializerScope             scope of this initializer
  * @param uniqieIdentifier             identifier of the expression, null if
  *                                     the expression's identifier should be null
  */
IntegerSetExpression IntegerSetInitializer(BlockScope initializerScope,
        String uniqueIdentifier) :
{
    List<Literal> values;
    List<Literal> s;
    StreamPos pos = new StreamPos();
}
{
    values = IntegerSetInitializerElement(pos)
    ( "#" s = IntegerSetInitializerElement(null) {
        if(values != null && s != null)
            values.addAll(s);
        else
            values = null;
    } )*
    {
        if(values != null) {
            Long prev = null;
            for(Literal l : values) {
                long i = l.getMaxPrecisionInteger();
                if(prev != null && prev >= i)
                    hedgeelleth.addParseException(
                            new ParseException(pos,
                                ParseException.Code.ILLEGAL,
                                "values not in increasing order"));
                prev = i;
            }
            StreamPos endPos = new StreamPos(streamName,
                token.endLine, token.endColumn);
            return new IntegerSetExpression(pos, endPos, initializerScope,
                values, uniqueIdentifier);
        } else
            return null;
    }
}

/**
 * A single element, either a number or range definition.
 *
 * @param pos                           if not null, position of the first token is
 *                                      copied to this parameter
 */
List<Literal> IntegerSetInitializerElement(StreamPos pos) :
{
    List values = new LinkedList<Long>();
    Literal iL;
    Literal minL;
    Literal maxL;
    Literal stepL = null;
    long i;
    long min = 0;
    long max = 0;
    long step = 1;
}
{
    (
            LOOKAHEAD([ "-" ] <INTEGER_LITERAL> "~") (
                minL = SignedLiteral() {
                    StreamPos currPos = getStreamPos();
                    if(pos != null)
                        pos.set(currPos);
                    if(minL == null)
                        values = null;
                    else
                        min = minL.getMaxPrecisionInteger();
                } "~" maxL = SignedLiteral() {
                    if(maxL == null)
                        values = null;
                    else
                        max = maxL.getMaxPrecisionInteger();
                }
                [
                    ":" stepL = SignedLiteral() {
                        if(stepL == null)
                            values = null;
                        else
                            step = stepL.getMaxPrecisionInteger();
                    }
                ] {
                    if(step == 0) {
                       hedgeelleth.addParseException(
                           new ParseException(currPos, ParseException.Code.ILLEGAL,
                           "step is zero"));
                        values = null;
                    }
                    if(values != null) {
                        if((min < max && step < 0) ||
                                (min > max && step > 0)) {
                           hedgeelleth.addParseException(
                               new ParseException(currPos, ParseException.Code.ILLEGAL,
                               "step sign mismatch"));
                            values = null;
                        }
                        // find the set's type
                        Set<Type> types = new HashSet<Type>();
                        types.add(minL.type);
                        types.add(maxL.type);
                        if(stepL == null)
                            types.add(new Type(Type.PrimitiveOrVoid.BYTE));
                        else
                            types.add(stepL.type);
                        Type outType = Type.integerTypesToBounding(types);
                        for(i = min; i <= max; i += step)
                            values.add(new Literal(outType, i));
                        if(i - step != max) {
                           hedgeelleth.addParseException(
                               new ParseException(currPos, ParseException.Code.ILLEGAL,
                               "values mismatch"));
                            values = null;
                        }
                    }
                }
            )
        |
            iL = SignedLiteral() {
                if(pos != null)
                    pos.set(getStreamPos());
                if(iL != null)
                    values.add(iL);
                else
                    values = null;
            }
    )
    {
        return values;
    }
}

/**
  * An integer value, possibly negated.
  */
Literal SignedLiteral() :
{
    boolean negated = false;
    Literal intValue;
}
{
    [ "-" { negated = true; } ] intValue = HedgeellethLiteral() {
        if(!intValue.type.isOfIntegerTypes()) {
           hedgeelleth.addParseException(
               new ParseException(getStreamPos(), ParseException.Code.ILLEGAL,
               "constant not integer"));
            intValue = null;
        } else if(negated) {
            if(intValue.type.getPrimitive() == Type.PrimitiveOrVoid.LONG)
                intValue = new Literal(-intValue.getLong());
            else
                // promote to integer
                intValue = new Literal(-intValue.getInteger());
        }
        return intValue;
    }
}
