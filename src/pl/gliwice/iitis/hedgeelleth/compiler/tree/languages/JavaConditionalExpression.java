/*
 * JavaConditionalExpression.java
 *
 * Created on Feb 13, 2009, 11:50:35 AM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree.languages;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.AbstractExpression;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.scope.BlockScope;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * A conditional expression. This is not a standard Hedgeelleth
 * expression, and is processed in a custom semantic checker
 * <code>JavaSemanticCheck</code>.
 *
 * @author Artur Rataj
 */
public class JavaConditionalExpression extends BlockExpression {
    /**
     * Condition.
     */
    public AbstractExpression condition;
    /**
     * Expression in the true branch.
     */
    public AbstractExpression truE;
    /**
     * Expression in the false branch.
     */
    public AbstractExpression falsE;

    /**
     * Creates a new instance of JavaConditionalExpression.
     *
     * @param pos                       position in the source code
     * @param outerScope                scope this expression belongs to
     * @param condition                 condition expression
     * @param truE                      true branch expression
     * @param falseE                    false branch expression
     */
    public JavaConditionalExpression(StreamPos pos, BlockScope outerScope,
            AbstractExpression condition, AbstractExpression truE,
            AbstractExpression falsE) {
        super(pos, outerScope, null);
        this.condition = condition;
        this.truE = truE;
        this.falsE = falsE;
    }
}
