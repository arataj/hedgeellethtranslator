/*
 * JavaSemanticCheck.java
 *
 * Created on Feb 13, 2009, 11:45:32 AM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree.languages;

import pl.gliwice.iitis.hedgeelleth.Hedgeelleth;
import pl.gliwice.iitis.hedgeelleth.compiler.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.scope.BlockScope;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.Block;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;

/**
 * A semantic check adopted to build and test
 * <code>JavaConditionalExpression</code>. The expression is a subclass
 * of Hedgeelleth's standard <code>BlockExpression</code>,
 * so no further custom classes are needed for translating it.<br>
 *
 * If you want to use <code>SemanticCheck</code> for frontent compatibility
 * and still have <code>JavaConditionalExpression</code> parsed,
 * you might instead call the static method <code>parse()</code> from the
 * frontend.
 *
 * @author Artur Rataj
 */
public class JavaSemanticCheck extends SemanticCheck {

    /**
     * Creates a new instance of SemanticCheck.
     *
     * @param compilation               compilation to check
     *                                  semantically
     */
    public JavaSemanticCheck(Compilation compilation)
            throws CompilerException {
        super(compilation, null);
    }
    /**
     * Parses <code>JavaConditionalExpression</code>. To be called only by
     * <code>visit(BlockExpression)</code>, otherwise use <code>parse()</code>.
     * 
     * @param sc                        semantic check object
     * @param c                         expression to parse
     */
    protected static void parseInternal(SemanticCheck sc, JavaConditionalExpression c)
            throws CompilerException {
        c.condition.accept(sc);
        if(!c.condition.getResultType().isBoolean())
            throw new ParseException(c.condition.getStreamPos(),
                    ParseException.Code.ILLEGAL,
                    "found " + c.condition.getResultType().toNameString() +
                    ", required boolean");
        c.truE.accept(sc);
        c.falsE.accept(sc);
        Type tType = c.truE.getResultType();
        Type fType = c.falsE.getResultType();
        Type bounding = Type.mostTightBounding(
                c.outerScope, tType, fType);
        if(bounding == null)
{
Type.mostTightBounding(c.outerScope, tType, fType);
            throw new ParseException(c.getStreamPos(),
                    ParseException.Code.ILLEGAL,
                    "no common supertype of " +
                    tType.toNameString() + " and " +
                    fType.toNameString() + " found");
}
        Variable result =  c.outerScope.getBoundingMethodScope().newInternalLocal(
                c.getStreamPos(), bounding, null, Variable.Category.INTERNAL_NOT_RANGE);
        AssignmentExpression trueAssignment = Hedgeelleth.
                newAssignment(c.truE.getStreamPos(),
                    (BlockScope)c.outerScope, result, c.truE);
        AssignmentExpression falseAssignment = Hedgeelleth.
                newAssignment(c.falsE.getStreamPos(),
                    (BlockScope)c.outerScope, result, c.falsE);
        c.block = new Block(c.condition.getStreamPos(),
                    c.outerScope);
        Hedgeelleth.appendIfThenElseCode(null, c.block, null, c.condition,
                trueAssignment, falseAssignment, null, null);
        // it is an assignment to itself, to fulfill the requirement
        // of <code>BlockExpression</code> that the last operation
        // must be an assignment
        AssignmentExpression finalAssignment = Hedgeelleth.
                newAssignment(c.condition.getStreamPos(),
                    (BlockScope)c.outerScope, result,
                    Hedgeelleth.newVariableExpression(
                        c.condition.getStreamPos(),
                        (BlockScope)c.outerScope, result));
        c.block.code.add(finalAssignment);
        c.setResultType(bounding);
    }
    /**
     * Parses <code>JavaConditionalExpression</code>. It just checks
     * the class of <code>sc</code> for being an instance of
     * <code>JavaConditionalExpression</code> and if true throws a runtime
     * exception. If false, this method then calls <code>parseInternal</code>.
     *
     * @param sc                        semantic check object, if it is
     *                                  <code>JavaSemanticCheck</code> then
     *                                  a runtime exception is thrown, as
     *                                  that semantic checker already parses
     *                                  <code>JavaConditionalExpression</code>
     * @param c                         expression to parse
     */
    public static void parse(SemanticCheck sc, JavaConditionalExpression c)
                throws CompilerException {
        if(sc instanceof JavaSemanticCheck)
            throw new RuntimeException("semantic checker already parses " +
                    "JavaConditionalExpression");
        parseInternal(sc, c);
    }
    @Override
    public Object visit(BlockExpression e) throws CompilerException {
        if(e instanceof JavaConditionalExpression) {
            JavaConditionalExpression c = (JavaConditionalExpression)e;
            parse(this, c);
            log(4, c.toString());
        }
        return super.visit(e);
    }
}
