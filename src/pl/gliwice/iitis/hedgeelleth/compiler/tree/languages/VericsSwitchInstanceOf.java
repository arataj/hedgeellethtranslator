/*
 * VericsSwitchInstanceOf.java
 *
 * Created on May 16, 2009, 2:42:59 PM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree.languages;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.AbstractExpression;
import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.scope.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.Block;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * A Verics's switch instanceof expression. UNUSED.<br>
 *
 * The expression is made of code normally supported by Hedgeelleth, but
 * this custom class is still needed to check the expressions semantically,
 * what is impossible within the parser. The checking method is included
 * in this class.
 *
 * @author Artur Rataj
 */
public class VericsSwitchInstanceOf extends BlockExpression {
    /**
     * Expression with the reference to the object whose type
     * is determined.
     */
    AbstractExpression objectExpression;
    /**
     * Subsequent checked types, if A is anywhere before B, then it is
     * forbidden that B extends A. Thus, a type can not be followed
     * by its subclass in this sequence, so a simple ordered checking
     * until first positive finds the most specific type.
     */
    List<Type> types;
    /**
     * Positions of the types in the input stream.
     */
    List<StreamPos> typePositions;
    /**
     * Subsequent blocks, one for each type.
     */
    List<Block> blocks;

    /**
     * Creates a new instance of VericsSwitchInstanceOf.
     *
     * @param pos                       position in the source code
     * @param outerScope                scope this expression belongs to
     * @param objectExpression          expression returning the tested
     *                                  object's reference
     * @param types                     subsequent types
     * @param typePositions             positions of the types in the
     *                                  input stream
     * @param blocks                    subsequent blocks, one for each type
     */
    public VericsSwitchInstanceOf(StreamPos pos, BlockScope outerScope,
            AbstractExpression objectExpression, List<Type> types,
            List<StreamPos> typePositions, List<Block> blocks) {
        super(pos, outerScope, null);
        this.objectExpression = objectExpression;
        this.types = types;
        this.typePositions = typePositions;
        this.blocks = blocks;
    }
    /**
     * Parses <code>VericsSwitchInstanceOf</code>. To be called only by
     * <code>visit(BlockExpression)</code>, otherwise use <code>parse()</code>.
     *
     * @param sc                        semantic check object
     * @param iof                       expression to parse
     */
    public static void parse(SemanticCheck sc, VericsSwitchInstanceOf iof)
            throws CompilerException {
        AbstractLocalOwnerScope scope = iof.objectExpression.outerScope;
        StreamPos pos = iof.objectExpression.getStreamPos();
        iof.objectExpression.accept(sc);
        Type objectType = iof.objectExpression.getResultType();
        if(!objectType.isJava())
            throw new ParseException(pos,
                    ParseException.Code.ILLEGAL,
                    "found " + objectType.toNameString() +
                    ", required object reference");
        // semantic check
        Iterator<StreamPos> typePositions = iof.typePositions.iterator();
        List<Type> prev = new LinkedList<Type>();
        for(Type t : iof.types) {
            StreamPos typePos = typePositions.next();
            if(!t.isJava())
                throw new ParseException(typePos,
                        ParseException.Code.ILLEGAL,
                        "must be java type");
            if(!Type.convertible(scope, t, objectType))
                throw new ParseException(typePos,
                        ParseException.Code.ILLEGAL,
                        "inconvertible types");
            for(Type p : prev)
                if(Type.castable(scope, t, p, null))
                    throw new ParseException(typePos,
                            ParseException.Code.ILLEGAL,
                            "type follows its supertype");
            prev.add(t);
        }
        /*
        // adding expressions
        iof.block = new Block(pos, scope);
        AbstractStatement nested= null;
        for(int i = iof.types.size() - 1; i >= 0; --i) {
            Type t = iof.types.get(i);
            if(nested == null)
                nested = ;
        }
        AssignmentExpression finalAssignment = Hedgeelleth.
                newAssignment(c.condition.getStreamPos(),
                    (BlockScope)c.outerScope, result,
                    Hedgeelleth.newVariableExpression(
                        c.condition.getStreamPos(),
                        (BlockScope)c.outerScope, result));
        c.block.code.add(finalAssignment);
        c.setResultType(bounding);
         */
    }
}
