/*
 * AbstractAccessibilityScope.java
 *
 * Created on Mar 20, 2009, 5:07:11 PM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree.scope;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.clazz.AbstractJavaClass;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.PrimaryExpression;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.modules.AbstractFrontend;

/**
 * An abstract scope that has special lookup methods that
 * test for accessibility. Extended by <code>JavaClassScope<code>
 * and <code>AbstractLocalOwnerScope</code>.
 *
 * @author Artur Rataj
 */
public abstract class AbstractAccessibilityScope extends AbstractScope {
    /**
     * Creates a new instance of AbstractLocalOwnerScope.
     *
     * @param owner                     owner of this scope -- a java class
     * @param parent                    parent scope
     * @param key                       key of this scope, to be used
     *                                  by parent
     */
    public AbstractAccessibilityScope(Object owner, AbstractScope parent, String key) {
        super(owner, parent, key);
    }
    /**
     * Calls <code>lookupJavaClassNA</code> and then performs accessibility
     * testing.
     *
     * @param identifier                identifier, can be fully qualified
     * @return                          java class
     */
    public  abstract AbstractJavaClass lookupJavaClassAT(NameList identifier) throws ParseException;
    /**
     * Calls <code>lookupVariableNA</code> and then performs accessibility
     * testing.
     *
     * @param identifier                identifier, can be fully qualified
     * @param scopeMode                 scope mode, as defined in
     *                                  <code>PrimaryExpression.ScopeMode</code>
     * @return                          variable
     */
    public abstract Variable lookupVariableAT(NameList identifier,
            PrimaryExpression.ScopeMode scopeMode) throws ParseException;
    /**
     * Calls <code>lookupMethodNA</code> and then performs accessibility
     * testing.
     *
     * @param signature                 signature, can be fully qualified
     * @param scopeMode                 scope mode, as defined in
     *                                  <code>PrimaryExpression.ScopeMode</code>
     * @return                          method
     */
    public abstract Method lookupMethodAT(MethodSignature signature,
            PrimaryExpression.ScopeMode scopeMode) throws ParseException;
    /**
     * Looks up a method or a constructor, given the method's name.
     * Performs accessibility testing.<br>
     *
     * It computes the signature and calls <code>lookupMethodAT</code>, see
     * that method's docs for details.
     *
     * @param frontend                  frontend of the language in which
     *                                  the name is defined
     * @param name                      name, can be fully qualified
     * @param arg                       list of arguments
     * @param callScope                 call scope, as defined in
     *                                  <code>PrimaryExpression.CallMode</code>
     * @return                          method or null if not found
     */
    public Method lookupMethodByNameAT(AbstractFrontend frontend,
            NameList name, List<? extends Typed> arg,
            PrimaryExpression.ScopeMode callScope)
            throws ParseException {
        return lookupMethodAT(new MethodSignature(frontend,
                name.toString(), arg), callScope);
    }
    /**
     * Looks up a constructor, given its class and arguments. Performs
     * accessibility testing.
     *
     * It computes the signature and calls <code>lookupMethodNA</code>, see
     * that method's docs for details.
     *
     * @param frontend                  frontend of the language in which
     *                                  the type is defined
     * @param type                      type of the constructor's class
     * @param arg                       arguments of the constructor
     * @return                          constructor or null if not found
     */
    public Constructor lookupConstructorByClassAT(AbstractFrontend frontend,
            Type type, List<? extends Typed> arg) throws ParseException {
        NameList name = new NameList(type.getJava());
        name.list.add(name.last());
        return (Constructor)lookupMethodByNameAT(frontend, name, arg,
                PrimaryExpression.ScopeMode.ALL);
    }
}
