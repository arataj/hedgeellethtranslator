/*
 * LocalOwnerScope.java
 *
 * Created on Dec 9, 2008
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree.scope;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.clazz.AbstractJavaClass;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.flags.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * An abstract scope that is also a scope for local variables --
 * subclassed by a method scope and a block scope.<br>
 *
 * Because it is a common scope for all types of scopes within a method, it
 * also declares <code>newLabel</code>.<br>
 *
 * @author Artur Rataj
 */
public abstract class AbstractLocalOwnerScope extends AbstractAccessibilityScope {
    /**
     * Local variables of this scope.
     */
    public Map<String, Variable> locals;

    /**
     * Creates a new instance of AbstractLocalOwnerScope.
     *
     * @param owner                     owner of this scope -- a java class
     * @param parent                    parent scope
     * @param key                       key of this scope, to be used
     *                                  by parent
     */
    public AbstractLocalOwnerScope(Object owner, AbstractScope parent, String key) {
        super(owner, parent, key);
    }
    @Override
    public  AbstractJavaClass lookupJavaClassNA(NameList identifier) throws ParseException {
        return parent.lookupJavaClassNA(identifier);
    }
    @Override
    public Variable lookupVariableNA(NameList identifier) throws ParseException {
        return parent.lookupVariableNA(identifier);
    }
    @Override
    public Method lookupMethodNA(MethodSignature signature) throws ParseException {
        return parent.lookupMethodNA(signature);
    }
    /**
     * Looks up a variable only in locals of this scope.
     *
     * @param identifier                name of the variable
     * @return                          a local variable or null if not
     *                                  found
     */
    public Variable lookupLocalInThis(NameList identifier) throws ParseException {
        return locals.get(identifier.toString());
    }
    /**
     * Adds a local variable to this scope. It checks if it is not
     * duplicate in parent scopes recursively until a non--local owner
     * scope is found, and if yes, a parse exception is thrown.
     *
     * @param variable                  local to add
     */
    public void addLocalInThis(Variable variable) throws ParseException {
        AbstractLocalOwnerScope s = this;
        while(true) {
            if(s.lookupLocalInThis(new NameList(variable.name)) != null)
                throw new ParseException(variable.getStreamPos(),
                        ParseException.Code.DUPLICATE,
                        "duplicate declaration of " + variable.getSourceName());
            if(s.parent instanceof AbstractLocalOwnerScope)
                s = (AbstractLocalOwnerScope)s.parent;
            else
                break;
        }
        locals.put(variable.name, variable);
    }
    /**
     * Creates a new, unique local variable, without a specified primitive
     * range.
     *
     * @param pos                       position in the source stream,
     *                                  with which this variable is
     *                                  associated, null for none
     * @param optionalName              if not null, the new local's name,
     *                                  which must be unique,
     *                                  if null, an unique name is chosen
     * @param category                  category of the variable to create
     * @return                          new local variable
     */
    public synchronized Variable newInternalLocal(StreamPos pos, Type type,
            String optionalName, Variable.Category category) {
        LocalFlags flags = new LocalFlags();
        // local owners have <code>Method</code> as their variable
        // owners
        Method variableOwner = (Method)getBoundingVariableOwner();
        if(!category.isInternal())
            throw new RuntimeException("must be an internal variable");
        Variable variable = new Variable(pos, variableOwner, type, flags,
                optionalName != null ? optionalName : variableOwner.getNewInternalLocalId(),
                true, category);
        try {
            addLocalInThis(variable);
        } catch(ParseException e) {
            throw new RuntimeException("when adding internal local variable: " +
                    e.toString());
        }
        return variable;
    }
    /**
     * Returns a new unique label within a method, recognized by all
     * scopes within the method.<br>
     * 
     * After <code>CodeMethod</code> is created, should no longer be
     * used, but <code>CodeMethod.newLabel()</code> should be used
     * instead.
     *
     * @return                          an unique label
     */
    abstract public String newLabel();
    /**
     * Get a directly or indirectly bounding method scope.
     *
     * @return                          a method scope
     */
    public MethodScope getBoundingMethodScope() {
        if(parent instanceof BlockScope)
            return ((BlockScope)parent).getBoundingMethodScope();
        else
            return (MethodScope)parent;
    }
}
