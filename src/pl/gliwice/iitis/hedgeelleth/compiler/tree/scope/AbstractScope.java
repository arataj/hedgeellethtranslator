/*
 * Scope.java
 *
 * Created on Jan 16, 2008, 11:11:26 AM
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree.scope;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.clazz.AbstractJavaClass;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;

/**
 * A scope.<br>
 *
 * If a looked up name is qualified, it is always passed to either a
 * package scope or a top scope, where it is dispatched to get a class
 * name and an unqualified name. Then, the unqualified name is passed
 * back to children scopes.<br>
 *
 * Possible scope levels:<br>
 * TOP: TopScope,
 * PACKAGE: PackageScope,
 * JAVA_TYPE: JavaClassScope,
 * METHOD: MethodScope,
 * BLOCK: BlockScope.
 *
 * Lookup rules: name can be either unqualified or fully qualified,
 * and, if qualified, all name list segments but possibly the last one must
 * specify a java class. Accessibility testing occurs only at the level
 * of <code>AbstractAccessibilityScope</code> in lookup methods postfixed
 * with AT, see the class' docs for details. This class' lookup methods
 * that end with NA do not have accessibility testing, and thus should not
 * be called when one is needed.
 *
 * @author Artur Rataj
 */
public abstract class AbstractScope {
    /**
     * Owner of this scope : block, method or java class, or, if it is
     * a package scope, string with the package's name.<br>
     *
     * Only <code>AbstractJavaClass</code> or <code>Method</code> scope owners
     * are variable owners as well.
     */
    public Object owner;
    /**
     * Parent scope, null for TOP scope.
     */
    public AbstractScope parent;
    
    /**
     * Creates a new instance of Scope. Does not add this scope to the parent,
     * it must be done by a subclass.
     *
     * @param owner                     owner of this scope, can be
     *                                  block, method, java class or compilation
     * @param parent                    parent scope, or null if none
     * @param key                       key of this scope, to be used
     *                                  by parent, the key's meaning is
     *                                  defined by the parent.
     */
    public AbstractScope(Object owner, AbstractScope parent, String key) {
        this.owner = owner;
        this.parent = parent;
    }
    /**
     * Looks up a java class. Details can be found in lookup rules
     * of this class docs. If the java class is not found, an
     * exception is thrown.
     *
     * @param identifier                identifier, can be fully qualified
     * @return
     */
    public  abstract AbstractJavaClass lookupJavaClassNA(NameList identifier) throws ParseException;
    /**
     * Looks up a variable. Details can be found in lookup rules
     * of this class docs. If the variable is not found, an
     * exception is thrown.
     * 
     * @param identifier                identifier, can be fully qualified
     * @return
     */
    public abstract Variable lookupVariableNA(NameList identifier) throws ParseException;
    /**
     * Looks up a method or a constructor. Details can be found in the lookup
     * rules of this class docs. If the method is not found, an
     * exception is thrown.<br>
     * 
     * If a method with arguments is searched, signature parsing is
     * performed. Thus, do not look up a method with arguments before
     * parsing is done, as the signature may be not yet possible to
     * generate, what would result in a parse exception.
     * 
     * @param signature                 signature, can be fully qualified
     * @return                          method
     */
    public abstract Method lookupMethodNA(MethodSignature signature) throws ParseException;
    /**
     * Returns the top scope.
     * 
     * @return                          scope
     */
    AbstractScope getTopScope() {
        AbstractScope s = this;
        while(s.parent != null)
            s = s.parent;
        return s;
    }
    /**
     * If the most direct owner is a non--static method, it returns
     * local "this" of the method, otherwise it returns null.
     * 
     * @return                          local "this" or null
     */
    public Variable getLocalThis() {
        VariableOwner vo = getBoundingVariableOwner();
        if(vo instanceof Method) {
            Method m = (Method)vo;
            if(m.flags.context != Context.NON_STATIC)
                // static context, no this
                return null;
            else
                return m.scope.locals.get(Method.LOCAL_THIS);
        } else
            return null;
    }
    /**
     * Returns the most direct VariableOwner of this scope.<br>
     *
     * See the docs of <code>VariableOwner</code> for details about the
     * class of the returned object.
     *
     * @return                          variable owner or null if this
     *                                  scope is above JAVA_TYPE
     */
    public VariableOwner getBoundingVariableOwner() {
        if(this instanceof JavaClassScope ||
                this instanceof MethodScope)
            return (VariableOwner)owner;
        else if(this instanceof BlockScope)
            return parent.getBoundingVariableOwner();
        else
            return null;
    }
    /**
     * Returns the most direct <code>AbstractJavaClass</code> of this scope.
     * 
     * @return                          AbstractJavaClass or null if this scope
     *                                  is above JAVA_TYPE
     */
    public AbstractJavaClass getBoundingJavaClass() {
        if(this instanceof JavaClassScope)
            return (AbstractJavaClass)owner;
        else if(this instanceof AbstractLocalOwnerScope)
            return parent.getBoundingJavaClass();
        else
            return null;
    }
    @Override
    public String toString() {
        String level;
        if(this instanceof TopScope)
            level = "TOP";
        else if(this instanceof PackageScope)
            level = "PACKAGE";
        else if(this instanceof JavaClassScope)
            level = "JAVA_TYPE";
        else if(this instanceof MethodScope)
            level = "METHOD";
        else if(this instanceof BlockScope)
            level = "BLOCK";
        else
            throw new RuntimeException("unknown scope");
        return "level = " + level + " owner = " + owner.toString();
    }
}
