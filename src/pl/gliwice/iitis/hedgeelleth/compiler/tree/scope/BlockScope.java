/*
 * BlockScope.java
 *
 * Created on Nov 27, 2008
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree.scope;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.clazz.AbstractJavaClass;
import pl.gliwice.iitis.hedgeelleth.compiler.code.AbstractCodeValue;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.PrimaryExpression;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;

/**
 * A scope of the block level.<br>
 *
 * @author Artur Rataj
 */
public class BlockScope extends AbstractLocalOwnerScope {
    /**
     * Direct children scopes, key is method signature.
     */
    public Set<BlockScope> innerBlockScopes;
    /**
     * Label context or null if not defined. Empty string for an
     * unnamed label context.
     */
    protected String labelContext;
    /**
     * Label name to which a break statement jumps. Usually used only in
     * a scope of either a break label block or a loop block.
     */
    protected String breakLoopLabel;
    /**
     * Label name to which a continue statement jumps. Usually used
     * only in a loop block.
     */
    protected String continueLoopLabel;
    /**
     * If this block is synchronized this field contains the
     * reference to the lock, otherwise it is null.
     */
    public AbstractCodeValue synchronization;

    /**
     * Creates a new instance of Scope.
     *
     * @param owner                     owner of this scope, either
     *                                  block or method
     * @param parent                    parent scope, can not be null for a block scope
     * @param key                       key of this scope, to be used
     *                                  by parent
     */
    public BlockScope(Object owner, AbstractLocalOwnerScope parent, String key) {
        super(owner, parent, key);
        if(parent instanceof MethodScope)
            ((MethodScope)parent).blockScope = this;
        else if(parent instanceof BlockScope)
            ((BlockScope)parent).innerBlockScopes.add(this);
        else
            throw new RuntimeException("parent of block scope is not " +
                    "a method or block scope");
        init();
    }
    /**
     * A simple scope for languages that do not use Hedgeeleth's semantic check.
     * 
     * @param parent                    parent scope, can be null
     * @param key                       key of this scope, to be used
     *                                  by parent
     */
    public BlockScope(AbstractLocalOwnerScope parent, String key) {
        super(null, parent, key);
        init();
    }
    /**
     * Common initialization for this scope.
     */
    private void init() {
        innerBlockScopes = new HashSet<BlockScope>();
        locals = new HashMap<String, Variable>();
        breakLoopLabel = null;
        continueLoopLabel = null;
        synchronization = null;
    }
    @Override
    public  AbstractJavaClass lookupJavaClassAT(NameList identifier) throws ParseException {
        return ((AbstractAccessibilityScope)parent).lookupJavaClassAT(identifier);
    }
    @Override
    public Variable lookupVariableAT(NameList identifier,
            PrimaryExpression.ScopeMode scopeMode) throws ParseException {
        Variable v;
        if(identifier.isQualifiedOrNC() || scopeMode == PrimaryExpression.ScopeMode.STATIC ||
                (v = lookupLocalInThis(identifier)) == null)
            v = ((AbstractAccessibilityScope)parent).lookupVariableAT(identifier,
                    scopeMode);
        return v;
    }
    @Override
    public Method lookupMethodAT(MethodSignature signature,
            PrimaryExpression.ScopeMode scopeMode) throws ParseException {
        return ((AbstractAccessibilityScope)parent).lookupMethodAT(signature,
                scopeMode);
    }
    @Override
    public String newLabel() {
        return getBoundingMethodScope().newLabel();
    }
    /**
     * Sets the label context. Can be used only if the scope is yet
     * undefined. Used in the parser for example when building
     * some loops or a switch statement.<br>
     *
     * A named context can not be nested within a context of the same name,
     * otherwise no context is set and this method returns false.
     *
     * @param context                   label context of this scope,
     *                                  empty string for unnamed
     * @return                          true if the context was set,
     *                                  false if it is duplicate
     */
    public boolean setLabelContext(String context) {
        if(getLabelContext() != null)
            throw new RuntimeException("label context already set");
        if(!context.isEmpty()) {
            if(findLabelContextScope(context) != null)
                return false;
        }
        labelContext = context;
        return true;
    }
    /**
     * Returns the label context or null if undefined.
     *
     * @return                          label context of this scope,
     *                                  empty string for unnamed,
     *                                  null if undefined
     */
    public String getLabelContext() {
        return labelContext;
    }
    /**
     * Searches for the innermost scope, including this scope,
     * that has a given label context.
     *
     * @param context                   label context
     * @return                          scope, null if not found
     */
    public BlockScope findLabelContextScope(String context) {
        BlockScope s = this;
        while(true) {
            if(s.getLabelContext() != null &&
                    s.getLabelContext().equals(context))
                return s;
            if(s.parent instanceof BlockScope)
                s = (BlockScope)s.parent;
            else
                return null;
        }
    }
    /**
     * Sets the break loop label. A label context must be defined before
     * calling this method.
     *
     * @param name                      name of the label, use newLabel()
     *                                  or other method to make the name
     *                                  unique within a method
     */
    public void setBreakLoopLabel(String name) {
        if(getLabelContext() == null)
            throw new RuntimeException("label context must be defined to " +
                    "set a label");
        breakLoopLabel = name;
    }
    /**
     * Sets the break loop label. A label context must be defined before
     * calling this method.
     *
     * @param name                      name of the label, use newLabel()
     *                                  or other method to make the name
     *                                  unique within a method
     */
    public void setContinueLoopLabel(String name) {
        if(getLabelContext() == null)
            throw new RuntimeException("label context must be defined to " +
                    "set a label");
        continueLoopLabel = name;
    }
    /**
     * Searches for a break label.
     *
     * @param context                   name of the label context or null if
     *                                  the innermost break label should be
     *                                  found
     * @return                          label name or null if not found
     */
    public String getBreakLoopLabel(String context) {
        if(context == null) {
            // no context defined, find the innermost break label,
            // if any
            if(breakLoopLabel != null)
                return breakLoopLabel;
            else if(parent instanceof BlockScope)
                return ((BlockScope)parent).getBreakLoopLabel(null);
            else
                return null;
        } else {
            // context defined, find the break label, if any,
            // at that exact context
            BlockScope s = findLabelContextScope(context);
            if(s == null)
                return null;
            else
                return s.getBreakLoopLabel(null);
        }
    }
    /**
     * Searches for a continue label.
     *
     * @param context                   name of the label context or null if
     *                                  the innermost label label should be
     *                                  found
     * @return                          label name or null if not found
     */
    public String getContinueLoopLabel(String context) {
        if(context == null) {
            // no context defined, find the innermost break label,
            // if any
            if(continueLoopLabel != null)
                return continueLoopLabel;
            else if(parent instanceof BlockScope)
                return ((BlockScope)parent).getContinueLoopLabel(null);
            else
                return null;
        } else {
            // context defined, find the break label, if any,
            // at that exact context
            BlockScope s = findLabelContextScope(context);
            if(s == null)
                return null;
            else
                return s.getContinueLoopLabel(null);
        }
    }
    /**
     * Returns number of synchronized blocks, from the outermost
     * to this one. For a non--synchronized code the method
     * returns 0.
     *
     * @return                          number of nested synchronization
     *                                  blocks
     */
    public int getSynchronizationCount() {
        int locksNum;
        if(parent instanceof BlockScope)
            locksNum = ((BlockScope)parent).getSynchronizationCount();
        else
            locksNum = 0;
        if(synchronization != null)
            ++locksNum;
        return locksNum;
    }
    /*
     * Returns references to locks of subsequent synchronized blocks,
     * since the outermost. For a non--synchronized code the method
     * returns an empty list.
     *
     * @return                          list of synchronization locks
     */
    /*
    public List<CodeFieldDereference> getSynchronization() {
        List<CodeFieldDereference> locks =
                new ArrayList<CodeFieldDereference>();
        if(parent instanceof BlockScope)
            locks = ((BlockScope)parent).getSynchronization();
        if(synchronization != null)
            locks.add(synchronization);
        return locks;
    }
     */
}
