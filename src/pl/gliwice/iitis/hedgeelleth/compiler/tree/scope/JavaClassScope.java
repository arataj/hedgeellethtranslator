/*
 * JavaClassScope.java
 *
 * Created on Nov 27, 2008
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree.scope;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.clazz.AbstractJavaClass;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.flags.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.PrimaryExpression;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;

/**
 * A scope of the java class level.
 *
 * This scope is between code and classes, and thus any field/method
 * lookup initiated at the code level must go through this. So,
 * all accessibility testing has been built--in into this scope.<br>
 *
 * There are versions of the lookup methods that end with "NA"
 * that do not have the accessibility testing, and methods that
 * end with "AT" that have the accessibility testing, the latter
 * only in scopes that implement <code>AccessibilityScope</code>,
 * namely all scopes but <code>TopScope</code> and
 * <code>PackageScope</code>.
 *
 * @author Artur Rataj
 */
public class JavaClassScope extends AbstractAccessibilityScope {
    /**
     * Children scopes, key is method local signature.
     */
    public Map<MethodSignature, MethodScope> methodScopes;
    /**
     * Class imports of this java class.
     */
    public List<NameList> classImports;
    /**
     * Static imports of this java class.
     */
    public List<NameList> staticImports;
    /**
     * Fields within this java class, key is unqualified field name.
     */
    public Map<String, Variable> fields;
    /**
     * List of unqualified field names in the source code order. Used
     * to create <code>CodeClass.listOfFields</code>. Kept in sync
     * with updates to <code>fields</code>. Should no longer be used,
     * though, after <code>CodeClass.listOfFields</code> is built.
     */
    public List<String> listOfFields;
    /**
     * Methods, keyed with local signatures.
     */
    public Map<MethodSignature, Method> methods;
    /**
     * List of local method signatures in the source code order. Used
     * to create <code>CodeClass.listOfMethods</code>. Kept in sync
     * with updates to <code>methods</code>. Should no longer be used,
     * though, after <code>CodeClass.listOfMethods</code> is built.
     */
    public List<MethodSignature> listOfMethods;

    /**
     * Creates a new instance of Scope.
     *
     * @param owner                     owner of this scope -- a java class
     * @param parent                    parent scope, or null if none
     * @param key                       key of this scope, to be used
     *                                  by parent
     */
    public JavaClassScope(AbstractJavaClass owner, PackageScope parent, String key) {
        super(owner, parent, key);
        parent.javaClassScopes.put(key, this);
        methodScopes = new HashMap<>();
        // this field is set in constructor of Java type
        classImports = null;
        staticImports = null;
        fields = new HashMap<>();
        listOfFields = new LinkedList<>();
        methods = new HashMap<>();
        listOfMethods = new LinkedList<>();
    }
    /**
     * Access classes of looked up objects.
     */
    enum AccessClass {
        /**
         * Belongs to the same class.
         */
        THIS,
        /**
         * Belongs to one of the superclasses, but outside the package.
         */
        SUPER,
        /**
         * Belongs to the same package, but not to a superclass.
         */
        PACKAGE,
        /**
         * Belongs to one of the superclasses, and also to the same package.
         */
        SUPER_PACKAGE,
        /**
         * Outside of the package, and also does not belong to a superclass.
         */
        GLOBAL,
        /**
         * Outside the whole application, so that it can only use the public API
         * of the application. Currently not used.
         */
        OUTSIDE_APPLICATION;

        /**
         * Determines the access class of a given object that has been
         * looked up.
         *
         * @param              variable owner of this scope
         * @param thatOwner             variable owner of the scope
         *                              of the tested looked--up object
         * @return                      an access class
         */
        public static AccessClass get(JavaClassScope scope, AbstractJavaClass thatOwner)
                throws ParseException {
            AbstractJavaClass thisOwner = (AbstractJavaClass)scope.owner;
            AccessClass ac;
            if(thisOwner == thatOwner)
                ac = AccessClass.THIS;
            else  {
                Object thisPackage = ((PackageScope)thisOwner.scope.parent).owner;
                Object thatPackage = ((PackageScope)thatOwner.scope.parent).owner;
                boolean samePackage = thisPackage.equals(thatPackage);
                if(thisOwner.isEqualToOrExtending(scope, thatOwner)) {
                    if(samePackage)
                        ac = AccessClass.SUPER_PACKAGE;
                    else
                        ac = AccessClass.SUPER;
                } else if(samePackage)
                    ac = AccessClass.PACKAGE;
                else
                    ac = AccessClass.GLOBAL;
            }
            return ac;
        }
    }
    /**
     * Checks if an object can be accessed from this scope. If not,
     * throws a parse error. The parse error does not have its stream
     * position set.
     *
     * @param thatOwner                 owner of the object whose accessibility
     *                                  is checked
     * @param am                        access modifier of the object whose
     *                                  accessibility is checked
     * @param accessedName              descriptive name, for error message,
     *                                  or null if to construct the error message
     *                                  to reflect that the accessibility of the owner
     *                                  itself is checked
     */
    public void testAccessibility(AbstractJavaClass thatOwner, AccessModifier am,
            String accessedName) throws ParseException {
        AccessClass ac = AccessClass.get(this, thatOwner);
        boolean error;
        switch(ac) {
            case THIS: {
                error = false;
                break;
            }
            case SUPER: {
                switch(am) {
                    case PROTECTED:
                    case PROTECTED_INTERNAL:
                    case PUBLIC:
                    case PUBLIC_API:
                        error = false;
                        break;

                    case PRIVATE:
                    case INTERNAL:
                        error = true;
                        break;

                    default:
                        throw new RuntimeException("unknown access modifier");
                }
                break;
            }
            case PACKAGE: {
                switch(am) {
                    case PUBLIC:
                    case PUBLIC_API:
                    case INTERNAL:
                    case PROTECTED_INTERNAL:
                        error = false;
                        break;

                    case PRIVATE:
                    case PROTECTED:
                        error = true;
                        break;

                    default:
                        throw new RuntimeException("unknown access modifier");
                }
                break;
            }
            case SUPER_PACKAGE: {
                switch(am) {
                    case PROTECTED:
                    case PUBLIC:
                    case PUBLIC_API:
                    case INTERNAL:
                    case PROTECTED_INTERNAL:
                        error = false;
                        break;

                    case PRIVATE:
                        error = true;
                        break;

                    default:
                        throw new RuntimeException("unknown access modifier");
                }
                break;
            }
            case GLOBAL: {
                switch(am) {
                    case PUBLIC:
                    case PUBLIC_API:
                        error = false;
                        break;

                    case PRIVATE:
                    case PROTECTED:
                    case INTERNAL:
                    case PROTECTED_INTERNAL:
                        error = true;
                        break;

                    default:
                        throw new RuntimeException("unknown access modifier");
                }
                break;
            }
            case OUTSIDE_APPLICATION: {
                switch(am) {
                    case PUBLIC_API:
                        error = false;
                        break;

                    case PUBLIC:
                    case PRIVATE:
                    case PROTECTED:
                    case INTERNAL:
                    case PROTECTED_INTERNAL:
                        error = true;
                        break;

                    default:
                        throw new RuntimeException("unknown access modifier");
                }
                break;
            }
            default:
                throw new RuntimeException("unknown access class");
        }
        if(error) {
            String s;
            if(accessedName == null)
                accessedName = thatOwner.getQualifiedName().toString() +
                         " in " + thatOwner.getStreamPos();
            s = accessedName + " has ${" + am.toString() + "} access";
            throw new ParseException(null, ParseException.Code.ILLEGAL,
                   s);
        }
    }
    /**
        It just tests the origin
        * of the looked--up object, and classifies it as THIS, SUPER, PACKAGE
        * or GLOBAL. Then, matches the class
        void
     */
    /**
     * Combines an import prefix with a java class name. If the
     * prefix does not end with an asterisk, the ending must then match
     * the first segment of the qualified name or the combination
     * is impossible.
     *
     * @param prefix                    import prefix, the last
     *                                  segment can be an asterisk
     * @param identifier                java class name or, if a static import, a method
     *                                  name
     * @return                          combined name, or null if impossible
     * 
     * !!!! TODO: access privileges may prevent conflicts
     */
    protected static NameList combineImport(NameList prefix, String identifier) {
        NameList combined = null;
        if(prefix.last().equals("*") ||
                prefix.last().equals(identifier)) {
            combined = new NameList(prefix);
            // replace the matching last segment with the identifier
            combined.list.remove(combined.list.size() - 1);
            combined.list.add(identifier);
        }
        return combined;
    }
    /**
     * Tries to combine a class name with all imports of this scope. If more than
     * one combination is found, a parse exception is thrown.<br>
     *
     * If a combination is found, the resulting qualified name is searched
     * using <code>lookupJavaClassNA()</code>.
     *
     * @param identifier                java class name
     * @return                          class or null if not found
     */
    protected AbstractJavaClass combineClassImports(String identifier) throws ParseException {
        AbstractJavaClass out = null;
        int count = 0;
        for(NameList prefix : classImports) {
            NameList combined = combineImport(prefix, identifier);
            // can be combined with the import, check if such an object exists
            if(combined != null) {
                // go to the top scope to avoid finding of unqualified local classes
                AbstractJavaClass j;
                try {
                    j = parent.parent.lookupJavaClassNA(combined);
                } catch(ParseException e) {
                    j = null;
                }
                if(j != null) {
                    ++count;
                    if(count > 1)
                        throw new ParseException(null, ParseException.Code.AMBIGUOUS,
                                "ambiguous name, can be " + out.getQualifiedName() +
                                " or " + j.getQualifiedName());
                    out = j;
                }
            }
        }
        return out;
    }
    /**
     * Tries to combine a field name with all static imports of this scope. If more than
     * one combination is found, a parse exception is thrown.<br>
     *
     * If a combination is found, the resulting qualified name is searched
     * using <code>findClosestMethod()</code>.
     *
     * @param s                         method's signature
     * @return                          method or null if not found
     */
    protected Variable combineFieldImports(NameList identifier) throws ParseException {
        Variable out = null;
        Variable prev = null;
        // qualified names are not searched for in static imports
        if(identifier.isQualifiedOrNC())
            throw new RuntimeException(
                "qualified field not allowed to be searched for in a static import: " + 
                    identifier.toString());
        else {
            String name = identifier.last();
            for(NameList prefix : staticImports) {
                NameList combined = combineImport(prefix, name);
                // can be combined with the import, check if such an object exists
                // go to the top scope to search for the package
                if(combined != null) {
                    NameList clazz = new NameList(combined);
                    clazz.list.remove(clazz.list.size() - 1);
                    // go to the top scope to avoid finding of unqualified local classes
                    AbstractJavaClass j = parent.parent.lookupJavaClassNA(clazz);
                    if(j != null) {
                        Variable v;
                        try {
                            v= j.scope.lookupVariableAT(
                                // search fields of <code>j</code> only
                                new NameList(name), PrimaryExpression.ScopeMode.OMIT_STATIC);
                        } catch(ParseException e) {
                            v = null;
                        }
                        if(v != null) {
                            if(v.flags.isPublic() && ((FieldFlags)v.flags).context != Context.NON_STATIC) {
                                out = v;
                                if(prev != null)
                                    throw new ParseException(null, ParseException.Code.AMBIGUOUS,
                                            "ambiguous field, found in " +
                                            ((AbstractJavaClass)prev.owner).getQualifiedName() +
                                            " and " + ((AbstractJavaClass)out.owner).getQualifiedName());

                                prev = out;
                            }
                        }
                    }
                }
            }
        }
        if(out == null)
            throw new ParseException(null, ParseException.Code.LOOK_UP,
                "${variable} not found: " + identifier.toString());
        return out;
    }
    /**
     * Tries to combine a method name with all static imports of this scope. If none or
     * more than one combination is found, a parse exception is thrown.<br>
     *
     * If a combination is found, the resulting qualified name is searched
     * using <code>findClosestMethod()</code>.
     *
     * @param s                         method's signature
     * @return                          method
     */
    protected Method combineMethodImports(MethodSignature s) throws ParseException {
        Method out = null;
        Method prev = null;
        String name = s.extractUnqualifiedNameString();
        // qualified names are not searched for in static imports
        if(!name.equals(s.extractQualifiedNameString()))
            throw new RuntimeException(
                "qualified method not allowed to be searched for in a static import: " +
                    s.getDeclarationDescription());
        else
            for(NameList prefix : staticImports) {
                NameList combined = combineImport(prefix, name);
                // can be combined with the import, check if such an object exists
                // go to the top scope to search for the package
                if(combined != null) {
                    NameList clazz = new NameList(combined);
                    clazz.list.remove(clazz.list.size() - 1);
                    // go to the top scope to avoid finding of unqualified local classes
                    AbstractJavaClass j = parent.parent.lookupJavaClassNA(clazz);
                    if(j != null) {
                        Method m;
                        try {
                            m = j.scope.findClosestMethod(s, true);
                        } catch(ParseException e) {
                            m = null;
                        }
                        if(m != null && m.flags.am == AccessModifier.PUBLIC) {
                            out = m;
                            if(prev != null)
                                throw new ParseException(null, ParseException.Code.AMBIGUOUS,
                                        "ambiguous call, found in " + prev.owner.getQualifiedName() +
                                        " and " + out.owner.getQualifiedName());

                            prev = out;
                        }
                    }
                }
            }
        if(out == null)
            throw new ParseException(null, ParseException.Code.LOOK_UP,
                "method not found: " + s.getDeclarationDescription());
        return out;
    }
    @Override
    public  AbstractJavaClass lookupJavaClassNA(NameList identifier) throws ParseException {
        AbstractJavaClass out;
        if(identifier == null)
            throw new ParseException(null, ParseException.Code.LOOK_UP,
                        "dereferenced null");
        if(identifier.isQualifiedOrNC())
            out = parent.lookupJavaClassNA(identifier);
        else {
            // search the current package
            AbstractJavaClass j = parent.lookupJavaClassNA(identifier);
            if(j == null) {
                // if not found, search the imported packages
                j = combineClassImports(identifier.last());
                if(j == null)
                    throw new ParseException(null, ParseException.Code.LOOK_UP,
                                "${variable} or class " + identifier.toString() + " not found");
            }
            out = j;
        }
        return out;
    }
    @Override
    public  AbstractJavaClass lookupJavaClassAT(NameList identifier) throws ParseException {
        AbstractJavaClass out = lookupJavaClassNA(identifier);
if(out == null)
    out = lookupJavaClassNA(identifier);
        testAccessibility(out, out.flags.am, null);
        return out;
    }
    /**
     * Finds a field variable. Searches also in super classes.
     *
     * @param name                      variable to be found
     * @return                          variable found or null if
     *                                  the variable has not been
     *                                  found
     */
    private Variable findFieldVariable(String name) throws ParseException {
        Variable v = null;
        AbstractJavaClass j = (AbstractJavaClass)owner;
        while(j != null) {
            Map<String, Variable> currFields = j.scope.fields;
            v = currFields.get(name);
            if(v != null) {
                /*
                if(protectedAccess)
                    checkProtectedAccess(v.flags.am, "field " + name, j);
                 */
                break;
            }
            j = j.scope.getExtendsJavaClass();
        }
        return v;
    }
    @Override
    public Variable lookupVariableNA(NameList identifier) throws ParseException {
        Variable v;
        if(identifier.isQualifiedOrNC() || (v = findFieldVariable(
                identifier.toString())) == null)
            v = parent.lookupVariableNA(identifier);
        return v;
    }
    @Override
    public Variable lookupVariableAT(NameList identifier,
            PrimaryExpression.ScopeMode scopeMode) throws ParseException {
        Variable out;
        switch(scopeMode) {
            case ALL:
                try {
                    out = lookupVariableNA(identifier);
                } catch(ParseException e) {
                    if(!identifier.isQualifiedOrNC())
                        out = combineFieldImports(identifier);
                    else
                        throw e;
                }
                break;
                
            case STATIC:
                out = combineFieldImports(identifier);
                break;
                
            case OMIT_STATIC:
                out = lookupVariableNA(identifier);
                break;

            default:
                throw new RuntimeException("unknown call mode");
        }
        AbstractJavaClass o = (AbstractJavaClass)out.owner;
        try {
            testAccessibility(o, o.flags.am, null);
        } catch(ParseException e) {
            e.prefixMessages("no access to field " + identifier.toString() + ": ");
            throw e;
        }
        if(out.isLocal())
            throw new RuntimeException("local looked up is java class scope");
        else
            testAccessibility(o, ((FieldFlags)out.flags).am, "field " + identifier.toString() +
                    // static field
                    (identifier.isQualifiedOrNC() ? "" : " of " + o.getQualifiedName()) +
                    " in " + o.getStreamPos());
        return out;
    }
    @Override
    public Method lookupMethodNA(MethodSignature signature) throws ParseException {
        Method m;
        if(!signature.extractClassString().isEmpty())
            m = parent.lookupMethodNA(signature);
        else
            m = findClosestMethod(signature, false);
        return m;
    }
    @Override
    public Method lookupMethodAT(MethodSignature signature,
            PrimaryExpression.ScopeMode scopeMode) throws ParseException {
        Method out;
        switch(scopeMode) {
            case ALL:
                try {
                    out = lookupMethodNA(signature);
                } catch(ParseException e) {
                    if(signature.isLocal())
                        out = combineMethodImports(signature);
                    else
                        throw e;
                }
                break;
                
            case STATIC:
                if(signature.isLocal())
                    out = combineMethodImports(signature);
                else
                    out = lookupMethodNA(signature);
                break;
                
            case OMIT_STATIC:
                out = lookupMethodNA(signature);
                break;

            default:
                throw new RuntimeException("unknown call mode");
        }
        String s;
        // if(out == null)
        //     out = out;
        if(out instanceof Constructor)
            s = "constructor";
        else
            s = "method";
        AbstractJavaClass o = out.owner;
        try {
            testAccessibility(o, o.flags.am, null);
        } catch(ParseException e) {
            e.prefixMessages("no access to " + s + " " +
                    signature.getDeclarationDescription() + ": ");
            throw e;
        }
        testAccessibility(o, out.flags.am, s + " " +
                signature.getDeclarationDescription() +
                " of " + o.getQualifiedName() + " in " + o.getStreamPos());
        return out;
    }
    /**
     * Returns the type extended by the java class that owns this scope,
     * or null if the owner of this scope is the special class object.<br>
     *
     * Returns the reference to the original object.
     *
     *
     * @return                          type or null
     */
    public Type getExtends() {
        return ((AbstractJavaClass)owner).extendS;
    }
    /**
     * It works like <code>getExtends()</code>, but returns
     * <code>AbstractJavaClass</code> instead of <code>Type</code>.<br>
     *
     * As it looks up a java class, a parse exception is thrown
     * if that java class is not found.
     *
     * @return                          java class or null
     */
    private AbstractJavaClass getExtendsJavaClass() throws ParseException {
        Type t = getExtends();
        if(t == null)
            return null;
        else
            return lookupJavaClassNA(t.getJava());
    }
    /*
     * Looks up a method.
     *
     * @param key                       signature
     * @return                          method scope, or null if not found
     */
    /*
    public MethodScope lookupMethodScope(MethodSignature key) {
        return methodScopes.get(key);
    }
     */
    /**
     * Adds a field. Also appends to <code>listOfFields</code>, so should
     * not be used for any operations but adding a field freshly read from
     * the source code.
     *
     * @param variable                  field to add
     */
    public  void addField(Variable variable) throws ParseException {
        if(fields.get(variable.name) != null)
            throw new ParseException(variable.getStreamPos(),
                    ParseException.Code.DUPLICATE,
                    "duplicate declaration of " + variable.getSourceName());
        fields.put(variable.name, variable);
        listOfFields.add(variable.name);
    }
    /**
     * Adds a method. Also appends to <code>listOfMethods</code>, so should
     * not be used for any operations but adding a method freshly read from
     * the source code.
     *
     * @param method                    method to add
     */
    public  void addMethod(Method method) throws ParseException {
        MethodSignature s = method.signature.newLocalSignature();
        // do not look up the method via normal functions
        // as they need semantic check to find proper
        // signatures
        if(methods.get(s) != null)
            throw new ParseException(method.getStreamPos(),
                    ParseException.Code.DUPLICATE,
                    "duplicate declaration of " + method.
                    getDeclarationDescription(true));
        else {
            methods.put(s, method);
            listOfMethods.add(s);
        }
    }
    /**
     * Find the most close method class. If the call is ambiguous or
     * the method is not found, a parse exception is thrown.<br>
     *
     * Takes into account the extended classes.<br>
     *
     * Parses signatures if not done yet.<br>
     *
     * Does not test accessibility, as it is an internal method that can be
     * called from other scopes.
     *
     * @param methodSignature           method signature in the call
     * @param staticOnly                limits the look--up to static methods
     *                                  only; used for static imports
     * @return                          closest method
     */
    Method findClosestMethod(MethodSignature methodSignature,
            boolean staticOnly) throws ParseException {
        ((AbstractJavaClass)owner).parseSignatures();
        methodSignature = methodSignature.newLocalSignature();
        if(((AbstractJavaClass)owner).frontend != methodSignature.frontend) {
            methodSignature = methodSignature.newTranslationToOtherFrontend(
                    ((AbstractJavaClass)owner).frontend);
        }
        Method m;
        m = methods.get(methodSignature);
/*if(methodSignature.toString().indexOf("println/G") != -1)
    m = m;*/
        AbstractJavaClass j = (AbstractJavaClass)owner;
        while(m == null) {
            j = j.scope.getExtendsJavaClass();
            if(j == null)
                break;
            m = j.scope.methods.get(methodSignature);
            /*
            if(m != null)
                checkProtectedAccess(m.flags.am, "method " +
                        methodSignature.getDeclarationDescription(), j);
             */
        }
        if(m == null) {
            Map<MethodSignature, Method> list = new HashMap<>();
            j = (AbstractJavaClass)owner;
            while(j != null) {
                Map<MethodSignature, Method> currMethods = j.scope.methods;
                String name = methodSignature.extractQualifiedNameString();
                for(MethodSignature signature : currMethods.keySet()) {
                    String s = signature.extractQualifiedNameString();
                    if(s.equals(name)) {
                        // System.out.println(identifier + " " + signature);
                        Method matching = currMethods.get(signature);
                        if((!staticOnly || matching.flags.context != Context.NON_STATIC) &&
                                matching.matchesCall(methodSignature)) {
                            /*
                            if(m != null)
                                throw new ParseException("ambiguous call, matches both " +
                                        m.getDeclarationDescription() + " and " +
                                        matching.getDeclarationDescription());
                            m = matching;
                             */
                            // identical--signature methods that are more
                            // direct extension--wise have greater
                            // priority
                            if(!list.containsKey(signature))
                                list.put(signature, matching);
                        }
                    }
                }
                j = j.scope.getExtendsJavaClass();
            }
            // then reduce by replaceability
            Set<Method> reduce = new HashSet<Method>();
            for(Method m1 : list.values())
                for(Method m2 : list.values()) {
                    if(m1 != m2 &&
                            m1.matchesCall(m2.signature)) {
                        if(m2.matchesCall(m1.signature))
                            throw new RuntimeException("can not determine invocation: " +
                                    "two--side replaceability " +
                                    "between " +m1.getDeclarationDescription(true) + " and " +
                                    m2.getDeclarationDescription(true));
                        reduce.add(m1);
                    }
                }
            for(Method r : reduce)
                list.remove(r.signature);
            Iterator<Method> i = list.values().iterator();
            if(list.size() > 1)
                throw new ParseException(null, ParseException.Code.AMBIGUOUS,
                        "ambiguous call, matches both " +
                        i.next().getDeclarationDescription(true) + " and " +
                        i.next().getDeclarationDescription(true));
            else if(list.size() == 1) {
                m = i.next();
                /*
                if(m.owner != (AbstractJavaClass)owner)
                    checkProtectedAccess(m.flags.am, "method " +
                            methodSignature.getDeclarationDescription(),
                            m.owner);
                 */
            } else
                throw new ParseException(null, ParseException.Code.LOOK_UP,
                        "not found: " +
                        methodSignature.getDeclarationDescription());
        }
        return m;
    }
}
