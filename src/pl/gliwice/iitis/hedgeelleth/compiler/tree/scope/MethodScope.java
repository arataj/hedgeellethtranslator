/*
 * MethodScope.java
 *
 * Created on Nov 27, 2008
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree.scope;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.clazz.AbstractJavaClass;
import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.PrimaryExpression;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;

/**
 * A scope of the method, that hold the method's arguments.
 *
 * @author Artur Rataj
 */
public class MethodScope extends AbstractLocalOwnerScope {
    /**
     * Child scope. It is the outermost block scope of this method.
     */
    public BlockScope blockScope;
    /**
     * Next unique label identifier. After <code>CodeMethod</code>
     * is created, should no longer be used.
     */
    public int uniqueLabelNum;

    /**
     * Creates a new instance of Scope.
     *
     * @param owner                     owner of this scope -- a method
     * @param parent                    parent scope, can be null only for fake
     *                                  methods, created outside the compilation
     * @param key                       key of this scope, to be used
     *                                  by parent, it must be a method's
     *                                  local signature; if owner is not null, a fake method
     *                                  must have it null
     */
    public MethodScope(VariableOwner owner, JavaClassScope parent, String key) {
        super(owner, parent, key);
        MethodSignature signature;
        if(((Method)owner).owner == null || key == null)
            // a fake method
            signature = new MethodSignature(null, "fake/");
        else
            signature = new MethodSignature(((Method)owner).owner.frontend,
                key);
        if(!signature.isLocal())
            throw new RuntimeException("signature must be local");
        if(parent != null && key != null)
            parent.methodScopes.put(signature, this);
        blockScope = null;
        locals = new HashMap<String, Variable>();
        uniqueLabelNum = 0;
    }
    @Override
    public  AbstractJavaClass lookupJavaClassAT(NameList identifier) throws ParseException {
        return ((AbstractAccessibilityScope)parent).lookupJavaClassAT(identifier);
    }
    @Override
    public Variable lookupVariableAT(NameList identifier,
            PrimaryExpression.ScopeMode scopeMode) throws ParseException {
        Variable v;
        if(identifier.isQualifiedOrNC() || scopeMode == PrimaryExpression.ScopeMode.STATIC ||
                (v = lookupLocalInThis(identifier)) == null)
            v = ((AbstractAccessibilityScope)parent).lookupVariableAT(identifier,
                    scopeMode);
        return v;
    }
    @Override
    public Method lookupMethodAT(MethodSignature signature,
            PrimaryExpression.ScopeMode callScope) throws ParseException {
        return ((AbstractAccessibilityScope)parent).lookupMethodAT(signature,
                callScope);
    }
    @Override
    public String newLabel() {
        if(uniqueLabelNum == -1)
            throw new RuntimeException("should no longer be used");
        return "*" + (uniqueLabelNum++);
    }
}
