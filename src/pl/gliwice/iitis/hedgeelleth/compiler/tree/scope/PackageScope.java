/*
 * PackageScope.java
 *
 * Created on Nov 27, 2008
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree.scope;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.clazz.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;

/**
 * A scope of the package level.
 *
 * Packages scopes temporarily have a java class's list of
 * imports when determining the extended or implemented types,
 * see the description in SemanticCheck.visit(Compilation)
 * for details.<br> !!! NO
 *
 * @author Artur Rataj
 */
public class PackageScope extends AbstractScope {
    /**
     * Children scopes, key is java class name.
     */
    public Map<String, JavaClassScope> javaClassScopes;
    /**
     * Java types, keyed with names.
     */
    public Map<String, AbstractJavaClass> javaClasses;

    /**
     * Creates a new instance of Scope.
     *
     * @param parent                    parent scope, or null if none
     * @param key                       key of this scope, to be used
     *                                  by parent
     */
    public PackageScope(TopScope parent, String key) {
        super(null, parent, key);
        owner = key;
        parent.packageScopes.put(key, this);
        javaClassScopes = new HashMap<>();
        javaClasses = new HashMap<>();
    }
    /**
     * Search for a java class in the current package, including an inner
     * java class.
     * 
     * @param identifier                identifier of a class, a method
     *                                  or a variable, assumed to be not
     *                                  prefixed with package name
     * @param ignoreLast                true if not identifier of a class,
     *                                  to ignore field name or method
     *                                  name
     * @return                          a java class or null if not found
     */
    protected AbstractJavaClass findJavaClass(NameList identifier, boolean ignoreLast) {
        AbstractJavaClass out = null;
        // search also for nested classes
        String s = "";
        for(int segments = 0; segments < identifier.list.size(); ++segments) {
            if(ignoreLast &&
                    segments == identifier.list.size() - 1)
                break;
            if(!s.isEmpty())
                s = "$" + s;
            s = identifier.list.get(segments) + s;
        }
        out = javaClasses.get(s);
        return out;
    }
    @Override
    public  AbstractJavaClass lookupJavaClassNA(NameList identifier) throws ParseException {
        // search this package
        AbstractJavaClass j = findJavaClass(identifier, false);
        if(j == null) {
            if(identifier.isQualifiedOrNC())
                // if not found and qualified, ask the parent
                j = parent.lookupJavaClassNA(identifier);
        } else {
            // check for clash
            AbstractJavaClass k;
            try {
                if(identifier.isQualifiedOrNC())
                    k = parent.lookupJavaClassNA(identifier);
                else
                    k = null;
            } catch(ParseException e) {
                k = null;
            }
            if(k != null)
                throw new ParseException(null, ParseException.Code.ILLEGAL,
                        "package/inner class name clash: " + identifier);
        }
        return j;
    }
    @Override
    public Variable lookupVariableNA(NameList identifier) throws ParseException {
        Variable v;
        AbstractJavaClass j = findJavaClass(identifier, true);
        if(j == null)
            v = parent.lookupVariableNA(identifier);
        else
            if(j instanceof ImplementingClass) {
                ImplementingClass c = (ImplementingClass)j;
                v = c.scope.fields.get(identifier.last());
                if(v == null)
                    throw new ParseException(null, ParseException.Code.LOOK_UP,
                            "class " + j.getQualifiedName() +
                            " does not have a field " + identifier.last());
            } else
                throw new ParseException(null, ParseException.Code.LOOK_UP,
                        "interfaces do not support fields");
        return v;
    }
    @Override
    public Method lookupMethodNA(MethodSignature signature) throws ParseException {
        Method m;
        AbstractJavaClass j = findJavaClass(new NameList(
                signature.extractClassString()), false);
        if(j == null)
            m = parent.lookupMethodNA(signature);
        else {
            m = j.scope.findClosestMethod(signature, false);
            if(m == null) {
                String s;
                if(j instanceof ImplementingClass)
                    s = "class";
                else
                    s = "interface";
                throw new ParseException(null, ParseException.Code.LOOK_UP,
                        s + " " + j.getQualifiedName() +
                        " does not have a method " +
                        signature.getDeclarationDescription());
            }
        }
        return m;
    }
    /**
     * Adds a Java type.
     *
     * @param identifier                identifier
     */
    public  void addJavaClass(AbstractJavaClass type) throws ParseException {
        if(javaClasses.get(type.name) != null)
            throw new ParseException(type.getStreamPos(), ParseException.Code.DUPLICATE,
                    "duplicate declaration of " + type.name);
        else
            javaClasses.put(type.name, type);
    }
    /**
     * Returns the name of the scope's package.<br>
     * 
     * This method belongs to this object as as packages theirselves
     * do not exist as objects, so they can not return their names.
     * 
     * @return
     */
    public String getPackageName() {
        return (String)owner;
    }
}
