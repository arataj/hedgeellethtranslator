/*
 * TopScope.java
 *
 * Created on Nov 27, 2008
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree.scope;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.clazz.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;

/**
 * A scope of the top level.
 *
 * @author Artur Rataj
 */
public class TopScope extends AbstractScope {
    /**
     * Children scopes, key is package name.
     */
    public Map<String, PackageScope> packageScopes;

    /**
     * Creates a new instance of TopScope.
     */
    public TopScope() {
        super(null, null, null);
        packageScopes = new HashMap<>();
    }
    @Override
    public  AbstractJavaClass lookupJavaClassNA(NameList identifier) throws ParseException {
        AbstractJavaClass j;
        if(identifier.isQualifiedOrNC()) {
            j = findJavaClass(identifier, false);
            if(j == null)
                throw new ParseException(null, ParseException.Code.UNKNOWN,
                        "class " + identifier.toString() + " not found");
        } else
            throw new RuntimeException("unqualified java class name at top scope");
        return j;
    }
    @Override
    public Variable lookupVariableNA(NameList identifier) throws ParseException {
        Variable v = null;
        AbstractJavaClass j = findJavaClass(identifier, true);
        if(j == null)
// {if(identifier.toString().indexOf("start") != -1)
//     v = v;
            throw new ParseException(null, ParseException.Code.LOOK_UP,
                    "${variable} not found: " + identifier.toString());
// }
        else
            if(j instanceof ImplementingClass) {
                ImplementingClass c = (ImplementingClass)j;
                v = c.scope.lookupVariableNA(new NameList(identifier.last()));
                if(v == null)
                    throw new ParseException(null, ParseException.Code.LOOK_UP,
                            "class " + j.getQualifiedName() +
                            " does not have a field " + identifier.last());
            } else
                throw new ParseException(null, ParseException.Code.LOOK_UP,
                        "interfaces do not support fields");
        return v;
    }
    @Override
    public Method lookupMethodNA(MethodSignature signature) throws ParseException {
        Method m;
/*if(signature.toString().equals("hc.Mem.del/Ljava.lang.Array<Llinearalgebra.Point>"))
    signature = signature;*/
        NameList javaClassName = new NameList(signature.
                extractClassString());
        AbstractJavaClass j = findJavaClass(javaClassName, false);
        if(j == null)
            throw new ParseException(null, ParseException.Code.LOOK_UP,
                    "method not found: " + signature.getDeclarationDescription());
        else {
            String s;
            m = j.scope.findClosestMethod(signature, false);
            if(m == null) {
                if(j instanceof ImplementingClass)
                    s = "class";
                else
                    s = "interface";
                throw new ParseException(null, ParseException.Code.LOOK_UP,
                        s + " " + j.getQualifiedName() +
                        " does not have a method " + signature.
                        getDeclarationDescription());
            }
        }
        return m;
    }
    /**
     * Search for a java class in all packages, including inner
     * java classes.
     *
     * @param identifier                identifier of a class, a method
     *                                  or a variable
     * @param ignoreLast                true if not identifier of a class,
     *                                  to ignore field name or method
     *                                  name
     * @return null if not found
     */
    protected AbstractJavaClass findJavaClass(NameList identifier, boolean ignoreLast)
            throws ParseException {
        AbstractJavaClass out = null;
        int count = 0;
        for(String packageKey : packageScopes.keySet()) {
            NameList prefix = new NameList(packageKey);
            if(identifier.list.size() > prefix.list.size()) {
                int pos;
                for(pos = 0; pos < prefix.list.size(); ++pos)
                    if(!prefix.list.get(pos).equals(identifier.list.get(pos)))
                        break;
                if(pos == prefix.list.size()) {
                    PackageScope packagE = packageScopes.get(packageKey);
                    NameList suffix = new NameList(identifier);
                    for(int i = 0; i < prefix.list.size(); ++i)
                        suffix.list.remove(0);
                    AbstractJavaClass j = packagE.findJavaClass(suffix, ignoreLast);
                    if(j != null) {
                        if(count != 0)
                            throw new ParseException(null, ParseException.Code.AMBIGUOUS,
                                    "ambiguous type, can be either " +
                                    out.getQualifiedName() + " or " +
                                    j.getQualifiedName());
                        out = j;
                        ++count;
                    }
                }
            }
        }
//if(identifier.toString().equals("g"))
//    identifier = identifier;
//        if(out == null)
//            throw new ParseException(null, ParseException.Code.UNKNOWN,
//                    "class " + identifier.toString() + " not found");
        return out;
    }
}
