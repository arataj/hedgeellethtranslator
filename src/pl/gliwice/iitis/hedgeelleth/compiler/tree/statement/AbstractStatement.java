/*
 * Statement.java
 *
 * Created on Dec 13, 2007, 4:01:27 PM
 *
 * Copyright (c) 2007 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree.statement;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.CommentTag;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.scope.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * A statement in a block of code.
 * 
 * @author Artur Rataj
 */
public abstract class AbstractStatement implements Node<Object>, SourcePosition, Cloneable {
    /**
     * Scope this statement belongs to.
     */
    public AbstractLocalOwnerScope outerScope;
    /**
     * Tags, empty list for no tags.
     */
    public List<CommentTag> tags;
    /**
     * Position in the parsed stream.
     */
    protected StreamPos pos;
    /**
     * A text range of this statement, null for unspecified.
     */
    public TextRange textRange;
    
    /**
     * If this statement can be dead. Usually true for some compiler--added
     * statements that are implicit in the source, like loop jumps.<br>
     *
     * Also recursively describes any possible sub--statements.<br>
     *
     * The default is false.<br>
     *
     * Thre is a subclass <code>LabeledStatement</code> that in some cases
     * has this value set to true in its constructor, see the class docs
     * for details.
     */
    public boolean canBeDead = false;
    /**
     * All references to the key variable should be replaced with references
     * to the values variable <code>replaceThrownOutTarget</code>. Null or
     * empty for no replacement. This is used only in joining special classes.
     * See the method <code>Generator.checkClassReplacements</code> for details.
     *
     * @see Generator.checkClassReplacements
     */
    public Map<Variable, Variable> thrownOutReplacement;
    
    /**
     * Creates a new instance of Statement.
     * 
     * @param pos                       position in the parsed stream
     * @param outerScope                scope this statement belongs to,
     *                                  either <code>BlockScope</code> or,
     *                                  for outermost the code block,
     *                                  <code>MethodScope</code>
     */
    public AbstractStatement(StreamPos pos, AbstractLocalOwnerScope outerScope) {
        this.pos = pos;
        //if(!(outerScope instanceof AbstractLocalOwnerScope))
        //    throw new RuntimeException("scope must be LOCAL OWNER one");
        this.outerScope = outerScope;
        tags = new LinkedList<>();
    }
    @Override
    public void setStreamPos(StreamPos pos) {
        //this.pos = pos;
        throw new RuntimeException("position of abstract statement " +
                "can be set only in constructor");
    }
    @Override
    public StreamPos getStreamPos() {
        return pos;
    }
    @Override
    public Object accept(Visitor v)  throws CompilerException {
        return v.visit(this);
    }
    @Override
    public AbstractStatement clone() {
        try {
            AbstractStatement copy = (AbstractStatement)super.clone();
            copy.tags = new LinkedList(tags);
            if(thrownOutReplacement != null)
                throw new RuntimeException("can not copy");
            return copy;
        } catch(CloneNotSupportedException e) {
            throw new RuntimeException("unexpected");
        }
    }
}
