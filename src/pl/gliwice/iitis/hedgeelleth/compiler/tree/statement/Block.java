/*
 * Block.java
 *
 * Created on Dec 13, 2007, 1:23:46 PM
 *
 * Copyright (c) 2007 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree.statement;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.scope.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * A block.
 * 
 * @author Artur Rataj
 */
public class Block extends AbstractStatement {
    /**
     * This block scope.
     */
    public BlockScope scope;
    /**
     * Code of this block, can be empty but not null.
     */
    public List<AbstractStatement> code;
    
    /**
     * Creates a new instance of Block.
     * 
     * @param pos                       position in the parsed stream
     * @param outerScope                scope this block belongs to
     */
    public Block(StreamPos pos, AbstractLocalOwnerScope outerScope) {
        super(pos, outerScope);
        scope = new BlockScope(this, outerScope, "");
        code = new ArrayList<>();
    }
    @Override
    public Object accept(Visitor v) throws CompilerException {
        return v.visit(this);
    }
    @Override
    public String toString() {
        String s = "{ ";
        for(AbstractStatement statement : code)
            s += statement.toString() + " ";
        return s + "}";
    }
}
