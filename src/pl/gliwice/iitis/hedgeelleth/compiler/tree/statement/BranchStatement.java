/*
 * Statement.java
 *
 * Created on Dec 13, 2007, 4:01:27 PM
 *
 * Copyright (c) 2007 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree.statement;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.scope.AbstractLocalOwnerScope;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.AbstractExpression;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * A conditional branch statement.
 * 
 * @author Artur Rataj
 */
public class BranchStatement extends AbstractStatement {
    /**
     * Boolean condition.
     */
    public AbstractExpression condition;
    /** 
     * Label where to jump if the condition is true, or null for
     * the next statement.
     */
    public String labelTrue;
    /** 
     * Label where to jump if the condition is false, or null for
     * the next statement.
     */
    public String labelFalse;
    
    /**
     * Creates a new instance of BranchStatement.
     * 
     * @param pos                       position in the parsed stream
     * @param outerScope                scope this statement belongs to
     * @param condition                 a boolean condition
     * @param labelTrue                 label where to jump if the
     *                                  condition is true, or null for
     *                                  the next statement
     * @param labelFalse                label where to jump if the
     *                                  condition is false, or null for
     *                                  the next statement
     */
    public BranchStatement(StreamPos pos, AbstractLocalOwnerScope outerScope,
            AbstractExpression condition, String labelTrue, String labelFalse) {
        super(pos, outerScope);
        this.condition = condition;
        this.labelTrue = labelTrue;
        this.labelFalse = labelFalse;
    }
    @Override
    public Object accept(Visitor v) throws CompilerException {
        return v.visit(this);
    }
}
