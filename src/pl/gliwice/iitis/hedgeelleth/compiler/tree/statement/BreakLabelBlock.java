/*
 * BreakLabelBlock.java
 *
 * Created on Feb 21, 2009, 3:23:13 PM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree.statement;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.scope.BlockScope;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * A block with a break label, that points to the exit of the block.
 *
 * @author Artur Rataj
 */
public class BreakLabelBlock extends Block {

    /**
     * Creates a new instance of BreakLabelBlock.
     *
     * @param pos                       position in the parsed stream
     * @param outerScope                scope this statement belongs to
     * @param labelContext              label context of this statement,
     *                                  must obey rules as defined in
     *                                  <code>BlockScope.setLabelContext</code>
     * @param sub                       substatement, if null, an empty
     *                                  substatement is created instead
     * @see BlockScope.setLabelContext
     */
    public BreakLabelBlock(StreamPos pos, BlockScope outerScope,
            String labelContext, AbstractStatement sub) {
        super(pos, outerScope);
        if(!scope.setLabelContext(labelContext))
            throw new RuntimeException("duplicate label context");
        String breakLabel = outerScope.newLabel();
        scope.setBreakLoopLabel(breakLabel);
        if(sub != null)
            code.add(sub);
        code.add(new LabeledStatement(pos, outerScope,
                breakLabel, null));
    }
    @Override
    public Object accept(Visitor v) throws CompilerException {
        return v.visit(this);
    }
}
