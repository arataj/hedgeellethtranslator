/*
 * JumpStatement.java
 *
 * Created on Dec 13, 2007, 4:01:27 PM
 *
 * Copyright (c) 2007 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree.statement;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.scope.AbstractLocalOwnerScope;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * A jump statement.
 * 
 * @author Artur Rataj
 */
public class JumpStatement extends AbstractStatement {
    /** 
     * Label where to jump.
     */
    public String label;

    /**
     * Creates a new instance of BranchStatement.
     * 
     * @param pos                       position in the parsed stream
     * @param outerScope                scope this statement belongs to
     * @param label                     label where to jump
     */
    public JumpStatement(StreamPos pos, AbstractLocalOwnerScope outerScope, String label) {
        super(pos, outerScope);
        this.label = label;
    }
    @Override
    public Object accept(Visitor v) throws CompilerException {
        return v.visit(this);
    }
}
