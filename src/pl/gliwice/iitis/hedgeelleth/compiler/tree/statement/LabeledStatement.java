/*
 * LabeledStatement.java
 *
 * Created on Dec 18, 2007, 11:34:38 AM
 *
 * Copyright (c) 2007 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree.statement;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.scope.BlockScope;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.EmptyExpression;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * A statement with a label.
 * 
 * @author Artur Rataj
 */
public class LabeledStatement extends AbstractStatement {
    /**
     * Label of this statement.
     */
    public String label;
    public AbstractStatement sub;
    
    /**
     * Creates a new instance of LabeledStatement.<br>
     *
     * If <code>sub</code> is null, then the field
     * <code>canBeDead</code> is for this statement set to true
     * in this constructor, as empty labels, even if explicit in
     * the source code, do not usually have to be used.
     *
     * @param pos                       position in the parsed stream
     * @param outerScope                scope this statement belongs to
     * @param label                     label of this statement
     * @param sub                       substatement, if null, an empty
     *                                  substatement is created instead
     */
    public LabeledStatement(StreamPos pos, BlockScope outerScope,
            String label, AbstractStatement sub) {
        super(pos, outerScope);
        this.label = label;
        if(sub == null) {
            canBeDead = true;
            sub = new EmptyExpression(pos, outerScope);
        }
        this.sub = sub;
    }
    @Override
    public Object accept(Visitor v) throws CompilerException {
        return v.visit(this);
    }
}
