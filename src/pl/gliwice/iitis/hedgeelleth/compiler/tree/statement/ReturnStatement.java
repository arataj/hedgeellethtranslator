/*
 * ReturnStatement.java
 *
 * Created on Dec 18, 2007, 11:34:38 AM
 *
 * Copyright (c) 2007 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree.statement;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.scope.AbstractLocalOwnerScope;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.AbstractExpression;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * A return statement.
 * 
 * @author Artur Rataj
 */
public class ReturnStatement extends AbstractStatement {
    /**
     * Subexpression of this statement, null for none.
     */
    public AbstractExpression sub;
    
    /**
     * Creates a new instance of ReturnStatement.
     * 
     * @param pos                       position in the parsed stream
     * @param outerScope                scope this statement belongs to
     * @param sub                       subexpression, null for none
     */
    public ReturnStatement(StreamPos pos, AbstractLocalOwnerScope outerScope,
            AbstractExpression sub) {
        super(pos, outerScope);
        this.sub = sub;
    }
    @Override
    public Object accept(Visitor v) throws CompilerException {
        return v.visit(this);
    }
    @Override
    public String toString() {
        String s = "return";
        if(sub != null)
            s += " " + sub.toString();
        return s;
    }
}
