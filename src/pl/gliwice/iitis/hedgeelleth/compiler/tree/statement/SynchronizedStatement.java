/*
 * SynchronizedStatement.java
 *
 * Created on Apr 18, 2008
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree.statement;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.scope.AbstractLocalOwnerScope;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.AbstractExpression;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * A synchronized statement.
 * 
 * @author Artur Rataj
 */
public class SynchronizedStatement extends AbstractStatement {
    /** 
     * Lock.
     */
    public AbstractExpression lock;
    /**
     * The block synchronized by the lock.
     */
    public Block block;

    /**
     * Creates a new instance of SynchronizedStatement. The synchronized
     * block should be added later.
     * 
     * @param pos                       position in the parsed stream
     * @param outerScope                scope this statement belongs to
     * @param lock                      lock expression
     */
    public SynchronizedStatement(StreamPos pos, AbstractLocalOwnerScope outerScope,
            AbstractExpression lock) {
        super(pos, outerScope);
        this.lock = lock;
    }
    @Override
    public Object accept(Visitor v) throws CompilerException {
        return v.visit(this);
    }
}
