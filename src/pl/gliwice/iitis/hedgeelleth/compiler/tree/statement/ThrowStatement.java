/*
 * ThrowStatement.java
 *
 * Created on May 9, 2011.
 *
 * Copyright (c) 2011 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree.statement;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.scope.AbstractLocalOwnerScope;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.AbstractExpression;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * A throw statement.
 * 
 * @author Artur Rataj
 */
public class ThrowStatement extends AbstractStatement {
    /**
     * Subexpression of this statement. Should be a variable or a 
     * constant representing <code>null</code> if no object
     * is thrown.
     */
    public AbstractExpression sub;
    
    /**
     * Creates a new instance of ReturnStatement.
     * 
     * @param pos                       position in the parsed stream
     * @param outerScope                scope this statement belongs to
     * @param sub                       subexpression
     */
    public ThrowStatement(StreamPos pos, AbstractLocalOwnerScope outerScope,
            AbstractExpression sub) {
        super(pos, outerScope);
        this.sub = sub;
    }
    @Override
    public Object accept(Visitor v) throws CompilerException {
        return v.visit(this);
    }
    @Override
    public String toString() {
        return "throw " + sub.toString();
    }
}
