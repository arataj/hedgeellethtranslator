/*
 * Expression.java
 *
 * Created on Dec 18, 2007, 11:22:56 AM
 *
 * Copyright (c) 2007 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.AbstractStatement;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.scope.BlockScope;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * An expression.
 * 
 * The result type is set in the semantic check.
 *
 * @author Artur Rataj
 */
public abstract class AbstractExpression extends AbstractStatement implements Typed, Cloneable {
    /**
     * Type of this expression, determined during
     * semantic check.
     */
    Type resultType;
    
    /**
     * Creates a new instance of Expression.
     * 
     * @param pos                       position in the parsed stream
     * @param scope                scope this expression belongs to
     */
    public AbstractExpression(StreamPos pos, BlockScope scope) {
        super(pos, scope);
    }
    @Override
    public Object accept(Visitor v)  throws CompilerException {
// System.out.println("#" + System.identityHashCode(this));
        return v.visit(this);
    }
    /**
     * If this expression resolves to a constant expression, a
     * respective literal is returned. Otherwise, null is
     * returned.<br>
     * 
     * This method does only a very simple analysis, including
     * unary minus, and is used only to set
     * <code>Type.constant</code>. If null is returned, it does
     * not mean that this expression could not be translated to
     * a constant one after the analysis in
     * <code>StaticAnalysis</code>.<br>
     * 
     * Primary expressions must already be parsed to call this
     * method.<br>
     * 
     * This implementation returns null.
     * 
     * @return a literal or null
     */
    public Literal getSimpleConstant() {
        return null;
    }
    @Override
    public void setResultType(Type resultType) {
        this.resultType = resultType;
    }
    @Override
    public Type getResultType() {
        return resultType;
    }
    @Override
    public AbstractExpression clone() {
        AbstractExpression copy = (AbstractExpression)super.clone();
        if(resultType != null)
            copy.resultType = new Type(resultType);
        return copy;
    }
}
