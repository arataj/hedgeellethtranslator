/*
 * AbstractOperator.java
 *
 * Created on Oct 2, 2015
 *
 * Copyright (c) 2015  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression;

/**
 *
 * @author Artur Rataj
 */
public interface AbstractOperator {
    /**
     * If is unary.
     * 
     * @return if this operator is unary
     */
    public static boolean isUnary(AbstractOperator op) {
        return op instanceof UnaryExpression.Op;
    }
    /**
     * If is binary.
     * 
     * @return if this operator is binary
     */
    public static boolean isBinary(AbstractOperator op) {
        return op instanceof BinaryExpression.Op;
    }
    /**
     * Returns an operator, that is represented by a given string
     * representation.
     * 
     * @param s string representation
     * @return operator
     */
    public static AbstractOperator parse(String s) {
        try {
            return UnaryExpression.Op.parse(s);
        } catch(RuntimeException e) {
            return BinaryExpression.Op.parse(s);
        }
    }
    /**
     * <p>Returns a semi--qualified enumeration identifier of this operation.</p>
     * 
     * <p>Example: <code>UnaryExpression.Khc</code></p>
     * 
     * @return a string containing an identifier
     */
    public static String enumId(AbstractOperator op) {
        if(isUnary(op))
            return "UnaryExpression.Op." + ((UnaryExpression.Op)op).name();
        else
            return "BinaryExpression.Op." + ((BinaryExpression.Op)op).name();
    }
}
