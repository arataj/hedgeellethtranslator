/*
 * AllocationExpression.java
 *
 * Created on Jan 20, 2008, 7:25:09 PM
 *
 * This file is copyright (c) 2008 Artur Rataj.
 *
 * This software is provided under the terms of the GNU Library General
 * Public License, either version 2 of the license or, at your option,
 * any later version. See http://www.gnu.org for details.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.scope.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * An allocation expression.<br>
 *
 * Created during parsing. This is why the constructor field is set later.
 * 
 * @author Artur Rataj
 */
public class AllocationExpression extends AbstractExpression {
    public enum Allocation {
        CONSTRUCTOR,
        ARRAY,
    };
    public Allocation allocation;

    /**
     * For ARRAY, element type, for CONSTRUCTOR,
     * the same as the result type.
     */
    Type elementType;
    /**
     * Either constructor parameters or array size expressions.
     */
    public List<AbstractExpression> expressions;
    /**
     * A constructor, found during semantic check.
     */
    public Constructor constructor;

    /**
     * Creates a new instance of AllocationExpression.
     * 
     * @param pos                       position in the parsed stream
     * @param outerScope                scope this expression belongs to
     * @param allocation                allocation type -- array or constructor
     * @param type                      type of allocated object or type of
     *                                  an element of allocated array
     * @param expressions               either constructor parameters or
     *                                  array size expressions
     * @param arrayClassName            fully qualified name of the special
     *                                  array class, needed only for array
     *                                  allocations, for constructor allocations
     *                                  can be null
     */
    public AllocationExpression(StreamPos pos, BlockScope outerScope, Allocation allocation,
            Type type, List<AbstractExpression> expressions, String arrayClassName) {
        super(pos, outerScope);
        this.allocation = allocation;
        this.expressions = expressions;
        this.elementType = type;
        Type t = null;
        switch(this.allocation) {
            case CONSTRUCTOR:
                t = new Type(type);
                break;

            case ARRAY:
                t = new Type(type);
                for(int i = 0; i < this.expressions.size(); ++i)
                    t.increaseDimension(arrayClassName);
                break;

            default:
                throw new RuntimeException("unknown type: " + this.allocation);
        }
        setResultType(t);
    }
    @Override
    public Object accept(Visitor v) throws CompilerException {
        return v.visit(this);
    }
    @Override
    public AllocationExpression clone() {
        AllocationExpression copy = (AllocationExpression)super.clone();
        copy.elementType = new Type(elementType);
        copy.expressions = new LinkedList<>();
        for(AbstractExpression e : expressions)
            copy.expressions.add(e.clone());
        if(constructor != null)
            throw new RuntimeException("no longer cloneable");
        return copy;
    }
    @Override
    public String toString() {
        String s = "new " + elementType.toNameString();
        if(allocation == Allocation.CONSTRUCTOR)
            s += "(";
        boolean firstExpression = true;
        for(AbstractExpression e : expressions) {
            switch(allocation) {
                case ARRAY:
                    s += "[" + e.toString() + "]";
                    break;

                case CONSTRUCTOR:
                    if(!firstExpression)
                        s += ", ";
                    s += e.toString();
                    break;

            default:
                throw new RuntimeException("unknown type: " + allocation);
            }
            firstExpression = false;
        }
        if(allocation == Allocation.CONSTRUCTOR)
            s += ")";
        return s;
    }
}
