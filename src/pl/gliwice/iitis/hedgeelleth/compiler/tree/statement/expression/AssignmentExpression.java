/*
 * AssignmentExpression.java
 *
 * Created on Jan 22, 2008, 9:09:47 PM
 *
 * This file is copyright (c) 2008 Artur Rataj.
 *
 * This software is provided under the terms of the GNU Library General
 * Public License, either version 2 of the license or, at your option,
 * any later version. See http://www.gnu.org for details.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.scope.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * An assignment expression. The
 * 
 * @author Artur Rataj
 */
public class AssignmentExpression extends AbstractExpression {
    /**
     * L value. The primary expression must represent a variable.
     * The variable may lack type, then the type will be completed
     * during semantic check of this assignment expression. Yet,
     * the variable can not be used by anything before the assignment
     * expression is parsed.<br>
     *
     * A known case of the lack of type are primary expression's
     * primitive range variables, that hold the range's bounds.
     * In such a case, respective <code>AssignmentExpression<code>s
     * are in the list <code>PrimaryPrefix.rangeExpressions</code>.
     */
    public PrimaryExpression lvalue;
    /**
     * R value.
     */
    public AbstractExpression rvalue;
    /**
     * Used only of <code>CompilationOptions.blankFinalsAllowed</code> is false:
     * if this assignment should accept that the lvalue is a final variable.
     * True only for initializers.
     */
    public boolean acceptFinal;
    /*
     * Used only of <code>CompilationOptions.blankFinalsAllowed</code> is false:
     * marks that a given assignment of a field is default, as opposed to a
     * source--level one.
     */
    /* public boolean defaultInitializer; */
    
    /**
     * Creates a new instance of AssignmentExpression, with
     * <code>acceptFinal</code> and <code>defaultInitializer</code> being false.
     *
     * @param pos                       position in the stream
     * @param outerScope                scope this expression belongs to
     * @param lvalue                    l value
     * @param rvalue                    r value
     */
    public AssignmentExpression(StreamPos pos, BlockScope outerScope,
            PrimaryExpression lvalue, AbstractExpression rvalue) {
        super(pos, outerScope);
        this.lvalue = lvalue;
        this.rvalue = rvalue;
        acceptFinal = false;
        /* defaultInitializer = false; */
    }
    @Override
    public Object accept(Visitor v) throws CompilerException {
        return v.visit(this);
    }
    @Override
    public AssignmentExpression clone() {
        AssignmentExpression copy = (AssignmentExpression)super.clone();
        copy.lvalue = lvalue.clone();
        copy.rvalue = rvalue.clone();
        return copy;
    }
    @Override
    public String toString() {
        return lvalue.toString() + " = " + rvalue.toString();
    }
}
