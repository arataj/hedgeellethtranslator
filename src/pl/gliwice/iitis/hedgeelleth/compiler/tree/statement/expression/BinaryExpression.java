/*
 * BinaryExpression.java
 *
 * Created on Jan 15, 2008, 12:52:21 PM
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.scope.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * A binary expression.
 * 
 * @author Artur Rataj
 */
public class BinaryExpression extends AbstractExpression {
    /**
     * Type of this expression.
     */
    public enum Op implements AbstractOperator {
        PLUS,
        MINUS,
        MULTIPLY,
        DIVIDE,
        MODULUS,
        MODULUS_POS,
        INCLUSIVE_OR,
        EXCLUSIVE_OR,
        AND,
        CONDITIONAL_OR,
        CONDITIONAL_AND,
        EQUAL,
        INEQUAL,
        LESS,
        GREATER,
        LESS_OR_EQUAL,
        GREATER_OR_EQUAL,
        SHIFT_LEFT,
        SHIFT_RIGHT,
        UNSIGNED_SHIFT_RIGHT,
        // the following are only for property expressions
        U,
        R,
        IMPLICATION,
        EQUIVALENCE;
                
        public boolean isRelational() {
            switch(this) {
                case EQUAL:
                case INEQUAL:
                case LESS:
                case GREATER:
                case LESS_OR_EQUAL:
                case GREATER_OR_EQUAL:
                    return true;
                    
                default:
                    return false;
            }
        }
        /**
         * If this operator is boolean. Includes logic operators.
         * 
         * @return if boolean
         */
        public boolean isBoolean() {
            switch(this) {
                case CONDITIONAL_OR:
                case CONDITIONAL_AND:
                case U:
                case R:
                    return true;
                    
                default:
                    return false;
            }
        }
        /**
         * Returns an opposite, domain--wise as opposed to
         * left/right wise, relational operator to this
         * one. If this operator is not relational null is
         * returned.
         * 
         * @return                      relational operator
         *                              or null
         */
        public Op newOpposite() {
            if(!isRelational())
                return null;
            else {
                Op o = null;
                switch(this) {
                    case EQUAL:
                        o = INEQUAL;
                        break;
                        
                    case INEQUAL:
                        o = EQUAL;
                        break;
                        
                    case LESS:
                        o = GREATER_OR_EQUAL;
                        break;
                        
                    case GREATER:
                        o = LESS_OR_EQUAL;
                        break;
                        
                    case LESS_OR_EQUAL:
                        o = GREATER;
                        break;
                        
                    case GREATER_OR_EQUAL:
                        o = LESS;
                        break;

                    default:
                        throw new RuntimeException("unknown operator: " + this);
                }
                return o;
            }
        }
        /**
         * Returns an operator, that is represented by a given string
         * representation.
         * 
         * @param s string representation
         * @return operator
         */
        public static Op parse(String s) {
                if(s.equals("+"))
                    return PLUS; 
                else if(s.equals("-"))
                    return MINUS;
                else if(s.equals("*"))
                    return MULTIPLY;
                else if(s.equals("/"))
                    return DIVIDE;
                else if(s.equals("%"))
                    return MODULUS;
                else if(s.equals("%+"))
                    return MODULUS_POS;
                else if(s.equals("=="))
                    return EQUAL;
                else if(s.equals("<>"))   
                    return INEQUAL;
                else if(s.equals("<"))
                    return LESS;
                else if(s.equals(">"))   
                    return GREATER;
                else if(s.equals(">="))
                    return LESS_OR_EQUAL;
                else if(s.equals("<="))
                    return GREATER_OR_EQUAL;
                else if(s.equals("|"))
                    return INCLUSIVE_OR;
                else if(s.equals("^"))
                    return EXCLUSIVE_OR;
                else if(s.equals("&"))
                    return AND;
                else if(s.equals("||"))
                    return CONDITIONAL_OR;
                else if(s.equals("&&"))
                    return CONDITIONAL_AND;
                else if(s.equals("U"))
                    return U;
                else if(s.equals("R"))
                    return R;
                else if(s.equals("->"))
                    return IMPLICATION;
                else if(s.equals("<->"))
                    return EQUIVALENCE;
                else
                    throw new RuntimeException("unknown operator");
        }
        @Override
        public String toString() {
            switch(this) {
                case PLUS:
                    return "+";

                case MINUS:
                    return "-";

                case MULTIPLY:
                    return "*";

                case DIVIDE:
                    return "/";

                case MODULUS:
                    return "%";
                    
                case MODULUS_POS:
                    return "%+";

                case INCLUSIVE_OR:
                    return "|";

                case EXCLUSIVE_OR:
                    return "^";

                case AND:
                    return "&";

                case EQUAL:
                    return "==";

                case INEQUAL:
                    return "!=";

                case LESS:
                    return "<";

                case GREATER:
                    return ">";

                case LESS_OR_EQUAL:
                    return "<=";

                case GREATER_OR_EQUAL:
                    return ">=";

                case CONDITIONAL_OR:
                    return "||";

                case CONDITIONAL_AND:
                    return "&&";

                case SHIFT_LEFT:
                    return "<<";

                case SHIFT_RIGHT:
                    return ">>";

                case UNSIGNED_SHIFT_RIGHT:
                    return ">>>";

                case U:
                    return "U";

                case R:
                    return "R";

                case IMPLICATION:
                    return "->";

                case EQUIVALENCE:
                    return "<->";
                    
                default:
                    throw new RuntimeException("unknown operator " + this.name());
            }
        }
    };
    /**
     * Type of this expression.
     */
    public Op operatorType;
    /**
     * Left subexpression.
     */
    public AbstractExpression left;
    /**
     * Right subexpression.
     */
    public AbstractExpression right;
    
    /**
     * Creates a new instance of BinaryExpression.
     *
     * @param pos                       position in the parsed stream
     * @param outerScope                scope this expression belongs to
     * @param operandType               operand type
     * @param left                      left subexpression
     * @param right                     right subexpression
     */
    public BinaryExpression(StreamPos pos, BlockScope outerScope,
            Op operatorType,
            AbstractExpression left, AbstractExpression right) {
        super(pos, outerScope);
        this.operatorType = operatorType;
        this.left = left;
        this.right = right;
    }
    @Override
    public Object accept(Visitor v) throws CompilerException {
        return v.visit(this);
    }
    /**
     * Returns a string representation of an operator.
     * 
     * @param operatorType              operator type
     * @return                          string representation of
     *                                  the operator
     */
    public static String getOperatorName(Op operatorType) {
        return operatorType.toString();
    }
    /**
     * Flips graphically the name of an operator, so that
     * it has the same meaning as the original operator
     * with the right and left operands fliped.
     * 
     * @param s                         original name
     * @return                          flipped name
     */
    public static String flipOperatorName(String s) {
        if(s.equals("!="))
            return s;
        if(s.length() == 2)
            s = "" + s.charAt(1) + s.charAt(0);
        for(int pos = 0; pos < s.length(); ++pos) {
            char c = s.charAt(pos);
            if(c == '<')
                c = '>';
            else if(c == '>')
                c = '<';
            s = s.substring(0, pos) + c +
                    s.substring(pos + 1);
        }
        return s;
    }
    @Override
    public BinaryExpression clone() {
        BinaryExpression copy = (BinaryExpression)super.clone();
        copy.left = left.clone();
        copy.right = right.clone();
        return copy;
    }
    @Override
    public String toString() {
        return "( " + left.toString() + " " + getOperatorName(operatorType) + " " +
                right.toString() + " )";
    }
}
