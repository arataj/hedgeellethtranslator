/*
 * BlockExpression.java
 *
 * Created on Feb 12, 2009
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.scope.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * A block expression contains a block of code. The last statement
 * should be an assignment expression, whose lvalue is treated as
 * the value of this expression.<br>
 *
 * Used to implement expressions that do not fit into the standard
 * unary or binary expressions, like for example the conditional
 * expression. It has a single field <code>block</code>, that can
 * either be set when constructing this expression in the parser,
 * or later, during semantic check, when more type infomation is
 * available. See <code>JavaConditionalExpression</code> for an example
 * of the latter case.
 * 
 * @author Artur Rataj
 */
public class BlockExpression extends AbstractExpression {
    /**
     * Block of code.
     */
    public Block block;

    /**
     * Creates a new instance of BlockExpression.
     * 
     * @param pos                       position in the parsed stream
     * @param outerScope                scope this expression belongs to
     * @param block                     block, the last statement should
     *                                  be an assignment, can be null if
     *                                  set later
     */
    public BlockExpression(StreamPos pos, BlockScope outerScope,
            Block block) {
        super(pos, outerScope);
        this.block = block;
    }
    @Override
    public Object accept(Visitor v)  throws CompilerException {
        return v.visit(this);
    }
    @Override
    public AbstractExpression clone() {
        throw new RuntimeException("not implemented");
    }
    @Override
    public String toString() {
        return "{(" +
                (block != null ? block.toString() : "<no block>") +
                ")}";
    }
}
