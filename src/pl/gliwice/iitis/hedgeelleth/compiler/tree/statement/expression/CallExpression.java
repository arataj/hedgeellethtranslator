/*
 * CallExpression.java
 *
 * Created on Jan 15, 2008, 3:56:28 PM
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.scope.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * A call method expression.<br>
 *
 * Created either (1) during semantic check of primary expression,
 * where <code>method</code> is set within the constructor, or (2)
 * outside the primary expression, where the method is only a constant
 * to be recognized and replaced during semantic check.
 *
 * @author Artur Rataj
 */
public class CallExpression extends AbstractExpression {
    /**
     * Invoked method.<br>
     *
     * If the invoked method is replaced after construction
     * of this expression, then
     * this expression's return type should be updated to the
     * replacement method's result type.
     */
    public Method method;
    /**
     * List of arguments.
     */
    public List<AbstractExpression> arg;
    /**
     * If this is a direct call, so in effect, no runtime call resolution
     * applies to this call. Only in calls of non--static methods this field
     * can have true value.
     */
    public boolean directNonStatic = false;
    /**
     * If this expression has been created outside the primary expression,
     * and the invoked method is non--static, then this field must
     * point to "this" of the invoked method. Otherwise must be null.<br>
     *
     * If this expression is a part of a primary expression, "this" of
     * a non--static call is extracted from the expression itself,
     * so this field is not used.<br>
     *
     * A typical case of a call expression outside a primary expression
     * is a call added implicitly by the compiler, like a call to an init
     * method or compiler--added call to superconstructor.
     */
    public AbstractExpression independentCallThis;
    
    /**
     * Creates a new instance of CallExpression. The call is by default
     * not direct. A result type is the called method's result type.
     * 
     * @param pos                       position in the parsed stream
     * @param outerScope                scope this expression belongs to
     * @param method                    called method or constructor
     * @param arg                       list of arguments
     */
    public CallExpression(StreamPos pos, BlockScope outerScope,
            Method method, List<AbstractExpression> arg) {
        super(pos, outerScope);
        this.method = method;
        this.arg = arg;
        setResultType(method.getResultType());
    }
    @Override
    public Object accept(Visitor v) throws CompilerException {
        return v.visit(this);
    }
    @Override
    public CallExpression clone() {
        CallExpression copy = (CallExpression)super.clone();
        copy.arg = new LinkedList<>();
        for(AbstractExpression e : arg)
            copy.arg.add(e.clone());
        if(independentCallThis != null)
            copy.independentCallThis = independentCallThis.clone();
        return copy;
    }
    @Override
    public String toString() {
        String s;
        if(method.signature == null)
            s = method.name;
        else
            s = method.ret.toString() + " " + method.signature;
        s += "(";
        boolean first = true;
        for(AbstractExpression e: arg) {
            if(!first)
                s += ", ";
            s += e.toString();
            first = false;
        }
        s += ")";
        if(directNonStatic)
            s += "[DIRECT]";
        return s;
    }
}
