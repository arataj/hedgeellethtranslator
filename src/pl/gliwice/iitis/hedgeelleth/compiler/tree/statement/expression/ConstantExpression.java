/*
 * EmptyExpression.java
 *
 * Created on Jan 20, 2008, 10:41:22 AM
 *
 * This file is copyright (c) 2008 Artur Rataj.
 *
 * This software is provided under the terms of the GNU Library General
 * Public License, either version 2 of the license or, at your option,
 * any later version. See http://www.gnu.org for details.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.scope.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * A constant expression.
 * 
 * @author Artur Rataj
 */
public class ConstantExpression extends AbstractExpression implements Cloneable {
    /**
     * A literal being the value of this expression.
     */
    public Literal literal;
    
    /**
     * Creates a new instance of EmptyExpression.
     * 
     * @param pos                       position in the parsed stream
     * @param outerScope                scope this expression belongs to
     * @param literal                   literal, if null, then either must be
     *                                  set later, along with return type, or
     *                                  this expression denotes an unknown constant
     */
    public ConstantExpression(StreamPos pos, BlockScope outerScope, Literal literal) {
        super(pos, outerScope);
        this.literal = literal;
        if(this.literal != null)
            setResultType(literal.type);
    }
    @Override
    public Object accept(Visitor v) throws CompilerException {
        return v.visit(this);
    }
    @Override
    public Literal getSimpleConstant() {
        return literal;
    }
    /**
     * If an expression is not null ans has a boolean value, then convert
     * <code>false</code> to 0 and <code>true</code> to 1. Otherwise,
     * return the expression as is.
     * @param expr input expression
     * @return converted expression or <code>expr</code>
     */
    public static ConstantExpression convertToInteger(ConstantExpression c) {
        if(c != null) {
            Literal l = c.getSimpleConstant();
            if(l.type.isBoolean())
                if(l.getBoolean())
                    c = new ConstantExpression(c.getStreamPos(), (BlockScope)c.outerScope,
                            new Literal(1));
                else
                    c = new ConstantExpression(c.getStreamPos(), (BlockScope)c.outerScope,
                            new Literal(0));
        }
        return c;
    }
    /**
     * If an expression is not null ans has an integer value, then convert
     * 0 to <code>false</code> and 1 to <code>true</code>. Otherwise,
     * return the expression as is.
     * 
     * @param expr input expression
     * @return converted expression or <code>expr</code>
     */
    public static ConstantExpression convertToBoolean(ConstantExpression c) {
        if(c != null) {
            Literal l = c.getSimpleConstant();
            if(l.type.isOfIntegerTypes()) {
                long v = l.getMaxPrecisionInteger();
                if(v == 0)
                    c = new ConstantExpression(c.getStreamPos(), (BlockScope)c.outerScope,
                            new Literal(false));
                else if(v == 1)
                    c = new ConstantExpression(c.getStreamPos(), (BlockScope)c.outerScope,
                            new Literal(true));
            }
        }
        return c;
    }
    /*
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + (this.literal != null ? this.literal.hashCode() : 0);
        return hash;
    }
    @Override
    public boolean equals(Object o) {
        return this.literal.equals(((ConstantExpression)o).literal);
    }
     */
    @Override
    public ConstantExpression clone() {
        ConstantExpression copy = (ConstantExpression)super.clone();
        copy.literal = ((Literal)literal).clone();
        return copy;
    }
    @Override
    public String toString() {
        if(literal == null)
            return "<unknown>";
        else
            return literal.toString();
    }
}
