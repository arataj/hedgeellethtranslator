/*
 * EmptyExpression.java
 *
 * Created on Jan 20, 2008, 10:41:22 AM
 *
 * This file is copyright (c) 2008 Artur Rataj.
 *
 * This software is provided under the terms of the GNU Library General
 * Public License, either version 2 of the license or, at your option,
 * any later version. See http://www.gnu.org for details.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.scope.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * An empty expression.
 *
 * @author Artur Rataj
 */
public class EmptyExpression extends AbstractExpression {
    /**
     * If to force translation of this expression into
     * <code>CodeOpNone</code>, with an annotation
     * <code>CompilerAnnotation.STATE</code>, what makes
     * it non--optimizable, see <code>CodeOpNone</code>
     * for details.
     */
    public boolean toNoneOp = false;

    /**
     * Creates a new instance of EmptyExpression.
     * 
     * @param pos                       position in the parsed stream
     * @param outerScope                scope this expression belongs to
     */
    public EmptyExpression(StreamPos pos, BlockScope outerScope) {
        super(pos, outerScope);
    }
    @Override
    public Object accept(Visitor v) throws CompilerException {
        return v.visit(this);
    }
    @Override
    public EmptyExpression clone() {
        EmptyExpression copy = (EmptyExpression)super.clone();
        return copy;
    }
}
