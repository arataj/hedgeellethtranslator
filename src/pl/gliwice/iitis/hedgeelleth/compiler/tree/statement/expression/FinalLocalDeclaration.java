/*
 * FinalLocalDeclaration.java
 *
 * Created on Aug 30, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.Variable;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.scope.BlockScope;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.StreamPos;

/**
 * A marker that a given final local variable has been declared.<br>
 *
 * The marker is inserted at the place of declaration, and directly before
 * a possible initialization of the variable. It is used to inform
 * <code>TraceValues.checkInitializedFinals()</code>, that any possible
 * initializations of the equivalent code variable should be cleared at the
 * respective point, as the variable is "freshly declared" from the source
 * code point of view. The information would be lost otherwise, as code
 * generation flattens the nested blocks and also the resulting nested scopes.
 *
 * @author Artur Rataj
 */
public class FinalLocalDeclaration extends EmptyExpression {
    /**
     * A final local variable, whose declaration is marked by this expression.
     */
    public final Variable variable;
    
    /**
     * Creates a new instance of EmptyExpression.
     * 
     * @param pos                       position in the parsed stream
     * @param outerScope                scope this expression belongs to
     * @param variable                  a final local
     */
    public FinalLocalDeclaration(StreamPos pos, BlockScope outerScope,
            Variable variable) {
        super(pos, outerScope);
        this.variable = variable;
        if(!this.variable.flags.isFinal())
            throw new RuntimeException("not final");
        if(!this.variable.isLocal())
            throw new RuntimeException("not local");
    }
     @Override
    public FinalLocalDeclaration clone() {
        FinalLocalDeclaration copy = (FinalLocalDeclaration)super.clone();
        return copy;
    }
}
