/*
 * IndexExpression.java
 *
 * Created on Dec 19, 2008.
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.scope.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * An array index expression.<br>
 *
 * This expression is not visited by
 * <code>pl.gliwice.iitis.hedgeelleth.compiler.tree.Visitor</code>, as the
 * resulting code is just a dereference, parsed as other dereferences.
 * See <code>CodeIndexDereference</code> for details.
 * 
 * Created during semantic check of primary expression, so the method can be
 * set already by the constructor.
 *
 * @author Artur Rataj
 */
public class IndexExpression extends AbstractExpression {
    /**
     * Indexing expression.
     */
    public AbstractExpression index;
    
    /**
     * Creates a new instance of IndexExpression. A result type
     * is computed, that has one dimension less than the indexed
     * variable.
     * 
     * @param pos                       position in the parsed stream
     * @param outerScope                scope this expression belongs to
     * @param indexed                   type of indexed variable
     * @param index                     indexing expression
     * @param arrayClassName            fully qualified name of the special
     *                                  array class, needed only for array
     *                                  allocations, for constructor allocations
     *                                  can be null
     */
    public IndexExpression(StreamPos pos, BlockScope outerScope,
            Type indexed, AbstractExpression index, String arrayClassName) {
        super(pos, outerScope);
        this.index = index;
        indexed = new Type(indexed);
        indexed.decreaseDimension(arrayClassName);
        setResultType(indexed);
    }
    @Override
    public Object accept(Visitor v) throws CompilerException {
        return v.visit(this);
    }
    @Override
    public IndexExpression clone() {
        IndexExpression copy = (IndexExpression)super.clone();
        copy.index = index.clone();
        return copy;
    }
    @Override
    public String toString() {
        String s = "[" + index.toString() + "]";
        return s;
    }
}
