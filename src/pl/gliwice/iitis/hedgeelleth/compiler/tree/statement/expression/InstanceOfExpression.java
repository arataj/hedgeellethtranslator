/*
 * InstanceOfExpression.java
 *
 * Created on Jan 15, 2008, 3:56:28 PM
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.scope.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * An instanceof expression. NOT USED NOW!
 * 
 * @author Artur Rataj
 */
public class InstanceOfExpression extends AbstractExpression {
    /**
     * Subexpression.
     */
    public AbstractExpression sub;
    /**
     * Tested type.
     */
    public Type type;
    
    /**
     * Creates a new instance of InstanceOfExpression.
     * 
     * @param pos                       position in the parsed stream
     * @param outerScope                scope this expression belongs to
     * @param expression                expression
     * @param type                      type
     */
    public InstanceOfExpression(StreamPos pos, BlockScope outerScope,
            AbstractExpression expression, Type type) {
        super(pos, outerScope);
        this.sub = expression;
        this.type = type;
    }
    @Override
    public Object accept(Visitor v) throws CompilerException {
        return v.visit(this);
    }
    @Override
    public InstanceOfExpression clone() {
        InstanceOfExpression copy = (InstanceOfExpression)super.clone();
        copy.sub = sub.clone();
        copy.type = new Type(type);
        return copy;
    }
}
