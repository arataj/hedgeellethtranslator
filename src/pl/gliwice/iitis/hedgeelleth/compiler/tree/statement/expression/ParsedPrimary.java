/*
 * ParsedPrimaryExpression.java
 *
 * Created on Jan 23, 2008, 1:19:26 PM
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.clazz.AbstractJavaClass;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.flags.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.languages.IntegerSetExpression;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.scope.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.SourcePosition;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.StreamPos;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.TextRange;
import pl.gliwice.iitis.hedgeelleth.modules.AbstractFrontend;
import pl.gliwice.iitis.hedgeelleth.modules.CliConstants;

/**
 * A semantic transformation of PrimaryExpression.
 *
 * @author Artur Rataj
 */
public class ParsedPrimary implements Typed {
    /**
     * The keyword "super".
     */
    public static final String SUPER_KEYWORD = "super";

    /**
     * Command--line defined constants.
     */
    public final CliConstants constants;
    /**
     * Subsequent items of this expression.
     */
    public final List<Typed> contents;
    /**
     * PrefixType of the result of this expression.
     */
    private Type resultType;
    /**
     * Frontend of the parsed expression.
     */
    private final AbstractFrontend frontend;
    
    /**
     * Creates a new instance of ParsedPrimary, using
     * a primary expression. Use during the semantic check.<br>
     * 
     * It constructs, if needed, and then performs the semantic
     * check of <code>CallExpression</code> and
     * <code>IndexExpression</code>. So, if there were no other
     * creators of these two types of expressions, the visit
     * methods of theses expression in <code>SemanticCheck</code>
     * would never be called. Actually, a call to an init
     * method or compiler--added call to constructor contain
     * <code>CallExpression</code> outside a primary expression,
     * see docs in <code>SemanticCheck</code> and
     * <code>CallExpression</code> for details.
     * 
     * @param constants                      command--line defined constants,
     * @param visitor                   visitor
     * @param e                         primary expression
     */
    public ParsedPrimary(CliConstants constants,
            SemanticCheck visitor, PrimaryExpression e)
            throws CompilerException {
//if(e.toString().contains("3"))
//    e = e;
        this.constants = constants;
        frontend = e.outerScope.getBoundingJavaClass().frontend;
/*if(e.toString().indexOf("f2") != -1)
    e = e;*/
        contents = new LinkedList<>();
        String thatString = "";
        Object pending;
        //String arrayClassName = e.outerScope.getBoundingMethodScope().getBoundingJavaClass().
        //        frontend.arrayClassName;
        //
        // merge any prefix contents with suffixes
        //
        List<PrimarySuffix> merged = new LinkedList<>(e.
                suffixList);
        switch(e.prefix.type) {
            case LITERAL:
                pending = new ConstantExpression(e.getStreamPos(),
                        (BlockScope)e.outerScope, (Literal)e.prefix.contents);
                break;
            
            case NAME:
                pending = (NameList)e.prefix.contents;
                break;
            
            case THIS:
                pending = new NameList(Method.LOCAL_THIS);
                break;
            
            case SUPER:
                pending = new NameList(SUPER_KEYWORD);
                break;
            
            case PARENTHESES:
                pending = (AbstractExpression)e.prefix.contents;
                break;
            
            case ALLOCATION:
                pending = (AbstractExpression)e.prefix.contents;
                break;

            default:
                throw new RuntimeException("unknown prefix");

        }
        if(pending instanceof AbstractExpression) {
            AbstractExpression ae = (AbstractExpression)pending;
            merged.add(0, new PrimarySuffix(PrimarySuffix.Type.EXPRESSION,
                    ae));
        } else if(pending instanceof NameList) {
            if(!merged.isEmpty() &&
                    merged.get(0).type == PrimarySuffix.Type.NAME) {
                // two following names, concatenate them
                NameList latter = new NameList(
                        (NameList)merged.get(0).contents);
                latter.line = ((NameList)pending).line;
                latter.column = ((NameList)pending).column;
                latter.list.addAll(0, ((NameList)pending).list);
                PrimarySuffix s = new PrimarySuffix(e.prefix.getStreamPos(),
                        latter);
                merged.remove(0);
                merged.add(0, s);
            } else
                merged.add(0, new PrimarySuffix(e.prefix.getStreamPos(),
                        (NameList)pending));
        } else
            throw new RuntimeException("unknown prefix type");
        //
        // parse the suffixes that resulted from merging
        //
        PrimarySuffix previous = null;
        for(int suffixPos = 0; suffixPos < merged.size(); ++suffixPos) {
            PrimarySuffix suffix = merged.get(suffixPos);
            switch(suffix.type) {
                case EXPRESSION:
                {
                    AbstractExpression expression = (AbstractExpression)suffix.contents;
                    expression.accept(visitor);
                    if(!contents.isEmpty())
                        throw new RuntimeException("direct expression not at the beginning");
                    // an expression, let's just add the expression
                    contents.add(expression);
                    break;
                }
                case NAME:
                {
                    // it can be a method or variable, unless it is the
                    // last element or the next element is index,
                    // then we know it is a variable
                    // let's use that knowledge, as it is the last iteration
                    // of this loop, and we do not want to add a special case
                    // code after the loop, that would handle the variable
                    // instead
                    NameItemType it;
                    if(suffixPos == merged.size() - 1 ||
                            merged.get(suffixPos + 1).type == PrimarySuffix.Type.INDEX)
                        it = NameItemType.VARIABLE;
                    else
                        it = NameItemType.IGNORE;
                    NameList name = (NameList)suffix.contents;
/*if(name.toString().equals("random")) {
    name = name;
}*/
                    if(previous == null) {
                        thatString = "";
                    } else if(previous.type == PrimarySuffix.Type.NAME) {
                        // name continuation, should not happen
                        throw new RuntimeException("name.name");
                    } else if(previous.type == PrimarySuffix.Type.EXPRESSION ||
                            previous.type == PrimarySuffix.Type.ARGUMENTS ||
                            previous.type == PrimarySuffix.Type.INDEX) {
                        // name that follows some expression, must be a dereference
                        AbstractExpression pendingExpression = (AbstractExpression)
                                contents.get(contents.size() - 1);
                        if(pendingExpression.getResultType().isPrimitive())
                            throw new CompilerException(pendingExpression.getStreamPos(),
                                    ParseException.Code.ILLEGAL,
                                    "dereferenced primitive value");
                        else {
                            AbstractJavaClass javaClass = e.outerScope.lookupJavaClassAT(
                                    pendingExpression.getResultType().getJava());
                            NameList fullName = new NameList(
                                    javaClass.getQualifiedName());
                            //if(it != NameItemType.IGNORE)
                            //    // the name is not pending but is actually
                            //    // a variable, so add it to "that"
                            //    fullName.list.addAll(name.list);
                            thatString = fullName.toString();
                        }
                    }
                    thatString = parseList(e.outerScope, thatString, name, it, contents,
                         e.scopeMode);
                    break;
                }
                case ARGUMENTS:
                    if(previous.type == PrimarySuffix.Type.NAME) {
                        // method call
                        List<AbstractExpression> arg = (List<AbstractExpression>)suffix.contents;
                        // check semantics of arguments
                        CompilerException errors = new CompilerException();
                        for(AbstractExpression a : arg)
                            try {
                                a.accept(visitor);
                            } catch(CompilerException f) {
                                errors.addAllReports(f);
                            }
                        if(errors.reportsExist())
                            throw errors;
                        // parseList ignored the last element as it was NameItemType.IGNORE,
                        // now we know it was a method, add the method's name
                        NameList name = new NameList(
                                thatString +(thatString.isEmpty() ? "" : ".") +
                                ((NameList)previous.contents).last(1));
                        Method method = e.outerScope.lookupMethodByNameAT(
                                        e.outerScope.getBoundingMethodScope().getBoundingJavaClass().frontend,
                                        name, arg,
                                        // "::" involves only the first method call
                                        suffixPos == 1 ? e.scopeMode : PrimaryExpression.ScopeMode.OMIT_STATIC);
                        /*
                        if(method == null)
                            throw new CompilerException(null, "method not found: " +
                                    Method.getDeclarationDescription(
                                    name.toString(), arg, null));
                         */
                        if(method.flags.context == Context.NON_STATIC) {
                            if(thatString.isEmpty()) {
                                // it is a call of non--static method with implicit "this"
                                // dereference in the source code, so add "this"
                                if(contents.size() != 0)
                                    throw new RuntimeException("implicit \"this\" after " +
                                            "some primary expression code");
                                Variable that = e.outerScope.getLocalThis();
                                contents.add(that);
                            }
                            Typed that;
                            if(contents.size() == 0)
                                that = null;
                            else
                                that = contents.get(contents.size() - 1);
                            if(that == null)
                                throw new CompilerException(e.getStreamPos(),
                                        ParseException.Code.ILLEGAL,
                                        "can not access method " + method.getDeclarationDescription(true) +
                                        " from a static context");
                            if(!that.getResultType().mutatorReference &&
                                    method.flags.mutator)
                                throw new CompilerException(e.getStreamPos(),
                                        ParseException.Code.ILLEGAL,
                                        "type " + that.getResultType().toNameString() +
                                        " does not allow mutator method");
                        }
                        AbstractExpression expression;
                        if(method.annotations.contains(CompilerAnnotation.
                                MODEL_GET_INT_CONST_STRING) ||
                                method.annotations.contains(CompilerAnnotation.
                                MODEL_GET_DOUBLE_CONST_STRING)) {
                            boolean floating = method.annotations.contains(
                                    CompilerAnnotation.MODEL_GET_DOUBLE_CONST_STRING);
                            Literal constName = arg.get(0).getSimpleConstant();
                            if(constName == null)
                                throw new CompilerException(arg.get(0).getStreamPos(),
                                        CompilerException.Code.PARSE,
                                        "direct string constant expected");
                            List<Literal> values = constants.defines.get(constName.getString());
                            if(values != null) {
                                //
                                // make the literals match the method's return type
                                //
                                List<Literal> intValues = new LinkedList<>();
                                if(floating) {
                                    for(Literal v : values) {
                                        double d = v.getMaxPrecisionFloatingPoint();
                                        intValues.add(new Literal(d));
                                    }
                                } else {
                                    for(Literal v : values) {
                                        long d = v.getMaxPrecisionInteger();
                                        if(d > Integer.MAX_VALUE || d < Integer.MIN_VALUE)
                                            throw new CompilerException(arg.get(0).getStreamPos(),
                                                    CompilerException.Code.PARSE,
                                                    "external constant " + d + " does not fit into type int");
                                        intValues.add(new Literal((int)d));
                                    }
                                }
                                values = intValues;
                            }
                            if(values == null) {
                                // constant not defined at the command line,
                                // mark as an unknown constant
                                //
                                // call expressions within primary expressions are not visited
                                expression = new CallExpression(e.getStreamPos(),
                                        (BlockScope)e.outerScope, method, arg);
                            } else if(values.size() == 1) {
                                // a single constant
                                //
                                // fitting into the target type must be checker later
                                expression = new ConstantExpression(e.getStreamPos(),
                                        (BlockScope)e.outerScope, values.iterator().next());
                                visitor.visit((ConstantExpression)expression);
                            } else if(values.size() > 1) {
                                // set of integer values
                                //
                                // field name must be completed later
                                expression = new IntegerSetExpression(e.getStreamPos(),
                                        e.getStreamPos(), (BlockScope)e.outerScope, values,
                                        null);
                                visitor.visit((ConstantExpression)expression);
                            } else
                                throw new CompilerException(suffix.getStreamPos(),
                                        ParseException.Code.ILLEGAL,
                                        "external constant " + constName.getString() + " defined, " +
                                        "but no values given");
                        } else {
                            if(method.annotations.contains(CompilerAnnotation.RANDOM_STRING))
                                method = method;
                            // call expressions within primary expressions are not visited
                            CallExpression c = new CallExpression(e.getStreamPos(),
                                (BlockScope)e.outerScope, method, arg);
                            StreamPos textBeg = e.prefix.textBeg;
                            StreamPos textEnd = e.suffixList.get(e.suffixList.size() - 1).textEnd;
                            if(textBeg != null && textEnd != null)
                                c.textRange = new TextRange(textBeg, textEnd);
                            expression = c;
                        }
                        /*
                        if(method.name.contains("del") && !arg.isEmpty() &&
                                arg.get(0).resultType.toString().contains("Array")) {
                            e.outerScope.lookupMethodByNameAT(
                                e.outerScope.getBoundingMethodScope().getBoundingJavaClass().frontend,
                                name, arg);
                        }
                         */
                        if(((NameList)previous.contents).list.get(0).equals(SUPER_KEYWORD) &&
                                ((NameList)previous.contents).list.size() == 2)
                            // a method call prefixed with "super" is direct
                            ((CallExpression)expression).directNonStatic = true;
                        // e.pos.toString() + ": " 
                        //prevType = ItemType.EXPRESSION;
                        // semantic check not needed -- already done
                        // expression.accept(visitor);
                        contents.add(expression);
                        NameList n = expression.getResultType().getJava();
                        if(n == null)
                            // a primitive type, can not be dereferenced
                            thatString = null;
                        else {
                            AbstractJavaClass javaClass = e.outerScope.
                                    lookupJavaClassAT(n);
                            NameList fullName = new NameList(
                                    javaClass.getQualifiedName());
                            thatString = fullName.toString();
                        }
                    } else
                        throw new CompilerException(suffix.getStreamPos(),
                                ParseException.Code.ILLEGAL,
                                "unexpected '('");
                    break;
                    
                case INDEX:
                    Typed indexed = null;
                    int pos = contents.size() - 1;
                    int dimension = 0;
                    do {
                        if(pos < 0)
                            break;
                        indexed = contents.get(pos);
                        --pos;
                        ++dimension;
                    } while(indexed instanceof IndexExpression);
                    Variable indexedVariable;
                    if(indexed instanceof Variable)
                        indexedVariable = (Variable)indexed;
                    else if(indexed instanceof CallExpression)
                        indexedVariable = ((CallExpression)indexed).method.returnLocal;
                    else
                        throw new ParseException(previous.getStreamPos(),
                                ParseException.Code.ILLEGAL,
                                "indexed non-variable");
                    if(!indexedVariable.getResultType().isJava())
                        throw new ParseException(previous.getStreamPos(),
                                ParseException.Code.ILLEGAL,
                                "indexed non-array variable");
                    AbstractJavaClass indexedJavaClass = e.outerScope.lookupJavaClassNA(
                            indexedVariable.getResultType().getJava());
                    if(indexedVariable.getResultType().getDimension(
                            indexedJavaClass.frontend.arrayClassName) < dimension)
                        throw new ParseException(previous.getStreamPos(),
                                ParseException.Code.ILLEGAL,
                                "indexed non-array variable");
                    AbstractExpression index = (AbstractExpression)suffix.contents;
                    // check semantics
                    index.accept(visitor);
                    SemanticCheck.checkIndexType(index, frontend);
                    Type indexedType = new Type(indexedVariable.getResultType());
                    int i = dimension;
                    while(i > 1) {
                        indexedType.decreaseDimension(indexedJavaClass.frontend.arrayClassName);
                        --i;
                    }
                    IndexExpression expression = new IndexExpression(e.getStreamPos(),
                            (BlockScope)e.outerScope,  indexedType,
                            index, indexedJavaClass.frontend.arrayClassName);
                    contents.add(expression);
                    NameList n = expression.getResultType().getJava();
                    if(n == null)
                        // a primitive type, can not be dereferenced
                        thatString = null;
                    else {
                        AbstractJavaClass javaClass = e.outerScope.
                                lookupJavaClassAT(n);
                        NameList fullName = new NameList(
                                javaClass.getQualifiedName());
                        thatString = fullName.toString();
                    }
                    break;

                default:
                    throw new RuntimeException("unknown suffix type: " + suffix.type);
            }
            previous = suffix;
        }
        setResultType(contents.get(contents.size() - 1).getResultType());
        if(e.scopeMode == PrimaryExpression.ScopeMode.STATIC) {
            Typed firstItem = contents.isEmpty() ? null : contents.get(0);
            if(!(firstItem instanceof Variable) &&
                    !(firstItem instanceof CallExpression))
                throw new ParseException(e.getStreamPos(),
                        ParseException.Code.ILLEGAL,
                        "field or method expected");
        }
    }
    /**
     * Item type in the list parsed by <code>parseList</code>.
     */
    enum NameItemType {
        IGNORE,
        VARIABLE,
        // variable or a class name or other part of a qualified
        // class name
        VARIABLE_OR_JAVA,
    };
    /**
     * Parses a list of names.
     *
     * @param scope                     expression scope
     * @param thatString                "that" string
     * @param list                      list of names to process
     * @param lastItem                  specifies the type of the last item
     *                                  in the list of names, or if to ignore
     *                                  the last item
     * @param contents                  contents to which this method
     *                                  appends the new contents the
     *                                  method found
     * @return                          new "that" string, if still needed
     */
    private final String parseList(AbstractAccessibilityScope scope, String thatString, NameList list,
            NameItemType lastItem, List<Typed> contents,
            PrimaryExpression.ScopeMode scopeMode) throws ParseException {
        /*
        if(that != null) {
            if(!that.getResultType().isJava())
                throw new ParseException("dereferenced primitive type");
            thatString = that.getResultType().getJava().toString();
        } else
            thatString = "";
         */
        // System.out.println("that = " + thatString + " list = " + list);
//        // a qualification prefix
//        // true if the last part has been used to determine the preceeding
//        // parts, but at the same time the last part should not be included
//        // in <code>thatString</code>, because <code>lastitem</code>
//        // is IGNORE
//        boolean ignoreLastPart = false;
        SCAN_NAME:
        for(int i = 0; i < list.list.size(); ++i) {
            PrimaryExpression.ScopeMode currScopeMode =
                    // "::" involves only the first field dereference
                    i == 0 ? scopeMode : PrimaryExpression.ScopeMode.OMIT_STATIC;
//if(thatString == null || contents == null)
//    i = i;
            if(thatString == null) {
                Typed dereferenced = contents.get(contents.size() - 1);
                throw new ParseException(null, ParseException.Code.INVALID,
                        "can not dereference " + dereferenced.
                                getResultType().toNameString());
            }
                
            boolean isFirstInExpression = thatString.isEmpty() &&
                    contents.isEmpty();
            boolean isLastNameItem = i == list.list.size() - 1;
            NameItemType it;
            if(isLastNameItem)
                it = lastItem;
            else
                it = NameItemType.VARIABLE_OR_JAVA;
            NameList n = new NameList(thatString);
            n.list.add(list.list.get(i));
            // System.out.println("testing " + n.toString() + " as " + it + "...");
/*if(n.toString().equals("ackBit"))
    n = n;*/
            switch(it) {
                case IGNORE:
                {
                    // it is the last element, whose type is not yet known,
                    // ignore
                    break SCAN_NAME;
                }
                case VARIABLE:
                case VARIABLE_OR_JAVA:
                {
                    if(it == NameItemType.VARIABLE && !isLastNameItem)
                        throw new RuntimeException(it + " can occur only " +
                                "as the last element");
                    // variables have the priority for VARIABLE_OR_JAVA
                    boolean onlyField = false;
                    boolean forbidField = false;
                    if(frontend.fieldAccessPrefix != null) {
                        String s = n.list.get(0);
                        if(s.startsWith(frontend.fieldAccessPrefix)) {
                            s = s.substring(frontend.fieldAccessPrefix.length());
                            if(s.isEmpty())
                                throw new ParseException(null,
                                        ParseException.Code.MISSING,
                                        "missing field name");
                            n.list.set(0, s);
                            onlyField = true;
                        } else if(isFirstInExpression && 
                                currScopeMode != PrimaryExpression.ScopeMode.STATIC)
                            // no field prefix or "::" at the beginning of the expression,
                            // so it can not be a field
                            forbidField = true;
                    }
                    Variable v = null;
                    boolean superFound = false;
                    try {
                        // there is no variable "super"
                        if(n.toString().equals(SUPER_KEYWORD)) {
                            if(onlyField)
                                throw new ParseException(null,
                                        ParseException.Code.MISSING,
                                        SUPER_KEYWORD + " preceeded with " + frontend.fieldAccessPrefix);
                            superFound = true;
                            v = scope.getLocalThis();
                        } else {
                            AbstractAccessibilityScope variableScope;
                            if(onlyField)
                                variableScope = scope.getBoundingJavaClass().scope;
                            else
                                variableScope = scope;
                            v = variableScope.lookupVariableAT(n,
                                    // "::" involves only the first field reference
                                    currScopeMode);
                        }
                    } catch(ParseException e) {
                        if(it == NameItemType.VARIABLE)
                            throw new ParseException(null, null, e);
                        if(currScopeMode == PrimaryExpression.ScopeMode.STATIC) {
                            // the first element after "::" was not found, even that it is
                            // supposed to be not a qualified name
                            throw e;
                        }
                    }
                    if(n.toString().equals(Method.LOCAL_THIS) &&
                            scope.getLocalThis() == null)
                        throw new ParseException(null,
                                ParseException.Code.ILLEGAL,
                                "this object does not exist in a static context");
                    NameList javaClassName;
                    if(v != null) {
                        if(!v.isLocal() && forbidField)
                            throw new ParseException(null,
                                    ParseException.Code.ILLEGAL,
                                    "field lacks a qualifier");
                        if(!v.isLocal() && ((FieldFlags)v.flags).context == Context.NON_STATIC &&
                                thatString.isEmpty()) {
                            // it is a non--static field with implicit "this"
                            // dereference in the source code, so add "this"
                            if(!isFirstInExpression)
                                throw new RuntimeException("implicit \"this\" after " +
                                        "some primary expression code");
                            Variable localThis = scope.getLocalThis();
                            if(localThis == null)
                                throw new ParseException(null,
                                        ParseException.Code.ILLEGAL,
                                        "non-static field " + v.name + " can not be accessed " +
                                        "from a static context");
                            contents.add(localThis);
                        }
                        contents.add(v);
                        if(v.type != null)
                            javaClassName = v.type.getJava();
                        else
                            // type can be null, see <code>AssignmentExpression.lvalue</code>
                            // for details
                            javaClassName = null;
                    } else {
                        if(onlyField)
                            throw new ParseException(null,
                                    ParseException.Code.ILLEGAL,
                                    "field " + n + " not found");
                        javaClassName = n;
                    }
                    // a <code>AbstractJavaClass</code> object is needed to get a qualified name
                    // System.out.println("jtn = " + javaClassName);
                    AbstractJavaClass j;
                    if(javaClassName != null) {
                        do {
                            try {
                                j = scope.lookupJavaClassAT(javaClassName);
                            } catch(ParseException e) {
                                if(v == null) {
                                    ++i;
                                    if(i < list.list.size()) {
                                        // the class name does not come from a variable, but from
                                        // the list -- append the next element from the list,except
                                        // for the last one, as a class name by itself is invalid
                                        javaClassName.list.add(list.list.get(i));
                                        if(i == list.list.size() - 1) {
                                            throw e;
                                            // ignoreLastPart = true;
                                        }
                                    } else
                                        throw e;
                                    j = null;
                                } else
                                    throw e;
                            }
                        } while(v == null && j == null);
                        if(v != null && !v.type.javaClassFound) {
                            v.type.javaClassFound = true;
                            v.type.type = j.getQualifiedName();
                        }
                        if(superFound) {
                            if(j.extendS == null)
                                throw new ParseException(null, null,
                                        "no super type found");
                            j = scope.lookupJavaClassAT(j.extendS.getJava());
                            if(j == null)
                                throw new RuntimeException("no super java class found");
                        }
                    } else
                        j = null;
                    if(j != null) {
                        // do nothing, as it is only a part of a qualified name
                        switch(it) {
                            case VARIABLE:
                                if(superFound)
                                    throw new ParseException(null, null,
                                            "\"super\" not dereferenced");
                                // should not be used any more as an element is known to
                                // be a variable only if it is the last one in a primary
                                // expression
//if(list.list.size() > i + 1)
//    list = list;
                                thatString = null;
                                break;

                            case VARIABLE_OR_JAVA:
                                thatString = j.getQualifiedName().toString();
                                break;
                        }
                    } else {
//if(list.list.size() > i + 1)
//    list = list;
                        // it is a primitive type
                        thatString = null;
                    }
                    break;
                }

                default:
                    throw new RuntimeException("unknown item type: " + it);
            }
        }
/*if(thatString != null && thatString.equals("java.lang.Thread.start"))
    thatString = thatString;*/
//        if(ignoreLastPart && thatString != null)
//            thatString = thatString.substring(0, thatString.lastIndexOf('.'));
        return thatString;
    }
    /**
     * If this expression has return value that is a variable
     * or an element of an array.
     *
     * @return                          if evaluates to a variable
     */
    public boolean isVariableExpression() {
        if(contents.size() == 0)
            return false;
        else {
            for(int index = contents.size() - 1; index >= 0; --index) {
                Typed t = contents.get(index);
                if(t instanceof Variable)
                    return true;
                else if(t instanceof IndexExpression)
                    // element of an array
                    continue;
                else
                    return false;
            }
            throw new RuntimeException("index expression at the beginning");
        }
    }
    /**
     * If this expression has return value that is a variable,
     * then return the variable. Otherwise, result null.
     *
     * @return variable or null
     */
    public Variable getVariable() {
        if(contents.size() == 1) {
            Typed t = contents.get(0);
            if(t instanceof Variable)
                return (Variable)t;
        }
        return null;
    }
    /**
     * Finds a dereference and adds possible dereferences to this
     * expression on the basis of the sequence of dereferences.
     * 
     * Exceptions thrown by this method have source position added
     * by one of the callers.
     * 
     * @param scope                     scope 
     * @param name                      string representing a sequence of
     *                                  dereferences, since the element
     *                                  whose index is equal to
     *                                  origNameBegin
     * @param origNameBegin             this is the variable origNameBegin
     *                                  in the constructor of this class,
     *                                  and there are the docs of the
     *                                  variable
     * @return                          resulting reference
     */
    /*
    private VariableReference findVariableReference(Scope scope,
                NameList name, int origNameBegin) throws CompilerException {
            Variable variable = scope.lookupVariable(name);
            if(variable == null)
                throw new CompilerException("variable " + name + " not found");
            SemanticCheck.findJavaTypes(scope, variable);
            Variable that;
            if(!variable.context)
                that = resolveDereferences(scope, name,
                    origNameBegin, Variable.class).object;
            else
                that = null;
            VariableReference reference = new VariableReference(
                    that, variable);
            return reference;
    }
     */
    /**
     * Resolves dereferences. Types are variables except for
     * the last element, that can also be a method.
     * 
     * This method is intended only for non--static methods and
     * variables. In the case of methods, the returned object is
     * "this" or null if the latest result should be used, that is, of the
     * previous part of the parsed primary expression.
     * 
     * The reference object representing a dereference is interpreted
     * as follows: if object is not null, it is dereferenced, if object
     * is null, the latest result is dereferenced.
     * 
     * Exceptions thrown by this method have source position added
     * by one of the callers.
     * 
     * @param scope                     scope
     * @param name                      fully qualified name
     *                                  of the resulting type
     * @param index                     index in the name since
     *                                  which the references should
     *                                  be resolved
     * @param objectType                if the name denotes a variable
     *                                  or a method
     * @return                          if the last element is variable,
     *                                  the returned reference in object
     *                                  contains the object for the variable
     *                                  or null if the last element is local,
     *                                  the variable in the reference is null.
     *                                  If the last element is a method,
     *                                  this method returns null if the latest
     *                                  result should be used as "this", otherwise
     *                                  the whole reference should used as "this"
     */
    /*
    VariableReference resolveDereferences(Scope scope, NameList name, int index,
            Class objectType) throws CompilerException {
        if(name.toString().equals("barber.start"))
            name = name;
        Variable that = null;
        Variable field = null;
        List<ItemType> currTypes = new LinkedList<ItemType>();
        List<Typed> currContents = new LinkedList<Typed>();
        int maxIndex = name.list.size() - 1;
        NameList currName = new NameList("");
        for(int i = 0; i < index; ++i)
            currName.list.add(name.list.get(i));
        int offset = 0;
        if(index == 0) {
            NameList tmp = new NameList(name.list.get(0));
            Variable v = scope.lookupVariable(tmp);
            if(v == null)
                v = v;
            if(v != null && !v.isLocal() && !v.context) {
                // this is a non--static field without any prefix, add "this"
                currName.list.add("this");
                ++maxIndex;
                ++offset;
            } else if(maxIndex == 0 && objectType == Method.class) {
                // this is a non--static method without any prefix, add "this"
                currName.list.add("this");
                ++maxIndex;
                ++offset;
            }
        }
        boolean continuation = index != 0;
        // if the size of this list exceeds 1, then an additional
        // assignment is needed because one dereference can
        // denote up to a single object dereference
        List<Variable> dereferences = new LinkedList<Variable>();
        // do not touch the last element as it can be dereferenced
        // directly in an expression
        for(int i = index; i <= maxIndex - 1; ++i) {
            if(i - offset >= 0)
                currName.list.add(name.list.get(i - offset));
            Variable v = scope.lookupVariable(currName);
            SemanticCheck.findJavaTypes(scope, v);
            Object o = null;
            if(index == 0 && currName.list.size() == 1 && v == null) {
                // check if the name is not prefixed with a class name
                NameList className = new NameList(currName);
                int j = i + 1;
                while(j <= maxIndex) {
                    className.list.add(name.list.get(j - offset));
                    if(i < maxIndex) {
                        v = scope.lookupVariable(className);
                        if(v != null) {
                            if(!v.context)
                                throw new CompilerException("non-static variable " +
                                        className + " referenced by a static object");
                            currName = className;
                            i = j;
                            break;
                        }
                    } else {
                        o = scope.lookup(objectType, className);
                        if(o != null) {
                            if(objectType == Variable.class) {
                                if(!((Variable)o).context)
                                    throw new CompilerException("non-static variable " +
                                            className + " referenced by a static object");
                            } else {
                                if(!((Method)o).flags.context)
                                    throw new CompilerException("non-static method " +
                                            className + " referenced by a static object");
                            }
                            // do not touch the last element as it
                            // can be dereferenced directly in an
                            // expression
                            className.list.remove(className.list.size() - 1);
                            currName = className;
                            i = j - 1;
                            break;
                        }
                    }
                    ++j;
                }
            }
            if(v == null && o == null) {
                throw new CompilerException("variable " + currName +
                        " not found");
            }
            if(o == null && v.type.isPrimitive())
                throw new CompilerException("dereferenced " +
                        "primitive variable " + currName);
            if(o != null) {
                // static variable or class name -- no dereference
                that = null;
                currTypes.clear();
                currContents.clear();
                dereferences.clear();
                continuation = false;
            } else {
                that = v;
                dereferences.add(that);
                if(dereferences.size() > 1) {
                    if(objectType == Method.class && i == maxIndex - 1) {
                        // special case -- if the next element is not a variable
                        // but a method, it will not need a subsequent dereference
                        // but instead it may use the latest variable as "this"
                        // so skip this dereference
                        that = dereferences.get(0);
                        field = v;
                        continuation = false;
                        break;
                    } else {
                        // put an intermediate assignment that dereferences
                        // a single object
                        currTypes.add(ItemType.DEREFERENCE);
                        Variable object;
                        if(continuation)
                            object = null;
                        else
                            object = dereferences.get(0);
                        currContents.add(new VariableReference(object,
                                that));
                        continuation = true;
                        // remove the object dereferenced in the added assignment
                        // from the list
                        dereferences.remove(0);
                    }
                }
            }
        }
        types.addAll(currTypes);
        contents.addAll(currContents);
        if(that == null && maxIndex - index > 0) {
            if(objectType == Variable.class) {
                throw new CompilerException("non-static variable " +
                        name + " referenced by a static object");
            } else {
                throw new CompilerException("non-static method " +
                        name + " referenced by a static object");
            }
        }
        if(continuation)
            that = null;
        if(objectType == Variable.class)
            return new VariableReference(that, null);
        else if(that == null)
            // use the latest result as "this"
            return null;
        else if(field == null)
            // "this" is a local
            return new VariableReference(null, that);
        else
            // special case -- "this" of a method may be a reference
            return new VariableReference(that, field);
    }*/
    @Override
    public final void setResultType(Type resultType) {
        this.resultType = resultType;
    }
    @Override
    public final Type getResultType() {
        return resultType;
    }
    /**
     * Creates an expression containing only a dereference.
     * 
     * The resulting expression is already parsed and is not apt
     * to be parsed again.
     * 
     * @param pos                       position in the parsed stream,
     *                                  can be null
     * @param scope                     scope of the new expression
     * @param object                    dereferenced object or null
     *                                  for local
     * @param variable                  field or a local variable
     *                                  to be represented by the
     *                                  new expression
     * @return                          new expression
     */
    /*
    public static PrimaryExpression newVariableExpressionParsed(StreamPos pos,
            Scope scope, Variable object, Variable variable) {
        PrimaryExpression expression = newVariableExpression(
                pos, scope, variable);
        expression.value = new ParsedPrimary();
        ParsedPrimary expressionParsed = expression.value;
        expressionParsed.types.add(ItemType.VARIABLE);
        expressionParsed.contents.add(new VariableReference(
                object, variable));
        expressionParsed.setResultType(variable.getResultType());
        return expression;
    }
     */
    /**
     * Returns if this expression represents a variable,
     * possibly after dereferencing or indexing.
     * 
     * @return                          if this expression
     */
    /*
    public boolean isVariableExpression() {
        ItemType t = types.get(types.size() - 1);
        return t == ItemType.DEREFERENCE ||
                t == ItemType.VARIABLE;
    }
     */
    @Override
    public String toString() {
        String s =  "";
        for(Typed t : contents) {
            if(!s.isEmpty())
                s += " :: ";
            s += t.toString();
        }
        s = "<< " + s + " >>";
        return s;
    }
}
