/*
 * PrimaryExpression.java
 *
 * Created on Jan 3, 2008, 11:19:40 AM
 *
 * Copyright (c) 2007 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.scope.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.clazz.AbstractJavaClass;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * A primary expression.<br>
 *
 * It can lack a type only if lvalue of <code>AssignmentExpression</code>,
 * see <code>AssignmentExpression.lvalue</code> for details.
 *
 * The prefix can contain range checking values.<br>
 *
 * Note: please do not confuse range checking of a variable
 * with range checking of a primary expression -- see the docs of
 * <code>CodeVariable</code> for details.
 *
 * @author Artur Rataj
 */
public class PrimaryExpression extends AbstractExpression {
    /**
     * Defines a mode of resolving calls within this primary expression,
     * but not within nested primary expressions.
     */
    public enum ScopeMode {
        /**
         * An arbitrary primary expression, looked up current class methods,
         * fully qualified methods and static import methods.
         */
        ALL,
        /**
         * This primary expression is assumed to begin with a static import method
         * call or a static import field reference, otherwise a parse error is thrown.
         */
        STATIC,
        /**
         * Like <code>ALL</code>, but static import methods and static import
         * fields are not looked up.
         */
        OMIT_STATIC,
    };
    /**
     * Call mode.
     */
    public ScopeMode scopeMode;
    /**
     * Prefix.
     */
    public PrimaryPrefix prefix;
    /**
     * List of suffixes.
     */
    public List<PrimarySuffix> suffixList;
    /**
     * This expression parsed, or null if none.
     */
    public ParsedPrimary value;
    
    /**
     * Construct an instance of PrimaryExpression.
     * 
     * @param pos                       position in the parsed stream
     * @param outerScope                scope this expression belongs to
     * @param scopeMode                 scope mode
     * @param prefix                    prefix
     * @param suffixList                list of suffixes
     */
    public PrimaryExpression(StreamPos pos, BlockScope outerScope,
            ScopeMode scopeMode, PrimaryPrefix prefix,
            List<PrimarySuffix> suffixList) {
        super(pos, outerScope);
/*
if(prefix.contents instanceof NameList &&
        ((NameList)prefix.contents).toString().equals("MAX_BUFFER"))
    prefix = prefix;
    */
        this.scopeMode = scopeMode;
        this.prefix = prefix;
        this.suffixList = suffixList;
    }
    @Override
    public Object accept(Visitor v) throws CompilerException {
        return v.visit(this);
    }
    /**
     * Creates an expression containing only a reference
     * to a variable. It is not parsed, thus, the exact instance
     * of the variable is lost.
     *
     * @param pos                       position in the parsed stream,
     *                                  can be null
     * @param scope                     scope of the new expression
     * @param variable                  a variable to be represented
     *                                  by the new expression; only its name is
     *                                  used and, if not a local, then its owner
     *                                  is used as well
     * @return                          new expression
     */
    public static PrimaryExpression newVariableExpression(StreamPos pos,
            BlockScope scope, Variable variable) {
        if(variable == null)
            throw new RuntimeException("null variable");
        NameList variableName = new NameList(variable.name);
        if(!variable.isLocal()) {
            // the variable is a field
            AbstractJavaClass clazz = (AbstractJavaClass)variable.owner;
            String fieldPrefix = clazz.frontend.fieldAccessPrefix;
            if(fieldPrefix != null)
                variableName.list.set(0, fieldPrefix + variableName.list.get(0));
        }
        PrimaryExpression expression = new PrimaryExpression(
                pos, scope, PrimaryExpression.ScopeMode.OMIT_STATIC,
                new PrimaryPrefix(pos, variableName),
                new LinkedList<>());
        return expression;
    }
//    <code>ConstExpression</code> does that.
//    /**
//     * Creates an expression representing a constant.
//     * @param pos position in the parsed stream, can be null
//     * @param scope scope of the new expression
//     * @param constant literal representing the constant
//     * @return 
//     */
//    public static PrimaryExpression newConstExpression(StreamPos pos,
//            BlockScope scope, Literal constant) {
//        PrimaryExpression expression = new PrimaryExpression(
//                pos, scope, PrimaryExpression.ScopeMode.OMIT_STATIC,
//                new PrimaryPrefix(pos, constant),
//                new LinkedList<>());
//        return expression;
//    }
    @Override
    public Literal getSimpleConstant() {
        if(value == null) {
            if(suffixList.isEmpty() && prefix.contents instanceof Literal) {
                return ((Literal)prefix.contents).clone();
            } else
                throw new RuntimeException("can not evaluate unparsed non-literal");
        } else if(value.contents.size() == 1) {
            Typed typed = value.contents.get(0);
            if(typed instanceof AbstractExpression)
                return ((AbstractExpression)typed).getSimpleConstant();
        }
        return null;
    }
    @Override
    public PrimaryExpression clone() {
        PrimaryExpression copy = (PrimaryExpression)super.clone();
        copy.prefix = prefix.clone();
        copy.suffixList = new LinkedList<>();
        for(PrimarySuffix s : suffixList)
            copy.suffixList.add(s.clone());
        if(value != null)
            throw new RuntimeException("no longer cloneable");
        return copy;
    }
    @Override
    public String toString() {
        //String s =  super.toString() + "#" + prefix.toString();
        String s =  prefix.toString();
        boolean afterPrefix = true;
        for(PrimarySuffix suffix : suffixList) {
            if(afterPrefix &&
                    (prefix.type == PrimaryPrefix.PrefixType.NAME ||
                    prefix.type == PrimaryPrefix.PrefixType.THIS ||
                    prefix.type == PrimaryPrefix.PrefixType.PARENTHESES ||
                    prefix.type == PrimaryPrefix.PrefixType.SUPER) &&
                    suffix.type == PrimarySuffix.Type.NAME)
                s += ".";
            s += suffix.toString();
            afterPrefix = false;
        }
        return s;
    }
}
