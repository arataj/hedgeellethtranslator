/*
 * PrimaryPrefix.java
 *
 * Created on Jan 20, 2008, 7:16:19 PM
 *
 * This file is copyright (c) 2008 Artur Rataj.
 *
 * This software is provided under the terms of the GNU Library General
 * Public License, either version 2 of the license or, at your option,
 * any later version. See http://www.gnu.org for details.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import pl.gliwice.iitis.hedgeelleth.compiler.primitiverange.PrimitiveRangeDescription;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * A primary prefix of an expression.<br>
 *
 * Can contains range checking values, see <code>PrimaryExpression</code>
 * for details.
 *
 * @author Artur Rataj
 */
public class PrimaryPrefix implements SourcePosition, Cloneable {
    public enum PrefixType {
        LITERAL,
        NAME,
        THIS,
        SUPER,
        PARENTHESES,
        ALLOCATION,
    };
    /**
     * Position in the parsed stream of the last component of this prefix.
     */
    protected StreamPos pos;
    /**
     * The first character in the parsed stream. For long prefixes, it may be sometimes
     * less informatory than <code>pos</code>. Null if not specified.
     */
    public StreamPos textBeg;
    /**
     * PrefixType.
     */
    public PrefixType type;
    /**
     * Contents.
     */
    public Object contents;
    /**
     * Description of primitive range, null for none.
     */
    public PrimitiveRangeDescription rangeDescription;
    /**
     * Primitive range expressions, empty list for none. If not empty,
     * contains two expressions: first one for minimum value, second
     * one for maximum value.
     */
    public List<AssignmentExpression> rangeExpressions;

    /**
     * Creates a new instance of PrimaryPrefix.
     * 
     * @param pos                       position in the parsed stream
     * @param type                      type of this prefix
     * @param contents                  contents of this prefix
     */
    public PrimaryPrefix(StreamPos pos, PrefixType type, Object contents) {
        this.pos = pos;
        this.type = type;
        this.contents = contents;
        rangeDescription = null;
        rangeExpressions = new LinkedList<>();
    }
    /**
     * Creates a new instance of PrimaryPrefix, of the type PrefixType.LITERAL.
     * 
     * @param pos                       position in the parsed stream
     * @param literal                   literal
     */
    public PrimaryPrefix(StreamPos pos, Literal literal) {
        this(pos, PrefixType.LITERAL, literal);
    }
    /**
     * Creates a new instance of PrimaryPrefix, of the type PrefixType.NAME.
     * 
     * @param pos                       position in the parsed stream
     * @param name                      name   
     */
    public PrimaryPrefix(StreamPos pos, NameList name) {
        this(pos, PrefixType.NAME, name);
    }
    /**
     * Creates a new instance of PrimaryPrefix, of the type PrefixType.PARENTHESES.
     * 
     * @param expression                expression in parentheses
     */
    public PrimaryPrefix(AbstractExpression expression) {
        this(expression.getStreamPos(), PrefixType.PARENTHESES, expression);
    }
    /**
     * Creates a new instance of PrimaryPrefix, of the type PrefixType.ALLOCATION.
     * 
     * @param expression                allocation expression
     */
    public PrimaryPrefix(AllocationExpression expression) {
        this(expression.getStreamPos(), PrefixType.ALLOCATION, expression);
    }
    @Override
    public void setStreamPos(StreamPos pos) {
        throw new RuntimeException("position of prefix can be set only by constructor");
    }
    @Override
    public StreamPos getStreamPos() {
        return pos;
    }
    @Override
    public PrimaryPrefix clone() {
        PrimaryPrefix copy;
        try {
            copy = (PrimaryPrefix)super.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException("unexpected");
        }
        copy.pos = new StreamPos(pos);
        switch(type) {
            case LITERAL:
                copy.contents = ((Literal)contents).clone(); //new Literal(((Literal)contents));
                break;
            
            case NAME:
                copy.contents = new NameList((NameList)contents);
                break;
            
            case THIS:
            case SUPER:
                /* empty */
                break;
            
            case PARENTHESES:
            case ALLOCATION:
                copy.contents = ((AbstractExpression)contents).clone();
                break;

            default:
                throw new RuntimeException("unknown prefix type");

        }
//        for(PrimarySuffix s : suffixList)
//            copy.suffixList.add(s.clone());
//        if(value != null)
//            throw new RuntimeException("no longer cloneable");
        if(copy.rangeDescription != null)
            copy.rangeDescription = rangeDescription.clone();
        copy.rangeExpressions = new LinkedList<>();
        for(AssignmentExpression e : rangeExpressions)
            copy.rangeExpressions.add(e.clone());
        return copy;
    }
    @Override
    public String toString() {
        String s = "";
        switch(type) {
            case LITERAL:
                s = ((Literal)contents).toString();
                break;

            case NAME:
                s = ((NameList)contents).toString();
                break;
                
            case THIS:
                s = "this";
                break;

            case SUPER:
                s = "super";
                break;

            case PARENTHESES:
                s = "(" + ((AbstractExpression)contents).toString() + ")";
                break;

            case ALLOCATION:
                s = ((AllocationExpression)contents).toString();
                break;

            default:
                throw new RuntimeException("unknown literal type: " + type);
        }
        return s;
    }
}
