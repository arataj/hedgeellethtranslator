/*
 * PrimarySuffix.java
 *
 * Created on Jan 20, 2008, 7:16:19 PM
 *
 * This file is copyright (c) 2008 Artur Rataj.
 *
 * This software is provided under the terms of the GNU Library General
 * Public License, either version 2 of the license or, at your option,
 * any later version. See http://www.gnu.org for details.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * A primary suffix of an expression.
 * 
 * @author Artur Rataj
 */
public class PrimarySuffix implements Cloneable, SourcePosition {
    public enum Type {
        EXPRESSION,
        INDEX,
        NAME,
        ARGUMENTS,
    };
    /**
     * Position in the parsed stream.
     */
    protected StreamPos pos;
    /**
     * The last character in the parsed stream, inclusive. Null if not specified.
     * 
     * @see textBeg
     */
    public StreamPos textEnd;
    /**
     * Type.
     */
    public Type type;
    /**
     * Contents.
     */
    public Object contents;
    
    /**
     * Creates a new instance of PrimarySuffix.
     *
     * @param pos                       position in the parsed stream
     * @param type                      type of this suffix
     * @param contents                  contents of this suffix
     */
    protected PrimarySuffix(StreamPos pos, Type type, Object contents) {
        this.pos = pos;
        this.type = type;
        this.contents = contents;
    }
    /**
     * Creates a new instance of PrimarySuffix, of the type either
     * Type.EXPRESSION or Type.INDEX. The former happens only as
     * a prefix, moved to the suffix within <code>ParsedPrimary</code>.
     * 
     * @param expression                expression
     */
    public PrimarySuffix(Type type, AbstractExpression expression) {
        this(expression.getStreamPos(), type, (Object)expression);
        if(type != Type.EXPRESSION && type != Type.INDEX)
            throw new RuntimeException("expression can be only " +
                    "Type.EXPRESSION or Type.INDEX");
    }
    /**
     * Creates a new instance of PrimarySuffix, of the type Type.NAME.
     * 
     * @param pos                       position in the parsed stream
     * @param name                      name   
     */
    public PrimarySuffix(StreamPos pos, NameList name) {
        this(pos, Type.NAME, name);
    }
    /**
     * Creates a new instance of PrimarySuffix, of the type Type.ARGUMENTS.
     * 
     * @param pos                       position in the parsed stream
     * @param list                      expression list
     */
    public PrimarySuffix(StreamPos pos, List<AbstractExpression> list) {
        this(pos, Type.ARGUMENTS, list);
    }
    @Override
    public void setStreamPos(StreamPos pos) {
        throw new RuntimeException("position of prefix can be set only by constructor");
    }
    @Override
    public StreamPos getStreamPos() {
        return pos;
    }
    @Override
    public PrimarySuffix clone() {
        PrimarySuffix copy;
        try {
            copy = (PrimarySuffix)super.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException("unexpected");
        }
        copy.pos = new StreamPos(pos);
        switch(type) {
            case EXPRESSION:
            case INDEX:
                copy.contents = ((AbstractExpression)contents).clone();
                break;
            
            case NAME:
                copy.contents = new NameList((NameList)contents);
                break;
            
            case ARGUMENTS:
                List<AbstractExpression> list = new LinkedList<>();
                for(AbstractExpression e : (List<AbstractExpression>)contents)
                    list.add(e.clone());
                copy.contents = list;
                    
            default:
                throw new RuntimeException("unknown prefix type");

        }
        return copy;
    }
    @Override
    public String toString() {
        String s = "";
        switch(type) {
            case EXPRESSION:
                s = ((AbstractExpression)contents).toString();
                break;

            case INDEX:
                s = "[" + ((AbstractExpression)contents).toString() + "]";
                break;

            case NAME:
                s = ((NameList)contents).toString();
                break;
                
            case ARGUMENTS:
                for(AbstractExpression e : (List<AbstractExpression>)contents) {
                    if(s.length() != 0)
                        s += ", ";
                    s += e.toString();
                }
                s = "(" + s + ")";
                break;

            default:
                throw new RuntimeException("unknown suffix type: " + type);
        }
        return s;
    }
}
