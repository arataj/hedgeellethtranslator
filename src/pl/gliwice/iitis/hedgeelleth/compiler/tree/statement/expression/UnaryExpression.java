/*
 * UnaryExpression.java
 *
 * Created on Jan 15, 2008, 12:52:21 PM
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.scope.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * An unary expression.
 * 
 * @author Artur Rataj
 */
public class UnaryExpression extends AbstractExpression {
    /**
     * Type of this expression.
     */
    public enum Op implements AbstractOperator {
        // this type is not used in parsing, present as logically
        // opposite to NEGATION
        NONE,
        NEGATION,
        CONDITIONAL_NEGATION,
        BITWISE_COMPLEMENT,
        PRE_INCREMENT,
        PRE_DECREMENT,
        POST_INCREMENT,
        POST_DECREMENT,
        CAST,
        INSTANCE_OF,
        // unused in frontends
        INSTANCE_OF_NOT_NULL,
        // the following are only for property expressions
        A,
        E,
        X,
        F,
        G,
        Kc,
        Eg,
        Dg,
        Cg,
        NEG_Kc,
        NEG_Eg,
        NEG_Dg,
        NEG_Cg,
        Oc,
        Khc,
        NEG_Oc,
        NEG_Khc;
        
        /**
         * If this operator is boolean. Includes logic operators.
         * 
         * @return if boolean
         */
        public boolean isBoolean() {
            switch(this) {
                case CONDITIONAL_NEGATION:
                case A:
                case E:
                case X:
                case F:
                case G:
                case Kc:
                case Eg:
                case Dg:
                case Cg:
                case NEG_Kc:
                case NEG_Eg:
                case NEG_Dg:
                case NEG_Cg:
                case Oc:
                case Khc:
                case NEG_Oc:
                case NEG_Khc:
                    return true;
                    
                default:
                    return false;
            }
        }
        /**
         * Returns an opposite operator to this
         * one, that is, such an operator that
         * would give an opposite boolean value.
         * If this operator is not CONDITIONAL_NEGATION
         * or NONE a runtime exception is thrown.
         * 
         * @return                      relational operator
         *                              or null
         */
        public Op newOppositeBoolean() {
            Op o;
            switch(this) {
                case NONE:
                    o = CONDITIONAL_NEGATION;
                    break;

                case CONDITIONAL_NEGATION:
                    o = NONE;
                    break;

                default:
                    throw new RuntimeException("no boolean oppposite");
            }
            return o;
        }
        /**
         * Returns an operator, that is represented by a given string
         * representation.
         * 
         * @param s string representation
         * @return operator
         */
        public static UnaryExpression.Op parse(String s) {
            if(s.equals("(none)"))
                return UnaryExpression.Op.NONE;
            else if(s.equals("-"))
                return UnaryExpression.Op.NEGATION;
            else if(s.equals("!"))
                return UnaryExpression.Op.CONDITIONAL_NEGATION;
            else if(s.equals("A"))
                return UnaryExpression.Op.A;
            else if(s.equals("E"))
                return UnaryExpression.Op.E;
            else if(s.equals("X"))
                return UnaryExpression.Op.X;
            else if(s.equals("F"))
                return UnaryExpression.Op.F;
            else if(s.equals("G"))
                return UnaryExpression.Op.G;
            else if(s.equals("K"))
                return UnaryExpression.Op.Kc;
            else if(s.equals("Eg"))
                return UnaryExpression.Op.Eg;
            else if(s.equals("Dg"))
                return UnaryExpression.Op.Dg;
            else if(s.equals("Cg"))
                return UnaryExpression.Op.Cg;
            else if(s.equals("!K"))
                return UnaryExpression.Op.NEG_Kc;
            else if(s.equals("!Eg"))
                return UnaryExpression.Op.NEG_Eg;
            else if(s.equals("!Dg"))
                return UnaryExpression.Op.NEG_Dg;
            else if(s.equals("!Cg"))
                return UnaryExpression.Op.NEG_Cg;
            else if(s.equals("Oc"))
                return UnaryExpression.Op.Oc;
            else if(s.equals("Kh"))
                return UnaryExpression.Op.Khc;
            else if(s.equals("!Oc"))
                return UnaryExpression.Op.NEG_Oc;
            else if(s.equals("!Kh"))
                return UnaryExpression.Op.NEG_Khc;
            else
                throw new RuntimeException("unknown operator");
        }
        @Override
        public String toString() {
            switch(this) {
                case NONE:
                    return "(none)";

                case NEGATION:
                    return "-";

                case CONDITIONAL_NEGATION:
                    return "!";

                case BITWISE_COMPLEMENT:
                    return "~";

                case PRE_INCREMENT:
                    return "++";

                case PRE_DECREMENT:
                    return "--";

                case POST_INCREMENT:
                    return "++";

                case POST_DECREMENT:
                    return "--";

                case A:
                case E:
                case X:
                case F:
                case G:
                case Kc:
                case Eg:
                case Dg:
                case Cg:
                case NEG_Kc:
                case NEG_Eg:
                case NEG_Dg:
                case NEG_Cg:
                case Oc:
                case Khc:
                case NEG_Oc:
                case NEG_Khc:
                    String s = name();
                    String t = "";
                    // the property operators
                    if(s.startsWith("NEG_")) {
                        s = s.substring(4);
                        t = "!";
                    }
                    if(s.charAt(0) == 'K')
                        s = s.substring(0, s.length() - 1);
                    return t + s;
                case CAST:
                    return "()";
                    
                case INSTANCE_OF:
                    return "instanceof";
                    
                case INSTANCE_OF_NOT_NULL:
                    return "instanceofnotnull";
                    
                default:
                    throw new RuntimeException("unknown operator");
            }
        }
    };
    /**
     * Type of this expression.
     */
    public Op operatorType;
    /**
     * Type to which the subexpression is casted if
     * <code>operatorType == CAST</code>, type to
     * which object's type is compared if
     * <code>operatorType == INSTANCE_OF</code>, for
     * other operators null.
     */
    public Type objectType;
    /**
     * If not null, a coalition of type <code>AsCoalition</code>.
     * Used only by the property operators.
     */
    public Object coalition;
    /**
     * Subexpression.
     */
    public AbstractExpression sub;

    /**
     * Creates a new instance of UnaryExpression.
     * 
     * @param pos                       position in the parsed stream
     * @param outerScope                scope this expression belongs to
     * @param type                      type
     * @param sub                       subexpression
     */
    public UnaryExpression(StreamPos pos, BlockScope outerScope,
            Op operatorType, AbstractExpression sub) {
        super(pos, outerScope);
        this.operatorType = operatorType;
        this.objectType = null;
        this.sub = sub;
    }
    @Override
    public Object accept(Visitor v)  throws CompilerException {
        return v.visit(this);
    }
    @Override
    public Literal getSimpleConstant() {
        switch(operatorType) {
            case NEGATION:
                Literal l = sub.getSimpleConstant();
                if(l != null)
                    return l.neg();
                break;
                
            case NONE:
                return sub.getSimpleConstant();
                
        }
        return null;
    }
    /**
     * Returns a string representation of an operator.
     * 
     * @param operatorType              operator type
     * @param objectType                type, not null only if
     *                                  <code>operatorType == CAST</code> or
     *                                  <code>operatorType == INSTANCE_OF</code>
     * @return                          a two--element array with the
     *                                  prefix and suffix parts of the
     *                                  name, respectively
     */
    public static String[] getOperatorName(Op operatorType,
            Type objectType) {
        String pre = "";
        String post = "";
        switch(operatorType) {
            case NONE:
            case NEGATION:
            case CONDITIONAL_NEGATION:
            case BITWISE_COMPLEMENT:
            case PRE_INCREMENT:
            case PRE_DECREMENT:
            case A:
            case E:
            case X:
            case F:
            case G:
            case Kc:
            case Eg:
            case Dg:
            case Cg:
            case NEG_Kc:
            case NEG_Eg:
            case NEG_Dg:
            case NEG_Cg:
            case Oc:
            case Khc:
            case NEG_Oc:
            case NEG_Khc:
                pre = operatorType.toString();
                break;
                
            case POST_INCREMENT:
            case POST_DECREMENT:
                post = operatorType.toString();
                break;

            case CAST:
                pre = "(" + objectType.toNameString(false) + ")";
                break;

            case INSTANCE_OF:
            case INSTANCE_OF_NOT_NULL:
                pre = "[" + operatorType.toString() + " " + objectType.toNameString(false) + "]";
                break;
                
            default:
                throw new RuntimeException("unknown operator type: " +
                        operatorType);
        }
        String[] name = {
            pre,
            post,
        };
        return name;
    }
    @Override
    public UnaryExpression clone() {
        UnaryExpression copy = (UnaryExpression)super.clone();
        copy.sub = sub.clone();
        return copy;
    }
    /**
     * Returns prefixing and postfixing textual representaions,
     * separated by '#'.
     * 
     * @return encoded textual representation of operators; '#' should
     * be replaced with <code>sub</code>'s textual representation
     */
    public String opToString() {
        String[] name = getOperatorName(operatorType, objectType);
        if(Character.isLetter(name[0].charAt(name[0].length() - 1)) &&
                coalition == null) {
            name[0] = name[0] + "( ";
            name[1] = " )" + name[1];
        }
        String cPre;
        String cPost;
        if(coalition != null) {
            cPre = "(" + coalition.toString() + ", ";
            cPost = " )";
        } else {
            cPre = "";
            cPost = "";
        }
        return name[0] + cPre + "#" + cPost + name[1];
    }
    @Override
    public String toString() {
        String opString = opToString();
        int pos = opString.indexOf('#');
        return opString.substring(0, pos) +
                sub.toString() +
                opString.substring(pos + 1);
    }
}
