/*
 * SolitaryExpression.java
 *
 * Created on Aug 1, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.solitary;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.SemanticCheck;
import pl.gliwice.iitis.hedgeelleth.compiler.sa.StaticAnalysis;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.clazz.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.flags.MethodFlags;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.scope.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.CompilerException;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.ParseException;
import pl.gliwice.iitis.hedgeelleth.modules.MessageStyle;

/**
 * Evaluates an abstract expresion, that is outside any compilation hierarchy,
 * and refers only to a flat set of variables, some of them with values.
 * 
 * @author Artur Rataj
 */
public class SolitaryExpression {
    /**
     * A factory of <code>SemanticCheck</code>.
     */
    SemanticCheckFactory semanticFactory;
    /**
     * A factory of <code>StaticAnalysis</code>.
     */
    StaticAnalysisFactory staticFactory;
    /**
     * The solitary expression, for which this environment is created.
     */
    AbstractExpression solitary;
    /**
     * Method wrapping the solitary expression. Created in <code>parse()</code>.
     */
    Method method;
    /**
     * Variables keyed with their names. Created in <code>parse()</code>.
     */
    Map<String, Variable> variables;
    /**
     * To store the original variables, so that this object can restore the original state
     * of the evaluated expression.
     */
    Map<String, Variable> origVariables;
    
    /**
     * Creates a new environment for a solitary expression.<br>
     * 
     * The expression needs to have its block scope, whose
     * parent scope should be null or a method scope.
     * If the expression does not have a parent scope, it will be created.
     * 
     * @param expr a solitary expression
     * @param variables variables to add to the expression's
     * parent scope, null for none
     * @param semanticFactory a factory of a custom <code>SemanticCheck</code>,
     * null for the default
     * @param staticFactory a factory of a custom <code>StaticAnalysis</code>,
     * null for the default
     */
    public SolitaryExpression(AbstractExpression expr,
            Collection<Variable> variables, SemanticCheckFactory semanticFactory,
            StaticAnalysisFactory staticFactory) throws CompilerException {
        if(expr.outerScope.parent != null &&
                !(expr.outerScope.parent instanceof MethodScope))
            throw new RuntimeException("expressions' parent scope " +
                    "should either be null or a method scope");
        this.solitary = expr.clone();
        if(semanticFactory == null)
            semanticFactory = new SemanticCheckFactory();
        this.semanticFactory = semanticFactory;
        if(staticFactory == null)
            staticFactory = new StaticAnalysisFactory();
        this.staticFactory = staticFactory;
        if(variables == null)
            variables = new HashSet<>();
        parse(variables);
    }
    /**
     * If the expression already has a method scope, so this
     * class' constructor won't need to add one.<br>
     * 
     * Used to check, if variables need to be added to the scope,
     * or if they are already there.
     * 
     * @param expr
     * @return 
     */
    public static boolean hasMethodScope(AbstractExpression expr) {
        if(expr.outerScope.parent != null) {
            if(!(expr.outerScope.parent instanceof MethodScope))
                throw new RuntimeException("expressions' parent scope " +
                    "should either be null or a method scope");
            return true;
        }
        return false;
    }
    /**
     * Creates the map of variables.
     * 
     * @param sourceSet input list of variables, must have unique names
     * @param variables map to put the variables into
     * @param duplicatesReplace if duplicate variables replace the former
     */
    protected final void makeVariables(Collection<Variable> source,
            Map<String, Variable> variables, boolean duplicatesReplace) {
        for(Variable v : source) {
            if(!duplicatesReplace && variables.containsKey(v.name))
                throw new RuntimeException("duplicate name of a variable: " +
                        v.name);
            variables.put(v.name, v);
        }
    }
    /**
     * Parses the solitary expression.
     * 
     * @param variableList variables, possibly used by the solitary expression;
     * empty set for none
     */
    protected final void parse(Collection<Variable> variableList)
            throws CompilerException {
        try {
            try {
                Unit unit = new Unit(null, new TopScope(), null);
                unit.frontend = new SolitaryFrontend();
                unit.packageScope = new PackageScope(unit.topScope,
                        "wrapperPackage");
                AbstractJavaClass clazz = new ImplementingClass(
                        null, unit, "wrapperClass");
                method = new Method(null, clazz, "wrapperMethod",
                        new Type(), null, new MethodFlags(), null);
            } catch(ParseException e) {
                throw new RuntimeException("unexpected: " + e);
            }
            if(solitary.outerScope.parent == null)
                solitary.outerScope.parent = method.scope;
            else
                method.scope = (MethodScope)solitary.outerScope.parent;
            variables = method.scope.locals;
            origVariables = new HashMap<>(variables);
            makeVariables(variableList, variables, true);
            method.code = new Block(method.getStreamPos(),
                    method.scope);
            method.code.code.add(solitary);
            SemanticCheck check = semanticFactory.newInstance(
                    new SolitaryPrimaryTransformer());
            check.visit(method.code);
        } catch(CompilerException e) {
            MessageStyle.apply(MessageStyle.Type.VERICS,
                    null, e);
            throw e;
        } finally {
            method.scope.locals = origVariables;
        }
    }
    /**
     * Evaluates an expression, given a list of knowns.
     * 
     * @param knowns knowns; if null, only constant expressions
     * can be evaluated
     * @return value or null for unknown
     */
    public Literal evaluate(Map<Variable, Literal> knowns) 
            throws CompilerException {
        try {
            method.scope.locals = variables;
            StaticAnalysis sa = staticFactory.newInstance(method, knowns);
            Literal out = sa.getSolitaryConstants().get(0);
            sa.restore();
            return out;
        } catch(CompilerException e) {
            throw e;
        } finally {
            method.scope.locals = origVariables;
        }
    }
}
