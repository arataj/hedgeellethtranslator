/*
 * JavaFrontend.java
 *
 * Created on Apr 14, 2009, 2:04:29 PM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.solitary;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.BlockExpression;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.PWD;
import pl.gliwice.iitis.hedgeelleth.interpreter.*;
import pl.gliwice.iitis.hedgeelleth.interpreter.exception.InterpreterException;
import pl.gliwice.iitis.hedgeelleth.modules.AbstractFrontend;

/**
 * A frontend for evaluating <code>SolitaryExpression</code>.<br>
 *
 * @author Artur Rataj
 */
public class SolitaryFrontend extends AbstractFrontend {
    /**
     * Creates a new instance of JavaFrontend.
     */
    public SolitaryFrontend() {
        super();
    }
    @Override
    public void parse(Compilation compilation, PWD topDirectory,
            String libraryPath, String fileName, CompilerException parseErrors) {
        // empty
    }
    @Override
    public Class getSemanticCheckClass() {
        return SemanticCheck.class;
    }
    @Override
    public void semanticCheck(Compilation compilation, SemanticCheck sc,
            BlockExpression blockExpression, CompilerException compileErrors)
            throws CompilerException {
        // empty
    }
    @Override
    public MethodSignature getMainMethodSignature(String arrayClassName,
            String stringClassName) {
        return null;
    }
    @Override
    public void setMainMethodArgs(List<String> args, AbstractInterpreter interpreter,
            RuntimeThread thread) throws InterpreterException {
        // empty
    }
}
