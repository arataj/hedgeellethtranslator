/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.solitary;

import pl.gliwice.iitis.hedgeelleth.compiler.AbstractPrimaryTransformer;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.NameList;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.ParseException;

/**
 *
 * @author art
 */
public class SolitaryPrimaryTransformer extends AbstractPrimaryTransformer {
    @Override
    public void transform(PrimaryExpression e) throws ParseException {
        if(e.prefix.type == PrimaryPrefix.PrefixType.NAME) {
            String name = e.prefix.toString();
            if(!e.suffixList.isEmpty())
                throw new ParseException(e.suffixList.get(0).getStreamPos(),
                        ParseException.Code.PARSE,
                        "unparsable in a solitary expression");
            /*for(PrimarySuffix s : e.suffixList) {
                if(s.type != PrimarySuffix.PrefixType.NAME)
                    throw new ParseException(s.getStreamPos(),
                            ParseException.Code.PARSE,
                            "unparsable in a solitary expression");
                name += s.type.toString();
            }*/
            e.prefix = new PrimaryPrefix(e.getStreamPos(),
                    new NameList(name, null));
        }
    }
}
