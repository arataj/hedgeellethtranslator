/*
 * StaticAnalysisFactory.java
 *
 * Created on Apr 11, 2016
 *
 * Copyright (c) 2016  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.solitary;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.sa.StaticAnalysis;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.CompilerException;

/**
 * A factory of <code>StaticAnalysis</code>. Subclass to produce a
 * subclass  of <code>StaticAnalysis</code>.
 * 
 * @author Artur Rataj
 */
public class StaticAnalysisFactory {
    public StaticAnalysis newInstance(Method method,
            Map<Variable, Literal> knowns) throws CompilerException {
        StaticAnalysis sa = new StaticAnalysis(knowns);
        sa.checkSolitary(method);
        return sa;
    }
}
