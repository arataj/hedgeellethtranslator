/*
 * AbstractInterpreter.java
 *
 * Created on May 3, 2008
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.interpreter;

import java.util.*;
import java.lang.ref.WeakReference;

import pl.gliwice.iitis.hedgeelleth.Hedgeelleth;
import pl.gliwice.iitis.hedgeelleth.compiler.code.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.op.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.StreamPos;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.*;
import pl.gliwice.iitis.hedgeelleth.interpreter.exception.*;
import pl.gliwice.iitis.hedgeelleth.interpreter.op.BarrierParties;

import hc.TuplesIO;

/**
 * An abstract interpreter of the internal assembler. It calls an
 * abstract visitor method for each operation.<br>
 *
 * A return balue of a visitor method returns, whether the interpreter
 * should return from the current method. Typically only
 * <code>CodeOpReturn</code> returns true.<br>
 *
 * The interpreter has a list of pending threads. These are the threads
 * that were created, started but not yet executed. A next thread can be
 * executed if the previous finishes, there is no multitasking within
 * the threads.<br>
 * 
 * Classes are instantiated once per an instance of interpreter.
 * 
 * @author Artur Rataj
 */
public abstract class AbstractInterpreter implements CodeVisitor<Object,
        InterpretingContext> {
    /**
     * A marker position, put into <code>InterpreterException</code> that is
     * thrown because an operation with <code>@MODEL_WAIT_FINISH</code>
     * has been interpreted. The exception whould silently end interpreting of the
     * main thread.
     */
    public static final StreamPos STOP_INTERPRETER_POS = new StreamPos("<stop>");
    
    /**
     * Code compiled to the internal assembler.
     */
    public CodeCompilation compilation;
    /**
     * Runtime representations of classes. List list should normally be
     * acessed through the method <code>getRuntimeClass</code>, that
     * automatically loads a class if needed.
     */
    Map<CodeClass, RuntimeObject> classes;
    /**
     * A set of classes currently statically initialized. For detecting circular
     * references. Keyed with class name.
     */
    SortedMap<String, CodeClass> loadPending = new TreeMap<>();
    /**
     * The most nested of the chain of loaded classes. Null if
     * <code>loadPending</code> is empty.
     */
    CodeClass lastBeingLoaded = null;
    /**
     * Next reference number.
     */
    long nextReferenceNum = 1000;
    /**
     * Threads in this interpreter.
     */
    Threads threads;
    /**
     * Caches runtime values of literals within code constants.
     * Used to reduce allocations in getValue().
     * 
     * There is a limited value of such literals, so mappings
     * are never removed from this cache.
     */
    Map<Literal, RuntimeValue> codeConstantCache;
    /**
     * Counter of all executed operations within this interpreter.
     */
    long opCounter;
    /**
     * Limit of the counter of all executed operations within this
     * interpreter, or -1 for no limit.
     */
    long opCounterLimit;
    /**
     * Runtime of the special array class.
     */
    public RuntimeObject arrayClass;
    /**
     * Runtime of the special string class.
     */
    public RuntimeObject stringClass;
    /**
     * A set of of all created objects within this interpreter.<br>
     *
     * The runtime objects are wrapped into weak references,
     * so that they can be discarder when no longer needed by garbage
     * collector.
     */
    Set<WeakReference<RuntimeObject>> objects;
    /**
     * Optional range map. If not null, setting of arithmetic values
     * is checked against this map.
     */
    public TypeRangeMap typeRangeMap;
    /**
     * Any barriers with the number of threads specified manually using
     * <code>@BARRIER_ACTIVATE</code> are in this map.<br>
     * 
     * Empty for no barriers with predefined number of threads.
     */
    public Map<RuntimeObject, BarrierParties> barrierThreadsNum;
    /**
     * A model control object, specified by an interpreted thread, null
     * for none. Used in the main thread if PTAs are generated.
     */
    public ModelControl model;
    /**
     * Tuples/matrix input/output objects, created but not closed.
     * Keyed with names of runtime object, which are sorted for
     * repeated compiler output.
     */
    public SortedMap<String, TuplesIO> io;
    /**
     * Static variables, that are unknown constants.
     */
    public Set<CodeVariable> unknownConstants;

    /**
     * Creates a new instance of AbstractInterpreter, without operation
     * counter limit.
     * 
     * @param compilation               code compiled to the
     *                                  internal assembler
     */
    public AbstractInterpreter(CodeCompilation compilation) {
        this.compilation = compilation;
        classes = new HashMap<>();
        threads = new Threads();
        codeConstantCache = new HashMap<>();
        objects = new HashSet<>();
        typeRangeMap = null;
        io = new TreeMap<>();
        unknownConstants = new HashSet<>();
        barrierThreadsNum = new HashMap<>();
        resetOpCounter();
        setOpCounterLimit(-1);
        initSpecialClasses();
        // resetPTA();
    }
//    public void resetPTA() {
//    }
    /**
     * Creates runtime representation of the special classes.
     */
    private void initSpecialClasses() {
        try {
            if(compilation.arraysEnabled)
                arrayClass = getRuntimeClass(Type.ARRAY_QUALIFIED_CLASS_NAME);
            else
                arrayClass = null;
            if(compilation.stringsEnabled)
                stringClass = getRuntimeClass(Type.STRING_QUALIFIED_CLASS_NAME);
            else
                stringClass = null;
        } catch(InterpreterException e) {
            throw new InterpreterFatalException(
                    InterpreterFatalException.Category.TYPE_MISSING,
                    "could not get runtime of a special class: " + e.toString());
        }
    }
    /**
     * Resets the counter of all operations executed so far by this
     * interpreter.
     */
    public final void resetOpCounter() {
        opCounter = 0;
    }
    /**
     * Returns the number of all operations executed so far by this
     * interpreter.
     *
     * @return                          value of the counter of all
     *                                  executed operations
     */
    public long getOpCounter() {
        return opCounter;
    }
    /**
     * Sets the limit of the number of all operations that can be executed
     * by this interpreter, -1 for no limit. Exceeding the limit causes
     * an <code>InterpreterException</code> to be thrown.
     *
     * @param limit                     limit of the counter of all
     *                                  executed operations, -1 for none
     */
    public final void setOpCounterLimit(long limit) {
        opCounterLimit = limit;
    }
    /**
     * Resets the per--class static serial numbers of various objects used by
     * this interpreter.<br>
     *
     * It should normally be done only within compiler testing for repeatable
     * output files, and it is not safe if multiple interpreters are run at
     * once.<br>
     *
     * This method currently calls only <code>RuntimeObject.resetSerial()</code>.
     */
    public static void resetSerialCounters() {
        RuntimeObject.resetSerial();
    }
    /**
     * Checks if the index value fits into 32--bit integer. Otherwise,
     * an interpreter exception is thrown, as this implementation requires
     * indices to fit into 32-bit integers.
     *
     * @param index                     index to test
     */
    void checkIndexValue(long index) throws InterpreterException {
        if(index < 0)
            throw new InterpreterException(null, "negative array size");
        if(index > Integer.MAX_VALUE)
            throw new InterpreterException(null, "array size can not exceed the maximum " +
                    "of 32-bit integer in this implementation");
    }
    /**
     * Returns the code class of a type, given its name.
     *
     * @param type                      java class name
     * @return                          code class
     */
    public CodeClass getCodeClass(String javaClassName) {
        return compilation.classes.get(javaClassName);
    }
    /**
     * Returns the code class of a type.<br>
     *
     * Throws an unchecked <code>InterpreterFatalException</code>
     * with the category <code>TYPE_MISSING</code> if the
     * type is null.
     *
     * @param type                      type
     * @return                          code class
     */
    public CodeClass getCodeClass(Type type) {
        if(type.isNull())
            throw new InterpreterFatalException(
                    InterpreterFatalException.Category.TYPE_MISSING,
                    "the constant <null> does not have a type");
        String name = type.getJava().toString();
        return getCodeClass(name);
    }
    /**
     * Returns the code class of a reference.
     *
     * @param reference                 reference
     * @return                          code class
     */
    public CodeClass getCodeClass(RuntimeValue reference) {
        return getCodeClass(reference.type);
    }
    /**
     * Creates a new thread. This method just wraps
     * <code>threads.getNew()</code>.
     * 
     * @param object                    the thread object
     * @param mainMethodStarts          if true, the main method
     *                                  is set as the start one,
     *                                  otherwise, the method run()
     *                                  is set instead
     * @return                          new thread
     */
    public RuntimeThread newThread(RuntimeObject object,
            boolean mainMethodStarts) throws InterpreterException {
        return threads.getNew(object, mainMethodStarts);
    }
    /**
     * Creates a new thread, that begins with invoking
     * the run() method of a given object, and adds the
     * thread to the list of pending threads.<br>
     *
     * Usually used for creating all threads in the interpreter
     * except for the main one.
     *
     * @param runnable                  object whose run() method
     *                                  is the start point of the
     *                                  thread
     * @param rm                        runtime method that creates
     *                                  the thread
     */
    protected void newPendingThread(RuntimeValue runnable, RuntimeMethod rm)
            throws InterpreterException {
        RuntimeThread t = newThread(runnable.getReferencedObject(),
                false);
        List<RuntimeValue> args  = new LinkedList<>();
        args.add(runnable);
        t.startMethodArgs = args;
        rm.pendingThreads.add(t);
    }
    /**
     * Loads a class. Checks for possible circular static references.
     * 
     * If the class or its superclasses do not have their
     * runtime represention, they are created. The methods
     * <code>static*</code> of the loaded classes are
     * interpreted.<br>
     *
     * This method can throw an interpreter exception if a static
     * variable is initialized with a value outside the variable's
     * range.
     * 
     * @param clazz class to load
     * @return the newly loaded class
     */
    protected RuntimeObject loadClass(CodeClass clazz) throws InterpreterException {
        if(loadPending.containsKey(clazz.name))
            throw new RuntimeException("class is already being loaded");
        loadPending.put(clazz.name, clazz);
        CodeClass tmpLoaded = lastBeingLoaded;
        lastBeingLoaded = clazz;
        CodeClass superClass = clazz.extendS;
        if(superClass != null && classes.get(superClass) == null) {
            // superclass exists but not loaded,
            // load it now
            getRuntimeClass(superClass);
        }
        RuntimeObject object = new RuntimeObject(clazz);
        // interpret the static* method
        classes.put(clazz, object);
        RuntimeThread t = new RuntimeThread(object, -1,
                clazz.methods.get(RuntimeObject.
                STATIC_METHOD_SIGNATURE));
        interpret(t);
        // add to objects
        objects.add(new WeakReference(object));
        lastBeingLoaded = tmpLoaded;
        loadPending.remove(clazz.name);
        return object;
    }
    /**
     * <p>Returns a runtime representation of a class, holding
     * values of static fields declared within the class,
     * but not within the superclasses.</p>
     * 
     * <p>May require a call to <code>loadClass()</code>.</p>
     * 
     * @param clazz                     class
     * @return                          runtime represention of
     *                                  the class
     */
    public RuntimeObject getRuntimeClass(CodeClass clazz) throws InterpreterException {
        if(clazz == null)
             throw new RuntimeException("code class is null");
        if(loadPending.containsKey(clazz.name) &&
                // a class being loaded has the right to refer to itself
                clazz != lastBeingLoaded) {
            String s = "";
            for(String name : loadPending.keySet()) {
                if(!s.isEmpty())
                    s += ", ";
                s += name;
            }
            throw new InterpreterException(clazz.getStreamPos(),
                    "circular static reference between " + s);
        }
        RuntimeObject object = classes.get(clazz);
        if(object == null)
            object = loadClass(clazz);
        return object;
    }
    /**
     * Returns a runtime representation of a class, holding
     * values of static fields of the class.<br>
     * 
     * It is a convenience method, that calls
     * <code>getRuntimeClass(CodeClass)</code>.
     * See the called method's documentation for details.
     * 
     * @param name                      fully qualified name
     * @return                          runtime represention of
     *                                  the class
     * @see #getRuntimeClass(CodeClass)
     */
    public RuntimeObject getRuntimeClass(String name)
            throws InterpreterException {
        return getRuntimeClass(getCodeClass(name));
    }
    /**
     * Creates a new runtime value, being a reference to given
     * object. The reference's serial number is computed.
     * 
     * @param object                    object to be referred by
     *                                  the new reference
     * @return                          new object reference
     */
    public RuntimeValue newReference(RuntimeObject object) {
        long referenceNum = nextReferenceNum;
        ++nextReferenceNum;
        RuntimeValue reference = new RuntimeValue(object, referenceNum);
        return reference;
    }
    /**
     * Returns the field <code>objects</code> or the set's copy.
     * 
     * @return                          all objects created within
     *                                  this interpreter, that are used,
     *                                  and possibly some no longer used
     */
    public Set<WeakReference<RuntimeObject>> getObjects() {
        return objects;
    }
    /**
     * Creates a runtime instance of an object, that holds
     * values of non--static fields declared within the class
     * and its superclasses, if any. Not for array creation,
     * newArrays() is for that.<br>
     *
     * This method does not interpret any constructors, so
     * one has to be interpreted after this method is called,
     * for a full instantiation of the object. See the comment
     * in the code for details.
     * 
     * @param clazz                     code class
     * @param type                      type of the object to create,
     *                                  see <code>RuntimeObject.type</code>
     *                                  for details
     * @return                          reference to the created
     *                                  object
     */
    public RuntimeValue newRuntimeObject(RuntimeObject runtimeClass,
            Type type) throws InterpreterException {
        if(runtimeClass.context == Context.NON_STATIC)
            throw new RuntimeException("object not representing a class");
        if(runtimeClass.codeClass.name.equals(Type.ARRAY_QUALIFIED_CLASS_NAME))
            throw new RuntimeException("newRuntimeObject() can not be used for " +
                    "array creation");
        // CodeClass clazz = runtimeClass.codeClass;
        RuntimeObject object = new RuntimeObject(runtimeClass, type);
        // the methods <code>*object</code> are called by constructor, which
        // in turn is called by <code>CodeOpAllocation</code>, so, unlike in
        // <code>getRuntimeClass()</code>, no initialization code is ever executed
        // in this method
        //
        // add to objects
        objects.add(new WeakReference(object));
        return newReference(object);
    }
    /**
     * Creates a runtime instance of an object.<br>
     *
     * It is a convenience method, that calls
     * <code>newRuntimeObject(RuntimeObject)</code>.
     * See the called method's documentation for details.
     * 
     * @param clazz                     code class
     * @param type                      type of the object to create,
     *                                  see <code>RuntimeObject.type</code>
     *                                  for details
     * @return                          reference to the created
     *                                  object
     * @see #newRuntimeObject(RuntimeObject)
     */
    public RuntimeValue newRuntimeObject(CodeClass clazz, Type type)
            throws InterpreterException {
        RuntimeObject runtimeClazz = getRuntimeClass(clazz);
        return newRuntimeObject(runtimeClazz, type);
    }
    /*
     * Returns an existing runtime object.
     * 
     * @param reference                 reference to the object
     * @return                          runtime object
     */
    /*
    public RuntimeObject getRuntimeObject(RuntimeValue reference) {
        if(reference == null)
                throw new RuntimeException("reference is null");
        RuntimeObject object = objects.get(reference).get();
        if(object == null)
                throw new RuntimeException("object not found");
        return object;
    }
     */
    /**
     * Sets the runtime value of a variable referred by a given
     * dereference.
     *
     * If null dereference, throws <code>InterpreterException</code>.
     * There is a convenience method <code>setValueUnchecked</code>
     * that throws an unechecked exception in such a case.
     * 
     * @param dereference               dereference
     * @param value                     value to set
     * @param rm                        current runtime method
     */
    public void setValue(AbstractCodeDereference dereference, RuntimeValue value,
            RuntimeMethod rm) throws InterpreterException {
        // add runtime type check
        if(dereference.object == null) {
            // object is null, then it is <code>CodeFieldDereference</code>
            CodeFieldDereference f = (CodeFieldDereference)dereference;
            // a local or static field
            if(f.variable.isLocal()) {
                // it is a local of current runtime method
                rm.setValue(this, rm, f.variable, value);
            } else {
                // it is a static field
                CodeClass owner = (CodeClass)f.variable.owner;
                RuntimeObject runtimeClass = getRuntimeClass(owner);
                runtimeClass.setValue(this, rm, f.variable, value);
            }
        } else if(dereference instanceof CodeFieldDereference) {
            // it is a non--static field
            CodeFieldDereference f = (CodeFieldDereference)dereference;
            RuntimeObject runtimeObject =
                    // object dereference does not have a range
                    getValue(new RangedCodeValue(
                    new CodeFieldDereference(
                        f.object)), rm).getReferencedObject();
            if(runtimeObject == null)
                throw new InterpreterException(null, "null dereference");
            runtimeObject.setValue(this, rm, f.variable, value);
        } else {
            // it is an array dereference
            CodeIndexDereference i = (CodeIndexDereference)dereference;
            RuntimeValue array = getValue(
                    // object dereference does not have a range
                    new RangedCodeValue(
                    new CodeFieldDereference(
                        i.object)), rm);
            ArrayStore store = (ArrayStore)array.getReferencedObject();
            if(store == null)
                throw new InterpreterException(null, "null dereference");
            long index = getValue(i.index, rm).getMaxPrecisionInteger();
            checkIndexValue(index);
            store.setElement(this, rm, i, (int)index, value);
        }
    }
    /**
     * It wraps <code>setValue</code>, replacing possible
     * <code>InterpreterException</code> with unchecked
     * <code>InterpreterFatalException</code>, giving it the
     * category <code>UNCHECKED_NULL_DEREFERENCE</code>.<br>
     *
     * Use when it is expected that null dereference should
     * never happen.
     *
     * @param dereference               dereference
     * @param value                     value
     * @param rm                        current runtime method
     */
    public void setValueUnchecked(AbstractCodeDereference dereference, RuntimeValue value,
            RuntimeMethod rm) {
        try {
            setValue(dereference, value, rm);
        } catch(InterpreterException e) {
            throw new InterpreterFatalException(
                    InterpreterFatalException.Category.UNCHECKED_NULL_DEREFERENCE,
                    "unexpected error: " + e.toString());
        }
    }
    /**
     * Returns the runtime container of a given variable
     * dereference.<br>
     * 
     * For locals it is a runtime method, for fields it is a
     * runtime object.<br>

     * This method can throw an interpreter exception if some static
     * variable of the container's runtime classs is initialized
     * with a value outside the variable's primitive range.
     * 
     * @param variable                  dereference
     * @param rm                        current runtime method
     * @return                          runtime container
     */
    public AbstractRuntimeContainer getContainer(AbstractCodeDereference r,
            RuntimeMethod rm) throws InterpreterException {
        AbstractRuntimeContainer c;
        if(r instanceof CodeFieldDereference) {
            CodeFieldDereference f = (CodeFieldDereference)r;
            if(f.object == null) {
                // a local or a static field
                if(f.variable.isLocal())
                    // it is a local of current runtime method
                    c = rm;
                else {
                    // it is a static field
                    CodeClass owner = (CodeClass)f.variable.owner;
                    c = getRuntimeClass(owner);
                }
            } else
/*{if(r.toString().indexOf("tick") != -1)
    r = r;*/
                // it is a non--static field
                c = getContainer(new CodeFieldDereference(r.object), rm).
                        getValue(this, rm, r.object).getReferencedObject();
/*}*/
        } else if(r instanceof CodeIndexDereference) {
            CodeIndexDereference i = (CodeIndexDereference)r;
            c = getContainer(new CodeFieldDereference(i.object), rm);
        } else
            throw new RuntimeException("invalid dereference");
        return c;
    }
    /**
     * Returns the runtime value of a given code value.<br>
     *
     * If null dereference occurs, <code>InterpreterException</code>
     * is thrown. There is a convenience method <code>getValueUnchecked</code>
     * that throws an unchecked exception in such a case.<br>
     *
     * If the value does not have a runtime value because of
     * the lack of initialization, unchecked
     * <code>InterpreterFatalException</code> is thrown with
     * category <code>UNINITIALIZED_VARIABLE</code>.
     * 
     * @param value                     ranged code value
     * @param rm                        current runtime method, if null,
     *                                  then requests for any locals will
     *                                  cause a runtime exception
     * @param allowUnknownConstant      allows the value to be an
     *                                  unknown constant; the returned value is
     *                                  meaningless in the case of an unknown
     *                                  constant
     * @return                          runtime value
     */
    public RuntimeValue getValue(RangedCodeValue value, RuntimeMethod rm,
            boolean allowUnknownConstant) throws InterpreterException {
        if(value == null)
            throw new RuntimeException("value is null");
        RuntimeValue l;
        if(value.value instanceof CodeConstant) {
            Literal codeLiteral = ((CodeConstant)value.value).value;
            l = codeConstantCache.get(codeLiteral);
            if(l == null) {
                l = new RuntimeValue(codeLiteral);
                codeConstantCache.put(codeLiteral, l);
            }
        } else if(value.value instanceof CodeFieldDereference) {
            CodeFieldDereference r =
                    (CodeFieldDereference)value.value;
            if(!allowUnknownConstant && unknownConstants.contains(r.variable))
                throw new InterpreterException(null,
                        "constant undefined but required by the interpreter: " +
                        r.variable.name);
            if(r.object == null) {
                // a local or a static field
                if(r.variable.isLocal()) {
                    // it is a local of current runtime method
                    if(rm == null)
                        throw new InterpreterFatalException(
                                InterpreterFatalException.Category.LOCAL_BUT_NO_RM,
                                "request for " + r.variable.toString() + " but no runtime method");
                    l = rm.getValue(this, rm, r.variable);
                    if(l == null)
                        throw new InterpreterFatalException(
                                InterpreterFatalException.Category.UNINITIALIZED_VARIABLE,
                                "uninitialized local variable " +r.variable.toString());
                } else {
                    // it is a static field
                    CodeClass owner = (CodeClass)r.variable.owner;
                    RuntimeObject runtimeClass = getRuntimeClass(owner);
                    l = runtimeClass.getValue(this, rm, r.variable);
                    if(l == null)
                        throw new InterpreterFatalException(
                                InterpreterFatalException.Category.
                                    UNINITIALIZED_VARIABLE,
                                "uninitialized static field variable " + r.variable.toString());
                }
            } else {
                // it is a non--static field
                RuntimeValue reference = getValue(
                        // object reference has no primitive range
                        new RangedCodeValue(
                            new CodeFieldDereference(r.object)), rm,
                        allowUnknownConstant);
                if(reference == null)
                    throw new InterpreterFatalException(
                            InterpreterFatalException.Category.
                                UNINITIALIZED_VARIABLE,
                            "uninitialized variable " +r.object.toString());
                if(reference.value instanceof String) {
                    // a string object represented not by a
                    // reference, but by a direct constant
                    if(value.value.toNameString().equals(Method.LOCAL_THIS +
                            "::" + stringClass.codeClass.listOfFields.get(0).name))
                        l = new RuntimeValue(reference.getString().length());
                    else
                        throw new RuntimeException("special class String has no such field");
                } else {
                    RuntimeObject runtimeObject = reference.getReferencedObject();
                    if(runtimeObject == null)
                        throw new InterpreterException(null, "null dereference of " +
                                r.object.toString());
                    if(!runtimeObject.codeClass.isEqualToOrExtending((CodeClass)r.variable.owner))
                        throw new InterpreterException(null, "invalid class cast: " +
                                runtimeObject.codeClass.name + " to " +
                                ((CodeClass)r.variable.owner).name);
                    l = runtimeObject.getValue(this, rm, r.variable);
                    if(l == null)
                        throw new InterpreterFatalException(
                                InterpreterFatalException.Category.
                                    UNINITIALIZED_VARIABLE,
                                "uninitialized non--static field " + r.variable.toString());
                }
            }
        } else {
            // array dereference
            CodeIndexDereference i = (CodeIndexDereference)value.value;
            if(!allowUnknownConstant && unknownConstants.
                    contains(i.getDereferencingVariable()))
                throw new InterpreterException(null,
                        "constant undefined but required by the interpreter: " +
                        i.getDereferencingVariable().name);
            ArrayStore store = (ArrayStore)getValue(
                    new RangedCodeValue(
                    new CodeFieldDereference(
                        i.object)), rm, allowUnknownConstant).
                    getReferencedObject();
            if(store == null)
                throw new InterpreterException(null, "null dereference");
            long index = getValue(i.index, rm, allowUnknownConstant).
                    getMaxPrecisionInteger();
            checkIndexValue(index);
            l = store.getElement(this, rm, i, (int)index);
        }
        if(value.range != null)
            value.range.check(l, this, rm);
        return l;
    }
    /**
     * <p>Calls <code>getValue()</code>, does not allow for unknown
     * constants.</p>
     *
     * <p>This is a convenience method.</p>
     * 
     * @param value ranged code value
     * @param rm current runtime method; if null, then requests for any locals
     * will cause a runtime exception
     * @return runtime value
     */
    public RuntimeValue getValue(RangedCodeValue value, RuntimeMethod rm)
            throws InterpreterException {
        return getValue(value, rm, false);
    }
    /**
     * If a value contains object reference, returns the object.
     * If the reference is to null, returns null. If it is not
     * an object reference, throws
     * <code>InterpreterNoObjectOrNullException</code>, which is
     * a subclass of <code>InterpreterException</code>.<br>
     * 
     * The method deals with <code>null</code> being either a
     * constant or a dereference to null.
     * 
     * @return
     */
    public RuntimeObject getReferencedOrNull(Literal value)
            throws InterpreterException {
        if(value.isNull())
            // null constant
            return null;
        else if(value instanceof RuntimeValue) {
            RuntimeValue r = (RuntimeValue)value;
            if(r.isNull())
                // reference to null
                return null;
            else if(r.type.isJava())
                // reference to object
                return r.getReferencedObject();
        }
        throw new InterpreterNoObjectOrNullException(
                "neither an object reference nor null");
    }

    /**
     * It wraps <code>getValue</code>, replacing possible
     * <code>InterpreterException</code> with unchecked
     * <code>InterpreterFatalException</code>, giving it the
     * category <code>WRAPPED_EXCEPTION</code>.<br>
     *
     * Use when it is expected that no error should happen.
     *
     * @param value                     ranged code value
     * @param rm                        current runtime method
     * @param allowUnknownConstant      allows the value to be an
     *                                  unknown constant; the returned value is
     *                                  meaningless in the case of an unknown
     *                                  constant
     * @return                          value
     */
    public RuntimeValue getValueUnchecked(RangedCodeValue value,
            RuntimeMethod rm, boolean allowUnknownConstant) {
        try {
            return getValue(value, rm, allowUnknownConstant);
        } catch(InterpreterException e) {
            throw new InterpreterFatalException(
                    InterpreterFatalException.Category.WRAPPED_EXCEPTION,
                    "unexpected error: " + e.toString());
        }
    }
    /**
     * <p>Calls <code>getValueUnchecked()</code>, does not allow
     * for unknown constants.</p>
     *
     * <p>This is a convenience method.</p>
     * 
     * @param value ranged code value
     * @param rm current runtime method
     * @return runtime value
     */
    public RuntimeValue getValueUnchecked(RangedCodeValue value,
            RuntimeMethod rm) {
        return getValueUnchecked(value, rm, false);
    }
    /**
     * Evaluates a list of code values. Does not allow for unknown constants.
     * 
     * @param list                      list of ranged code values
     * @param rm                        runtime method
     * @return                          list of runtime values
     */
    public List<RuntimeValue> getValues(List<RangedCodeValue> codeValues,
            RuntimeMethod rm) throws InterpreterException {
        List<RuntimeValue> runtimeValues = new ArrayList<>();
        for(RangedCodeValue v : codeValues) {
            RuntimeValue l = getValue(v, rm);
            runtimeValues.add(l);
        }
        return runtimeValues;
    }
    /**
     * Recursively allocates arrays, given the lengths in subsequent
     * dimensions. The leaf arrays are filled with default initialization
     * values, as specified by
     * <code>Hedgeelleth.newDefaultInitializerLiteral</code>.<br>
     *
     * Arrays differ from normal object in that their container
     * contains a store. Because array is not directly associated with any
     * variable, that is, various variables may reference an array,
     * then no variable name is by default associated with the array's
     * store. Thus, to increase readability, a postfix
     * <code>namePostfix</code> may be appended, that, for example,
     * might contain the name of the first variable to which the array's
     * reference is written.
     *
     * @param context                   context of interpreting, runtime
     *                                  method must be specified
     * @param type                      type of the root array
     * @param sizeValues                lengths in subsequent dimensions,
     *                                  must be of integer types
     * @param namePostfix               an optional textual postfix to
     *                                  add to the store's name, null
     *                                  for none
     * @return                          reference to the root array
     */
    public RuntimeValue newArrays(InterpretingContext context, Type type,
            List<RuntimeValue> sizeValues, String namePostfix) throws InterpreterException {
        RuntimeValue sizeValue = sizeValues.get(0);
        long size = sizeValue.getMaxPrecisionInteger();
        checkIndexValue(size);
        ArrayStore array = new ArrayStore(arrayClass, type, (int)size);
        // add to objects
        objects.add(new WeakReference(array));
        array.appendToName(namePostfix);
        RuntimeValue that = newReference(array);
        List<RuntimeValue> argValues = new LinkedList<>();
        // add the constructor's "this"
        argValues.add(that);
        // add array's length
        argValues.add(sizeValue);
        List<Type> constructorArgs = new LinkedList<>();
        // array size is a long int
        //constructorArgs.add(Hedgeelleth.getIndexingConstant(0,
        //        context.rm.method.owner.frontend).type);
        constructorArgs.add(new Type(Type.PrimitiveOrVoid.LONG));
        MethodSignature signature = new MethodSignature(null,
                new NameList(arrayClass.codeClass.name).last(), constructorArgs);
        CodeMethod arrayConstructor = arrayClass.codeClass.lookupMethod(signature);
        if(arrayConstructor == null)
            throw new RuntimeException("could not find array constructor " +
                    signature);
        interpret(context, arrayConstructor, argValues);
        if(context.stop)
            throw new RuntimeException("unexpected stop in an array constructor");
        // initialize contents of the array
        for(int index = 0; index < size; ++index) {
            List<RuntimeValue> sizeValuesTail = new LinkedList<>(sizeValues);
            sizeValuesTail.remove(0);
            Type elementType = new Type(type);
            elementType.decreaseDimension(null);
            RuntimeValue element;
            if(sizeValuesTail.isEmpty())
                // create a default initializer literal
                // !!! what if default is out of range?
                element = new RuntimeValue(Hedgeelleth.
                        newDefaultInitializerLiteral(elementType));
            else
                // create recursively subarrays
                element = newArrays(context,
                        elementType, sizeValuesTail,
                        namePostfix);
            array.setElement(null, null, null, index, element);
        }
        return that;
    }
    /**
     * Interprets a single operation. Override to modify the interpreter.<br>
     * 
     * This implementation interprets the following operations:
     * assignments, calls, allocations, returns. Other operations cause
     * an exception to be thrown.
     *
     * @param context                   context of interpreting, runtime
     *                                  method must be specified
     * @param op                        operation to interpret
     */
    protected void interpret(InterpretingContext context, AbstractCodeOp op)
                throws InterpreterException {
            try {
                if(opCounterLimit != -1) {
                        if(opCounter == opCounterLimit)
                            throw new InterpreterException(op.getStreamPos(),
                                    "limit of " + opCounterLimit +
                                    " operations interpreted reached, bailing out");
                        else if(opCounter > opCounterLimit)
                            throw new RuntimeException(op.getStreamPos() + ": " +
                                    "number of " + opCounterLimit +
                                    " operations interpreted exceeded despite limit: " +
                                    opCounter);
                }
                op.accept(context, this);
                ++opCounter;
            } catch(TranslatorException e) {
                throw (InterpreterException)e;
            }
    }
    /**
     * Execute a given method. This method finishes when the
     * executed method ends.<br>
     * 
     * The interpreted code may either start new threads
     * or return the list of pending threads.
     * 
     * @param context                   context of interpreting, runtime method
     *                                  specifies the caller, which is null
     *                                  if none
     * @param method                    method to execute
     * @param args                      arguments of the method, its size
     *                                  must be equal to the number of
     *                                  arguments
     * @param caller                    runtime method that called, or
     *                                  null for none
     * @return                          runtime method that is called
     */
    public RuntimeMethod interpret(InterpretingContext parentContext,
            CodeMethod method, List<RuntimeValue> args)
            throws InterpreterException {
        // make a copy as the field <code>rm<code> is modified
        InterpretingContext context = new InterpretingContext(parentContext);
        context.rm = new RuntimeMethod(method);
// if(method == null || method.args == null || args == null)
//     method = method;
        if(method.args.size() != args.size())
            throw new RuntimeException("bad number of arguments");
        Iterator<CodeVariable> i = method.args.iterator();
        for(RuntimeValue value : args) {
            CodeVariable a = i.next();
            CodeVariable aProto = method.argProtos.get(a);
            if(aProto != null)
                a = aProto;
            CodeFieldDereference local = new CodeFieldDereference(
                    a);
            setValue(local, value, context.rm);
        }
// if(method.code.ops.size() > 2 && method.signature.toString().indexOf("main") != -1)
//     method = method;
        // it is provided that there is at least the return operation
        context.index = 0;
        INTERPRET_METHOD:
        while(true) {
            AbstractCodeOp op  = method.code.ops.get(context.index);
            try {
                interpret(context, op);
            } catch(InterpreterException e) {
                if(e.getStreamPos() == STOP_INTERPRETER_POS || context.stop) {
                    if(context.interpretThreads)
                        throw new RuntimeException("STOP_INTERPRETER found, but " +
                                "other threads may await interpretation as " +
                                "InterpretingContext.interpretThreads is true");
                    parentContext.stop = true;
                    break INTERPRET_METHOD;
                }
                e.completePos(op.getStreamPos());
                e.addStackFrame(op);
                throw e;
            }
            if(context.stop) {
                parentContext.stop = true;
                break;
            }
            if(context.index == -1)
                break;
        }
        // this method returned
        if(context.interpretThreads) {
            for(RuntimeThread t : context.rm.pendingThreads)
                interpret(t, context.interpretThreads);
            context.rm.pendingThreads.clear();
        }
        return context.rm;
    }
    /**
     * Execute a thread. This method finishes when the
     * executed thread ends.<br>
     * 
     * The interpreted code may either start new threads
     * or return the list of pending threads.
     * 
     * @param thread                    the thread to execute
     * @param interpretThreads          true if to interpret possible
     *                                  threads started by the code,
     *                                  false to return these threads
     *                                  as pending instead
     * @return                          runtime od the thread's called
     *                                  start method
     */
    public RuntimeMethod interpret(RuntimeThread thread,
            boolean interpretThreads) throws InterpreterException {
        InterpretingContext context = new InterpretingContext(this,
                thread, interpretThreads, null, InterpretingContext.UseVariables.ALL);
        return interpret(context, thread.startMethod,
                thread.startMethodArgs);
    }
    /**
     * Execute a thread. This method finishes when the
     * executed thread ends.<br>
     *
     * The interpreted code may start new threads.
     * 
     * This is a convenience method.
     * 
     * @param thread                    the thread to execute
     * @return                          runtime od the thread's called
     *                                  start method
     */
    public RuntimeMethod interpret(RuntimeThread thread) throws InterpreterException {
        return interpret(thread, true);
    }
    /**
     * If the value is arithmetic, and its type is not that of <code>type</code>,
     * then the value is converted to <code>type</code>. Otherwise, the
     * value is returned as is. It is assumed that the code passed semantic check and
     * thus no precision loss is possible.
     * 
     * @param value value for possible type conversion
     * @param type type
     * @return either <code>value</code> or its converted equivalent
     */
    public static RuntimeValue convertArithmetic(RuntimeValue value, Type type) {
        // arithmetic value should be converted to the type of this store
        if(value.type.numericPrecision() != 0 && !value.type.equals(type)) {
            switch(type.getPrimitive()) {
                case BYTE:
                    return new RuntimeValue((byte)value.getMaxPrecisionInteger());
                    
                case CHAR:
                    return new RuntimeValue((char)value.getMaxPrecisionInteger());

                case SHORT:
                    return new RuntimeValue((short)value.getMaxPrecisionInteger());

                case INT:
                    return new RuntimeValue((int)value.getMaxPrecisionInteger());
                    
                case LONG:
                    return new RuntimeValue(value.getMaxPrecisionInteger());
                    
                case FLOAT:
                    return new RuntimeValue((float)value.getMaxPrecisionFloatingPoint());
                    
                case DOUBLE:
                    return new RuntimeValue(value.getMaxPrecisionFloatingPoint());

                default:
                    throw new RuntimeException("unsupported type");
            }
        } else
            return value;
    }
}
