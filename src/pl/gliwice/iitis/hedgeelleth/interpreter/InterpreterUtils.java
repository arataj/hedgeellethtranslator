/*
 * InterpreterUtils.java
 *
 * Created on Jun 23, 2015
 *
 * Copyright (c) 2015  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.interpreter;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.code.CodeClass;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.CompilerException;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.*;
import pl.gliwice.iitis.hedgeelleth.interpreter.exception.InterpreterException;

import hc.TuplesIO;

/**
 * Utility methods for interpreting.
 * 
 * @author Artur Rataj
 */
public class InterpreterUtils {
    /**
     * Interpretes the main method of a given class.
     * 
     * @param interpreter interpreter to use
     * @param clazz class, whose main method is to be run
     * @param runtimeArgs runtime arguments
     * @return runtime of the method run
     */
    public static RuntimeMethod interpret(AbstractInterpreter interpreter,
            CodeClass clazz, List<String> runtimeArgs)
            throws InterpreterException, CompilerException {
        RuntimeObject object = interpreter.getRuntimeClass(clazz);
        RuntimeThread thread = interpreter.newThread(object,
                true);
        // empty list of arguments
        thread.startMethod.owner.frontend.setMainMethodArgs(
                runtimeArgs, interpreter, thread);
        RuntimeMethod rm = interpreter.interpret(thread, false);
        if(!interpreter.io.isEmpty()) {
            CompilerException error = new CompilerException();
            for(TuplesIO io : interpreter.io.values()) {
                try {
                    io.close();
                } catch(java.io.IOException e) {
                    /* ignored -- missing close will be reported anyway */
                }
                error.addReport(new CompilerException.Report(null,
                        CompilerException.Code.IO,
                        "stream " + io.toString() +
                        " is not closed after the main thread finishes"));
            }
            throw error;
        }
        return rm;
    }
}
