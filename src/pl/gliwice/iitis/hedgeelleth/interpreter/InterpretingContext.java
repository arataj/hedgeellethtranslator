/*
 * InterpretingContext.java
 *
 * Created on Mar 7, 2009, 6:41:40 PM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.interpreter;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.interpreter.container.RuntimeMethod;

/**
 * Contains context of interpreting of a code operation.
 *
 * @author Artur Rataj
 */
public class InterpretingContext {
    public static enum UseVariables {
        NO,
        ALL,
        LOCAL
    }
    /**
     * Interpreter.
     */
    public AbstractInterpreter interpreter;
    /**
     * Thread.
     */
    public RuntimeThread thread;
    /**
     * True if to interpret possible threads started by the code,
     * false to return these threads as pending instead.
     */
    public boolean interpretThreads;
    /**
     * Current runtime method.
     */
    public RuntimeMethod rm;
    /**
     * If false, use the interpreter to evaluate only constants that exist directly in the
     * evaluated expressions; set to true if the runtime may contain.
     */
    public UseVariables useVariables;
    /**
     * Index of the next operation to be executed within the method, or -1
     * to indicate a return from the method.
     */
    public int index;
    /**
     * If thie interpreting should be stopped immediately.
     */
    public boolean stop;

    /**
     * Creates a new instance of InterpretingContext.
     *
     * @param interpreter               interpreter
     * @param thread                    thread
     * @param interpretThreads          true if to interpret possible
     *                                  threads started by the code,
     *                                  false to return these threads
     *                                  as pending instead
     * @param rm                        current runtime method
     * @param useVariables if false, use the interpreter to evaluate only constants that
     * exist directly in the evaluated expressions; set to true if the runtime may contain
     * bogus variable values
     */
    public InterpretingContext(AbstractInterpreter interpreter, RuntimeThread thread,
                boolean interpretThreads, RuntimeMethod rm, UseVariables useVariables) {
        this.interpreter = interpreter;
        this.thread = thread;
        this.interpretThreads = interpretThreads;
        this.rm = rm;
        this.useVariables = useVariables;
        stop = false;
    }
    /**
     * Creates a new instance of InterpretingContext. This is a copying
     * constructor, that makes a shallow copy.
     *
     * @param context                   context to copy
     */
    public InterpretingContext(InterpretingContext context) {
        interpreter = context.interpreter;
        thread = context.thread;
        interpretThreads = context.interpretThreads;
        rm = context.rm;
        useVariables = context.useVariables;
        stop = context.stop;
    }
    /**
     * Increases the operation index by one.
     */
    public void toNext() {
        ++index;
    }
}
