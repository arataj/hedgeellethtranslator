/*
 * ModelControl.java
 *
 * Created on Nov 8, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.interpreter;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.code.op.AbstractCodeOp;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.CompilerException;
import pl.gliwice.iitis.hedgeelleth.compiler.util.text.StringAnalyze;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.ArrayStore;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.RuntimeObject;

/**
 * Represents details about generating an output model file.<br>
 * 
 * Modified by the interpreter, read by the PTA backend. Not
 * used outside the backend.<br>
 * 
 * Runtime objects represent PTA threads.
 * 
 * @author Artur Rataj
 */
public class ModelControl {
    /**
     * Represents a single named model property. The property
     * is not formatted, but is represented by a raw text.
     */
    public static class Property {
        /**
         * Name of this property. Empty for none.
         */
        public String name;
        /**
         * Textual representation of the property.
         */
        public String property;
        
        /**
         * Creates a new named property.
         * 
         * @param name name, empty or null for none
         * @param property textual representation
         */
        protected Property(String name, String property) {
            if(name == null)
                name = "";
            this.name = name;
            this.property = property;
        }
        /*
        @Override
        public boolean equals(Object o) {
            return name.equals(((Property)o).name);
        }
        @Override
        public int compareTo(Object o) {
            return name.compareTo(((Property)o).name);
        }
        @Override
        public int hashCode() {
            int hash = 5;
            hash = 47 * hash + Objects.hashCode(this.name);
            return hash;
        }
         */
    };
    /**
     * Represents, what is returned by hc's library method
     * <code>Model.isStatistical()</code>.
     */
    protected boolean isStatisticalHint;
    /**
     * What to append verbatim to the output model file, if supported
     * by the backend. Empty for nothing.
     */
    protected StringBuilder appendVerbatim;
    /**
     * Prefix to prepend an element's name. Present if a custom naming
     * convention has benn specified for a given PTA.
     */
    protected Map<RuntimeObject, String> namePrefix;
    /**
     * Suffix to append to an element's name. Present if a custom naming
     * convention has benn specified for a given PTA.
     */
    protected Map<RuntimeObject, String> nameSuffix;
    /**
     * Subsequent model properties. Empty list for no properties.
     */
    protected List<Property> properties;
    /**
     * State names.
     */
    protected Map<RuntimeObject, Map<AbstractCodeOp,
            Set<String>>> state;
    /**
     * Players. Empty map for no players. Several objects with the same name
     * constitute a single player. 
     */
    protected Map<RuntimeObject, String> player;
    /**
     * List of player names, in the order of the first encounter.
     */
    protected List<String> playerList;
    
    public ModelControl() {
        appendVerbatim = new StringBuilder();
        namePrefix = new HashMap<>();
        nameSuffix = new HashMap<>();
        properties = new ArrayList<>();
        state = new HashMap<>();
        player = new HashMap<>();
        playerList = new LinkedList<>();
    }
//    /**
//     * A copying constructor.
//     * 
//     * @param orig model control object, clones collections, but not
//     * elements
//     */
//    public ModelControl(ModelControl orig) {
//        appendVerbatim = new StringBuilder(orig.appendVerbatim);
//        namePrefix = new HashMap<>(orig.namePrefix);
//        nameSuffix = new HashMap<>(orig.nameSuffix);
//        properties = new ArrayList<>(orig.properties);
//        state = new HashMap<>(orig.state);
//        player = new HashMap<>(orig.player);
//        playerList = new LinkedList<>(orig.playerList);
//    }
    /**
     * Sets the hint isStatistical.
     * 
     * @param statistical value to be assigned to the field
     * <code>isStatisticalHint</code>
     */
    public void setStatisticalHint(boolean statistical) {
        isStatisticalHint = statistical;
    }
    /**
     * Returns a value of the hint isStatistical.
     * 
     * @return value of the field  <code>isStatisticalHint</code>
     */
    public boolean isStatisticalHint() {
        return isStatisticalHint;
    }
    /**
     * Empties <code>appendVerbatim</code>.
     */
    public void removeVerbatim() {
        appendVerbatim = new StringBuilder();
    }
    /**
     * Prefixes <code>appendVerbatim</code> with a given text.
     * 
     * @param text text to add
     */
    public void prependVerbatim(String text) {
        appendVerbatim.insert(0, text);
    }
    /**
     * Appends given text to <code>appendVerbatim</code>.
     * 
     * @param text text to add
     */
    public void appendVerbatim(String text) {
        appendVerbatim.append(text);
    }
    /**
     * Returns, what to add verbatim to the end of the model file,
     * if possible. The returned string is trimmed of any empty lines before
     * and after it.
     * 
     * @return a string, empty for nothing to add
     */
    public String getVerbatimSuffix() {
        return StringAnalyze.trimEmptyLines(
                appendVerbatim.toString());
    }
    /**
     * Specifies a custom naming convention for elements of
     * a given PTA.
     * 
     * @param object object instance representing a PTA thread
     * @param prefix prefix
     * @param suffix suffix
     */
    public void setNamingConvention(RuntimeObject object, String prefix,
            String suffix) {
        if(object == null)
            throw new RuntimeException("naming convention can not be set on null");
        namePrefix.put(object, prefix);
        nameSuffix.put(object, suffix);
    }
    /**
     * If a custom naming convention has been specified for a PTA.
     * 
     * @param object object instance representing a PTA thread
     * @return if a custom naming convention has been specified with
     * <code>setNamingConvention()</code>
     */
    public boolean hasNamingConvention(RuntimeObject object) {
        return namePrefix.keySet().contains(object);
    }
    /**
     * Specifies, how an element should be called given
     * custom naming conventions of some PTA. If a naming convention
     * was never registered with <code>object</code> using
     * <code>setNamingConvention()</code>, then
     * null is returned.
     * 
     * @param object object instance representing a PTA thread
     * @param element the element's name
     * @return name according to a convention
     */
    public String getElementName(RuntimeObject object, String element)
            throws CompilerException {
        if(element.isEmpty() && !(object instanceof ArrayStore))
            throw new RuntimeException("only an array is expected to be represented " +
                    "by an empty element");
        if(hasNamingConvention(object)) {
            String suffix = nameSuffix.get(object);
            if(!element.isEmpty() &&
                    Character.isDigit(element.charAt(element.length() - 1)) &&
                    !suffix.isEmpty() && Character.isDigit(suffix.charAt(0)))
                // separate two digits for readability
                element += "_";
            element = namePrefix.get(object) + element + suffix;
            if(element.isEmpty())
                throw new CompilerException(null, CompilerException.Code.ILLEGAL,
                        "naming convention for array " + object.getUniqueName() +
                        " resulted in an empty name");
            return element;
        } else
            return null;
    }
    /**
     * Clears all model properties.
     */
    public void clearProperties() {
        properties.clear();
    }
    /**
     * Adds a named model property.
     * 
     * @param name name, null or empty for none
     * @param property textual representation of the property
     */
    public void addProperty(String name, String property) {
        properties.add(new Property(name, property));
    }
    /**
     * Returns all model properties.
     * 
     * @return a list of subsequent named properties
     */
    public List<Property> getProperties() {
        return properties;
    }
    /**
     * Adds a state.
     * 
     * @param object object instance that represents a PTA thread
     * @param op operation, at which the state is located
     * @param name name of the state
     */
    public void addState(RuntimeObject object, AbstractCodeOp op,
            String name) {
        Map<AbstractCodeOp, Set<String>> map = state.get(object);
        if(map == null) {
            map = new HashMap<>();
            state.put(object, map);
        }
        Set<String> set = map.get(op);
        if(op == null) {
            set = new HashSet<>();
            map.put(op, set);
        }
        set.add(name);
    }
    /**
     * Returns a state name, if any.
     * 
     * @param object object instance that represents a PTA thread
     * @param op operation, at which the state is searched for
     * @param return set of state names, empty for no states defined
     * at a given location
     */
    public Set<String> getStateName(RuntimeObject object,
            AbstractCodeOp op) {
        Map<AbstractCodeOp, Set<String>> map = state.get(object);
        Set<String> out;
        if(map != null)
            out = map.get(op);
        else
            out = null;
        if(out == null)
            out = new HashSet<>();
        return out;
    }
    /**
     * Specifies a player's name. It can not be specified twice for
     * an object, or otherwise a runtime exception is thrown. More
     * objects with the same name constitute a single player.
     * 
     * @param object object instance representing a PTA thread
     * @param name a player's name
     */
    public void addPlayer(RuntimeObject object, String name) {
        if(isPlayer(object))
            throw new RuntimeException("duplicate definition");
        player.put(object, name);
        if(!playerList.contains(name))
            playerList.add(name);
    }
    /**
     * If player's name has already been specified for a given
     * object.
     * 
     * @param object object instance representing a PTA thread
     * @return if the objects already has a player's name
     */
    public boolean isPlayer(RuntimeObject object) {
        return player.keySet().contains(object);
    }
    /**
     * Returns players keyed with a name.
     * 
     * @return definition of players
     */
    public Map<String, Set<RuntimeObject>> getPlayers() {
        Map<String, Set<RuntimeObject>> players =
                new HashMap<>();
        for(String name : player.values()) {
            Set<RuntimeObject> participants = new HashSet<>();
            for(RuntimeObject p : player.keySet())
                if(player.get(p).equals(name))
                    participants.add(p);
            players.put(name, participants);
        }
        return players;
    }
    /**
     * Returns players in the order of declarations.
     * 
     * @return list of players, empty for no players
     */
    public List<String> getPlayerList() {
        return playerList;
    }
}
