/*
 * RuntimeThread.java
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.interpreter;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.interpreter.exception.InterpreterException;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.RuntimeObject;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.RuntimeMethod;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.CodeIndexDereference;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.flags.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.scope.BlockScope;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.scope.MethodScope;

/**
 * An interpreter's thread.<br>
 *
 * If you want a unique thread number to be automatically computed,
 * you may create all objects of this arrayType using the method
 * Threads.getNew(). The method is usually used for creating of
 * all threads but some temporary ones.<br>
 *
 * Start method arguments are defined in the list startMethodArgs,
 * the list is empty for no arguments. The arguments in the list
 * must match the arguments of the start method.<br>
 *
 * If the thread should be started using the main method,
 * the thread's object is expected to contain a method
 * <code>main(String[])</code>.<br>.
 *
 * If the thread should not be started using the main method,
 * the thread's object is expected to contain a method <code>run()</code>,
 * and also an optional field <code>runnable</code>, if exists,
 * is used by <code>getRunClass</code>.
 *
 * @author Artur Rataj
 */
public class RuntimeThread {
    /**
     * Signature of a run() method.
     */
    public static final MethodSignature RUN_METHOD_SIGNATURE =
            new MethodSignature(null, "run", new LinkedList<Typed>());
    
    /**
     * Object of this thread.
     */
    public RuntimeObject object;
    /**
     * Name of this thread.
     */
    public String name;
    /**
     * Start method of this thread. It is the main method for the main thread, or
     * the run method for other threads.
     */
    public CodeMethod startMethod;
    /**
     * Arguments of the start method. By default it is an empty list.
     */
    public List<RuntimeValue> startMethodArgs;
    /**
     * Stack. UNUSED.
     */
    Stack<RuntimeMethod> _stack;
    
    /**
     * Creates a new instance of interpreter thread.<br>
     * 
     * The start method is undefined, and it does not have any
     * arguments.<br>
     * 
     * Start method arguments are en empty list.<br>
     * 
     * @param object                    object of this thread
     * @param num                       number of this thread
     *                                  to differentiate it
     *                                  from other threads, for
     *                                  temporary threads usually
     *                                  -1
     * @see Threads.getNew
     */
    public RuntimeThread(RuntimeObject object, int num) {
        this.object = object;
        name = RuntimeThread.getRunClass(object,
                false) + "_" + num;
        startMethodArgs = new LinkedList<RuntimeValue>();
    }
    /**
     * Creates a new instance of interpreter thread.<br>
     * 
     * Start method arguments are an ampty list.
     * 
     * @param object                    object of this thread
     * @param num                       number of this thread
     *                                  to differentiate it
     *                                  from other threads, for
     *                                  temporary threads usually
     *                                  -1
     * @param startMethod               the start method
     * 
     * @see Threads.getNew
     * @see #setMainMethodArg
     */
    public RuntimeThread(RuntimeObject object, int num,
            CodeMethod startMethod) {
        this(object, num);
        this.startMethod = startMethod;
    }
    /**
     * Creates a new instance of interpreter thread.<br>
     * 
     * Start method arguments are an empty list.
     * 
     * @param object                    object of this thread
     * @param num                       number of this thread
     *                                  to differentiate it
     *                                  from other threads, for
     *                                  temporary threads usually
     *                                  -1
     * @param mainMethodStarts          if true, the main method
     *                                  is set as the start one,
     *                                  otherwise, the method run()
     *                                  is set instead and the object
     *                                  must be java.lang.Thread or
     *                                  its subclass
     * @see Threads.getNew
     * @see #setMainMethodArg
     */
    public RuntimeThread(RuntimeObject object, int num,
            boolean mainMethodStarts) throws InterpreterException {
        this(object, num);
        name = RuntimeThread.getRunClass(object,
                mainMethodStarts) + "_" + num;
        startMethod = getRunMethod(object, mainMethodStarts);
        if(startMethod == null)
            if(mainMethodStarts)
                throw new InterpreterException(null, "can not find main method");
            else
                throw new InterpreterException(null, "can not find run() method");
    }
    /**
     * Returns the start method.
     * 
     * @param object                    the thread's object
     * @param mainMethodStarts          if true, the main method
     *                                  is set as the start one,
     *                                  otherwise, the method run()
     *                                  is set instead and the object
     *                                  must be java.lang.Thread or
     *                                  its subclass
     * @return                          a start method
     */
    protected static CodeMethod getRunMethod(RuntimeObject object,
            boolean mainMethodStarts) throws InterpreterException {
        CodeClass clazz = object.codeClass;
        CodeClass c = clazz;
        if(mainMethodStarts)
            return c.mainMethod;
        else
            return c.lookupMethod(RUN_METHOD_SIGNATURE);
    }
    /**
     * Returns a class that has possibly invoked the thread's
     * start method, provided that
     * the thread's own <code>run</code> method either calls
     * <code>runnable.run()</code> or is overridden.
     *
     * The class returned is the
     * thread's object class unless both of these happen:
     * (1) the main method of the threads' object is not the start one
     * of the thread, and (2) the field
     * <code>runnable</code>, if any, does not
     * represent the constant "null".<br>
     *
     * This method is usually only used for things like naming the
     * thread with a verbose name. If
     *
     * @param object                    thread's object
     * @param mainMethodStarts          if true, the main method
     *                                  is set as the start one,
     *                                  otherwise, the method run()
     *                                  is set instead and the object
     *                                  must be java.lang.Thread or
     *                                  its subclass
     * @return                          a class with possible
     *                                  run method
     */
    protected static String getRunClass(RuntimeObject object,
            boolean mainMethodStarts) {
        CodeClass clazz = object.codeClass;
        if(mainMethodStarts)
            return clazz.name;
        else {
            CodeVariable v = clazz.lookupField("runnable");
            // <code>runnable</code> is of a Java type, thus,
            // it does not have a primitive bound and the
            // interpreter is not required to get the value
            RuntimeValue runnable = object.getValue(null, null, v);
            // the field "runnable" is not required to exist
            if(runnable != null && !runnable.isNull())
                return runnable.getReferencedObject().codeClass.name;
            else
                return clazz.name;
        }
    }
    @Override
    public String toString() {
        return name;
    }
}
