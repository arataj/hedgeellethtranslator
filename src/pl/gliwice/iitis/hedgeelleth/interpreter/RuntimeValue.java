/*
 * RuntimeValue.java
 *
 * Created on Jan 3, 2008, 11:22:26 AM
 *
 * Copyright (c) 2007 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.interpreter;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.interpreter.container.RuntimeObject;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.AbstractRuntimeContainer;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;

/**
 * Runtime value -- a literal with added functionality of
 * runtime object reference.
 * 
 * @author Artur Rataj
 */
public class RuntimeValue extends Literal {
    /**
     * If this object is a runtime reference, then the reference number,
     * otherwise -1.
     */
    long referenceNum;

    /**
     * Creates a new instance of RuntimeValue, type runtime object
     * reference.
     * 
     * @param object                    the referenced object
     * @param num                       runtime number of the reference,
     *                                  must be &gt;= 0
     */
    public RuntimeValue(RuntimeObject object, long num) {
        super();
        value = object;
        type = new Type(object.type);
        if(num < 0)
            throw new RuntimeException("negative reference number");
        this.referenceNum = num;
    }
    /**
     * Creates a new instance of RuntimeValue, on the basis of a
     * literal.
     * 
     * @param l                         literal
     */
    public RuntimeValue(Literal l) {
        super(l);
        referenceNum = -1;
    }
    /**
     * Creates a new instance of RuntimeValue, type INTEGER.
     *
     * @param value                     integer value
     */
    public RuntimeValue(int value) {
        super(value);
        referenceNum = -1;
    }
    /**
     * Creates a new instance of RuntimeValue, type BYTE.
     *
     * @param value                     byte value
     */
    public RuntimeValue(byte value) {
        super(value);
        referenceNum = -1;
    }
    /**
     * Creates a new instance of RuntimeValue, type SHORT.
     *
     * @param value                     short value
     */
    public RuntimeValue(short value) {
        super(value);
        referenceNum = -1;
    }
    /**
     * Creates a new instance of RuntimeValue, type LONG.
     *
     * @param value                     long value
     */
    public RuntimeValue(long value) {
        super(value);
        referenceNum = -1;
    }
    /**
     * Creates a new instance of RuntimeValue, type FLOAT.
     *
     * @param value                     floating value
     */
    public RuntimeValue(float value) {
        super(value);
        referenceNum = -1;
    }
    /**
     * Creates a new instance of RuntimeValue, type DOUBLE.
     *
     * @param value                     double value
     */
    public RuntimeValue(double value) {
        super(value);
        referenceNum = -1;
    }
    /**
     * Creates a new instance of RuntimeValue, type CHARACTER.
     *
     * @param value                     character value
     */
    public RuntimeValue(char value) {
        super(value);
        referenceNum = -1;
    }
    /**
     * Creates a new instance of RuntimeValue, type String.
     *
     * @param value                     string value
     * @param stringClassName           name of the special string class,
     *                                  null for that defined in <code>Type</code>
     */
    public RuntimeValue(String value, String stringClassName) {
        super(value, stringClassName);
        referenceNum = -1;
    }
    /**
     * Creates a new instance of RuntimeValue, type BOOLEAN.
     *
     * @param value                     boolean value
     */
    public RuntimeValue(boolean value) {
        super(value);
        referenceNum = -1;
    }
    /**
     * Creates a new instance of RuntimeValue, type NULL.
     */
    public RuntimeValue() {
        super();
        referenceNum = -1;
    }
    /**
     * A copying constructor. Should be used only in code constants.
     * 
     * @param v                         runtime value to copy
     */
    public RuntimeValue(RuntimeValue v) {
        super(v);
        referenceNum = v.referenceNum;
    }
    /**
     * Returns the referenced object or null if it is a null
     * literal. Throw a runtime exception if not an object reference.
     * 
     * @return                          runtime object or null
     */
    public RuntimeObject getReferencedObject() {
        if(value instanceof RuntimeObject)
            return (RuntimeObject)value;
        else if(type.isNull())
            return null;
        else
            throw new RuntimeException("not an object reference");
    }
    /**
     * Returns the number of a runtime method reference.
     * 
     * @return                          long value
     */
    public long getObjectReferenceNum() {
        if(referenceNum != -1)
            return referenceNum;
        else
            //throw new RuntimeException("not an object reference");
            return -9999999;
    }
    @Override
    public boolean equals(Object o) {
        boolean equals = true;
        if(!(o instanceof RuntimeValue))
            equals = false;
        else {
            RuntimeValue v = (RuntimeValue)o;
            if(referenceNum != v.referenceNum)
                equals = false;
        }
        return equals;
    }
    @Override
    public int hashCode() {
        int hash = super.hashCode();
        hash = 23 * hash + (int)this.referenceNum;
        return hash;
    }
    /**
     * Prints this object along with its contents, detects recursive
     * references.
     * 
     * @param stack                     objects already printed, to
     *                                  detect repeated references,
     *                                  null for none
     * @return
     */
    public String toString(Set<AbstractRuntimeContainer> alreadyPrinted) {
        if(alreadyPrinted == null)
            alreadyPrinted = new HashSet<>();
        if(value instanceof RuntimeObject) {
            RuntimeObject ro = (RuntimeObject)value;
            // a runtime object reference
            String s;
            if(alreadyPrinted.contains(ro))
                s = "<repeated>";
            else {
                alreadyPrinted.add(ro);
                s = ro.toString(alreadyPrinted);
            }
            return "#" + getObjectReferenceNum() + " -> " + s;
                    
        } else
            return super.toString();
    }
    @Override
    public Literal clone() {
        RuntimeValue copy = (RuntimeValue)super.clone();
        copy.referenceNum = referenceNum;
        return copy;
    }
    @Override
    public String toString() {
        return toString(null);
    }
}
