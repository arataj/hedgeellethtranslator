/*
 * TADDThreadFactory.java
 *
 * Created on Apr 21, 2008
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.interpreter;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.interpreter.container.RuntimeObject;
import pl.gliwice.iitis.hedgeelleth.interpreter.exception.InterpreterException;

/**
 * Threads of a single interpreter.
 * 
 * @author Artur Rataj
 */
public class Threads {
    /**
     * Serial numbers of new threads, the key is a thread class name.
     * If a value does not exists, it implies a serial number 0.
     */
    Map<String, Integer> threadSerialNum;
    
    /**
      * Creates a new instance of Threads.
      */
    public Threads() {
        threadSerialNum = new HashMap<>();
    }
    /**
     * Creates a new thread. Method arguments must be set separately.
     * Usually used for all threads but the temporary ones.
     * 
     * @param object                    the thread object
     * @param mainMethodStarts          if true, the main method
     *                                  is set as the start one,
     *                                  otherwise, the method run()
     *                                  is set instead
     * @return                          new thread
     */
    public RuntimeThread getNew(RuntimeObject object,
            boolean mainMethodStarts) throws InterpreterException {
        String name = RuntimeThread.getRunClass(object,
                mainMethodStarts);
        if(!threadSerialNum.containsKey(name))
            threadSerialNum.put(name, 0);
        int serialNum = threadSerialNum.get(name);
        threadSerialNum.put(name, serialNum + 1);
        RuntimeThread thread = new RuntimeThread(object, serialNum,
                mainMethodStarts);
        return thread;
    }
}
