/*
 * RuntimeContainer.java
 *
 * Created on May 8, 2008
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.interpreter.container;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.interpreter.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.CodeVariable;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.CodeFieldDereference;
import pl.gliwice.iitis.hedgeelleth.compiler.primitiverange.CodeVariablePrimitiveRange;
import pl.gliwice.iitis.hedgeelleth.interpreter.exception.InterpreterException;

/**
 * An abstract  runtime container can hold values of variables.
 * 
 * If the container is a runtime object then the values are
 * non--static fields. If the container is a runtime method then
 * the values are local variables.
 * 
 * @author Artur Rataj
 */
abstract public class AbstractRuntimeContainer {
    /**
     * Values, the key is the variable. A missing mapping means a
     * yet not initialized field.
     */
    public Map<CodeVariable, RuntimeValue> values;
    
    /**
     * Creates a new instance of RuntimeContainer.
     */
    public AbstractRuntimeContainer() {
        values = new HashMap<>();
    }
    /**
     * Sets a value to a variable, that belongs to this container. Primitive
     * bound checking is performed, and if it fails,
     * <code>InterpreterException</code> is thrown.
     *
     * @param interpreter               used by <code>checkPrimitiveBound</code>,
     *                                  see that method's docs for details
     * @param rm                        used by <code>checkPrimitiveBound</code>,
     *                                  see that method's docs for details
     * @param variable                  variable whose value is to be set
     * @param value                     value to set
     */
    public void setValue(AbstractInterpreter interpreter, RuntimeMethod rm,
            CodeVariable variable, RuntimeValue value) throws InterpreterException {
        CodeVariablePrimitiveRange.check(interpreter, rm,
                // incomplete dereference, but runtime container given
                new CodeFieldDereference(variable), this, interpreter.typeRangeMap,
                value);
        values.put(variable, AbstractInterpreter.convertArithmetic(value, variable.type));
    }
//    /**
//     * Removes a variable, that belongs to this container.
//     *
//     * @param variable                  variable whose value is to be set
//     */
//    public void removeValue(CodeVariable variable) {
//        values.remove(variable);
//    }
    /**
     * Returns a value of a variable, that belongs to this container. Primitive
     * bound checking is performed, even if it should not normally fail.
     * If it fails, though, a runtime exception is thrown.
     *
     * @param interpreter               used by <code>checkPrimitiveBound</code>,
     *                                  see that method's docs for details
     * @param rm                        used by <code>checkPrimitiveBound</code>,
     *                                  see that method's docs for details
     * @param variable                  variable whose value is returned
     * @return                          value of the variable, or null if the
     *                                  variable has not been initialized
     */
    public RuntimeValue getValue(AbstractInterpreter interpreter, RuntimeMethod rm,
            CodeVariable variable) {
        RuntimeValue value = values.get(variable);
        if(value != null)
            try {
                CodeVariablePrimitiveRange.check(interpreter, rm,
                        // incomplete dereference, but runtime container given
                        new CodeFieldDereference(variable), this,
                        interpreter != null ? interpreter.typeRangeMap : null,
                        value);
            } catch(InterpreterException e) {
                throw new RuntimeException("unexpected failure : " + e.toString());
            }
        return value;
    }
    /**
     * Returns all variables, that have their values stored in this
     * container.
     *
     * @return                          set of variables
     */
    public Set<CodeVariable> getVariables() {
        return values.keySet();
    }
    /**
     * Returns name of this container, unique for containers within
     * an interpreter.
     *
     * @return                          unique name
     */
    abstract public String getUniqueName();
    /**
     * Returns an unique name of a runtime variable.<br>
     *
     * <code>RuntimeObject</code> overrides this method for
     * fields. The overriding method causes, that
     * the field's name contains the class where the field was declared,
     * as opposed to the class of the object to which the field belongs.
     * This way, if one variable shadows another and they share a common
     * container, no name clash occurs.
     *
     * @param variable                  variable, must belong to this
     *                                  container
     * @return                          unique runtime variable's name
     */
    public String getRuntimeVariableName(CodeVariable variable) {
        String variableName = getUniqueName();
        variableName +=  "::" + (variable != null ? variable.name : variable);
        return variableName;
    }
    /**
     * Removes a variable from this container.
     * 
     * @param variable                  variable to remove
     */
    public void removeVariable(CodeVariable variable) {
        // variables.remove(variable);
        values.remove(variable);
    }
    /**
     * Prints this object along with its contents, detects recursive
     * references.
     * 
     * @param stack                     objects already printed, to
     *                                  detect repeated references,
     *                                  null for none
     * @return
     */
    public String toString(Set<AbstractRuntimeContainer> alreadyPrinted) {
        if(alreadyPrinted == null)
            alreadyPrinted = new HashSet<AbstractRuntimeContainer>();
        String s = "[";
        SortedMap<String, CodeVariable> variablesSorted = new TreeMap<>();
        for(CodeVariable variable : values.keySet())
            variablesSorted.put(variable.toNameString(), variable);
        for(CodeVariable variable : variablesSorted.values()) {
            s += " " + variable.toNameString();
            RuntimeValue value = values.get(variable);
            if(value != null)
                s += "=" + value.toString(alreadyPrinted);
        }
        s += " ]";
        return s;
    }
    @Override
    public String toString() {
        return toString(null);
    }
}
