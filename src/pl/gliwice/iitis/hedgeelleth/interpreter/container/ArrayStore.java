/*
 * ArrayStore.java
 *
 * Created on Mar 5, 2009, 2:39:51 PM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.interpreter.container;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.code.CodeConstant;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.AbstractCodeDereference;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.CodeFieldDereference;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.CodeIndexDereference;
import pl.gliwice.iitis.hedgeelleth.compiler.code.optimizer.runtime.RuntimeStaticAnalysis;
import pl.gliwice.iitis.hedgeelleth.compiler.primitiverange.CodeVariablePrimitiveRange;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Type;
import pl.gliwice.iitis.hedgeelleth.interpreter.exception.InterpreterException;
import pl.gliwice.iitis.hedgeelleth.interpreter.exception.InterpreterFatalException;
import pl.gliwice.iitis.hedgeelleth.interpreter.*;

/**
 * Array store.<br>
 *
 * Adds to the container's field--dependent <code>setValue</code>,
 * <code>getValue</code> index--dependent counterparts. The method
 * <code>setValue</code>, <code>getValue</code> are usually only
 * used on the array length field.<br>
 *
 * The element access methods have the possibility of range checking
 * of variable primitive ranges of a variables, that reference
 * this store.
 *
 * @author Artur Rataj
 */
public class ArrayStore extends RuntimeObject {
    /**
     * Description of an element within some store.
     */
    public static class ElementRef {
        /**
         * Store.
         */
        public ArrayStore store;
        /**
         * The element's index.
         */
        public int index;
        
        /**
         * Creates a new element reference.
         * 
         * @param store array store
         * @param index index within the store
         */
        public ElementRef(ArrayStore store, int index) {
            this.store = store;
            this.index = index;
        }
        @Override
        public int hashCode() {
            int hash = 7;
            hash = 67 * hash + Objects.hashCode(this.store);
            hash = 67 * hash + this.index;
            return hash;
        }
        @Override
        public boolean equals(Object o) {
            if(o instanceof ElementRef) {
                ElementRef other = (ElementRef)o;
                return store == other.store &&
                        index == other.index;
            } else
                throw new RuntimeException("invalid comparison");
        }
        @Override
        public String toString() {
            return store.toString() + "[" + index + "]";
        }
    }
    /**
     * Length of this store in the number of elements.
     */
    int length;
    /**
     * Store of references.
     */
    RuntimeValue[] store;

    /**
     * Creates a new instance of ArrayStore.<br>
     *
     * Length of the array, depending on language, must probably be also
     * passed to a constructor that sets a respective field representing
     * the field, so that the length can be queries on language lebel.
     * See the method <code>AbstractInterpreter.newArrays</code> for example
     * on this.
     *
     * @param arrayClass                runtime of array class
     * @param type                      type of the created array store,
     *                                  see <code>RuntimeObject.type</code>
     *                                  for details
     * @param length                    length of the array, in the number
     *                                  of elements
     */
    public ArrayStore(RuntimeObject arrayClass, Type type, int length) {
        super(arrayClass, type);
        this.length = length;
        store = new RuntimeValue[this.length];
    }
    /**
     * Returns the length of this store, in the number of elements.
     * 
     * @return                          size of this store
     */
    public int getLength() {
        return length;
    }
    /**
     * Checks if the index is within bounds of this sttore, and if not,
     * throws an <code>InterpreterException</code>.
     * 
     * @param index                     index to check
     */
    protected void checkIndex(int index) throws InterpreterException {
        if(index < 0 || index >= length) {
            String s = "index out of bounds: " + index;
            if(index >= length)
                s += ", array size is " + length;
            throw new InterpreterException(null, s);
        }
    }
    /**
     * Returns the value at a given index of this store.<br>
     *
     * Throws <code>InterpreterException</code> if index is out of
     * bounds or value is out of primitive range.
     *
     * @param interpreter               used by <code>checkPrimitiveBound</code>,
     *                                  see that method's docs for details
     * @param rm                        used by <code>checkPrimitiveBound</code>,
     *                                  see that method's docs for details
     * @param target                    target dereference, to get possible
     *                                  variable primitive range, null for
     *                                  no range checking
     * @param index                     index of this store
     * @param value                     value to store
     */
    public void setElement(AbstractInterpreter interpreter, RuntimeMethod rm,
            AbstractCodeDereference target, int index, RuntimeValue value) throws InterpreterException {
        checkIndex(index);
        CodeVariablePrimitiveRange.check(interpreter, rm, target, null,
                interpreter != null ? interpreter.typeRangeMap : null, value);
        store[index] = AbstractInterpreter.convertArithmetic(value,
                type.getElementType(null, 0));
    }
    /**
     * Calls <code>set</code>, but if the called method throws
     * <code>InterpreterException</code>, the exception is replaced
     * with unchecked <code>InterpreterFatalException</code>, that
     * has the category <code>UNCHECKED_INDEX_OUT_OF_BOUNDS</code><br>.
     *
     * Use if the index is expected to be never out of bounds and
     * the value is expected to be never outside the target's
     * primitive range.
     *
     * @param interpreter               used by <code>checkPrimitiveBound</code>,
     *                                  see that method's docs for details
     * @param rm                        used by <code>checkPrimitiveBound</code>,
     *                                  see that method's docs for details
     * @param target                    target dereference, to get possible
     *                                  variable primitive range, null for
     *                                  no range checking
     * @param index                     index of this store
     * @param value                     value to store
     */
    public void setElementUnchecked(AbstractInterpreter interpreter, RuntimeMethod rm,
            AbstractCodeDereference target, int index, RuntimeValue value) {
        try {
            setElement(interpreter, rm, target, index, value);
        } catch(InterpreterException e) {
            throw new InterpreterFatalException(InterpreterFatalException.Category.
                    UNCHECKED_INDEX_OUT_OF_BOUNDS, e.getMessage());
        }
    }
    /**
     * Returns the value at a given index of this store.<br>
     *
     * Throws <code>InterpreterException</code> if index is out of
     * bounds or variable is out of source's possible primitive range.
     *
     * @param interpreter               used by
     *                                  <code>CodeVariablePrimitiveRange.check()</code>,
     *                                  see that method's docs for details
     * @param rm                        used by
     *                                  <code>CodeVariablePrimitiveRange.check()</code>,
     *                                  see that method's docs for details
     * @param source                    source dereference, to get possible
     *                                  variable primitive range, null for
     *                                  no range checking
     * @param index                     index of this store
     * @return                          value to retrieve
     */
    public RuntimeValue getElement(AbstractInterpreter interpreter, RuntimeMethod rm,
            AbstractCodeDereference source, int index) throws InterpreterException {
        checkIndex(index);
        RuntimeValue value = store[index];
        CodeVariablePrimitiveRange.check(interpreter, rm, source, null,
                interpreter != null ? interpreter.typeRangeMap : null, value);
        return value;
    }
    /**
     * Calls <code>set</code>, but if the called method throws
     * <code>InterpreterException</code>, the exception is replaced
     * with unchecked <code>InterpreterFatalException</code>, that
     * has the category <code>UNCHECKED_INDEX_OUT_OF_BOUNDS</code><br>.
     *
     * Use if the index is expected to be never out of bounds and
     * the value is expected to be never outside the source's
     * primitive range.
     *
     * @param interpreter               used by <code>checkPrimitiveBound</code>,
     *                                  see that method's docs for details
     * @param rm                        used by <code>checkPrimitiveBound</code>,
     *                                  see that method's docs for details
     * @param source                    source dereference, to get possible
     *                                  variable primitive range, null for
     *                                  no range checking
     * @param index                     index of this store
     * @param value                     value to store
     */
    public RuntimeValue getElementUnchecked(AbstractInterpreter interpreter, RuntimeMethod rm,
            AbstractCodeDereference source, int index) {
        try {
            return getElement(interpreter, rm, source, index);
        } catch(InterpreterException e) {
            throw new InterpreterFatalException(InterpreterFatalException.Category.
                    // !!! can also be value out of bounds
                    UNCHECKED_INDEX_OUT_OF_BOUNDS, e.getMessage());
        }
    }
    /**
     * Returns a description of this store's element.
     * 
     * @param index index of the element to refer to
     * @return representation of a given element of this store
     */
    public ElementRef getElementRef(int index) {
        return new ElementRef(this, index);
    }
}
