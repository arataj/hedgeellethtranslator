/*
 * InternalRuntimeContainer.java
 *
 * Created on May 9, 2008
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.interpreter.container;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.code.CodeClass;
import pl.gliwice.iitis.hedgeelleth.compiler.code.CodeVariable;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Context;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Type;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Variable;

/**
 * A runtime container for holding internal runtime variables.
 * 
 * Allows for easy creation and look up of such variables.
 * 
 * @author Artur Rataj
 */
public class InternalRuntimeContainer extends AbstractRuntimeContainer {
    /**
     * Name, must be unique within the interpreter.
     */
    public String name;
    /**
     * Class of this container. It is an internal class
     * unique for each internal runtime container.
     */
    public CodeClass clazz;
    /**
     * Code variables keyed with names.
     */
    public Map<String, CodeVariable> variablesByName;
    
    /**
     * Creates a new instance of InternalRuntimeContainer.
     * 
     * @param name                      name of this container,
     *                                  must be unique within
     *                                  the interpreter
     */
    public InternalRuntimeContainer(String name) {
        super();
        this.name = name;
        clazz = new CodeClass("#internal_runtime_container_" + name,
                null);
        variablesByName = new HashMap<>();
    }
    /**
     * Creates a new code variable, of category
     * <code>Variable.Category.INTERNAL_NOT_RANGE</code>.
     * 
     * @param name                      name of the variable,
     *                                  unique within this container
     * @return                          code variable
     */
    public CodeVariable newVariable(String name, Type type) {
        CodeVariable cv =  new CodeVariable(null, clazz, name, type,
                Context.NON_STATIC, false, false, false,
                Variable.Category.INTERNAL_NOT_RANGE);
        // variables.add(cv);
        if(variablesByName.get(name) != null)
            throw new RuntimeException("duplicate name of variable");
        variablesByName.put(name, cv);
        return cv;
    }
    @Override
    public void removeVariable(CodeVariable variable) {
        super.removeVariable(variable);
        variablesByName.values().remove(variable);
    }
    @Override
    public String getUniqueName() {
        return name;
    }
}
