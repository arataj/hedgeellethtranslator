/*
 * RuntimeField.java
 *
 * Created on Sep 16, 2009, 2:17:02 PM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.interpreter.container;

import pl.gliwice.iitis.hedgeelleth.compiler.code.CodeClass;
import pl.gliwice.iitis.hedgeelleth.compiler.code.CodeVariable;
import pl.gliwice.iitis.hedgeelleth.interpreter.AbstractInterpreter;
import pl.gliwice.iitis.hedgeelleth.interpreter.RuntimeValue;
import pl.gliwice.iitis.hedgeelleth.interpreter.exception.InterpreterException;

/**
 * Represents a runtime field. Static fields have the object part
 * null.<br>
 *
 * Objects like that are normally not used by the interpreter, as
 * the interpreter uses the combination of a dereference and a runtime
 * container. The container returns <code>RuntimeValue</code> of a
 * dereferenced object.<br>
 * 
 * Still, <code>RuntimeField</code> might be needed for some special
 * cases outside the interpreter. For example,
 * <code>RuntimeStaticAnalysis</code>, instead of a runtime container,
 * uses <code>TraceValues</code> to get <code>RuntimeValue</code>s.
 * After a runtime value is obtained, then, together with a code
 * variable that represents a field, forms this object. 
 *
 * @author Artur Rataj
 */
public class RuntimeField {
    /**
     * Dereferenced runtime object for non--static fields.
     * For static fields, like in <code>CodeFieldDereference>,
     * null. To get the container of both non--static and static
     * fields, use <code>getContainer</code>.
     */
    public final RuntimeValue object;
    /**
     * Represented field's code variable.
     */
    public final CodeVariable field;

    /**
     * Creates a new instance of RuntimeField.
     *
     * @param object                    dereferenced runtime object, null
     *                                  for no object
     * @param field                     field's code variable, non--static
     *                                  if <code>object</code> is not null,
     *                                  static otherwise
     */
    public RuntimeField(RuntimeValue object, CodeVariable field) {
//if(object.toString().contains("Both") && field.toString().contains("Pair"))
//    object = object;
        this.object = object;
        this.field = field;
    }
    /**
     * Returns the container of the field.<br>
     *
     * This method can throw an interpreter exception if some static
     * variable of the field's container runtime classs is initialized
     * with a value outside that variable's primitive range. This
     * may happen only if this field is static, as otherwise the underlying
     * class has already been initialized before.
     *
     * @param interpreter               interpreter, needed only for static
     *                                  fields, as non--static fields have the
     *                                  container stored in <code>object</code>
     * @return                          runtime container
     */
    public AbstractRuntimeContainer getContainer(AbstractInterpreter interpreter)
            throws InterpreterException {
        if(object != null)
            return object.getReferencedObject();
        else
            return interpreter.getRuntimeClass((CodeClass)field.owner);
    }
    @Override
    public boolean equals(Object o) {
        if(o instanceof RuntimeField) {
            RuntimeField f = (RuntimeField)o;
            if(field != f.field)
                return false;
            if(object == null) {
                return f.object == null;
            } else if(f.object == null)
                return false;
            return object.equals(f.object);
        } else
            return false;
    }
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + (this.object != null ? this.object.hashCode() : 0);
        hash = 61 * hash + (this.field != null ? this.field.hashCode() : 0);
        return hash;
    }
    @Override
    public String toString() {
        return (object != null && object.getReferencedObject() != null) ?
            // non--static field, ask the container about its
            // runtime name
            object.getReferencedObject().getRuntimeVariableName(field) :
            // static field, can find the runtime name on its
            // own, use this, as the container is unknown without
            // the interpreter
            field.toNameString();
    }
}
