/*
 * RuntimeMethod.java
 *
 * Created on May 4, 2008
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.interpreter.container;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.interpreter.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Context;

/**
 * Runtime parameters of a called method: values of locals and
 * pending new threads.
 * 
 * @author Artur Rataj
 */
public class RuntimeMethod extends AbstractRuntimeContainer {
    /**
     * Method.
     */
    public CodeMethod method;
    /**
     * Threads started in this runtime, also inside possible
     * other methods.
     */
    public List<RuntimeThread> pendingThreads;
    
    /**
     * Creates a new instance of RuntimeMethod.
     */
    public RuntimeMethod(CodeMethod method) {
        super();
        // for(CodeVariable v : method.variables.values())
        //     variables.add(v);
        this.method = method;
        pendingThreads = new LinkedList<>();
    }
    /**
     * Returns runtime value of "this". If this is a static method
     * or argument "this" is uninitialized, null is returned.
     * 
     * @return                          runtime value of "this" or
     *                                  null
     */
    public RuntimeValue getThisArg() {
        if(method.context != Context.NON_STATIC)
            return null;
        else
            return values.get(method.getThisArg());
    }
    @Override
    public String getUniqueName() {
        RuntimeValue thiS = getThisArg();
        if(thiS == null) {
            if(method.context == Context.NON_STATIC)
                throw new RuntimeException("can not determine name " +
                        "because object value is missing");
            return method.getKey();
        } else
            return thiS.getReferencedObject().name + "::" +
                    method.getKey();
    }
    @Override
    public String toString() {
        String s = method.getKey() + " " + super.toString();
        for(RuntimeThread t : pendingThreads)
            s += " " + t.toString();
        return s;
    }
}
