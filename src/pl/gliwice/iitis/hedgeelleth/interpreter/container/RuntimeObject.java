/*
 * RuntimeObject.java
 *
 * Created on May 3, 2008
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.interpreter.container;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.clazz.AbstractJavaClass;
import pl.gliwice.iitis.hedgeelleth.compiler.code.*;

/**
 * A runtime object.
 * 
 * Classes are also objects. If this object represents a
 * class, the non--static java class variables of this
 * object are the respective static variables of the class.
 * 
 * @author Artur Rataj
 */
public class RuntimeObject extends AbstractRuntimeContainer {
    /**
     * Type of this object. For objects representing class' instances,
     * the type is different from the class' type for parameterized types.
     */
    public Type type;
    /**
     * Signature of a *static method.
     */
    public static final MethodSignature STATIC_METHOD_SIGNATURE =
            new MethodSignature(null, AbstractJavaClass.INIT_STATIC,
                new LinkedList<Typed>());
    /**
     * Signature of an *object method.
     */
    public static final MethodSignature OBJECT_METHOD_SIGNATURE =
            new MethodSignature(null, AbstractJavaClass.INIT_OBJECT,
                new LinkedList<Typed>());

    /**
     * Name of this object, unique within each interpreter.
     */
    protected String name;
    /**
     * Serial number of this object, unique within each
     * interpreter.
     */
    protected int serial;
    /**
     * Runtime class of this object, not null only for
     * objects not representing classes.
     */
    public RuntimeObject runtimeClass;
    /**
     * Code class of this object.
     */
    public CodeClass codeClass;
    /**
     * If this is an object representing a class or an instance.
     */
    public Context context;
    /**
     * Counter of serial numbers, used to generate unique
     * name for runtime objects.
     */
    private static int nextSerialNum;
    {
        if(nextSerialNum == 0)
            // reset it only once per the family of classes,
            // whose root is <code>RuntimeObject</code>
            resetSerial();
    }
    
    /**
     * Creates a new instance of RuntimeObject, representing
     * a class.
     * 
     * @param clazz                     class of this object
     */
    public RuntimeObject(CodeClass codeClass) {
        super();
        this.runtimeClass = null;
        this.codeClass = codeClass;
        context =  Context.STATIC;
        this.type = codeClass.type;
        initializeRuntimeObject();
    }
    /**
     * Creates a new instance of RuntimeObject, representing a class'
     * instance.
     * 
     * @param clazz                     runtime class of this object
     * @param type                      type of this object, can be different
     *                                  from the class' type for parameterized
     *                                  types
     */
    public RuntimeObject(RuntimeObject runtimeClass, Type type) {
        super();
        this.runtimeClass = runtimeClass;
        this.codeClass = this.runtimeClass.codeClass;
        context = Context.NON_STATIC;
        this.type = type;
        initializeRuntimeObject();
    }
    /**
     * Common initialization for RuntimeObject.
     */
    private final void initializeRuntimeObject() {
        // setVariables();
        name = codeClass.name;
        serial = getNextSerial();
        if(context != Context.NON_STATIC)
            name += ".class";
        else
            name += "#" + serial;
    }
    /**
     * Returns an unique serial number for a created runtime object.
     * @return
     */
    public static synchronized int getNextSerial() {
        return nextSerialNum++;
    }
    /**
     * Resets the per--class static serial number.<br>
     *
     * This should normally be done only
     * when loading this class, and is not safe if multiple
     * interpreters are run at once.<br>
     *
     * This method is normally used only by this class' static block
     * and in compiler testing. The tests need repeatable output
     * files, so they reset the serial number counter.
     */
    public static void resetSerial() {
        nextSerialNum = 100;
    }
    /*
     * Determines the java class variables of this object.
     * 
     * If this object represents a java object, it chooses the
     * non--static fields of the corresponding class. If it
     * object represents a java class, it chooses the static
     * fields.
     * 
     * Map of values is empty, what means that the fields
     * are not yet initialized. They will be initialized
     * by either *static or *object methods.
     */
    /*
    void setVariables() {
        for(CodeVariable v : codeClass.fields.values())
            if(v.type.isJava() && v.context == context)
                variables.add(v);
    }
     */
    /**
     * Appends a string to this object's name. Should be typically
     * used only directly after this object is created, as the name
     * might later be assumed to be constant in some code.
     *
     * @param postfix                   string to append
     */
    public void appendToName(String postfix) {
        name += postfix;
    }
    @Override
    public String getUniqueName() {
        return name;
    }
    /**
     * Returns an unique name of a runtime field.<br>
     *
     * This method handles the special case of shadowed
     * fields, see the supermethod docs for details.
     *
     * @param field                     field, must belong to this
     *                                  container
     * @return                          unique field name
     */
    @Override
    public String getRuntimeVariableName(CodeVariable field) {
        if((field.context != Context.NON_STATIC) != (context != Context.NON_STATIC))
            throw new RuntimeException("static / non--static mismatch");
        String s = ((CodeClass)field.owner).name;
        if(field.context != Context.NON_STATIC)
            // only one instance, no need for serial
            s += ".";
        else
            s +=  "#" + getSerial() + "::";
        s += field.name;
        return s;
    }
    /**
     * Returns serial number of this container, unique for containers
     * within an interpreter.
     *
     * @return                          unique serial number
     */
    public int getSerial() {
        return serial;
    }
    /**
     * Prints this object along with its contents, detects recursive
     * references.
     * 
     * @param alreadyPrinted            objects already printed, to
     *                                  detect repeated references,
     *                                  null for none
     * @return
     */
    @Override
    public String toString(Set<AbstractRuntimeContainer> alreadyPrinted) {
        String s = name + " " + super.toString(alreadyPrinted);
        return s;
    }
    @Override
    public String toString() {
        return toString(null);
    }
}
