/*
 * InterpreterException.java
 *
 * Created on May 3, 2008
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.interpreter.exception;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.code.op.AbstractCodeOp;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * An interpreter exception.
 * 
 * @author Artur Rataj
 */
public class InterpreterException extends TranslatorException {
    /**
     * Stack trace, empty for none.
     */
    List<AbstractCodeOp> stack;
    
    /**
     * Creates a new instance of InterpreterException.
     * 
     * @param pos                       position, can be null
     * @param description               description of this exception
     */
    public InterpreterException(StreamPos streamPos, String description) {
        super(streamPos, description);
        stack = new LinkedList<>();
    }
    /**
     * Adds a stack frame. Frames should be added from the innermost.
     * 
     * @param op an operation within a runtime method, that called
     * a method, from which an interpreter exception originated
     */
    public void addStackFrame(AbstractCodeOp op) {
        stack.add(op);
    }
    /**
     * Prints stack information, if any.
     * 
     * @param ignoreInner how many inner frames to ignore,
     * can be larger than the total number of frames
     * @return textual description of stack trace
     */
    public String dumpStack(int ignoreInner) {
        StringBuilder trace = new StringBuilder();
        int count = 0;
        for(AbstractCodeOp op : stack) {
            if(count > ignoreInner)
                trace.append(", ");
            if(count >= ignoreInner)
                trace.append(op.getStreamPos());
            ++count;
        }
        return trace.toString();
    }
//    /**
//     * Prints stack information, if any.
//     * 
//     * @param pos ignore the most inner frame 
//     * @return textual description of stack trace
//     */
//    public String dumpStack(StreamPos pos) {
//        StringBuilder trace = new StringBuilder();
//        int count = 0;
//        for(AbstractCodeOp op : stack) {
//            if(count > ignoreInner)
//                trace.append(", ");
//            if(count >= ignoreInner)
//                trace.append(op.getStreamPos());
//            ++count;
//        }
//        return trace.toString();
//    }
    /**
     * Get description only, without position, with stack trace
     * 
     * @return description of interpreting error, with stack, yet without this
     * exception's position
     */
    public String getRawDescription() {
        return getRawMessage() + ", ${stack} " + dumpStack(0);
    }
    @Override
    public String toString() {
        String s = dumpStack(0);
        if(!s.isEmpty())
            s = " stack: " + s;
        return super.toString() + s;
    }
}
