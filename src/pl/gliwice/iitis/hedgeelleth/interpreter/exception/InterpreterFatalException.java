/*
 * InterpreterFatalException.java
 *
 * Created on Feb 17, 2009, 3:19:03 PM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.interpreter.exception;

/**
 * An interpreter exception that normally should not happen,
 * unchecked. Contains error category identifier.
 *
 * @author Artur Rataj
 */
public class InterpreterFatalException extends RuntimeException {
    public enum Category {
        /**
         * Uninitialized variable.
         */
        UNINITIALIZED_VARIABLE,
        /**
         * Missing type.
         */
        TYPE_MISSING,
        /**
         * Null dereference where it should never happen.
         */
        UNCHECKED_NULL_DEREFERENCE,
        /**
         * Index out of bounds where it should never happen.
         */
        UNCHECKED_INDEX_OUT_OF_BOUNDS,
        /**
         * Wrapped non--fatal exception.
         */
        WRAPPED_EXCEPTION,
        /**
         * A value of a local was asked, but no runtime method was given.
         */
        LOCAL_BUT_NO_RM,
    };
    /**
     * Category of the interpreter error that caused this
     * exception.
     */
    public Category category;

    /**
     * Creates a new instance of InterpreterFatalException. 
     */
    public InterpreterFatalException(Category category, String message) {
        super(message);
        this.category = category;
    }
}
