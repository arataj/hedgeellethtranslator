/*
 * InterpreterMissingException.java
 *
 * Created on Dec 4, 2009, 1:39:46 PM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.interpreter.exception;

/**
 * A subclass of <code>InterpreterException</code> thrown when a
 * value can not be evaluated because no interpreter or runtime
 * method are present, and a method allows such a case.<br>
 *
 * Thrown by <code>CodePrimaryPrimitiveRange.check()</code>.
 *
 * @author Artur Rataj
 */
public class InterpreterMissingException extends InterpreterException {
    /**
     * Creates a new instance of InterpreterNoObjectOrNullException.
     *
     * @param description               description of this exception
     */
    public InterpreterMissingException(String description) {
        super(null, description);
    }
}
