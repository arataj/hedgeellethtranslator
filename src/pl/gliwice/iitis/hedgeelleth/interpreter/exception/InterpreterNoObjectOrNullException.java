/*
 * InterpreterNoObjectOrNullException.java
 *
 * Created on May 20, 2009, 7:02:18 PM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.interpreter.exception;

/**
 * A subclass of <code>InterpreterException</code> thrown by
 * <code>AbstractInterpreter.getReferencedOrNull</code>.
 *
 * @author Artur Rataj
 */
public class InterpreterNoObjectOrNullException extends InterpreterException {
    /**
     * Creates a new instance of InterpreterNoObjectOrNullException.
     *
     * @param description               description of this exception
     */
    public InterpreterNoObjectOrNullException(String description) {
        super(null, description);
    }
}
