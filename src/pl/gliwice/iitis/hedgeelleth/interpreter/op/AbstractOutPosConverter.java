/*
 * OutPosWriter.java
 *
 * Created on Sep 22, 2015
 *
 * Copyright (c) 2015  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.interpreter.op;

import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.StreamPos;

/**
 * Converts a stream position into some respective string. Its
 * method <code>convertPos()</code> is used from
 * <code>BaseInterpreter</code> to call e.g.  some preprocessor,
 * to map the position into a source positions in the original file,
 * and then mark it in the interpreter's output, and effectively in a
 * preprocessed file.
 *
 * @author Artur Rataj
 */
public interface AbstractOutPosConverter {
    /**
     * Converts a position into some string representation.
     * 
     * @param pos position to convert
     * @return output string; typically begins with a line comment
     * prefix
     */
    public String convertPos(StreamPos pos);
}
