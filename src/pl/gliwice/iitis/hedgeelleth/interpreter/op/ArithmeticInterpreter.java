/*
 * ArithmeticInterpreter.java
 *
 * Created on Mar 9, 2009, 11:27:54 AM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.interpreter.op;

import pl.gliwice.iitis.hedgeelleth.interpreter.exception.InterpreterException;
import pl.gliwice.iitis.hedgeelleth.compiler.code.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.op.*;
import pl.gliwice.iitis.hedgeelleth.interpreter.*;

/**
 * Adds value interpretation functionality to the interpreter by implementing
 * <code>CodeOpUnaryExpression</code>, <code>CodeOpBinaryExpression</code>
 * and <code>CodeOpBranch</code>.
 *
 * @author Artur Rataj
 */
public class ArithmeticInterpreter extends BaseInterpreter {
    /**
     * Creates a new instance of ArithmeticInterpreter.
     *
     * @param compilation               code compiled to the
     *                                  internal assembler
     */
    public ArithmeticInterpreter(CodeCompilation compilation) {
        super(compilation);
    }
    @Override
    public Object visit(InterpretingContext context, CodeOpBinaryExpression be) throws InterpreterException {
        setValue(be.getResult(), CodeArithmetics.computeValue(context, be), context.rm);
        context.toNext();
        return null;
    }
    @Override
    public Object visit(InterpretingContext context, CodeOpBranch b) throws InterpreterException {
        RuntimeValue condition = context.interpreter.getValue(b.condition,
                context.rm);
        if(condition.getBoolean()) {
            if(b.labelRefTrue != null)
                context.index = b.labelRefTrue.codeIndex;
            else
                context.toNext();
        } else {
            if(b.labelRefFalse != null)
                context.index = b.labelRefFalse.codeIndex;
            else
                context.toNext();
        }
        return null;
    }
    @Override
    public Object visit(InterpretingContext context, CodeOpUnaryExpression ue) throws InterpreterException {
        setValue(ue.getResult(), CodeArithmetics.computeValue(context, ue, null /* !!! unify??? */), context.rm);
        context.toNext();
        return null;
    }
}
