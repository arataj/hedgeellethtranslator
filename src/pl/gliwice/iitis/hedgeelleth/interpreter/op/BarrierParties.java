/*
 * BarrierParties.java
 *
 * Created on Nov 22, 2014
 *
 * Copyright (c) 2014  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.interpreter.op;

import java.nio.IntBuffer;
import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.StreamPos;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.RuntimeMethod;
import pl.gliwice.iitis.hedgeelleth.interpreter.exception.InterpreterException;

/**
 * Parties of a barrier. A single party is represented by a runtime method,
 * which in turn represents a thread.
 * 
 * @author Artur Rataj
 */
public class BarrierParties {
    /**
     * If this barrier is directional.
     */
    public final boolean directional;
    /**
     * Total number of parties. In the case of a directional barrier,
     * it is a sum of <code>numProducers</code> and
     * <code>numConsumers</code>.
     */
    public final int numUsers;
    /**
     * Number of producers. Zero for a non--directional barrier.
     */
    public final int numProducers;
    /**
     * Number of consumers. Zero for a non--directional barrier.
     */
    public final int numConsumers;
    /**
     * Actually collected parties.
     * 
     * If at least a single uses is both a producer and a consumer, then
     * collected users &lt; collected producers + collected consumers.
    */
    protected final List<RuntimeMethod> collectedUsers;
    /**
     * Actually collected producers. Null for a non--directional barrier.
     */
    protected final List<RuntimeMethod> collectedProducers;
    /**
     * Actually collected consumers. Null for a non--directional barrier.
     */
    protected final List<RuntimeMethod> collectedConsumers;
    /**
     * Passes synchronisation values between producers and consumers.
     */
    public IntBuffer[] producerValues;
    
    /**
     * Creates a new instance of <code>BarrierParties</code>, which
     * can be either directional or not.
     * 
     * @pos position in the source; needed to generate exceptions
     * @param directional if a directional barrier
     * @param numUsers total number of threads, which use this barrier
     * @param numProducers number of producers; 0 for a non--directional
     * barrier
     * @param numConsumers number of consumers; 0 for a non--directional
     * barrier
     */
    public BarrierParties(StreamPos pos, boolean directional,
            long numUsers, long numProducers,
            long numConsumers) throws InterpreterException {
        this.directional = directional;
        check(pos, numUsers);
        check(pos, numProducers);
        check(pos, numConsumers);
        this.numUsers = (int)numUsers;
        this.numProducers = (int)numProducers;
        this.numConsumers = (int)numConsumers;
        if(!directional && numProducers + numConsumers > 0)
            throw new RuntimeException("a non--directional barrier can not " +
                    "have producers or consumers");
        collectedUsers = new LinkedList<>();
        if(directional) {
            collectedProducers = new LinkedList<>();
            collectedConsumers = new LinkedList<>();
        } else {
            collectedProducers = null;
            collectedConsumers = null;
        }
        producerValues = new IntBuffer[this.numProducers];
        for(int i = 0; i < this.numProducers; ++i) {
            producerValues[i] = IntBuffer.allocate(1);
            producerValues[i].flip();
        }
    }
    /**
     * Adds a party to a given list, if the party is not present yet.
     * In any case, retruns an index of the party.
     * 
     * @param list a list of parties
     * @param party a party
     * @return an index f the party
     */
    private int add(List<RuntimeMethod> list, RuntimeMethod party) {
        int index = list.indexOf(party);
        if(index == -1) {
            list.add(party);
            index = list.size() - 1;
        }
        return index;
    }
    /**
     * Adds a party, if not present yet. If a directional barrier, It can
     * be a producer or a consumer.
     * 
     * @return index of the party added, in the list of all parties
     */
    public int addUser(RuntimeMethod party) {
        return add(collectedUsers, party);
    }
    /**
     * Adds a producer party, if not present yet. Must be already present
     * in the list of all parties.
     * 
     * @param party party to add, or to check its index
     * @return index of the party added, in the list of producer parties
     */
    public int addProducer(RuntimeMethod party) {
        if(!collectedUsers.contains(party))
            throw new RuntimeException("absent in the list of all parties");
        if(!directional)
            throw new RuntimeException("producer can not be added to a non--directional barrier");
        return add(collectedProducers, party);
    }
    /**
     * Adds a consumer party, if not present yet. Must be already present
     * in the list of all parties.
     * 
     * @param party party to add, or to check its index
     * @return index of the party added, in the list of consumer parties
     */
    public int addConsumer(RuntimeMethod party) {
        if(!collectedUsers.contains(party))
            throw new RuntimeException("absent in the list of all parties");
        if(!directional)
            throw new RuntimeException("consumer can not be added to a non--directional barrier");
        return add(collectedConsumers, party);
    }
    /**
     * Rough check of the validity of a value.
     * 
     * @pos position in the source; needed to generate exceptions
     * @param num number of threads, that are a certain kind of
     * this barrier's users
     */
    private void check(StreamPos pos, long num) throws InterpreterException {
        if(num < 0)
            throw new InterpreterException(pos,
                    "negative number of threads");
        if(num > Integer.MAX_VALUE)
            throw new InterpreterException(pos,
                    "number of threads exceeds 32--bit integer");
    }
    /**
     * Checks, if declared parties match collected parties. Should be called
     * only after all general parties, producers and consumers are collected.
     * 
     * @return null if matches, otherwise an error message
     */
    public String countMatches() {
        String error = "";
        int usersTotal;
        if(directional)
            usersTotal = collectedProducers.size() + collectedConsumers.size();
        else
            usersTotal = collectedUsers.size();
        if(numUsers != usersTotal) {
            error = "declares a total of " + numUsers +
                        " parties, but their actual number is " + collectedUsers.size();
        }
        if(directional) {
            if(numProducers != collectedProducers.size()) {
                if(!error.isEmpty())
                    error += "; ";
                error += "declares " + numProducers + " producer " +
                            "parties, but their actual number is " + collectedProducers.size();
            }
            if(numConsumers != collectedConsumers.size()) {
                if(!error.isEmpty())
                    error += "; ";
                error += "declares " + numConsumers + " consumer " +
                            "parties, but their actual number is " + collectedConsumers.size();
            }
        } else {
            if(numProducers != 0) {
                if(!error.isEmpty())
                    error += "; ";
                error += "can not have producer parties, " +
                            "but their actual number is " + collectedProducers.size();
            }
            if(numConsumers != 0) {
                if(!error.isEmpty())
                    error += "; ";
                error += "can not have consumer parties, " +
                            "but their actual number is " + collectedConsumers.size();
            }
        }
        if(error.isEmpty())
            error = null;
        return error;
    }
}
