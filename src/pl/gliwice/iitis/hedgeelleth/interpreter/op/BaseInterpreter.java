/*
 * BaseInterpreter.java
 *
 * Created on Mar 7, 2009, 6:04:25 PM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.interpreter.op;

import java.util.*;
import java.io.*;

import hc.TuplesIO;
import hc.MatrixIO;

import pl.gliwice.iitis.hedgeelleth.compiler.CompilerAnnotation;
import pl.gliwice.iitis.hedgeelleth.compiler.code.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.op.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Context;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Type;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Variable;
import pl.gliwice.iitis.hedgeelleth.interpreter.*;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.ArrayStore;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.RuntimeMethod;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.RuntimeObject;
import pl.gliwice.iitis.hedgeelleth.interpreter.exception.InterpreterException;

/**
 * A base interpreter, that implements <code>CodeOpAllocation</code>,
 * <code>CodeOpAssignment</code>, <code>CodeOpCall</code>,
 * <code>CodeOp</code>, <code>CodeOp</code>, <code>CodeOp</code><br>.
 * Encounter of a not supported operation or <code>CodeOpNone</code>
 * with unknown annotations causes an unchecked exception
 * <code>UnsupportedOperationException</code> to be thrown.<br>
 *
 * Within <code>CodeOpCall</code> the following annotations are recognized:
 * IGNORE and MARKER that prevent any action on the operation,
 * START_THREAD that starts a thread, and BARRIER_ACTIVATE that
 * manually specifies the number of threads, that uses a given barrier.<br>
 *
 * Override the visitor methods in subclasses for custom
 * interpretation of code operations.
 *
 * @author Artur Rataj
 */
public class BaseInterpreter extends AbstractInterpreter {
    /**
     * Prefix added by the interpreter before strings printed to the
     * console by the interpreted process. Unused if the print output
     * is redirected elsewhere.
     * 
     * TODO: print/println
     */
    public static final String INTERPRETER_PRINT_PREFIX = "interpreter: ";
    /**
     * If null, print output is redirected t the console. Otherwise,
     * this writer is used. The interpreted neither opens not closes it.
     */
    PrintWriter outWriter = null;
    /**
     * <p>If not null, converts a position of the <code>println()</code>
     * statement into a string to be appended after what the statement
     * has printed.</p>
     * 
     * <p>To be used to mark source positions by some preprocessor
     * etc.</p>
     */
    AbstractOutPosConverter outPosConverter = null;
    
    /**
     * Creates a new instance of BaseInterpreter.
     *
     * @param compilation               code compiled to the
     *                                  internal assembler
     */
    public BaseInterpreter(CodeCompilation compilation) {
        super(compilation);
    }
    /**
     * If not null, the print output is set to the stream, instead of
     * the console. The stream should be open, and the interpreter
     * never closes it.
     * 
     * @param stream output stream or null
     */
    public void setOutPosConverter(AbstractOutPosConverter converter) {
        outPosConverter = converter;
    }
    /**
     * If not null, the print output is set to the stream, instead of
     * the console. The stream should be open, and the interpreter
     * never closes it.
     * 
     * @param stream output stream or null
     */
    public void setOutStream(PrintWriter writer) {
        outWriter = writer;
    }
    @Override
    public Object visit(InterpretingContext context, CodeOpBinaryExpression op) throws InterpreterException {
        throw new InterpreterException(op.getStreamPos(), "operation not supported: " + op.toString());
    }
    @Override
    public Object visit(InterpretingContext context, CodeOpBranch op) throws InterpreterException {
        throw new InterpreterException(op.getStreamPos(), "operation not supported: " + op.toString());
    }
    @Override
    public Object visit(InterpretingContext context, CodeOpUnaryExpression op) throws InterpreterException {
        throw new InterpreterException(op.getStreamPos(), "operation not supported: " + op.toString());
    }
    @Override
    public Object visit(InterpretingContext context, CodeOpAllocation a) throws InterpreterException {
        if(a.constructor != null) {
            CodeMethod called = a.constructor.method;
            boolean execute = true;
            // does not use result type, as it might have
            // been changed by optimizer to a superclass
            // when replacing target variable
            Type type = a.getInstantiatedType();
            // constructor allocation
            RuntimeValue that = newRuntimeObject(
                    getCodeClass(type), type);
            List<RuntimeValue> argValues = getValues(a.args, context.rm);
            // add the constructor's "this"
            argValues.add(0, that);
            for(String s : called.annotations) {
                CompilerAnnotation annotation;
                switch(annotation = CompilerAnnotation.parse(s)) {
                    case IGNORE:
                    case MARKER:
                        execute = false;
                        break;

                    case UNKNOWN_ANNOTATION:
                        /* empty */
                        break;

                    case IO_NEW_TUPLES:
                    case IO_NEW_MATRIX:
                        String fileName = argValues.get(1).getString();
                        TuplesIO ioObject;
                        switch(annotation) {
                            case IO_NEW_TUPLES:
                                ioObject = new TuplesIO(fileName);
                                break;
                                
                            case IO_NEW_MATRIX:
                                ioObject = new MatrixIO(fileName);
                                break;
                                
                            default:
                                throw new RuntimeException("unknown i/o object");
                                
                        }
                        io.put(that.getReferencedObject().getUniqueName(),
                                ioObject);
                        break;
                        
                    default:
                        throw new InterpreterException(called.getStreamPos(),
                            "unsupported compiler annotation for allocation " + s);
                }
            }
            if(execute)
                interpret(context, a.constructor.method,
                        argValues);
            setValue(a.getResult(), that, context.rm);
        } else {
            // array allocation
            List<RuntimeValue> sizeValues = getValues(a.args, context.rm);
            String namePostfix = "#";
            if(a.result instanceof CodeFieldDereference) {
                CodeFieldDereference f = (CodeFieldDereference)a.result;
                if(f.object != null)
                    namePostfix += f.object.type.getJava() + "::";
                namePostfix += f.variable.toNameString();
            } else if(a.result instanceof CodeIndexDereference) {
                CodeIndexDereference i = (CodeIndexDereference)a.result;
                namePostfix += i.object.toNameString() + "[]";
            } else
                throw new RuntimeException("unknown dereference type");
            RuntimeValue rootThat = newArrays(context,
                    a.getInstantiatedType(), sizeValues,
                    namePostfix);
            setValue(a.getResult(), rootThat, context.rm);
        }
        context.toNext();
        return null;
    }
    @Override
    public Object visit(InterpretingContext context, CodeOpAssignment a) throws InterpreterException {
        setValue(a.getResult(), getValue(a.rvalue, context.rm), context.rm);
        context.toNext();
        return null;
    }
    @Override
    public Object visit(InterpretingContext context, CodeOpCall c) throws InterpreterException {
        CodeMethod called;
        if(c.methodReference.method.context != Context.NON_STATIC)
            called = c.methodReference.method;
        else
            called = c.getCalled(getCodeClass(
                getValue(c.getThisArg(), context.rm)));
        List<RuntimeValue> argValues = getValues(c.args, context.rm);
        boolean execute = true;
        for(String s : called.annotations) {
            CompilerAnnotation annotation;
            switch(annotation = CompilerAnnotation.parse(s)) {
                case IGNORE:
                case MARKER:
                    execute = false;
                    break;

                case START_THREAD:
                    if(called.context != Context.NON_STATIC)
                        throw new InterpreterException(called.getStreamPos(),
                                "method starting a thread can not be static");
                    newPendingThread(argValues.get(0), context.rm);
                    break;

                case INTERRUPT:
                    throw new InterpreterException(c.getStreamPos(),
                            "a thread can not be interrupted within the interpreter");

                case BARRIER_ACTIVATE:
                    // must be a non--static method
                    RuntimeObject thiS = (RuntimeObject)argValues.get(0).value;
                    boolean directional;
                    switch(argValues.size()) {
                        case 2:
                            directional = false;
                            break;
                            
                        case 3:
                            directional = true;
                            break;
                            
                        default:
                            throw new RuntimeException("unexpected number of arguments");
                    }
                    long numUsers;
                    long numProducers;
                    long numConsumers;
                    if(directional) {
                        numProducers = argValues.get(1).getMaxPrecisionInteger();
                        numConsumers = argValues.get(2).getMaxPrecisionInteger();
                        numUsers = numProducers + numConsumers;
                    } else {
                        numUsers = argValues.get(1).getMaxPrecisionInteger();
                        numProducers = 0;
                        numConsumers = 0;
                    }
                    barrierThreadsNum.put(thiS, new BarrierParties(c.getStreamPos(),
                            directional, numUsers, numProducers, numConsumers));
                    break;
                    
                case PRINT:
                case PRINTLN:
                    String p = argValues.get(c.isStatic() ? 0 : 1).getString();
                    if(outWriter == null)
                        System.out.println(INTERPRETER_PRINT_PREFIX + p);
                    else {
                        String mappedPos = "";
                        if(annotation == CompilerAnnotation.PRINTLN) {
                            if(outPosConverter != null)
                                mappedPos = outPosConverter.convertPos(c.getStreamPos());
                            mappedPos += "\n";
                        }
                        outWriter.print(p + mappedPos);
                    }
                    break;
                    
                case MODEL_NAME:
                    if(model == null)
                        throw new InterpreterException(c.getStreamPos(),
                                "no model control, can not set name");
                    else {
                        RuntimeObject object = argValues.get(0).getReferencedObject();
                        if(object == null)
                            throw new InterpreterException(c.getStreamPos(),
                                    "naming convention set on null");
                        model.setNamingConvention(object,
                                argValues.get(1).getString(), argValues.get(2).getString());
                    }
                    break;
                    
                case MODEL_CHECK:
                    if(model == null)
                        throw new InterpreterException(c.getStreamPos(),
                                "no model control, can not add property");
                    else
                        model.addProperty(argValues.get(0).getString(), argValues.get(1).
                                getString());
                    break;
                    
                case MODEL_STATE_OR:
                case MODEL_STATE_AND:
                    throw new InterpreterException(c.getStreamPos(),
                            "state can not be specifed here");
                    
                case MODEL_PLAYER:
                    RuntimeObject player = argValues.get(0).getReferencedObject();
                    if(model == null)
                        throw new InterpreterException(c.getStreamPos(),
                                "no model control, can not add player");
                    else if(model.isPlayer(player))
                        throw new InterpreterException(c.getStreamPos(),
                                player.getUniqueName() + " already has a player name");
                    else
                        model.addPlayer(player, argValues.get(1).getString());
                    break;
                    
                case MODEL_IS_STATISTICAL:
                    setValue(c.getResult(), new RuntimeValue(compilation.modelControl.
                            isStatisticalHint()), context.rm);
                    break;

                case IO_GET_ROW_INDEX:
                case IO_GET_NEXT:
                case IO_ADD_NEXT:
                case IO_CLOSE:
                    TuplesIO io = this.io.get(argValues.get(0).getReferencedObject().
                            getUniqueName());
                    if(io == null)
                        throw new InterpreterException(c.getStreamPos(),
                                "i/o stream closed");
                    RuntimeValue ioReturn;
                    try {
                        ioReturn = handleIO(context, io, annotation, argValues, c);
                    } catch(IOException e) {
                        throw new InterpreterException(c.getStreamPos(),
                                e.getMessage());
                    }
                    if(c.getResult() != null)
                        setValue(c.getResult(), ioReturn, context.rm);
                    break;
                    
                case INTERPRETER_STOP:
                    throw new InterpreterException(STOP_INTERPRETER_POS, "");
                    
                case MODEL_GET_INT_CONST:
                case MODEL_GET_DOUBLE_CONST:
                {
                    if(context.rm.method.isStaticInitializationMethod() &&
                            c.getResult() instanceof CodeFieldDereference &&
                            ((CodeFieldDereference)c.getResult()).variable.context !=
                                Context.NON_STATIC) {
                        CodeVariable variable = ((CodeFieldDereference)c.getResult()).variable;
                        if(!variable.finaL)
                            throw new InterpreterException(variable.getStreamPos(),
                                    "external constant must be final: " +
                                    argValues.get(0).getString());
                        // direct assignment of a static field by an external constant --
                        // the only allowed usage of unknown constants within the interpreter
                        //
                        // put a meaningless value, but remember, that in reality it is an
                        // unknown constant
                        switch(annotation) {
                            case MODEL_GET_INT_CONST:
                                setValue(c.getResult(), new RuntimeValue(-1), context.rm);
                                break;
                                
                            case MODEL_GET_DOUBLE_CONST:
                                setValue(c.getResult(), new RuntimeValue(-1.0), context.rm);
                                break;
                                
                            default:
                                throw new RuntimeException("unexpected annotation");
                        }
                        unknownConstants.add(variable);
                    } else
                        throw new InterpreterException(c.getStreamPos(),
                                "constant undefined but required by the interpreter: " +
                                argValues.get(0).getString());
                    break;
                }
                    
                //
                // binary mathematical functions
                //
                case MATH_POW:
                case MATH_MIN:
                case MATH_MAX:
                {
                    if(c.getResult().getResultType().getPrimitive() ==
                            Type.PrimitiveOrVoid.LONG ||
                            c.getResult().getResultType().getPrimitive() ==
                            Type.PrimitiveOrVoid.INT) {
                        long b = argValues.get(0).getMaxPrecisionInteger();
                        long e = argValues.get(1).getMaxPrecisionInteger();
                        boolean toInt = c.getResult().getResultType().getPrimitive() ==
                            Type.PrimitiveOrVoid.INT;
                        long r;
                        switch(annotation) {
                            case MATH_POW:
                                r = 1;
                                while(e > 0) {
                                    r *= b;
                                    --e;
                                }
                                break;
                                
                            case MATH_MIN:
                                r = Math.min(b, e);
                                break;
                                
                            case MATH_MAX:
                                r = Math.max(b, e);
                                break;
                                
                            default:
                                throw new RuntimeException("unnknown");
                        }
                        RuntimeValue rv;
                        if(toInt)
                            rv = new RuntimeValue((int)r);
                        else
                            rv = new RuntimeValue(r);
                        setValue(c.getResult(), rv, context.rm);
                    } else if(c.getResult().getResultType().getPrimitive() ==
                            Type.PrimitiveOrVoid.DOUBLE) {
                        double b = argValues.get(0).getMaxPrecisionFloatingPoint();
                        double e = argValues.get(1).getMaxPrecisionFloatingPoint();
                        double value;
                        switch(annotation) {
                            case MATH_POW:
                                value = Math.pow(b, e);
                                break;
                                
                            case MATH_MIN:
                                value = Math.min(b, e);
                                break;
                                
                            case MATH_MAX:
                                value = Math.max(b, e);
                                break;
                                
                            default:
                                throw new RuntimeException("unnknown");
                        }
                        setValue(c.getResult(), new RuntimeValue(value), context.rm);
                    } else
                        throw new InterpreterException(called.getStreamPos(),
                            "unsupported compiler-annotated function with the return value of " +
                            c.getResult().getResultType().getPrimitive().toString());
                    break;
                }  
                //
                // binary mathematical functions
                //
                case SUBSTRING:
                {
                    String string = argValues.get(0).getString();
                    int start = -1;
                    int stop = -1;
                    String substring;
                    try {
                        start = Math.toIntExact(argValues.get(1).getMaxPrecisionInteger());
                        stop = Math.toIntExact(argValues.get(2).getMaxPrecisionInteger());
                        substring = string.substring(start, stop);
                    } catch(ArithmeticException e) {
                        InterpreterException ie = new InterpreterException(called.getStreamPos(),
                            "arithmetic overflow");
                        ie.addStackFrame(c);
                        throw ie;
                    } catch(IndexOutOfBoundsException e) {
                        String message;
                        if(start < 0)
                            message = "start position = " + start + " is negative";
                        else if(stop < 0)
                            message = "stop position = " + stop + " is negative";
                        else if(start > string.length())
                            message = "start position = " + start + " past the end";
                        else if(stop > string.length())
                            message = "stop position = " + stop + " past the end";
                        else
                            throw new RuntimeException("unexpected index out of bounds");
                        InterpreterException ie = new InterpreterException(called.getStreamPos(),
                            "index out of bounds: " + message);
                        ie.addStackFrame(c);
                        throw ie;
                    }
                    setValue(c.getResult(), new RuntimeValue(substring, null), context.rm);
                    break;
                }  
                case UNKNOWN_ANNOTATION:
                    // the interpreter unknown non--compiler annotations
                    break;
                    
                default:
                    throw new InterpreterException(called.getStreamPos(),
                        "unsupported compiler annotation for call " + s);
            }
        }
        if(execute) {
            RuntimeMethod calledRm = interpret(context, called, argValues);
            context.rm.pendingThreads.addAll(calledRm.pendingThreads);
            calledRm.pendingThreads.clear();
            if(c.getResult() != null)
                setValue(c.getResult(), calledRm.getValue(
                        this, calledRm, called.returnValue), context.rm);
        }
        context.toNext();
        return null;
    }
    @Override
    public Object visit(InterpretingContext context, CodeOpJump op) throws InterpreterException {
        context.index = op.gotoLabelRef.codeIndex;
        return null;
    }
    @Override
    public Object visit(InterpretingContext context, CodeOpNone op) throws InterpreterException {
        throw new InterpreterException(op.getStreamPos(), "operation not supported: " + op.toString());
        // context.toNext();
        // return null;
    }
    @Override
    public Object visit(InterpretingContext context, CodeOpReturn op) throws InterpreterException {
        context.index = -1;
        return null;
    }
    @Override
    public Object visit(InterpretingContext context, CodeOpThrow op) throws InterpreterException {
        String message;
        if(op.thrown != null) {
            RuntimeObject o = getValue(new RangedCodeValue(op.thrown), context.rm).getReferencedObject();
            message = ": " + o.getValue(context.interpreter, context.rm, o.codeClass.
                    lookupField("message")).getString();
        } else
            message = "";
        throw new InterpreterException(op.getStreamPos(), "exception" +
                message);
    }
    @Override
    public Object visit(InterpretingContext context, CodeOpSynchronize op) throws InterpreterException {
        throw new InterpreterException(op.getStreamPos(), "operation not supported: " + op.toString());
        // context.toNext();
        // return null;
    }
    /**
     * Handles a matrix i/o call.
     * 
     * @param context interpreting context
     * @param io <code>MatrixIO<code> object
     * @param annotation annotation of the i/o method
     * @param argValues arguments of the i/o method
     * @param result call operation
     */
    private RuntimeValue handleIO(InterpretingContext context, TuplesIO io,
            CompilerAnnotation annotation, List<RuntimeValue> argValues,
            CodeOpCall call) throws IOException, InterpreterException {
        RuntimeValue ret;
        String name = argValues.get(0).getReferencedObject().getUniqueName();
        switch(annotation) {
            case IO_GET_ROW_INDEX:
            {
                ret = new RuntimeValue(io.getLineIndex());
                break;
            }
            case IO_GET_NEXT:
            {
                double[] out = io.getNext();
                if(out == null)
                    ret = new RuntimeValue();
                else {
                    Type type = new Type(Type.PrimitiveOrVoid.DOUBLE);
                    type.increaseDimension(null);
                    List<RuntimeValue> sizeValues = new LinkedList<>();
                    sizeValues.add(new RuntimeValue(out.length));
                    RuntimeValue array = newArrays(context,
                            type, sizeValues, name + "_line" + io.getLineIndex());
                    ArrayStore as = (ArrayStore)array.value;
                    for(int index = 0; index < out.length; ++index)
                        as.setElement(context.interpreter, context.rm, call.getResult(),
                                index, new RuntimeValue(out[index]));
                    ret = array;
                }
                break;
            }
            case IO_ADD_NEXT:
            {
                ArrayStore as = (ArrayStore)argValues.get(1).value;
                double[] in = new double[as.getLength()];
                AbstractCodeValue source = call.args.get(1).value;
                if(!(source instanceof AbstractCodeDereference))
                    source = null;
                for(int index = 0; index < as.getLength(); ++index)
                    in[index] = as.getElement(context.interpreter, context.rm,
                            (AbstractCodeDereference)source, index).getDouble();
                io.addNext(in);
                ret = new RuntimeValue();
                break;
            }   
            case IO_CLOSE:
            {
                this.io.remove(name);
                io.close();
                ret = new RuntimeValue();
                break;
            }
            default:
                throw new RuntimeException("unknown i/o method");
        }
        return ret;
    }
}
