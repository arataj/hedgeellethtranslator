/*
 * CodeArithmetics.java
 *
 * Created on Mar 10, 2009, 10:41:30 AM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.interpreter.op;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.TypeRangeMap;
import pl.gliwice.iitis.hedgeelleth.compiler.code.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.CodeFieldDereference;
import pl.gliwice.iitis.hedgeelleth.compiler.code.op.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Literal;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Type;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Type.PrimitiveOrVoid;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.CompilerException;
import pl.gliwice.iitis.hedgeelleth.interpreter.exception.InterpreterException;
import pl.gliwice.iitis.hedgeelleth.interpreter.exception.InterpreterFatalException;
import pl.gliwice.iitis.hedgeelleth.interpreter.InterpretingContext;
import pl.gliwice.iitis.hedgeelleth.interpreter.RuntimeValue;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.RuntimeObject;
import pl.gliwice.iitis.hedgeelleth.interpreter.exception.InterpreterMissingException;

/**
 * A stateless class for performing code arithmetics, either compile--time
 * if the interptering context <code>c</code> is null or runtime otherwise.<br>
 *
 * If <code>c</code> is null, then the returned <code>RuntimeValue</code> is guaranteed
 * to have its type within the subclasses of <code>Literal</code>.
 *
 * @author Artur Rataj
 */
public class CodeArithmetics {
    /**
     * An unique prefix of a runtime class cast interpreter exception.
     */
    public final static String RUNTIME_CLASS_CAST_MESSAGE = "runtime class cast exception";
    /**
     * Performs either a compile--time or, if the interpreting context
     * is available, a runtime estimation of a value. In the latter case, the
     * value is guaranteed to be <code>RuntimeValue</code>.<br>
     *
     * The interpreter exception can posssibly be thrown only if <code>c</code>
     * is not null or a value is out of primitive bound.
     * 
     * If the expression's primitive range can not be checked because
     * there is no interpreting context, this method returns null as well.
     *
     * @param c                         interpreter's context for runtime
     *                                  evaluation, can be null if if
     *                                  <code>v</code> is a constant
     * @param v                         ranged code value to estimate
     * @return                          resulting value or null if the
     *                                  expression could not be computed
     */
    protected static Literal getValue(InterpretingContext c, RangedCodeValue v)
            throws InterpreterException {
        Literal out = null;
        if(v.value instanceof CodeConstant) {
            out = ((CodeConstant)v.value).value;
            if(v.range != null) {
                try {
                    // checked because the interpreter has been bypassed,
                    // along with its range checking
                    v.range.check(out,
                            (c != null && c.useVariables != InterpretingContext.UseVariables.NO) ? c.interpreter : null,
                            (c != null && c.useVariables != InterpretingContext.UseVariables.NO) ? c.rm : null);
                } catch(InterpreterMissingException e) {
                    out = null;
                }
            }
        } else if(c != null && c.useVariables == InterpretingContext.UseVariables.ALL) {
            out = c.interpreter.getValue(v, c.rm);
        } else if(c != null && (v.value instanceof CodeFieldDereference) &&
                ((CodeFieldDereference)v.value).isLocal() &&
                c.useVariables == InterpretingContext.UseVariables.LOCAL) {
            out = c.interpreter.getValue(v, c.rm);
        }
        return out;
    }
    /**
     * Compute the value of an unary expression, without the assignment to
     * the expression's result. The interpreter exception can posssibly
     * be thrown only if <code>c</code> is not null or a value is out of
     * primitive bound.<br>
     *
     * For cast expressions, assumes default variable bounds. If this should
     * not be like that, a proper map <code>castRanges</code>
     * should be supplied.
     *
     * @param c                         interpreter's context for runtime
     *                                  evaluation or null
     * @param ue                        expression to compute
     * @param castRanges                custom ranges for the cast operator,
     *                                  null for Java default
     * @return                          resulting value or null if the
     *                                  expression could not be computed
     */
    public static RuntimeValue computeValue(InterpretingContext c, CodeOpUnaryExpression ue,
            TypeRangeMap castRanges) throws InterpreterException {
        RuntimeValue out = null;
        try {
            Literal sub = getValue(c, ue.sub);
            if(sub != null) {
                switch(ue.op) {
                    case UNARY_BITWISE_COMPLEMENT:
                    {
                        PrimitiveOrVoid p = sub.type.getPrimitive();
                        switch(p) {
                            case INT:
                                out = new RuntimeValue((int)~
                                        sub.getMaxPrecisionInteger());
                                break;

                            case LONG:
                                out = new RuntimeValue(~
                                        sub.getMaxPrecisionInteger());
                                break;

                            default:
                                throw new RuntimeException("unknown or invalid type: " + p);
                        }
                        break;
                    }
                    case UNARY_CONDITIONAL_NEGATION:
                    {
                        out = new RuntimeValue(!sub.getBoolean());
                        break;
                    }
                    case UNARY_NEGATION:
                    {
                        PrimitiveOrVoid p = sub.type.getPrimitive();
                        switch(p) {
                            case INT:
                                out = new RuntimeValue(Math.toIntExact(
                                        -sub.getMaxPrecisionInteger()));
                                break;

                            case LONG:
                                out = new RuntimeValue(
                                        -sub.getMaxPrecisionInteger());
                                break;

                            case FLOAT:
                                out = new RuntimeValue((float)-
                                        sub.getMaxPrecisionFloatingPoint());
                                break;

                            case DOUBLE:
                                out = new RuntimeValue((double)-
                                        sub.getMaxPrecisionFloatingPoint());
                                break;

                            default:
                                throw new RuntimeException("unknown or invalid type: " + p);
                        }
                        break;
                    }
                    case UNARY_CAST:
                    {
                        if(sub.type.isPrimitive()) {
                            // cast of a numeric type to another numeric type
                            PrimitiveOrVoid p = ue.objectType.getPrimitive();
                            if(castRanges == null)
                                castRanges = new TypeRangeMap(p, false);
                            out = new RuntimeValue(castRanges.computeCastModulo(p, sub));
                        } else {
                            // only possible if interpreter is known
                            if(c != null) {
                                // test if the cast is valid
                                RuntimeObject runtime = c.interpreter.getReferencedOrNull(sub);
                                if(runtime == null) {
                                    // casting null constant or null reference
                                    out = new RuntimeValue();
                                } else {
                                    // casting a non--null reference, check type
                                    // of casted object
                                    RuntimeObject runtimeClass = runtime.runtimeClass;
                                    if(runtimeClass == null)
                                       throw new RuntimeException("casting a class: " +
                                                ((RuntimeValue)sub).getReferencedObject());
                                    CodeClass base = runtimeClass.codeClass;
                                    CodeClass target = c.interpreter.getCodeClass(ue.objectType);
                                    while(base != null && base != target)
                                        base = base.extendS;
                                    if(base == null)
                                        throw new InterpreterException(ue.getStreamPos(),
                                                RUNTIME_CLASS_CAST_MESSAGE + ": " +
                                                runtimeClass.codeClass.name + " to " +
                                                target.name);
                                    out = c.interpreter.newReference(runtime);
                                }
                            }
                        }
                        break;
                    }
                    case UNARY_INSTANCE_OF:
                    case UNARY_INSTANCE_OF_NOT_NULL:
                    {
                        // only possible if interpreter is known
                        if(c != null) {
                            RuntimeObject runtime = c.interpreter.getReferencedOrNull(
                                    sub);
                            if(runtime == null) {
                                if(ue.op == AbstractCodeOp.Op.UNARY_INSTANCE_OF_NOT_NULL)
                                    throw new InterpreterException(ue.getStreamPos(),
                                            RUNTIME_CLASS_CAST_MESSAGE +
                                            " not allowing null: " +
                                            "null to " + ue.objectType.toNameString());
                                else
                                    // <code>null instanceof</code> is always false
                                    out = new RuntimeValue(false);
                            } else {
                                RuntimeObject clazz = runtime.runtimeClass;
                                if(clazz == null)
                                   throw new RuntimeException("class is not an instance: " +
                                            ((RuntimeValue)sub).getReferencedObject());
                                CodeClass objectClass = runtime.codeClass;
                                CodeClass comparedClass = c.interpreter.getCodeClass(ue.objectType);
                                out = new RuntimeValue(objectClass.isEqualToOrExtending(
                                        comparedClass));
                            }
                        }
                        break;
                    }
                    case UNARY_ABS:
                    {
                        PrimitiveOrVoid p = sub.type.getPrimitive();
                        switch(p) {
                            case INT:
                                out = new RuntimeValue(Math.toIntExact(
                                        Math.abs(sub.getMaxPrecisionInteger())));
                                break;

                            case LONG:
                                out = new RuntimeValue(Math.abs(
                                        sub.getMaxPrecisionInteger()));
                                break;

                            case FLOAT:
                                out = new RuntimeValue((float)Math.abs(
                                        sub.getMaxPrecisionFloatingPoint()));
                                break;

                            case DOUBLE:
                                out = new RuntimeValue((double)Math.abs(
                                        sub.getMaxPrecisionFloatingPoint()));
                                break;

                            default:
                                throw new RuntimeException("unknown or invalid type: " + p);
                        }
                        break;
                    }
                    default:
                    {
                        throw new RuntimeException("invalid operator " +
                                "type: " + ue.op);
                    }
                }
            }
        } catch(InterpreterException e) {
            e.completePos(ue.getStreamPos());
            throw e;
        } catch(ArithmeticException e) {
            throw new InterpreterException(ue.getStreamPos(), "integer overflow");
        }
        return out;
    }
    /**
     * Compute the value of an unary expression, without the assignment to
     * the expression's result, without the possibility of runtime evaluation
     * using the interpreter. See the called method
     * <code>computeValue(InterpretingContext, CodeOpUnaryExpression)</code>
     * for details. This is a convenience method.
     *
     * @param ue                        expression to compute
     * @param castRanges                custom ranges for the cast operator,
     *                                  null for Java default
     * @return                          resulting value or null if the
     *                                  expression could not be computed
     */
    public static Literal computeValue(CodeOpUnaryExpression ue, TypeRangeMap castRanges)
            throws InterpreterException {
        return computeValue(null, ue, castRanges);
    }
    /**
     * Compute the value of a binary expression, without the assignment to
     * the expression's result. The interpreter exception can posssibly
     * be thrown only if <code>c</code> is not null.<br>
     *
     * @param c                         interpreter's context for runtime
     *                                  evaluation or null
     * @param be                        expression to compute
     * @return                          resulting value or null if the
     *                                  expression could not be computed
     */
    public static RuntimeValue computeValue(InterpretingContext c, CodeOpBinaryExpression be)
            throws InterpreterException {
        RuntimeValue out = null;
        try {
            Literal left = getValue(c, be.left);
            Literal right = getValue(c, be.right);
            if(left != null && right != null) {
                if(left.type.isBoolean() && right.type.isBoolean()) {
                    boolean leftB = left.getBoolean();
                    boolean rightB = right.getBoolean();
                    boolean result = false;
                    switch(be.op) {
                        case BINARY_EQUAL:
                            result = leftB == rightB;
                            break;

                        case BINARY_INEQUAL:
                            result = leftB != rightB;
                            break;

                        default:
                            throw new RuntimeException("invalid operator " +
                                    "for boolean operands: " + be.op);
                    }
                    out = new RuntimeValue(result);
                } else if(left.type.isString(null) || right.type.isString(null)) {
                    switch(be.op) {
                        case BINARY_PLUS:
                            out = new RuntimeValue(left.toJavaString() + right.toJavaString(),
                                null);
                            break;
                            
                        case BINARY_EQUAL:
                        case BINARY_INEQUAL:
                            boolean equal;
                            if(left.type.type == null && right.type.type == null)
                                equal = true;
                            else if(left.type.type == null || right.type.type == null)
                                equal = false;
                            else 
                                equal = left.getString().equals(right.getString());
                            if(be.op == AbstractCodeOp.Op.BINARY_INEQUAL)
                                equal = !equal;
                            out = new RuntimeValue(equal);
                            break;
                            
                        default:
                            throw new RuntimeException("illegal operator");
                    }
                } else {
                    int maxOperandPrecision = Math.max(
                            left.type.numericPrecision(),
                            right.type.numericPrecision());
                    if(maxOperandPrecision >= Type.NumericPrecision.FLOAT) {
                        // float operation
                        double leftD = left.getMaxPrecisionFloatingPoint();
                        double rightD = right.getMaxPrecisionFloatingPoint();
                        double result = Double.NaN;
                        boolean booleanResult = false;
                        boolean resultIsBoolean = false;
                        switch(be.op) {
                            case BINARY_DIVIDE:
                                result = leftD/rightD;
                                break;

                            case BINARY_MINUS:
                                result = leftD - rightD;
                                break;

                            case BINARY_MODULUS:
                                result = leftD%rightD;
                                break;
                                
                            case BINARY_MODULUS_POS:
                                if(leftD >= 0.0)
                                    result = leftD%rightD;
                                else
                                    result = (leftD + 1)%rightD + (rightD - 1);
                                break;

                            case BINARY_MULTIPLY:
                                result = leftD*rightD;
                                break;

                            case BINARY_PLUS:
                                result = leftD + rightD;
                                break;

                            case BINARY_EQUAL:
                                booleanResult = leftD == rightD;
                                resultIsBoolean = true;
                                break;

                            case BINARY_INEQUAL:
                                booleanResult = leftD != rightD;
                                resultIsBoolean = true;
                                break;

                            case BINARY_LESS:
                                booleanResult = leftD < rightD;
                                resultIsBoolean = true;
                                break;

                            case BINARY_LESS_OR_EQUAL:
                                booleanResult = leftD <= rightD;
                                resultIsBoolean = true;
                                break;

                            default:
                                throw new RuntimeException("invalid operator " +
                                        "for floating point precision: " + be.op);
                        }
                        if(resultIsBoolean)
                            out = new RuntimeValue(booleanResult);
                        else
                            switch(maxOperandPrecision) {
                                case Type.NumericPrecision.FLOAT:
                                    out = new RuntimeValue((float)result);
                                    break;

                                case Type.NumericPrecision.DOUBLE:
                                    out = new RuntimeValue(result);
                                    break;

                                default:
                                    throw new RuntimeException("unknown or invalid precision: " +
                                            maxOperandPrecision);
                            }
                    } else if(maxOperandPrecision > 0) {
                        // integer operation
                        long leftL = left.getMaxPrecisionInteger();
                        long rightL = right.getMaxPrecisionInteger();
                        long result = -1;
                        boolean divisionByZero = false;
                        boolean booleanResult = false;
                        boolean resultIsBoolean = false;
                        switch(be.op) {
                            case BINARY_DIVIDE:
                                if(rightL == 0)
                                    divisionByZero = true;
                                else
                                    result = leftL/rightL;
                                break;

                            case BINARY_INCLUSIVE_OR:
                                result = leftL|rightL;
                                break;

                            case BINARY_EXCLUSIVE_OR:
                                result = leftL^rightL;
                                break;

                            case BINARY_AND:
                                result = leftL&rightL;
                                break;

                            case BINARY_MINUS:
                                result = Math.subtractExact(leftL, rightL);
                                break;

                            case BINARY_MODULUS:
                                result = leftL%rightL;
                                break;

                            case BINARY_MODULUS_POS:
                                if(leftL >= 0.0)
                                    result = leftL%rightL;
                                else
                                    result = Math.addExact(Math.addExact(leftL, 1)%rightL,
                                            Math.subtractExact(rightL, 1));
                                break;

                            case BINARY_MULTIPLY:
                                result = Math.multiplyExact(leftL, rightL);
                                break;

                            case BINARY_PLUS:
                                result = Math.addExact(leftL, rightL);
                                break;

                            case BINARY_SHIFT_LEFT:
                                if(rightL >= 62)
                                    throw new InterpreterException(be.getStreamPos(),
                                                        "integer overflow");
                                result = Math.multiplyExact(leftL, 1L << rightL);
                                break;

                            case BINARY_SHIFT_RIGHT:
                                result = leftL>>rightL;
                                break;

                            case BINARY_UNSIGNED_SHIFT_RIGHT:
                                result = leftL>>>rightL;
                                break;

                            case BINARY_EQUAL:
                                booleanResult = leftL == rightL;
                                resultIsBoolean = true;
                                break;

                            case BINARY_INEQUAL:
                                booleanResult = leftL != rightL;
                                resultIsBoolean = true;
                                break;

                            case BINARY_LESS:
                                booleanResult = leftL < rightL;
                                resultIsBoolean = true;
                                break;

                            case BINARY_LESS_OR_EQUAL:
                                booleanResult = leftL <= rightL;
                                resultIsBoolean = true;
                                break;

                            default:
                                throw new RuntimeException("invalid operator " +
                                        "type: " + be.op);
                        }
                        if(resultIsBoolean)
                            out = new RuntimeValue(booleanResult);
                        else if(!divisionByZero) {
                            switch(maxOperandPrecision) {
                                case Type.NumericPrecision.INT:
                                    out = new RuntimeValue((int)result);
                                    break;

                                case Type.NumericPrecision.LONG:
                                    out = new RuntimeValue(result);
                                    break;

                                default:
                                    throw new RuntimeException("unknown or invalid precision: " +
                                            maxOperandPrecision);
                            }
                        } else if(c != null)
                            // allow division by zero to be compiled, throw an exception
                            // only when interpreting
                            throw new InterpreterException(be.getStreamPos(), "division by zero");
                    } else {
                        // interpreter is needed to compare references
                        if(c != null) {
                            RuntimeObject leftR = c.interpreter.getReferencedOrNull(left);
                            RuntimeObject rightR = c.interpreter.getReferencedOrNull(right);
                            boolean equals = leftR == rightR;
                            switch(be.op) {
                                case BINARY_EQUAL:
                                    // do nothing
                                    break;

                                case BINARY_INEQUAL:
                                    equals = !equals;
                                    break;

                                default:
                                    throw new RuntimeException("invalid operand " +
                                            "for object comparison");

                            }
                            out = new RuntimeValue(equals);
                        }
                    }
                }
            }
        } catch(InterpreterException e) {
            e.completePos(be.getStreamPos());
            throw e;
        } catch(ArithmeticException e) {
            throw new InterpreterException(be.getStreamPos(), "integer overflow");
        }
        return out;
    }
    /**
     * Compute the value of a binary expression, without the assignment to
     * the expression's result, at compile time. It is a convenience
     * method.
     *
     * @param be                        expression to compute
     * @return                          resulting value or null if the
     *                                  expression could not be computed
     */
    public static Literal computeValue(CodeOpBinaryExpression be) throws InterpreterException {
        return computeValue(null, be);
    }
}
