/*
 * ArrayTypeDescription.java
 *
 * Created on Feb 4, 2009
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.languagedescription;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.Type;

/**
 * Description of an array type. Specifies element type, that can be
 * either a certain primitive or a java class.
 *
 * @author Artur Rataj
 */
public class ArrayTypeDescription implements Comparable<ArrayTypeDescription> {
    /**
     * Type of the array element, null for a java class.
     */
    public Type.PrimitiveOrVoid elementType;

    /**
     * Creates a new instance of ArrayTypeDescription.
     *
     * @param elementType               type of the array element,
     *                                  null for a java class
     */
    public ArrayTypeDescription(Type.PrimitiveOrVoid elementType) {
        this.elementType = elementType;
    }
    @Override
    public int compareTo(ArrayTypeDescription d) {
        if(elementType == d.elementType)
            return 0;
        else if(elementType == null)
            // object arrays are sorted after primitive arrays
            return 1;
        else if(d.elementType == null)
            return -1;
        else
            return elementType.compareTo(d.elementType);
    }
    @Override
    public boolean equals(Object o) {
        if(o instanceof ArrayTypeDescription)
            return compareTo((ArrayTypeDescription)o) == 0;
        else
            return false;
    }
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + (this.elementType != null ? this.elementType.hashCode() : 0);
        return hash;
    }
    @Override
    public String toString() {
        String s;
        if(elementType == null)
            s = "OBJECT";
        else
            s = elementType.toString();
        return s.toLowerCase();
    }
}
