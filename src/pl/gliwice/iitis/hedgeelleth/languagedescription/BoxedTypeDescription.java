/*
 * BoxedTypeDescription.java
 *
 * Created Jun 22, 2018
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.languagedescription;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.Type;

/**
 * A primitive type with boxing/unboxing enabled.
 * 
 * @author Artur Rataj
 */
public class BoxedTypeDescription implements Comparable<BoxedTypeDescription> {
    /**
     * Primitive type.
     */
    public Type.PrimitiveOrVoid type;
    /**
     * Class representing a nullable variant of the primitive type.
     */
    public String className;
    /**
     * If a short form <code>&ltprimitive type&gr;?</code> is allowed.
     * This is false for java, as there are no explicit nullable primitives.
     */
    public boolean shortAllowed;
    
    /**
     * Creates a new instance of PrimitiveTypeDescription. 
     * 
     * @param type primitive type
     * @param className class representing a nullable variant of the primitive type
     * @param shortAllowed if a short form <code>&ltprimitive type&gr;?</code> is
     * allowed
     */
    public BoxedTypeDescription(Type.PrimitiveOrVoid type, String className,
            boolean shortAllowed) {
        this.type = type;
        this.className = className;
        this.shortAllowed = shortAllowed;
    }
    @Override
    public int compareTo(BoxedTypeDescription pt) {
        int i = type.compareTo(pt.type);
        if(i == 0)
            i = className.compareTo(pt.className);
        if(i == 0)
            i = new Boolean(shortAllowed).compareTo(shortAllowed);
        return i;
    }
    @Override
    public boolean equals(Object o) {
        if(o instanceof BoxedTypeDescription)
            return compareTo((BoxedTypeDescription)o) == 0;
        else
            return false;
    }
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + (this.type != null ? this.type.hashCode() : 0);
        hash = 79 * hash + (this.className != null ? this.className.hashCode() : 0);
        hash = 79 * hash + (shortAllowed ? 1 : 0);
        return hash;
    }
}
