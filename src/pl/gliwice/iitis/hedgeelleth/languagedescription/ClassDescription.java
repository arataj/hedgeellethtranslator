/*
 * ClassDescription.java
 *
 * Created on Mar 5, 2009, 4:12:54 PM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.languagedescription;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * A class description. Contains the (function, source name) pairs of
 * a special class, and (function, source name) definitions of fields.
 *
 * @author Artur Rataj
 */
public class ClassDescription {
    /**
     * Position of the definition of this class description
     * in the source stream.
     */
    public StreamPos pos;
    /**
     * Function of the special class.
     */
    public String function;
    /**
     * Class source name.
     */
    public String sourceName;
    /**
     * Fields, empty list for none. Keyed with field function, value is
     * field's source name.
     */
    public Map<String, String> fields;

    /**
     * Creates a new instance of ClassDescription.
     *
     * @param pos                       position of the definition of
     *                                  this class description in the
     *                                  source stream
     */
    public ClassDescription(StreamPos pos) {
        this.pos = pos;
        fields = new HashMap<String, String>();
    }
}
