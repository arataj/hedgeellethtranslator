/*
 * Comment.java
 *
 * Created on Sep 19, 2008
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.languagedescription;

import java.util.*;

/**
 * A comment type -- single line with a custom prefix, multi--line
 * "* *" type, special multi--line "** *" type.
 * 
 * @author Artur Rataj
 */
public enum CommentType {
    // the order defines order of definitions in lexer
    SINGLE_LINE,
    SPECIAL_MULTI_LINE,
    MULTI_LINE;
}
