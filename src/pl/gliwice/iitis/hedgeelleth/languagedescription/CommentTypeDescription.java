/*
 * CommentTypeDescription.java
 *
 * Created on Nov 18, 2008
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.languagedescription;

/**
 * Description of comment: type and, in case of single line comments,
 * prefix.
 * 
 * @author Artur Rataj
 */
public class CommentTypeDescription implements Comparable<CommentTypeDescription> {
    /**
     * Type of this comment.
     */
    public CommentType type;
    /**
     * Prefix of a single line comment, null for other comment types.
     */
    public String singleLinePrefix = null;
    
    /**
     * Creates a new instance of CommentTypeDescription. 
     * 
     * @param type                      one of pre--defined comment types
     * @param singleLinePrefix          in case of a single line comment, it
     *                                  defines the prefix, otherwise must
     *                                  be null
     */
    public CommentTypeDescription(CommentType type,
            String singleLinePrefix) {
        this.type = type;
        this.singleLinePrefix = singleLinePrefix;
        if(type != CommentType.SINGLE_LINE &&
                this.singleLinePrefix != null)
            throw new RuntimeException("prefix can be specified only for " +
                    "single--line comments");
    }
    @Override
    public int compareTo(CommentTypeDescription ct) {
        int i = type.compareTo(ct.type);
        if(i == 0)
            i = singleLinePrefix.compareTo(ct.singleLinePrefix);
        return i;
    }
    @Override
    public boolean equals(Object o) {
        if(o instanceof CommentTypeDescription)
            return compareTo((CommentTypeDescription)o) == 0;
        else
            return false;
    }
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + (this.type != null ? this.type.hashCode() : 0);
        hash = 53 * hash + (this.singleLinePrefix != null ? this.singleLinePrefix.hashCode() : 0);
        return hash;
    }
}
