/*
 * Comment.java
 *
 * Created on Sep 19, 2008
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.languagedescription;

/**
 * An identifier type -- standard identifier consists of a sequence
 * or letters and digits, but must begin with a letter. The ASCII version
 * limits characters to the ASCII set, the Unicode version supports letters
 * or digits from the Unicode set.
 * 
 * Additional characters, that can appear anywhere in the identifier, can
 * be specified.
 * 
 * @author Artur Rataj
 */
public enum IdentifierType {
    STANDARD_ASCII,
    SUBSET_LETTERS_UNICODE,
    STANDARD_UNICODE;
}
