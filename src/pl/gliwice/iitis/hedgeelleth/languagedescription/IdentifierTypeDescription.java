/*
 * IdentifierTypeDescription.java
 *
 * Created on Nov 18, 2008
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.languagedescription;

/**
 * Identifier description: type and miscelaneous characters.
 * 
 * @author Artur Rataj
 */
public class IdentifierTypeDescription {
    /**
     * Type of this identifier.
     */
    public IdentifierType type;
    /**
     * Additional characters, that can appear anywhere in the
     * identifier. Empty string if none.
     */
    public String miscCharacters;
    
    /**
     * Creates a new instance of IdentifierTypeDescription.
     * 
     * @param type                      type
     * @param miscCharacters            miscelaneous characters
     */
    public IdentifierTypeDescription(IdentifierType type,
            String miscCharacters) {
        this.type = type;
        this.miscCharacters = miscCharacters;
    }
}
