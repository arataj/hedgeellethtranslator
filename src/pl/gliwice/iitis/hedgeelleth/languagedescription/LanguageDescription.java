/*
 * LanguageDescription.java
 *
 * Created on Sep 19, 2008
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.languagedescription;

import java.util.*;

/**
 * A language description.
 * 
 * Sorts alphabetically some collections, what does not impact on the grammar
 * recognized, but helps generating parsers more similar to each other.
 * 
 * @author Artur Rataj
 */
public class LanguageDescription {
    /**
     * Short name of the language. Should start with an uppercase letter.
     */
    public String languageName;
    /**
     * Mime type of the language files.
     */
    public String mimeType;
    /**
     * If the lexer should collect all comment tokens in the
     * stream.
     */
    public boolean collectCommentTokens;
    /**
     * Parser package name.
     */
    public String packagE;
    /**
     * Parser class name.
     */
    public String clazz;
    /**
     * Parser imports.
     */
    public Set<String> imports;
    /**
     * Whitespace.
     */
    public Set<String> whitespace;
    /**
     * Comment types supported.
     */
    public Set<CommentTypeDescription> commentTypes;
    /**
     * Keywords.
     */
    public Set<String> keywords;
    /**
     * Separators.
     */
    public Set<String> separators;
    /**
     * Operators.
     */
    public Set<String> operators;
    /**
     * Illegal tokens outside of constants.
     */
    public Set<String> illegalTokens;
    /**
     * Identifier type.
     */
    public IdentifierTypeDescription identifierType;
    /**
     * Primitive types available.
     */
    public Set<PrimitiveTypeDescription> primitiveTypes;
    /**
     * Primitive types with boxing enabled.
     */
    public Set<BoxedTypeDescription> boxing;
    /**
     * Literal types available.
     */
    public Set<LiteralTypeDescription> literals;
    /**
     * Array types allowed. Empty list if no arrays are allowed.
     */
    public Set<ArrayTypeDescription> arrays;
    /**
     * Name of the object class, used in the source code.
     * See <code>AbstractFrontend.objectClassName</code> for details.<br>
     *
     * This value is copied to <code>AbstractFrontend.objectClassName</code> by
     * the generated parser.
     */
    public String objectClassName;
    /**
     * Name of the array class, used in the source code.
     * See <code>AbstractFrontend.arrayClassName</code> for details.<br>
     *
     * This value is copied to <code>AbstractFrontend.arrayClassName</code> by
     * the generated parser.
     */
    public String arrayClassName;
    /**
     * Name of the length field in the array class, used in the source code. See
     * <code>AbstractFrontend.arrayClassLengthFieldName</code> for details.<br>
     *
     * This value is copied to <code>AbstractFrontend.arrayClassLengthFieldName</code>
     * by the generated parser.
     */
    public String arrayClassLengthFieldName;
    /**
     * Name of the string class, used in the source code.
     * See <code>AbstractFrontend.stringClassName</code> for details.<br>
     *
     * This value is copied to <code>AbstractFrontend.stringClassName</code> by
     * the generated parser.
     */
    public String stringClassName;
    /**
     * Name of the length field in the string class, used in the source code. See
     * <code>AbstractFrontend.stringClassLengthFieldName</code> for details.<br>
     *
     * This value is copied to <code>AbstractFrontend.stringClassLengthFieldName</code>
     * by the generated parser.
     */
    public String stringClassLengthFieldName;
    /**
     * Name of the root exception class, used in the source code.
     * See <code>AbstractFrontend.rootExceptionClassName</code> for details.<br>
     *
     * This value is copied to <code>AbstractFrontend.rootExceptionClassName</code> by
     * the generated parser.
     */
    public String rootExceptionClassName;
    /**
     * Name of the message field in the root exception class, used in the source code.
     * See <code>AbstractFrontend.rootExceptionClassMessageFieldName</code> for
     * details.<br>
     *
     * This value is copied to <code>AbstractFrontend.rootExceptionClassMessageFieldName</code>
     * by the generated parser.
     */
    public String rootExceptionClassMessageFieldName;
    /**
     * Name of the unchecked exception class, used in the source code.
     * See <code>AbstractFrontend.uncheckedExceptionClassName</code> for details.<br>
     *
     * This value is copied to <code>AbstractFrontend.uncheckedExceptionClassName</code> by
     * the generated parser.
     */
    public String uncheckedExceptionClassName;
    /**
     * Default imports for each java class, empty list for none, null if classes are absent or
     * Hedgeelleth does not handle them.
     */
    public Set<String> defaultImports;
    /**
     * Contents of the code part, not pre--processed.
     */
    public VerbatimSection code;
    /**
     * Contents of the grammar part, not pre--processed.
     */
    public VerbatimSection grammar;
    /**
     * Prefix to access fields of the method's class, null to allow direct access
     * of such fields.
     *
     * This value is copied to <code>AbstractFrontend.fieldAccessPrefix</code> by
     * the generated parser.
     */
    public String fieldAccessPrefix;
    /**
     * True to enable mutator methods and references.
     *
     * This value is copied to <code>AbstractFrontend.mutatorFlags</code> by
     * the generated parser.
     */
    public boolean mutatorFlags;
    /**
     * Maximum number of bits of a signed integer that is the value of an
     * indexing expression, -1 if arrays are absent or Hedgeelleth does not handle them.<br>
     * 
     * The default is -1.
     */
    public int maxArrayIndexBits = -1;
    /**
     * If false, <code>STRING_LITERAL</code> is generated, that accepts only
     * Java escape sequences. If true, any escape sequences are accepted allowed
     * by <code>StringEscape.convert()</code>.<br>
     * 
     * The default is true. False may lead to cryptic error messages, as a string with
     * invalid escape characters would be considered to be not a string at all.
     */
    public boolean customEscapeSequences = true;
    
    /**
     * Creates a new instance of LanguageDescription. 
     * 
     * Creates empty collections of whitespace, comment types,
     * keywords, separators, operators,
     */
    public LanguageDescription() {
        /* empty */
    }
}
