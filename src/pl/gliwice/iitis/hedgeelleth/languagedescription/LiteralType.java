/*
 * Comment.java
 *
 * Created on Sep 19, 2008
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.languagedescription;

/**
 * A literal type, with a possible precision specifier for arithmetic
 * literals.
 * 
 * @author Artur Rataj
 */
public enum LiteralType {
    DECIMAL,
    HEXADECIMAL,
    OCTAL,
    FLOATING_POINT,
    QUOTED_CHARACTER,
    QUOTED_STRING,
    TRUTH,
    NULL,
    INFINITY;

    /**
     * Returns true for literals defined using an integer constant:
     * <code>DECIMAL</code>, <code>HEXADECIMAL</code> and
     * <code>OCTAL</code>.
     * 
     * @return
     */
    public boolean isInteger() {
        switch(this) {
            case DECIMAL:
            case HEXADECIMAL:
            case OCTAL:
                return true;
                
            default:
                return false;
                    
        }
    }
}
