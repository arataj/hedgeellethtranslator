/*
 * LiteralTypeDescription.java
 *
 * Created on Nov 18, 2008
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.languagedescription;

import java.util.*;

/**
 * A description of a literal, contains literal type and precision
 * specifier.
 * 
 * @author Artur Rataj
 */
public class LiteralTypeDescription implements Comparable<LiteralTypeDescription> {
    /**
     * Literal type.
     */
    public LiteralType type;
    
    /**
     * Precision specifier, null for non--arithmetic types, non--null
     * for arithmetic types.
     */
    public PrecisionSpecifier specifier;
    /**
     * False constant string, not null only for the literal type TRUTH.
     * Must match some keyword.
     */
    public String falseString = null;
    /**
     * True constant string, not null only for the literal type TRUTH.
     * Must match some keyword.
     */
    public String trueString = null;
    /**
     * Null constant string, not null only for the literal type NULL.
     * Must match some keyword.
     */
    public String nullString = null;
    /**
     * Infinity constant string, not null only for the literal type INFINITY.
     * Must match some keyword.
     */
    public String infinityString = null;

    /**
     * Creates a new instance of LiteralTypeDescription. 
     */
    public LiteralTypeDescription(LiteralType type, PrecisionSpecifier specifier) {
        this.type = type;
        this.specifier = specifier;
    }
    @Override
    public int compareTo(LiteralTypeDescription lt) {
        int i = type.compareTo(lt.type);
        if(i == 0)
            i = specifier.toString().compareTo(
                    lt.specifier.toString());
        if(i == 0 && falseString != null)
            i = falseString.compareTo(
                    lt.falseString);
        if(i == 0 && trueString != null)
            i = trueString.compareTo(
                    lt.trueString);
        return i;
    }
    @Override
    public boolean equals(Object o) {
        if(o instanceof LiteralTypeDescription)
            return compareTo((LiteralTypeDescription)o) == 0;
        else
            return false;
    }
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + (this.type != null ? this.type.hashCode() : 0);
        hash = 79 * hash + (this.specifier != null ? this.specifier.hashCode() : 0);
        hash = 79 * hash + (this.falseString != null ? this.falseString.hashCode() : 0);
        hash = 79 * hash + (this.trueString != null ? this.trueString.hashCode() : 0);
        return hash;
    }
}
