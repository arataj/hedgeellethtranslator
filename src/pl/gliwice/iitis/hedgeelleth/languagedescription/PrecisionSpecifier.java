/*
 * PrecisionSpecifier.java
 *
 * Created on Nov 21, 2008
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.languagedescription;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;

/**
 * Precision specifying characters for an arithmetic type having a given
 * number of bits. The number of bits must be either 32 for float and int
 * or 64 for double and long. The characters must be either letters or a dot.
 * The dot means empty, default specifier.
 * 
 * @author Artur Rataj
 */
public class PrecisionSpecifier {
    /**
     * Literal type constants.
     */
    public enum LiteralType {
        INTEGER,
        FLOATING_POINT,
    };
    /**
     * Literal type to which this specifier refers.
     */
    public LiteralType type;


    /**
     * Maps bits to sets of characters.
     */
    private final Map<Integer, String> map;
    
    /**
     * Creates a new instance of PrecisionSpecifier. 
     */
    public PrecisionSpecifier() {
        type = null;
        map = new TreeMap<Integer, String>();
    }
    /**
     * Adds a given set of characters. A set for a given of number of
     * bits can be added only once.<br>
     * 
     * Tests if number of bits is valid, if a sequence for a given
     * number of bits is added only once, and if there is not a
     * duplicate specifier character.
     * 
     * @param bits                      bits specified by the set of
     *                                  characters
     * @param characters                characters that specify a literal
     *                                  with the number of bits
     */
    public void put(int bits, String characters) throws ParseException {
        if(bits != 32 && bits != 64)
            throw new ParseException(null, ParseException.Code.INVALID,
                    "invalid number of bits");
        for(int i = 0; i < characters.length(); ++i) {
            char c = characters.charAt(i);
            if(!Character.isLetter(c) && c != '.')
                throw new ParseException(null, ParseException.Code.INVALID,
                        "invalid specifier: " + c);
            for(String s: map.values())
                if(s.indexOf(c) != -1)
                    throw new ParseException(null, ParseException.Code.DUPLICATE,
                            "duplicate specifier: " + c);
        }
        if(map.get(bits) != null)
            throw new ParseException(null, ParseException.Code.DUPLICATE,
                    "specifiers for this number of bits already defined");
        map.put(bits, characters);
    }
    /**
     * Selects characters for a given number of bits, or all characters.
     * 
     * @param bits                      number of bits or -1 for all
     *                                  characters
     * @return                          set of characters
     */
    public String select(int bits) {
        String out = "";
        for(int b : map.keySet())
            if(bits == -1 || bits == b)
                out += map.get(b);
        return out;
    }
    /**
     * Tests if this specifier is valid. If not, parse exception is thrown.
     * This method should be called after the specifier is complete.<br>
     * 
     * Currently it is tested is there is one and only one default value.
     * Other validity checks are performed within <code>add()</code>.
     */
    public void testValid() throws ParseException {
        int count = 0;
        for(String s: map.values())
            for(int i = 0; i < s.length(); ++i)
                if(s.charAt(i) == '.')
                    ++count;
        if(count == 0)
            throw new ParseException(null, ParseException.Code.MISSING,
                    "default specifier missing");
        else if(count > 1)
            throw new ParseException(null, ParseException.Code.DUPLICATE,
                    "duplicate default specifier");
    }
    private boolean isSubset(Map<Integer, String> subset,
            Map<Integer, String> set) {
        for(int bits : subset.keySet()) {
            String subsetChars = subset.get(bits);
            String setChars = set.get(bits);
            if(setChars == null)
                return false;
            for(int i = 0; i < subsetChars.length(); ++i)
                if(setChars.indexOf(subsetChars.charAt(i)) == -1)
                    return false;
        }
        return true;
    }
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + (this.type != null ? this.type.hashCode() : 0);
        hash = 53 * hash + (this.map != null ? this.map.hashCode() : 0);
        return hash;
    }
    @Override
    public boolean equals(Object o) {
        PrecisionSpecifier ps = (PrecisionSpecifier)o;
        return isSubset(map, ps.map) && isSubset(ps.map, map);
    }
    @Override
    public String toString() {
        String s = type.toString() + ":";
        for(Integer i : map.keySet()) {
            String v = map.get(i);
            s += i + v;
        }
        return s;
    }
}
