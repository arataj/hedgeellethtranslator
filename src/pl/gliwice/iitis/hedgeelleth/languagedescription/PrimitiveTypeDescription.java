/*
 * PrimitiveTypeDescription.java
 *
 * Created on Nov 18, 2008
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.languagedescription;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.Type;

/**
 * A keyword associated to some primitive type.
 * 
 * @author Artur Rataj
 */
public class PrimitiveTypeDescription implements Comparable<PrimitiveTypeDescription> {
    /**
     * Primitive type.
     */
    public Type.PrimitiveOrVoid type;
    /**
     * Keyword representing the type, must match one from the list
     * of keywords in language description.
     */
    public String keyword;
    
    /**
     * Creates a new instance of PrimitiveTypeDescription. 
     * 
     * @param type                      primitive type
     * @param keyword                   keyword of the primitive type
     */
    public PrimitiveTypeDescription(Type.PrimitiveOrVoid type, String keyword) {
        this.type = type;
        this.keyword = keyword;
    }
    @Override
    public int compareTo(PrimitiveTypeDescription pt) {
        int i = type.compareTo(pt.type);
        if(i == 0)
            i = keyword.compareTo(pt.keyword);
        return i;
    }
    @Override
    public boolean equals(Object o) {
        if(o instanceof PrimitiveTypeDescription)
            return compareTo((PrimitiveTypeDescription)o) == 0;
        else
            return false;
    }
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + (this.type != null ? this.type.hashCode() : 0);
        hash = 79 * hash + (this.keyword != null ? this.keyword.hashCode() : 0);
        return hash;
    }

}
