/*
 * GrammarSection.java
 *
 * Created on Nov 18, 2008
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.languagedescription;

/**
 * The unprocessed verbatim section.
 * 
 * @author Artur Rataj
 */
public class VerbatimSection {
    /**
     * Contents of the grammar section.
     */
    public String text;
    /**
     * Name of the input stream. Used for possible error generation.
     */
    public String streamName;
    /**
     * Number of first line of the grammar section in the input stream,
     * counting from 1. Used for possible error generation.
     */
    public int firstLineNum;

    /**
     * Creates a new object representing the unprocessed grammar section.
     * 
     * @param text                      unprocessed text of the grammar section
     * @param streamName                name of the input stream, for error
     *                                  generation
     * @param firstLineNum              number of first line of the grammar
     *                                  section in the input stream, counting
     *                                  from 1. Used for possible error
     *                                  generation
     */
    public VerbatimSection(String text, String streamName, int firstLineNum) {
        this.text = text;
        this.streamName = streamName;
        this.firstLineNum = firstLineNum;
    }
}
