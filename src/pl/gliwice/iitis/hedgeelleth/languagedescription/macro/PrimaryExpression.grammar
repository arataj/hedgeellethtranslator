/*
 * A java--like primary expression. Supports identifiers, dereference,
 * qualification using "this" and "super", method calling, variable indexing,
 * allocation and static import calls.<br>
 *
 * The parameters <code>$enablePrimitiveRange</code>,
 * <code>$enablePrimitiveRange</code> and <code>$parseClassName</code>
 * are related to respectively named options in
 * <code>VariableDeclarator.grammar</code>.<br>
 *
 * Any primitive range tags found in comments are automatically reported by
 * <code>hedgeelleth.findPrimitiveRangeTag</code>.<br>
 *
 * Grammar:
 *
 *     PrimaryExpressionP ::= [ $unknown | ] PrimaryPrefixP ( PrimarySuffixP )*
 *     PrimaryPrefixP ::= HedgeellethLiteral | ( [ $fieldPrefix ] Name ) | "this" | "super" |
 *         "(" $expression ")" | $allocationExpression
 *     PrimarySuffixP ::= "[" $expression "]" | "." Name | Arguments
 *     Arguments ::= "(" [ ListOfExpressions ] ")"
 *
 * Provided productions: PrimaryExpressionP, PrimaryPrefixP, PrimarySuffixP, Arguments
 * Top productions: PrimaryExpressionP
 *
 * Parameters:
 *
 *     $expression                      an abstract expression production
 *     $allocationExpression            production of an allocation expression
 *     $fieldPrefix                     prefix of field names of the method's this,
 *                                      being a shorthand of "this.", null for none,
 *                                      normally this value is the same as
 *                                      <code>field_access_prefix</code>;
 *                                      if not null, must be declared as a keyword
 *     $staticImportPrefix              if null, this expression's call mode is always
 *                                      <code>PrimaryExpression.CallMode.ALL</code>; if not null,
 *                                      then (i) if this expression begins with the prefix, the call mode is
 *                                      <code>PrimaryExpression.CallMode.STATIC</code>,
 *                                      (ii) if this expression does not begin with the prefix, then the call
 *                                      mode is <code>PrimaryExpression.CallMode.OMIT_STATIC</code>
 *     $enablePrimitiveRangeTag         true if to search for a range tag @(min, max) within the
 *                                      comment preceding each primary expression, false otherwise
 *     $enablePrimitiveRangeModifier    true if to search for a range modifier
 *                                      preceding each primary expression, false otherwise
 *     $parseClassName                  name of the parser's class, required if
 *                                      $enablePrimitiveRangeTag is true, to make class name of parser's
 *                                      constants
 *     $unknown                     null or a boolean constant representing an unknown value
 *     $unknownMethod            null or a qualified name of a static method to call in order to compute
 *                                      an unknown value; can not be null if $unknown is not one
 *
 * Requires: HEDGEELLETH(LITERAL), Name.grammar, ListOfExpressions.grammar,
 * if primitive ranges are enabled then PrimitiveRangeUtils.code
 *
 * Copyright 2009 Artur Rataj.
 * Parts of the code that are taken from JavaCC demo grammars
 * are copyright (c) 2008 Sun Microsystem.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

/**
 * A primary expression.
 *
 * @param parent parent's scope
 */
PrimaryExpression PrimaryExpressionP(BlockScope parent) :
{
    AbstractExpression expression = null;
    PrimaryExpression.ScopeMode scopeMode;
#IFNULL $staticImportPrefix
    scopeMode = PrimaryExpression.ScopeMode.ALL;
#ENDIF
#IFNOTNULL $staticImportPrefix
    scopeMode = PrimaryExpression.ScopeMode.OMIT_STATIC;
#ENDIF
    PrimaryPrefix prefix;
    List<PrimarySuffix> postfixList = new LinkedList<PrimarySuffix>();
    PrimarySuffix postfix;
    StreamPos pos = null;
}
{
  (
#IFNOTNULL $unknown
  (
    { pos = getStreamPos(getToken(1)); }
    $unknown {
          PrimaryExpression tmp = Hedgeelleth.getUnknown(getStreamPos(), null,
                $unknownMethod);
          prefix = tmp.prefix;
          postfixList.addAll(tmp.suffixList);
/*          prefix = new PrimaryPrefix(getStreamPos(),
                new NameList($unknownMethod));
          prefix.textBeg = pos;
          postfix = new PrimarySuffix(pos = getStreamPos(), new LinkedList());
          postfixList.add(postfix);*/
    }
  ) |
#ENDIF
  (
#IFNOTNULL $staticImportPrefix
                [ $staticImportPrefix {
                    scopeMode = PrimaryExpression.ScopeMode.STATIC;
                    pos = getStreamPos();
                } ]
#ENDIF
    prefix = PrimaryPrefixP(parent) {
        if(pos == null)
            pos = prefix.getStreamPos();
    } (
    postfix = PrimarySuffixP(parent) { postfixList.add(postfix); } )*
  ))
  {
    return new PrimaryExpression(pos, parent, scopeMode, prefix, postfixList);
  }
}

/**
 * Primary prefix.
 */
PrimaryPrefix PrimaryPrefixP(BlockScope parent) :
{
  PrimaryPrefix prefix;
  Literal literal;
  NameList name;
  AbstractExpression expression;
  AllocationExpression allocationExpression;
  boolean fieldPrefix = false;
  String range = null;
  StreamPos rangePos = null;
  StreamPos prefixTextBeg;
}
{
  (
      // this is just a trick to ensure the next token is available
      <ERROR> {
          hedgeelleth.addParseException(
              new ParseException(getStreamPos(), ParseException.Code.PARSE,
              "unexpected token"));
          throw new ParseException();
      }
  )
  |
  (
#IF $enablePrimitiveRangeTag
      {
          Token token = getCurrentToken().next;
          if(token == null)
              token = token;
          if(token.specialToken != null && false)
              System.out.println("\n*\n* " + token.image + " | " + token.specialToken.image + "\n*");
          List<CommentTag> tags = hedgeelleth.findCommentTags(token, true, null, true);
          rangePos = new StreamPos();
          range = hedgeelleth.findPrimitiveRangeTag(tags, true, rangePos, true, true);
          if(range == null)
              rangePos = null;
      }
#ENDIF
#IF $enablePrimitiveRangeModifier
      [
           // the bounding tokens of the modifier should match
           // <code>PrimitiveRangeDescription.LEFT_STRING</code> and
           // <code>PrimitiveRangeDescription.RIGHT_STRING</code>,
           // respectively
           "<<" {  // formerly "(<"
               rangePos = new StreamPos();
               range = hedgeelleth.findPrimitiveRangeModifier(
                   $parseClassNameConstants.tokenImage, rangePos,
                    true);
               if(range == null)
                   rangePos = null;
           }
           // ">>" is consumed by <code>findPrimitiveRangeModifier // formerly ">)"</code>
      ]
#ENDIF
      { prefixTextBeg = getStreamPos(getToken(1)); }
      (
                literal = HedgeellethLiteral() {
                    prefix = new PrimaryPrefix(getStreamPos(), literal);
                }
            |
#IFNOTNULL $fieldPrefix
                [ $fieldPrefix { fieldPrefix = true; } ]
#ENDIF
                name = Name() {
                    if(fieldPrefix)
                        name.list.set(0, $fieldPrefix + name.list.get(0));
                    prefix = new PrimaryPrefix(getStreamPos(), name);
                }
            |
                "this" { prefix = new PrimaryPrefix(getStreamPos(), PrimaryPrefix.PrefixType.THIS, null); }
            |
                LOOKAHEAD("super" "." Name())
                "super" { prefix = new PrimaryPrefix(getStreamPos(), PrimaryPrefix.PrefixType.SUPER, null); }
            |
                "(" expression = $expression(parent) ")" {
                    // expression is null if a parse error occured
                    if(expression != null)
                        prefix = new PrimaryPrefix(expression);
                    else
                        throw new ParseException();
                }
            |
                allocationExpression = $allocationExpression(parent) {
                    prefix = new PrimaryPrefix(allocationExpression);
                }
      )
      { prefix.textBeg = prefixTextBeg; }
  )
  {
#IF $enablePrimitiveRangeTag || $enablePrimitiveRangeModifier
    if(range != null) {
        PrimitiveRangeDescription description =
            new PrimitiveRangeDescription();
        description.text = range;
        description.textPos = rangePos;
        List<AssignmentExpression> rangeExpressions =
            parsePrimaryPrimitiveRangeText(parent, description,
            true);
        prefix.rangeDescription = description;
        prefix.rangeExpressions.addAll(rangeExpressions);
    }
#ENDIF
    return prefix;
  }
}

/**
 * Primary suffix.
 */
PrimarySuffix PrimarySuffixP(BlockScope parent) :
{
  PrimarySuffix suffix;
  AbstractExpression expression;
  NameList name;
  List<AbstractExpression> list;
}
{
  (
          "[" expression = $expression(parent) "]" {
              // suffix type INDEX
              // expression is null if a parse error occured
              if(expression != null)
                  suffix = new PrimarySuffix(PrimarySuffix.Type.INDEX,
                          expression);
              else
                  throw new ParseException();
          }
        |
          "." name = Name() {
              // suffix type NAME
              suffix = new PrimarySuffix(getStreamPos(), name);
          }
        |
          list = Arguments(parent) {
              StreamPos pos;
              if(list.size() == 0)
                  pos = getStreamPos();
              else
                  pos = list.get(0).getStreamPos();
              // suffix type ARGUMENTS
              suffix = new PrimarySuffix(pos, list);
          }
  )
  {
    suffix.textEnd = getStreamEndPos(getCurrentToken());
    return suffix;
  }
}

/**
 * Arguments of a method's call.
 */
List<AbstractExpression> Arguments(BlockScope parent) :
{
    List<AbstractExpression> list;
}
{
    "(" list = ListOfExpressions(parent) ")"
    {
        return list;
    }
}
