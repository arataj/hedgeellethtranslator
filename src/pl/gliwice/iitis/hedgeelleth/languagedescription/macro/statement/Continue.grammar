/*
 * A continue statement with an optional label, compatible with
 * <code>LabelContext.grammar</code>, generates a <code>JumpStatement</code>.
 *
 * Grammar:
 *
 *     ContinueStatementP ::= $continue [ <IDENTIFIER> ] $semicolon
 *
 * Provided productions: ContinueStatementP
 * Top productions: ContinueStatementP
 *
 * Parameters:
 *
 *     $continue                        the keyword that begins this statement
 *     $semicolon                       the keyword that ends this statement
 *
 * Requires: nothing
 *
 * Copyright 2009 Artur Rataj.
 * Parts of the code that are taken from JavaCC demo grammars
 * are copyright (c) 2008 Sun Microsystem.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

/**
 * A continue statement.
 */
AbstractStatement ContinueStatementP(BlockScope parent) :
{
    Token t;
    String labelContext = null;
    StreamPos pos;
}
{
  $continue [ t = <IDENTIFIER> { labelContext = t.image; } ] { pos = getStreamPos(); } $semicolon
  {
    String name = parent.getContinueLoopLabel(labelContext);
    if(name == null)
        if(labelContext == null)
            throw new ParseException(pos,
                ParseException.Code.ILLEGAL,
                "continue outside of a loop");
        else
            throw new ParseException(pos,
                ParseException.Code.LOOK_UP,
                "loop label " + labelContext +
                " not found");
    return new JumpStatement(pos, parent,
        name);
  }
}
