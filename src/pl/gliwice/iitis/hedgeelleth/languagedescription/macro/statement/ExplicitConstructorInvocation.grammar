/*
 * A java--like explicit constructor invocation, using "this" or "super".
 * Constructs a direct non--static call expression using either
 * <code>Hedgeelleth.CONSTRUCTOR_THIS</code> or
 * <code>Hedgeelleth.CONSTRUCTOR_SUPER</code>.
 *
 * Grammar:
 *
 *     ExplicitConstructorInvocation ::= ( "this" | "super" ) Arguments $semicolon
 *
 * Provided productions: ExplicitConstructorInvocation
 * Top productions: ExplicitConstructorInvocation
 *
 * Parameters:
 *
 *     $semicolon                       token that ends this statement
 *
 * Requires: Arguments or PrimaryExpression.grammar
 *
 * Copyright 2009 Artur Rataj.
 * Parts of the code that are taken from JavaCC demo grammars
 * are copyright (c) 2008 Sun Microsystem.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

/**
 * An explicit invocation of a constructor.  Returns null if
 * a parse exception occured.
 *
 * @param scope                     scope of the block to which this
 *                                  statement belongs
 * @param fcs                       first statement in a constructor
 */
AbstractExpression ExplicitConstructorInvocation(BlockScope scope,
    boolean fcs) :
{
      StreamPos pos;
      Method method;
      List<AbstractExpression> arg;
      CallExpression call;
}
{
       (
              LOOKAHEAD("super" Arguments() ";")
              "super" {
                  method = Hedgeelleth.CONSTRUCTOR_SUPER;
              }
           |
              LOOKAHEAD("this" Arguments() ";")
              "this" {
                  method = Hedgeelleth.CONSTRUCTOR_THIS;
              }
       )
       { pos = getStreamPos(); }
       arg = Arguments(scope) $semicolon {
            call = new CallExpression(pos, scope, method, arg);
            if(!fcs)
                throw new ParseException(pos, ParseException.Code.PARSE,
                    "call to constructor can only be the first statement " +
                    "of another constructor");
            // calls to constructors are always direct
            call.directNonStatic = true;
            return call;
       }
}
