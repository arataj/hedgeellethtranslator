/*
 * A java--like if--then--else statement. If used in Verics with ternary logic, then
 * the unknown block is also possible.
 *
 * Grammar, variant with parentheses
 *
 *     IfStatementP ::= [ $booleanType $lvalue $final $nonFinal ] "if" "(" $expression ")" $statement [ "else"
 *         $statement ] (( [ "unknown" $statement ] ))
 *
 * Grammar, variant with braces
 *
 *     IfStatementP ::= [ $booleanType $lvalue $final $nonFinal ] "if" $expression "{" $statement "}" [ "else"
 *         "{" $statement "}" ] (( [ "unknown" "{" $statement "}" ] ))
 *
 * A ternary variant recognizes keywords "if" and "ifp".
 *
 * Provided productions: IfStatementP
 * Top productions: IfStatementP
 *
 * Parameters:
 *
 *     $lvalue                          a primary expression production, null if no lvalue allowed
 *     $booleanType                     not null only if $lvalue is not null; a keyword for boolean type
 *     $final                           not null only if $lvalue is not null;
 *                                      keyword for the "final" modifier, null for no such
 *                                      keyword in this place, if not null, must be declared
 *                                      as a keyword, see also the note above
 *     $nonFinal                        not null only if $lvalue is not null;
 *                                      opposite of $final; keyword for the "non--final" modifier, null for no such
 *                                      keyword in this place, if not null, must be declared
 *                                      as a keyword, see also the note above
 *     $expression                      an abstract expression production
 *     $statement                       production of a statement which should be the body;
 *                                      if $parentheses is false, should be a block statement beginning
 *                                      with "{"
 *     $parentheses                   false for a variant without parentheses; requires a block
 *                                      statement after the condition
 *     $ternary                    true for a ternary variant
 *     $unknownMethod            a qualified name of a static method to call in order to compute
 *                                      an unknown value; null if $ternary is false
 *
 * Requires: TokenUtils.code
 *
 * Copyright 2009, 2016 Artur Rataj.
 * Parts of the code that are taken from JavaCC demo grammars
 * are copyright (c) 2008 Sun Microsystem.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

/**
 * An if--then--else statement.
 */
Block IfStatementP(BlockScope parent) :
/*
 * The disambiguating algorithm of JavaCC automatically binds dangling
 * else's to the innermost if statement.  The LOOKAHEAD specification
 * is to tell JavaCC that we know what we are doing.
 */
{
    IfLocals v = new IfLocals(getStreamPos(), parent);
}
{
#IFNOTNULL $lvalue
  [
    [ $booleanType { v.declaration = true; } ]
    v.lvalue = $lvalue(v.block.scope) { v.assignmentPos = getStreamPos(); }
#IFNOTNULL $final
        {
            v.finaL = false;
        }
        [
             $final {
                 if(!v.declaration)
                    hedgeelleth.addParseException(new ParseException(getStreamPos(),
                        ParseException.Code.ILLEGAL, "not a declaration"));
                 v.finaL = true;
             }
        ]
#ENDIF
#IFNOTNULL $nonFinal
        {
            v.finaL = true;
        }
        [
             $nonFinal {
                 if(!v.declaration)
                    hedgeelleth.addParseException(new ParseException(getStreamPos(),
                        ParseException.Code.ILLEGAL, "not a declaration"));
                 v.finaL = false;
             }
        ]
#ENDIF
  ]
#ENDIF
  {
      if(v.declaration) {
        if(v.lvalue == null)
            hedgeelleth.addParseException(new ParseException(getStreamPos(),
                ParseException.Code.ILLEGAL, "variable name missing"));
        else {
            String name;
            if(v.lvalue.prefix.contents instanceof NameList) {
                name = ((NameList)v.lvalue.prefix.contents).toString();
                if(!v.lvalue.suffixList.isEmpty())
                    hedgeelleth.addParseException(new ParseException(v.lvalue.getStreamPos(),
                        ParseException.Code.PARSE, "variable name expected"));
            } else {
                name = null;
                hedgeelleth.addParseException(new ParseException(v.lvalue.getStreamPos(),
                    ParseException.Code.PARSE, "variable name expected"));
            }
            if(name != null) {
                LocalFlags flags = new LocalFlags();
                flags.setFinal(v.finaL);
                v.variable = new Variable(
                        v.lvalue.getStreamPos(),
                        parent.getBoundingVariableOwner(),
                        new Type(Type.PrimitiveOrVoid.BOOLEAN),
                        flags,
                        name, true, Variable.Category.SOURCE);
                ((AbstractLocalOwnerScope)parent).addLocalInThis(
                    v.variable);
            }
          }
      }
  }
  (
         "if" { v.probabilistic = false; }
#IF $ternary
     |
         "ift" { v.probabilistic = true; }
#ENDIF
  )
#IF $parentheses
  "("
#ENDIF
  v.condition = $expression(v.block.scope) {
        if(v.condition == null) {
            // a parse error occured
#IF $parentheses
            skipBeforeCharacter(')');
#ENDIF
#IFNOT $parentheses
            skipBeforeCharacter('{');
            if(!getCurrentToken().image.equals("{"))
                // failed to find the loop body
                return v.block;
#ENDIF
        }
  }
#IF $parentheses
  ")"
#ENDIF
(
    (
        branchTrueP(parent, v)
        [
        LOOKAHEAD(1) branchFalseP(parent, v)
        ]
#IF $ternary
        [
        LOOKAHEAD(1) branchUnknownP(parent, v)
        ]
#ENDIF
    )
    |
    (
        branchFalseP(parent, v)
#IF $ternary
        [
        LOOKAHEAD(1) branchUnknownP(parent, v)
        ]
#ENDIF
    )
#IF $ternary
    |
    (
        branchUnknownP(parent, v)
    )
#ENDIF
#IFNOT $parentheses
    |
    LOOKAHEAD(1) ";" {
        if(!v.probabilistic)
            hedgeelleth.addParseException(new ParseException(getStreamPos(),
                ParseException.Code.PARSE, "block expected in non-ternary if, found `;'"));
    }
#ENDIF
)
  {
    if(v.probabilistic && !v.trueBranch)
        v.truE = new EmptyExpression(v.condition.getStreamPos(), v.block.scope);
    // truE is null if parse error occured,
    // falsE is null if the "else" block is absent or a parse error occured
    // unknowN is null if the "unknown" block is absent or a parse error occured
    // just checking truE, as it is required to be not null by the
    // called method
    if(v.truE != null && v.condition != null) {
         if(v.probabilistic && v.elseifShortcut)
            hedgeelleth.addParseException(new ParseException(v.elseifPos,
                ParseException.Code.PARSE, "can not use else-if without braces in a ternary if"));
         if(v.lvalue != null && !v.probabilistic)
            hedgeelleth.addParseException(new ParseException(v.assignmentPos,
                ParseException.Code.ILLEGAL, "assignment requires a ternary if"));
         else if(v.unknowN != null && !v.probabilistic)
            hedgeelleth.addParseException(new ParseException(v.unknownPos,
                ParseException.Code.ILLEGAL, "an unknown block requires a ternary if"));
        else
            hedgeelleth.appendIfThenElseCode(v.probabilistic ? $unknownMethod : null,
                v.block, v.lvalue, v.condition,
                v.truE, v.falsE, v.unknowN,
                getStreamPos());
    }
    return v.block;
  }
}

void branchTrueP(BlockScope parent, IfLocals v) :
{
}
{
#IF $parentheses
  (
    { v.trueBranch = true; } v.truE = $statement(v.block.scope, false)
#ENDIF
#IFNOT $parentheses
  (
    { v.trueBranch = true; } try {
         v.truE = $statement(v.block.scope)
    } catch(ParseException e) {
         hedgeelleth.addParseException(e);
         v.truE = null;
    }
#ENDIF
  {
        if(!v.probabilistic && v.truE == null)
            // a parse error occured
            skipBeforeToken("\"else\"", 1);
  }
  )
}

void branchFalseP(BlockScope parent, IfLocals v) :
{
}
{
        "else" { v.elseifPos = getStreamPos(); }
#IF $parentheses
        v.falsE = $statement(v.block.scope, false)
#ENDIF
#IFNOT $parentheses
    try {
            v.falsE = $statement(v.block.scope)
        |
            v.falsE = IfStatementP(v.block.scope) { v.elseifShortcut = true; }
    } catch(ParseException e) {
         hedgeelleth.addParseException(e);
         v.falsE = null;
    }
#ENDIF
    {
         if(v.falsE == null)
             // a parse error occured
             skipAfterSemicolon();
    }
}

#IF $ternary
void branchUnknownP(BlockScope parent, IfLocals v) :
{
}
{
        "unknown" { v.unknownPos = getStreamPos(); }
#IF $parentheses
        v.unknowN = $statement(v.block.scope, false)
#ENDIF
#IFNOT $parentheses
    try {
            v.unknowN = $statement(v.block.scope)
    } catch(ParseException e) {
         hedgeelleth.addParseException(e);
         v.unknowN = null;
    }
#ENDIF
    {
         if(v.unknowN == null)
             // a parse error occured
             skipAfterSemicolon();
    }
}
#ENDIF