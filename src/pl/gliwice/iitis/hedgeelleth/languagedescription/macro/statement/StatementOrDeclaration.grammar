/*
 * Adds local variable declaration as a choice to a statement.
 *
 * If a declaration is found, a new scope is created, nested within the previous one.
 * It is for detecting attempts of using a local before the local is declared, and also
 * for correct resolution of shadowing of a field by a local. When the scope changes,
 * the element in the <code>scope</code> argument is changed accordingly,
 * so that the parent production knows about the change.
 *
 * Grammar:
 *
 *     $statementOrDeclaration ::= LocalVariableDeclaration | $statement
 *
 * Provided productions: $statementOrDeclaration
 * Top productions: $statementOrDeclaration
 *
 * Parameters:
 *
 *     $statementOrDeclaration          name of the top production, it is configurable to make
 *                                      it possible to have two productions with different
 *                                      values of $orphaned
 *     $statement                       statement production
 *     $orphaned                        keywords that are likely to be orphaned
 *                                      if found at the place of a statement or a declaration,
 *                                      the keywords should be separated with "|" like in:
 *                                      "else" | "null"
 *     $allowedCommentTags
 *                                      array with names of allowed named comment tags; any tag with
 *                                      allowed name is registered as used; unnamed comment tags are
 *                                      typically range tags, which have a separate mechanism for usage
 *                                      tracing, thus, they can not be covered by this parameter; this
 *                                      parameter can be null, if no comment tags should be registered
 *                                      as used
 *     $stateTagName                    a quoted string, representing name of the tag, which should
 *                                      have its distinct non--optimizable operation generated, null for
 *                                      none; typically, the value is the same as in
 *                                      <code>statement/Block.grammar</code>
 *     $ifBooleanType                   if not null, allows for local--declaring ternary ifs and should be
 *                                      equal to the boolean type keyword
 *     //$type                           if $ifBooleanType is not null, then type production; otherwise null
 *     $nonFinal                      non--final variable modifier or null if $ifBooleanType is null
 *
 * Requires: LocalVariableDeclaration.grammar, if $ifBooleanType is not null then If.grammar
 *
 * Copyright 2009 Artur Rataj.
 * Parts of the code that are taken from JavaCC demo grammars
 * are copyright (c) 2008 Sun Microsystem.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

/**
 * Local variable declaration or a statement. It returns a list because
 * one declaration can contain more variables.
 *
 * @param block                     array with the the block to which this
 *                                  statement belongs, can be modified with a nested
 *                                  scope
 * @param fcs                       first statement in a constructor
 */
void $statementOrDeclaration(Block[] block,
    boolean fcs) :
{
    Block topBlock = block[0];
    List<AbstractStatement> sublist = new LinkedList<AbstractStatement>();
    Block ifBlock;
    List<AbstractExpression> declaration;
    List<AbstractStatement> allList = new LinkedList<AbstractStatement>();
    AbstractStatement s;
    Token next = token;
}
{
  (
        LOOKAHEAD( $orphaned ) (
            // a few keywords that are likely to be orphaned
            // if found at this place of the stream
            $orphaned
        ) {
            throw new ParseException(getStreamPos(), ParseException.Code.PARSE,
                "orphaned \"" + getCurrentToken().image + "\"");
        }
     |
        LOOKAHEAD( LocalVariableDeclarationLookahead() )
        (
            {
                // each local variable declaration opens a new scope
                Block nested = new Block(getStreamPos(), block[0].scope);
                block[0].code.add(nested);
                block[0] = nested;
                allList.add(block[0]);
            }
            (
#IFNOTNULL $ifBooleanType
            LOOKAHEAD( $ifBooleanType <IDENTIFIER> [ $nonFinal ] ("if" | "ift"))
            ifBlock = IfStatementP(block[0].scope) { sublist.addAll(ifBlock.code); }
            |
#ENDIF
            declaration = LocalVariableDeclaration(block[0].scope) { sublist.addAll(declaration); }
            ";"
            )
            {
                block[0].code = new LinkedList<AbstractStatement>();
                block[0].code.addAll(sublist);
                allList.addAll(sublist);
            }
        )
     |
        (
            s = $statement(block[0].scope, fcs) {
                fcs = false;
                // s is null if parse error occured
                if(s != null) {
                    block[0].code.add(s);
                    allList.add(s);
                }
            }
        )
  )
  {
    // move to the statement's or declaration's first token
    next = next.next;
    // search for tags just before the statement
    List<CommentTag> tags = hedgeelleth.findCommentTags(next, false,
        $allowedCommentTags, false);
#IFNOTNULL $stateTagName
    List<CommentTag> tagsCopy = new LinkedList<CommentTag>(tags);
    for(CommentTag tag : tagsCopy)
        if(tag.name.equals($stateTagName)) {
            tags.remove(tag);
            EmptyExpression tagHolder = new EmptyExpression(
                    getStreamPos(next), block[0].scope);
            tagHolder.toNoneOp = true;
            tagHolder.tags.add(tag);
            topBlock.code.add(0, tagHolder);
            allList.add(0, tagHolder);
        }
#ENDIF
    for(AbstractStatement e : allList)
        e.tags.addAll(tags);
  }
}
