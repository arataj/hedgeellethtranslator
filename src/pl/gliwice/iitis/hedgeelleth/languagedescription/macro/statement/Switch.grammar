/*
 * A java--like switch statement. // If $instanceof is not null, this production
 * // supports Verics' switch instanceof variant.
 *
 * Grammar:
 *
 *     SwitchStatementP ::= "switch" "(" $expression ")" "{"
 *         ( SwitchLabel ( $statementOrDeclaration )* )*
 *     SwitchLabel ::= ( "case" $expression | "default" ) ":"
 *
 * Provided productions: SwitchStatementP
 * Top productions: SwitchStatementP
 *
 * Parameters:
 *
 *     $expression                      an abstract expression production
 *     $statementOrDeclaration          production of a statement or declaration
 *     $lvalue                          type of the variable to which is assigned the
 *                                      switch expression
 *     $parentheses                   false for a variant without parentheses
 *     $parser                          name of the parser, without the
 *                                      directory and the extension, to access
 *                                      the constants file; required only if $parentheses is false
 *
 * Requires: TokenUtils.code
 *
 * Copyright 2009 Artur Rataj.
 * Parts of the code that are taken from JavaCC demo grammars
 * are copyright (c) 2008 Sun Microsystem.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

AbstractStatement SwitchStatementP(BlockScope parent) :
{
    Block block = new Block(getStreamPos(), parent);
    BlockScope switchScope = block.scope;
    switchScope.setLabelContext("");
    AbstractExpression expression;
    AbstractExpression caseExpression;
    List<AbstractStatement> statements;
    List<AbstractStatement> switchPart = new LinkedList<AbstractStatement>();
    List<AbstractStatement> caseCode = new LinkedList<AbstractStatement>();
    Block[] currCaseBlock = new Block[1];
    // a dummy block to house <code>caseCode</code>
    Block caseBlock = new Block(null, switchScope);
    // cancel a subscope in this dummy block
    caseBlock.scope = switchScope;
    caseBlock.code = caseCode;
    currCaseBlock[0] = caseBlock;
    // not nul if a "default" case was found
    String defaultCaseLabel = null;
    Token last = null;
    boolean stuck = false;
    Set<AbstractExpression> labels = new HashSet<AbstractExpression>();
    // labels should be unique, register them to be checked for this during
    // the static analysis
    hedgeelleth.getCompilation().uniqueExpressions.add(labels);
    String caseLabel;
}
{
    "switch"
#IF $parentheses
    "("
#ENDIF
    expression = $expression(parent) {
          if(expression == null) {
              // a parse error occured
#IF $parentheses
            skipBeforeCharacter(')');
#ENDIF
#IFNOT $parentheses
            skipBeforeCharacter('{');
            if(!getCurrentToken().image.equals("{"))
                // failed to find the loop body
                return block;
#ENDIF
          }
    }
#IF $parentheses
    try {
          ")"
    } catch(ParseException e) {
          // condition was invalid
          expression = null;
          // move to the invalid token
          getNextToken();
          hedgeelleth.addParseException(
              new ParseException(getStreamPos(), ParseException.Code.PARSE,
              "invalid switch condition: unexpected `" + getCurrentToken().image + "'"));
          skipAfterCharacter(')');
    }
#ENDIF
    {
        Variable v = switchScope.newInternalLocal(getStreamPos(),
            $lvalue, null, Variable.Category.INTERNAL_NOT_RANGE);
        PrimaryExpression switchLValue = hedgeelleth.newVariableExpression(getStreamPos(),
            switchScope, v);
        // expression is null if a parse error occured
        if(expression != null) {
            AssignmentExpression assignment = hedgeelleth.newAssignment(getStreamPos(),
                switchScope, v, expression);
            block.code.add(assignment);
        }
        String breakLabel = block.scope.newLabel();
        switchScope.setBreakLoopLabel(breakLabel);
    }
    try {
        "{"
    } catch(ParseException e) {
        getNextToken();
        hedgeelleth.addParseException(
            new ParseException(getStreamPos(), ParseException.Code.PARSE,
            "invalid switch statement: unexpected `" + getCurrentToken().image + "'"));
#IF $parentheses
        skipAfterCharacter('{');
#ENDIF
#IFNOT $parentheses
        // "case" is likely
        String[] after = {
            "\";\"",
            "\"{\"",
        };
        String[] before = {
            "\"case\"",
            "\"default\"",
            "\"}\"",
        };
        hedgeelleth.skipTokens($parserConstants.tokenImage,
                after, before, 1);
#ENDIF
    }
    try {
        (
            {
                if(getCurrentToken() == last) {
                    if(stuck)
                        // stuck on parse errors
                        throw new ParseException(null, null, "");
                    else {
                        // try one more time
                        getNextToken();
                        stuck = true;
                    }
                } else
                    stuck = false;
                last = getCurrentToken();
            }
            caseExpression = SwitchLabel(block.scope, switchLValue)
            {
                if(caseExpression == null) {
                    // parse error, handled already in SwitchLabel()
                } else if(caseExpression instanceof BinaryExpression) {
                    // not an empty expression, so no parse error occured within the
                    // expression, and this is an equality expression
                    BinaryExpression b = (BinaryExpression)caseExpression;
                    // register for uniqueness check
                    labels.add(b.right);
                } else if(caseExpression instanceof EmptyExpression) {
                    // a default label
                } else
                    throw new RuntimeException("invalid case expression");
            }
            {
                caseLabel = block.scope.newLabel();
                currCaseBlock[0].code.add(
                    new LabeledStatement(currCaseBlock[0].getStreamPos(),
                    currCaseBlock[0].scope, caseLabel, null));
            }
            (
                {
                    if(getCurrentToken() == last) {
                        if(stuck)
                            // stuck on parse errors
                            throw new ParseException(null, null, "");
                        else {
                            // try one more time
                            getNextToken();
                            stuck = true;
                        }
                    } else
                        stuck = false;
                    last = getCurrentToken();
                }
                $statementOrDeclaration(currCaseBlock, false)
            )*
            {
                if(caseExpression instanceof EmptyExpression) {
                    // a default label
                    if(defaultCaseLabel != null)
                        hedgeelleth.addParseException(
                            new ParseException(caseExpression.getStreamPos(), ParseException.Code.PARSE,
                            "duplicate default case"));
                    defaultCaseLabel = caseLabel;
                } else
                    // a non--default label
                    switchPart.add(new BranchStatement(block.getStreamPos(),
                        block.scope, caseExpression,
                        caseLabel, null));
            }
        )*
    } catch(ParseException e) {
        skipBeforeCharacter('}');
    }
    try {
         "}"
    } catch(ParseException e) {
         getNextToken();
         hedgeelleth.addParseException(
             new ParseException(getStreamPos(), ParseException.Code.PARSE,
             "invalid switch statement: unexpected `" + getCurrentToken().image + "'"));
         skipAfterCharacter('}');
    }
    {
          block.code.addAll(switchPart);
          String defaultChoiceLabel;
          if(defaultCaseLabel != null)
              defaultChoiceLabel = defaultCaseLabel;
          else
              defaultChoiceLabel = breakLabel;
          block.code.add(new JumpStatement(block.getStreamPos(), switchScope,
              defaultChoiceLabel));
          block.code.addAll(caseCode);
          block.code.add(new LabeledStatement(block.getStreamPos(), block.scope,
              breakLabel, null));
          return block;
   }
}

/**
  * For case, returns the equality expression with the label value on the right side,
  * for default returns an empty expression. If a parse error occured, returns null.
  */
AbstractExpression SwitchLabel(BlockScope parent, PrimaryExpression switchLValue) :
{
    AbstractExpression expression;
    StreamPos pos;
}
{
    (
           "case" { pos = getStreamPos(); }
            expression = $expression(parent) {
                  if(expression == null)
                      // a parse error occured
                      skipBeforeCharacter(':');
            }
            try {
                  ":"
            } catch(ParseException e) {
                  // expression was invalid
                  expression = null;
                  // move to the invalid token
                  getNextToken();
                  hedgeelleth.addParseException(
                      new ParseException(getStreamPos(), ParseException.Code.PARSE,
                      "invalid case value: unexpected `" + getCurrentToken().image + "'"));
                  skipAfterCharacter(')');
            }
            {
                // expression is null if a parse error occured
                if(expression != null)
                    expression = new BinaryExpression(pos, parent,
                        BinaryExpression.Op.EQUAL,
                        switchLValue, expression);
            }
        |
            "default" { pos = getStreamPos(); } ":"
            {
                expression = new EmptyExpression(pos, parent);
            }
    )
    {
        return expression;
    }
}
