/*
 * AbstractBackend.java
 *
 * Created on Apr 14, 2009, 1:58:15 PM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.modules;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.code.CodeCompilation;
import pl.gliwice.iitis.hedgeelleth.compiler.util.ModelType;
import pl.gliwice.iitis.hedgeelleth.compiler.util.PWD;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;

/**
 * An abstract backend.
 *
 * @author Artur Rataj
 */
public abstract class AbstractBackend {
    /**
     * Mode of generating files by the backend.
     */
    public enum OutputMode {
        /**
         * Normal, non--experiment file generation.
         */
        NORMAL,
        /**
         * Experiment mode -- file generation in a separate
         * directory.
         */
        EXPERIMENT,
        /**
         * Like <code>EXPERIMENT</code>, but the directory
         * is emptied. usually before the first experiment
         * only.
         */
        EXPERIMENT_CLEAN,
    };

    /**
     * Creates a new instance of AbstractBackend.
     */
    public AbstractBackend() {
    }
    /**
     * Translates internal representation into output files.<br>
     * 
     * If there is one than a more type of generated files, then the
     * types should be ordered in the returned list as follows:
     * declaration only files, implementation files with optional
     * declaration, property files, other files.
     *
     * @param codeCompilation           compilation to get the translation
     *                                  results
     * @param topDirectory              top directory of the output files
     * @param outputFileNamePrefix      optional prefix of the output
     *                                  file name or null
     * @param translationErrors         exception to add possible parse errors
     * @param outputMode                defines structure of output files generated
     *                                  by the backend
     * @param modelTypes this method adds to this set all types of low-level models generated
     * for e.g. a model checker; not modified if no such models are generated
     * @return                          list with the names of the generated
     *                                  output files, if no output file
     *                                  is generated for whatever reason,
     *                                  then the list is empty; the file names
     *                                  should contain only experiment directory name,
     *                                  if any, and the file name itself
     */
    public abstract List<String> translate(CodeCompilation codeCompilation,
            PWD topDirectory, String outputFileNamePrefix,
            CompilerException translationErrors, OutputMode outputMode,
            Set<ModelType> modelTypes);
}
