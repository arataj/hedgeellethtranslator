/*
 * AbstractFrontend.java
 *
 * Created on Apr 14, 2009, 1:55:48 PM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.modules;

import java.util.List;
import pl.gliwice.iitis.hedgeelleth.compiler.SemanticCheck;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.BlockExpression;
import pl.gliwice.iitis.hedgeelleth.compiler.util.PWD;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.interpreter.AbstractInterpreter;
import pl.gliwice.iitis.hedgeelleth.interpreter.RuntimeThread;
import pl.gliwice.iitis.hedgeelleth.interpreter.exception.InterpreterException;

/**
 * An abstract frontend. It translates the source code to a semantically
 * checked tree.
 *
 * @author Artur Rataj
 */
public abstract class AbstractFrontend {
    /**
     * Defines all possible types of special classes within this frontend.
     */
    public enum SpecialClassCategory {
        // no special class
        NONE,
        // object
        OBJECT,
        // array
        ARRAY,
        // string
        STRING,
        // root exception
        ROOT_EXCEPTION,
        // unchecked exception
        UNCHECKED_EXCEPTION,
    };

    /**
     * A fully qualified name of the object class, used in the source.
     * It is replaced during the generation of code by
     * <code>Type.OBJECT_QUALIFIED_CLASS_NAME</code>.<br>
     *
     * A respective class should exist in the library, with the contents
     * as described in Hedgeelleth user guide.<br>
     *
     * The object class must be defined if Hedgeelleth handles the language's
     * classes, otherwise this field can be null.
     */
    public String objectClassName;
    /**
     * A fully qualified name of the array class, used in the source.
     * It is replaced during the generation of code by
     * <code>Type.ARRAY_QUALIFIED_CLASS_NAME</code>.<br>
     *
     * A respective class should exist in the library, with the contents
     * as described in Hedgeelleth user guide.<br>
     *
     * If the arrays are not allowed, then this is null.<br>
     * The default is null.
     */
    public String arrayClassName;
    /**
     * The name of the length field in the array class, used in the
     * source. It is replaced during the generation of code by
     * <code>Type.ARRAY_CLASS_LENGTH_FIELD_NAME</code>.<br>
     *
     * If the arrays are not allowed, then this is null.<br>
     * The default is null.
     */
    public String arrayClassLengthFieldName;
    /**
     * A fully qualified name of the string class, used in the source.
     * It is replaced during the generation of code by
     * <code>Type.STRING_QUALIFIED_CLASS_NAME</code>.<br>
     *
     * A respective class should exist in the library, with the contents
     * as described in Hedgeelleth user guide.<br>
     *
     * If the strings are not allowed, then this is null.<br>
     * The default is null.
     */
    public String stringClassName;
    /**
     * The name of the length field in the array class, used in the
     * source. It is replaced during the generation of code by
     * <code>Type.STRING_CLASS_LENGTH_FIELD_NAME</code>.<br>
     *
     * If the strings are not allowed, then this is null.<br>
     * The default is null.
     */
    public String stringClassLengthFieldName;
    /**
     * A fully qualified name of the root exception class, used in the source.
       It is replaced during the generation of code by
     * <code>Type.ROOT_EXCEPTION_QUALIFIED_CLASS_NAME</code>.<br>
     *
     * A respective class should exist in the library, with the contents
     * as described in Hedgeelleth user guide.<br>
     *
     * If the exceptions are not allowed, then this is null.<br>
     * The default is null.
     */
    public String rootExceptionClassName;
    /**
     * The name of the message field in the root exception class, used in the
     * source. It is replaced during the generation of code by
     * <code>Type.ROOT_EXCEPTION_CLASS_MESSAGE_FIELD_NAME</code>.<br>
     *
     * If the exceptions are not allowed, then this is null.<br>
     * The default is null.
     */
    public String rootExceptionClassMessageFieldName;
    /**
     * A fully qualified name of the unchecked exception class, used in the source.
     * This must be a subclass of the root exception class.
     * It is replaced during the generation of code by
     * <code>Type.UNCHECKED_EXCEPTION_QUALIFIED_CLASS_NAME</code>.<br>
     *
     * A respective class should exist in the library, with the contents
     * as described in Hedgeelleth user guide.<br>
     *
     * If the unchecked exceptions are not allowed, then this is null.<br>
     * The default is null.
     */
    public String uncheckedExceptionClassName;
    /**
     * Prefix to access fields of the method's class, null to allow direct access
     * of such fields. The default is null.
     */
    public String fieldAccessPrefix;
    /**
     * True to enable mutator methods and references. The default is false.
     */
    public boolean mutatorFlags;
    /**
     * Maximum numeric precision, as defined in <code>Type</code>, of the type
     * of an indexing expression. The default is <code>Type.NumericPrecision.INT</code>.<br>
     * 
     * If arrays are absent or not handled by Hedgeelleth, this value should be -1.
     */
    public int maxArrayIndexPrecision;

    /**
     * Creates a new instance of JavaFrontend.
     */
    public AbstractFrontend() {
        arrayClassName = null;
        arrayClassLengthFieldName = null;
        stringClassName = null;
        stringClassLengthFieldName = null;
        rootExceptionClassName = null;
        uncheckedExceptionClassName = null;
        rootExceptionClassMessageFieldName = null;
        fieldAccessPrefix = null;
        mutatorFlags = false;
        maxArrayIndexPrecision = Type.NumericPrecision.INT;
    }
    /**
     * Tests if a given file belongs to the library. To be used with
     * the parser.
     * 
     * @param libraryPath path to the library; must be equal to the prefix
     * of the names of the files which belong to the library
     * @param fileName tested file name or null for a non--library input
     * stream
     */
    protected boolean isLibrary(String libraryPath, String fileName) {
        if(fileName == null)
            return false;
        else
            return fileName.startsWith(libraryPath);
    }
    /**
     * Parses source file into internal representation, without semantic
     * check.
     *
     * @param compilation               compilation to add the translation
     *                                  results
     * @param topDirectory              top directory of the compiled files
     * @param libraryPath               path to the library
     * @param fileName                  file to compile
     * @param parseErrors               exception to add possible parse errors
     */
    public abstract void parse(Compilation compilation, PWD topDirectory,
            String libraryPath, String fileName, CompilerException parseErrors);
    /**
     * Returns the class of a semantic checker that should be applied to this
     * frontend's language. All frontends within a compilation must agree on
     * a single such class. If there are language--specific differences that
     * require different semantic check, then they can only involve different
     * parsing of <code>BlockExpression</code>, that should be defined in
     * <code>semanticCheck()</code>.
     * 
     * @return                          <code>SemanticCheck</code> or its
     *                                  subclass
     */
    public abstract Class getSemanticCheckClass();
    /**
     * Performs semantic check of <code>BlockExpression</code>,
     * after all source files were parsed. Only language--specific semantic
     * check not performed by the class returned by
     * <code>getSemanticCheckClass()</code> should be handled here. If
     * this method is called with <code>blockExpression</code> of the type
     * it should not handle, then the method should just do nothing.<br>
     *
     * The method is called only if <code>parse()</code> completed without
     * parse errors, and only for <code>BlockExpression</code> objects
     * that origin from source files parsed by this backend.<br>
     *
     * @param compilation               compilation to add the translation
     *                                  results
     * @param sc                        semantic check object, common for all
     *                                  compatible frontends
     * @param blockExpression           block expression to parse
     * @param compileErrors             exception to add possible compile errors
     */
    public abstract void semanticCheck(Compilation compilation, SemanticCheck sc,
            BlockExpression blockExpression, CompilerException compileErrors)
            throws CompilerException;
    /**
     * Returns a category of the special class represented by a given
     * name, within the language of this frontend.
     *
     * @param name                      fully qualified class name
     * @return                          category, never null
     */
    public SpecialClassCategory getSpecialClassCategory(String name) {
        if(name.equals(objectClassName))
            return SpecialClassCategory.OBJECT;
        else if(name.equals(arrayClassName))
            return SpecialClassCategory.ARRAY;
        else if(name.equals(stringClassName))
            return SpecialClassCategory.STRING;
        else if(name.equals(rootExceptionClassName))
            return SpecialClassCategory.ROOT_EXCEPTION;
        else if(name.equals(uncheckedExceptionClassName))
            return SpecialClassCategory.UNCHECKED_EXCEPTION;
        else
            return SpecialClassCategory.NONE;
    }
    /**
     * Translates a class name defined in this frontend into one defined
     * in another frontend.
     *
     * @param source                    fully qualified class name to
     *                                  translate
     * @param foreign                   frontend to which language
     *                                  the name should be translated
     * @return                          translated fully qualified
     *                                  class name
     */
    public String translateClassName(String source, AbstractFrontend foreign) {
        switch(getSpecialClassCategory(source)) {
            case NONE:
                // do nothing
                break;

            case OBJECT:
                source = foreign.objectClassName;
                break;

            case ARRAY:
                source = foreign.arrayClassName;
                break;

            case STRING:
                source = foreign.stringClassName;
                break;

            case ROOT_EXCEPTION:
                source = foreign.rootExceptionClassName;
                break;

            case UNCHECKED_EXCEPTION:
                source = foreign.uncheckedExceptionClassName;
                break;

            default:
                throw new RuntimeException("unknown special class category");
        }
        return source;
    }
    /**
     * Translates a type defined in this frontend into one defined
     * in another frontend.
     *
     * @param source                    type to translate, must represent
     *                                  a java class or a runtime exception
     *                                  is thrown
     * @param foreign                   frontend to which language
     *                                  the type should be translated
     * @return                          translated type
     */
    public Type translateType(Type source, AbstractFrontend foreign) {
        if(!source.isJava())
            throw new RuntimeException("source must represent a java class");
        return new Type(new NameList(translateClassName(source.getJava().toString(),
                foreign)), Type.Wildcard.NONE, source.nullableReference, source.mutatorReference);
    }
    /**
     * Returns the signature of the main method of an application in the language
     * of this frontend.
     * 
     * @param arrayClassName            fully qualified name of the
     *                                  array class, null for
     *                                  <code>ARRAY_QUALIFIED_CLASS_NAME</code>
     * @param stringClassName           fully qualified name of the
     *                                  string class, null for
     *                                  <code>STRING_QUALIFIED_CLASS_NAME</code>
     * @return
     * @see #arrayClassName
     * @see #stringClassName
     */
    abstract public MethodSignature getMainMethodSignature(String arrayClassName,
            String stringClassName);
    /**
     * Converts a list of strings into an argument of the
     * start method of a given thread.
     * 
     * @param args                      list of String converted
     *                                  into the argument of the
     *                                  start method; must be empty if the main method
     *                                  characteristic for a given frontend does not accept
     *                                  arguments
     * @param interpreter               interpreter, needed to create
     *                                  the arguments
     * @param thread                    a runtime thread, to which belongs the
     *                                  start method
     */
    public abstract void setMainMethodArgs(List<String> args, AbstractInterpreter interpreter,
            RuntimeThread thread) throws InterpreterException;
}
