/*
 * CliConstants.java
 *
 * Created on Jan 25, 2016
 *
 * Copyright (c) 2016  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.modules;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.Literal;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.NameList;

/**
 * CLI constants.
 * @author Artur Rataj
 */
public class CliConstants {
    public enum CType {
        INTEGER,
        DOUBLE,
        BOOLEAN,
        STRING,
    };
    /**
     * Names and values.
     */
    public SortedMap<String, List<Literal>> defines = new TreeMap<>();
    /**
     * If an array.
     */
    public SortedMap<String, Boolean> array = new TreeMap<>();
    /**
     * Type of a single value.
     */
    public SortedMap<String, CType> type = new TreeMap<>();
    
    /**
     * Adds a scalar.
     * 
     * @param name name of the constant
     * @param value value of the constant
     */
    public void add(String name, Literal value) {
        if(defines.keySet().contains(name))
            throw new RuntimeException("duplicate name");
        List<Literal> list = new LinkedList<>();
        list.add(new Literal(value));
        defines.put(name, list);
        array.put(name, false);
        CType t;
        if(value.type.type == null || value.type.getJava() != null)
            t = CType.STRING;
        else {
            switch(value.type.getPrimitive()) {
                case INT:
                    t = CType.INTEGER;
                    break;

                case DOUBLE:
                    t = CType.DOUBLE;
                    break;

                case BOOLEAN:
                    t = CType.BOOLEAN;
                    break;

                default:
                    throw new RuntimeException("unsupported type");
            }
        }
        type.put(name, t);
    }
    /**
     * Deletes a constant.
     * 
     * @param name name of the constant
     */
    public void remove(String name) {
        defines.remove(name);
        array.remove(name);
        type.remove(name);
    }
    /**
     * Adds an array.
     * 
     * @param name name of the constant
     * @param value values of the constant; list must not be empty and
     * all elements must be of the same type
     */
    public void add(String name, List<Literal> value) {
        if(defines.keySet().contains(name))
            throw new RuntimeException("duplicate name");
        List<Literal> list = new LinkedList<>();
        list.addAll(value);
        defines.put(name, list);
        array.put(name, true);
        CType t;
        if(value.get(0).type.type == null || value.get(0).type.getJava() != null)
            t = CType.STRING;
        else {
            switch(value.get(0).type.getPrimitive()) {
                case INT:
                    t = CType.INTEGER;
                    break;

                case DOUBLE:
                    t = CType.DOUBLE;
                    break;

                case BOOLEAN:
                    t = CType.BOOLEAN;
                    break;

                default:
                    throw new RuntimeException("unsupported type");
            }
        }
        type.put(name, t);
    }
    /**
     * Adds an integer scalar.
     * 
     * @param name name of the constant
     * @param value value of the constant
     */
    public void add(String name, int value) {
        add(name, new Literal(value));
    }
    /**
     * Adds a boolean scalar.
     * 
     * @param name name of the constant
     * @param value value of the constant
     */
    public void add(String name, boolean value) {
        add(name, new Literal(value));
    }
    /**
     * Adds a double scalar.
     * 
     * @param name name of the constant
     * @param value value of the constant
     */
    public void add(String name, double value) {
        add(name, new Literal(value));
    }
    /**
     * Adds a string scalar.
     * 
     * @param name name of the constant
     * @param value value of the constant, can be null
     * @param stringClass frontend--dependent name of the string class
     */
    public void add(String name, String value, String stringClass) {
        if(value == null)
            add(name, new Literal());
        else
            add(name, new Literal(value, stringClass));
    }
    /**
     * Adds an integer array.
     * 
     * @param name name of the constant
     * @param value values of the constant
     */
    public void add(String name, int[] value) {
        List<Literal> list = new LinkedList<>();
        for(int i : value)
            list.add(new Literal(i));
        add(name, list);
    }
    /**
     * Adds a double array.
     * 
     * @param name name of the constant
     * @param value values of the constant
     */
    public void add(String name, double[] value) {
        List<Literal> list = new LinkedList<>();
        for(double i : value)
            list.add(new Literal(i));
        add(name, list);
    }
    /**
     * Adds a boolean array.
     * 
     * @param name name of the constant
     * @param value values of the constant
     */
    public void add(String name, boolean[] value) {
        List<Literal> list = new LinkedList<>();
        for(boolean i : value)
            list.add(new Literal(i));
        add(name, list);
    }
    /**
     * Adds a string array.
     * 
     * @param name name of the constant
     * @param value values of the constant
     * @param stringCLass frontend--dependent name of the string class
     */
    public void add(String name, String[] value, String stringClass) {
        List<Literal> list = new LinkedList<>();
        for(String i : value)
            list.add(new Literal(i, stringClass));
        add(name, list);
    }
    @Override
    public String toString() {
        StringBuilder out = new StringBuilder();
        for(String name : defines.keySet()) {
            List<Literal> values = defines.get(name);
            boolean a = array.get(name);
            CType t = type.get(name);
            out.append(name);
            out.append(" ");
            out.append(t.toString());
            if(a)
                out.append("[]");
            out.append(" ");
            boolean first = true;
            for(Literal v : values) {
                if(!first)
                    out.append(", ");
                out.append(v.toString());
                first = false;
            }
            out.append("\n");
        }
       return out.toString();
    }
}
