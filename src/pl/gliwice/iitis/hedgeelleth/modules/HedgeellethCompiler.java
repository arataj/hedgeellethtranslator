/*
 * HedgeellethCompiler.java
 *
 * Created on Apr 14, 2009, 1:53:39 PM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.modules;

import java.util.*;
import java.io.*;

import pl.gliwice.iitis.hedgeelleth.compiler.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.CodeCompilation;
import pl.gliwice.iitis.hedgeelleth.compiler.code.Generator;
import pl.gliwice.iitis.hedgeelleth.compiler.code.GeneratorOptions;
import pl.gliwice.iitis.hedgeelleth.compiler.code.optimizer.Optimizer;
import pl.gliwice.iitis.hedgeelleth.compiler.sa.StaticAnalysis;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.languages.IntegerSetExpression;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.AbstractExpression;
import pl.gliwice.iitis.hedgeelleth.compiler.util.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.CompilerException.Report;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * A compiler class.
 *
 * @author Artur Rataj
 */
public class HedgeellethCompiler {
    public static class Options {
        /**
         * Experiment mode constants.
         */
        public enum ExperimentMode {
            /**
             * Never allow experiments.
             */
            FALSE,
            /**
             * <p>Always enable experiments, even if there
             * are no initializations integer sets.</p>
             * 
             * <p>Allows for set initializers of final static
             * fields.</p>
             */
            TRUE,
            /**
             * <p>Enable experiments if there is at least a single
             * field is initialized with an integer set.</p>
             * 
             * <p>Allows for set initializers of final static
             * fields.</p>
             * 
             * <p>Can not be used in compiler tests.</p>
             */
            ADAPT,
        };
        /**
         * The method <code>Generator.enableGeneratorTags</code>
         * is called with this parameter.
         *
         * Default is <code>true</code>.
         */
        public boolean enableGeneratorTags = true;
         /**
         * ExperimentsMode. Determines the structure of the output
         * files.
         *
         * Default is <code>FALSE</code>.
         */
        public ExperimentMode experimentMode = ExperimentMode.FALSE;
        /**
         * If the directory with experiment output files should be
         * cleaned before the files are generated, ignored if
         * <code>allowExperiments</code> is false.
         *
         * Default is <code>true</code>.
         */
        public boolean cleanExperimentsDirectory = true;
        /**
         * If to generate sources with integer set expressions
         * replaced with constants, that are dependent on the
         * experiment.
         *
         * Default is <code>false</code>.
         */
        public boolean generateExperimentSources = false;
        /**
         * Copied to <code>CompilationOptions.blankFinalsAllowed</code>.<br>
         * 
         * Default is <code>true</code>.
         */
        public boolean blankFinalsAllowed = true;
        /**
         * Copied to <code>GeneratorOptions.primaryRangeChecking</code>.<br>
         * 
         * Default is <code>true</code>.
         */
        public boolean primaryRangeChecking = true;
        /**
        * Style applied to compiler messages.<br>
        * 
        * The default is <code>CompilationOptions.Style.VERICS</code>.
        */
        public MessageStyle.Type messageStyle = MessageStyle.Type.VERICS;
        /**
         * Tunes the optimizer for a specific generation of code.<br>
         * 
         * Has its default values.
         */
        public Optimizer.Tuning optimizerTuning = new Optimizer.Tuning();
        /**
         * Command--line defines constants, keyed with names. The
         * lists of values should have arithmetic values and be sorted.<br>
         * 
         * The default is an empty map.
         */
        public CliConstants constants;
        /**
         * If to report generated output files in a machine--readable form.
         */
        public boolean reportOutput;
        /**
         * Test options, null if outside compiler tests.
         */
        public CompilationOptions.Test test;
    }
    /**
     * A short summary of translation, for producing the report.
     */
    public static class TranslationSummary {
        /**
         * Model type.
         */
        public final ModelType MODEL_TYPE;
        /**
         * List of generated files.
         */
        public final List<String> OUT_FILES;
        
        public TranslationSummary(ModelType modelType,
                List<String> outFiles) {
            MODEL_TYPE = modelType;
            OUT_FILES = outFiles;
        }
    };

    /**
     * Frontends keyed with file extensions, an instance of a frontend can
     * occur multiple times.
     */
    Map<String, AbstractFrontend> frontends;
    /**
     * Backend.
     */
    AbstractBackend backend;

    /**
     * Creates a new instance of HedgeellethCompiler.<br>
     * 
     * The frontends must be compatible, that is, thay all should return
     * the same class from <code>AbstractFrontend.getSemanticCheckClass()</code>.
     * See that method's docs for details on handling language--specific
     * semantic check.
     *
     * @param frontends                 frontends keyed with file extensions,
     *                                  an instance of a frontend can occur
     *                                  multiple times
     * @param backend                   backend
     */
    public HedgeellethCompiler(Map<String, AbstractFrontend> frontends,
            AbstractBackend backend) {
        this.frontends = frontends;
        this.backend = backend;
    }
    /**
     * Returns a frontend depending on the extension of a file name.
     * 
     * @param fileName                  name of the file
     * @return                          frontend
     */
    protected AbstractFrontend getFrontend(String fileName) {
        String extension = CompilerUtils.getFileNameExtension(
                fileName, 1);
        return frontends.get(extension);
    }
    /**
     * Produces all combinations of integer set constants.
     *
     * @param base base to make the combinations, of the length
     * <code>index</code>
     * @param index current base index, 0 for the top call
     * @param integerSets all integer sets used to make the combinations
     * @return resulting combinations
     */
    protected List<List<Long>> findAllCombinations(List<Long> base, int index,
            List<IntegerSetExpression> integerSets) {
        List<List<Long>> out = new LinkedList<>();
        for(long l : integerSets.get(index).values) {
            // new base
            List<Long> newBase = new LinkedList<>(base);
            newBase.add(l);
            if(newBase.size() == integerSets.size())
                // the new base is already a finished combination
                out.add(newBase);
            else
                out.addAll(findAllCombinations(newBase, index + 1, integerSets));
        }
        return out;
    }
    /**
     * Returns the experiment prefix, depending on current values of
     * constants in the integer set expressions.
     * 
     * @param integerSets               all integer set expressions
     *                                  in the compilation
     * @return                          prefix of the current experiment
     */
    protected String getExperimentPrefix(List<IntegerSetExpression> integerSets) {
        String experimentPrefix = "";
        for(IntegerSetExpression i : integerSets) {
            if(!experimentPrefix.isEmpty())
                experimentPrefix += ",";
            experimentPrefix += i.literal.getMaxPrecisionInteger();
        }
        return experimentPrefix;
    }
    /**
     * Generates source files, with integer set initializers replaced
     * with concrete constants.
     *
     * @param integerSets               all integer set expressions
     *                                  in the compilation
     * @param experimentDirectory       name of the experiment directory,
     *                                  without any separator chars
     */
    protected void generateSourceFiles(List<IntegerSetExpression> integerSets,
            PWD topDirectory, String experimentDirectory) throws IOException {
        Map<String, String> sources = new HashMap<>();
        // begin/end offsets of the expressions in the source stream
        Map<IntegerSetExpression, Integer> begin = new HashMap<>();
        Map<IntegerSetExpression, Integer> end = new HashMap<>();
        // get all files with integer set expressions and
        // compute the offsets
        for(IntegerSetExpression e : integerSets) {
            StreamPos beginPos = e.getStreamPos();
            StreamPos endPos = e.endPos;
            String contents;
            if(!sources.containsKey(beginPos.name)) {
                contents = CompilerUtils.newScanner(topDirectory.getFile(
                    beginPos.name)).useDelimiter("\\Z").next();
                sources.put(beginPos.name, contents);
            } else
                contents = sources.get(beginPos.name);
            int beginOffset = beginPos.getOffset(contents);
            int endOffset = endPos.getOffset(contents);
            if(beginOffset == -1 || endOffset == -1)
                throw new RuntimeException("could not determine expression's offset");
            begin.put(e, beginOffset);
            end.put(e, endOffset);
        }
        // read, modify, and save the source files
        String experimentPrefix = getExperimentPrefix(integerSets);
        for(String source : sources.keySet()) {
            String target = experimentDirectory + File.separatorChar +
                    experimentPrefix;
            File targetDirectory = topDirectory.getFile(target);
            if(!targetDirectory.exists() && !targetDirectory.mkdir())
                throw new IOException("could not create directory " +
                        targetDirectory.getAbsolutePath());
            target = target + File.separatorChar + source;
            String contents = sources.get(source);
            for(int x = 0; x < integerSets.size(); ++x) {
                IntegerSetExpression e = integerSets.get(x);
                if(e.getStreamPos().name.equals(source)) {
                    int beginOffset = begin.get(e);
                    int endOffset = end.get(e);
                    int cutLength = endOffset - beginOffset + 1;
                    String insertion = "" + e.literal.getMaxPrecisionInteger();
                    int insertionLength = insertion.length();
                    int diff = insertionLength - cutLength;
                    for(int y = x + 1; y < integerSets.size(); ++y) {
                        IntegerSetExpression f = integerSets.get(y);
                        if(f.getStreamPos().name.equals(source)) {
                            if(begin.get(f) > beginOffset) {
                                // correct the positions affected by this
                                // modification
                                begin.put(f, begin.get(f) + diff);
                                end.put(f, end.get(f) + diff);
                            }
                        }
                    }
                    contents = contents.substring(0, beginOffset) +
                            insertion +
                            contents.substring(endOffset + 1);
                }
            }
            PrintWriter out = new PrintWriter(topDirectory.getFile(target));
            out.println(contents);
            out.close();
        }
    }
    /**
     * A method for compiling a set of files, with given option values.<br>
     *
     * If there were translation errors, they are reported within a thrown
     * <code>CompilerException</code>.<br>
     *
     * If the frontends actually used in the current translation are
     * incompatible, this method throws a runtime exception.
     *
     * @param inputTopDirectory         top directory of the compiled files
     * @param outputTopDirectory        top directory of the output files
     * @param libraryPath               path to the library
     * @param files                     files to compile
     * @param optimizations             optimizations to perform; it excludes
     *                                  the backend, which may perform additional
     *                                  optimizations
     * @param options                   translation options of this compiler
     * @return                          list with the names of the generated
     *                                  output files, if no output file
     *                                  is generated for whatever reason,
     *                                  then the list is empty; the order
     *                                  of the files in the list is:
     *                                  subsequent experiments, and within
     *                                  an experiment the files are ordered
     *                                  by their types as defined in
     *                                  <code>AbstractBackend.translate</code>
     */
    public TranslationSummary translate(PWD inputTopDirectory, PWD outputTopDirectory,
            String libraryPath, List<String> files, List<Optimizer.Optimization> optimizations,
            HedgeellethCompiler.Options options) throws CompilerException {
        if(files.isEmpty())
            throw new CompilerException(null, CompilerException.Code.IO,
                    "no input files");
        Compilation compilation = new Compilation(
                new CompilationOptions(options));
        // collects parse errors from all files
        CompilerException parseErrors = new CompilerException();
        // sort the files for more repeatable identifiers in
        // the output file
        Set<String> sortedFiles = new TreeSet<>(files);
        Set<Class> sc = new HashSet<>();
        for(String fileName : sortedFiles) {
            AbstractFrontend f = getFrontend(fileName);
            f.parse(compilation, inputTopDirectory,
                    libraryPath, fileName, parseErrors);
            sc.add(f.getSemanticCheckClass());
        }
        if(sc.size() > 1)
            throw new RuntimeException("incompatible frontends");
        if(parseErrors.reportsExist())
            throw parseErrors;
        List<IntegerSetExpression> integerSets = null;
        // System.out.println("Semantic check...");
        try {
            java.lang.reflect.Constructor[] constructors = sc.iterator().next().getConstructors();
            if(constructors.length != 1)
                throw new RuntimeException("semantic checker should have exactly one " +
                        "public constructor");
            // perform semantic check
            SemanticCheck c = (SemanticCheck)constructors[0].newInstance(
                    compilation, null);
            integerSets = c.integerSets;
        } catch(Exception e) {
            String cause;
            if(e.getCause() != null) {
                if(e.getCause() instanceof CompilerException)
                        throw (CompilerException)e.getCause();
                cause = ", cause is: " + e.getCause().toString() + " at " +
                    e.getCause().getStackTrace()[0].toString();
            } else
                cause = "";
            String s = "could not create semantic checker: " +
                    e.toString() + cause;
            System.err.println("HedgeellethCompiler: " + s);
            throw new RuntimeException(s);
        }
        Iterator<List<Long>> combinations;
        if(!integerSets.isEmpty()) {
            if(options.experimentMode == Options.ExperimentMode.ADAPT &&
                    !integerSets.isEmpty())
                options.experimentMode = Options.ExperimentMode.TRUE;
            if(options.experimentMode == Options.ExperimentMode.TRUE) {
                // check if all initializers initialize fields as required
                CompilerException e = new CompilerException();
                for(IntegerSetExpression i : integerSets)
                    if(i.fullyQualifiedFieldName == null)
                        e.addReport(new Report(i.getStreamPos(),
                            CompilerException.Code.ILLEGAL, "integer set does not " +
                            "initialize a field"));
                if(e.reportsExist())
                    throw e;
                // generate combinations
                combinations = findAllCombinations(
                        new LinkedList<Long>(), 0, integerSets).iterator();
            } else {
                CompilerException e = new CompilerException();
                for(AbstractExpression x : integerSets)
                    e.addReport(new Report(x.getStreamPos(),
                        CompilerException.Code.ILLEGAL, "integer set found but " +
                        "experiments not enabled"));
                throw e;
            }
        } else
            combinations = null;
        List<String> outputFileNames = new LinkedList<>();
        Set<ModelType> modelTypes = new HashSet<>();
        // System.out.println("ok.");
        AbstractBackend.OutputMode outputMode = null;
        boolean experimentsPending = false;
        boolean first = true;
        do {
            if(options.experimentMode == Options.ExperimentMode.TRUE &&
                    combinations != null) {
                Iterator<Long> currCombination = combinations.next().iterator();
                for(IntegerSetExpression i : integerSets) {
                    Long j = currCombination.next();
                    // let the integer set expression <code>i</code> acts
                    // as a constant of the value of <code>l</code> in the
                    // current experiment
                    i.literal = new Literal(i.getResultType(), j);
                }
                if(first && options.cleanExperimentsDirectory)
                    outputMode = AbstractBackend.OutputMode.EXPERIMENT_CLEAN;
                else
                    outputMode = AbstractBackend.OutputMode.EXPERIMENT;
            } else
                outputMode = AbstractBackend.OutputMode.NORMAL;
            // System.out.println("Static analysis...");
            StaticAnalysis sa = new StaticAnalysis(compilation);
            // System.out.println("ok.");
            // System.out.println("Generating code...");
            GeneratorOptions generatorOptions = new GeneratorOptions();
            generatorOptions.enabledGeneratorTags = options.enableGeneratorTags;
            generatorOptions.primaryRangeChecking =
                    options.primaryRangeChecking;
            Generator generator = new Generator(compilation,
                    generatorOptions);
            CodeCompilation codeCompilation = generator.compile();
            codeCompilation.modelControl = compilation.modelControl;
            // System.out.println(codeCompilation.toString());
            Optimizer optimizer = new Optimizer(optimizations);
            optimizer.optimize(codeCompilation,
                    options.optimizerTuning);
            // System.out.println(codeCompilation.toString());
            // System.out.println("ok.");
            String experimentPrefix;
            if(options.experimentMode == Options.ExperimentMode.TRUE)
                experimentPrefix = getExperimentPrefix(integerSets);
            else
                experimentPrefix = "";
            // preserve the verbatim suffix
            String verbatimSuffix = codeCompilation.modelControl.getVerbatimSuffix();
            outputFileNames.addAll(backend.translate(
                    codeCompilation, outputTopDirectory,
                    experimentPrefix, parseErrors,
                    outputMode, modelTypes));
            codeCompilation.modelControl.removeVerbatim();
            codeCompilation.modelControl.appendVerbatim(verbatimSuffix);
            if(parseErrors.reportsExist())
                throw parseErrors;
            if(options.experimentMode == Options.ExperimentMode.TRUE) {
                if(options.generateExperimentSources) {
                    String d = outputFileNames.iterator().next();
                    d = d.substring(0, d.lastIndexOf(File.separatorChar));
                    try {
                        generateSourceFiles(integerSets, outputTopDirectory, d);
                    } catch(IOException e) {
                        throw new CompilerException(null, CompilerException.Code.IO,
                                "could not write source file: " + e.getMessage());
                    }
                }
                experimentsPending = combinations.hasNext();
                if(experimentsPending)
                    sa.restore();
            }
            first = false;
        } while(experimentsPending);
        if(options.experimentMode == Options.ExperimentMode.TRUE) {
            // write the key file
            File f = new File(outputFileNames.iterator().next());
            File key = outputTopDirectory.getFile(f.getParent() +
                    File.separatorChar + "key.txt");
            try {
                try(PrintWriter writer = new PrintWriter(key)) {
                    for(IntegerSetExpression i : integerSets)
                        writer.println(i.fullyQualifiedFieldName);
                }
            } catch(IOException e) {
                throw new CompilerException(null, CompilerException.Code.IO,
                        "could not write key file: " + e.getMessage());
            }
        }
        if(outputFileNames.isEmpty())
            throw new RuntimeException("no output files generated but no " +
                    "exception thrown");
        if(modelTypes.size() > 1)
            throw new RuntimeException("unexpected: multiple model types generated");
        return new TranslationSummary(modelTypes.isEmpty() ?
                null : modelTypes.iterator().next(), outputFileNames);
    }
}
