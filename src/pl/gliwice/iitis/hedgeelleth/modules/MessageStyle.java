/*
 * MessageStyle.java
 *
 * Created on Jul 3, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.modules;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.CompilerException;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.CompilerException.Report;

/**
 * Applies a given <code>CompilationOptions.Style</code> to compiler
 * messages.
 * 
 * @author Artur Rataj
 */
public class MessageStyle {
    /**
     * Prefix denoting a style variable;
     */
    final static String PREFIX = "${";
    /**
     * Suffix denoting a style variable;
     */
    final static String SUFFIX = "}";
    /**
     * Keyword for a stack trace.
     */
    final static String STACK_TRACE_KEYWORD = "stack";
    /**
     * Prefix, which precedes stack trace within exceptions.
     */
    final public static String STACK_TRACE_PREFIX =
            PREFIX + STACK_TRACE_KEYWORD + SUFFIX;
    /**
     * type of the style used to tune compiler messages.
     */
    public enum Type {
        JAVA,
        VERICS;

        /**
         * Dictionary. Each entry begins with a variable name, then there
         * are translations, at indices as returned by <code>styleIndex</code>.<br>
         *
         * To specify a plural form within the processed text, append <code>$</code> to
         * a variable name, as in <code>variableName$</code>.<br>
         * 
         * To specify a plural in the dictionary:
         * <code>singular form/plural form</code>. If the
         * latter is <code>singular form</code> with s appended, then it
         * may be omitted as in <code>singular form/<code>.<br>
         * 
         * Verbatim "/" can be escaped by "\\/".
         */
        final static String[][] dictionary = {
            { "void", "void/", "V/", },
            { "boolean", "boolean/", "B/", },
            { "char", "char/", "java.char/", },
            { "byte", "byte/", "java.byte/", },
            { "short", "short/", "java.short/", },
            { "int", "int/", "Z\\/32/", },
            { "long", "long/", "Z/", },
            { "float", "float/", "R\\/32/", },
            { "double", "double/", "R/", },
            { "variable", "variable/", "symbol/", },
            { "finalVariable", "final variable/", "constant/", },
            { "java.lang.", "/", "java.lang./", },
            { "null", "null/", "null/", },
            { "nonObjective", "static/", "non-objective/", },
            { "private", "private/", "pv/", },
            { "protected", "protected/", "ex/", },
            { "public", "public/", "ap/", },
            { "public api", "public api/", "pa/", },
            { "internal", "internal/", "pk/", },
            { "protected internal", "protected internal/", "ac/", },
            { STACK_TRACE_KEYWORD, "stack/", "stack/"},
            { "verics.lang.", "verics.lang./", "/"},
        };
        /**
         * Return an index into a dictionary entry of a string
         * belonging to a specific style.
         * 
         * @param type style
         * @return index
         */
        static int styleIndex(Type type) {
            switch(type) {
                case JAVA:
                    return 1;
                    
                case VERICS:
                    return 2;
                    
                default:
                    throw new RuntimeException("unknown style");
            }
        }
        /**
         * Finds the index of the first slash not scaped by "\\".
         * 
         * @param s string to scan
         * @return index ot -1 for none
         */
        protected int indexOfNotEscapedSlash(String s) {
            int pos = 0;
            while(pos < s.length()) {
                pos = s.indexOf("/", pos);
                if(pos == -1 || pos == 0)
                    break;
                if(s.charAt(pos - 1) != '\\')
                    break;
                ++pos;
            }
            return pos;
        }
        /**
         * Returns a string that substitutes a given style variable.
         * 
         * @param miscVariables miscelanous variables, keyed with names
         * without delimiters; value is style--independend, but may have
         * plural/singular forms as described in <code>dictionary</codr>,
         * `/' must be escaped thus; null for none
         * @param name name of a style variable; if contains dots, then the
         * successive separated parts are are compared against the dictionary,
         * and then the first prefix is replaced
         * @return style--specific string, null if no subtitute found
         */
        String lookup(Map<String, String> miscVariables, String name) {
            int miscVariablesNum;
            Iterator<String> miscVariablesIterator;
            if(miscVariables == null) {
                miscVariablesNum = 0;
                miscVariablesIterator = null;
            } else {
                miscVariablesNum = miscVariables.size();
                miscVariablesIterator = miscVariables.keySet().iterator();
            }
            boolean plural = name.endsWith("$");
            if(plural)
                name = name.substring(0, name.length() - 1);
            for(int index = 0; index < dictionary.length + miscVariablesNum; ++index) {
                String key;
                if(index < dictionary.length)
                    key = dictionary[index][0];
                else
                    key = miscVariablesIterator.next();
                if(name.equals(key)) {
                    String translation;
                    if(index < dictionary.length)
                        translation = dictionary[index][styleIndex(this)];
                    else
                        translation = miscVariables.get(key);
                    String singularForm = translation.substring(0, indexOfNotEscapedSlash(translation));
                    if(plural) {
                        String pluralForm = translation.substring(indexOfNotEscapedSlash(translation) + 1);
                        if(pluralForm.isEmpty())
                            pluralForm = singularForm + "s";
                        return pluralForm.replace("\\/", "/");
                    } else
                        return singularForm.replace("\\/", "/");
                }
            }
            // not found, let us look at prefixes
            int initPos = name.indexOf(".");
            int pos = initPos;
            while(pos != -1 && pos < name.length() - 1) {
                String s = name.substring(0, pos + 1);
                try {
                    s = lookup(miscVariables, s);
                } catch(IllegalArgumentException f) {
                    if(pos < name.length())
                        pos = name.indexOf(".", pos + 1);
                    else
                        pos = -1;
                    continue;
                }
                return s + name.substring(pos + 1);
            }
            if(initPos != -1 && initPos < name.length() - 1)
                // a non--translatable qualified name
                return name;
            else
                throw new IllegalArgumentException("unknown style variable \"" + name + "\"");
        }
    };
    
    /**
     * Replaces style variables in the form <code>${variableName}</code> with
     * style--dependent strings.
     * 
     * @param type style type
     * @param miscVariables miscelanous variables, keyed with names
     * without delimiters, null for none; passed to <code>lookup()</code>, for details
     * see that method
     * @param in string to read
     * @return processed string
     */
    protected static String process(Type type, Map<String, String> miscVariables,
            String in) {
        StringBuilder out = new StringBuilder();
        int pos = 0;
if(in == null)
    in = in;
        while(pos < in.length()) {
            int begin = in.indexOf(PREFIX, pos);
            if(begin == -1) {
                out.append(in.substring(pos));
                break;
            } else
                out.append(in.substring(pos, begin));
            begin += PREFIX.length();
            int end = in.indexOf(SUFFIX, begin);
            if(end == -1)
                throw new RuntimeException("style variable not terminated in \"" +
                        in + "\"");
            String name = in.substring(begin, end);
            String translation = type.lookup(miscVariables, name);
            out.append(translation);
            pos = end + SUFFIX.length();
        }
        return out.toString();
    }
    /**
     * Appliess <code>process()</code> to a compiler exception.
     * 
     * @param type style type
     * @param miscVariables miscelanous variables, keyed with names
     * without delimiters, null for none; passed to <code>lookup()</code>,
     * for details see that method
     * @param messages exception with reports to process
     */
    public static void apply(Type type, Map<String, String> miscVariables,
            CompilerException messages) {
        Collection<Report> list = new LinkedList<>();
        for(Report r : messages.getReports()) {
            r.message = process(type, miscVariables, r.message);
            // name can be null for input stream
            if(r.pos != null && r.pos.name != null)
                r.pos.name = process(type, miscVariables, r.pos.name);
            list.add(r);
        }
        // sort reports again, as messages might have changed
        messages.clearReports();
        for(Report r : list)
            messages.addReport(r);
    }
}
