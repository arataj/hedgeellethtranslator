/*
 * AbstractTokenizer.java
 *
 * Created on Oct 10, 2015
 *
 * Copyright (c) 2015  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.tokenizer;

import java.util.*;

/**
 * An abstract syntax--coloring tokenizer.
 * 
 * @author Artur Rataj
 */
public abstract class AbstractTokenizer {
    /**
     * List of tokens.
     */
    protected List<TokenDescription> tokens;
    /**
     * Tokens keyed with their ids.
     */
    protected Map<Integer,TokenDescription> idToToken;
    
    /**
     * Initializes the look--up tables.
     */
    public AbstractTokenizer() {
        tokens = new ArrayList<>();
        idToToken = new TreeMap<>();
    }
    public TokenDescription getToken(int id) {
        return idToToken.get(id);
    }
    public Collection<TokenDescription> getTokenIds() {
        return tokens;
    }
}
