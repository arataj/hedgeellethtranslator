/*
 * HedgeellethToken.java
 *
 * Created on Oct 10, 2015
 *
 * Copyright (c) 2015  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.tokenizer;

import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.StreamPos;

/**
 * A single token in a stream: type and position.
 * 
 * @author Artur Rataj
 */
public class HedgeellethToken {
    /**
     * Type of this token.
     */
    public final TokenDescription TYPE;
    /**
     * First character of this token in the stream.
     */
    public final StreamPos START;
    /**
     * Last character of this token in the stream.
     */
    public final StreamPos STOP;
    
    public HedgeellethToken(TokenDescription type, StreamPos start, StreamPos stop) {
        TYPE = type;
        START = start;
        STOP = stop;
    }
    @Override
    public String toString() {
        return START.toString() + "..." + STOP.toString() + ":" + TYPE.toString();
    }
}
