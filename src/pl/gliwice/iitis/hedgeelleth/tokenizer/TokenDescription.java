/*
 * TokenDescription.java
 *
 * Created on Oct 10, 2015
 *
 * Copyright (c) 2015  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.tokenizer;

/**
 * Description of a token to color.
 * 
 * @author Artur Rataj
 */
public class TokenDescription {
    /**
     * Name of this token.
     */
    public final String NAME;
    /**
     * Class of this token.
     */
    public final String CLASS;
    /**
     * Parser--specific id of this token.
     */
    public final int ID;
    
    /**
     * Creates a description of a token.
     * 
     * @param name name
     * @param clazz class
     * @param id parser--specific id
     */
    public TokenDescription(String name, String clazz, int id) {
        NAME = name;
        CLASS = clazz;
        ID = id;
    }
    @Override
    public String toString() {
        return CLASS + "::" + NAME;
    }
}
