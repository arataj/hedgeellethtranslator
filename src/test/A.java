/*
 * A.java
 *
 * Created on Jan 27, 2009
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package test;

import java.util.*;

/**
 *
 * @author Artur Rataj
 */
public class A {
    static int fa;
    static int staticF;
    Object sub(int i) {
        return 0;
    }
    final int fi;
    /**
     * Creates a new instance of A. 
     */
    public A() {
        super();
        m();
        fi = m();
        int i = 2;
            int k = 7;
            i = 7;
        K: {
            int q = 9;
            i = 18;
            if(i == 18)
                break K;
            i = 20;
        }
        int q = 4;
        do {
            break;
        } while(i == 7);

        switch(i) {
            default:
                i = 4;
                break;

            case 2:
                i = -4;
        }
    }
    String get(A a, B b) {
        return "A.get(A, B)";
    }
    protected int m() {
        return fi;
    }
    public static void main(String[] args) {
        /*
        List<String> listStrings = new ArrayList<String>();
        List<? super String> listObjects = listStrings;
        List<? extends String> i = new LinkedList<String>();
        List<? extends Object> o = i;
        List<? super Set> iS = new LinkedList<Set>();
        List<? super TreeSet> oS = iS;
        Object oo = oS.get(0);
        listObjects.add(new String());
        
        //String str = listObjects.get(0);
*/
         new A();
    }
}
