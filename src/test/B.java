/*
 * B.java
 *
 * Created on Jan 27, 2009
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package test;

/**
 *
 * @author Artur Rataj
 */
class B extends A {
    static int staticF;
    private int fu;
    @Override
    public Integer sub(int i) {
        switch(i) {
            case 2+4:
                break;

        }
        return 0;
    }

    /**
     * Creates a new instance of B. 
     */
    public B() {
        int i = super.staticF;
        do {
            break;
        } while(i == 5);
    }
    String get(C a, B b) {
        return "B.get(A, A)";
    }
    String get(C c) {
        return "B.get(c)";
    }
    public static void main(String[] args) {
        A a = new A();
        B b = new B();
        C c = new C();
        if(A.class.isInstance(b))
            ;
        System.out.println(b.get(c, c));
    }
    protected int m() {
        return 5;
    }
}
