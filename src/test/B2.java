/*
 * B2.java
 *
 * Created on Jan 27, 2009
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package test;

/**
 *
 * @author Artur Rataj
 */
public class B2 extends A {
    /**
     * Creates a new instance of B2. 
     */
    public B2() {
    }
}
