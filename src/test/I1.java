/*
 * I1.java
 *
 * Created on Apr 28, 2009
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package test;

/**
 *
 * @author Artur Rataj
 */
public interface I1 extends I0 {
    public int r();
}
